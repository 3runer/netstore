<%-- Start NetStore  
NOTE:  This jsp is assumed to exist in the web application root.--%>
<html>

<%-- Imports --%>
<%@ page session="false" import="net.ibs.framework.*,se.ibs.bap.util.*,se.ibs.ccf.*,se.ibs.ns.cf.*" %>

<%-- Declarations --%>
<% 	
	// No caching
	response.setHeader("Cache-Control", "no-cache");	// For HTTP version 1.1
	response.setHeader("Pragma", "no-cache");			// For HTTP version 1.0
	response.setDateHeader("Expires", 0); 				// To prevent caching at the proxy server 
	
	String nextPage = null;
	String property  = null;
	
	// Check if NetStore is closed
    ApplicationStatus status = NSObject.getApplicationStatus();
    if (status != null
    		&& status.getCloseStatus() >= ApplicationStatus.CLOSED_FOR_NEW) {
   		nextPage = NSConstantsPages.NSCLOSED;
	}

    // override config
    if (!IBSNetStoreEnvironment.isRegistered("NetStore")) {
    	if (IBSNetStoreEnvironment.getBasePath() == null) {
    		property = System.getProperty("xt.bin");
    		if (property != null && (property = property.trim()).length() > 0) {
				IBSNetStoreEnvironment.setBasePath(property);	
			}
    	}
		property = System.getProperty("xt.esite");
		if (property == null || (property = property.trim()).length() == 0) {
			property = "NS";
		}
		try {
			IBSNetStoreEnvironment.registerApplication(property, "NetStore");
		} catch (Exception e) {
   			BAPTrace.error("NSStart.jsp", "Unexpected exception: ", e);
   			request.setAttribute("SystemSetupError", e.getMessage());
   			nextPage = "SystemSetupError.jsp";
   		}	
  	}
    if(System.getProperty("xt.esite") != null)
    {
    property = System.getProperty("xt.esite");
    }
    
 	// initialize NetStore (if not already so)
    if (nextPage == null) {
    	try { 	
	 		NSObject.handle(NSObject.LOAD);
	 		// is session in use?
			Session s = new Session(request.getSession(false));
	    	if (!s.isEmpty()) {
	    		String clientAppl =
	    			NSObject.getConfigAttribute(CCFConstantsConfig.CLIENT_APPLICATION);		
	    		String comparand = (String) s.getAttribute(CCFConstantsSession.CLIENT_APPLICATION);
	    		if (comparand == null || !clientAppl.equalsIgnoreCase(comparand)) {
	    			SessionTracker.unregisterSession(s);
	    			s = new Session(request.getSession(true));
	    		}
	    	}
    	} catch (Exception e) {
    		BAPTrace.error(NSConstantsPages.NSSTART, "Unexpected exception: ", e);
	   		request.setAttribute("SystemSetupError", e.getMessage());
	   		nextPage = NSConstantsPages.SYSTEMSETUPERROR;
    	}	
    }
    
    if (nextPage != null) {
    	nextPage = NSObject.getJspName(nextPage);
    	if (!nextPage.startsWith("/")) {
    		nextPage = "/" + nextPage;
    	}
    	RequestDispatcher rd = application.getRequestDispatcher(nextPage);
 	    rd.forward(request, response);
 	    return;
    }
    
    // Get start servlet 
    String startServlet = response.encodeURL(NSObject.getServlet("se.ibs.ns.cf.StartServlet"));
%>

<head>
	<title>Start</title>
	<script language="JavaScript">
		function redirect(url) {
  			top.location.replace(url);
		}
		function wsRedirect(appName, url) {
  			top.location.replace("/"+appName+"/home.html");
		}
	</script>
</head>

<body onLoad="javascript:wsRedirect('<%=property%>', '<%= startServlet %>'); return false;"></body> 

</html> 
