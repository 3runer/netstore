/*
 *  All javascripts used in NetStore  
 */
/*******************************************************************************
 * Method to validate length of text.
 ******************************************************************************/
function validateLength(inputElement, length) {
	if (inputElement.value.length > length)
		inputElement.value = inputElement.value.substring(0, length);
}

/*******************************************************************************
 * Message alert
 * 
 * @param headTxt
 *            the header text
 * @param msg
 *            the message to display
 ******************************************************************************/
function message(headTxt, msg) {
	alert(headTxt + "\n\n" + msg);
}

/*******************************************************************************
 * 
 * Select all checkboxes on a page.
 * 
 * @param selectAllCheckbox
 *            the checkbox invoking this function
 * 
 ******************************************************************************/
function selectAllCheckboxes(formName, selectAllCheckbox) {
	len = document.forms[formName].elements.length;
	var i = 0;
	while (i != len) {
		if (document.forms[formName].elements[i].type == 'checkbox'
				&& document.forms[formName].elements[i].name != eval("selectAllCheckbox.name"))
			document.forms[formName].elements[i].checked = selectAllCheckbox.checked;
		i++;
	}
}

/*******************************************************************************
 * disable/enable the given checkbox
 * 
 * @param name
 *            the name of the checkbox to enable/disable
 * @disable disable (true) or enable (false) the checkbox
 ******************************************************************************/
function disableCheckbox(formName, name, disable) {
	var length = document.forms[formName].elements.length;
	for ( var i = 0; i < length; i++) {
		var element = document.forms[formName].elements[i];
		if (element.type == 'checkbox' && element.name == eval("name")) {
			element.disabled = disable;
			if (disable)
				element.checked = false;
			break;
		}
	}
}

/*******************************************************************************
 * 
 * Set checkboxes to selected or deselected
 * 
 * @param checked
 *            Set checkboxes to selected or deselected (true or false)
 * @param name
 *            The name of the check boxes to select or deselect
 * 
 ******************************************************************************/
function toggleCheckboxes(formName, checked, name) {
	len = document.forms[formName].elements.length;
	var i = 0;
	while (i != len) {
		if (document.forms[formName].elements[i].type == 'checkbox'
				&& document.forms[formName].elements[i].name == eval("name"))
			document.forms[formName].elements[i].checked = checked;
		i++;
	}
}

/*******************************************************************************
 * Updates the entry string to uppercase.
 * 
 * @param inputElement
 *            The charactes to changes to uppercase
 ******************************************************************************/
function toUpperCase(inputElement) {
	if (inputElement.value.length > 0)
		inputElement.value = inputElement.value.toUpperCase();
}

/*******************************************************************************
 * Confirm sign off
 * 
 * @param headTxt
 *            the header text
 ******************************************************************************/
function confirmSignoff(headtxt, href) {
	if (top.window.frameSetLoaded) {
		if (confirm(headtxt + "!")) {
			top.window.frameSetLoaded = false;
			top.location.replace(href);
		}
	}
}

/*******************************************************************************
 * Switch language
 * 
 * Ignored if frameset not loaded yet
 ******************************************************************************/
function languageSwitch(href, code, warningText) {
	if (top.window.frameSetLoaded) {
		if (!top.window.blockedLanguageSwitch || confirm(warningText)) {
			top.window.frameSetLoaded = false;
			top.location.replace(href + '?TO_LANGUAGE=' + code);
		}
	}
}
/*******************************************************************************
 * Replace the frameset
 * 
 * Ignored if frameset not loaded yet
 ******************************************************************************/
function replaceFrameset(href) {
	top.window.frameSetLoaded = false;
	top.location.replace(href);
}

/*******************************************************************************
 * General home frame confirmation prompt
 ******************************************************************************/
function confirmation(text, href) {
	if (confirm(text)) {
		getFrame().location.replace(href);
	}
}

/*******************************************************************************
 * Allowed card checkbox changed from selected to not selected confirmation
 ******************************************************************************/
function confirmCardInactivation(formName, text, actionName, checkbox) {
	if (confirm(text)) {
		submitLinkOnce(formName, actionName, checkbox.value);
	} else {
		checkbox.checked = !checkbox.checked;
	}
}

/*******************************************************************************
 * Allowed card checkbox changed from not selected to selected confirmation
 ******************************************************************************/
function confirmCardActivation(formName, text, cardInfo, actionName, checkbox) {
	text = text + "\n" + cardInfo;
	cn = prompt(text, "");
	if (cn == null) {
		checkbox.checked = !checkbox.checked;
	} else {
		submitLinkOnce(formName, actionName, cn);
	}
}
/*******************************************************************************
 * Open standard sized a possitioned new window with the specified jsp or html
 * 
 * Ignored if frameset not loaded yet
 ******************************************************************************/
function showWindow(jspName) {
	if (top.window.frameSetLoaded) {
		if (top.window.infoWindow != null && !top.window.infoWindow.closed) {
			top.window.infoWindow.close();
		}
		if (navigator.userAgent.indexOf("MSIE") != -1) {
			top.window.infoWindow = window
					.open(
							jspName,
							"NetStore",
							"left=500,top=200,height=400,width=600,toolbar=no,menubar=no,location=no,status=yes,scrollbars=yes,resizable=yes");
			top.window.infoWindow.focus();
		} else {
			top.window.infoWindow = window
					.open(
							jspName,
							"NetStore",
							"screenX=500,screenY=200,height=400,width=600,toolbar=no,menubar=no,location=no,status=yes,scrollbars=yes,resizable=yes");
			top.window.infoWindow.focus();
		}
	}
}

/*******************************************************************************
 * Open new window for the specified jsp or html with specifeied size and pos
 * 
 * Ignored if frameset not loaded yet
 ******************************************************************************/
function showWindow(jspName, anchorName, windowName, x, y, h, w) {

	if (top.window.frameSetLoaded) {
		if (top.window.infoWindow != null && !top.window.infoWindow.closed) {
			top.window.infoWindow.close();
		}
		str = ",height="
				+ h
				+ ",width="
				+ w
				+ ",toolbar=no,menubar=no,location=no,status=no,scrollbars=yes,resizable=yes";
		if (navigator.userAgent.indexOf("MSIE") != -1) {
			str = "left=" + x + ",top=" + y + str;
		} else {
			str = "screenX=" + x + ",screenY=" + y + str;
		}

		if (anchorName != null) {
			jspName = jspName + "#" + anchorName;
		}
		top.window.infoWindow = window.open(jspName, windowName, str);
		top.window.infoWindow.focus();
	}
}
function showCartWindow(jspName, anchorName, windowName, x, y, h, w) {

	if (top.window.frameSetLoaded) {
		if (top.window.cartWindow != null && !top.window.cartWindow.closed) {
			top.window.cartWindow.close();
		}
		str = ",height="
				+ h
				+ ",width="
				+ w
				+ ",toolbar=no,menubar=no,location=no,status=no,scrollbars=yes,resizable=yes";
		if (navigator.userAgent.indexOf("MSIE") != -1) {
			str = "left=" + x + ",top=" + y + str;
		} else {
			str = "screenX=" + x + ",screenY=" + y + str;
		}

		if (anchorName != null) {
			jspName = jspName + "#" + anchorName;
		}
		top.window.cartWindow = window.open(jspName, windowName, str);
		top.window.cartWindow.focus();
	}
}
/*******************************************************************************
 * cross-window JavaScripting. Open new window for the specified jsp or html
 * with specified size and pos Checks if the exist already open remote window
 * Ignored if frameset not loaded yet
 ******************************************************************************/

var listName = "";
var itemKey = null;
var action = "";
function openShoppingListWindow(actionValue, key, jspName, windowName, x, y, h,
		w) {

	if (top.window.frameSetLoaded) {

		itemKey = key;
		action = actionValue;

		if (top.window.shoppingListWindow != null
				&& !eval("typeof(top.window.shoppingListWindow)!=\"undefined\"")
				&& !top.window.shoppingListWindow.closed) {// if mywindow has
			// been opened, and
			// mywindow hasn't
			// already been
			// closed
			top.window.shoppingListWindow.close(); // close it in order to
			// re-assign properties
		}

		str = ",height="
				+ h
				+ ",width="
				+ w
				+ ",toolbar=no,menubar=no,location=no,status=no,scrollbars=yes,resizable=yes";
		if (navigator.userAgent.indexOf("MSIE") != -1) {
			str = "left=" + x + ",top=" + y + str;
		} else {
			str = "screenX=" + x + ",screenY=" + y + str;
		}

		top.window.shoppingListWindow = open(jspName, windowName, str);

		// If no opener was found can happen in NetScape 2.x
		if (top.window.shoppingListWindow.opener == null) {
			top.window.shoppingListWindow.opener = self;
		}
		top.window.shoppingListWindow.focus();
	}
}
/*******************************************************************************
 * Submit the specfied button when the Enter key is pressed
 ******************************************************************************/
function submitEnter(button, ev) {
	var keycode;
	if (window.event)
		keycode = window.event.keyCode;
	else if (ev)
		keycode = ev.which;
	else
		return true;

	if (keycode == 13) {
		submitButtonOnce(button);
		return false;
	} else
		return true;
}
/*******************************************************************************
 * Submit the specfied link when the Enter key is pressed
 ******************************************************************************/
function submitLinkOnEnter(formName, action, value, ev) {
	var keycode;
	if (window.event)
		keycode = window.event.keyCode;
	else if (ev)
		keycode = ev.which;
	else
		return true;

	if (keycode == 13) {
		submitLinkOnce(formName, action, value);
		return false;
	} else
		return true;
}
/*******************************************************************************
 * cross-window JavaScripting. Method to update data from the remote window
 * 
 * Uses the attribute listName, action, itemKey which is initilized in
 * javascript method openShoppingListWindow(actionValue, key,
 * jspName,windowName,x,y,h,w)
 * 
 ******************************************************************************/
function updateDataFromShoppingListWindow(formName) {

	if (document.forms[formName].IBS_LISTSELECTION_VALUE != null) {
		document.forms[formName].IBS_LISTSELECTION_VALUE.value = escape(listName);
	}

	if (top.window.shoppingListWindow != null) {
		top.window.shoppingListWindow.close();
	}
	submitLinkOnce(formName, action, itemKey);
}

/*******************************************************************************
 * Disable all buttons
 ******************************************************************************/
function disableAllButtons() {
	var totalFormsNumber = document.forms.length;
	if (totalFormsNumber > 0) {
		for ( var i = 0; i < totalFormsNumber; i++) {
			var form = document.forms[i];
			for ( var j = 0; j < form.elements.length; j++) {
				var formElement = form.elements[j];
				if (formElement.type == "submit"
						|| formElement.type == "button") {
					formElement.disabled = true;
				}
			}
		}
	}
}

/*******************************************************************************
 * Reset and reload form
 ******************************************************************************/
function resetForm(formName) {
	document.forms[formName].reset();
}

/*******************************************************************************
 * Disable actions in all frames
 ******************************************************************************/
function disableActions() {
	top.window.frameSetLoaded = false;
	if (navigator.userAgent.indexOf("Mac") < 0) {
		var no = top.frames.length;
		for ( var i = 0; i < no; i++) {
			if (top.frames[i].document.body != null) {
				top.frames[i].document.body.style.cursor = 'wait';
			}
		}
	}
}

/*******************************************************************************
 * Enable actions in all frames
 ******************************************************************************/
function enableActions() {
	if (navigator.userAgent.indexOf("Mac") < 0) {
		var no = top.frames.length;
		for ( var i = 0; i < no; i++) {
			if (top.frames[i].document.body != null) {
				top.frames[i].document.body.style.cursor = 'default';
			}
		}
	}
	top.window.frameSetLoaded = true;
}

/*******************************************************************************
 * Invoke link:
 * 
 * Invoke the link once when the current frame is loaded. If the link is clicked
 * while frameset not loaded yet or something else is blocking the actions (via
 * the 'blocked' variable) is the invokation ignored
 ******************************************************************************/
var loaded = false;
var blocked = false;
var activeHref = null;
var loops = 0;

function invokeLink(href) {
	if (!blocked && top.window.frameSetLoaded) {
		activeHref = href;
		blocked = true;
		invokeLinkWhenLoaded();
	}
}

function invokeLinkNoFrameSetCheck(href) {
	if (!blocked) {
		activeHref = href;
		blocked = true;
		invokeLinkWhenLoaded();
	}
}

function invokeLinkWhenLoaded() {	
	if (loaded) {
		if (navigator.userAgent.indexOf("Mac") < 0) {
			document.body.style.cursor = 'wait';
		}
		disableAllButtons();
		location.href = activeHref;
	} else {
		loops++;
		if (loops < 720) {			  
			setTimeout("invokeLinkWhenLoaded()", 250);
		} else {
			blocked = false;
			alert('Not ready loading page!');
		}
	}
}

/*******************************************************************************
 * Submit link:
 * 
 * Submit the link once when the frame set and current frame is loaded. If the
 * link is submitted while frameset not loaded yet or something else is blocking
 * the actions (via the 'blocked' variable) is the submit ignored
 * 
 * Note: include following input fields in jsp:
 * <INPUT NAME="IBS_LINKSELECTION_ACTION" TYPE="HIDDEN" VALUE="">
 * <INPUT NAME="IBS_LINKSELECTION_VALUE" TYPE="HIDDEN" VALUE="">
 ******************************************************************************/
var sbmAction = null;
var sbmValue = null;

function submitLinkOnce(formName, action, value) {
	if (!blocked && top.window.frameSetLoaded) {
		blocked = true;
		sbmAction = action;
		sbmValue = value;
		submitLinkWhenLoaded(formName);
	}
}

function submitLinkWhenLoaded(formName) {
	
		if (loaded) {
		if (navigator.userAgent.indexOf("Mac") < 0) {
			document.body.style.cursor = 'wait';
		}
		disableAllButtons();
		document.forms[formName].IBS_LINKSELECTION_ACTION.value = sbmValue;
		document.forms[formName].IBS_LINKSELECTION_ACTION.name = sbmAction;
		document.forms[formName].IBS_LINKSELECTION_VALUE.value = sbmValue;
		document.forms[formName].submit();
		if (navigator.userAgent.indexOf("MSIE") != -1
				&& navigator.userAgent.indexOf("Mac") < 0) {
			document.forms[formName].IBS_LINKSELECTION_VALUE.value = "";
		}
	} else {
		loops++;
		if (loops < 720) {
			// Code as in NetStore 7.0.0.6 	
			// setTimeout("submitLinkWhenLoaded()", 250);
			 
			// Fix to NST-66
			var func = function() {submitLinkWhenLoaded(formName);}; 
			   setTimeout(func, 250);
			
		} else {
			blocked = false;
			alert('Not ready loading page!');
		}
	}
}

/*******************************************************************************
 * Submit buttons:
 * 
 * Submit the specified button once when the current frame is loaded. If the
 * button is clicked while something else is blocking actions (via the 'blocked'
 * variable) is the submit ignored
 * 
 * Note: include following input fields in jsp: <INPUT NAME="IBS_SUBMIT_ACTION"
 * TYPE="HIDDEN" VALUE="">
 ******************************************************************************/
var sbmButton = "";

function submitButtonOnce(button) {
	if (!blocked && top.window.frameSetLoaded) {
		blocked = true;
		sbmButton = button.name;
		submitButtonWhenLoaded(button);
	}
}

function submitButtonOnceNoFrameSetCheck(button) {
	if (!blocked) {
		top.window.frameSetLoaded = false;
		blocked = true;
		sbmButton = button.name;
		submitButtonWhenLoaded(button);
	}
}

function submitButtonWhenLoaded(button) {
 
	if (loaded) {
		if (navigator.userAgent.indexOf("Mac") < 0) {
			document.body.style.cursor = 'wait';
		}
		disableAllButtons();
		button.form.IBS_SUBMIT_ACTION.name = sbmButton;
		button.form.submit();
	} else {
		loops++;
		if (loops < 720) {
			
			// Code as in NetStore 7.0.0.6 	
			// setTimeout("submitButtonWhenLoaded()", 250);
			 
			// Fix to NST-66				
			var func = function() {submitButtonWhenLoaded(button);}; 
			   setTimeout(func, 250);
			
		} else {
			blocked = false;
			alert('Not ready loading page!');
		}
	}
}

/*******************************************************************************
 * Submit the specfied button when the Enter key is pressed
 ******************************************************************************/
function submitenter(button, e) {
	var keycode;
	if (window.event)
		keycode = window.event.keyCode;
	else if (e)
		keycode = e.which;
	else
		return true;

	if (keycode == 13) {
		submitButtonOnce(button);
		return false;
	} else
		return true;
}
/*******************************************************************************
 * Submit resequence form * Note: include following input fields in jsp:
 * <INPUT NAME="IBS_RESEQUENCE_ACTION" TYPE="HIDDEN" VALUE="">
 * <INPUT NAME="IBS_RESEQUENCE_COLUMN" TYPE="HIDDEN" VALUE="">
 * <INPUT NAME="IBS_RESEQUENCE_SEQUENCE" TYPE="HIDDEN" VALUE="">
 ******************************************************************************/
var prevAction = "";

var sbmResAction = null;
var sbmResCol = null;
var sbmResSeq = null;

function submitResequenceFormWhenLoaded(formName) {	
	if (loaded) {
		if (navigator.userAgent.indexOf("Mac") < 0) {
			document.body.style.cursor = 'wait';
		}
		disableAllButtons();
		document.forms[formName].IBS_RESEQUENCE_ACTION.name = sbmResAction;
		document.forms[formName].IBS_RESEQUENCE_COLUMN.value = sbmResCol;
		document.forms[formName].IBS_RESEQUENCE_SEQUENCE.value = sbmResSeq;
		document.forms[formName].submit();
	} else {
		loops++;
		if (loops < 720) {
			
			// Code as in NetStore 7.0.0.6 	
			// setTimeout("submitResequenceFormWhenLoaded()", 250);
			 
			// Fix to NST-66			
			  var func = function() {submitResequenceFormWhenLoaded(formName);}; 
			   setTimeout(func, 250);
			
		} else {
			blocked = false;
			alert('Not ready loading page!');
		}
	}
}

function submitResequenceForm(formName, action, column, sequence) {
	if (!blocked && top.window.frameSetLoaded) {
		if (prevAction != action) {
			blocked = true;
			prevAction = action;
			sbmResAction = action;
			sbmResCol = column;
			sbmResSeq = sequence;
			submitResequenceFormWhenLoaded(formName);
		}
	}
}

function toggleResequenceImage(imageName, src) {
	if (document.images) {
		document[imageName].src = src;
	}
}

/*******************************************************************************
 * Frame specific scripts *
 ******************************************************************************/
function reloadShoppingCartWhenHomeFrameLoaded() {
	
	if (getFrame().loaded) {
		reloadShoppingCart();
	} else {
		count++;
		if (count < 720) {
			setTimeout("reloadShoppingCartWhenHomeFrameLoaded()", 250);
		} else {
			alert('Not ready loading home frame!');
		}
	}
}

function reloadShoppingCart() {
	if (top.bottom) {
		top.bottom.location.reload();
	}
}

// ---------------------------------------------------------------------------

function invokeDestinationFrame(href) {

	if (!getFrame().blocked && top.window.frameSetLoaded) {
		activeHref = href;
		getFrame().blocked = true;
		invokeDestinationFrameWhenLoaded();
	}
}

function invokeDestinationFrameWhenLoaded() {	
	if (loaded && getFrame().loaded) {
		getFrame().location.href = activeHref;
	} else {
		loops++;
		if (loops < 720) {
			setTimeout("invokeDestinationFrameWhenLoaded()", 250);
		} else {
			getFrame().blocked = false;
			alert('Not ready loading page!');
		}
	}
}

// ---------------------------------------------------------------------------
function submitDestinationLinkOnce(formName, action, value) {

	if (!getFrame().blocked && top.window.frameSetLoaded) {
		getFrame().blocked = true;
		sbmAction = action;
		sbmValue = value;
		submitDestinationLinkWhenLoaded(formName);
	}
}

function submitDestinationLinkWhenLoaded(formName) {

	if (loaded && getFrame().loaded) {
		disableAllButtons();
		document.forms[formName].IBS_LINKSELECTION_ACTION.name = sbmAction;
		document.forms[formName].IBS_LINKSELECTION_VALUE.value = sbmValue;
		document.forms[formName].submit();
		if (navigator.userAgent.indexOf("MSIE") != -1
				&& navigator.userAgent.indexOf("Mac") < 0) {
			document.forms[formName].IBS_LINKSELECTION_VALUE.value = "";
		}
	} else {
		loops++;
		if (loops < 720) {
			// Code as in NetStore 7.0.0.6 	
			// setTimeout("submitDestinationLinkWhenLoaded()", 250);
			 
			// Fix to NST-66			
			var func = function() {submitDestinationLinkWhenLoaded(formName);}; 
			   setTimeout(func, 250); 

			
		} else {
			getFrame().blocked = false;
			alert('Not ready loading page!');
		}
	}
}

/*******************************************************************************
 * Portal specific scripts *
 ******************************************************************************/
function addOnLoadEvent(func) {

	// Check for standard addEventListener
	if (typeof window.addEventListener != "undefined")
		window.addEventListener("load", func, false);

	// Check for MSIE-attachEvent
	else if (typeof window.attachEvent != "undefined") {
		window.attachEvent("onload", func);

		// Check for other
	} else {
		// If previous onload exists, execute it first
		if (window.onload != null) {
			var oldOnload = window.onload;
			window.onload = function(e) {
				oldOnload(e);
				window[func]();
			};
		} else
			window.onload = func;
	}
}

function getFrame() {
	if (eval(top.homemain) && eval(top.homemain.location))
		return top.homemain;
	else
		return top.window;
}

/*******************************************************************************
 *  NST-358 - Script for expand / collapse div to provide option to buy dependent (child) items from Product Information page *
 ******************************************************************************/
function expandBlock(collapsedBlock, expandedBlock, expand) {
	if (expand) {
		document.getElementById(collapsedBlock).style.display = 'none';
		document.getElementById(expandedBlock).style.display = 'block';
	}
	else {
		document.getElementById(collapsedBlock).style.display = 'block';
		document.getElementById(expandedBlock).style.display = 'none';
	}
}