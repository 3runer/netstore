/**
*	Javascript settings for Viewer
*
*	(LOADIMAGE)
*	Usage: loadImage( 	fileName, 				(Required)
*						imageText, 				(Required)
*						errorMessage, 			(Optional)
*						bigImageMessage, 		(Optional)
*						imageBackgroundColor, 	(Optional)
*						waitImageSrc, 			(Optional)
*						closeImageSrc )			(Optional)
*
*	Minimal: loadImage(fileName,imageText,'','','','','','')
*
*	Javascript parameter descriptions, settings and parameters for ImageViewer
*
*	fileName:				The image source
*	imageText:				The alt text when hover image
*	minimumWait:			Minimum time to load a picture (Default: 500 ms)
*	backgroundColor:		Background color before fade (Default: '#000000')
*	backgroundFade:			In procent, fadeamount (Default: 20)
*	imageBackgroundColor:	Color for the image background and border
*	screenWidthCor:			X correction parameter for fine tuning the image-box (Default: 20)
*	screenHeightCor:		Y correction parameter for fine tuning the image-box (Default: 80)
*	waitImageSrc:			Image to be diplayed when loading the product image (Default: '/NS/IBSStaticResources/NS_Resources/wait.gif')
*	closeImageSrc:			Image to be displaed f?r closing the image viewer (Default: '/NS/IBSStaticResources/NS_Resources/close.gif')
*	errorMessage:			Error message to be displayed when image not found 
*	bigImageMessage:		Warning message to be displayed when the product image is big
*	warnOnBigImages:		Warning if image is bigger than screen, (Default: false)
*
*
*	(MINIVIEWER)
*
*	Instructions:			This image miniviewer uses the IMAGES. For each IMAGE, one 
*							image in miniviewer. If the thumbnail does not excists, miniviewer shows a 
*							default image "Thumbnail missing!". For best performance use thumbnail-images with 
*							height: 80px and width: 0 - 130px and the big-images with height: (150 - 600) px 
*							and width (150 - 800) px. Starting miniviewer in two steps. First add the images 
*							to miniviewer, then start it. 
*							
*							(Step one)
*							<%
*								int i = 0; 
*								foreach (image)
*								{
*									out.println("addImage('image_src','thumbnail_src','image_title',is_default);");
*									i++;
*								}
*							%>
*							
*							(Step two)
*							initMiniViewer(container_id, ...);
*/

function Viewer() {
}
Viewer.backgroundColor = "#000000";
Viewer.backgroundFade = 20;
Viewer.imageBackgroundColor = "#FFFFFF";
Viewer.screenWidthCor = 20;
Viewer.screenHeightCor = 60;
Viewer.showFadeIn = true;

Viewer.waitimg = new Image();

Viewer.home = document.createElement("div");
Viewer.home.setAttribute("id", "home");
Viewer.home.style.filter = "alpha (opacity=" + Viewer.backgroundFade + ")";
Viewer.home.style.opacity = Viewer.backgroundFade / 100;
Viewer.home.style.position = "absolute";
Viewer.home.style.zIndex = "1";
Viewer.home.style.top = "0px";
Viewer.home.style.left = "0px";
Viewer.home.style.width = "100%";
Viewer.home.style.height = "100%";
Viewer.home.style.backgroundColor = Viewer.backgroundColor;

Viewer.w = screen.width;
Viewer.h = screen.height;
Viewer.pw, Viewer.ph, Viewer.t;
Viewer.prototype.error = false, Viewer.fadeInValue;

Viewer.big_image_div_container = document.createElement("div");
Viewer.big_image_div_container.setAttribute("id", "big_image_div_container");
Viewer.big_image_div_container.style.position = "absolute";
Viewer.big_image_div_container.style.zIndex = "2";
Viewer.big_image_div_container.style.top = "0px";
Viewer.big_image_div_container.style.width = "99%";
Viewer.big_image_div_container.style.height = "100%";

Viewer.big_div = document.createElement("div");
Viewer.big_div.setAttribute("id", "big_div");
Viewer.big_div.style.position = "absolute";
Viewer.big_div.style.top = "0px";
Viewer.big_div.style.zIndex = "2";
Viewer.big_div.style.top = "50%";
Viewer.big_div.style.left = "50%";
Viewer.big_div.style.padding = "15px";// Fix for NST-554

Viewer.image = new Image();
PopupMaxWidth=400;
Viewer.image.style.backgroundColor = "#FFFFFF";
Viewer.image.onerror = loadError;
Viewer.image.onAbort = loadError;


Viewer.closebtn = new Image();
Viewer.closebtn.style.position = "absolute";
Viewer.closebtn.style.bottom = 3 + "px";
Viewer.closebtn.style.right = 3 + "px";
Viewer.closebtn.style.marginRight = "0";// Fix for NST-554
Viewer.closebtn.style.marginBottom = "0";
Viewer.closebtn.onclick = hideImage;
Viewer.closebtn.style.cursor = "pointer";

Viewer.statusBar = document.createElement("div");
Viewer.statusBar.style.position = "absolute";
Viewer.statusBar.style.bottom = 20 + "px";
if (document.all) {
	Viewer.statusBar.style.bottom = 10 + "px";
}
Viewer.statusBar.style.left = "50%";
Viewer.statusBar.style.marginLeft = "-40px";
Viewer.statusBar.style.marginBottom = "20px";
Viewer.statusBar.style.border = "0px solid gray";
Viewer.statusBar.style.display = "inline";

Viewer.statusText = document.createElement("div");
Viewer.statusText.style.border = "0px solid gray";
Viewer.statusText.style.paddingLeft = "10px";
Viewer.statusText.style.paddingRight = "10px";
Viewer.statusText.style.textDecoration = "none";
Viewer.statusText.style.fontFamily = "Arial";
Viewer.statusText.style.fontSize = "12px";
if (document.all) {
	Viewer.statusText.style.display = "inline";
} else {
	Viewer.statusText.style.display = "-moz-inline-box";
	Viewer.statusText.style.display = "inline-block";
}

Viewer.left = new Image();
Viewer.left.onclick = getNextImage;
Viewer.left.style.cursor = "pointer";

Viewer.right = new Image();
Viewer.right.onclick = getPrevImage;
Viewer.right.style.cursor = "pointer";

window.onscroll = doScroll;
Viewer.prototype.loadImage = loadImage;
Viewer.prototype.showcontainer = showContainer;
Viewer.prototype.waitforloading = waitforloading;
Viewer.prototype.showImage = showImage;
Viewer.prototype.unfade_frames = unfade_frames;
Viewer.prototype.fade_frames = fade_frames;
Viewer.prototype.removeChildren = removeChildren;
Viewer.prototype.fadein = fadein;

function doScroll() {
	if (Viewer.big_div.hasChildNodes()) { 
		Viewer.home.style.left = document.body.scrollLeft + "px";
		Viewer.home.style.top = document.body.scrollTop + "px";
		//Fix for NST-554
		/*Viewer.big_div.style.marginTop = -((Viewer.ph + Viewer.screenHeightCor) / 2)
			+ document.body.scrollTop + "px";
		Viewer.big_div.style.marginLeft = -((Viewer.pw + Viewer.screenWidthCor) / 2)
			+ document.body.scrollLeft + "px";*/
	}
}

Viewer.missingImage = -1;

function waitforloading() {
	if (Viewer.error) {
		var current;
		if (MiniViewer.windowSize == 1 || Viewer.missingImage
				== (current = parseInt(MiniViewer.ImagesContainer.childNodes[1].id.substring(1)))) {
			Viewer.missingImage = -1;
			Viewer.waitimg.style.display="none";
			clearTimeout(Viewer.t);
			MiniViewer.message.innerHTML = Viewer.errorMessage;
			hideContainer();
		} else {
			MiniViewer.message.innerHTML = Viewer.errorMessage;
			if (Viewer.missingImage == -1) {
				Viewer.missingImage = current;
			}
			Viewer.showNext ? getNextImage() : getPrevImage();
		}
	} else if (Viewer.image.complete) {
		Viewer.missingImage = -1;
		Viewer.pw = Viewer.image.width;
		Viewer.ph = Viewer.image.height;
		showImage();
	}
	else
	{
		Viewer.t = setTimeout("waitforloading()",1);
	}
}

function getNextImage() {
	Viewer.showNext = true;
	hideImage();
	getNextThumbnail();
	var index = parseInt(MiniViewer.ImagesContainer.childNodes[1].id.substring(1));
	new Viewer().loadImage(MiniViewer.imageParameters[index][4],
			MiniViewer.ImagesContainer.childNodes[1].firstChild.title, "","", "", "",index);
}

function getPrevImage() {
	Viewer.showNext = false;
	hideImage();
	getPrevThumbnail();
	var index = parseInt(MiniViewer.ImagesContainer.childNodes[1].id.substring(1));
	new Viewer().loadImage(MiniViewer.imageParameters[index][4],
			MiniViewer.ImagesContainer.childNodes[1].firstChild.title, "","", "", "",index);
}

function showImage() {
	Viewer.big_div.removeChild(Viewer.waitimg);
	Viewer.waitimg.style.display = "inline";
	clearTimeout(Viewer.t);
	Viewer.big_div.appendChild(Viewer.closebtn);

	if (MiniViewer.imageParameters.length > 1) {
		Viewer.statusText.innerHTML = getStatus();
		Viewer.statusBar.appendChild(Viewer.left);
		Viewer.statusBar.appendChild(Viewer.statusText);
		Viewer.statusBar.appendChild(Viewer.right);
		Viewer.big_div.appendChild(Viewer.statusBar);
	}
	/*console.log(Viewer.image.style);
	Viewer.big_div.style.marginTop = -((Viewer.image.style.height) / 2)
			+ document.body.scrollTop + "px";
	alert(Viewer.big_div.style.marginTop);
	Viewer.big_div.style.marginLeft = -((Viewer.image.style.width) / 2)
			+ document.body.scrollLeft + "px";*/
	Viewer.big_div.style.backgroundColor = Viewer.imageBackgroundColor;
	/*Viewer.big_div.style.height = (Viewer.ph + 60) + "px";
	if (document.all) {
		Viewer.big_div.style.height = (Viewer.ph + 70) + "px";
	}*/

	Viewer.big_div.appendChild(Viewer.image);
	window.scrollBy(0, 0);
	if (Viewer.showFadeIn) {
		Viewer.fadeInValue = 10;
		Viewer.big_div.style.filter = "alpha (opacity=" + Viewer.fadeInValue + ")";
		Viewer.big_div.style.MozOpacity = Viewer.fadeInValue / 100;
		fadein();
	}
}

function fadein() {
	Viewer.fadeInValue = Viewer.fadeInValue + 10;

	Viewer.big_div.style.filter = "alpha (opacity=" + Viewer.fadeInValue + ")";
	Viewer.big_div.style.Opacity = Viewer.fadeInValue / 100;
	Viewer.big_div.style.MozOpacity = Viewer.fadeInValue / 100;

	if (Viewer.fadeInValue < 100) {
		setTimeout("fadein()", 5);
	}
}
function msieversion()// Fix for NST-554
{
   var ua = window.navigator.userAgent;
   var msie = ua.indexOf ( "MSIE " );

   if ( msie > 0 )      // If Internet Explorer, return version number
      return parseInt (ua.substring (msie+5, ua.indexOf (".", msie )));
   else{
		   return 0; // If another browser, return 0
   }

}


function loadImage(filename, imageText, errorMessage,imageBackgroundColor, waitImageSrc, closeImageSrc,index) {
	// Enhancement (NSD -34):Load the image only if current image is the center image in the thumbnail viewer/miniviewer
	var windowindex = 0;
	if (MiniViewer.windowSize > 1) {
		windowindex = 1;
	}
	// Fix (NST-30):Load the image only if current image is the center image in the thumbnail viewer/miniviewer
	if (index != "") {
		var testCurrent = parseInt(MiniViewer.ImagesContainer.childNodes[windowindex].id.substring(1));
		if(testCurrent!= index)
			return;
	}

	if (imageBackgroundColor != "")
		Viewer.imageBackgroundColor = imageBackgroundColor;

	if (waitImageSrc != "")
		Viewer.waitImageSrc = waitImageSrc;

	//Efix701_22 - NST-47 initialize close if not initialized before
	if (closeImageSrc != ""){
		Viewer.closeImageSrc = closeImageSrc;
		Viewer.closebtn.src = Viewer.closeImageSrc;
	}
	//NST-47 end
	
	showContainer();

	Viewer.big_div.appendChild(Viewer.waitimg);
	Viewer.error = false;
	Viewer.waitimg.src = Viewer.waitImageSrc;
	Viewer.pw = Viewer.waitimg.width;
	Viewer.ph = Viewer.waitimg.height;
	Viewer.image.setAttribute("src", filename);
	Viewer.image.setAttribute("alt", imageText);
	Viewer.image.setAttribute("title", imageText);
	
	// Fix for NST-554
	Viewer.image.onload = function(e){
		var maxWidth,averWidth, averHeight;
		if ( msieversion() > 0 ){
			Viewer.image.style.width ="expression(this.width < 400 ? 400: true);";
		}
		if(PopupMaxWidth < this.width){
			averWidth=(PopupMaxWidth/this.width)*100;
			averHeight=(averWidth/100)*this.height;
			Viewer.image.style.maxHeight=averHeight+"px";
			Viewer.image.style.maxWidth=PopupMaxWidth+"px";
			Viewer.big_div.style.marginLeft = -((PopupMaxWidth) / 2)+ "px";
			Viewer.big_div.style.marginTop = -((averHeight) / 2)+ "px";
		}
		else{
			Viewer.big_div.style.marginLeft = -((this.width) / 2)+ "px";
			Viewer.big_div.style.marginTop = -((this.height) / 2)+ "px";
		}
		//Viewer.big_div.style.marginTop = (((this.height>this.maxHeight)) / 2)+ "px";
		//Viewer.big_div.style.marginLeft = ((this.width) / 2)+ "px";
	}
	//alert( Viewer.screenHeightCor+" "+document.body.scrollTop);
	/*Viewer.big_div.style.marginTop = -((20 + Viewer.screenHeightCor) / 2)
			+ document.body.scrollTop + "px";
	Viewer.big_div.style.marginLeft = -((100 + Viewer.screenWidthCor) / 2)
			+ document.body.scrollLeft + "px";*/
	Viewer.big_div.style.backgroundColor = "";
	Viewer.big_div.style.height = "";
	Viewer.big_div.style.width = "";

	Viewer.t = setTimeout("waitforloading()", 100);
}

function hideContainer() {
	Viewer.waitimg.style.display = "inline";
	removeChildren(Viewer.big_image_div_container);
	document.body.removeChild(Viewer.big_image_div_container);
	unfade_frames();
}

function showContainer() {
	Viewer.big_image_div_container.appendChild(Viewer.big_div);
	document.body.appendChild(Viewer.big_image_div_container);
	fade_frames();
}

function hideImage() {
	removeChildren(Viewer.big_div);
	hideContainer();
}

function loadError() {
	Viewer.error = true;
}

function fade_frames() {
	document.body.appendChild(Viewer.home);
	Viewer.home.style.left = document.body.scrollLeft + "px";
	Viewer.home.style.top = document.body.scrollTop + "px";
}
function unfade_frames() {
	document.body.removeChild(Viewer.home);
}

function removeChildren(node) {
	if (node) {
		while (node.hasChildNodes()) {
			removeChildren(node.firstChild);
			node.removeChild(node.firstChild);
		}
	}
}

/* MiniViewer */

function MiniViewer() {
}

MiniViewer.imageShowing = 0;
MiniViewer.windowSize = 3;
MiniViewer.fadeInValue = 10;
MiniViewer.smalThumbSize = 50;
MiniViewer.bigThumbSize = 80;

MiniViewer.imageParameters = new Array();
MiniViewer.miniImages = new Array();
MiniViewer.t = null;

MiniViewer.ImagesContainer = document.createElement("div");
MiniViewer.ImagesContainer.setAttribute("id", "ImagesContainer");
MiniViewer.ImagesContainer.style.border = "0px solid gray";
MiniViewer.ImagesContainer.align = "center";
MiniViewer.ImagesContainer.style.padding = "2px";

MiniViewer.Container = document.createElement("div");
MiniViewer.Container.setAttribute("id", "ImagesContainer");
MiniViewer.Container.style.border = "0px solid red";
MiniViewer.Container.align = "center";

MiniViewer.statusBar = document.createElement("div");
MiniViewer.statusBar.style.border = "0px solid gray";
MiniViewer.statusBar.style.display = "inline";

MiniViewer.statusText = document.createElement("div");
MiniViewer.statusText.style.border = "0px solid gray";
MiniViewer.statusText.style.paddingLeft = "10px";
MiniViewer.statusText.style.paddingRight = "10px";
MiniViewer.statusText.style.textDecoration = "none";
MiniViewer.statusText.style.fontFamily = "Arial";
MiniViewer.statusText.style.fontSize = "12px";
if (document.all) {
	MiniViewer.statusText.style.display = "inline";
} else {
	MiniViewer.statusText.style.display = "-moz-inline-box";
	MiniViewer.statusText.style.display = "inline-block";
}

MiniViewer.message = document.createElement("div");
MiniViewer.message.style.fontFamily = "Arial";
MiniViewer.message.style.fontSize = "10px";
MiniViewer.message.style.color = "#FF0000";
MiniViewer.message.style.height = "20px";
MiniViewer.message.innerHTML = "";

MiniViewer.left = new Image();
MiniViewer.left.onclick = getNextThumbnail;
MiniViewer.left.style.cursor = "pointer";

MiniViewer.right = new Image();
MiniViewer.right.onclick = getPrevThumbnail;
MiniViewer.right.style.cursor = "pointer";

function getPrevThumbnail() {
	if (Viewer.missingImage < 0)
		MiniViewer.message.innerHTML = "";
	MiniViewer.ImagesContainer
			.appendChild(MiniViewer.ImagesContainer.firstChild);
	startMiniViewer(MiniViewer.imageShowing);
}

function getNextThumbnail() {
	if (Viewer.missingImage < 0)
		MiniViewer.message.innerHTML = "";
	MiniViewer.ImagesContainer.insertBefore(
			MiniViewer.ImagesContainer.lastChild,
			MiniViewer.ImagesContainer.firstChild);
	startMiniViewer(MiniViewer.imageShowing);
}

function populateMiniImage() {
	if (MiniViewer.windowSize > MiniViewer.imageParameters.length) {
		MiniViewer.windowSize = MiniViewer.imageParameters.length;
	}

	for (var i = 0, length = MiniViewer.imageParameters.length; i < length; ++i) {
		if (MiniViewer.imageParameters[i][0] == true) {
			MiniViewer.imageShowing = i;
		}
	}

	for (var i = MiniViewer.imageShowing + MiniViewer.imageParameters.length; i < MiniViewer.imageShowing
			+ MiniViewer.imageParameters.length * 2; i++) {
		MiniViewer.ImagesContainer.appendChild(MiniViewer.imageParameters[(i)
				% MiniViewer.imageParameters.length][1]);
	}

	MiniViewer.ImagesContainer.insertBefore(
			MiniViewer.ImagesContainer.lastChild,
			MiniViewer.ImagesContainer.firstChild);
	startMiniViewer(MiniViewer.imageShowing);
}

function hideImages() {
	for ( var i = 0; i < MiniViewer.imageParameters.length; i++) {
		if (i >= MiniViewer.windowSize) {
			MiniViewer.ImagesContainer.childNodes[i].style.display = "none";
		}
		if (MiniViewer.windowSize != 1) {
			MiniViewer.ImagesContainer.childNodes[i].firstChild.height = MiniViewer.smalThumbSize;
			MiniViewer.ImagesContainer.childNodes[i].firstChild.width = getImageXSize(
					MiniViewer.imageParameters[parseInt(MiniViewer.ImagesContainer.childNodes[i].id
							.substring(1))][2],
					MiniViewer.imageParameters[parseInt(MiniViewer.ImagesContainer.childNodes[i].id
							.substring(1))][3], MiniViewer.smalThumbSize);
			MiniViewer.ImagesContainer.childNodes[i].firstChild.style.marginTop = (MiniViewer.bigThumbSize - MiniViewer.smalThumbSize)
					/ 2 + "px";
		} else {
			MiniViewer.ImagesContainer.childNodes[i].firstChild.height = MiniViewer.bigThumbSize;
			MiniViewer.ImagesContainer.childNodes[i].firstChild.width = getImageXSize(
					MiniViewer.imageParameters[i][2],
					MiniViewer.imageParameters[i][3], MiniViewer.bigThumbSize);
		}

		MiniViewer.ImagesContainer.childNodes[i].firstChild.style.cursor = "default";
	}

	for ( var i = 0; i < MiniViewer.windowSize; i++) {
		if (document.all) {
			MiniViewer.ImagesContainer.childNodes[i].style.display = "inline";
		} else {
			MiniViewer.ImagesContainer.childNodes[i].style.display = "-moz-inline-box ";
			MiniViewer.ImagesContainer.childNodes[i].style.display = "inline-block";
		}

		MiniViewer.ImagesContainer.childNodes[i].style.filter = "alpha (opacity=" + 20 + ")";
		MiniViewer.ImagesContainer.childNodes[i].style.opacity = 20 / 100;
		MiniViewer.ImagesContainer.childNodes[i].style.width = "80px";
	}
}

function startMiniViewer(start) {
	hideImages();

	MiniViewer.statusText.innerHTML = getStatus();
	if (MiniViewer.imageParameters.length > 1) {
		MiniViewer.statusBar.appendChild(MiniViewer.left);
		MiniViewer.statusBar.appendChild(MiniViewer.statusText);
		MiniViewer.statusBar.appendChild(MiniViewer.right);
	} else {
		MiniViewer.statusBar.appendChild(MiniViewer.statusText);
	}
	MiniViewer.Container.appendChild(MiniViewer.statusBar);
	MiniViewer.Container.appendChild(MiniViewer.message);

	MiniViewer.fadeInValue = 10;
	fadeImage();
	var index = 0;
	if (MiniViewer.windowSize > 1) {
		index = 1;
	}
	MiniViewer.ImagesContainer.childNodes[index].firstChild.height = MiniViewer.bigThumbSize;
	MiniViewer.ImagesContainer.childNodes[index].firstChild.width = getImageXSize(
			MiniViewer.imageParameters[parseInt(MiniViewer.ImagesContainer.childNodes[index].id
					.substring(1))][2],
			MiniViewer.imageParameters[parseInt(MiniViewer.ImagesContainer.childNodes[index].id
					.substring(1))][3], MiniViewer.bigThumbSize);
	MiniViewer.ImagesContainer.childNodes[index].firstChild.style.marginTop = 0;
	MiniViewer.ImagesContainer.childNodes[index].style.width = "130px";
	MiniViewer.ImagesContainer.childNodes[index].firstChild.style.cursor = "pointer";
}

function fadeImage() {
	MiniViewer.fadeInValue = MiniViewer.fadeInValue + 10;
	var index = 0;
	if (MiniViewer.windowSize > 1) {
		index = 1;
	}
	MiniViewer.ImagesContainer.childNodes[index].style.filter = "alpha (opacity=" + MiniViewer.fadeInValue + ")";
	MiniViewer.ImagesContainer.childNodes[index].style.Opacity = MiniViewer.fadeInValue / 100;
	MiniViewer.ImagesContainer.childNodes[index].style.MozOpacity = MiniViewer.fadeInValue / 100;
	if (MiniViewer.fadeInValue < 100) {
		setTimeout("fadeImage()",1);
	}
}

function createImageContainer(id, im) {
	imageContainer = document.createElement("div");
	imageContainer.setAttribute("id", "i" + id);
	imageContainer.style.border = "0px solid black";
	imageContainer.align = "center";
	imageContainer.appendChild(im);
	return imageContainer;
}

function checkLoadingImages() {
	allImagesComplete = false;
	for (i = 0; i <MiniViewer.imageParameters.length; i++)
	{
		if (MiniViewer.imageParameters[i][1].firstChild.complete == false) {
			if(MiniViewer.t != null)
				clearTimeout(MiniViewer.t);
			
			MiniViewer.t = setTimeout("checkLoadingImages()",2000);
			break;
		}
		else
		{
			MiniViewer.imageParameters[i][3] = MiniViewer.imageParameters[i][1].firstChild.width;
			MiniViewer.imageParameters[i][2] = MiniViewer.imageParameters[i][1].firstChild.height;	
		}
		if(i == MiniViewer.imageParameters.length -1) //all thumbnails loaded
			allImagesComplete = true;
		
	}	
	if(allImagesComplete)
	{
		MiniViewer.Container.appendChild(MiniViewer.ImagesContainer);
		MiniViewer.e.appendChild(MiniViewer.Container);
		populateMiniImage();
	}
	return;
}

function getImageXSize(x, y, height) {
	var ratio = y / x;
	return ratio * height;
}

function getStatus() {
	var index = 0;
	if (MiniViewer.windowSize > 1) {
		index = 1;
	}
	var current =
		parseInt(MiniViewer.ImagesContainer.childNodes[index].id.substring(1));
	return (current + 1) + " / " + MiniViewer.imageParameters.length;
}

function addImage(image, thumbnail, title, isDefault) {
	object = new Image();
	object.src = thumbnail;	
	object.title = title;
	var index = MiniViewer.imageParameters.length;
	object.onmouseover = new Function("fadeOver(" + index + ",true)");
	object.onmouseout = new Function("fadeOver(" + index + ",false)");
	object.onclick = new Function("new Viewer().loadImage('" + image + "','" + title
			+ "','','','','','"+index+"')");
	MiniViewer.imageParameters[index] = new Array(isDefault,
			createImageContainer(index, object), object.width, object.height, image);
}

function fadeOver(nr, loc) {
	if (MiniViewer.imageParameters.length > 1) {
		if (nr != parseInt(MiniViewer.ImagesContainer.childNodes[1].id
				.substring(1))) {
			if (loc == true) {
				MiniViewer.imageParameters[nr][1].style.MozOpacity = .5;
				MiniViewer.imageParameters[nr][1].style.Opacity = .5;
			} else {
				MiniViewer.imageParameters[nr][1].style.MozOpacity = .2;
				MiniViewer.imageParameters[nr][1].style.Opacity = .2;
			}
		}
	}
}

function initMiniViewer(id_s, props) {
	MiniViewer.left.src = props.imageLeft;
	MiniViewer.right.src = props.imageRight;
	MiniViewer.e = document.getElementById(id_s);
	if (MiniViewer.imageParameters.length > 0) 
		checkLoadingImages();

	Viewer.left.src = props.imageLeft;
	Viewer.right.src = props.imageRight;
	
	Viewer.closebtn.src = props.imageClose;
	Viewer.waitImageSrc = props.imageWait;
	Viewer.errorMessage = props.errorMessage;
}
