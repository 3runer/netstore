/*
 *  Common javascripts used in NetStore Administration application  
 */
var loaded = false;
var blocked = false;
var activeHref = null;
var loops = 0;

/*******************************************************************************
 * Message alert
 * 
 * @param headTxt
 *            the header text
 * @param msg
 *            the message to display
 ******************************************************************************/
function message(headTxt, msg) {
	alert(headTxt + "\n\n" + msg);
}

/*******************************************************************************
 * Confirm sign off
 * 
 * @param headTxt
 *            the header text
 ******************************************************************************/
function confirmSignoff(headtxt, href) {
	if (confirm(headtxt + "!")) {
		invokeLink(href);
	}
}

/*******************************************************************************
 * Confirmation prompt if administrator really want to close the pool
 ******************************************************************************/
function confirmClosePool(formName, text, href) {
	var poolName = document.forms[formName].QRY_POOLNAME_CLOSE.value;
	if (confirm(text + ' ' + poolName)) {
		submitLinkOnce(formName, 'ACTION_CLOSE_POOL', href);
	}
}

/*******************************************************************************
 * 
 * Submit button confirmation prompt
 * 
 ******************************************************************************/
function submitButtonConfirmation(text, href, button) {
	if (confirm(text)) {
		submitButtonOnce(button);
	}
}
/*******************************************************************************
 * 
 * Select all checkboxes on a page.
 * 
 * @param selectAllCheckbox
 *            the checkbox invoking this function
 * 
 ******************************************************************************/
function selectAllCheckboxes(formName, selectAllCheckbox) {
	len = document.forms[formName].elements.length;
	var i = 0;
	while (i != len) {
		if (document.forms[formName].elements[i].type == 'checkbox'
				&& document.forms[formName].elements[i].name != eval("selectAllCheckbox.name"))
			document.forms[formName].elements[i].checked = selectAllCheckbox.checked;
		i++;
	}
}
/*******************************************************************************
 * 
 * Set checkboxes to selected or deselected
 * 
 * @param checked
 *            Set checkboxes to selected or deselected (true or false)
 * @param name
 *            The name of the check boxes to select or deselect
 * 
 ******************************************************************************/
function toggleCheckboxes(formName, checked, name) {
	len = document.forms[formName].elements.length;
	var i = 0;
	while (i != len) {
		if (document.forms[formName].elements[i].type == 'checkbox'
				&& document.forms[formName].elements[i].name == eval("name"))
			document.forms[formName].elements[i].checked = checked;
		i++;
	}
}

/*******************************************************************************
 * Invoke link:
 * 
 * Invoke the specified link once when the current page is loaded. If the link
 * is clicked while something else is blocking actions (via the 'blocked'
 * variable) is the invokation ignored
 ******************************************************************************/
function invokeLink(href) {
	if (!blocked) {
		activeHref = href;
		blocked = true;
		invokeLinkWhenLoaded();
	}
}

function invokeLinkWhenLoaded() {	
	if (loaded) {
		disableAllButtons();
		location.href = activeHref;
	} else {
		loops++;
		if (loops < 720) {
			setTimeout("invokeLinkWhenLoaded()", 250);
		} else {
			blocked = false;
		}
	}
}

/*******************************************************************************
 * Submit link:
 * 
 * Submit the specified link once when the current page is loaded. If the link
 * is submitted while something else is blocking actions (via the 'blocked'
 * variable) is the submit ignored
 * 
 * Note: include following input fields in jsp: <INPUT
 * NAME="IBS_LINKSELECTION_ACTION" TYPE="HIDDEN" VALUE=""> <INPUT
 * NAME="IBS_LINKSELECTION_VALUE" TYPE="HIDDEN" VALUE="">
 ******************************************************************************/
var sbmAction = null;
var sbmValue = null;

function submitLinkOnce(formName, action, value) {
	if (!blocked) {
		blocked = true;
		sbmAction = action;
		sbmValue = value;
		submitLinkWhenLoaded(formName);
	}
}

function submitLinkWhenLoaded(formName) {

	if (loaded) {
		disableAllButtons();
		document.forms[formName].IBS_LINKSELECTION_ACTION.name = sbmAction;
		document.forms[formName].IBS_LINKSELECTION_VALUE.value = sbmValue;
		document.forms[formName].submit();
		if (navigator.userAgent.indexOf("MSIE") != -1) {
			document.forms[formName].IBS_LINKSELECTION_VALUE.value = "";
		}
	} else {
		loops++;
		if (loops < 720) {
			
			// Code as in NetStore 7.0.0.6 	
			// setTimeout("submitLinkWhenLoaded()", 250);
			 
			// Fix to NST-66	
			var func = function() {submitLinkWhenLoaded(formName);}; 
			   setTimeout(func, 250);
			
		} else {
			blocked = false;
		}
	}
}

/*******************************************************************************
 * Submit buttons:
 * 
 * Submit the specified button once when the current page is loaded. If the
 * button is clicked while something else is blocking actions (via the 'blocked'
 * variable) is the submit ignored
 * 
 * Note: include following input fields in jsp: <INPUT NAME="IBS_SUBMIT_ACTION"
 * TYPE="HIDDEN" VALUE="">
 ******************************************************************************/
var sbmButton = "";

function submitButtonOnce(button) {
	if (!blocked) {
		blocked = true;
		sbmButton = button.name;
		submitButtonWhenLoaded(button);
	}
}

function submitButtonWhenLoaded(button) {
	if (loaded) {
		disableAllButtons();
		button.form.IBS_SUBMIT_ACTION.name = sbmButton;
		button.form.submit();
	} else {
		loops++;
		if (loops < 720) {
			// Code as in NetStore 7.0.0.6 	
			// setTimeout("submitButtonWhenLoaded()", 250);
			 
			// Fix to NST-66
			var func = function() {submitButtonWhenLoaded(button);}; 
			 setTimeout(func, 250);
			
		} else {
			blocked = false;
		}
	}
}

function disableAllButtons() {
	var totalFormsNumber = document.forms.length;
	if (totalFormsNumber > 0) {
		for ( var i = 0; i < totalFormsNumber; i++) {
			var form = document.forms[i];
			for ( var j = 0; j < form.elements.length; j++) {
				var formElement = form.elements[j];
				if (formElement.type == "submit"
						|| formElement.type == "button") {
					formElement.disabled = true;
				}
			}
		}
	}
}

/*******************************************************************************
 * Portal specific scripts *
 ******************************************************************************/
function addOnLoadEvent(func) {

	// Check for standard addEventListener
	if (typeof window.addEventListener != "undefined")
		window.addEventListener("load", func, false);

	// Check for MSIE-attachEvent
	else if (typeof window.attachEvent != "undefined") {
		window.attachEvent("onload", func);

		// Check for other
	} else {
		// If previous onload exists, execute it first
		if (window.onload != null) {
			var oldOnload = window.onload;
			window.onload = function(e) {
				oldOnload(e);
				window[func]();
			};
		} else
			window.onload = func;
	}
}

function getFrame() {
	if (eval(top.homemain) && eval(top.homemain.location))
		return top.homemain;
	else
		return top.window;
}