@echo off
rem ---------------------------------------------------------------------------
rem Place this file in the \bin folder of the Liferay installation
rem ---------------------------------------------------------------------------

rem ---------------------------------------------------------------------------
rem Set the path to the IBS NetStore XT installation
rem ---------------------------------------------------------------------------
set JAVA_OPTS=-Dxt.bin="C:\NetStoreXT\bin"

rem ---------------------------------------------------------------------------
rem Uncomment to enable remote debugging
rem ---------------------------------------------------------------------------
rem set enable_remote_debug=enabled

rem ---------------------------------------------------------------------------
rem Start Liferay and NetStore
rem ---------------------------------------------------------------------------
if defined enable_remote_debug goto start_remote_debug

call catalina start
goto end

:start_remote_debug
set JPDA_ADDRESS=8000
set JPDA_TRANSPORT=dt_socket
call catalina jpda start
goto end

:end
