<%-- Show 'session in use' page.
NOTE: This jsp is assumed to exist in the web application root. --%>
<html>

<%-- Imports --%>
<%@ page session="false" import="se.ibs.ccf.*, se.ibs.ns.cf.*" %>

<%-- Declarations --%>
<%
se.ibs.ccf.Request wrappedRequest = new se.ibs.ccf.Request(request);
String lc = NSRequestHelper.getUserLanguageCode(wrappedRequest);
String css = NSObject.getStyleSheet(request.getHeader("User-Agent").indexOf("MSIE") < 0);
String appl = (String)request.getAttribute("SessionInUseAppl");
String title = NSObject.translate("CON_SESSION_IN_USE",lc,"Session in use");
String themePath  = NSObject.getThemeResourcePath();
String imageExt = CCFObject.getImageExtension(request.getHeader("User-Agent"));
%>
 
<head>
<title><%= title %></title>
<link rel="STYLESHEET" href="<%= css %>"  type="text/css">
</head>

<body class="IBSBody">
<%-- FuncPageBegin --%>
<div align="left">
<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
	<tr>
		<td height="20" valign="top" colspan="2"></td>
	</tr>
	<tr>
		<td width="20" align="left"><img border="0" src="<%= themePath %>BackgroundBody<%= imageExt %>" width="20" height="1"></td>
		<td align="left" >
			<table border="0" cellspacing="0" cellpadding="0" height="97%" width="97%">
			<tr>	
				<td width="9" height="9"><img border="0" src="<%= themePath %>MainTopLeft<%= imageExt %>" width="9" height="9"></td>
		<%	if (((String) request.getHeader ("User-Agent")).indexOf ("MSIE") < 0) { %>
				<td class="IBSBackgroundDummyCell">
						<img border="0" src="<%= themePath %>1pxDummy<%= imageExt %>" width="1" height="1"></td>
		<%	} else { %>
				<td class="IBSBackgroundDummyCell" width="100%"><img border="0" src="<%= themePath %>1pxDummy<%= imageExt %>" width="1" height="1"></td>
		<%	} %>
				<td width="9" height="9"><img border="0" src="<%= themePath %>MainTopRight<%= imageExt %>" width="9" height="9"></td>
			</tr>		
			<tr>
				<td class="IBSBackgroundCell" colspan="3">	
				<table cellpadding="0" cellspacing="0" width="97%" height="97%">
				<tr>
					<td valign="top"><img border="0" src="<%= themePath %>1pxDummy<%= imageExt %>" width="20" height="1"></td>
					<td valign="top" width="100%">
<%-- end --%>
      			<table border="0" width="100%">
					<tr>
						<td class="IBSPageTitleText" valign="bottom" nowrap>
            				<font class="IBSPageTitleHeader"><%= title %></font>
            			</td>
          			</tr>
          			<tr>
          				<td class="IBSPageTitleDivider" height="1"></td>
  					</tr>
					<tr>
						<td class="IBSEmpty" height="100px">&nbsp;</td>
   					</tr>	
						
			<%	if (appl != null && appl.equalsIgnoreCase("*CURRENT")) { %>
					<tr>
						<td class="IBSTextWarning" align="center">
								<%= NSObject.translate("TXT_CF_024",lc,"You are already using the session, start a new browser and try again.") %>
						</td>
					</tr>
   
			<%	} else { %>
	
					<tr>
						<td class="IBSTextWarning" align="center">
							<%= NSObject.translate("TXT_CF_021",lc,"The session is already in use by application") %>&nbsp;<%= appl %>
						</td>
					</tr>
					<tr>
						<td class="IBSTextWarning" align="center">	
						<%	if (request.isRequestedSessionIdFromURL()) { %>
								<%= NSObject.translate("TXT_CF_022",lc,"Start a new browser and try again.") %>
						<%	} else { %>
								<%= NSObject.translate("TXT_CF_023",lc,"Start a new browser and try again or change your server configuration to use URL-rewriting.") %>
						<%	} %>
						</td>
					</tr>
	
			<%	} %>
   			</table>
<%-- FuncPageEnd --%>
    				</td>
    			</tr>
	    		</table>
    			</td>
			</tr>
			</table>
		</td>
	 </tr>
</table>
</div>
<%-- end --%>
</body>
</html>