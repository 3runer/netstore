
var cbpHorizontalMenu = (function() {

	var $listItems = $( '#cbp-hrmenu > ul > li' ),
		$menuItems = $listItems.children( 'a' ),
		$body = $( 'body' ),
		current = -1;

	function init() {
		var $listItems = $( '#cbp-hrmenu > ul > li' ),
		$menuItems = $listItems.children( 'a' );
		$($menuItems).off().on( 'click', open );
		$listItems.off().on( 'click', function( event ) { event.stopPropagation(); } );
	}

	function open( event ) {
		var $listItems1 = $( '#cbp-hrmenu > ul > li' );

		if( current != -1 ) {
			$listItems1.eq( current ).removeClass( 'cbp-hropen' );
		}

		var $item = $( event.currentTarget ).parent( 'li' ),
			idx = $item.index();
			idx = idx-1;

		if( current === idx ) {
			$item.removeClass( 'cbp-hropen' );
			current = -1;
		}
		else {
			$item.addClass( 'cbp-hropen' );
			current = idx;
			$body.off( 'click' ).on( 'click', close );
		}

		return false;

	}

	function close( event ) {
		var $listItems2 = $( '#cbp-hrmenu > ul > li' );
		$listItems2.eq( current ).removeClass( 'cbp-hropen' );
		current = -1;
	}

	return { init : init,
		close:close
		
	};

})();

