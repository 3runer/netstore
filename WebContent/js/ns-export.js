function Table () {
	    this.headers = [];
	    this.rows = [];
	    this.entityName="ROW";
	    this.rootElementName='TABLE';
	    this.heading = null;
	}
	 
	Table.prototype.setEntityName = function(entityName) {
		if (!isNull(entityName)){
			 this.entityName = entityName;
		}
	};
	
	Table.prototype.setRootElementName = function(rootElementName) {
		if (!isNull(rootElementName)){
			 this.rootElementName = rootElementName;
		}
	};
	
	Table.prototype.setHeaders = function(headerArray) {
		if (!isNull(headerArray) && Array.isArray(headerArray)){
			 this.headers = headerArray;
		}
	};

	Table.prototype.addHeader = function(headerName) {
		if (!isNull(headerName)){
		    this.headers.push(headerName);
		}
	};

	Table.prototype.addRow = function(colArray) {
		if (!isNull(colArray) && Array.isArray(colArray)){
			for(var i=0;i<colArray.length;i++){
				if(isNull(colArray[i])){
					colArray[i]=" ";//insert blank string to error in jspdf
				}
			}
			this.rows.push(colArray);
		}
	};

	Table.prototype.getRows = function() {
	    return this.rows;
	};

	Table.prototype.getHeaders = function() {
	    return this.headers;
	};

	Table.prototype.getData = function() {
		var data = [];
	//	data.push(this.addDataWithPropName(this.getHeaders()));
		
		//add row data
		var rows= this.getRows();
		for(var i=0;i<rows.length ;i++){
			data.push(this.addDataWithPropName(rows[i]));
		}
	    return data;
	};

	Table.prototype.getDataWithHeaders = function() {
		var data = [];
		data.push(this.getHeaders());
		
		//add row data
		var rows= this.getRows();
		for(var i=0;i<rows.length ;i++){
			data.push(rows[i]);
		}
	    return data;
	};

	Table.prototype.addDataWithPropName = function(colArray) {
		var rowObj = {};
		var internalColName= "COL_";
		for(var i=0;i<colArray.length ;i++){
			rowObj[this.headers[i]] = colArray[i];
		}
		return rowObj;
	};

	

	Table.prototype.getHTML = function() {
	   var str = "<TABLE  border='1' >" ;
	   var th = this.headers;
	   //make headers
	   str = str +  "<TR> " ;
	   for(var i=0; i<th.length ; i++){
		   str = str + " <TD nowrap> " + th[i] + "  </b> </font> </TD> " ;
	   }
	   str = str +  " </TR> " ;
	   
	   //make table rows
	   var rows = this.rows;
	   for(var i=0; i<rows.length ; i++){
		   var row = rows[i];
		   
		   str = str + " <TR> " ;
		   for(var j=0; j<row.length ; j++){
			   str = str + " <TD> " + row[j] + " </TD> " ;
		   }
		   str = str +  " </TR> " ; 
	   }
	   str = str +  " </TABLE> " ; 
	   return str;
	};
	
	Table.prototype.getXMLStartTag = function(str){
		return " <"+ str  + "> " ;
	};
	
	Table.prototype.getXMLEndTag = function(str){
		return " </"+ str  + "> " ;
	};
	
	Table.prototype.getXML = function() {
		var  NEW_LINE = '\r\n';
		var str = "<?xml version='1.0' encoding='UTF-8'?>" + NEW_LINE ;

		//make root
		str = str + this.getXMLStartTag(this.rootElementName) + NEW_LINE ;

		//make table rows
		var rows = this.rows;
		for(var i=0; i<rows.length ; i++){
			var row = rows[i];

			str = str + this.getXMLStartTag(this.entityName) + NEW_LINE ;
			for(var j=0; j<row.length ; j++){
				str = str + this.getXMLStartTag(this.headers[j]) + row[j] +  this.getXMLEndTag(this.headers[j]) + NEW_LINE;
			}
			str = str + this.getXMLEndTag(this.entityName) + NEW_LINE ;
		}
		str = str + this.getXMLEndTag(this.rootElementName) + NEW_LINE ;
		return str;
	};

	Table.prototype.getExcelData = function(){
		var excel = this.getHTML();
		var excelFile = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel'" +
				" xmlns='http://www.w3.org/TR/REC-html40'>";
		excelFile += "<head>";
		excelFile += "<!--[if gte mso 9]>";
		excelFile += "<xml>";
		excelFile += "<x:ExcelWorkbook>";
		excelFile += "<x:ExcelWorksheets>";
		excelFile += "<x:ExcelWorksheet>";
		excelFile += "<x:Name>";
		excelFile += "{worksheet}";
		excelFile += "</x:Name>";
		excelFile += "<x:WorksheetOptions>";
		excelFile += "<x:DisplayGridlines/>";
		excelFile += "</x:WorksheetOptions>";
		excelFile += "</x:ExcelWorksheet>";
		excelFile += "</x:ExcelWorksheets>";
		excelFile += "</x:ExcelWorkbook>";
		excelFile += "</xml>";
		excelFile += "<![endif]-->";
		excelFile += "</head>";
		excelFile += "<body>";
		excelFile += excel;
		excelFile += "</body>";
		excelFile += "</html>";
		
		return excelFile;
	};
function datenum(v, date1904) {
		if(date1904) v+=1462;
		var epoch = Date.parse(v);
		return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
	}
	 
	function sheet_from_array_of_arrays(data, opts) {
		var ws = {};
		var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
		for(var R = 0; R != data.length; ++R) {
			for(var C = 0; C != data[R].length; ++C) {
				if(range.s.r > R) range.s.r = R;
				if(range.s.c > C) range.s.c = C;
				if(range.e.r < R) range.e.r = R;
				if(range.e.c < C) range.e.c = C;
				var cell = {v: data[R][C] };
				if(cell.v == null) continue;
				var cell_ref = XLSX.utils.encode_cell({c:C,r:R});
				
				if(typeof cell.v === 'number') cell.t = 'n';
				else if(typeof cell.v === 'boolean') cell.t = 'b';
				else if(cell.v instanceof Date) {
					cell.t = 'n'; cell.z = XLSX.SSF._table[14];
					cell.v = datenum(cell.v);
				}
				else cell.t = 's';
				
				ws[cell_ref] = cell;
			}
		}
		if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
		return ws;
	}
	
	function Workbook() {
		if(!(this instanceof Workbook)) return new Workbook();
		this.SheetNames = [];
		this.Sheets = {};
	}
	
	function strToArrayBuf(s) {
		var buf = new ArrayBuffer(s.length);
		var view = new Uint8Array(buf);
		for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
		return buf;
	}
	
	function saveExcel(data,sheetName,fileName){
		if(!isIE9orLower() && !isNull(data)){
			var wb = new Workbook();
			var ws = sheet_from_array_of_arrays(data);
			wb.SheetNames.push(sheetName);
			wb.Sheets[sheetName] = ws;
			wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});

			blob = new Blob([strToArrayBuf(wbout)],{type:"application/octet-stream"});
			saveAs(blob, fileName);
		}else{
			//Do nothing as IE9 is not supported 
		}
	}
	
	function saveXML(xmlStr,fileName){
		if(!isIE9orLower() && !isNull(xmlStr)){
			blob = new Blob([xmlStr], { type: 'text/xml' });
			saveAs(blob, fileName);
		}else{
			//Do nothing as IE9 is not supported 
		}
	}
	
	function savePDF (data,TblTitle,fileName){
		if(!isIE9orLower() && !isNull(data)){
			var fontSizeHeading = 12, fontSizeTable =10, doc;
			doc = new jsPDF('landscape', 'pt', 'a4', true);
			//doc.setFont("times", "normal");
			doc.setFontSize(fontSizeHeading);
			doc.text(20,50,TblTitle);

			doc.setFontSize(fontSizeTable);
			height = doc.drawTable(data, {xstart:20,ystart:20,tablestart:70,marginleft:20});
			//doc.text(50, height + 20, 'hi world');
			doc.save(fileName);
		}else{
			//Do nothing as IE9 is not supported 
		}
	};

	function savePdfFromHtml (data,TblTitle,fileName){
		if(!isIE9orLower() && !isNull(data)){
			var fontSizeHeading = 14, fontSizeTable =10, doc;
			pdf = new jsPDF('landscape', 'pt', 'a3', true);
			    var img = new Image();
			    img.src = 'images/NetStore_Logo.png';
			    img.onload = function () {


			    var canvas = document.createElement("canvas");
			    canvas.width =this.width;
			    canvas.height =this.height;

			    var ctx = canvas.getContext("2d");
			    ctx.drawImage(this, 0, 0);


			    var dataURL = canvas.toDataURL("image/png");
			   
			   //return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
			   // return(dataURL);
				pdf.addImage(dataURL, 'PNG', 40, 10, 150, 70);

				var margins = { top: 110, bottom: 60, left: 40, width: 900 };
				
				pdf.setFontSize(fontSizeHeading);
				pdf.text(40,100,TblTitle);

				specialElementHandlers = {
						// element with id of "bypass" - jQuery style selector
						'#bypassme': function (element, renderer) {
							// true = "handled elsewhere, bypass text extraction"
							return true;
						}
				};
				
				// all coords and widths are in jsPDF instance's declared units
				// 'inches' in this case
				pdf.fromHTML(
					data, // HTML string or DOM elem ref.
					margins.left, // x coord
					margins.top, { // y coord
						'width': margins.width, // max width of content on PDF
						'elementHandlers': specialElementHandlers
					},

					function (dispose) {
						// dispose: object with X, Y of the last line add to the PDF 
						//          this allow the insertion of new lines after html
						pdf.save(fileName);
					}, margins);
			    };	
		}else{
			//Do nothing as IE9 is not supported 
		}
	};

	function addHTMLTableToPdf(jsPDFObj,tblData,TableName,config,callback){
		if(isNull(config)){
			//set defaults
			config = {
					startX:40,
					startY:50,
					x:startX,
					y:startY,
					normalFontSize:10,
					headingFontSize:12,
					rowHeigth:25,
					columnWidth:null,
					margins:{top: 70, bottom: 60, left: 40, width: 900 }
			};
		}
		
		y = addHeadingToPDF(jsPDFObj,TableName,config,false);
		
		var x = config.x; 
		var y = config.y ;
		
		jsPDFObj.setFontSize(config.normalFontSize);
		
		jsPDFObj.fromHTML(
					tblData, // HTML string or DOM elem ref.
					x, // x coord
					y, { // y coord
						'width': config.margins.width // max width of content on PDF
						//'elementHandlers': specialElementHandlers
					},
					function (dispose) {
						// dispose: object with X, Y of the last line add to the PDF 
						//          this allow the insertion of new lines after html
						callback();
					}, config.margins);

	};
	
	function addPageIfRequired(jsPDFObj,config){
		var pageHeigth = jsPDFObj.internal.pageSize.height - config.margins.bottom;
		if(config.y>=pageHeigth){
			jsPDFObj.addPage();
			config.y = config.startY;
			config.x = config.startX;
		}
	};
	
	function addSectionToPdf(jsPDFObj,sectionData,sectionName,config){
		if(isNull(config)){
			//set defaults
			config = {
					startX:40,
					startY:50,
					x:startX,
					y:startY,
					normalFontSize:11,
					headingFontSize:13,
					rowHeigth:20,
					columnWidth:null,
					margins:{top: 70, bottom: 60, left: 40, width: 900 }
			};
		}
		config.y = addHeadingToPDF(jsPDFObj,sectionName,config,true);

		var rows = sectionData.getRows();
		jsPDFObj.setFontSize(config.normalFontSize);
		for(var i=0;i<rows.length;i++){
			var row = rows[i];
			addPageIfRequired(jsPDFObj,config);
			for(var j=0;j<row.length;j++){
				config.x= config.x+ config.columnWidth[j];
				jsPDFObj.text(config.x,config.y,row[j]);
			}
			config.x = config.startX;
			config.y = config.y + config.rowHeigth;
		}
		config.y = config.y+config.rowHeigth;
		addPageIfRequired(jsPDFObj,config);
		return config.y ; //add space of one extra line for next section
	};
	
	function addHeadingToPDF (jsPDFObj, heading, config, gotoNextLine){
		addPageIfRequired(jsPDFObj,config);
		if(!isNull(heading) && !isEmptyString(heading)){
			jsPDFObj.setFontSize(config.headingFontSize);
			//var fontType = jsPDFObj.getFontType();
			jsPDFObj.setFontType("bold");
			jsPDFObj.text(config.x, config.y,heading);
			jsPDFObj.setFontType("normal");
			if(isNull(gotoNextLine) || gotoNextLine){
				config.y = config.y + config.rowHeigth;
			}
			addPageIfRequired(jsPDFObj,config);
		}
		return config.y;
	}
	
	function saveDetailsPDF (ordersLines,basicInfo,orderText,orderReferences,orderFees,addresses,docHeading,fileName){
		if(!isIE9orLower()){
			
			var img = new Image();
		    img.src = 'images/NetStore_Logo.png';
		    img.onload = function () {
		    	pdf = new jsPDF('landscape', 'pt', 'a3', true);
		    	var canvas = document.createElement("canvas");
		    	canvas.width =this.width;
		    	canvas.height =this.height;
		    	var ctx = canvas.getContext("2d");
		    	ctx.drawImage(this, 0, 0);
		    	var dataURL = canvas.toDataURL("image/png");
		    	pdf.addImage(dataURL, 'PNG', 40, 10, 150, 70);
		    	var pdfmargins = { top: 110, bottom: 60, left: 40, width: 900 };
		    	config = {
					startX:40,
					startY:100,
					x:40,
					y:100,
					normalFontSize:10,
					headingFontSize:12,
					rowHeigth:25,
					columnWidth:[0,200,150,200,150],
					margins:pdfmargins
		    	};
			
		    	var callback2 = function(){
		    		pdf.save(fileName);
		    	};

		    	var callback1 = function(){

		    		config.y = pdf.lastCellPos.y + pdf.lastCellPos.h + 40;
				
		    		if(!isNull(basicInfo)){
		    			config.columnWidth =[0,200,150,150,200],
		    			config.y = addSectionToPdf(pdf,basicInfo,basicInfo.heading,config);
		    		}
				
		    		if(!isNull(orderText)){
		    			config.columnWidth = [0,200];
		    			config.y = addSectionToPdf(pdf,orderText,orderText.heading,config);
		    		}

		    		if(!isNull(orderReferences)){
		    			config.columnWidth = [0,200,150,150,200];
		    			config.y = addSectionToPdf(pdf,orderReferences,orderReferences.heading,config);
		    		}
		    		if(!isNull(orderFees)){
		    			config.columnWidth = [0,200,200];
		    			config.y = addSectionToPdf(pdf,orderFees,orderFees.heading,config);
		    		}
		    		if(!isNull(addresses)){
		    			addHTMLTableToPdf(pdf,addresses.getHTML(),addresses.heading,config,callback2);
		    		}else{
					callback2();
		    		}
		    	};
			
		    	config.y = addHeadingToPDF(pdf,docHeading,config,true);
			
		    	if(!isNull(ordersLines)){
		    		addHTMLTableToPdf(pdf,ordersLines.getHTML(),ordersLines.heading,config,callback1);
		    	}else{
				callback1();
		    	}
		    };
			
		}else{
			//Do nothing as IE9 is not supported 
		}
	};
	
	function NEWsaveDetailsPDF (ordersLines,basicInfo,orderText,orderReferences,orderFees,addresses,docHeading,fileName){
		if(!isIE9orLower()){
			
			var img = new Image();
		    img.src = 'images/NetStore_Logo.png';
		    img.onload = function () {
		    	pdf = new jsPDF('portrait','pt', 'a4', true);
		    	var canvas = document.createElement("canvas");
		    	canvas.width =this.width;
		    	canvas.height =this.height;
		    	var ctx = canvas.getContext("2d");
		    	ctx.drawImage(this, 0, 0);
		    	var dataURL = canvas.toDataURL("image/png");
		    	pdf.addImage(dataURL, 'PNG', 40, 10, 150, 70);
		    	var pdfmargins = { top: 110, bottom: 60, left: 40, width: 900 };
		    	config = {
					startX:40,
					startY:100,
					x:40,
					y:100,
					normalFontSize:10,
					headingFontSize:12,
					rowHeigth:25,
					columnWidth:[0,200,150,200,150],
					margins:pdfmargins
		    	};
			
		    	var callback2 = function(){
		    		pdf.save(fileName);
		    	};

		    	var callback1 = function(){

		    		config.y = pdf.lastCellPos.y + pdf.lastCellPos.h + 40;
				
		    		if(!isNull(basicInfo)){
		    			config.columnWidth =[0,200,150,150,200,200],
		    			config.y = addSectionToPdf(pdf,basicInfo,basicInfo.heading,config);
		    		}
				
		    		if(!isNull(orderText)){
		    			config.columnWidth = [0,200];
		    			config.y = addSectionToPdf(pdf,orderText,orderText.heading,config);
		    		}

		    		if(!isNull(orderReferences)){
		    			config.columnWidth = [0,200,150,150,200];
		    			config.y = addSectionToPdf(pdf,orderReferences,orderReferences.heading,config);
		    		}
		    		if(!isNull(orderFees)){
		    			config.columnWidth = [0,200,200];
		    			config.y = addSectionToPdf(pdf,orderFees,orderFees.heading,config);
		    		}
		    		if(!isNull(addresses)){
		    			addHTMLTableToPdf(pdf,addresses.getHTML(),addresses.heading,config,callback2);
		    		}else{
					callback2();
		    		}
		    	};
			
		    	config.y = addHeadingToPDF(pdf,docHeading,config,true);
			
		    	if(!isNull(ordersLines)){
		    		addHTMLTableToPdf(pdf,ordersLines.getHTML(),ordersLines.heading,config,callback1);
		    	}else{
				callback1();
		    	}
		    };
			
		}else{
			//Do nothing as IE9 is not supported 
		}
	};
	
	
	function testSavePDF(data,headers,TblTitle,fileName){
		var pdf = new jsPDF('landscape', 'pt', 'a3', true),src;
        //pdf.setLineWidth(2);
		//pdf.setFontSize(10);
		var data = [] ;
		for(var i=0;i<50;i++){
			var row = new Object();
			row.header1 = "val-" + i;
			row.header2 = "val-" + i;
			row.header3 = "val-" + i;
			row.header4 = "val-" + i;
			data.push(row);
		}
		
       pdf.table(20, 20, data, data[0].keys, { printHeaders: false,autoSize:true,fontSize:14});
		// pdf.table(20, 20, data, headers, { printHeaders: false,autoSize:true,fontSize:9});
       // src = pdf.output('datauristring'); 
        pdf.save(fileName);
	};