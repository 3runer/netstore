var netStoreApp = angular.module('netStoreApp', [
'catalougeControllers',
'ui.router',
'configPropertyService',
'ui.bootstrap',
'authenticationService',
'AppServicesModule',
'slick'
]);

netStoreApp.config(function($stateProvider,$httpProvider) {
	if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};    
    }    

    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
    
	$stateProvider

//	.state('root', {
//	url: "",
//	abstract :true,
//	template: '<ui-view/>',
//	resolve:{
//	configObj:  function(NewConfigService){
//	return NewConfigService.init().$promise;
//	}
//	},
//	controller: function($scope,configObj){
//	$scope.simple = configObj;
//	}
//	})
	.state('home', {
		url: "",
		title: 'TXT_PAGE_TITLE_HOME',
//		resolve:{
//			configObj:  function(NewConfigService){
//				return NewConfigService.init().$promise;
//			}
//		},
		views: {
			"catalogMenuView": { 
				templateUrl: "partial/menu_template.html"
			},
			"detailSection": { 
				templateUrl: "partial/homecat_template.html"
//					templateUrl : function($stateParams){
//					if($stateParams.CAT_ID == null || $stateParams.CAT_ID == 'undefined'){
//					return 'partial/homecat_template.html';
//					}else{
//					return 'partial/main_catalog_template.html';
//					}
//					}
					//controller : 'catHomeCtrl'
			},
			"bannerView@home": { 
				templateUrl: "partial/banner.html"
			},
			"catalogView@home": { 
				templateUrl: "partial/homecat_grid_view.html"
			},
			"loginView": { 
				templateUrl: "partial/login_view.html"
			},
			"functionMenuView": {
				templateUrl: "partial/functionMenu_view.html"
			},
			
			"dlgView": {
				templateUrl: "partial/dailog_views.html"
			},
			"bestOfferView@home": { 
				templateUrl: "partial/best_offers_template.html"
			},
			"mostPolpularView@home": { 
				templateUrl: "partial/most_popular_template.html"
			}
		}
	})

	.state('home.invoice', {
		url: "/invoiceHistory",
		parent:'home',
		title: 'TXT_PAGE_TITLE_INVOICE_HISTORY',
		views: {
			"detailSection@": { 
				templateUrl: "partial/invoice_history_list.html"
			}
		}
	})
	.state('home.invoice.detail', {
		url: "/invoiceDetail",
		parent:'home',
		title: 'TXT_PAGE_TITLE_INVOICE_DETAIL',
		views: {
			"detailSection@": { 
				templateUrl: "partial/invoice_detail.html"
			}
		}
	})
	.state('home.invoiceline', {
		url: "/invoiceLineDetail",
		parent:'home',
		title: 'TXT_PAGE_TITLE_INVOICE_LINE_DETAIL',
		views: {
			"detailSection@": { 
				templateUrl: "partial/invoice_line_detail.html"
			}
		}
	})
	.state('home.request', {
		url: "/requestHistory",
		parent:'home',
		title: 'TXT_PAGE_TITLE_REQUEST_HISTORY',
		views: {
			"detailSection@": { 
				templateUrl: "partial/request_history_list.html"
			}
		}
	})
	.state('home.request.detail', {
		url: "/requestDetail",
		parent:'home',
		title: 'TXT_PAGE_TITLE_REQUEST_DETAIL',
		views: {
			"detailSection@": { 
				templateUrl: "partial/request_detail.html"
			}
		}
	})
	.state('home.requestline', {
		url: "/requestLineDetail",
		parent:'home',
		title: 'TXT_PAGE_TITLE_REQUEST_LINE_DETAIL',
		views: {
			"detailSection@": { 
				templateUrl: "partial/request_line_detail.html"
			}
		}
	})
	.state('home.requestsubmit', {
		url: "/requestsubmit",
		parent:'home',
		title: 'TXT_PAGE_TITLE_SUBMIT_REQUEST',
		views: {
			"detailSection@": { 
				templateUrl: "partial/request_new.html"
			},
			"requestBase@home.requestsubmit": { 
				templateUrl: "partial/add_product_template.html"
					
			},
		"requestFor@home.requestsubmit": { 
				templateUrl: "partial/request_new_line_template.html"
		}
					
			
		}
	})
	.state('home.requestsubmit.addproduct', {
		url: "/RequestAddProduct",
		parent:'home.requestsubmit',
		title: 'TXT_PAGE_TITLE_SUBMIT_REQUEST',
		views: {

			"requestFor@home.requestsubmit": { 
				templateUrl: "partial/add_product_template.html"
			}
					
			
		}
	})
	.state('home.requestsubmit.confirmation', {
		url: "/RequestConfirmation",
		parent:'home.requestsubmit',
		title: 'TXT_PAGE_TITLE_SUBMIT_REQUEST',
		views: {

			"detailSection@": { 
				templateUrl: "partial/request_confirmation.html"
			},
			"requestFor@home.requestsubmit": { 
				templateUrl: "partial/request_new_line_template.html"
			}
		}
	})
	
	.state('home.requestsubmit.received.compare', {
		url:"/compare",
		parent:'home.requestsubmit.received',
		title: 'TXT_PAGE_TITLE_COMPARE',
		views: {
			"promotionSection@home.requestsubmit.received": { 
				templateUrl: "partial/catalog_compare.html"
			}
		}
	})

	
	.state('home.requestsubmit.received', {
		url: "/RequestReceived",
		parent:'home.requestsubmit',
		title: 'TXT_PAGE_TITLE_SUBMIT_REQUEST',
		labelKey : 'CON_TABLE_VIEW',
		views: {

			"detailSection@": { 
				templateUrl: "partial/request_received_confirmation.html"
			},
			"promotionSection@home.requestsubmit.received":{
				templateUrl: "partial/you_may_like_template.html"
			},
			"catalogViewPromotion@home.requestsubmit.received": { 
				templateUrl: "partial/you_may_like_grid.html"
			}
				
		}
	})
	.state('home.requestsubmit.received.promotionList', {
		url: "/list",
		labelKey : 'CON_GRID_VIEW',
		parent:'home.requestsubmit.received',
		title: 'TXT_PAGE_TITLE_SUBMIT_REQUEST',
		views: {
			"catalogViewPromotion@home.requestsubmit.received": { 
				templateUrl: "partial/you_may_like_list.html"
			}
		}
	})
		.state('home.requestsubmit.received.promotionGrid', {
		url: "/grid",
		labelKey : 'CON_TABLE_VIEW',
		parent:'home.requestsubmit.received',
		title: 'TXT_PAGE_TITLE_SUBMIT_REQUEST',
		views: {
			"catalogViewPromotion@home.requestsubmit.received": { 
				templateUrl: "partial/you_may_like_grid.html"
			}
		}
	})
	
	.state('home.requestsubmit.requestLine', {
		url: "/RequestNewLine",
		parent:'home.requestsubmit',
		title: 'TXT_PAGE_TITLE_SUBMIT_REQUEST',
		views: {

			"requestFor@home.requestsubmit": { 
				templateUrl: "partial/request_new_line_template.html"
			}
					
			
		}
	})
	.state('home.requestsubmit.order', {
		url: "/orderRequestsubmit/{ORDER_NUM}",
		parent:'home.requestsubmit',
		title: 'TXT_PAGE_TITLE_SUBMIT_REQUEST',
		views: {

			"requestFor@home.requestsubmit": { 
				templateUrl: "partial/order_history_new_request.html"
			}
					
			
		}
	})
	.state('home.requestsubmit.orderdetail', {
		url: "/orderDetails",
		parent:'home.requestsubmit',
		title: 'TXT_PAGE_TITLE_SUBMIT_REQUEST',
		views: {
			"requestFor@home.requestsubmit": { 
				templateUrl: "partial/order_detail_new_request.html"
			}
		}
	})

	.state('home.requestsubmit.invoice', {
		url: "/invoiceRequestsubmit/{INVOICE_NUM}",
		parent:'home.requestsubmit',
		title: 'TXT_PAGE_TITLE_SUBMIT_REQUEST',
		views: {

			"requestFor@home.requestsubmit": { 
				templateUrl: "partial/invoice_history_new_request.html"
			}
					
			
		}
	})
	.state('home.requestsubmit.invoicedetail', {
		url: "/invoiceDetails",
		parent:'home.requestsubmit',
		title: 'TXT_PAGE_TITLE_SUBMIT_REQUEST',
		views: {
			"requestFor@home.requestsubmit": { 
				templateUrl: "partial/invoice_detail_new_request.html"
			}
		}
	})
	
	//ADM-XXX
	.state('home.requestsubmit.transportnote', {
		url: "/transportnoteRequestsubmit/{INVOICE_NUM}",
		parent:'home.requestsubmit',
		title: 'TXT_PAGE_TITLE_SUBMIT_REQUEST',
		views: {
			
			"requestFor@home.requestsubmit": { 
				templateUrl: "partial/transportnote_history_new_request.html"
			}
	
	
		}
	})
	.state('home.requestsubmit.transportnotedetail', {
		url: "/transportnoteDetails",
		parent:'home.requestsubmit',
		title: 'TXT_PAGE_TITLE_SUBMIT_REQUEST',
		views: {
			"requestFor@home.requestsubmit": { 
				templateUrl: "partial/transportnote_detail_new_request.html"
			}
		}
	})
	//ADM-XXX End
	.state('home.requestsubmit.shoppinglist', {
		url: "/shoppinglistRequest",
		parent:'home.requestsubmit',
		title: 'TXT_PAGE_TITLE_SUBMIT_REQUEST',
		views: {
			"requestFor@home.requestsubmit": { 
				templateUrl: "partial/shopping_list_req_submit.html"
			}
		}
	})
	.state('home.quotation', {
		url: "/quotationHistory",
		parent:'home',
		title: 'TXT_PAGE_TITLE_QUOTATION_HISTORY',
		views: {
			"detailSection@": { 
				templateUrl: "partial/quotation_history_list.html"
			}
		}
	})
	.state('home.quotation.detail', {
		url: "/quotationDetail",
		parent:'home',
		title: 'TXT_PAGE_TITLE_QUOTATION_DETAIL',
		views: {
			"detailSection@": { 
				templateUrl: "partial/quotation_detail.html"
			}
		}
	})
	.state('home.quotationline', {
		url: "/quotationLineDetail",
		parent:'home',
		title: 'TXT_PAGE_TITLE_QUOTATION_LINE_DETAIL',
		views: {
			"detailSection@": { 
				templateUrl: "partial/quotation_line_detail.html"
			}
		}
	})
	// ADM-003
	.state('home.transportNote', {
		url: "/transportNoteHistory",
		parent:'home',
		title: 'TXT_PAGE_TITLE_TRANSPORTNOTE_HISTORY',
		views: {
			"detailSection@": { 
				templateUrl: "partial/transportNote_history_list.html"
			}
		}
	})
	.state('home.transportNote.detail', {
		url: "/transportNoteDetail",
		parent:'home',
		title: 'TXT_PAGE_TITLE_TRANSPORTNOTE_DETAIL',
		views: {
			"detailSection@": { 
				templateUrl: "partial/transportNote_detail.html"
			}
		}
	})
	.state('home.transportNoteline', {
		url: "/transportNoteLineDetail",
		parent:'home',
		title: 'TXT_PAGE_TITLE_TRANSPORTNOTE_LINE_DETAIL',
		views: {
			"detailSection@": { 
				templateUrl: "partial/transportNote_line_detail.html"
			}
		}
	})
	.state('home.catalog', {
		url: "/maincatalog/{ITEM_CODE}",
		parent:'home',
		title: 'TXT_PAGE_TITLE_CATALOG',
		views: {
			"detailSection@": { 
				templateUrl: "partial/catalog_template.html"
			}
		}
	})
		.state('home.catalogSub', {
		url: "/catalog/{ITEM_CODE}/{ELEMENT_ID}",
		parent:'home',
		title: 'TXT_PAGE_TITLE_CATALOG',
		views: {
			"detailSection@": { 
				templateUrl: "partial/catalog_template.html"
			}
		}
	})
	
	.state('home.homeCatalogTable', {
		url: "/homeCatalogList",
		labelKey : 'CON_GRID_VIEW',	
		title: 'TXT_PAGE_TITLE_CATALOG',
		views: {
			"catalogView@home": { 
				templateUrl: "partial/homecat_list_view.html"
			}
//			"bannerView@home": { 
//				templateUrl: "partial/banner.html"
//			},
//			"bestOfferView@home": { 
//				templateUrl: "partial/best_offers_template.html"
//			},
//			"mostPolpularView@home": { 
//				templateUrl: "partial/most_popular_template.html"
//			}
	
		}
	})
	.state('home.homeCatalogGrid', {
		url:"/homeCatalogGrid",
		labelKey : 'CON_TABLE_VIEW',	
		parent:'home',
		title: 'TXT_PAGE_TITLE_CATALOG',
		views: {
			"catalogView@home": { 
				templateUrl: "partial/homecat_grid_view.html"
			}
//			"bannerView@home": { 
//				templateUrl: "partial/banner.html"
//			},
//			"bestOfferView@home": { 
//				templateUrl: "partial/best_offers_template.html"
//			},
//			"mostPolpularView@home": { 
//				templateUrl: "partial/most_popular_template.html"
//			}
		}
	})

	
	.state('home.bestoffer', {
		url:"/BestOffer",
		parent:'home',
		pageLabel : 'CON_BEST_OFFERS',
		labelKey : 'CON_TABLE_VIEW',	
		title: 'TXT_PAGE_TITLE_BEST_OFFER',
		views: {
			"detailSection@": { 
				templateUrl: "partial/promotional_catalog_bestoffer_template.html"
			//	controller : 'promotionOffersCtrl'
			},
			"catalogViewPromotion@home.bestoffer": { 
				templateUrl: "partial/promotional_catalog_grid.html"
			}
	
		}
	})
	.state('home.bestoffer.Compare', {
		url:"/Compare/{ID}",
		parent:'home.bestoffer',
		title: 'TXT_PAGE_TITLE_COMPARE',
		views: {
			"detailSection@": { 
				templateUrl: "partial/catalog_compare.html"
			}
		}
	})
	
	.state('home.goThankYou.Compare', {
		url:"/Compare/{ID}",
		parent:'home.goThankYou',
		title: 'TXT_PAGE_TITLE_COMPARE',
		views: {
			"detailSection@": { 
				templateUrl: "partial/catalog_compare.html"
			}
		}
	})
		.state('home.bestoffer.promotionList', {
		url: "/list",
		pageLabel : 'CON_BEST_OFFERS',
		parent:'home.bestoffer',
		labelKey : 'CON_GRID_VIEW',	
		title: 'TXT_PAGE_TITLE_BEST_OFFER',
		views: {
			"catalogViewPromotion@home.bestoffer": { 
				templateUrl: "partial/promotional_catalog_list.html"
			}
		}
	})
		.state('home.bestoffer.promotionGrid', {
		url: "/grid",
		pageLabel : 'CON_BEST_OFFERS',
		labelKey : 'CON_TABLE_VIEW',
		parent:'home.bestoffer',	
		title: 'TXT_PAGE_TITLE_BEST_OFFER',
		views: {
			"catalogViewPromotion@home.bestoffer": { 
				templateUrl: "partial/promotional_catalog_grid.html"
			}
		}
	})
	
	
	.state('home.popular', {
		url:"/MostPopular",		
		parent:'home',
		pageLabel : 'CON_MOST_POPULAR',
		labelKey : 'CON_TABLE_VIEW',
		title: 'TXT_PAGE_TITLE_MOST_POPULAR',
		views: {
			"detailSection@": { 
				templateUrl: "partial/promotional_catalog_mostpopular_template.html"
			//	controller : 'promotionOffersCtrl'
			},
			"catalogViewPromotion@home.popular": { 
				templateUrl: "partial/promotional_catalog_grid.html"
			}
		}
	})
	.state('home.popular.promotionList', {
		url: "/list",
		parent:'home.popular',
		pageLabel : 'CON_MOST_POPULAR',
		labelKey : 'CON_GRID_VIEW',		
		title: 'TXT_PAGE_TITLE_MOST_POPULAR',
		views: {
			"catalogViewPromotion@home.popular": { 
				templateUrl: "partial/promotional_catalog_list.html"
			}
		}
	})
		.state('home.popular.promotionGrid', {
		url: "/grid",
		labelKey : 'CON_TABLE_VIEW',
		pageLabel : 'CON_MOST_POPULAR',
		parent:'home.popular',	
		title: 'TXT_PAGE_TITLE_MOST_POPULAR',
		views: {
			"catalogViewPromotion@home.popular": { 
				templateUrl: "partial/promotional_catalog_grid.html"
			}
		}
	})
	//ADM-008
	.state('home.alternatives', {
		url:"/AlternativesItems",		
		parent:'home',
		pageLabel : 'CON_ALTERNATIVES_ITEMS',
		labelKey : 'CON_TABLE_VIEW',
		title: 'TXT_PAGE_TITLE_CON_ALTERNATIVES_ITEMS',
		views: {
			"detailSection@": { 
				templateUrl: "partial/alternatives_items_home_template.html"
			},
			"catalogViewPromotion@home.alternatives": { 
				templateUrl: "partial/promotional_catalog_grid.html"
			}
		}
	})
	.state('home.alternatives.list', {
		url: "/list",
		parent:'home.alternatives',
		pageLabel : 'CON_ALTERNATIVES_ITEMS',
		labelKey : 'CON_GRID_VIEW',		
		title: 'TXT_PAGE_TITLE_CON_ALTERNATIVES_ITEMS',
		views: {
			"catalogViewPromotion@home.alternatives": { 
				templateUrl: "partial/promotional_catalog_list.html"
			}
		}
	})
	.state('home.alternatives.grid', {
		url: "/grid",
		parent:'home.alternatives',	
		pageLabel : 'CON_ALTERNATIVES_ITEMS',
		labelKey : 'CON_TABLE_VIEW',
		title: 'TXT_PAGE_TITLE_CON_ALTERNATIVES_ITEMS',
		views: {
			"catalogViewPromotion@home.alternatives": { 
				templateUrl: "partial/promotional_catalog_grid.html"
			}
		}
	})
	//ADM-008 end
	.state('home.mainCatalog', {
		url:"/mainCatalog/{CAT_ID}/{ELEMENT_ID}/{ELEMENT_DESC}/{VIEW}",
		parent:'home',
		title: 'TXT_PAGE_TITLE_MAIN_CATALOG',
		views: {
			"detailSection@": { 
				templateUrl: "partial/main_catalog_template.html",
				controller : 'mainCatalogCtrl'
			}
		}
	})
	.state('home.mainCatalog.Compare', {
		url:"/Compare",
		parent:'home.mainCatalog',
		title: 'TXT_PAGE_TITLE_COMPARE',
		views: {
			"detailSection@": { 
				templateUrl: "partial/product_compare.html",
				controller : 'mainCatalogCtrl'
			}
		}
	})
	.state('home.catalog.Compare', {
		url:"/Compare",
		parent:'home.catalog',
		title: 'TXT_PAGE_TITLE_COMPARE',
		views: {
			"detailSection@": { 
				templateUrl: "partial/product_compare.html",
				controller : 'catMenuCtrlView'
			}
		}
	})
	.state('home.Compare', {
		url:"/Compare",
		parent:'home',
		title: 'TXT_PAGE_TITLE_COMPARE',
		views: {
			"detailSection@": { 
				templateUrl: "partial/homecat_product_compare.html"
				//controller : 'mainCatalogCtrl'
			}
		}
	})
	
	.state('home.mail', {
		url:"/mail",
		parent:'home',
		title: 'TXT_PAGE_TITLE_CATALOG',
		views: {
			"catalogView@home": { 
				templateUrl: "partial/homecat_grid_view.html"
			}
		}
	})
	.state('home.shoppingcart', {
		url: "/shoppingCart",
		parent:'home',
		title: 'TXT_PAGE_TITLE_SHOPPING_CART',
		labelKey : 'CON_TABLE_VIEW',
		views: {
			"detailSection@": { 
				templateUrl: "partial/shoppingCart.html"
			},
			"shopCartChildView@home.shoppingcart": { 
				templateUrl: "partial/shopCartMainView.html"
			},
			"productSec@home.shoppingcart": { 
				templateUrl: "partial/add_product_shopping_template.html"
			},
			"copySec@home.shoppingcart": { 
				templateUrl: "partial/copy_paste_shopping_template.html"
			},
			"promotionSection@home.shoppingcart":{
				templateUrl: "partial/you_may_like_template.html"
			},
			"catalogViewPromotion@home.shoppingcart": { 
				templateUrl: "partial/you_may_like_grid.html"
			}
					
		}
	})
	
	.state('home.shoppingcart.compare', {
		url:"/compare",
		parent:'home.shoppingcart',
		title: 'TXT_PAGE_TITLE_COMPARE',
		views: {
			"promotionSection@home.shoppingcart": { 
				templateUrl: "partial/catalog_compare.html"
			}
		}
	})
	
	.state('home.shoppingcart.promotionList', {
		url: "/list",
		labelKey : 'CON_GRID_VIEW',
		parent:'home.shoppingcart',
		title: 'TXT_PAGE_TITLE_SHOPPING_CART',
		views: {
			"catalogViewPromotion@home.shoppingcart": { 
				templateUrl: "partial/you_may_like_list.html"
			}
		}
	})
		.state('home.shoppingcart.promotionGrid', {
		url: "/grid",
		labelKey : 'CON_TABLE_VIEW',
		parent:'home.shoppingcart',
		title: 'TXT_PAGE_TITLE_SHOPPING_CART',
		views: {
			"catalogViewPromotion@home.shoppingcart": { 
				templateUrl: "partial/you_may_like_grid.html"
			}
		}
	})
	
	.state('home.shoppingcart.addproduct', {
		url: "/CartAddProduct",
		parent:'home.shoppingcart',
		title: 'TXT_PAGE_TITLE_SHOPPING_CART',
		views: {

			"productSec@home.shoppingcart": { 
				templateUrl: "partial/add_product_shopping_template.html"
			}
					
			
		}
	})
	.state('home.shoppingcart.copyproduct', {
		url: "/CartCopyProduct",
		parent:'home.shoppingcart',
		title: 'TXT_PAGE_TITLE_SHOPPING_CART',
		views: {

			"copySec@home.shoppingcart": { 
				templateUrl: "partial/copy_paste_shopping_template.html"
			}
					
			
		}
	})
	
	.state('home.shoppingcart.interruptedCart', {
	url: "/interruptedCart",
	parent:'home.shoppingcart',
	title: 'TXT_PAGE_TITLE_SHOPPING_CART',
	views: {
		"shopCartChildView@home.shoppingcart": { 
			templateUrl: "partial/interruptedCarts.html"
		}
	}
	})
	.state('home.reviewOrder', {
		url: "/reviewOrder",
		parent:'home',
		title: 'TXT_PAGE_TITLE_REVIEW_ORDER',
		views: {
			"detailSection@": { 
				templateUrl: "partial/reviewOrder.html"
			}
		}
	})
	.state('home.goThankYou', {
		url: "/thankYou/{PAY_MODE}/{MESSAGE_CODE}/{ORDER_NUM}",
		labelKey : 'CON_TABLE_VIEW',
		parent:'home',
		title: 'TXT_PAGE_TITLE_THANK_YOU',
		views: {
			"detailSection@": { 
				templateUrl: "partial/thank_you.html"
			},
			"promotionSection@home.goThankYou":{
				templateUrl: "partial/you_may_like_template.html"
			},
			"catalogViewPromotion@home.goThankYou": { 
				templateUrl: "partial/you_may_like_grid.html"
			}
			

		}
	})
	
	.state('home.goThankYou.promotionList', {
		url: "/list",
		parent:'home.goThankYou',
		title: 'TXT_PAGE_TITLE_THANK_YOU',
		labelKey : 'CON_GRID_VIEW',
		views: {
			"catalogViewPromotion@home.goThankYou": { 
				templateUrl: "partial/you_may_like_list.html"
			}
		}
	})
		.state('home.goThankYou.promotionGrid', {
		url: "/grid",
		labelKey : 'CON_TABLE_VIEW',
		parent:'home.goThankYou',
		title: 'TXT_PAGE_TITLE_THANK_YOU',
		views: {
			"catalogViewPromotion@home.goThankYou": { 
				templateUrl: "partial/you_may_like_grid.html"
			}
		}
	})
	.state('home.goThankYou.compare', {
		url:"/compare",
		parent:'home.goThankYou',
		title: 'TXT_PAGE_TITLE_COMPARE',
		views: {
			"promotionSection@home.goThankYou": { 
				templateUrl: "partial/catalog_compare.html"
			}
		}
	})
		
	.state('home.shoppingcart.information', {
		url: "/deliveryPaymentInformation",
		parent:'home.shoppingcart',
		title: 'TXT_PAGE_TITLE_SHOPPING_CART',
		views: {
			"shopCartChildView@home.shoppingcart": { 
				templateUrl: "partial/deliveryPaymentInformation.html"
			}
		}
	})
	.state('home.account', {
		url: "/account",
		parent:'home',
		title: 'TXT_PAGE_TITLE_ACCOUNT',
		views: {
			"detailSection@": { 
				templateUrl: "partial/account_receivable.html"
			}
		}
	})
	.state('home.order', {
		url: "/order",
		parent:'home',
		title: 'TXT_PAGE_TITLE_ORDER',
		views: {
			"detailSection@": { 
				templateUrl: "partial/order_history.html"
			}
		}
	})
	.state('home.order.detail', {
		url: "/orderDetails/{ORDER_NUM}",
		parent:'home',
		title: 'TXT_PAGE_TITLE_ORDER',
		views: {
			"detailSection@": { 
				templateUrl: "partial/order_detail.html"
			}
		}
	})
	.state('home.orderline', {
		url: "/orderLineDetail",
		parent:'home',
		title: 'TXT_PAGE_TITLE_ORDER',
		views: {
			"detailSection@": { 
				templateUrl: "partial/order_line_detail.html"
			}
		}
	})
	
	.state('home.productDetail', {
		url:"/ProductDetail/{ITEM_CODE}",
		parent:'home',
		title: 'TXT_PAGE_TITLE_PRODUCT_DETAIL',
		labelKey : 'CON_TABLE_VIEW',
		views: {
			"detailSection@": { 
				templateUrl: "partial/product_detail.html"
				//controller : 'productDetailCtrl'
			},
			"promotionSection@home.productDetail":{
				templateUrl: "partial/you_may_like_template.html"
			},
			"catalogViewPromotion@home.productDetail": { 
				templateUrl: "partial/you_may_like_grid.html"
			}			
			//ADM-008
			,"alternativeItemsView@home.productDetail": { 
				templateUrl: "partial/alternatives_items_template.html"
			}
			//ADM-008 end
		}
	})
	.state('home.productDetail.promotionList', {
		url:"/list",
		labelKey : 'CON_GRID_VIEW',
		parent:'home.productDetail',
		title: 'TXT_PAGE_TITLE_PRODUCT_DETAIL',
		views: {
			"catalogViewPromotion@home.productDetail": { 
				templateUrl: "partial/you_may_like_list.html"
			}
		}
	})
	.state('home.productDetail.compare', {
		url:"/compare",
		parent:'home.productDetail',
		title: 'TXT_PAGE_TITLE_COMPARE',
		views: {
			"promotionSection@home.productDetail": { 
				templateUrl: "partial/catalog_compare.html"
			}
		}
	})
	.state('home.productDetail.promotionGrid', {
		url:"/grid",
		labelKey : 'CON_TABLE_VIEW',
		parent:'home.productDetail',
		title: 'TXT_PAGE_TITLE_PRODUCT_DETAIL',
		views: {
			"catalogViewPromotion@home.productDetail": { 
				templateUrl: "partial/you_may_like_grid.html"
			}		}
	})

	

	
	.state('home.topitem', {
		url: "/topItems",
		parent:'home',
		title: 'TXT_PAGE_TITLE_TOP_ITEMS',
		views: {
			"detailSection@": { 
				templateUrl: "partial/top_items.html"
			}
		}
	})
	
	.state('home.editaccount', {
		url: "/editAccount",
		parent:'home',
		title: 'TXT_PAGE_TITLE_EDIT_ACCOUNT',
		views: {
			"detailSection@": { 
				templateUrl: "partial/edit_account.html"
			}
		}
	})
	.state('home.shoppinglist', {
		url: "/shoppingList",
		parent:'home',
		title: 'TXT_PAGE_TITLE_SHOPPING_LIST',
		views: {
			"detailSection@": { 
				templateUrl: "partial/shopping_list.html"
			}
		}
	})
	.state('home.shoppinglist.details', {
		url: "/shoppingListDetails",
		parent:'home',
		title: 'TXT_PAGE_TITLE_SHOPPING_LIST',
		views: {
			"detailSection@": { 
				templateUrl: "partial/shoppingList_details.html"
			},
			"productSection@home.shoppinglist.details": { 
				templateUrl: "partial/add_product_shopping_list.html"
			}
		}
	})
	/*.state('home.mainCatalog.productDetail', {
		url:"/ProductDetail",
		parent:'home.mainCatalog',
		views: {
			"detailSection@": { 
				templateUrl: "partial/product_detail.html",
				controller : 'productDetailCtrl'
			}
		}
	})*/
	.state('home.prodSearchResult', {
		url: "/prodSearchResult/{FREE_TEXT}/{AND_OR}/{BOOL_VALUE}",
		parent:'home',
		title: 'TXT_PAGE_TITLE_PRODUCT_SEARCH_RESULT',
		views: {
			"detailSection@": { 
				templateUrl: "partial/productSearch_template.html"
			},
			"productSearchResultView@home.prodSearchResult": { 
				templateUrl: "partial/prodSearchResult_grid_view.html"
			}
		}
	})
		.state('home.prodSearchResultMenu', {
		url: "/prodSearchResult/{FREE_TEXT}",
		parent:'home',
		title: 'TXT_PAGE_TITLE_PRODUCT_SEARCH_RESULT',
		views: {
			"detailSection@": { 
				templateUrl: "partial/productSearch_template.html"
			},
			"productSearchResultView@home.prodSearchResultMenu": { 
				templateUrl: "partial/prodSearchResult_grid_view.html"
			}
		}
	})
	
	.state('home.prodSearchResultMenu.listView', {
	url: "/listView",
	parent:'home.prodSearchResultMenu',
	title: 'TXT_PAGE_TITLE_PRODUCT_SEARCH_RESULT',
	searchSectionCollapsed:true,
	views: {
		"productSearchResultView@home.prodSearchResultMenu": { 
			templateUrl: "partial/prodSearchResult_list_view.html"
		}
	}
	})

	.state('home.prodSearchResult.listView', {
	url: "/listView",
	parent:'home.prodSearchResult',
	title: 'TXT_PAGE_TITLE_PRODUCT_SEARCH_RESULT',
	searchSectionCollapsed:true,
	views: {
		"productSearchResultView@home.prodSearchResult": { 
			templateUrl: "partial/prodSearchResult_list_view.html"
		}
	}
	})
	.state('home.prodSearchResult.Compare', {
		url:"/Compare",
		parent:'home.prodSearchResult',
		title: 'TXT_PAGE_TITLE_COMPARE',
		views: {
			"detailSection@": { 
				templateUrl: "partial/product_search_compare.html"
				//controller : 'mainCatalogCtrl'
			}
		}
	})
	
	.state('home.prodSearchResult.gridView', {
	url: "/gridView",
	parent:'home.prodSearchResult',
	title: 'TXT_PAGE_TITLE_PRODUCT_SEARCH_RESULT',
	searchSectionCollapsed:false,
	views: {
		"productSearchResultView@home.prodSearchResult": { 
			templateUrl: "partial/prodSearchResult_grid_view.html"
		}
	}
	})
	.state('home.changeCustomer', {
	url: "/changeCustomer",
	parent:'home',
	title: 'TXT_PAGE_TITLE_CHANGE_CUSTOMER',
	views: {
		"detailSection@": { 
			templateUrl: "partial/change_customer.html"
		}
	}
	});
	
	//Enable cross domain calls
    $httpProvider.defaults.useXDomain = true;

    //Remove the header used to identify ajax call  that would prevent CORS from working
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    
    $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
});

/*
 * directives and then animation removed from here by Prateek as a part of NST-2306
 */