
var IS_AUTHENTICATED = 'isAuthenticated';
var authenticationService = angular.module('authenticationService', []);

authenticationService.factory('authentication',['Util','DataSharingService','NewConfigService','SessionTracking','ShoppingCartService','ShoppingListService','$http','$rootScope','$q','$log','$window','$state',
                                                function(Util,DataSharingService,NewConfigService,SessionTracking,ShoppingCartService,ShoppingListService,$http,$rootScope,$q,$log,$window,$state){
	var serviceObject = { };
	var  winSession = $window.sessionStorage; 
	
	serviceObject.getUserData = function(username,password,langCode){
		$log.info("authentication.getUserData() -- going to fetch data from server");

		var params = ["UserID",username,"Password",password];
		
		if(Util.isNotNull(langCode)){
			params.push("LanguageCode");
			params.push(langCode);
		}

		var deferred = $q.defer();
		this.slientLogout(username).then(function(data){
			Util.getDataFromServer('userResponseBaseUrl',params).then(function(data){
				deferred.resolve(data);
			});
		});
		return deferred.promise;
	};

	serviceObject.userLoginByPost = function(username,password,langCode){
		$log.info("authentication.userLoggingbyPost() -- going to fetch data from server");
		var params = this.getPostRequestParam(username,password,langCode);
		var deferred = $q.defer();
		this.slientLogout(username).then(function(data){
			Util.postDataToServer('validateLoggedUserUrl',params).then(function(data){
				deferred.resolve(data);
			});
		});
		return deferred.promise;
	};
	
	serviceObject.getPostRequestParam = function(username,password,langCode){
		var reqObj = {};
		var param = null;
		reqObj.userID =username;
		reqObj.password =password;
		
		if(!isNull(langCode)){
			reqObj.languageCode = langCode;
		}
		param = [reqObj];
		param = "UserLoginData="+JSON.stringify(param);
		return param;
	};
	
	serviceObject.isUserAuthenticated = function(data)
	{
		var isAuthenticated = SessionTracking.isAuthenticated(data);
		if(isAuthenticated){
			this.initSession(data);
		}
		$log.log("authentication.isUserAuthenticated = "+ isAuthenticated );

		return isAuthenticated;
	};

	serviceObject.isLoggedIn = function()
	{
		return SessionTracking.isLoggedIn();
	};

	serviceObject.getLoginLabel = function()
	{
		return SessionTracking.getLoginLabel(Util.translate);
	};

	serviceObject.getUserName = function()
	{
		var usrObj = SessionTracking.getUserSession();
		if(usrObj!=null){
			user_name = "Welcome "+usrObj.userName;
		}
	};

	serviceObject.getUserId = function()
	{
		return SessionTracking.getUserId();
	};
	
	
	serviceObject.syncUISession = function(){
		var webSettings = DataSharingService.getWebSettings();
		var params = [];
		if(!isNull(webSettings.userCode) && webSettings.userCode!='NETSTORE_DEFAULT'){
			params = ["UserID",webSettings.userCode];
		}
		NewConfigService.getWebUser(params).then(function(data){
			var isAuthenticated = SessionTracking.isAuthenticated(data);
			if(isAuthenticated && !serviceObject.isLoggedIn()){
				serviceObject.isUserAuthenticated(data);
				$rootScope.isSignedIn = true;
			}else if(!isAuthenticated && serviceObject.isLoggedIn()){
				serviceObject.closeSession();
				$rootScope.isSignedIn = false;
			}
			console.log("syncUISession " + JSON.stringify(data));
		});
	};
	
	serviceObject.initSession = function(data){
		DataSharingService.setToSessionStorage(userResponseObject_Key,data);
		$rootScope.isSignedIn = true;
		SessionTracking.init();
		
	//	$rootScope.initCommitsNextPromise = $rootScope.deferredNext.promise;
	
		var deferred = $q.defer();
		$rootScope.userSettingsLoaded = deferred.promise;
		//$rootScope.openLoader();
		serviceObject.loadWebSettings(true).then(function(data){
			ShoppingCartService.getTemporaryOrderData([]);
			serviceObject.loadFilterDetailsSettings().then(function(data){
				deferred.resolve();
				//$rootScope.closeLoader();
			});
			$rootScope.shoppingList_lineCount =data.numOfShopListItemLines;
			$rootScope.currentList=data.defaultShoppingListId;
			$rootScope.CopyOrderDelimeter = data.defaultDelimiter;
			var requestParams = ["ListId",$rootScope.currentList,"ListOwnerCode",data.selectedListOwner];
			ShoppingListService.getShoppingListDetails(requestParams);
			$rootScope.getMenuList();
			//$rootScope.deferredNext.resolve();
		});
	};

	serviceObject.closeSession = function()
	{	
		if(this.isLoggedIn()){	
			var userId = this.getUserId();
			if(userId!=null  && userId!=''){
				var params = ["UserID",userId]	;
				Util.doLogout("logoutUrl",params).then(function(data){
					DataSharingService.removeFromSessionStorage("filterDetailsSettingsObject");
					DataSharingService.removeFromSessionStorage("changed_locale_code");
					NewConfigService.loadWebSettings().then(function(data){
						///$state.go('home');
						$rootScope.reloadHomePageUrl();
					});
					$rootScope.currentOrder_lineCount = "";
					if(Util.isNotNull(data)){
						if(Util.isNotNull(data.messageCode) && data.messageCode == 1100){
							$log.log("authentication.closeSession : Successfully logged out from server" );
						}else{
							$log.warn("authentication.closeSession : Unknown error occurred at sever while logout" );
						}
					}
				});
				SessionTracking.close();
				$rootScope.isSignedIn = false;
				DataSharingService.removeFromSessionStorage(userResponseObject_Key);
				$log.log("authentication.closeSession : user session removed" );
			}
		}else{
			$log.log("authentication.closeSession : User not logged in" );
		}
	};
	

	serviceObject.loadWebSettings = function(isForceLoad){
		var webSettingObj = DataSharingService.getWebSettings();
		var deferred = $q.defer();	
		if((angular.isDefined(isForceLoad) && isForceLoad == true) ||  webSettingObj == null){
			var params = [];
			var userId = this.getUserId();
			if(userId!=null  && userId!=''){
				params = ["UserID",userId]	;
			}
			Util.getDataFromServer('webSettingsUrl',params).then(function(data){
				DataSharingService.setWebSettings(data);
				$rootScope.webSettings = data;
				SessionTracking.init();
				deferred.resolve(data);
				console.log("authentication.loadWebSettings() : " + JSON.stringify(DataSharingService.getWebSettings()));
			});
		}else{
			deferred.resolve(null);
		}
		return deferred.promise ;
	};

	serviceObject.loadFilterDetailsSettings = function(){

		//$rootScope.deferredSecondNext = $q.defer();
		//$rootScope.initCommitsSecondNextPromise = $rootScope.deferredSecondNext.promise;
		var deferred = $q.defer();
		Util.getFilterListDetails([]).then(function(data){
			DataSharingService.setFilterDetailsSettings(data);
			$rootScope.filterDetailSettings = data;
			$rootScope.deferredSecondNext.resolve();
			deferred.resolve(data);
		});
		return deferred.promise ;
	};
	
	serviceObject.slientLogout = function(userId){
		var deferred = $q.defer();	
		if(!isNull(userId)){
			var params = ["UserID",userId];
			Util.doLogout("logoutUrl",params).then(function(data){
				deferred.resolve(null);
			},function(reason){
				deferred.resolve(null);
			});
		}else{
			deferred.resolve(null);
		}
		return deferred.promise ;
	};
	
	serviceObject.changePassword = function(params){
		var deferred = $q.defer();	
		if(!isNull(params)){
			Util.postDataToServer("changeUserPasswordUrl",params).then(function(data){
				deferred.resolve(data);
			},function(reason){
				deferred.resolve(data);
			});
		}else{
			deferred.resolve(null);
		}
		return deferred.promise ;
	};
	
	return serviceObject;
}]);

