PriceCalculation = function(item,rootScope){
		this.itemObject =  item;
		this.currencyList = null;
		this.unitList = [];
		this.rootScope = rootScope;
		this.selectedUnit = "";
		this.selectedCurrency = null;
		this.dateString = rootScope.priceCalDate;
		this.quantity="1";
		this.itemObject.validQuantity=true;
		this.itemObject.validQuantityPriceCalc=true;
		this.quantityBoxMsg="";
		this.itemCodePropName =  "code";
		this.descCodePropName =  "description";
		this.descCodePropNameProdSearch =  "itemDescription";
		this.unitPropName =  "salesUnit";
		this.pricePropName =  "actualPrice";
//		this.salesUnitsPropName =  "salesUnits";
		this.salesUnitsPropName = "salesUnitsDesc";

		this.buyAllowedPropName =  "isBuyingAllowed";

		this.enableCalculateButton = true;
		this.requestObject = null;
		this.responseObject = null;

	};
	
	PriceCalculation.prototype.init = function(){
		this.currencyList = this.rootScope.getCurrencyList();
		this.selectedCurrency = this.getDefaultCurrency();
		this.unitList = this.getUnits();
		this.setSelectedUnit();
		this.dateString = this.rootScope.priceCalDate;
		this.populateResponseObject();
		this.calculatePrice();
	};
	
	PriceCalculation.prototype.getDefaultCurrency = function(){
		var defCurrency;
//		var webSettings = this.rootScope.webSettings;
//		if(!isNull(webSettings) && !isNull(webSettings['currencyCode'])){
//			defCurrency  = getObjectFromList('code',webSettings['currencyCode'],this.currencyList);
//			if(isNull(defCurrency) && Array.isArray(this.currencyList) &&  this.currencyList.length > 0){
//					defCurrency = this.currencyList[0];
//			}else{
//				defCurrency = webSettings['currencyCode'];
//			}
//		}
		if(Array.isArray(this.currencyList) &&  this.currencyList.length > 0){
			for(var i=0; i<this.currencyList.length; i++){
				
				var currency =  this.currencyList[i];
				if(currency.hasOwnProperty('defaultCurrencyCode')){
					defCurrency = {};
					defCurrency = currency;
					break;
				}
			}
		} 
		if(!isNull(webSettings) && !isNull(webSettings['currencyCode']) && isNull(defCurrency)){
			defCurrency  = getObjectFromList('code',webSettings['currencyCode'],this.currencyList);
			if(isNull(defCurrency) && Array.isArray(this.currencyList) &&  this.currencyList.length > 0){
				defCurrency = {};	
				defCurrency = this.currencyList[0];
			}else{
				defCurrency = {};
				defCurrency = webSettings['currencyCode'];
			}
		}
		return defCurrency;
	};
	
	PriceCalculation.prototype.createRequestObject = function(){
		//PriceCalculationData=[{"itemCode":"BEOCOM 999999","unitCode":"BOX10","currencyCode":"INR","dateString":"2014-09-19","quantity":"1"}]
		this.requestObject = new Object();
		var myStr = this.getItemCode();
		myStr = myStr.replace(/"/g, '\\"');
		this.requestObject.itemCode = encodeURIComponent(myStr);
		this.requestObject.unitCode = this.selectedUnit;
		this.requestObject.currencyCode = returnBlankIfNull(this.selectedCurrency.code);
		this.requestObject.dateString = returnBlankIfNull(this.dateString);
		if(isNull(this.itemObject.newQuantity) || this.itemObject.newQuantity == ""){
			this.itemObject.newQuantity = "1";
		}
		this.requestObject.quantity = this.itemObject.newQuantity;
		return this.requestObject;
	};
	
	PriceCalculation.prototype.populateResponseObject = function(){
		//PriceCalculationData=[{"itemCode":"BEOCOM 999999","unitCode":"BOX10","currencyCode":"INR","dateString":"2014-09-19","quantity":"1"}]
		this.responseObject = new Object();
		this.responseObject.price = "";
		this.responseObject.unit = "";
		this.responseObject.discountAmount = "";
		this.responseObject.netValue = "";
		this.responseObject.grossValue = "";
		this.responseObject.discountPercent = "";
		//currencyCode is not part of response, just to display currencyCode after WS response
		this.responseObject.currencyCode="";
		return this.responseObject;
	};
	
	PriceCalculation.prototype.isBuyAllowed = function(){
		var isBuyAllowed = false;
		if(!isNull(this.itemObject)){
			isBuyAllowed = this.itemObject[this.buyAllowedPropName];
			if(isNull(isBuyAllowed)){
				isBuyAllowed = false;
			}
		}
		return isBuyAllowed;
	};
	
	PriceCalculation.prototype.getParam = function(){
		var reqObj =  this.createRequestObject();
		var param = "PriceCalculationData=" +JSON.stringify([reqObj]);
		return param;
	};
	
	PriceCalculation.prototype.getItemCode = function(){
		var itemCode = null;
		if(!isNull(this.itemObject)){
			itemCode = this.itemObject[this.itemCodePropName];
		}
		return itemCode;
	};
	
	PriceCalculation.prototype.getValidUnit = function(unitValue){
		var disableListButton = false;
		if(!isNull(unitValue) && unitValue != ""){
			disableListButton = false;
		}else{
			disableListButton = true;
		}
		return disableListButton;
	};
		
	PriceCalculation.prototype.getDesc = function(){
		var desc = null;
		if(!isNull(this.itemObject)){
			desc = this.itemObject[this.descCodePropName];
			
			if(desc==undefined){
				desc = this.itemObject[this.descCodePropNameProdSearch];
			}
		}
		return desc;
	};
	
	PriceCalculation.prototype.getUnits = function(){
		var units = [];
		if(!isNull(this.itemObject)){
			units = this.itemObject[this.salesUnitsPropName];
		}
		return units;
	};
	
	PriceCalculation.prototype.setSelectedUnit = function(){
		if(!isNull(this.itemObject)){
			this.selectedUnit = this.itemObject[this.unitPropName];
		}
	};
	
	PriceCalculation.prototype.getPrice = function(){
		var price = "";
		if(!isNull(this.responseObject)){
			//this.selectedUnit = this.itemObject[this.unitPropName];
		}else{
			price = this.itemObject[this.pricePropName];
		}
		return price;
	};
	
	PriceCalculation.prototype.calculatePrice = function(){
		
		if(this.itemObject.validQuantityPriceCalc){
			this.enableCalculateButton = false;
			this.rootScope.getPriceCalculations(this.getParam(), this);
		}
	};
	
	PriceCalculation.prototype.setResponse = function(responseObject){
		this.enableCalculateButton = true;
		if(Array.isArray(responseObject) && responseObject.length > 0 ){
			copyObjectWithNull (responseObject[0],this.responseObject);
			//this.responseObject.currencyCode = this.requestObject.currencyCode;
		}
	};
	
	PriceCalculation.prototype.buy = function(){
		
		//this.itemObject.newQuantity = this.quantity;
		this.itemObject.newUnit = this.selectedUnit;
		this.itemObject.newDelDate = this.dateString;
		
		buyObj = new Buy([this.itemObject]);
		buyObj.setPropNameItemCode(this.itemCodePropName);
		buyObj.setPropNameOrdered('newQuantity');
		buyObj.setPropNameUnit('newUnit');
		buyObj.setPropNameDelDate('newDelDate');
		
		this.buyObj = buyObj;
		if(this.itemObject.validQuantityPriceCalc){
			this.rootScope.buyMultipleItems(this.itemObject,this.buyObj);
		}
	};
	
	PriceCalculation.prototype.addItemToCurrentList = function(){
		//this.itemObject.newQuantity = this.quantity;
		this.itemObject.newUnit = this.selectedUnit;
		this.itemObject.newDelDate = this.dateString;
		
		buyObj = new Buy([this.itemObject]);
		buyObj.setPropNameItemCode(this.itemCodePropName);
		buyObj.setPropNameOrdered('newQuantity');
		buyObj.setPropNameUnit('newUnit');
		buyObj.setPropNameDelDate('newDelDate');
		
		this.buyObj = buyObj;
		this.rootScope.addMultipleItemsToList(this.itemObject,this.buyObj);
//		if(this.itemObject.validQuantity){
//			this.rootScope.addMultipleItemsToList(this.itemObject,this.buyObj);
//		}
	};