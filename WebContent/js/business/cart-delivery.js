function CartDelivery(DeliveryInfo,webSettingsDel)
{
	this.webSettingsDel = webSettingsDel;
	this.deliveryInfo = DeliveryInfo;
	this.UIMessageKey=null;
	this.orderText = null;
	this.messageCode = "";	
	this.StateList = [];
	this.CountryList = [];
	this.CountyList = [];
	this.selectedState = {};
	this.mannerOfTransportList = [];
	this.selectedTransport = {};
	this.selectedCountry = {};
	this.selectedCounty = {};	
	this.selectedAddress = {};
	this.isCompleteDeliveryFlag = false;	
	this.sendEmailOnDispatch = false;	
	this.sendOrderEmailOnDispatch = false;
	this.sendInvoiceEmailOnDispatch = false;
	this.orderTextReference = null;
	this.deliveryAddress = {};
	this.billingAddress = {};
	this.defaultAddress = {};
	this.isBillingAddressDifferent = false;
	this.paymentMethod = {};
	this.orderReference = {};
	this.goodsMark = "";
	this.orderFeeWebText = "";
	this.lineCount = 0;
	this.ErrOnControl = {};
	//this.customerDeliveryAddressList = DeliveryInfo.customerDeliveryAddressList;
	//this.addressList = deepCopy(this.customerDeliveryAddressList);
	this.isFieldsEditedByUser = false;
	this.creationDate = "";
	this.completeDelivery = "";
	this.init();
	
};

CartDelivery.prototype.init = function(){
	var deliveryInfo = this.deliveryInfo;
	if(!isNull(deliveryInfo)){	
		this.customerDeliveryAddressList = deliveryInfo.customerDeliveryAddressList;
		this.addressList = deepCopy(this.customerDeliveryAddressList);
		this.paymentMethod.name = "Invoice";
		this.paymentMethod.type = "invoice";
		this.currentOrder = deliveryInfo.currentOrder;	
		this.orderLines = deliveryInfo.orderLines;	
		this.emailOrderDispatch = returnBlankIfNull(deliveryInfo.emailOrderDispatch);
		this.emailInvoiceDispatch = returnBlankIfNull(deliveryInfo.emailInvoiceDispatch);
		this.lineCount = deliveryInfo.lineCount;
		this.nsOrderValueInfo = deliveryInfo.nsOrderValueInfo;
		if(isNull(this.nsOrderValueInfo)){
			this.nsOrderValueInfo = {};
		}
		if(isNull(this.nsOrderValueInfo.emDisCount)){
			this.nsOrderValueInfo.emDisCount="0";
		}
		this.addressNamesList = deliveryInfo.listOfDeliveryAddressNames;
		this.emailConfirmDispatch = returnBlankIfNull(deliveryInfo.emailConfirmDispatch);
		this.setDeliveryFlags();		
		this.updateAddressList();		
		this.defaultAddressIndex = deliveryInfo.defaultAddressNumber - 1;
		this.setAddress(deliveryInfo);
		this.orderText= deliveryInfo.orderText;	
		this.orderFeeWebText= deliveryInfo.orderFeeWebText;
		this.setOrderReference(deliveryInfo);
		this.isFieldsEditedByUser = false;
		this.creationDate = deliveryInfo.currentOrder.orderDate.year + "/" +
		deliveryInfo.currentOrder.orderDate.month + "/" +
		deliveryInfo.currentOrder.orderDate.dayOfMonth;//year/month/date
		this.completeDelivery = deliveryInfo.currentOrder.isCompleteDelivery;
	}
};
CartDelivery.prototype.setDeliveryInfoResponse = function(data){
//	this.messageCode = data;
	var errorMsg = null;
	var errorParam = [];
	if(!isNull(data) && !isNull(data.deliveryDetailsBean))
	{
		this.setWarningMsg(data.deliveryDetailsBean);
		switch(data.deliveryDetailsBean.saveDelInfoCode){
		case 4411 :
			//errorMsg = null;
			errorMsg = 'MSG_DATA_SAVED_SUCCESSFULLY';
			this.orderText = this.getFreeText();
			this.isSavedSuccessfully = true;
			this.deliveryInfo = data.temporaryOrderData;
			this.init();
			break;
		case 4412 :
			errorMsg = 'MSG_DELIVERY_NOT_ALLOWED_WITH_COMPLETE_DELIVERY';
			errorParam = [];
			this.isSavedSuccessfully = false;
			break;
		case 0 :
			errorMsg = 'MSG_UNEXPECTED_ERROR';
			errorParam = [];
			this.isSavedSuccessfully = false;
			break;
		default :
			errorMsg = null;
			errorParam = [];
			this.isSavedSuccessfully = false;
		};
	}
	
	this.UIMessageKey = translate.getTranslation(errorMsg,errorParam);
};

CartDelivery.prototype.setWarningMsg= function(deliveryDetailsBean){
	if(!isNull(deliveryDetailsBean)){
		this.deliveryAddressChangeMessage = deliveryDetailsBean.deliveryAddressChangeMessage;
		this.motChangeMessage = deliveryDetailsBean.motChangeMessage;
		this.delNotAllowedWithCompleteDel = deliveryDetailsBean.delNotAllowedWithCompleteDel;
	}else{
		
	}
};

CartDelivery.prototype.showDeliveryAddressChangeMessage= function(){
	if(!isNull(this.deliveryAddressChangeMessage)){
		return true;
	}else{
		return false;
	}	
};

CartDelivery.prototype.showMotChangeMessage= function(){
	if(!isNull(this.motChangeMessage)){
		return true;
	}else{
		return false;
	}	
};

CartDelivery.prototype.showDelNotAllowedWithCompleteDel= function(){
	if(!isNull(this.delNotAllowedWithCompleteDel)){
		return true;
	}else{
		return false;
	}	
};

CartDelivery.prototype.setOrderReference = function(deliveryInfo){
	if(!isNull(deliveryInfo.currentOrder)){
		this.orderReference.yourOrderNumber = deliveryInfo.currentOrder.customerOrderNumber;
		this.orderReference.yourReference = deliveryInfo.currentOrder.yourReference;
		if(!isNull(deliveryInfo.billingAddress))
		this.orderReference.ourReference = deliveryInfo.billingAddress.ourReference;
		else
			this.orderReference.ourReference = "";
		//NST-2205 Free text is no longer saved
		this.orderReference.orderText = deliveryInfo.currentOrder.orderText;
		//*END NST-2205
		this.goodsMark = deliveryInfo.currentOrder.shipmentMark;
	} else{
		this.orderReference.yourOrderNumber = "";
		this.orderReference.yourReference = "";
		if(!isNull(deliveryInfo.billingAddress))
			this.orderReference.ourReference = deliveryInfo.billingAddress.ourReference;
			else
				this.orderReference.ourReference = "";
		//NST-2205 Free text is no longer saved
		this.orderReference.orderText ="";
		//*END NST-2205
		this.goodsMark = "";
		
	}
	this.setFreeText(this.orderText);
	
};

CartDelivery.prototype.onDataChange = function (){
	this.isFieldsEditedByUser = true;
};

CartDelivery.prototype.getFreeText = function(){
	return this.orderReference.freeText;
};

CartDelivery.prototype.setFreeText = function(orderText){
	this.orderReference.freeText = orderText;	
};

CartDelivery.prototype.getDeliveryObject = function(){
	var deliveryAddressInfo = {};
	var deliveryInfo = this;
	var deliveryAddress = deliveryInfo.deliveryAddress;
	deliveryAddressInfo.name = returnBlankIfNull(deliveryAddress.name);
	deliveryAddressInfo.addressName = returnBlankIfNull(deliveryAddress.addressName);
	deliveryAddressInfo.adressCode = returnBlankIfNull(deliveryAddress.addressCode);
	deliveryAddressInfo.addressLine1 = returnBlankIfNull(deliveryAddress.address1);
	deliveryAddressInfo.addressLine2 = returnBlankIfNull(deliveryAddress.address2);
	deliveryAddressInfo.addressLine3 = returnBlankIfNull(deliveryAddress.address3);
	deliveryAddressInfo.adressLine4 = returnBlankIfNull(deliveryAddress.address4);
	deliveryAddressInfo.postalCode = returnBlankIfNull(deliveryAddress.postalCode);
	deliveryAddressInfo.countryCode = returnBlankIfNull(deliveryAddress.countryCode);
	deliveryAddressInfo.stateProvCode = returnBlankIfNull(deliveryAddress.stateProvCode);
	deliveryAddressInfo.countyCode = returnBlankIfNull(deliveryAddress.countyCode);
	deliveryAddressInfo.isCompleteDelivery = this.getCompleteDeliveryFlagValue(deliveryInfo.isCompleteDeliveryFlag);
	deliveryAddressInfo.mannerOfTransportCode = returnBlankIfNull(deliveryAddress.mannerOfTransportCode);
	deliveryAddressInfo.mannerOfTransportDesc = returnBlankIfNull(deliveryAddress.mannerOfTransportDesc);
	deliveryAddressInfo.emailConfirmDispatch = returnBlankIfNull(deliveryInfo.emailConfirmDispatch);
	deliveryAddressInfo.emailOrderDispatch = returnBlankIfNull(deliveryInfo.emailOrderDispatch);
	deliveryAddressInfo.emailOrderDispCheckBox = returnBlankIfNull(deliveryInfo.sendOrderEmailOnDispatch);
	deliveryAddressInfo.emailInvoiceDispCheckBox = returnBlankIfNull(deliveryInfo.sendInvoiceEmailOnDispatch);
	deliveryAddressInfo.yourOrder = returnBlankIfNull(deliveryInfo.orderReference.yourOrderNumber);
	deliveryAddressInfo.yourReference = returnBlankIfNull(deliveryInfo.orderReference.yourReference);
	//NST-2205 Free text is no longer saved
	deliveryAddressInfo.orderText = returnBlankIfNull(deliveryInfo.orderReference.orderText);
	//*END NST-2205
	deliveryAddressInfo.shipmentMark = returnBlankIfNull(deliveryInfo.goodsMark);
	if(this.isFieldsEditedByUser){
		deliveryAddressInfo.addressNumber=999;
	}else{
		deliveryAddressInfo.addressNumber = deliveryAddress.AddressNumber;
	}
	deliveryAddressInfo.emailConfirmDispCheckBox = this.sendEmailOnDispatch ? 'YES' : 'NO';
	return deliveryAddressInfo;
};

CartDelivery.prototype.getDeliveryParam = function(deliveryInfo){
	var param = "";
	var paramObject = this.getDeliveryObject();
	paramObject= encodeObjectData(paramObject);
	param =  "SaveDeliveryInformation=["+JSON.stringify(paramObject)+"]";
	return param;
};

CartDelivery.prototype.changeAddress = function(){
	this.clearError();
	this.updateAddress();
};


CartDelivery.prototype.setAddress = function(deliveryInfo){
	this.setDefaultAddress();
	this.selectedAddress = this.defaultAddress;
	this.deliveryAddress = deliveryInfo.ivDispatchAddress;
	this.dispatchAddressCode =	deliveryInfo.ivDispatchAddressCode;
	this.billingAddress = this.getSelectedBillingAddress(this.defaultAddressIndex);
	
};

CartDelivery.prototype.setSelectedAddress = function (){
	this.selectedAddress = getObjectFromList('addressCode',this.selectedAddress.addressCode,this.customerDeliveryAddressList);
};

CartDelivery.prototype.updateSelectedAddress = function(){
	var selectedAddressIndex = this.selectedAddress.addressCode - 1;
	this.updateBillingAddress(selectedAddressIndex);
	this.updateDeliveryAddress();
};

CartDelivery.prototype.getSelectedBillingAddress = function(index)
{
	return this.deliveryInfo.billingAddress;
	//return this.addressList[index];

};
CartDelivery.prototype.updateBillingAddress = function(index){
	//	this.billingAddress = this.getSelectedBillingAddress(index);
		this.billingAddress = this.getSelectedBillingAddress(this.defaultAddressIndex);
			
};
CartDelivery.prototype.updateDeliveryAddress = function(){
		this.deliveryAddress = this.selectedAddress;
			
};
CartDelivery.prototype.setDefaultAddress = function(){
	var addressList = this.customerDeliveryAddressList;
	this.defaultAddress = addressList[this.defaultAddressIndex];
	if(isNull(this.defaultAddress)){
		for(var i=0;(i<addressList.length); i++){
			if(addressList[i].AddressNumber == (this.defaultAddressIndex +1)){
				this.defaultAddress = addressList[i];
				break;
			}
		}
	}
};

CartDelivery.prototype.changeSelectedAddress = function(){
	this.setTransport();
	this.setCountry();
	this.updateSelectedAddress();
	
	
};

CartDelivery.prototype.clearError = function(){
	this.deliveryAddressChangeMessage = null;
	this.motChangeMessage = null;
	this.delNotAllowedWithCompleteDel = null;
	this.UIMessageKey=null;
};


CartDelivery.prototype.setMannerOfTransportList = function(transportMannerList){
	this.mannerOfTransportList= transportMannerList;	
	this.setTransport();
	
};

CartDelivery.prototype.setTransport = function(){
	this.selectedTransport = getObjectFromList('code',this.selectedAddress.mannerOfTransportCode,this.mannerOfTransportList);
	this.setTransportForSelectedAddress();
};

CartDelivery.prototype.setTransportForSelectedAddress = function(){
	this.selectedAddress.mannerOfTransportCode = this.selectedTransport.code;
	this.selectedAddress.mannerOfTransportDesc = this.selectedTransport.description;
	};
	
CartDelivery.prototype.updateAddress = function(){
	this.setCountry();
	this.setTransport();
	this.setState();
	this.setCounty();
	this.setSelectedAddress();
	this.updateSelectedAddress();
	
};
CartDelivery.prototype.setCountryList = function(countryList){
	this.CountryList= countryList;	
	this.setCountry();
	
};

CartDelivery.prototype.setCountry = function(){
	this.selectedCountry = getObjectFromList('code',this.selectedAddress.countryCode,this.CountryList); 
	this.setCountryForSelectedAddress();
	
};
CartDelivery.prototype.setCountryForSelectedAddress = function(){
	this.selectedAddress.countryCode = this.selectedCountry.code;
	this.selectedAddress.countryDesc = this.selectedCountry.description;
	};

CartDelivery.prototype.setTransportForSelectedAddress = function(){
	this.selectedAddress.mannerOfTransportCode = this.selectedTransport.code;
	this.selectedAddress.mannerOfTransportDesc = this.selectedTransport.description;
	
	};


CartDelivery.prototype.setCountyList = function(countyList){
	this.CountyList= countyList;	
	this.setCounty();
};
CartDelivery.prototype.setCounty = function(){
	this.selectedCounty = getObjectFromList('countyCode',this.selectedAddress.countyCode,this.CountyList);	
	this.setCountyForSelectedState();
};
CartDelivery.prototype.setCountyForSelectedState = function(){
	if(!isNull(this.selectedCounty)){
		this.selectedAddress.countyCode = this.selectedCounty.countyCode;
		this.selectedAddress.countyDesc = this.selectedCounty.countyDesc;
	}
	else{
		this.selectedAddress.countyCode = null;
		this.selectedAddress.countyDesc = null;
	}
};
CartDelivery.prototype.setStateList = function(stateList){
	this.StateList= stateList;	
	this.setState();	
};

CartDelivery.prototype.setState = function(){
		this.selectedState = getObjectFromList('stateProvincecode',this.selectedAddress.stateProvCode,this.StateList);	
		this.setStateForSelectedCountry();
};
CartDelivery.prototype.setStateForSelectedCountry = function(){
	if(!isNull(this.selectedState)){
		this.selectedAddress.stateProvCode = this.selectedState.stateProvincecode;
		this.selectedAddress.stateName = this.selectedState.stateProvinceDesc;
	}
	else{
		this.selectedAddress.stateProvCode = null;
		this.selectedAddress.stateName = null;
	}
};

CartDelivery.prototype.setDeliveryFlags = function(){
	var deliveryInfo = this.deliveryInfo;
	this.allowDispatchAddressOverride = deliveryInfo.allowDispatchAddressOverride;
	this.allowChangeMannerOfTransport = deliveryInfo.allowChangeMannerOfTransport;
	this.emailDispatchOverrideAllowed = deliveryInfo.emailDispatchOverrideAllowed;
	this.emailInvoiceOverrideAllowed = deliveryInfo.emailInvoiceOverrideAllowed;
	this.emailOrderOverrideAllowed = deliveryInfo.emailOrderOverrideAllowed;
	this.isShowEmailConfirmDispatch = deliveryInfo.isShowEmailConfirmDispatch;
	this.sendEmailOnDispatch = deliveryInfo.isShowEmailConfirmDispChecked;
	this.isShowEmailOrderDispatch = deliveryInfo.isShowEmailOrderDispatch;
	this.isShowEmailInvoiceDispatch = deliveryInfo.isShowEmailInvoiceDispatch;
	this.sendInvoiceEmailOnDispatch = deliveryInfo.isShowEmailInvoiceDispChecked;
	this.sendOrderEmailOnDispatch = deliveryInfo.isShowEmailOrderDispChecked;
	this.isDeliveryAddressModified =  deliveryInfo.isDeliveryAddressModified;	
	this.isCompleteDeliveryFlag= deliveryInfo.ivCompleteDelivery;
	
};

CartDelivery.prototype.getCompleteDeliveryFlagValue = function(flagValue){
	var value =  "";
	if(flagValue){
		value = "YES" ;		
	}
	return value;
};

CartDelivery.prototype.updateAddressList=function(){
	var addressList = this.customerDeliveryAddressList;
	if(!isNull(addressList)){
		for(var i=0;i<addressList.length;i++){
			 this.customerDeliveryAddressList[i] = this.getAddressObject(addressList[i]);
		}
	}
};
CartDelivery.prototype.getAddressObject=function(address)
{
	var obj = new Object();
	obj.code = address.code;
	obj.name = address.name;
	obj.AddressNumber = address.AddressNumber;
	obj.address1 = address.address1;
	obj.address2 = address.address2;
	obj.address3 = address.address3;
	obj.address4 = address.address4;	
	obj.countryCode = address.countryCode;
	obj.countyCode = address.countyCode;
	obj.countyDesc = address.countyDesc;;
	obj.postalCode = address.postalCode;
	obj.stateProvCode = address.stateProvCode;
	obj.stateName = "";
	obj.phoneNumber = address.phoneNumber;	
	obj.mannerOfTransportDesc = address.mannerOfTransportDesc;	
	obj.mannerOfTransportCode = address.mannerOfTransportCode;
	obj.countryDesc = address.countryDesc;
	obj.stateProvDesc = address.stateProvDesc;
	obj.currencyCode = address.currencyCode;
	obj.termsOfDeliveryCode = address.termsOfDeliveryCode;
	obj.termsOfDeliveryCodeDesc = address.termsOfDeliveryCodeDesc;	
	obj.ourReference = address.ourReference;
	obj.creditLimit = address.creditLimit;
	obj.creditBalance = address.creditBalance;
	obj.debtorCode = address.debtorCode;
	obj.addressCode = address.addressCode;
	obj.addressName = address.addressName;
	obj.addressDesc = address.addressDesc;
	obj.displayAddressName = this.getAddressDisplayName(address);
	return obj;
};

CartDelivery.prototype.getAddressDisplayName=function(address){
	var displayName =  address.addressName+","+address.address4;
	return displayName;
};


CartDelivery.prototype.validateDeliveryUpdates = function(CartDelivery)
{
	var isValidRequest = true;
	var errorMsg = null;
	var errorParam = [];
	if(isEmptyString(CartDelivery.selectedAddress.addressName ) && this.webSettingsDel.allowDispatchAddressOverride == 'Y'){					
		this.ErrOnControl['addressName'] = true;
		isValidRequest = false;
	}else{
		this.ErrOnControl['addressName'] = false;
	}
	if(isEmptyString(CartDelivery.selectedAddress.address4 ) && this.webSettingsDel.allowDispatchAddressOverride == 'Y'){					
		this.ErrOnControl['city'] = true;
		isValidRequest = false;
	}else{
		this.ErrOnControl['city'] = false;
	}
	if(isEmptyString(CartDelivery.selectedAddress.address1 ) && this.webSettingsDel.allowDispatchAddressOverride == 'Y'){					
		this.ErrOnControl['address1'] = true;
		isValidRequest = false;
	}else{
		this.ErrOnControl['address1'] = false;
	}
	if(isEmptyString(CartDelivery.selectedAddress.postalCode ) && this.webSettingsDel.allowDispatchAddressOverride == 'Y'){					
		this.ErrOnControl['postalCode'] = true;
		isValidRequest = false;
	}else{
		this.ErrOnControl['postalCode'] = false;
	}
	if(isEmptyString(CartDelivery.selectedCountry.code ) && this.webSettingsDel.allowDispatchAddressOverride == 'Y'){					
		this.ErrOnControl['country'] = true;
		isValidRequest = false;
	}else{
		this.ErrOnControl['country'] = false;
	}
	if(CartDelivery.isShowEmailConfirmDispatch){
		if(isEmptyString(CartDelivery.emailConfirmDispatch) && this.webSettingsDel.allowDispatchAddressOverride == 'Y'){					
			this.ErrOnControl['dispatchEmail'] = true;
			isValidRequest = false;
		}else{
			this.ErrOnControl['dispatchEmail'] = false;
		}
	}else{
		this.ErrOnControl['dispatchEmail'] = false;
	}
	if(!isValidRequest){
		errorMsg = 'MSP_MANDATORY_DATA_MISSING';
		errorParam = ['*'];
	}
	this.UIMessageKey = translate.getTranslation(errorMsg,errorParam);
	return isValidRequest;
};