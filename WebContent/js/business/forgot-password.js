function ForgotPassword()
	{
		this.userId = "";
		this.emailId ="";
		this.errorMsg = null;
		this.successMsg =null;
	};
	ForgotPassword.prototype.getParams =function(){
        var params = [] ;
        if(!isEmptyString(this.userId)){
               params.push("UserID");
               params.push(this.userId);
        }
        if(!isEmptyString(this.emailId)){
               params.push("Email");
               params.push(this.emailId);
        }
        return params;
 };
 
 ForgotPassword.prototype.isValidRequest = function(){
        var isValid = false;
        if(!isEmptyString(this.userId) || !isEmptyString(this.emailId)){
               isValid = true;
        }
        return isValid;
 };

//	ForgotPassword.prototype.getParams =function(){
//		var params = [] ;
//		if(!isEmptyString(this.userId)){
//			params.push("UserID");
//			params.push(this.userId);
//		}
//		if(this.isValidEmailId()){
//			params.push("Email");
//			params.push(this.emailId);
//		}
//		return params;
//	};
//	
//	ForgotPassword.prototype.isValidRequest = function(){
//		var isValid = false;
//		if(!isEmptyString(this.userId) || this.isValidEmailId()){
//			isValid = true;
//		}
//		return isValid;
//	};
	
	ForgotPassword.prototype.isValidEmailId = function(){
		var email = new Email(this.emailId)  ;
		return email.isValidEmailAddress();
	};
	
	ForgotPassword.prototype.setErrorMsg = function(errorMsg){
		if(isNull(errorMsg) || isEmptyString(errorMsg)){
			this.errorMsg =  null;
		}else{
			this.errorMsg =  errorMsg; 
			this.successMsg = null;
		}
	};
	
	ForgotPassword.prototype.setSuccessMsg = function(successMsg){
		if(isNull(successMsg) || isEmptyString(successMsg)){
			this.successMsg =  null;
		}else{
			this.successMsg =  successMsg;
			this.errorMsg = null;
			this.userId = "";
			this.emailId ="";
		}
	};
	
	ForgotPassword.prototype.showErrorMsg = function(){
		if(!isNull(this.errorMsg) ){
			return true ;
		}else{
			return false ;
		}
	};
	
	ForgotPassword.prototype.showSuccessMsg = function(){
		if(!isNull(this.successMsg) ){
			return true ;
		}else{
			return false ;
		}
	};