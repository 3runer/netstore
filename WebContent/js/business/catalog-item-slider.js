function CatalogItemSlider (itemList,groupSize) {
	this.itemWidth = 155; // item width in px
    this.itemList = itemList;
    this.currentIndex=0;
    this.masterIndex = 0;
    // currentGroup must start from 1 
    this.currentGroup=1;
    this.calculateGroupsize();
    this.reachedLastPage = false;
    this.currentGroupItems = this.makeCurrentGroup();
}

CatalogItemSlider.prototype.calculateGroupsize = function(){
	var maxWidth = this.getMaxAvailableWidth();
	this.groupSize=Math.floor(maxWidth/this.itemWidth);
	if(isNull(this.groupSize) || this.groupSize<=0){
		this.groupSize=1;
	}
};

CatalogItemSlider.prototype.appendItems = function(itemList){
	if(Array.isArray(itemList) && itemList.length > 0){
		 appendItemsToList(itemList, this.itemList) ;
	}else{
		this.reachedLastPage = true;
	}
};


CatalogItemSlider.prototype.setMasterIndex = function(masterIndex){
	if(masterIndex>=0 && masterIndex<this.itemList.length){
		//this.masterIndex = masterIndex;
		var groupIndex = Math.floor((masterIndex / this.groupSize));
		if(groupIndex<=0){
			this.currentGroup =1;
		}else{
			this.currentGroup = groupIndex+1;
		}
		//var imageIndex = masterIndex % this.groupSize;
		this.currentIndex = masterIndex;
	}
};

CatalogItemSlider.prototype.setMatrixItemList = function(itemList) {
	if (!isNull(itemList) && Array.isArray(itemList)){
		for(var i = 0; i< itemList.length;i++)
		{
			if(!this.isImageExist(itemList[i]))
			{
				this.itemList.push(itemList[i]);
			}
		}
	}
};

CatalogItemSlider.prototype.getItemIndex = function(image)
{
	var imageIndex = 0;
	var itemList = this.itemList;
	 for(var i = 0; i< itemList.length;i++)
		 {
		 	if(image == itemList[i])
		 		{
		 			imageIndex = i;
		 			break;
		 		}
		 }
	return imageIndex;
};

CatalogItemSlider.prototype.isImageExist = function(image)
{
	var imageAvailable = false;
	var itemList = this.itemList;
	 for(var i = 0; i< itemList.length;i++)
		 {
		 	if(image == itemList[i])
		 		{
		 			imageAvailable = true;
		 			break;
		 		}
		 }
	 return imageAvailable;
};


CatalogItemSlider.prototype.setItemList = function(itemList) {
	if (!isNull(itemList) && Array.isArray(itemList)){
		 this.itemList = itemList;
	}
};

CatalogItemSlider.prototype.getDefaultImage = function() {
	return './images/ImageIcon.png';
};

CatalogItemSlider.prototype.hasItems = function() {
	if (isNull(this.itemList) || this.itemList.length == 0 ){
		 return false;
	}else{
		return true;
	}
};

CatalogItemSlider.prototype.setCurrentIndex = function(index) {
	//set index according to currentGroup number
	if(this.currentGroup>1){
		var endIndex = (this.currentGroup * this.groupSize)-1;
		var startIndex = endIndex - this.groupSize + 1;
		this.currentIndex=startIndex+index;
	}else{
		this.currentIndex=index;
	}
	if(this.currentIndex >= this.itemList.length || this.currentIndex < 0){
		//console.log("CatalogItemSlider.prototype.setCurrentIndex() : Index out of range= " + this.currentIndex );
	}
	//this.updateMasterIndex();
	return this.currentIndex;
};

CatalogItemSlider.prototype.getCurrentIndex = function() {
	return  this.currentIndex;
};

CatalogItemSlider.prototype.getNumberOfGroups = function(){
	var maxNoOfGrps = Math.ceil((this.itemList.length/ this.groupSize));
	return maxNoOfGrps;
};

CatalogItemSlider.prototype.incrementGroupNumber = function(){
	var tempCurrGrpNum = this.currentGroup;
	if(this.itemList.length>=1 && this.itemList.length> this.groupSize){
		var maxNoOfGrps = this.getNumberOfGroups();
		if(this.currentGroup<maxNoOfGrps){
			this.currentGroup++;
		}
	}else{
		this.currentGroup =0;
	}
	if(tempCurrGrpNum!=this.currentGroup){
		isGrpNumChanged = true;
	}
	return isGrpNumChanged;
};

CatalogItemSlider.prototype.decrementGroupNumber= function(){
	var tempCurrGrpNum = this.currentGroup;
	var isGrpNumChanged = false;
	if(this.itemList.length>=1 ){
		if(this.itemList.length> this.groupSize){
			if(this.currentGroup>1 ){
				this.currentGroup--;
			}
		}else{
			this.currentGroup=1;
		}
	}else{
		this.currentGroup =0;
	}
	if(tempCurrGrpNum!=this.currentGroup){
		isGrpNumChanged = true;
	}
	return isGrpNumChanged;
};

CatalogItemSlider.prototype.nextGroup = function() {
	var isGrpNumChanged = this.incrementGroupNumber();
	if(isGrpNumChanged){
		this.makeCurrentGroup();
	}
	//console.log("CatalogItemSlider.prototype.nextGroup() : currentGroup= " + this.currentGroup );
	return this.getCurrentGroup();
};

CatalogItemSlider.prototype.getCurrentGroup = function() {
	return this.currentGroupItems;
};

CatalogItemSlider.prototype.makeCurrentGroup = function() {
	this.currentGroupItems = [];
	if(this.itemList.length>this.groupSize){
		var endIndex = (this.currentGroup * this.groupSize)-1;
		var startIndex = endIndex - this.groupSize + 1;
		for(var i= startIndex; i<this.itemList.length && i<=endIndex;i++ ){
			this.currentGroupItems.push(this.itemList[i]);
		};
	}else{
		this.currentGroupItems = this.itemList;
	}
	return this.currentGroupItems;
};

CatalogItemSlider.prototype.isLoadNextPage = function() {
	var flag = false;
	if (this.currentGroup >= this.getNumberOfGroups()){
		flag =  true;
	}
	if(!this.reachedLastPage){
		flag = true;
	}
	return flag;
};

CatalogItemSlider.prototype.showNext = function() {
	var flag = false;
	if(this.currentGroup < this.getNumberOfGroups()){
		flag = true;
	}else if(this.currentGroup == this.getNumberOfGroups() && !this.reachedLastPage){
		flag = false;
	}
	return flag;
};

CatalogItemSlider.prototype.showPrevious = function() {
	var flag = false;
	if(this.currentGroup>1){
		flag = true;
	}
	return flag;
};

CatalogItemSlider.prototype.previousGroup = function() {
	var isGrpNumChanged = this.decrementGroupNumber();
	if(isGrpNumChanged){
		this.makeCurrentGroup();
	}
	//console.log("CatalogItemSlider.prototype.previousGroup() : currentGroup= " + this.currentGroup );
	return this.getCurrentGroup();
};

CatalogItemSlider.prototype.getAllImages = function() {
	return this.itemList;
};

	
CatalogItemSlider.prototype.getMaxAvailableWidth = function(){
	var width = 200; //initialized with some default width for fallback
	var ele =  document.getElementById("bestOfferDiv");
	if(isNull(ele)){
		ele =  document.getElementById("mostPopularDiv");
	}
	if(!isNull(ele)){
		width = ele.offsetWidth;
		//minus width required for left and right arrows
		width = width-60;
		//console.log("CatalogItemSlider getMaxAvailableWidth = "+width);
	}

	return width;
};