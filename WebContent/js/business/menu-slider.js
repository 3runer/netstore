function MenuSlider(menuList){
		this.menuList = menuList;
		this.canvasContext = null;
		this.currentGroupIndex = 0 ;
		
		this.menuGroups = [];
		
		this.initCanvasContext();
		//this.maxWidth = this.getMaxAvailableWidth();
		//this.createGroups();
	};
	
	MenuSlider.prototype.initCanvasContext = function(){
		var canvas = document.getElementById('myCanvas');
		var context = canvas.getContext('2d');
		
		//element id - need to used to get font size and name
		/*var element = document.getElementById('widthNav'),
	    style = window.getComputedStyle(element),
	    font = style.getPropertyValue('font-family');
		
	    fontSize = style.getPropertyValue('font-size');
	    
	    context.font = fontSize+ " " +font;*/
	    this.canvasContext = context;
	};
	
	MenuSlider.prototype.getTextWidth = function(text){
		var metrics = this.canvasContext.measureText(text);
		var width = metrics.width;
		//add 1 px as buffer space
		return width+1;
	};
	
	MenuSlider.prototype.getMaxAvailableWidth = function(){
		var width = document.getElementById("widthNav").offsetWidth;
		//minus width required for left and right arrows
		width = width-40;
		//console.log("cat menu getMaxAvailableWidth = "+width);
		return width;
	};
	
	MenuSlider.prototype.createGroups = function(){
		var rightLeftPadding = 20;
		var maxTextWidth = 130;
		var minTextWidth = 50;
		if(Array.isArray(this.menuList)){
			var menuGrp = [];
			var menuGrpWidth = 0;
			var menuLenght = 1;
			for(var i=0; i<this.menuList.length;i++){
				var menu = this.menuList[i];
				//menu.description = menu.description + "wwwwwwwwwwwww";
				var textWidth =  this.getTextWidth(menu.description);
				if(textWidth>maxTextWidth){
					textWidth = maxTextWidth;
					
				}
				if(textWidth<minTextWidth){
					textWidth = minTextWidth;
				}
				menuGrpWidth = menuGrpWidth + textWidth + rightLeftPadding;
				
				if(menuLenght<=6){
					menu.comWidth = menuGrpWidth;
					menuGrp.push(menu);
					menuLenght++;
				}else{
					//reached to max limit
					this.menuGroups.push(menuGrp);
					menuGrp = [];
					menuGrpWidth = textWidth + rightLeftPadding;
					menu.comWidth = menuGrpWidth;
					menuLenght = 1;
					menuGrp.push(menu);
				}
			}
			//add last group
			this.menuGroups.push(menuGrp);
			//console.log("menu groups = "+JSON.stringify(this.menuGroups));
		}
	};
	
	MenuSlider.prototype.getCurretGroup = function(){
		return this.menuGroups[this.currentGroupIndex];
	};
	
	MenuSlider.prototype.nextGroup = function(){
		if(this.currentGroupIndex < this.menuGroups.length-1){
			this.currentGroupIndex++;
		}else{
			//cycle it 
			this.currentGroupIndex = 0;
		}
	};
	
	MenuSlider.prototype.previousGroup = function(){
		if(this.currentGroupIndex > 0 ){
			this.currentGroupIndex--;
		}else{
			//cycle it 
			this.currentGroupIndex=this.menuGroups.length-1;
		}
	};
	
	MenuSlider.prototype.showNavigationArrows = function(){
		if(this.menuGroups.length > 1 ){
			return true;
		}else{
			return false;
		}
	};