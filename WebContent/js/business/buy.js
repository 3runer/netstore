function Buy(prdlist){
	    this.prdlist = prdlist ;
	    this.UIErrorKey=null;
	    this.buyList = [];
	    this.propNameItemCode='itemCode';
	    this.propNameOrdered='ordered';
	    this.propNameUnit='unit';
	    this.propNameDelDate='delDate';
	    this.propNameShipmentMarking='shipmentMarking';
	    this.propNameisBuyAllowed='isBuyingAllowed';
	    this.propNameQuotationPrice='quotationPrice';
	};
	
	Buy.prototype.createObject = function(itemCode,ordered,unit,delDate,shipmentMarking){
		var obj = new Object();
		var myStr = itemCode.replace(/"/g, '\\"');
		obj.itemCode=encodeURIComponent(myStr);
		//obj.itemCode=encodeURIComponent(itemCode);
		obj.quantity=ordered;
		obj.unitCode=returnBlankIfNull(unit);
		if(!isNull(delDate)|| delDate==""){
		obj.deliveryDate=returnBlankIfNull(delDate);
		}
		obj.shipmentMark=returnBlankIfNull(shipmentMarking);
		return obj;
	};
	
	Buy.prototype.createObjectQuotation = function(itemCode,ordered,unit,delDate,shipmentMarking,price){
		var obj = new Object();
		var myStr = itemCode.replace(/"/g, '\\"');
		obj.itemCode=encodeURIComponent(myStr);
		obj.quantity=ordered;
		obj.unitCode=returnBlankIfNull(unit);
		if(!isNull(delDate)|| delDate==""){
		obj.deliveryDate=returnBlankIfNull(delDate);
		}
		obj.shipmentMark=returnBlankIfNull(shipmentMarking);
		obj.quoPrice = returnBlankIfNull(price);
		return obj;
	};
	
	Buy.prototype.createShoppingListObject = function(itemCode,unit,ordered,skipInteger){
		var obj = new Object();
		var myStr = itemCode.replace(/"/g, '\\"');
		obj.itemCode=encodeURIComponent(myStr);
		//obj.itemCode=encodeURIComponent(itemCode);
		obj.unitCode=returnBlankIfNull(unit);
		obj.quantity=ordered;
		if(skipInteger == true){
			obj.quantity = ordered.replace(",",".");
		}
		return obj;
	};
	Buy.prototype.setPropNameOrdered= function(propName){
		this.propNameOrdered=propName;
	};
	Buy.prototype.setPropNameItemCode= function(propName){
		this.propNameItemCode=propName;
	};
	Buy.prototype.setPropNameUnit= function(propName){
		this.propNameUnit=propName;
	};
	Buy.prototype.setPropNameDelDate= function(propName){
		this.propNameDelDate=propName;
	};
	Buy.prototype.setPropNameShipmentMarking= function(propName){
		this.propNameShipmentMarking=propName;
	};
	Buy.prototype.setPropNameisBuyAllowed= function(propName){
		this.propNameisBuyAllowed=propName;
	};
	Buy.prototype.setpropNameQuotationPrice= function(propName){
		this.propNameQuotationPrice=propName;
	};
	
	
	Buy.prototype.getBuyEnabledItems = function (list){
		var buyEnabledItems = [];
		for(var i=0; i<list.length;i++ ){
			var item = list[i];
			
			
			if(item.hasOwnProperty(this.propNameOrdered) && item.hasOwnProperty(this.propNameisBuyAllowed)){
				if(!isEmptyString(item[this.propNameOrdered]) && item[this.propNameisBuyAllowed]){
					if(isValidQuantity(item[this.propNameOrdered])){
						buyEnabledItems.push(item);
						item.validQuantity=true;
					}else{
						item.validQuantity=false;
					}
				}
			}
		}
		return buyEnabledItems;
	};
	
	Buy.prototype.getAddToListEnabledItems = function (list){
		var addToListEnabledItems = [];
		for(var i=0; i<list.length;i++ ){
			var item = list[i];
			if(item.hasOwnProperty(this.propNameOrdered)){
				if(!isEmptyString(item[this.propNameOrdered])){
					if(isValidQuantity(item[this.propNameOrdered])){
						addToListEnabledItems.push(item);
						item.validQuantity=true;
					}else{
						item.validQuantity=false;	
					}
				}
			}
		}
		return addToListEnabledItems;
	};
	
	Buy.prototype.validateBuyItem = function(item, skipInteger, maxQuantity){
		var isValid = true;
		this.UIErrorKey = null;
		this.UIErrorParams = [];
		if(item.hasOwnProperty(this.propNameOrdered)){
//			if(isEmptyString(item[this.propNameItemCode] )){
//				this.UIErrorKey = 'MSG_ITEM_NOT_SPECIFIED'; 
//				isValid = false;
//			}
			if(!isEmptyString(item[this.propNameOrdered])){
				//this is a buy item, check for valid quantity
				var quant = item[this.propNameOrdered];
				if(skipInteger == true){
					quant = quant.replace(",",".");
					item[this.propNameOrdered] = quant;
				}
				if(isNaN(quant) || quant<=0 || (skipInteger ? false : !isInteger(item[this.propNameOrdered]))){
					this.UIErrorKey = 'MSG_INVALID_QUANTITY'; 
					isValid = false;
				}else if(!isNull(maxQuantity)){
					if(!isValidQuantity(item[this.propNameOrdered],maxQuantity, skipInteger) || !isValidDecimalsBuy(item, item[this.propNameOrdered],skipInteger)){
						this.UIErrorKey = 'MSG_INVALID_QUANTITY'; 
						isValid = false;
				}
				}
			}else{
				//this is not a buy item
				isValid = false;
			}
		}else{
			//this is not a buy item
			isValid = false;
		}
		return isValid;
	};
	
	Buy.prototype.createBuyList = function(list, skipInteger, maxQuantity){
		//clean buyList before addition
		this.buyList=[];
		//single object list can be passed for individual product buy
		if(isNull(list)){
			list = this.getBuyEnabledItems(this.prdlist);
		}
		//add items which has valid code, and quantity
		for(var i=0; i<list.length;i++ ){
			var item = list[i];
			if(this.validateBuyItem(item, skipInteger, maxQuantity)){
				var newObj = this.createObject(item[this.propNameItemCode],item[this.propNameOrdered],item[this.propNameUnit],
						item[this.propNameDelDate],item[this.propNameShipmentMarking]);
				this.buyList.push(newObj);
			}else{
				this.buyList=[];
				break;
			}
		}
		return this.buyList;
	};

	Buy.prototype.createShoppingList = function(list, skipInteger){

		this.shoppingListItems=[];
		
		if(isNull(list)){
			list = this.getAddToListEnabledItems(this.prdlist);
		}

		for(var i=0; i<list.length;i++ ){
			var item = list[i];
			if(this.validateBuyItem(item, skipInteger)){
				var newObj = this.createShoppingListObject(item[this.propNameItemCode],item[this.propNameUnit],item[this.propNameOrdered],skipInteger);
				this.shoppingListItems.push(newObj);
			}else{
				this.shoppingListItems=[];
				break;
			}
		}
		return this.shoppingListItems;
	};
	
	Buy.prototype.getShoppingListParam = function (item, skipInteger){
		var shoppingListItems;
		if(!isNull(item)){
			var itemList = [];
			itemList.push(item);
			shoppingListItems = this.createShoppingList(itemList, skipInteger);
		}else{
			shoppingListItems = this.createShoppingList(null, skipInteger);
		}
		var requestParam = null;
		if(Array.isArray(shoppingListItems) && shoppingListItems.length>0 )
		{
			requestParam =  "AddItemsToListDetails="+JSON.stringify(shoppingListItems);			
		}
		return requestParam;
	};
	
	
	Buy.prototype.getParam = function (item, skipInteger, maxQuantity){
		var itemBuyList;
		//if item is passed, means its a single buy, put it in an array and pass further
		if(!isNull(item)){
			var itemList = [];
			itemList.push(item);
			itemBuyList = this.createBuyList(itemList, skipInteger,maxQuantity);
		}else{
			itemBuyList = this.createBuyList(null, skipInteger, maxQuantity);
		}
		var requestParam = null;
		if(Array.isArray(itemBuyList) && itemBuyList.length>0 )
		{
			requestParam =  "inputItems="+JSON.stringify(itemBuyList);			
		}
		return requestParam;
	};
	
	Buy.prototype.getParamQuo = function (item, skipInteger){
		var itemBuyList;
		//if item is passed, means its a single buy, put it in an array and pass further
		if(!isNull(item)){
			var itemList = [];
			itemList.push(item);
			itemBuyList = this.createBuyListQuo(itemList, skipInteger);
		}else{
			itemBuyList = this.createBuyListQuo(null, skipInteger);
		}
		var requestParam = null;
		if(Array.isArray(itemBuyList) && itemBuyList.length>0 )
		{
			requestParam =  "inputItems="+JSON.stringify(itemBuyList);			
		}
		return requestParam;
	};
	
	Buy.prototype.createBuyListQuo = function(list, skipInteger){
		
		this.buyList=[];
		if(isNull(list)){
			list = this.getBuyEnabledItems(this.prdlist);
		}
		for(var i=0; i<list.length;i++ ){
			var item = list[i];
			if(this.validateBuyItem(item, skipInteger)){
				var newObj = this.createObjectQuotation(item[this.propNameItemCode],item[this.propNameOrdered],item[this.propNameUnit],
						item[this.propNameDelDate],item[this.propNameShipmentMarking],item[this.propNameQuotationPrice]);
				this.buyList.push(newObj);
			}else{
				this.buyList=[];
				break;
			}
		}
		return this.buyList;
	};