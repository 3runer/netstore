//quick view start
	
	QuickViewProduct = function(item,rootScope){
		this.itemObject =  item;
		this.currencyList = null;
		this.unitList = [];
		this.rootScope = rootScope;
		this.selectedUnit = "";
		this.selectedCurrency = null;
		this.dateString = '';
		this.quantity="1";
		this.itemObject.validQuantity=true;
		this.itemObject.validQuantityPriceCalc=true;
		this.quantityBoxMsg="";
		this.itemCodePropName =  "code";
		this.descCodePropName =  "description";
		this.attentionUrlPropName= "attentionImageURL";
		this.attentionImageShowPropName="isShowAttentionImage";
		this.isRelativePropName="isRelative";
		this.unitPropName =  "salesUnit";
		this.itemDiscountPercentagePropName = "discountPercentage";
		this.discountPricePropName = "discountPrice";
		this.itemWebTextPropName = "itemWebText";
		this.imageUrlPropName = "imageUrl";
		this.pricePropName =  "actualPrice";
		//this.salesUnitsPropName =  "salesUnits";
		this.salesUnitsPropName = "salesUnitsDesc";
		this.buyAllowedPropName =  "isBuyingAllowed";
		this.isMatrixbaseItemPropName = "isMatrixBaseItem";
		// ADM-015
		this.quantityPropName = "quantity";
		// ADM-015 end
		
		this.enableCalculateButton = true;
		this.requestObject = null;
		this.responseObject = null;
		// ADM-020
		this.mailFrom = "";
		this.startShowError = false;
		this.UIErrorKey = null;
		// ADM-020 end
	};
	
	QuickViewProduct.prototype.init = function(){
		this.currencyList = this.rootScope.getCurrencyList();
		this.selectedCurrency = this.getDefaultCurrency();
		this.unitList = this.getUnits();
		this.setSelectedUnit();
		this.dateString = getDateStrYYYYMMDD();

	};
	
	// ADM-020
	QuickViewProduct.prototype.initForItemDetail = function(imageUrl){
		this.init();
		this.itemCodePropName =  "itemCode";
		this.descCodePropName =  "itemDesc";
		this.buyAllowedPropName = "isBuyAllowed";
		this.salesUnitsPropName = "salesUnitDesc";
		this.itemObject.imageUrl = imageUrl;
	};
	
	QuickViewProduct.prototype.getDefaultCurrency = function(){
		var defCurrency = {};
//		var webSettings = this.rootScope.webSettings;
//		if(!isNull(webSettings) && !isNull(webSettings['currencyCode'])){
//			defCurrency  = getObjectFromList('code',webSettings['currencyCode'],this.currencyList);
//			if(isNull(defCurrency) && Array.isArray(this.currencyList) &&  this.currencyList.length > 0){
//					defCurrency = this.currencyList[0];
//			}else{
//				defCurrency = webSettings['currencyCode'];
//			}
//		}
		if(Array.isArray(this.currencyList) &&  this.currencyList.length > 0){
			for(var i=0; i<this.currencyList.length; i++){
				var currency =  this.currencyList[i];
				if(currency.hasOwnProperty('defaultCurrencyCode')){
					defCurrency = currency;
					break;
				}
			}
		}
		return defCurrency;
	};
	
	QuickViewProduct.prototype.createRequestObject = function(){
		//PriceCalculationData=[{"itemCode":"BEOCOM 999999","unitCode":"BOX10","currencyCode":"INR","dateString":"2014-09-19","quantity":"1"}]
		this.requestObject = new Object();
		this.requestObject.itemCode = this.getItemCode();
		this.requestObject.unitCode = this.selectedUnit;
		this.requestObject.currencyCode = returnBlankIfNull(this.selectedCurrency.code);
		this.requestObject.dateString = returnBlankIfNull(this.dateString);
		if(isNull(this.itemObject.newQuantity) || this.itemObject.newQuantity == ""){
			this.itemObject.newQuantity = "1";
		}
		this.requestObject.quantity = this.itemObject.newQuantity;
		return this.requestObject;
	};
	
	QuickViewProduct.prototype.createRequestObjectUnitChange = function(){
		//PriceCalculationData=[{"itemCode":"BEOCOM 999999","unitCode":"BOX10","currencyCode":"INR","dateString":"2014-09-19","quantity":"1"}]
		this.requestObject = new Object();
		this.requestObject.itemCode = this.getItemCode();
		this.requestObject.unitCode = this.selectedUnit;
		this.requestObject.actualPrice = this.pricePropName;
		
		if(isNull(this.itemObject.newQuantity) || this.itemObject.newQuantity == ""){
			this.itemObject.newQuantity = "1";
		}
		this.requestObject.quantity = this.itemObject.newQuantity;
		return this.requestObject;
	};
	

	
		QuickViewProduct.prototype.onUnitChangeQuickViewDlg = function(){
		
		//this.itemObject.newUnit = this.selectedUnit;
		var newUnitSelected = this.selectedUnit;
		var itemCodeSelected = this.getItemCode();
		
		
		buyObj = new Buy([this.itemObject]);
		buyObj.setPropNameItemCode(this.itemCodePropName);
		//buyObj.setPropNameOrdered('newQuantity');
		buyObj.setPropNameUnit('newUnit');
		//buyObj.setPropNameDelDate('newDelDate');
		
		
		this.buyObj = buyObj;
		this.rootScope.onUnitChangeQuickView(itemCodeSelected,newUnitSelected,this.itemObject,this);
				
	};
//	
//	QuickViewProduct.prototype.getParamOnUnitChange = function(){
//		var reqObj =  this.createRequestObjectUnitChange();
//		var param = "PriceCalculationData=" +JSON.stringify([reqObj]);
//		return param;
//	};
//	
//	QuickViewProduct.prototype.calculatePrice1 = function(){
//		if(this.itemObject.validQuantity){
//			
//			this.rootScope.getQuickViewChanges(this.getParamOnUnitChange(), this);
//		}
//	};
	
//	
//	QuickViewProduct.prototype.setResponse = function(responseObject){
//		
//		if(Array.isArray(responseObject) && responseObject.length > 0 ){
//			copyObjectWithNull (responseObject[0],this.responseObject);
//			this.responseObject.currencyCode = this.requestObject.currencyCode;
//			
//		}
//	};
//

	
	//	
//	
//	QuickViewProduct.prototype.setResponseUnitChange = function(responseObject){
//		console.log("set data for response change");
//		
//		if(Array.isArray(responseObject) && responseObject.length > 0 ){
//			copyObjectWithNull (responseObject[0],this.responseObject);
//			//this.responseObject.currencyCode = this.requestObject.currencyCode;
//			this.responseObject.newQuantity=this.requestObject.quantity;
//			
//		}
//		this.responseObject.newQuantity = "1";
//	};
	
	QuickViewProduct.prototype.isBuyAllowed = function(){
		var isBuyAllowed = false;
		if(!isNull(this.itemObject)){
			isBuyAllowed = this.itemObject[this.buyAllowedPropName];
			if(isNull(isBuyAllowed)){
				isBuyAllowed = false;
			}
		}
		return isBuyAllowed;
	};
	
	QuickViewProduct.prototype.isMatrixBaseItem = function(){
		var isMatrixBaseItem = false;
		if(!isNull(this.itemObject)){
			isMatrixBaseItem = this.itemObject[this.isMatrixbaseItemPropName];
			if(isNull(isMatrixBaseItem)){
				isMatrixBaseItem = false;
			}
		}
		return isMatrixBaseItem;
	};
	
	QuickViewProduct.prototype.getParam = function(){
		var reqObj =  this.createRequestObject();
		var param = "PriceCalculationData=" +JSON.stringify([reqObj]);
		return param;
	};
	

	QuickViewProduct.prototype.getItemCode = function(){
		var itemCode = null;
		if(!isNull(this.itemObject)){
			itemCode = this.itemObject[this.itemCodePropName];
		}
		return itemCode;
	};
	
	QuickViewProduct.prototype.getValidUnit = function(validUnit){
		var hideAddToList = true;
		if(!isNull(validUnit) && validUnit !=""){
			hideAddToList = true;
		}else{
			hideAddToList = false;
		}
		return hideAddToList;
	};
	
	QuickViewProduct.prototype.getItemDiscountPercentage = function(){
		var itemCode = 0;
		if(!isNull(this.itemObject)){
			itemCode = this.itemObject[this.itemDiscountPercentagePropName];
		}
		return itemCode;
	};
	
	QuickViewProduct.prototype.getExtendedInfo = function(){
		var extendedTextInfo = "";
		if(!isNull(this.itemObject)){
			extendedTextInfo = this.itemObject[this.itemWebTextPropName];
		}
		return extendedTextInfo;
	};
	
	QuickViewProduct.prototype.getDiscountPrice = function(){
		var discPrice = 0;
		if(!isNull(this.itemObject)){
			discPrice = this.itemObject[this.discountPricePropName];
		}
		return discPrice;
	};
	
	
	QuickViewProduct.prototype.getDesc = function(){
		var desc = null;
		if(!isNull(this.itemObject)){
			desc = this.itemObject[this.descCodePropName];
		}
		return desc;
	};
	
	
	QuickViewProduct.prototype.getAttentionURLName = function(){
		var attentionUrl = null;
		if(!isNull(this.itemObject)){
			attentionUrl = this.itemObject[this.attentionUrlPropName];
		}
		return attentionUrl;
	};
	
	QuickViewProduct.prototype.getAttentionDisplay = function(){
		var attentionDisplay = false;
		if(!isNull(this.itemObject)){
			attentionDisplay = this.itemObject[this.attentionImageShowPropName];
		}
		//console.log(attentionDisplay);
		return attentionDisplay;getImageUrl
	};

	QuickViewProduct.prototype.getIsRelative = function(){
		var isRelative = true;
		if(!isNull(this.itemObject)){
			isRelative = this.itemObject[this.isRelativePropName];
		}
		//console.log(attentionDisplay);
		return isRelative;
	};
	
	QuickViewProduct.prototype.getImageUrl = function(){
		var imageURL = false;
		if(!isNull(this.itemObject)){
			imageURL = this.itemObject[this.imageUrlPropName];
		}
		//console.log(imageURL);
		return imageURL;
	};
	
	
	QuickViewProduct.prototype.getUnits = function(){
		var units = [];
		if(!isNull(this.itemObject)){
			units = this.itemObject[this.salesUnitsPropName];
		}
		return units;
	};
	
	QuickViewProduct.prototype.setSelectedUnit = function(){
		if(!isNull(this.itemObject)){
			this.selectedUnit = this.itemObject[this.unitPropName];
		}
	};
	
	QuickViewProduct.prototype.getSelectedUnit = function(){
		var selectedUnit = "";
		if(!isNull(this.itemObject)){
			selectedUnit = this.itemObject["defaultSalesUnit"];
		}
		return selectedUnit
	};
	
	QuickViewProduct.prototype.getPrice = function(){
		var price = "";
		if(!isNull(this.itemObject)){
			//this.selectedUnit = this.itemObject[this.unitPropName];
			price = this.itemObject[this.pricePropName];
		}
		return price;
	};
	
	QuickViewProduct.prototype.calculatePrice = function(){
		if(this.itemObject.validQuantity){
			this.enableCalculateButton = false;
			this.rootScope.getPriceCalculations(this.getParam(), this);
		}
	};
	
	QuickViewProduct.prototype.setResponse = function(responseObject){
		this.enableCalculateButton = true;
		if(Array.isArray(responseObject) && responseObject.length > 0 ){
			copyObjectWithNull (responseObject[0],this.responseObject);
			this.responseObject.currencyCode = this.requestObject.currencyCode;
			
		}
	};
	
	QuickViewProduct.prototype.buy = function(){
		//this.itemObject.newQuantity = this.quantity;
		this.itemObject.newUnit = this.selectedUnit;
//		this.itemObject.newDelDate = this.dateString;
		//this.itemObject.itemQty = this.itemObject.newQuantity;
		
		
		buyObj = new Buy([this.itemObject]);
		buyObj.setPropNameItemCode(this.itemCodePropName);
		buyObj.setPropNameOrdered('newQuantity');
		buyObj.setPropNameUnit('newUnit');
//		buyObj.setPropNameDelDate('newDelDate');
		
		this.buyObj = buyObj;
		
		this.rootScope.checkValidQuantityQuickInfo(this.itemObject,this.itemObject.newQuantity);
		
		
		if(this.itemObject.validQuantityPriceCalc){
			
			this.rootScope.buyMultipleItems(this.itemObject,this.buyObj);
		}
	};
	
	QuickViewProduct.prototype.addItemToCurrentList = function(){
		//this.itemObject.newQuantity = this.quantity;
		this.itemObject.newUnit = this.selectedUnit;
		this.itemObject.newDelDate = this.dateString;
		
		buyObj = new Buy([this.itemObject]);
		buyObj.setPropNameItemCode(this.itemCodePropName);
		buyObj.setPropNameOrdered('newQuantity');
		buyObj.setPropNameUnit('newUnit');
	//	buyObj.setPropNameDelDate('newDelDate');
		
		this.buyObj = buyObj;
		this.rootScope.checkValidQuantityQuickInfo(this.itemObject,this.itemObject.newQuantity);
		
		if(this.itemObject.validQuantityPriceCalc){
			this.rootScope.addMultipleItemsToList(this.itemObject,this.buyObj);
		}
	};
	
	// ADM-015
	QuickViewProduct.prototype.getQuantity = function(){
		var quantity = 1;
		if(!isNull(this.itemObject)){
			quantity = this.itemObject[this.quantityPropName];
		}
		//console.log(attentionDisplay);
		return quantity;
	};
	
	// ADM-020
	QuickViewProduct.prototype.isValidEmailAddress = function() {
		//var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		//if (re.test(this.mailFrom)){  
		if(this.mailFrom != ""){
			return true;
		}else{
			return false;
		}
	};
	
	// ADM-020
	QuickViewProduct.prototype.isValidValues = function() {
		var isValid = true;
		if(!this.isValidEmailAddress()) {
			if(this.startShowError){
				this.UIErrorKey ='MSG_INVALID_EMAIL_ADDRESS';
			}
			return isValid = false;
		}
		this.rootScope.checkValidQuantityQuickInfo(this.itemObject,this.itemObject.newQuantity);
		isValid = this.itemObject.validQuantityPriceCalc;
		if(isValid){
			this.UIErrorKey  = null;
		}
		return isValid;
	};	
	
	// ADM-020
	QuickViewProduct.prototype.getUIErrorKey = function() {
		return this.UIErrorKey;
	};	

	//test quick view end