function UIHistoryManager(){
		this.CONST_SES_STORE_KEY = "_HM_stateCacheList";
		this.CONST_SES_RELOAD_STATUS = "_HM_reloadStatus";
		//if(isNull(this.getFromSessionObject(this.CONST_SES_STORE_KEY))){
			this.state = null;
			this.stateCacheList = [];
			this.cachedScopeForBackState = null;
			this.backStateForPromo = false;
			this.isBackAction = false;
			this.stateName = null;
			this.stateParams = null;
			this.maxLimit = 8;
		/*} else {
			this.stateCacheList = this.getFromSessionObject(this.CONST_SES_STORE_KEY);
		}*/
		
	};
	
	UIHistoryManager.prototype.setReloadForAll = function(){
		if(this.getReloadStatus()){
			this.setReloadStatus(false);
			if(Array.isArray(this.stateCacheList)){
				for(var i=0; i<this.stateCacheList.length; i++){
					var stateObj = this.stateCacheList[i];
					stateObj.isReloadRequired = true;
					//console.log("setReloadForAll() - " + stateObj.stateName  +" , isReloadRequired="+ stateObj.isReloadRequired);
				}
				this.setToSessionObject(this.CONST_SES_STORE_KEY,this.stateCacheList);
			}
		}
	};
	
	UIHistoryManager.prototype.setReloadStatus = function(isReloadRequired){
		this.setToSessionObject(this.CONST_SES_RELOAD_STATUS, isReloadRequired);
	};
	
	UIHistoryManager.prototype.getReloadStatus = function(){
		var flag = this.getFromSessionObject(this.CONST_SES_RELOAD_STATUS);
		if(isNull(flag)){
			flag = false;
		}
		return flag;
	};
	
	UIHistoryManager.prototype.setToSessionObject = function(key,ValObj){
		if(!isNull(window.sessionStorage)){
			window.sessionStorage.removeItem(key);
			window.sessionStorage.setItem(key, JSON.stringify(ValObj));
		}
	};

	UIHistoryManager.prototype.getFromSessionObject = function(key)  {
		var obj = null;
		if(!isNull(window.sessionStorage)){
			var retrievedObject = window.sessionStorage.getItem(key);
			if(!isNull(retrievedObject)){
				obj = JSON.parse(retrievedObject);
			}
		}
		return obj;
	};
	
	UIHistoryManager.prototype.registerBackAction = function(){
		this.isBackAction = true;
	};
	
	UIHistoryManager.prototype.isBackAction = function(){
		return this.isBackAction ;
	};
	
	UIHistoryManager.prototype.loadScopeWithCache = function(currentScope){
		this.deepClone(this.cachedScopeForBackState, currentScope);
		
		if(this.stateCacheList.length > 0){
			var stateObj = this.stateCacheList[this.stateCacheList.length-1];
			stateObj.isLoadedWithBack = true;
		};
		
		this.isBackAction  = false;
	};
	
	UIHistoryManager.prototype.loadScopeWithCacheReviewOrder = function(currentScope){
		if(this.stateCacheList.length > 0){
			var stateObj = this.stateCacheList[this.stateCacheList.length-1];
			stateObj.isLoadedWithBack = true;
		};

		this.isBackAction  = false;
	};
	
	UIHistoryManager.prototype.init = function(routerState){
		this.state = routerState;
		//if(isNull(this.getFromSessionObject(this.CONST_SES_STORE_KEY))) {
			this.stateCacheList = [];
		//}
		this.setToSessionObject(this.CONST_SES_RELOAD_STATUS, false);
	};
	
	UIHistoryManager.prototype.getStateCacheList = function(){
		if(isNull(this.stateCacheList) || this.stateCacheList.length==0){
			//load from session
			this.stateCacheList = this.getFromSessionObject(this.CONST_SES_STORE_KEY);
			if(isNull(this.stateCacheList)){
				this.stateCacheList=[];
			}
			return this.stateCacheList;
		}else{
			return this.stateCacheList;
		}
	};
	
	UIHistoryManager.prototype.getLastState = function(){
		var stateList = this.getStateCacheList();
		
		var cachedState = stateList.pop();
		if(!isNull(cachedState) && cachedState.isLoadedWithBack){
			//do one more pop if loaded from cache
			cachedState = stateList.pop();
		}else if(!isNull(cachedState) && !this.isChangeInState(cachedState)){
			//do one more pop if both states are same
			cachedState = stateList.pop();
		}
		return cachedState;
	};

	UIHistoryManager.prototype.gotoHomeCatalog = function(){
		this.isBackAction = false;
		this.state.go('home.homeCatalogGrid');
	};
	
	UIHistoryManager.prototype.goBack = function(){
	//	this.setReloadForAll();
		var stateCacheObj = this.getLastState();
		this.registerBackAction();

		if(isNull(stateCacheObj) || this.stateCacheList.length==0){
			this.gotoHomeCatalog();
		}else{
			if(stateCacheObj.isReloadRequired){
				this.isBackAction  = false;
				this.state.go(stateCacheObj.stateName, stateCacheObj.params,{reload: true});
			}else{
				this.isBackAction = true;
				this.cachedScopeForBackState = stateCacheObj.scopeObject;
				this.backStateForPromo = true;
				this.state.go(stateCacheObj.stateName, stateCacheObj.params);
			}
		}
	};
	
	UIHistoryManager.prototype.isChangeInState = function(stateCacheObj){
		var flag = true;
		if(!isNull(stateCacheObj.scopeObject) && stateCacheObj.scopeObject.repeats) return true;
		if(isEqualStrings(this.state.current.name, stateCacheObj.stateName)){
			flag = false;
		}
		return flag;
	};
	
	UIHistoryManager.prototype.addStateTolist = function(currentStateObj){
		var previousState = null;
		var listSize = this.stateCacheList.length;
		if(listSize>0){
			previousState = this.stateCacheList[listSize-1];
			if(previousState.stateName == currentStateObj.stateName){
				//remove last state, as it is duplicate of new state 
				this.stateCacheList.pop();
				this.stateCacheList.push(currentStateObj);
			}else{
				this.stateCacheList.push(currentStateObj);
			}
		}else{
			this.stateCacheList.push(currentStateObj);
		}
	};
	
	UIHistoryManager.prototype.loginStatusChanged = function(){
		this.setReloadStatus(true);
	};
	
	UIHistoryManager.prototype.pushScopeObject = function(scopeObject){
		var scopeObjectForCache = new Object();
		this.deepClone(scopeObject, scopeObjectForCache);
		var stateCacheObj = new StateCache(this.stateName,scopeObjectForCache);
		this.deepClone(this.stateParams, stateCacheObj.params);
		this.addStateTolist(stateCacheObj);
		//console.log(" scope object cached: " + JSON.stringify(stateCacheObj));
		this.cleanOldCache();
		this.setToSessionObject(this.CONST_SES_STORE_KEY,this.stateCacheList);
	};
	
	UIHistoryManager.prototype.cleanOldCache = function(){
		if(this.stateCacheList.length>this.maxLimit){
			this.stateCacheList.splice(0,this.stateCacheList.length-this.maxLimit);
		}
	};
	
	UIHistoryManager.prototype.updateState = function(stateName,stateParam){
		this.stateParams = new Object();
		if(isNull(stateName) || isEmptyString(stateName) || stateName=="home"){
			stateName = "home.homeCatalogGrid";
		}
		this.stateName = stateName;
		this.deepClone(stateParam, this.stateParams);
	};
	
	UIHistoryManager.prototype.deepClone = function(fromObject, toObject){
		if(!isNull(toObject) && !isNull(fromObject)){
			for (var key in fromObject) {
				//copy all the fields
				var prop = fromObject[key];
				if(typeof prop != "function" && ( key.indexOf("$",0) != 0 ) && ( key != "this" ) && ( key != "angular.js")){
					if(typeof prop == "object"){
						toObject[key] = deepCopyWithFunctions(prop);
					}else{
						toObject[key] = prop;
					}
				}
			}
		}
	};
	
	UIHistoryManager.prototype.getCache = function(currentscope) {
		var scl = historyManager.getStateCacheList();
		for(var i = scl.length-1; i >= 0; i--) {
			if(scl[i].scopeObject.identifierUnique == currentscope.identifierUnique) {
				return scl[i].scopeObject;
			}
		}
		return null;
	};
	
	UIHistoryManager.prototype.removeCache = function(identifierUnique) {
		var scl = historyManager.getStateCacheList();
		for(var i = scl.length-1; i >= 0; i--) {
			if(!isNull(scl[i]) && !isNull(scl[i].scopeObject) && !isNull(scl[i].scopeObject.identifierUnique) && (scl[i].scopeObject.identifierUnique == identifierUnique)) {
				scl.splice(i, 1);
				i++;
				//break;
			}
		}
	};
	
	UIHistoryManager.prototype.loadScopeWithCacheProvided = function(currentScope, cache){
		this.deepClone(cache, currentScope);
		
		if(this.stateCacheList.length > 0){
			var stateObj = this.stateCacheList[this.stateCacheList.length-1];
			stateObj.isLoadedWithBack = true;
		};
		
		this.isBackAction  = false;
	};
	
//	function PersistObject(){
//		
//	};
//
//	PersistObject.prototype.setObject = function(key,ValObj){
//		if(!isNull(window.localStorage)){
//			window.localStorage.removeItem(key);
//			window.localStorage.setItem(key, JSON.stringify(ValObj));
//		}
//	};
//
//	PersistObject.prototype.getObject = function(key)  {
//		var obj = null;
//		if(!isNull(window.localStorage)){
//			var retrievedObject = window.localStorage.getItem(key);
//			if(!isNull(retrievedObject)){
//				obj = JSON.parse(retrievedObject);
//			}
//		}
//		return obj;
//	};