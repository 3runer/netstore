function NewAccount(newCustomerPolicy,newWebUserPolicy,newCustomerTypePolicy){
//	NewCustomerData=[{"accountType":"COMPANY","customerName":"Anant12","address1":"noida","postcode":"201301",
//		"city":"london","country":"GB","contact":"anant12","phoneNum":"9090909090",
//		"language":"EN","userId":"NS900070","webUserName":"Anant13",
//		"emailId":"anant.fauzdar@ibs.net","locale":"*DEFAULT"}];
	
	this.newCustomerPolicy=newCustomerPolicy;
	this.newWebUserPolicy = newWebUserPolicy;
	this.newCustomerTypePolicy = newCustomerTypePolicy;

	this.showPWDpage = false;
	
	this.CONST_NEW_COMPANY_ACCOUNT="COMPANY";
	this.CONST_NEW_PERSON_ACCOUNT="PRIVATE";
	this.CONST_INTERNET_ENABLE_CUSTOMER="INTERNET_ENABLE_CUSTOMER";
	this.mandatoryFields = ['accountType','customerName','address1','postcode','city','contact','userId','webUserName','emailId'];

	this.CONST_PAGE_NEW_ACCOUNT = 'NEW_ACCOUNT';
	this.CONST_PAGE_CUSTOMER = 'CUSTOMER';
	this.CONST_PAGE_WEB_USER = 'WEB_USER';
	this.CONST_PAGE_PASSWORD = 'PASSWORD';
	this.CONST_PAGE_VERIFY = 'VERIFY';

	
	this.CONST_PAGE_NAME_LIST = [this.CONST_PAGE_NEW_ACCOUNT,this.CONST_PAGE_CUSTOMER,this.CONST_PAGE_WEB_USER,this.CONST_PAGE_VERIFY];
	this.CONST_PAGE_NAME_LIST_INTERNET = [this.CONST_PAGE_NEW_ACCOUNT,this.CONST_PAGE_WEB_USER,this.CONST_PAGE_VERIFY];
	this.CONST_PAGE_NAME_LIST_WITH_PWD = [this.CONST_PAGE_NEW_ACCOUNT,this.CONST_PAGE_CUSTOMER,this.CONST_PAGE_WEB_USER,this.CONST_PAGE_PASSWORD,this.CONST_PAGE_VERIFY];
	this.CONST_PAGE_NAME_LIST_WITH_PWD_INTERNET = [this.CONST_PAGE_NEW_ACCOUNT,this.CONST_PAGE_WEB_USER,this.CONST_PAGE_PASSWORD,this.CONST_PAGE_VERIFY];

	this.countryList = [];
	this.stateList = [];
	this.countyList = [];
	this.localeList = [];
	this.languageList = [];
	this.currentPageNum = 1;
	
	this.selectedCountry = "";
	this.selectedState = "";
	this.selectedLanguage = "";
	this.selectedLocale = null;
	this.errorMsg = null;
	this.validateCustomerErrorMsg = null;
	
	this.request = new Object();
	this.request.accountType=this.CONST_NEW_COMPANY_ACCOUNT;
	this.request.customerName="";
	this.request.address1="";
	this.request.address2="";
	this.request.address3="";
	this.request.postcode="";
	this.request.city="";
	this.request.state="";
	this.request.country="";
	this.request.contact="";
	this.request.phoneNum="";
	this.request.faxNum="";
	this.request.language="";
	this.request.userId="";
	this.request.webUserName="";
	this.request.emailId="";
	this.request.locale="";
	this.request.vatRegNum="";
	this.request.customerCode="";
	this.request.password="";
	this.confPassword="";
	
	this.pageNameList = null;
	this.init();
}

NewAccount.prototype.init = function () {
	if((this.newCustomerPolicy=="*DIRECT" || this.newCustomerPolicy=="*DIRECT_SUNDRY" || this.newCustomerPolicy=="*NOT_ALLOWED") 
			&& this.newWebUserPolicy == "*DIRECT" ){
		this.showPWDpage = true;
	};
	if(this.newCustomerPolicy=="*NOT_ALLOWED" || this.newCustomerTypePolicy=="*PRIVATE"){
		this.isNewCustomerAllowed = false;
	}else{
		this.isNewCustomerAllowed = true;
	}
	if(this.newCustomerTypePolicy=="*COMPANY" || this.newCustomerTypePolicy=="*SELECT"){
		this.newPrivatePersonAllowed = false;
	}else{
		this.newPrivatePersonAllowed = true;
	}
	
	//set selection for account type
	if(this.isNewCustomerAllowed){
		this.request.accountType=this.CONST_NEW_COMPANY_ACCOUNT;
	}else if (this.newPrivatePersonAllowed){
		this.request.accountType=this.CONST_NEW_PERSON_ACCOUNT;
	}else{
		this.request.accountType=this.CONST_INTERNET_ENABLE_CUSTOMER;
	}
	
	this.setPageNameList();
};

NewAccount.prototype.clearRequestObject = function () {
	this.request.customerName="";
	this.request.address1="";
	this.request.address2="";
	this.request.address3="";
	this.request.postcode="";
	this.request.city="";
	this.request.state="";
	this.request.county="";
	this.request.country="";
	this.request.contact="";
	this.request.phoneNum="";
	this.request.faxNum="";
	this.request.language="";
	this.request.userId="";
	this.request.webUserName="";
	this.request.emailId="";
	this.request.locale="";
	this.request.vatRegNum="";
	this.request.customerCode="";
	this.request.password="";
	this.confPassword="";
	this.selectedCountry = "";
	this.selectedState = "";
	this.countyList = [];
	this.stateList = [];
};

NewAccount.prototype.setDefaultLocale = function () {
	if(isNull(this.selectedLocale)){
		if(!isNull(this.localeList) && !isNull(this.localeList.length>0)){
			this.selectedLocale=this.localeList[0];
		}
	}
};

NewAccount.prototype.setValidateCustomerResponse = function (response) {
	if(!isNull(response) && !isNull(response.messageCode)){
		var msgCode = response.messageCode + "";
//	    4506 - Valid ID
//	    4503 - Invalid ID
//	    4504 - Customer Web User Exist
		switch(msgCode){
//		case "4506":
//			this.validateCustomerErrorMsg = null;
//			this.gotoNextPage();
//			break;
		case "4503":
			this.validateCustomerErrorMsg = translate.getTranslation('MSG_INVALID_CUSTOMER');
			break;
		case "4504":
			this.validateCustomerErrorMsg = translate.getTranslation('MSG_CUSTOMER_WEBUSER_EXIST');
			break;
		}
	}else if(!isNull(response)){
		//customer data received 
		this.validateCustomerErrorMsg = null;
		this.gotoNextPage();
		this.request.address1=response.address1;
		this.request.address2=response.address2;
		this.request.address3=response.address3;
		this.request.city=response.city;
		this.request.state=response.state;
		this.selectedCountry = getObjectFromList('code',response.country,this.countryList);
		this.request.userId=response.customerCode;
		this.request.customerName=response.customerName;
	}
};

NewAccount.prototype.onAccountTypeChange = function () {
	this.validateCustomerErrorMsg = null;
	this.clearRequestObject();
	this.setPageNameList();
};

NewAccount.prototype.onCountryChange = function () {
	this.countyList = [];
	if(!isNull(this.selectedCountry) && !isNull(this.selectedCountry.code)){
		this.request.country=this.selectedCountry.code;
		this.request.language=this.selectedLanguage.code;
	}
};

NewAccount.prototype.onStateChange = function () {
	if(!isNull(this.selectedState) && !isNull(this.selectedState.stateCode)){
		this.request.state=this.selectedState.stateCode;
	}
};

NewAccount.prototype.onCountyChange = function () {
	if(!isNull(this.selectedCounty) && !isNull(this.selectedCounty.countyCode)){
		this.request.county=this.selectedCounty.countyCode;
	}
};

NewAccount.prototype.onLanguageChange = function () {
	if(!isNull(this.selectedLanguage) && !isNull(this.selectedLanguage.code)){
		this.request.language=this.selectedLanguage.code;
	}
};

NewAccount.prototype.onLocaleChange = function () {
	if(!isNull(this.selectedLocale) && !isNull(this.selectedLocale.code)){
		this.request.locale=this.selectedLocale.code;
	}
};

NewAccount.prototype.isMandatoryField = function (objKeyName) {
	var isMandatory = false;
	if(!isNull(objKeyName) && !isEmptyString(objKeyName)){
		for(var i=0; i<this.mandatoryFields.length ;i++){
			if(objKeyName==this.mandatoryFields[i]){
				isMandatory = true;
				break;
			}
		}
	}
	return isMandatory;
};

NewAccount.prototype.getNumberOfPageIcons = function () {
	var numberOfPages = this.getPageNameList().length;
	return numberOfPages;
};


NewAccount.prototype.setPageNameList = function () {
	if(this.showPWDpage){
		if(this.request.accountType == this.CONST_INTERNET_ENABLE_CUSTOMER){
			this.pageNameList = this.CONST_PAGE_NAME_LIST_WITH_PWD_INTERNET;
		}else{
			this.pageNameList = this.CONST_PAGE_NAME_LIST_WITH_PWD;
		}
	}else{
		if(this.request.accountType == this.CONST_INTERNET_ENABLE_CUSTOMER){
			this.pageNameList = this.CONST_PAGE_NAME_LIST_INTERNET;
		}else{
			this.pageNameList = this.CONST_PAGE_NAME_LIST;
		}
	}
	return this.pageNameList;
};

NewAccount.prototype.getPageNameList = function () {
	return this.pageNameList;
};


NewAccount.prototype.isCurrentPageNumber = function (pageNum) {
	var flag= false;
	if(this.currentPageNum==pageNum){
		flag = true;
	}
	return flag;
};

NewAccount.prototype.hasStateList = function () {
	var flag= false;
	
	if(Array.isArray(this.stateList) && this.stateList.length>0){
		flag = true;
		
	}
	return flag;
};

NewAccount.prototype.hasCountyList = function () {
	var flag= false;
	
	if(Array.isArray(this.countyList) &&this.countyList.length>0){
		flag = true;
	
	}
	return flag;
};

NewAccount.prototype.getPageTitle = function () {
	var pageTitle = "";
	var pageName  = this.getPageName();
	switch(pageName){
	case this.CONST_PAGE_NEW_ACCOUNT:
		pageTitle = translate.getTranslation('CON_NEW_ACCOUNT');
		break;
	case this.CONST_PAGE_CUSTOMER:
		pageTitle = translate.getTranslation('CON_NEW_ACCOUNT');
		pageTitle = pageTitle + " - " +translate.getTranslation('CON_CUSTOMER'); 
		break;
	case this.CONST_PAGE_WEB_USER:
		pageTitle = translate.getTranslation('CON_NEW_ACCOUNT');
		pageTitle = pageTitle + " - " +translate.getTranslation('CON_WEBUSER'); 
		break;
	case this.CONST_PAGE_VERIFY:
		pageTitle = translate.getTranslation('CON_NEW_ACCOUNT');
		pageTitle = pageTitle + " - " +translate.getTranslation('CON_VERIFY'); 
		break;
		
	case this.CONST_PAGE_PASSWORD:
		pageTitle = translate.getTranslation('CON_NEW_ACCOUNT');
		pageTitle = pageTitle + " - " +translate.getTranslation('CON_PASSWORD'); 
		break;
	}
	return pageTitle;
};

NewAccount.prototype.getPageName = function () {
	var pageName = null;
	pageName = this.getPageNameList()[this.currentPageNum-1];
	return pageName;
};

NewAccount.prototype.showPage = function (pageName) {
	var showPage = false;
	showPage = this.getPageNameList()[this.currentPageNum-1] == pageName;
	return showPage;
};

NewAccount.prototype.prevoiusPage = function () {
	if(this.currentPageNum > 1){
		this.currentPageNum--;
	}
};

NewAccount.prototype.nextPage = function (userIDCode) {
	if(this.request.accountType == this.CONST_INTERNET_ENABLE_CUSTOMER && this.currentPageNum==1){
		// it is handled in set validate customer response function
		if(isNull(this.request.customerCode) || isEmptyString(this.request.customerCode)){
			this.validateCustomerErrorMsg = translate.getTranslation('MSG_ENTER_CUSTOMER_NUMBER');
		}else{
			this.validateCustomerErrorMsg = "";
		}
	}else{
		if(this.currentPageNum < this.getNumberOfPageIcons()){
			this.currentPageNum++;
			if(!isNull(userIDCode)){
			this.request.userId=userIDCode;
			}
		}
	}
};

NewAccount.prototype.gotoNextPage = function () {
	if(this.currentPageNum < this.getNumberOfPageIcons()){
		this.currentPageNum++;
	}
};

NewAccount.prototype.isNextNewAccountPage = function () {
	var isNext = false;
	if(!isNull(this.request.accountType) && !isEmptyString(this.request.accountType)){
		isNext = true; 
		this.setDefaultLocale();
	}
	return isNext ;
};

NewAccount.prototype.isNextCustomerPage = function () {
	var isNext = false;
	if(this.request.accountType==this.CONST_NEW_COMPANY_ACCOUNT){
		isNext = (this.hasValue(this.request.customerName) && this.hasValue(this.request.address1) && this.hasValue(this.request.postcode) 
				&& this.hasValue(this.request.country) && this.hasValue(this.request.city) && this.hasValue(this.request.contact) && this.hasValue(this.request.phoneNum) );
	}else{
		isNext = (this.hasValue(this.request.customerName) && this.hasValue(this.request.address1) && this.hasValue(this.request.postcode) 
				&& this.hasValue(this.request.country) && this.hasValue(this.request.city) && this.hasValue(this.request.phoneNum) );
	}
	return isNext ;
};

NewAccount.prototype.isNextWebUserPage = function () {
	var isNext = false;

	isNext = (this.hasValue(this.request.userId) && this.hasValue(this.request.webUserName)  && this.hasValue(this.request.emailId));
	
	return isNext ;
};

NewAccount.prototype.isNextPasswordPage = function () {
	var isNext = true;
		isNext =  (this.hasValue(this.request.password) && this.hasValue(this.confPassword) && 
				(this.request.password == this.confPassword) && this.request.password.length > 2);
	return isNext ;
};

NewAccount.prototype.hasValue = function (obj) {
	return (!isNull(obj) && !isEmptyString(obj)) ;
};

NewAccount.prototype.getRequestParam = function () {
	var arr = [];
	var reqParam = "";
	var newReqObj = {};
	if(this.request.accountType == this.CONST_INTERNET_ENABLE_CUSTOMER){
		newReqObj.accountType = this.request.accountType;
		newReqObj.customerCode = this.request.customerCode;
		newReqObj.userId = this.request.userId;
		newReqObj.webUserName = this.request.webUserName;
		newReqObj.emailId = this.request.emailId;
		newReqObj.locale = this.request.locale;
		if(this.showPWDpage){
			newReqObj.password = this.request.password;
		}
		arr.push(newReqObj);
		reqParam = "NewCustomerData=" + JSON.stringify(arr);
	}else if (this.request.accountType == this.CONST_NEW_PERSON_ACCOUNT){
		copyObject(this.request, newReqObj);
		newReqObj.vatRegNum="";
		newReqObj.contact="";
		if(!this.showPWDpage){
			if(newReqObj.hasOwnProperty("password")){
				delete newReqObj.password;
			}
		}
		arr.push(newReqObj);
		reqParam = "NewCustomerData=" + JSON.stringify(arr);
	}else{
		//for company
		copyObject(this.request, newReqObj);
		if(!this.showPWDpage){
			if(newReqObj.hasOwnProperty("password")){
				delete newReqObj.password;
			}
		}
		arr.push(newReqObj);
		reqParam = "NewCustomerData=" + JSON.stringify(arr);
	}
	return reqParam;
};

NewAccount.prototype.showError = function (errorMsgClr) {
	var flag= false;
	if(errorMsgClr==true){
	flag= true;
	this.errorMsg = null;
	}
	if(!isNull(this.errorMsg)){
		flag = true;
	}
	return flag;
};

NewAccount.prototype.setSuccessCallBack = function(successCallBackFn){
	this.successCallBackFn = successCallBackFn;
};

NewAccount.prototype.setResponse = function (responseObj) {
	//console.log("create new account response " + JSON.stringify(responseObj));
	if(!isNull(responseObj) && !isNull(responseObj.messageCode) ){
		var messageCode = responseObj.messageCode + "";
		switch(messageCode){
		case "4500" : 
			//4500 - User created successfully
			this.errorMsg = null;
			this.successCallBackFn();
			break;
		case "4501" : 
			//4501 - Unexpected error occurred
			this.errorMsg = translate.getTranslation('MSG_UNEXPECTED_ERROR_OCCURED_WHEN_CREATE_NEW_ACCOUNT');
			break;
		case "4502" : 
			//4502 - User already exist
			this.errorMsg = translate.getTranslation('MSG_USER_ALREADY_EXIST');
			break;
		case "4503" : 
			//4503 - User id contains invalid character
			this.errorMsg = translate.getTranslation('MSG_ID_CONTAINS_INVALID_CHARACTERS');
			break;
		case "4505" : 
			//4505 - Invalid Vat Registration Number
			this.errorMsg = translate.getTranslation('MSG_INVALID_VAT_REGISTRATION_NUMBER');
			break;	
		case "6003" : 
			//6003 - Password Not Correct
			this.errorMsg = translate.getTranslation('MSG_PASSWORD_NOT_CORRECT');
			break;	
		case "6004" : 
			//6004 - Pasword Contains Inavlid Characters
			this.errorMsg = translate.getTranslation('MSG_PASSWORD_CONTAINS_INVALID_CHARACTERS');
			break;	
		}
	}
};