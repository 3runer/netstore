/* Main request object  */
function Request(customerName, customerCode, yourReference, yourReferenceCode,
		requestTypeList, requestFor) {
	this.requestBase = requestFor;
	this.customer = customerCode;
	this.customerName = customerName;
	this.yourReference = yourReferenceCode;
	this.yourReferenceName = yourReference.substring(0, 20);
	// this.probType = requestTypeList[0];
	this.probType = null;
	this.reqDesc = "";
	this.reference = null;
	this.docType = null;
	this.newRequestLines = [];
	this.requestTypeList = requestTypeList;
	this.UIErrorKey = null;
	this.ErrOnControl = {};
	this.requestParam = "";
	this.config = {};
}

/* to update the request base on selection */
Request.prototype.updateRequestBase = function(requestFor) {
	this.requestBase = requestFor;

};

Request.prototype.getUIErrorKey = function() {
	return this.UIErrorKey;
};

Request.prototype.clearUIErrorKey = function() {
	this.UIErrorKey = null;
	this.UIErrorParams = [];
	this.ErrOnControl = {};
};

Request.prototype.getUIErrorParams = function() {
	return this.UIErrorParams;
};

/* To create a blank request object */
Request.prototype.createBlankRequest = function() {
	var obj = new Object();
	obj.requestBase = this.requestBase;
	obj.customer = this.customer;
	obj.customerName = this.customerName;
	obj.yourReference = this.yourReference;
	obj.yourReferenceName = this.yourReferenceName;
	obj.probType = null;
	obj.reqDesc = null;
	obj.reference = null;
	obj.docType = null;
	obj.requestTypeList = this.requestTypeList;
	return obj;
};

/* To initialize the request object */
Request.prototype.createRequestObject = function(requestType, reqDesc,
		reference, docType) {
	var obj = new Object();
	obj.requestBase = this.requestBase;
	obj.customer = this.customer;
	obj.customerName = this.customerName;
	obj.yourReference = this.yourReferenceName;
	obj.yourReferenceName = this.yourReferenceName;
	obj.probType = returnBlankIfObjectNull(requestType, 'code');
	obj.reqDesc = reqDesc;
	obj.reference = returnBlankIfNull(reference);
	obj.docType = returnBlankIfNull(docType);
	obj.selctedProbType = requestType;
	return obj;
};

/* To reset the request description and reference number controls */
Request.prototype.clear = function() {
	this.reqDesc = "";
	this.reference = "";
	this.docType = "";
	// this.probType = this.requestTypeList[0];
	this.probType = null;
};

Request.prototype.isReferenceAvailable = function() {
	var isRefernceExist = false;
	if (!isEmptyString(this.reqDesc)) {
		isRefernceExist = true;
	}
	if (!isEmptyString(this.reference)) {
		isRefernceExist = true;
	}
	if (!isNull(this.probType)) {
		isRefernceExist = true;
	}

	return isRefernceExist;

};

Request.prototype.updateReference = function(reference, docType) {
	this.reference = returnBlankIfNull(reference);
	this.docType = returnBlankIfNull(docType);
};

/* To make the new request param string */
Request.prototype.getRequestParam = function(requestLineParam) {
	var requestParam = "";
	var requestObject = this.createRequestObject(this.probType, this.reqDesc,
			this.reference, this.docType);
	if (this.validateRequest(requestObject)) {

		requestObject = replaceObjectKey(requestObject, [ 'selctedProbType',
				'customerName', 'yourReferenceName' ]);
		requestParam = "requestInfoData=[" + JSON.stringify(requestObject)
				+ "]";
		if (!isEmptyString(requestLineParam)) {
			requestParam = requestParam + "&" + requestLineParam;
		}
		this.requestParam = requestParam;
	}
	return requestParam;
};

/* To validate the request lines list in the Add product table */
Request.prototype.validateRequest = function(requestObject) {
	var isValidRequest = true;
	/*
	 * ADM-XXX if(isEmptyString(requestObject.reqDesc )){ this.UIErrorKey =
	 * 'MSG_REQUEST_DESCRIPTION_MUST_BE_ENTERED'; this.UIErrorParams = [];
	 * this.ErrOnControl['reqDesc'] = true; isValidRequest = false; } else {
	 * this.ErrOnControl['reqDesc'] = false; }
	 * if(isEmptyString(requestObject.probType )){ this.UIErrorKey =
	 * 'MSG_REQUEST_TYPE_MUST_BE_SELECTED'; this.UIErrorParams = [];
	 * this.ErrOnControl['probType'] = true; isValidRequest = false; } else {
	 * this.ErrOnControl['probType'] = false; }
	 */
	if (isValidRequest) {
		this.UIErrorKey = null;
		this.UIErrorParams = [];
	}
	return isValidRequest;
};
