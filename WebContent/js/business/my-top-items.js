// top items
	function MyTopItems(){		
		this.filterMsg = null;			
		this.pageDataFound = false; // to be remove
		this.dataFound = true;
		this.isfilterOn = false;
		this.filterList =  null;
		this.sort = new Sort();
		this.paginationObj = null;
		this.UIMessageKey=null;
		this.messageCode = "";		
		
		this.listHeader = {};
		this.itemsList = [];
		this.filterParam = [];
	//	this.pageKey = "";
	//	this.tempItemsList = [];
	};
	
	
	MyTopItems.prototype.updateFilterStatus = function(list,isFirstPageData){
		if(Array.isArray(list) && list.length > 0 && isFirstPageData){
			this.filterList.cacheFilters();			
		}else if(this.isfilterOn && isFirstPageData){			
			this.filterList.applyPreviousFilters();			
			showMsgNoObjFound();
		}
	};
	MyTopItems.prototype.getSearchParams = function(){
		var params  = [];
		if(this.isfilterOn){
			params = this.sort.addSortParams(this.filterList.getParamsList());
		}else{
			params = this.sort.getParam();
		}
		return params;
	};	
	MyTopItems.prototype.getItemsList = function(){
		return this.itemsList;
	};
	
	MyTopItems.prototype.setFilterList = function(filterList){
		this.filterList = filterList;
	};
	MyTopItems.prototype.setPaginationObj = function(paginationObj){
		this.paginationObj = paginationObj;
	};
	MyTopItems.prototype.setItemList = function(itemList,isFirstPageData){
		if(Array.isArray(itemList) && itemList.length > 0 ){
			//retain last response data
			this.itemsList = itemList;
			
		}else{
			if(!isFirstPageData){
				this.itemsList = null;
			}
		}
	//	this.setDataFound(itemList,isFirstPageData);
	//	this.setPageLoadMsg(isFirstPageData);
		this.updateFilterStatus(itemList,isFirstPageData);
	};
	MyTopItems.prototype.getItemsList = function()
	{
		return this.itemsList ;
	};
//	MyTopItems.prototype.setDataFound = function(dataList,isFirstPageData){
////if(Array.isArray(dataList) && dataList.length > 0){
////	this.dataFound = true;
////}else if(!isFirstPageData){
////	this.dataFound = false;
////}
//};
//
//MyTopItems.prototype.setPageLoadMsg = function(isFirstPageData){
////if(!isFirstPageData && !this.dataFound){
////	this.pageLoadMsg = translate.getTranslation('CON_NO_MORE_PAGE');
////}else{
////	this.pageLoadMsg = "";
////}
//};

//	
//	MyTopItems.prototype.setItemsList = function(data,isFilterOn,isPagedData,lastPageKey)
//	{	
//		
//		if(isNull(data) || data[0].messageCode==7001)
//		{
//			this.pageDataFound = false;
//			this.itemsList =[]; 
//			if(isFilterOn)
//			{
//				if(isPagedData){
//					this.filterMsg = null;					
//				}
//				else{					
//					//this.filterMsg =  translate.getTranslation("MSG_NO_OBJECTS_FOR_SELECTION",null);
//					this.filterMsg = null;
//					showMsgNoObjFound();
//					this.pageDataFound = true;
//					this.itemsList  = this.tempItemsList; 
//					
//					
//				}
//			}
//			else{
//				this.filterMsg =  null;
//			}
//			
//		}
//		else
//		{
//			this.pageDataFound = true;
//			this.filterMsg =  null;
//			this.listHeader = data[0];
//		//	data.splice(0,1); // to be removed after final web service deployed
//			this.itemsList = data;
//			this.tempItemsList = data;
//			this.pageKey = lastPageKey;
//
//		}
//
//		
//		
//		
//	};
	
	
//	MyTopItems.prototype.fetchTopItems = function(requestParam)
//	{
//		this.topItemService.getTopItemList(requestParam).then(function(data){
//			if(isNull(data))
//				{
//					this.itemsList =[]; 
//				}
//			else
//				{
//					this.listHeader = data[0];
//					data.splice(0,1);
//					this.itemsList = data;
//				}
//		});
//	};