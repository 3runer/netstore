function Disclaimer(WsShowDisclaimer){
	this.WsShowDisclaimer = WsShowDisclaimer;
	this.disclaimerKey = "SHOW_DISCLAIMER";
	this.showDisclaimer = null;
};

Disclaimer.prototype.getDisclaimerKey = function(userId){
	var key = this.disclaimerKey;
//	if(!isNull(userId) || !isEmptyString(userId)){
//		key = key+"_"+userId;
//	}
	return key;
};

Disclaimer.prototype.loadDisclaimerFromLS= function(userId){
	var key = this.getDisclaimerKey(userId);
	if(!isNull(window.localStorage)){
		var retrievedObject = window.localStorage.getItem(key);
		if(!isNull(retrievedObject)){
			this.showDisclaimer = JSON.parse(retrievedObject);
		}
	}
};

Disclaimer.prototype.showCookieDisclaimer = function(userId) {
	var flag = true;
	if(this.WsShowDisclaimer){
		if(isNull(this.showDisclaimer)){
			this.loadDisclaimerFromLS(userId);
			if(!isNull(this.showDisclaimer)){
				flag  = this.showDisclaime;
			}
		}else if(!isNull(this.showDisclaimer) && !this.showDisclaimer){
			flag = false;
		}
	}else{
		flag = false;
	}
	return flag;
};

Disclaimer.prototype.updateCookieDisclaimer = function(userId, isAccepted) {
	var key = this.getDisclaimerKey(userId);
	if(!isNull(window.localStorage)){
		window.localStorage.removeItem(key);
		window.localStorage.setItem(key, JSON.stringify(isAccepted));
	}
};