//catalouge Object
function Catalog(itemCode, paramList, webSettings ){		
	this.catalogType= null;
	this.pageType = null;
	this.showPromotionalProducts = true;
	this.showPopularProducts = true;
	this.filterMsg = null;			
	this.pageDataFound = false;
	this.UIMessageKey=null;
	this.messageCode = "";	
	this.pageIndex = 1;			
	//this.CatalogService = CatalogService;
	this.catalogData = {};
	this.catalogTableHeader = {};
	this.productList = [];
	this.requestParam = paramList;
	this.itemCode = returnBlankIfNull(itemCode);
	this.webSettings = webSettings;
	this.filterOn = false;
	this.sort = new Sort();
	this.showExtendInfo = false;
};

Catalog.prototype.setPageType = function (PageType){
	this.pageType = PageType;
};

Catalog.prototype.setCatalogType = function (CatalogType,PageType){
	this.pageType = PageType; 
	if(isNull(CatalogType))	{
		this.catalogType = "NONE";}
	else{
		this.catalogType = CatalogType;
	}
	this.setRequestParam();
	this.setPromotinalProductFlag();
	this.setPopularProductFlag();
};
Catalog.prototype.getRequestParam = function (){
	return this.requestParam;
};


Catalog.prototype.setPromotinalProductFlag = function()
{
	this.showPromotionalProducts = this.webSettings.isShowPromotionalItems;
};
Catalog.prototype.setPopularProductFlag = function()
{
	this.showPopularProducts = this.webSettings.isShowMostPopularItems;
};

Catalog.prototype.setRequestParam = function (){
	var param = [];

	switch(this.catalogType){
	case  'NONE' :	param = this.getParamBasedOnPage();	break;
	default :		param = this.getParamBasedOnType();

	};
	appendItemsToList(param,this.requestParam);
};


Catalog.prototype.getParamBasedOnType = function (){
	var type = this.catalogType;
	var param = [];
	switch(type)
	{
	case 'BEST_OFFER': param = this.getBestOfferParam(); break;
	case 'MOST_POPULAR': param = this.getMostPopularParam(); break;
	/*ADM-008*/case 'ALTERNATIVES': param = this.getAlternativeParam(); break;

	};
	return param;
};
Catalog.prototype.getParamBasedOnPage = function (){
	var page = this.pageType;
	var param = [];
	switch(page)
	{
	case 'RequestConf' : param = this.getRequestRecievedParam();break;
	case 'ShoppingCart' : param = this.getShoppingCartParam();break;
	case 'ProductDetails' : param = this.getProductDetailParam();break;
	case 'Catalogue' : param = this.getThankYouParam();break;

	};
	return param;
};
Catalog.prototype.getBestOfferParam = function (){

	//var param = [];
	var	param = ['Page','Catalogue','Type','PromotionCampaign'];
	return param;
};
Catalog.prototype.getMostPopularParam = function (){

	var	param = ['Page','Catalogue','Type','MostPopular'];
	return param;
};
//ADM-008
Catalog.prototype.getAlternativeParam = function (){
	
	var	param = ['Page','Catalogue','Type','AlternativesItems','ItemCode',this.itemCode];
	return param;
};

Catalog.prototype.getRequestRecievedParam = function (){
	var	param = ['Page','RequestConf'];

	return param;
};

Catalog.prototype.getShoppingCartParam = function (){
	var	param = ['Page','ShoppingCart'];

	return param;
};
Catalog.prototype.getProductDetailParam = function (){
	var	param = ['Page','ProductDetails','ItemCode',this.itemCode];

	return param;
};

Catalog.prototype.getThankYouParam = function (){

	var	param = ['Page','Catalogue','Type','PromotionCampaign'];

	return param;
};

Catalog.prototype.updateProductInfo = function(product)
{
	var obj = new Object();	
	copyObjectWithNull(product,obj);

//	obj.code = product.code;
//	obj.description = product.description;
//	obj.defaultSalesUnit = product.defaultSalesUnit;
//	obj.actualPrice = product.actualPrice;
//	obj.currencyCode = product.currencyCode;
//	obj.imageURL = product.imageURL;
//	obj.isBuyingAllowed = product.isBuyingAllowed;
//	obj.isMatrixBaseItem = product.isMatrixBaseItem;
//	obj.availabilityOfItem = product.availabilityOfItem;
//	obj.enquiryImage = product.enquiryImage;
//	obj.itemWebText = product.itemWebText;
//	obj.availQty = product.availQty;
//	obj.sortPrice = product.sortPrice;
//	obj.salesUnits = product.salesUnits;
//	obj.productUnitList = product.productUnitList;

	obj.buyQuantity = null;
	obj.selectedUnit = product.defaultSalesUnit;

	return  obj; 	
};

Catalog.prototype.getUpdatedProductList = function(list)
{
	var newList =  [];
	if(!isNull(list)){
		for (var i=0;i<list.length ; i++){
			var  item  = list[i];
			newList.push(this.updateProductInfo(item));					
		}

	}
	return newList;
};

Catalog.prototype.getUpdatedProductData= function(product,productSalesUnit)
{
	var list= this.productList;
	if(!isNull(list)){
		for (var i=0;i<list.length ; i++){
			if(list[i].code == product.code)
			{
				list[i].selectedUnit = productSalesUnit;
			}
		}
	}

	return list;
};

Catalog.prototype.getCatalogTableHeader = function(){
	return this.catalogTableHeader;
};

Catalog.prototype.getCatalogColumnsToCheck = function(){
	var  columnsToCheck = ['actualPrice','discountPrice','discountPercentage','isBuyingAllowed','availabilityOfItem','enquiryImage'];
	return columnsToCheck;
};

Catalog.prototype.setCatalogTableHeader = function(list){
	var columnsToCheck = this.getCatalogColumnsToCheck();
	var isBuyAllowed = null;
	if(!isNull(list)){
		for (var i=0;i<list.length ; i++){
			var  item  = list[i];
			setTableHeader(item,columnsToCheck,this.getCatalogTableHeader());
			if(isBuyAllowed==null && item.isBuyingAllowed){
				isBuyAllowed = true;
			}
		}
		//set table header for buy
		if(isBuyAllowed){
			this.getCatalogTableHeader().isBuyingAllowed=true;
		}else{
			this.getCatalogTableHeader().isBuyingAllowed=false;
		}
	}

};

Catalog.prototype.setCatalog = function(catalog,appendToCatalog){  
	if(appendToCatalog)
	{
		var productList = this.getUpdatedProductList(catalog.productList);
		appendItemsToList(productList,this.productList);						
		setCatalogExtendedData(this.productList);
	}
	else
	{
		this.catalogData = catalog;				
		this.productList = this.getUpdatedProductList(catalog.productList);
		setCatalogExtendedData(this.productList);
		this.setCatalogTableHeader(this.productList);
	}
};

Catalog.prototype.getCatalogProductList = function(){
	return this.productList;
};

Catalog.prototype.setPageIndex = function(pageIndex,isNextPage)
{
	if(isNextPage)
	{
		this.pageIndex += 1;
	}
	else
	{
		if(this.pageIndex <=1){
			this.pageIndex = 1;
		}
		else
		{
			this.pageIndex -= 1;
		}
	}
};

Catalog.prototype.getPageIndex = function()
{
	return this.pageIndex;
};