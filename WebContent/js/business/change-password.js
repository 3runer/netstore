function ChangePassword(userId, isForcePasswordChange,callState,langCode) {
	this.userId=userId;
	this.callState = callState;
	this.oldPwd = "";
	this.newPwd = "";
	this.confNewPwd = "";
	this.successCallBackFn;
	this.isForcePasswordChange = isForcePasswordChange;
	this.isPwdChangedSuccessfully = false;
	this.languageCode = "";
	if(!isNull(langCode)){
	this.languageCode = langCode;
	}
	
	
	this.init();
};

ChangePassword.prototype.init = function(){
	if(!isNull(this.isForcePasswordChange) && this.isForcePasswordChange){
		this.errorMsg = translate.getTranslation('TXT_CF_001');
	}
};

ChangePassword.prototype.setSuccessCallBack = function(successCallBackFn){
	this.successCallBackFn = successCallBackFn;
};

ChangePassword.prototype.checkValues = function(){
	var flag = false;
	flag = !isEmptyString(this.oldPwd) && !isEmptyString(this.newPwd) && !isEmptyString(this.confNewPwd) 
	&& (this.newPwd == this.confNewPwd) && (this.newPwd.length > 2) ;

	return flag;
};

ChangePassword.prototype.getParams = function(){
	var	params = null;
	if(this.isForcePasswordChange){
		params = ["userID",this.userId,"oldPW",this.oldPwd,"newPW",this.newPwd, "confPW",this.confNewPwd];
	}else{
		params = ["oldPW",this.oldPwd,"newPW",this.newPwd, "confPW",this.confNewPwd];
	}
	return params;
};

ChangePassword.prototype.setResponse = function (responseObj) {
	//console.log("create new account response " + JSON.stringify(responseObj));
	if(!isNull(responseObj) && !isNull(responseObj.messageCode) ){
		var messageCode = responseObj.messageCode + "";
		switch(messageCode){
		case "6000" : 
			//6000 - PASSWORD SUCCESSFULLY CHANGED
			this.errorMsg = null;
			this.isPwdChangedSuccessfully = true;
			if(!isNull(this.successCallBackFn)){
				//this.successCallBackFn();
			}
			break;
		case "6001" : 
			//6001 - Unexpected error occurred
			this.errorMsg = translate.getTranslation('MSG_PASSWORD_UNEXPECTED_ERROR');
			this.isPwdChangedSuccessfully = false;
			break;
		case "6002" : 
			//6002 - OLD_PASSWORD_NOT_CORRECT
			this.errorMsg = translate.getTranslation('MSG_OLD_PASSWORD_NOT_CORRECT');
			this.isPwdChangedSuccessfully = false;
			break;
		case "6003" : 
			//6003 - MSG_PASSWORD_NOT_CORRECT
			this.errorMsg = translate.getTranslation('MSG_PASSWORD_NOT_CORRECT');
			this.isPwdChangedSuccessfully = false;
			break;
		case "6004" : 
			//6004 - MSG_PASSWORD_CONTAINS_INVALID_CHARACTERS
			this.errorMsg = translate.getTranslation('MSG_PASSWORD_CONTAINS_INVALID_CHARACTERS');
			this.isPwdChangedSuccessfully = false;
			break;
		}
	}
};

ChangePassword.prototype.getRequestParamForPost = function () {
	var	params = null;
	//ChangePasswordData=[{"userID":"ANIL","oldPassword":"ANIL1","newPassword":"ANIL2","confirmPassword":"ANIL2"}]
	var reqObj = {};
	reqObj.oldPassword  = this.oldPwd;
	reqObj.newPassword  = this.newPwd;
	reqObj.confirmPassword  = this.confNewPwd;
	if(this.isForcePasswordChange){
		reqObj.userID  = this.userId;
		reqObj.languageCode = this.languageCode;
		params = [reqObj];
		params = "ChangePasswordData="+JSON.stringify(params);
	}else{
		params = [reqObj];
		params = "ChangePasswordData="+JSON.stringify(params);
	}
	return params;
};