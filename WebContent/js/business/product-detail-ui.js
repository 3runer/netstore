function ProductDetailUI(rootScope){
		this.prdBaseDetail = null;
		this.prdInfo = null;
		this.requestItemCode=null;
		this.selectedUnit=null;
		this.selectedSeg2 = null;
		this.selectedSeg3 = null;
		this.matrixRowList = null;
		this.matrixColumnList = null;
		//this.rootScope = rootScope;
		this.quantity = null;
		//ProductDetailDynamicInfo - is to address diffrent kind of items like matrix based, component based or standard/normal item.
		//this.prdInfo = new ProductDetailDynamicInfo();
	};
	
	ProductDetailUI.prototype.updateProductInfoForMatrix = function(matrixItem){
	//	var matrixItem = this.getItemMatrixItem(this.selectedSeg2, this.selectedSeg3);
		if(!isNull(matrixItem)){
			this.prdInfo.itemCode = matrixItem.itemCode;
			this.prdInfo.itemDesc = matrixItem.itemDesc;
			this.prdInfo.itemGroup = matrixItem.itemGroup;
			this.prdInfo.isBuyAllowed = matrixItem.isBuyAllowed;
			this.prdInfo.availabilityOfItem = matrixItem.availabilityOfItem;
			this.prdInfo.actualPrice =  matrixItem.actualPrice;
			this.prdInfo.discountPrice = matrixItem.discountPrice;
			this.prdInfo.discountPercentage = matrixItem.discountPercentage;
			
			this.setExtendedData();
//			this.prdInfo.showAvailabilityAsLink = showAvailabilityAsLink();
//			this.prdInfo.showAvailabilityAsImage = showAvailabilityAsImage();
//			this.prdInfo.availabilityImageName = getAvailabilityImage(this.prdInfo.availabilityOfItem);
//			this.prdInfo.availabilityText = getAvailabilityText(this.prdInfo.availabilityOfItem);
//			handleEmptyAvailabilityImage(this.prdInfo);
		}
	};
	
	ProductDetailUI.prototype.updateProductInfoForComponent= function(){
		//as of now no need to implement this method. use updateProductInfoForNormal()
	};
	
	ProductDetailUI.prototype.updateProductInfoForNormal= function(){
		if(!isNull(this.prdBaseDetail)){
			this.prdInfo.itemCode = this.prdBaseDetail.itemCode;
			this.prdInfo.itemDesc=this.prdBaseDetail.itemDesc;
			this.prdInfo.itemGroup=this.prdBaseDetail.itemGroup;
			this.prdInfo.isBuyAllowed = this.prdBaseDetail.isBuyAllowed;
			this.prdInfo.availabilityOfItem = this.prdBaseDetail.availabilityOfItem;
			this.prdInfo.actualPrice =  this.prdBaseDetail.actualPrice;
			this.prdInfo.discountPrice = this.prdBaseDetail.discountPrice;
			this.prdInfo.discountPercentage = this.prdBaseDetail.discountPercentage;
			
			this.setExtendedData();
//			this.prdInfo.showAvailabilityAsLink = showAvailabilityAsLink();
//			this.prdInfo.showAvailabilityAsImage = showAvailabilityAsImage();
//			this.prdInfo.availabilityImageName = getAvailabilityImage(this.prdInfo.availabilityOfItem);
//			this.prdInfo.availabilityText = getAvailabilityText(this.prdInfo.availabilityOfItem);
		}
	};
	
	ProductDetailUI.prototype.setExtendedData = function(){
		if(!isNull(this.prdInfo)){
			this.prdInfo.showAvailabilityAsLink = showAvailabilityAsLink();
			this.prdInfo.showAvailabilityAsImage = showAvailabilityAsImage();
			this.prdInfo.availabilityImageName = getAvailabilityImage(this.prdInfo.availabilityOfItem);
			this.prdInfo.availabilityText = getAvailabilityText(this.prdInfo.availabilityOfItem);
			handleEmptyAvailabilityImage(this.prdInfo);
		}
	};
	
	ProductDetailUI.prototype.setMatrixChildExtendedData = function(item){
		if(!isNull(item)){
			item.showAvailabilityAsLink = showAvailabilityAsLink();
			item.showAvailabilityAsImage = showAvailabilityAsImage();
			item.availabilityImageName = getAvailabilityImage(item.availabilityMatrixItem);
			item.availabilityText = getAvailabilityText(item.availabilityMatrixItem);
			handleEmptyAvailabilityImage(this.prdInfo);
		}
	};
	
	ProductDetailUI.prototype.getSelectedUnit = function(){
		if(!isNull(this.selectedUnitObject) && !isNull(this.selectedUnitObject.salesUnit)){
			this.selectedUnit = this.selectedUnitObject.salesUnit;
		}
		return this.selectedUnit;
	};
	
	ProductDetailUI.prototype.setSelectedUnitObj = function(selectedUnit,unitList){
		if(!this.hasOwnProperty("selectedUnitObject")){
			this.selectedUnitObject = getSelectedSalesUnit(selectedUnit,unitList);
			this.selectedUnit = this.selectedUnitObject.salesUnit;
		}
	};
	
	ProductDetailUI.prototype.setSelectedUnit = function(selectedUnit){
		this.selectedUnit = selectedUnit;
	};
	
	ProductDetailUI.prototype.getUnits = function(){
		return this.prdBaseDetail.unitCodes;
	};
	
	ProductDetailUI.prototype.setRequestItemCode = function(itemCode){
		this.requestItemCode = itemCode;
	};
	
	ProductDetailUI.prototype.getRequestItemCode = function(){
		return this.requestItemCode ;
	};
	
	ProductDetailUI.prototype.setPrdBaseDetail = function(prdBaseDetail){
		this.prdBaseDetail = prdBaseDetail;
		this.prdInfo = prdBaseDetail;
		//this.setSelectedUnit(this.prdBaseDetail.salesUnit);
		this.setSelectedUnitObj(this.prdBaseDetail.salesUnit,this.prdBaseDetail.unitCodesDesc);
		this.setExtendedData();
	};
	
	ProductDetailUI.prototype.setChildDataResponse = function(childDataResponse){
		this.prdInfo = childDataResponse;
		this.setExtendedData();
		//this.setSelectedUnit(childDataResponse.salesUnit);
		this.setSelectedUnitObj(childDataResponse.salesUnit,childDataResponse.unitCodesDesc);
	};
	
	ProductDetailUI.prototype.getParams = function(){
		return this.makeParams(this.getRequestItemCode(), this.getSelectedUnit());
	};
	
	ProductDetailUI.prototype.makeParams = function(itemCode,unit){
		var requestParams = [];
		if(!isNull(itemCode) && !isEmptyString(itemCode)){
			requestParams = ["ITEM_CODE",itemCode];
			if(!isNull(unit) && !isEmptyString(unit)){
				requestParams.push("UNIT_CODE");
				requestParams.push(unit);
			}
		}
		return requestParams;
	};
	
	ProductDetailUI.prototype.getParamsForMatrixItem = function(){
		var matrixcode= this.getItemMatrixCode(this.selectedSeg2, this.selectedSeg3);
		this.setRequestItemCode(matrixcode);
		return this.makeParams(matrixcode, this.getSelectedUnit());
	};
	
	ProductDetailUI.prototype.getParamsForMatrixTableItem = function(matrixCode){		
		return this.makeParams(matrixCode, this.getSelectedUnit());
	};
	
	//seg2 - verticalTemplateName, seg3 - columnTemplateName
	ProductDetailUI.prototype.getItemMatrixCode =  function(seg2,seg3){
		var matrixCode = this.getItemMatrixItem(seg2, seg3);
		return matrixCode.itemMatrixCode;
	};
	
	//seg2 - verticalTemplateName, seg3 - columnTemplateName
	ProductDetailUI.prototype.getItemMatrixItem =  function(seg2,seg3){
		var matrixItem = null;
		//console.log("seg2 = " +JSON.stringify(seg2) + " seg3 = " + JSON.stringify(seg3));
		if(!isNull(this.prdBaseDetail.matrixItemDetailList[0])){
			var childList =  this.prdBaseDetail.matrixItemDetailList[0].childMatrixList;
			if(Array.isArray(childList)){
				for(var i=0;i<childList.length ; i++){
					var item  = childList[i];
					//console.log("item = " +JSON.stringify(item));
					if(isEqualStrings(item.verticalTemplateName,seg2.key) && isEqualStrings(item.columnTemplateName,seg3.key)){
						matrixItem = item;
						break;
					}
				}
			}
		}
		return matrixItem;
	};
	
	ProductDetailUI.prototype.getMatrixColumnList= function(){
		return this.matrixColumnList;
	};
	
	ProductDetailUI.prototype.setMatrixColumnList= function(matrixColumnList){
		this.matrixColumnList = matrixColumnList;
		if(Array.isArray(matrixColumnList) && matrixColumnList.length>0 ){
			this.selectedSeg3 = matrixColumnList[0];
		}
	};
	
	ProductDetailUI.prototype.getMatrixRowList= function(){
		return this.matrixRowList;
	};
	
	ProductDetailUI.prototype.setMatrixRowList= function(matrixRowList){
		this.matrixRowList = matrixRowList;
		if(Array.isArray(matrixRowList) && matrixRowList.length>0){
			this.selectedSeg2 = matrixRowList[0];
		}
	};