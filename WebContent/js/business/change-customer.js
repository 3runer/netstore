function ChangeCustomer(){
	this.CONST_TYPE_SESSION = "session";
	this.CONST_TYPE_DEFAULT = "default";
	this.isChangeCustomerEnabled = true;
	this.isOtherCustSelected = false;
	this.selectedCustomer = null;
	//this.selectedCustomer.selectedType = null;
	this.customerList = [];
	this.deftCust = null;
	this.sort = new Sort();
	this.isfilterOn = false;
	this.filterList =  null;
	
	this.selectedCustCode = null;
	this.request = {};
	this.request.CustomerCode=null;
	this.request.Type = null;
	this.dataFound = true;
	this.isChangeCustomerSuccessful = false;
	this.databaseDefaultCustCode = null;
	
	this.typeSession = {};
	this.typeSession.id = this.CONST_TYPE_SESSION;
	this.typeSession.value = translate.getTranslation('CON_FOR_THIS_SESSION'); 
	this.typeDefault = {};
	this.typeDefault.id = this.CONST_TYPE_DEFAULT;
	this.typeDefault.value = translate.getTranslation('CON_AS_DEFAULT_CUSTOMER');  
	this.typeList =[this.typeSession ,this.typeDefault];
	this.paginationObj = null;

};

ChangeCustomer.prototype.getChangeCustomerParams = function(){
	var params = [];
	if(!isNull(this.selectedCustomer) && !isNull(this.selectedCustomer.code)  && !isNull(this.selectedCustomer.changeType) ){
		params.push("CustomerCode");
		params.push(this.selectedCustomer.code);
		params.push("Type");
		params.push(this.selectedCustomer.changeType.id);
	}
	return params;
};

ChangeCustomer.prototype.isRequestForDefaultType = function(){
	if(this.CONST_TYPE_DEFAULT == this.selectedCustomer.changeType.id){
		return true;
	}else{
		return false;
	}
};

ChangeCustomer.prototype.isChangeEnabled = function(){
	var flag = true;
	if(!isNull(this.selectedCustomer) && this.selectedCustomer.defaultCustomer){
		flag =  false;
	};
	return flag;
};

ChangeCustomer.prototype.setDataFound = function(custList,isFirstPageData){
	if(Array.isArray(custList) && custList.length > 0){
		this.dataFound = true;
	}else if(!isFirstPageData){
		this.dataFound = false;
	}
};

ChangeCustomer.prototype.setPageLoadMsg = function(isFirstPageData){
	if(!isFirstPageData && !this.dataFound){
		this.pageLoadMsg = translate.getTranslation('CON_NO_MORE_PAGE');
	}else{
		this.pageLoadMsg = "";
	}
};

ChangeCustomer.prototype.updateFilterStatus = function(list,isFirstPageData){
	if(Array.isArray(list) && list.length > 0 && isFirstPageData){
		this.filterList.cacheFilters();
		//this.filterMsg = null;
	}else if(this.isfilterOn && isFirstPageData){
		//this.filterMsg = translate.getTranslation('MSG_NO_OBJECTS_FOR_SELECTION');
		this.filterList.applyPreviousFilters();
		//this.paginationObj = this.lastPaginationObj;
		showMsgNoObjFound();
	}
};

ChangeCustomer.prototype.setChangeCustomerAllowed = function (custList,isFirstPageData){
	this.isChangeCustomerEnabled = true;
	//if(isFirstPageData ){
		if ( !Array.isArray(custList) || ((custList.length < 2) && (custList[0].defaultCustomer) )){
			
			this.isChangeCustomerEnabled = false;
		}
		 // NST-2192
        if(Array.isArray(this.customerList)){
         if(( this.customerList.length >= 2 ) ||(!(this.customerList[0].defaultCustomer) && (this.customerList.length < 2 ))){
        	 this.isChangeCustomerEnabled = true;
        	}  else if((this.customerList[0].defaultCustomer) && (this.customerList.length < 2 )){
            this.isChangeCustomerEnabled = false;
        	} 
        }
};

ChangeCustomer.prototype.setCustomerList = function(custList,isFirstPageData){
	if(Array.isArray(custList) && custList.length > 0 ){
		//retain last response data
		this.customerList = custList;
		this.setChangeTypeForAll();
	}else{
		if(!isFirstPageData){
			this.customerList = null;
		}
	}
	this.setChangeCustomerAllowed(custList,isFirstPageData);
	this.setDataFound(custList,isFirstPageData);
	this.setPageLoadMsg(isFirstPageData);
	this.updateFilterStatus(custList,isFirstPageData);
};

ChangeCustomer.prototype.setDatabaseDefaultCustCode = function(databaseDefaultCustCode){
	this.databaseDefaultCustCode = databaseDefaultCustCode;
};

ChangeCustomer.prototype.addToCustomerList = function(custList){
	appendItemsToList(custList,this.customerList);
	this.setDataFound(custList);
	this.setChangeTypeForAll();
};

ChangeCustomer.prototype.setFilterList = function(filterList){
	this.filterList = filterList;
};

ChangeCustomer.prototype.setDefaultCustomer = function(customer){
	if(!isNull(customer)){
		this.deftCust = customer;
		this.selectedCustomer = customer;
		this.selectedCustCode = customer.code;
		this.setSelectedChangeType (this.selectedCustomer);
	}
};

ChangeCustomer.prototype.setChangeTypeForAll = function(){
	if(Array.isArray(this.customerList)){
		for(var i=0 ; i<this.customerList.length ; i++){
			this.setSelectedChangeType(this.customerList[i]);
		}
	}
};

ChangeCustomer.prototype.setSelectedChangeType = function(customer){
	if(!isNull(customer) && this.selectedCustCode == customer.code){
		if(customer.code == this.databaseDefaultCustCode){
			customer.changeType = this.typeDefault;
		}else{
			customer.changeType = this.typeSession;
		}
	}else{
		customer.changeType = this.typeSession;
	}
};

ChangeCustomer.prototype.setPaginationObj = function(paginationObj){
//	if (!isNull(this.paginationObj)){
//		this.lastPaginationObj = deepCopy(this.paginationObj);
//	}
	this.paginationObj = paginationObj;
};

ChangeCustomer.prototype.setCustomerSelected = function(customer){
	this.selectedCustomer = customer;
	this.isOtherCustSelected = true;
};

ChangeCustomer.prototype.setCustomerNextPage = function(){
	this.isOtherCustSelected = false;
};

ChangeCustomer.prototype.getDefaultCustomer = function(){
	return this.deftCust ;
};

ChangeCustomer.prototype.getSearchCustParams = function(){
	var params  = [];
	if(this.isfilterOn){
		params = this.sort.addSortParams(this.filterList.getParamsList());
	}else{
		params = this.sort.getParam();
	}
	return params;
};

ChangeCustomer.prototype.setResponse = function(response){
	if(!isNull(response) && !isNull(response.messageCode)){
		var msgCode = response.messageCode + "";
		switch(msgCode){
		case "4250" :
			//Default Customer Changed Successfully
			this.isChangeCustomerSuccessful = true;
			this.errorMsg = null;
			break;
		case "4251" :
			//Customer Change Not Allowed
			this.errorMsg = translate.getTranslation('MSG_CUSTOMER_CHANGE_NOT_ALLOWED');
			this.isChangeCustomerSuccessful = false;
			break;
		case "4252" :
			//Customer Code Not Available
			this.errorMsg = translate.getTranslation('MSG_CUSTOMER_CODE_NOT_AVAILABLE');
			this.isChangeCustomerSuccessful = false;
			break;
		case "9002" :
			//Customer has login restriction ADM-006
			this.errorMsg = translate.getTranslation('MSG_ACCESS_DENIED_FOR_CUSTOMER_RESTRICTION');
			this.isChangeCustomerSuccessful = false;
			break;
		default :
			this.errorMsg = null;
			this.isChangeCustomerSuccessful = false;
		}
	}
};

ChangeCustomer.prototype.getAddressStr = function(cust){
	var address  = "";
	if(!isEmptyString(cust.address1)){
		address = cust.address1 +", " ;
	}
	if(!isEmptyString(cust.address2)){
		address = address + cust.address2 +", " ;
	}
	if(!isEmptyString(cust.address3)){
		address = address + cust.address3 +", " ;
	}
	if(!isEmptyString(cust.address4)){
		address = address + cust.address4 +", " ;
	}
	if(!isEmptyString(cust.postalCode)){
		address = address + cust.postalCode +", " ;
	}
	if(!isEmptyString(cust.countryCode)){
		address = address + cust.countryCode ;
	}
	return address;
};

ChangeCustomer.prototype.getDefaultAddressStr = function(){
	var address  = "";
	if(!isNull(this.deftCust)){
		var cust = this.deftCust;
		if(!isEmptyString(cust.address1)){
			address = cust.address1 + " ";
		}
		
		if(!isEmptyString(cust.address2)){
			address = address + cust.address2 +"<br>" ;
		}else{
			address = address +"<br>" ;
		}
		
		if(!isEmptyString(cust.address3)){
			address = address + cust.address3 +" " ;
		}
		if(!isEmptyString(cust.address4)){
			address = address + cust.address4 +" " ;
		}
		if(!isEmptyString(cust.postalCode)){
			address = address + cust.postalCode +"<br>" ;
		}else{
			address = address +"<br>" ;
		}
		
		if(!isEmptyString(cust.countryCode)){
			address = address + cust.countryCode ;
		}
	}

	return address;
};