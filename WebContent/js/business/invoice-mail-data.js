function InvoiceMailData(orderNumber,documentType,invoiceNumber,invoiceYear){
		this.orderNumber = orderNumber;
		this.documentType = documentType;
		this.invoiceNumber = invoiceNumber;
		this.year = invoiceYear;
	    this.mailFrom = "";
	    this.mailTo = "";
	    this.subject = "";
	    this.body =  "";
	};
	
	InvoiceMailData.prototype.setEmailProperties = function(emailObject){
		this.mailFrom = emailObject.mailFrom;
	    this.mailTo =emailObject.mailTo;
	    this.subject = emailObject.subject;
	    this.body =  emailObject.body;
	};