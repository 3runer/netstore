function CredentialInfo(translateFunc)
	{
		this.translateFunc = translateFunc;
		this.credentialInfo = {};
		this.credentialInfo['oldPassword'] = "";
		this.credentialInfo['newPassword'] = "";
		this.credentialInfo['confirmPassword'] = "";
		this.UIErrorKey= null;
		
	}
	CredentialInfo.prototype.clearAll = function()
	{
	
		this.credentialInfo['oldPassword'] = "";
		this.credentialInfo['newPassword'] = "";
		this.credentialInfo['confirmPassword'] = "";
	};
	
	CredentialInfo.prototype.updateInfo = function(responseData)
	{
		var errorMsg = null;
		if(!isNull(responseData.messageCode))
		{
			switch (responseData.messageCode){
			case 6000 :
				errorMsg = 'MSG_PASSWORD_SUCCESSFULLY_CHANGED';
				this.clearAll();
				break;
			case 6001 :
				errorMsg = 'MSG_PASSWORD_UNEXPECTED_ERROR';
				break;
			case 6002 :
				errorMsg = 'MSG_OLD_PASSWORD_NOT_CORRECT';
				break;
			case 6003 :
				errorMsg = 'MSG_PASSWORD_NOT_CORRECT';
				break;
			case 6004 :
				errorMsg = 'MSG_PASSWORD_CONTAINS_INVALID_CHARACTERS';
				break;
			}
		}	
		
		this.UIErrorKey = this.translateFunc(errorMsg);
	};