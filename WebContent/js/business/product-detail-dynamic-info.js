function ProductDetailDynamicInfo(){
		this.itemcode = null;
		this.itemDesc=null;
		this.itemGroup=null;
		this.isBuyAllowed = null;
		this.availabilityOfItem = null;
		this.actualPrice =  null;
		this.discountPrice = null;
		this.discountPercentage = null;
		this.showAvailabilityAsLink = null;
		this.showAvailabilityAsImage = null;
		this.availabilityImageName = null;
		this.availabilityText = null;
	};