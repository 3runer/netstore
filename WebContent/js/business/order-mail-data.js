function OrderMailData(orderNumber){
		this.orderNumber = returnBlankIfNull(orderNumber);
	    this.mailFrom = "";
	    this.mailTo = "";
	    this.subject = "";
	    this.body =  "";
	};
	
	OrderMailData.prototype.setEmailProperties = function(emailObject){
		this.mailFrom = emailObject.mailFrom;
	    this.mailTo =emailObject.mailTo;
	    this.subject = emailObject.subject;
	    this.body =  emailObject.body;
	};