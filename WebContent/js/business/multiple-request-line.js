/* creating the Request Line object */
function MutlipleRequestLine(requestTypeList, requestLineList,
		rootScope/* ADM-XXX */, translateFunc, reasonCodeList) {
	this.requestLineList = requestLineList;
	this.newRequestLineList = [];
	this.blankLinesToAdd = 5;
	this.requestTypeList = requestTypeList;
	this.UIErrorKey = null;
	this.requestParam = "";
	// this.rootScope = rootScope;
	this.showUnitList = false;
	this.isValidProductList = false;
	this.tempRequestLineList = [];
	this.translateFunc = translateFunc;
	this.showRequestLines = false;
	// ADM-XXX
	this.webSettings = rootScope.webSettings;
	this.reasonCodeList = reasonCodeList;
	this.collapsed_edit = true;
};

MutlipleRequestLine.prototype.getUIErrorKey = function() {
	return this.UIErrorKey;
};

MutlipleRequestLine.prototype.getUIErrorParams = function() {
	return this.UIErrorParams;
};

/* To create a blank request line object */
MutlipleRequestLine.prototype.createBlankObject = function(index) {
	var obj = new Object();
	obj.rowNum = index;
	obj.lineNumber = null;
	obj.invoiceLineNumber = null;
	obj.productDescription = null;
	obj.product = null;
	obj.quantity = null;
	obj.unit = null;
	obj.probType = null;
	obj.requestDesc = null;
	obj.requestTypeList = this.requestTypeList;
	obj.showUnitList = false;
	obj.activeUnitList = [];
	obj.isValidProduct = true;
	obj.validQuantity = true;
	obj.UIErrorKey = null;
	obj.errorMsg = "";
	obj.itemNumberOfDecimalsAllowed = "";
	// ADM-XXX
	obj.orderNumber = null;
	obj.orderLineNumber = null;
	obj.batch = null;
	obj.expireDate = null;
	obj.serialNo = null;
	obj.reasonCode = null;
	obj.reasonCodeList = this.reasonCodeList;
	obj.validDate = true;
	// ADM-XXX End
	return obj;
};

/* To get the new request lines */
MutlipleRequestLine.prototype.getLineList = function() {
	return this.newRequestLineList;
};

/* To initialize the request line object */
MutlipleRequestLine.prototype.createObject = function(
		itemNumberOfDecimalsAllowed, lineNumber, productCode, quantity, unit,
		requestType, lineDescription, invoiceLineNumber, description, unitList,
		restricted/* ADM-XXX */, orderNumber, orderLineNumber, batch,
		expireDate, serialNo, reasonCode) {
	var obj = new Object();
	obj.lineNumber = returnBlankIfNull(lineNumber) + "";
	obj.invoiceLineNumber = returnZeroIfNull(invoiceLineNumber);
	obj.product = returnBlankIfNull(productCode);
	obj.productDescription = returnBlankIfNull(description);
	obj.quantity = quantity;
	obj.unit = unit;
	obj.probType = returnBlankIfObjectNull(requestType, 'code');
	obj.selctedProbType = requestType;
	obj.requestDesc = returnBlankIfNull(lineDescription);
	obj.requestTypeList = this.requestTypeList;
	obj.showUnitList = false;
	obj.activeUnitList = unitList;
	obj.isValidProduct = true;
	obj.validQuantity = true;
	obj.UIErrorKey = null;
	obj.errorMsg = "";
	obj.itemNumberOfDecimalsAllowed = itemNumberOfDecimalsAllowed;
	obj.restricted = returnBlankIfNull(restricted);
	// ADM-XXX
	obj.orderNumber = returnZeroIfNull(orderNumber);
	obj.orderLineNumber = returnZeroIfNull(orderLineNumber);
	obj.batch = returnBlankIfNull(batch);
	obj.expireDate = returnBlankIfNull(expireDate);
	obj.serialNo = returnBlankIfNull(serialNo);
	obj.reasonCode = returnBlankIfObjectNull(reasonCode, 'code');
	obj.selectedReasonCode = reasonCode;
	obj.reasonCodeList = this.reasonCodeList;
	obj.validDate = true;
	// ADM-XXX End
	return obj;
};

/* To draw the five blank rows in Add product table */
MutlipleRequestLine.prototype.init = function() {
	if (isNull(this.newRequestLineList) || this.newRequestLineList.length == 0) {
		this.newRequestLineList = [];
		for ( var i = 0; i < this.blankLinesToAdd; i++) {
			this.newRequestLineList.push(this.createBlankObject(i));
		}
		this.isValidProductList = false;
		this.UIErrorKey = null;
	}
};

MutlipleRequestLine.prototype.updateRequestLines = function() {
	var newList = this.newRequestLineList;
	if (!isNull(newList)) {
		for ( var i = 0; i < newList.length; i++) {
			newList[i].probType = newList[i].selctedProbType;
			newList[i].itemNumberOfDecimalsAllowed = newList[i].itemNumberOfDecimalsAllowed;
		}
	}
};

MutlipleRequestLine.prototype.setUnitList = function(list) {
	this.activeUnitList = list;
};

/* To add the five blank rows in Add product table */
MutlipleRequestLine.prototype.addMoreLines = function() {
	var lastIndex = this.newRequestLineList.length;
	for ( var i = 0; i < this.blankLinesToAdd; i++) {
		this.newRequestLineList.push(this.createBlankObject(lastIndex));
		lastIndex += 1;
	}
};

/*
 * To clear all the row entries in Add product table and making 5 new request
 * line rows
 */
MutlipleRequestLine.prototype.clearAll = function() {
	this.blankLinesToAdd = 5;
	this.newRequestLineList = [];
	this.addMoreLines();
};

MutlipleRequestLine.prototype.clear = function() {
	this.newRequestLineList = [];
};

MutlipleRequestLine.prototype.isRequestLineEmpty = function() {
	var isEmpty = false;
	if (this.newRequestLineList.length <= 0) {
		isEmpty = true;
	}
	return isEmpty;
};

/* To get the new request lines list product/invoice/order */
MutlipleRequestLine.prototype.getRequestLines = function() {
	var requestLinesParam = "";
	var tempList = this.newRequestLineList;
	var requestLineList = this
			.createtNewRequestLineList(this.newRequestLineList);
	var requestLineListValidate = this
			.createtNewRequestLineListValidate(this.newRequestLineList);
	var isRequestLineListValid = this.validateRequestLineList(requestLineList);
	if (isRequestLineListValid) {
		// ADM-XXX
		// requestLinesParam =
		// "requestLineData="+JSON.stringify(replacer(requestLineListValidate,['selctedProbType','requestTypeList','showUnitList','activeUnitList','isValidProduct','UIErrorKey','errorMsg',
		// 'productDescription']));
		requestLinesParam = "requestLineData="
				+ JSON.stringify(replacer(requestLineListValidate, [
						'selctedProbType', 'requestTypeList', 'showUnitList',
						'activeUnitList', 'isValidProduct', 'UIErrorKey',
						'errorMsg'/* ADM-XXX */, 'reasonCodeList' ]));
		// ADM-XXX End
		this.newRequestLineList = requestLineList;
	} else {
		this.newRequestLineList = tempList;
	}
	this.requestParam = requestLinesParam;
	return requestLineList;
};

/*
 * To create the new request line list from the Add product table section for
 * request base - product/invoice/order
 */
MutlipleRequestLine.prototype.createtNewRequestLineList = function(
		requestLineList) {
	var itemsLinesObjList = [];
	var lineNumber = 10;
	if (!isNull(requestLineList)) {
		for ( var i = 0; i < requestLineList.length; i++) {
			var itemLineObj = new Object();
			if (this.isRequestLineValid(requestLineList[i].product,
					requestLineList[i].quantity))// ,requestLineList[i].probType))
			{
				if (isNull(requestLineList[i].probType)) {
					itemLineObj = this.createObject(
							requestLineList[i].itemNumberOfDecimalsAllowed,
							lineNumber, requestLineList[i].product,
							requestLineList[i].quantity,
							requestLineList[i].unit, null,
							requestLineList[i].requestDesc,
							requestLineList[i].invoiceLineNumber,
							requestLineList[i].productDescription,
							null/* ADM-XXX */, requestLineList[i].restricted,
							requestLineList[i].orderNumber,
							requestLineList[i].orderLineNumber,
							requestLineList[i].batch,
							requestLineList[i].expireDate,
							requestLineList[i].serialNo,
							requestLineList[i].reasonCode);
				} else {
					itemLineObj = this.createObject(
							requestLineList[i].itemNumberOfDecimalsAllowed,
							lineNumber, requestLineList[i].product,
							requestLineList[i].quantity,
							requestLineList[i].unit,
							requestLineList[i].probType,
							requestLineList[i].requestDesc,
							requestLineList[i].invoiceLineNumber,
							requestLineList[i].productDescription,
							requestLineList[i].activeUnitList/* ADM-XXX */,
							requestLineList[i].restricted,
							requestLineList[i].orderNumber,
							requestLineList[i].orderLineNumber,
							requestLineList[i].batch,
							requestLineList[i].expireDate,
							requestLineList[i].serialNo,
							requestLineList[i].reasonCode);
				}
				lineNumber = lineNumber + 10;
				itemsLinesObjList.push(itemLineObj);
			}
		}
	}
	return itemsLinesObjList;
};

MutlipleRequestLine.prototype.createtNewRequestLineListValidate = function(
		requestLineList) {
	var itemsLinesObjList = [];
	var lineNumber = 10;
	if (!isNull(requestLineList)) {
		for ( var i = 0; i < requestLineList.length; i++) {
			var itemLineObj = new Object();
			if (this.isRequestLineValid(requestLineList[i].product,
					requestLineList[i].quantity))// ,requestLineList[i].probType))
			{
				if (isNull(requestLineList[i].probType)) {
					itemLineObj = this.createObject(
							requestLineList[i].itemNumberOfDecimalsAllowed,
							lineNumber,
							encodeURIComponent(requestLineList[i].product),
							requestLineList[i].quantity,
							requestLineList[i].unit, null,
							requestLineList[i].requestDesc,
							requestLineList[i].invoiceLineNumber,
							requestLineList[i].productDescription,
							requestLineList[i].activeUnitList,
							// ADM-XXX
							requestLineList[i].restricted,
							requestLineList[i].orderNumber,
							requestLineList[i].orderLineNumber,
							requestLineList[i].batch,
							requestLineList[i].expireDate,
							requestLineList[i].serialNo,
							requestLineList[i].reasonCode);
				} else {
					itemLineObj = this.createObject(
							requestLineList[i].itemNumberOfDecimalsAllowed,
							lineNumber,
							encodeURIComponent(requestLineList[i].product),
							requestLineList[i].quantity,
							requestLineList[i].unit,
							requestLineList[i].probType,
							requestLineList[i].requestDesc,
							requestLineList[i].invoiceLineNumber,
							requestLineList[i].productDescription,
							requestLineList[i].activeUnitList,
							// ADM-XXX
							requestLineList[i].restricted,
							requestLineList[i].orderNumber,
							requestLineList[i].orderLineNumber,
							requestLineList[i].batch,
							requestLineList[i].expireDate,
							requestLineList[i].serialNo,
							requestLineList[i].reasonCode);
				}
				lineNumber = lineNumber + 10;
				itemsLinesObjList.push(itemLineObj);
			}
		}
	}
	return itemsLinesObjList;
};

/*
 * To create the new request line list from the selected reference lines
 * (order/invoice)
 */
MutlipleRequestLine.prototype.createtNewRequestLineFromList = function(
		requestLineList, isInvoiceType, isShoppingListType/* ADM-XXX */,
		isTransportNote) {
	var itemsLinesObjList = [];
	if (!isNull(requestLineList)) {
		for ( var i = 0; i < requestLineList.length; i++) {
			var itemLineObj = new Object();
			// ADM-XXX
			var line = requestLineList[i];
			if (isTransportNote) {
				itemLineObj = this.createObject(false, line.orderLineNumber,
						line.itemCode, null, line.unit, this.requestTypeList,
						null, null, line.itemDescription, null, null,
						line.orderNumber, line.orderLineNumber, null, null,
						null, this.reasonCodeList);
			}
			if (isInvoiceType) {
				itemLineObj = this.createObject(
						line.itemNumberOfDecimalsAllowed,
						line.invoiceLineNumber, line.item, line.quantity,
						line.unit, this.requestTypeList, null,
						line.invoiceLineNumber, line.itemDescription);
			}
			if (!isInvoiceType && !isShoppingListType/* ADM-XXX */
					&& !isTransportNote) {
				itemLineObj = this.createObject(
						line.itemNumberOfDecimalsAllowed, line.lineNumber,
						line.itemCode, line.ordered, line.unit,
						this.requestTypeList, null, null, line.itemDescription,
						line.itemUnits, line.restricted);
			}
			if (isShoppingListType) {
				itemLineObj = this.createObject(
						line.itemNumberOfDecimalsAllowed, line.lineNumber,
						line.code, line.quantity,
						line.selectedUnitObject.salesUnitDesc, null, null,
						null, null);
			}
			itemsLinesObjList.push(itemLineObj);
		}
	}
	return itemsLinesObjList;
};

/* To create the new request line list for order/invoice */
MutlipleRequestLine.prototype.createListFromSelection = function(list,
		isInvoiceType, isShoppingListType/* ADM-XXX */, isTransportNote) {
	var tempList = this.newRequestLineList;
	var requestLineList = this.createtNewRequestLineFromList(list,
			isInvoiceType, isShoppingListType/* ADM-XXX */, isTransportNote);
	if (isNull(requestLineList)) {
		this.newRequestLineList = tempList;
	} else {
		this.newRequestLineList = requestLineList;
		//ADM-XXX Begin
		if(!isNull(tempList)){
			for ( var i = 0; i < tempList.length; i++) {
				var line = tempList[i];
				this.newRequestLineList.push(line);
			}
		}
		//ADM-XXX End
	}
	this.setLineObjectStatus(this.newRequestLineList);
};

/* To validate the request lines entry in the Add product table */
MutlipleRequestLine.prototype.isRequestLineValid = function(itemCode,
		itemQuantity) {
	var isValid = true;
	if (isEmptyString(itemCode) && isEmptyString(itemQuantity)) {
		return false;
	}
	if (!isEmptyString(itemCode)) {
		itemCode = itemCode.trim();
	}
	if (!isEmptyString(itemQuantity)) {
		itemQuantity = itemQuantity.trim();
	}
	return isValid;
};

MutlipleRequestLine.prototype.validateRequestLine = function(reqLine, page) {
	var line = reqLine;
	// var rootScope = this.rootScope;
	// var requestParam = getItemCodeRequestParam(reqLine.product,page) ;
	// if(!isEmptyString(reqLine.product))
	// {
	// rootScope.fetchItemDetails(requestParam,reqLine,this);
	// }
	// else
	// {
	// this.UIErrorKey = null;
	// this.UIErrorParams = [];
	// }

};

MutlipleRequestLine.prototype.setLineObjectStatus = function(list) {
	if (!isNull(list)) {
		for ( var i = 0; i < list.length; i++) {
			list[i].isValidProduct = true;
			if (list[i].restricted == true) {
				var translateFunc = "";
				addUIError(list[i], translateFunc, 'MSG_INVALID_ITEM');
				list[i].isValidProduct = false;
				this.isValidProductList = false;
			}
		}
	}

};

MutlipleRequestLine.prototype.deleteLine = function(index) {
	var list = this.getLineList();
	if (Array.isArray(list) && list.length > 0 && index < list.length) {
		list.splice(index, 1);
	}
};

MutlipleRequestLine.prototype.EditLine = function(index) {
	var list = this.getLineList();
	var item = list[index];
	this.tempRequestLineList[index + ""] = {};
	// this.tempRequestLineList
	copyObjectWithNull(item, this.tempRequestLineList[index + ""]);
};

MutlipleRequestLine.prototype.CancelEditLine = function(index) {
	var list = this.getLineList();
	var item = list[index];
	copyObjectWithNull(this.tempRequestLineList[index + ""], item);
};

MutlipleRequestLine.prototype.validateProductList = function(lineListObject,
		page) {
	var rootScope = this.rootScope;
	var lineList = lineListObject.getLineList();
	// var paramList = this.getProductParamList(lineList,page);
	// var requestParam = getProductListRequestParam(paramList);
	//		
	// rootScope.fetchProductsDetails(requestParam,lineListObject);
	//	   
	//	   
};

MutlipleRequestLine.prototype.AddProductListToCart = function() {
	// var rootScope = this.rootScope;
	// rootScope.showNewRequestLine(this.getRequestLines());
};

MutlipleRequestLine.prototype.getProductParamList = function(lineObjectList,
		page) {
	var lineParamList = [];
	if (!isNull(lineObjectList)) {
		for ( var i = 0; i < lineObjectList.length; i++) {
			var lineParamObject = new Object();
			lineParamObject['itemCode'] = encodeURIComponent(lineObjectList[i].product);
			lineParamObject['unitCode'] = returnBlankIfNull(lineObjectList[i].unit);
			lineParamObject['page'] = page;
			if (!isNull(lineObjectList[i].product)) {
				lineParamList.push(lineParamObject);
			}
		}
	}
	return lineParamList;
};

var validateAllLines = function(lineObject) {
	var isValidObject = true;
	var list = lineObject.getLineList();
	if (list.length > 0) {
		for ( var i = 0; i < list.length; i++) {
			if (!list[i].isValidProduct) {
				isValidObject = false;
				break;
			}
		}
	}
	if (isValidObject) {
		lineObject.isValidProductList = true;
		setUIError(lineObject, null);
	} else {
		lineObject.isValidProductList = false;
	}
};

/* To validate the request lines list in the Add product table */
MutlipleRequestLine.prototype.validateRequestLineList = function(
		requestLineList) {
	var translateFunc = this.translateFunc;
	var isValidRequestLine = true;
	if (requestLineList.length > 0) {
		for ( var i = 0; i < requestLineList.length; i++) {
			var ordItem = this.newRequestLineList[i];
			if (isEmptyString(requestLineList[i].product)) {
				this.UIErrorKey = 'MSG_ITEM_NOT_SPECIFIED';
				this.UIErrorParams = [];
				isValidRequestLine = false;
				setUILineError(ordItem, translateFunc, 'MSG_ITEM_NOT_SPECIFIED');
				ordItem.isValidProduct = false;
			}
			/*
			 * ADM-XXX if(isEmptyString(requestLineList[i].probType )){
			 * this.UIErrorKey = 'MSG_REQUEST_TYPE_MUST_BE_SELECTED';
			 * this.UIErrorParams = []; isValidRequestLine = false;
			 * setUILineError(ordItem,translateFunc,'MSG_REQUEST_TYPE_MUST_BE_SELECTED');
			 * ordItem.isValidProduct = false; }
			 */
			if (isEmptyString(requestLineList[i].unit)) {
				setUIError(this, 'MSG_ITEM_INVALID_UNIT');
				isValidRequestLine = false;
				setUILineError(ordItem, translateFunc, 'MSG_ITEM_INVALID_UNIT');
				ordItem.isValidProduct = false;
			}

			if (isEmptyString(requestLineList[i].quantity)) {
				this.UIErrorKey = 'MSG_QUANTITY_NOT_SPECIFIED';
				this.UIErrorParams = [];
				setUILineError(ordItem, translateFunc,
						'MSG_QUANTITY_NOT_SPECIFIED');
				ordItem.isValidProduct = false;
				isValidRequestLine = false;
			} else {
				// var isNumber = isInt(requestLineList[i].quantity);
				var pattern = /^[0-9.,]*$/;

				if (!pattern.test(requestLineList[i].quantity)) {
					this.UIErrorKey = 'MSG_INVALID_QUANTITY';
					this.UIErrorParams = [];
					isValidRequestLine = false;
					setUILineError(ordItem, translateFunc,
							'MSG_INVALID_QUANTITY');
					ordItem.isValidProduct = false;
				}
			}

			// ADM-XXX
			if (!validateDate(requestLineList[i].expireDate,
					this.webSettings.defaultDateFormat)) {
				this.UIErrorKey = 'MSG_INVALID_DATE';
				this.UIErrorParams = [];
				isValidRequestLine = false;
				setUILineError(ordItem, translateFunc, 'MSG_INVALID_DATE');
				setUILineError(requestLineList[i], translateFunc,
						'MSG_INVALID_DATE');
				requestLineList[i].validDate = false;
				ordItem.isValidProduct = false;
			} else {
				requestLineList[i].validDate = true;
			}

			if (isValidRequestLine) {
				ordItem.isValidProduct = true;
			}
		}
	} else {
		isValidRequestLine = false;
		this.UIErrorKey = 'MSG_ITEM_NOT_SPECIFIED';
		this.UIErrorParams = [];
	}
	if (isValidRequestLine) {
		this.UIErrorKey = null;
		this.UIErrorParams = [];
	}
	return isValidRequestLine;
};
