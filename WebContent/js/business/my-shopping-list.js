//Shopping List Object
function MyShoppingList(ShoppingListService,translateFunc){		
	this.filterMsg = null;			
	this.currentList=null;
	this.pageDataFound = false;
	this.UIMessageKey=null;
	this.messageCode = "";				
	this.translateFunc = translateFunc;
	this.ShoppingListService = ShoppingListService;
	this.listHeader = {};
	this.newList = {};
	this.defaultList = "";
	this.itemsList = [];
	this.listHasLock = false;
	this.ErrOnControl = {};
	
	this.isfilterOn = false;
	this.filterList =  null;
	this.sort = new Sort();
	this.paginationObj = null;
	
};

MyShoppingList.prototype.updateFilterStatus = function(list,isFirstPageData){
	if(Array.isArray(list) && list.length > 0 && isFirstPageData){
		if(!isNull(this.filterList)){
		this.filterList.cacheFilters();}			
	}else if(this.isfilterOn && isFirstPageData){			
		this.filterList.applyPreviousFilters();			
		showMsgNoObjFound();
	}
};
MyShoppingList.prototype.getSearchParams = function(){
	var params  = [];
	if(this.isfilterOn){
		params = this.sort.addSortParams(this.filterList.getParamsList());
	}else{
		params = this.sort.getParam();
	}
	return params;
};	
MyShoppingList.prototype.setFilterList = function(filterList){
	this.filterList = filterList;
};
MyShoppingList.prototype.setPaginationObj = function(paginationObj){
	this.paginationObj = paginationObj;
};
MyShoppingList.prototype.setNewList= function(){
	this.newList.listId=null;
	this.newList.listDesc=null;
	this.newList.customerId=null;
	this.newList.listOwnerCode=null;
	this.newList.checked=null;
	this.UIMessageKey=null;
};

MyShoppingList.prototype.getItemsList = function(){
	return this.itemsList;
};

MyShoppingList.prototype.getDefaultList= function(){
	for(var i=0; i<this.itemsList.length ;i++){
		if(this.defaultList==this.itemsList[i].listId){
		    return  (this.itemsList[i]);
			break;
		}
	}
	// this.defaultList;
};
MyShoppingList.prototype.setItemList = function(itemList,data,isFirstPageData,refreshList){
	
	
	if(Array.isArray(itemList) && itemList.length > 0 ){
		//retain last response data
		this.itemsList = itemList;		
		this.setItemsBlankIfNull();
		this.defaultList = returnBlankIfNull(data.defaultShoppingListId);
		
	}else{
		if(!isFirstPageData){
			this.itemsList = [];
		}
		if(refreshList){
			this.itemsList = [];
		}
	}
	
	
	
	this.updateFilterStatus(itemList,isFirstPageData);
};
MyShoppingList.prototype.setItemsList = function(data,isFilterOn,isPagedData)
{	
	
	if(isNull(data) || data.messageCode==111)
	{
		this.defaultList = "";
		this.itemsList =[]; 
		if(isFilterOn)
		{
			if(isPagedData){
				this.filterMsg = null;
			}
			else{
				this.filterMsg =  this.translateFunc('MSG_NO_OBJECTS_FOR_SELECTION',null);
				showMsgNoObjFound();
			}
		}
		else{
			this.filterMsg =  null;
		}
		this.pageDataFound = false;
	}
	else
	{
		this.pageDataFound = true;
		this.filterMsg =  null;			
		this.itemsList = data.listSearchBean;
		this.defaultList = data.defaultShoppingListId;
		this.setItemsBlankIfNull();
	}

};

MyShoppingList.prototype.getShoppingListLockStatus = function(){
	var listHasLock = false;
	for(var i=0; i<this.itemsList.length ;i++){
		var item=this.itemsList[i];
		
		var ListLock = item.listLock;
		if(ListLock == 2)
			{
				listHasLock = true;
				break;
			}
	}
	this.listHasLock = listHasLock;
	
	return listHasLock;
	
};

MyShoppingList.prototype.setItemsBlankIfNull = function(){
	for(var i=0; i<this.itemsList.length ;i++){
		var item=this.itemsList[i];
		item.listDesc=returnBlankIfNull(item.listDesc);
	}
};

MyShoppingList.prototype.getItemsList = function()
{
	return this.itemsList ;
};

MyShoppingList.prototype.updateMsg = function(responseData,item)
{
	var errorMsg = null;
	var errorParam = [];
	
	this.ErrOnControl.ListID = false;		
	this.ErrOnControl.ListDesc = false;	
	
	if(!isNull(responseData.messageCode))
	{
		switch (responseData.messageCode){
		case 2260 :
			errorMsg = null;
			break;
		case 2259 :
			errorMsg = 'ERROR_IN_DELETING_LIST';
			errorParam = [responseData.listId];
			break;
		case 2266 :
		//	errorMsg = 'MSG_NEW_LIST_CREATED_SUCCESSFULLY';
			break;
		case 2250 :
			errorMsg = 'MANDATORY_LISTID_MISSING';
			this.ErrOnControl.ListID = true;
			break;
		case 2251 :
			errorMsg = 'MSP_LIST_ID_ALREADY_EXIST';
			errorParam = [item.listId];
			this.ErrOnControl.ListID = true;
			break;
		case 2252 :
			errorMsg = 'MSG_LIST_ID_CONTAINS_INVALID_CHARACTERS';
			this.ErrOnControl.ListID = true;
			break;
		case 2253 :
			errorMsg = 'MSG_SHARED_SHOPPING_LIST_NO_CUSTOMER_CONNECTED_TO_THE_LIST';
			break;
		case 2254 :
			errorMsg = 'ERROR_IN_SAVING_NEW_LIST';
			break;
		case 2255 :
			errorMsg = 'MAX_NUM_OF_LIST_HAS_EXCEEDED';
			break;
		case 2257 :
			errorMsg = 'NO_LIST_FOR_THE_USER_CREATE_NEW_LIST';
			break;
		case 2263 :
		//	errorMsg = 'SUCCCESSFULLY_SET_DEFAULT_LIST';
			break;
		case 2264 :
			errorMsg = 'ERROR_IN_SETTING_DEFAULT_LIST';
			break;
		case 2272 :
			errorMsg = 'LONG_DESCRIPTION_NOT_ALLOWED';
			this.ErrOnControl.ListDesc = true;
			break;	
			
		default :
			errorMsg = null;
			errorParam = [];
		}
	}	
	if(isNull(errorMsg))
		{
			this.setNewList();
		}
	
	this.UIMessageKey = translate.getTranslation(errorMsg,errorParam);
};

MyShoppingList.prototype.isValidValues = function(newList) {
	var isValid = true;
	this.ErrOnControl.ListID = false;		
	this.ErrOnControl.ListDesc = false;
	if(isEmptyObject(newList)){
		isValid = false; 
			
		this.UIMessageKey  = null;
	}
	else
		{
		
			if(isEmptyString(newList.listId.trim()))  {		
				this.UIMessageKey =translate.getTranslation('MANDATORY_LISTID_MISSING');
				this.ErrOnControl.ListID = true;
				return isValid = false;
			}
			if(!this.isValidListId(newList)) {
				this.UIMessageKey =translate.getTranslation('MSG_LIST_ID_CONTAINS_INVALID_CHARACTERS');
				this.ErrOnControl.ListID = true;
				return isValid = false;
			}
		}
	if(isValid){
		this.UIMessageKey  = null;
	}
	return isValid;
};

MyShoppingList.prototype.isValidListId = function(newList){
//	var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var re = /^[a-z0-9]+$/i;
	if (re.test(newList.listId)){  
	    return true;
	}else{
		return false;
	}
};