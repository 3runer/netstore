function GroupImageSlider (imageList,groupSize) {
	    this.imageList = imageList;
	    this.currentIndex=0;
	    this.masterIndex = 0;
	    // currentGroup must start from 1 
	    this.currentGroup=1;
	    
	    if(!isNull(groupSize) && groupSize>=1){
	    	this.groupSize=Math.ceil(groupSize);
	    }else{
	    	this.groupSize=1;
	    }
	}
	
	GroupImageSlider.prototype.setMasterIndex = function(masterIndex){
		if(masterIndex>=0 && masterIndex<this.imageList.length){
			//this.masterIndex = masterIndex;
			var groupIndex = Math.floor((masterIndex / this.groupSize));
			if(groupIndex<=0){
				this.currentGroup =1;
			}else{
				this.currentGroup = groupIndex+1;
			}
			//var imageIndex = masterIndex % this.groupSize;
			this.currentIndex = masterIndex;
		}
	};
	
	GroupImageSlider.prototype.setMatrixImageList = function(imageList) {
		if (!isNull(imageList) && Array.isArray(imageList)){
			for(var i = 0; i< imageList.length;i++)
			{
				if(!this.isImageExist(imageList[i]))
				{
					this.imageList.push(imageList[i]);
				}
			}
		}
	};
	
	GroupImageSlider.prototype.getImageIndex = function(image)
	{
		var imageIndex = 0;
		var imageList = this.imageList;
		 for(var i = 0; i< imageList.length;i++)
			 {
			 	if(image == imageList[i])
			 		{
			 			imageIndex = i;
			 			break;
			 		}
			 }
		return imageIndex;
	};
	
	GroupImageSlider.prototype.isImageExist = function(image)
	{
		var imageAvailable = false;
		var imageList = this.imageList;
		 for(var i = 0; i< imageList.length;i++)
			 {
			 	if(image == imageList[i])
			 		{
			 			imageAvailable = true;
			 			break;
			 		}
			 }
		 return imageAvailable;
	};
	
	
	GroupImageSlider.prototype.setImageList = function(imageList) {
		if (!isNull(imageList) && Array.isArray(imageList)){
			 this.imageList = imageList;
		}
	};
	
	GroupImageSlider.prototype.getDefaultImage = function() {
		return './images/ImageIcon.png';
	};
	
	GroupImageSlider.prototype.hasNoImage = function() {
		if (isNull(this.imageList) || this.imageList.length == 0 ){
			 return true;
		}else{
			return false;
		}
	};
	
	GroupImageSlider.prototype.setCurrentIndex = function(index) {
		//set index according to currentGroup number
		if(this.currentGroup>1){
			var endIndex = (this.currentGroup * this.groupSize)-1;
			var startIndex = endIndex - this.groupSize + 1;
			this.currentIndex=startIndex+index;
		}else{
			this.currentIndex=index;
		}
		if(this.currentIndex >= this.imageList.length || this.currentIndex < 0){
			//console.log("GroupImageSlider.prototype.setCurrentIndex() : Index out of range= " + this.currentIndex );
		}
		//this.updateMasterIndex();
		return this.currentIndex;
	};
	
	GroupImageSlider.prototype.getCurrentIndex = function() {
		return  this.currentIndex;
	};
	
	GroupImageSlider.prototype.hasMultipleImages = function() {
		if (!isNull(this.imageList) && this.imageList.length > 1){
			 return true;
		}else{
			return false;
		}
	};
	
	GroupImageSlider.prototype.getNumberOfGroups = function(){
		var maxNoOfGrps = Math.ceil((this.imageList.length/ this.groupSize));
		return maxNoOfGrps;
	};
	
	GroupImageSlider.prototype.incrementGroupNumber = function(){
		if(this.imageList.length>=1 && this.imageList.length> this.groupSize){
			var maxNoOfGrps = this.getNumberOfGroups();
			if(this.currentGroup<maxNoOfGrps){
				this.currentGroup++;
			}else if (this.currentGroup==maxNoOfGrps){
				this.currentGroup =1; //make cycle
			}
		}else{
			this.currentGroup =0;
		}
	};
	
	GroupImageSlider.prototype.decrementGroupNumber= function(){
		if(this.imageList.length>=1 ){
			if(this.imageList.length> this.groupSize){
				if(this.currentGroup>1 ){
					this.currentGroup--;
				}else {
					var maxNoOfGrps = this.getNumberOfGroups();
					this.currentGroup=maxNoOfGrps; //make cycle
				}
			}else{
				this.currentGroup=1;
			}
		}else{
			this.currentGroup =0;
		}
	};
	
	GroupImageSlider.prototype.nextImageGroup = function() {
		this.incrementGroupNumber();
		return this.currentImageGroup();
	};
	
	GroupImageSlider.prototype.currentImageGroup = function() {
		var imagesGrp = [];
		if(this.imageList.length>this.groupSize){
			var endIndex = (this.currentGroup * this.groupSize)-1;
			var startIndex = endIndex - this.groupSize + 1;
			for(var i= startIndex; i<this.imageList.length && i<=endIndex;i++ ){
				imagesGrp.push(this.imageList[i]);
			};
		}else{
			imagesGrp = this.imageList;
		}
		return imagesGrp;
	};
	
	GroupImageSlider.prototype.previousImageGroup = function() {
		this.decrementGroupNumber();
		return this.currentImageGroup();
	};
	
	GroupImageSlider.prototype.getAllImages = function() {
		return this.imageList;
	};
	
	GroupImageSlider.prototype.showNavigationLinks = function() {
		if(this.getNumberOfGroups()>1){
			return true;
		}else{
			return false;
		}
	};