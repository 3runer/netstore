function FilterList() {
	    this.filterList ;
	    this.previousFilterList;
	}
	
	FilterList.prototype.getSelectedFilters = function() {
		var selectedFilterList = [];
		var filterObj = null ;
		for(var i=0; i<this.filterList.length;i++){
			filterObj = this.filterList[i];
			if(!isNull(filterObj.selectedItems)){
				selectedFilterList.push(filterObj);
			}
		}
		//console.log(" selected filters : " + JSON.stringify(selectedFilterList));
		return selectedFilterList;
	};
	
	FilterList.prototype.getSelectedFiltersProductSearch = function() {
		
		var paramList = [];
		var filterObj = null ;
		for(var i=0; i<this.filterList.length;i++){
			filterObj = this.filterList[i];
			if(filterObj.dataList.length>0){
				for(var k=0;k<this.filterList[i].dataList.length;k++){
					if(!isNull(this.filterList[i].dataList[k].checked) && this.filterList[i].dataList[k].checked !="false" && this.filterList[i].dataList[k].checked !=null ){
						paramList.push(this.filterList[i].dataList[k]);
						
					}
				}	
			}
		}
		return paramList;
	};
	
	
	FilterList.prototype.setSelectedOption = function(filterObj,optionItem) {
		var flag = false;
		if(filterObj.isInputList){
			switch(filterObj.filterName){
			case 'CON_CUSTOMER' : 
				flag  = this.setCustomerListOption(filterObj, optionItem);
				break;
			case 'CON_RESULTS_PER_PAGE' :
				break;
			default :
				if (!isNull(filterObj.selectedItems)){
					if(filterObj.selectedItems == optionItem.code){
						flag = true;
					}
				}else {
					//make first option as selected
					if(Array.isArray(filterObj.dataList) && filterObj.dataList.length > 0 && filterObj.dataList[0].code == optionItem.code){
						//filterObj.selectedItems = optionItem.code;
						flag = true;
					}
				}
			}
		}
		return flag;
	};
	
	FilterList.prototype.setCustomerListOption = function(filterObj,optionItem) {
		var flag = false;
		if(!isNull(filterObj.selectedItems)){
			if(filterObj.selectedItems == optionItem.code){
				flag = true;
			}
		}else if(!isNull(optionItem.defaultCode) && optionItem.defaultCode == optionItem.code){
			flag = true;
		}
		return flag;
	};
	
	FilterList.prototype.setResultPerPageListOption = function(filterObj,resultperPage) {
		var flag = false;
		if(!isNull(filterObj.selectedItems)){
			if(filterObj.selectedItems == resultperPage){
				flag = true;
			}
		}else if(resultperPage == filterObj.resultCount){
			flag = true;
		}
		return flag;
	};
	
	
	FilterList.prototype.removeAllSelectedFilters = function() {
		for(var i=0; i<this.filterList.length;i++){
			this.filterList[i].selectedItems = null;
		}
	};
	
	FilterList.prototype.removeAllSelectedFiltersAdvanceSearch = function() {
		for(var i=0; i<this.filterList.length;i++){
			for(var k =0;k<this.filterList[i].dataList.length;k++){
			this.filterList[i].dataList[k].checked = null;
			}
		}
	};
	
	FilterList.prototype.removeSelection = function(filterObj) {
		if(!isNull(filterObj)){
			filterObj.selectedItems = null;
		}
		this.getParamsList();
	};
	
	FilterList.prototype.removeSelectionProductSearch = function(filterObj) {
		if(!isNull(filterObj)){
			filterObj.checked = null;
		}
		this.getParamsList();
	};
	
	
	FilterList.prototype.getFilters = function() {
		return this.filterList ;
	};
	
	FilterList.prototype.setDateRangeFilterValidityMessage = function() {
		for(var i=0; i<this.filterList.length;i++){
			var filterObj = this.filterList[i];			
			if(filterObj.dateType && filterObj.rangeType && !filterObj.RangeSelector){
					clearFilterMessage(filterObj);
	                  break;
			}
            
		}

	};
	
	FilterList.prototype.hasEnabledFilters = function() {
        var hasEnabledFilters = false;
        for(var i=0; i<this.filterList.length;i++){
               if(this.filterList[i].isEnabled){
                     hasEnabledFilters = true;
                     break;
               }
        }
        return hasEnabledFilters;
	};

	FilterList.prototype.cacheFilters = function() {
		this.previousFilterList =  deepCopyWithFunctions(this.filterList);
	};
	
	FilterList.prototype.applyPreviousFilters = function() {
		if(!isNull(this.previousFilterList)){
			this.filterList  = deepCopyWithFunctions(this.previousFilterList); 
		}
	};
	
	FilterList.prototype.setTranslatedFilterName = function(filterObj){
		if(!isNull(filterObj) && isNull(filterObj.filterValueName)){
			filterObj.filterValueName = translate.getTranslation(filterObj.filterName);
		}
	};
	
	FilterList.prototype.createFilterList = function(staticFilterConfig, filterDataList, dynamicFilterConfig, propNameOfDataList) {
		var finalFilterList = [];
		if(!isNull(propNameOfDataList)){
			
			for(var i=0;i<dynamicFilterConfig.length;i++){
				var filterObject = new Object();
				var filterName = dynamicFilterConfig[i].filterName;
				copyObject(dynamicFilterConfig[i],filterObject);
				this.setTranslatedFilterName(filterObject);
				var staticFilterConfigObject = getObjectFromList('filterName', filterName ,staticFilterConfig);
				copyObject(staticFilterConfigObject,filterObject);
				var filterDataObject = getObjectFromList('filterName', filterName ,filterDataList);
				if(!isNull(filterDataObject)){
					if(!isNull(filterDataObject[propNameOfDataList])){
						filterObject.dataList = filterDataObject[propNameOfDataList];
						if(!isNull(filterDataObject.maxVisualListSize)){
							filterObject.maxVisualListSize = filterDataObject.maxVisualListSize;
						}
					}
					finalFilterList.push(filterObject);
				}
			}
		}else{
			finalFilterList = this.createFilterList2(staticFilterConfig, filterDataList, dynamicFilterConfig);
		}
		for(var i=0;i<finalFilterList.length;i++){
			var filterItem=finalFilterList[i];
			if(filterItem.isInputList){
				if(filterItem.filterName=='CON_CUSTOMER'){					
					this.setDefaultCustomer(filterItem, filterItem.dataList);
				}else if(filterItem.filterName == 'CON_RESULTS_PER_PAGE'){
					filterItem.selectedItems=filterItem.resultCount;
				}else if(!isNull(filterItem.dataList) && filterItem.dataList.length > 0){
					filterItem.selectedItems=filterItem.dataList[0].code;
				}
			}
		}
		this.filterList = finalFilterList;
		return finalFilterList;
	};
	
	FilterList.prototype.setDefaultCustomer = function(filterObj, datalist) {
		if(isNull(datalist) || isNull(datalist.length)) return;
		for(var i=0;i<datalist.length;i++){
			var optionItem= datalist[i];
			if(!isNull(optionItem.defaultCode) && optionItem.defaultCode == optionItem.code){
				filterObj.selectedItems=optionItem.defaultCode;
			}
		}
	};
	
	//createFilterList2 - create filter list using global filterDataList 
	FilterList.prototype.createFilterList2 = function(staticFilterConfig, filterDataList, dynamicFilterConfig) {
		var finalFilterList = [];
		for(var i=0;i<dynamicFilterConfig.length;i++){
			var filterObject = new Object();
			var filterName = dynamicFilterConfig[i].filterName;
			copyObject(dynamicFilterConfig[i],filterObject);
			this.setTranslatedFilterName(filterObject);
			var staticFilterConfigObject = getObjectFromList('filterName', filterName ,staticFilterConfig);
			var propNameOfDataList = staticFilterConfigObject.dataListName;
			copyObject(staticFilterConfigObject,filterObject);
			//var filterDataObject = getObjectFromList('filterName', filterName ,filterDataList);
			var filterDataObject = filterDataList[propNameOfDataList];
			if(!isNull(filterDataObject)){
				//if(!isNull(filterDataObject[propNameOfDataList])){
					filterObject.dataList = filterDataObject;
				//}
				finalFilterList.push(filterObject);
			}
			else
				{
				finalFilterList.push(filterObject);
				}
		}
		this.filterList = finalFilterList;
		return finalFilterList;
	};
	
	/**
	 * 
	 * @returns parameter and its value in an array e.g. ["param1","paramValue1","param2","paramValue2"]
	 */
	FilterList.prototype.getParamsList = function() {
		var paramList = [];
		var filterObj = null ;
		for(var i=0; i<this.filterList.length;i++){
			filterObj = this.filterList[i];
						
			if(filterObj.rangeType)
			{
				if(!isNull(filterObj.selectedItems1 ) && !isEmptyString(filterObj.selectedItems1)){	
					paramList.push(filterObj.filterParamValue1);
					paramList.push(encodeURIComponent(filterObj.selectedItems1));
				}
				if(!isNull(filterObj.selectedItems2)&& !isEmptyString(filterObj.selectedItems2)){
					paramList.push(filterObj.filterParamValue2);
					paramList.push(encodeURIComponent(filterObj.selectedItems2));
				}
			}
			
			if(!isNull(filterObj.selectedItems)){	
				if(!isEmptyString(filterObj.selectedItems.code)){
					paramList.push(filterObj.filterParam);
					paramList.push(encodeURIComponent(filterObj.selectedItems.code));
				}
				else if(!isEmptyString(filterObj.selectedItems) || angular.isNumber(filterObj.selectedItems))
				{
					paramList.push(filterObj.filterParam);
					paramList.push(encodeURIComponent(filterObj.selectedItems));
				}
			}
		}
		//console.log(" Filter getParamsList : " + JSON.stringify(paramList));
		return paramList;
	};
	
	
	FilterList.prototype.getParamsListProductSearch = function(itemId,itemDesc,pageNumber,oprAndOr,langCode,
			/* ADM-011 */minsanCode, eanCode, manufacturerName, substanceName, text) {
		var paramList = [];
		var paramList1 =[];
		var filterParamList = [];
		var filterObj = null ;
		var obj = new Object();
		if(!isNull(itemId)){
			var itemIDReplaced = itemId.replace(/"/g, '\\"');
			obj.ITEM_ID = itemIDReplaced; 
		}
		if(!isNull(itemDesc)){
			var itemDescReplaced = itemDesc.replace(/"/g, '\\"');
			obj.ITEM_DESC = itemDescReplaced; 
		}
		//ADM-011
		if(!isNull(minsanCode)){
			var minsanCodeReplaced = minsanCode.replace(/"/g, '\\"');
			obj.MINSAN_CODE = minsanCodeReplaced; 
		}
		if(!isNull(eanCode)){
			var eanCodeReplaced = eanCode.replace(/"/g, '\\"');
			obj.EAN_CODE = eanCodeReplaced; 
		}
		if(!isNull(substanceName)){
			var substanceNameReplaced = substanceName.replace(/"/g, '\\"');
			obj.SUBSTANCE_NAME = substanceNameReplaced; 
		}
		if(!isNull(manufacturerName)){
			var manufacturerNameReplaced = manufacturerName.replace(/"/g, '\\"');
			obj.MANUFACTURER_NAME = manufacturerNameReplaced; 
		}
		if(!isNull(text)){
			var textReplaced = text.replace(/"/g, '\\"');
			obj.FREE_TEXT = textReplaced; 
		}
		//ADM-011 end
		if(!isNull(oprAndOr)){
			obj.OPR_AND_OR = oprAndOr;
		}
		if(!isNull(langCode)){
			obj.languageCode = langCode;
		}
		obj.SEARCH_TYPE ="ADV";
		obj.PAGE_NUMBER = pageNumber;
		for(var j=0;j<this.filterList.length;j++){
			if(this.filterList[j].filterParam != null){
				filterParamList.push(this.filterList[j].filterParam);				
			}			
		}
		
		for(var i=0; i<this.filterList.length;i++){
			filterObj = this.filterList[i];
			var abc = filterParamList[i];
						
			if(filterObj.rangeType)
			{
				if(!isNull(filterObj.selectedItems1 ) && !isEmptyString(filterObj.selectedItems1)){	
					paramList.push(filterObj.filterParamValue1);
					paramList.push(filterObj.selectedItems1);
				}
				if(!isNull(filterObj.selectedItems2)&& !isEmptyString(filterObj.selectedItems2)){
					paramList.push(filterObj.filterParamValue2);
					paramList.push(filterObj.selectedItems2);
				}
			}
			if(filterObj.dataList.length>0){
				
				var obj1 = new Object();
				var obj2 = new Object();
				//obj.searchType = "ADV";
				
			//	paramList.push(filterObj.filterParam);
				for(var k=0;k<this.filterList[i].dataList.length;k++){
					
					if(!isNull(this.filterList[i].dataList[k].checked) && this.filterList[i].dataList[k].checked !="false" && this.filterList[i].dataList[k].checked !=null ){
						var myStr = this.filterList[i].dataList[k].checked; 
						var myStrDesc = this.filterList[i].dataList[k].desc;
						myStr = myStr.replace(/"/g, '\\"');
						myStrDesc = myStrDesc.replace(/"/g, '\\"');
						obj1.filterCode = encodeURIComponent(myStr);
						obj1.filterDesc = encodeURIComponent(myStrDesc);
						paramList1.push(obj1);
						obj1 ={};
						
						//paramList.push(filterObj.dataList[k].checked);
					}
				}	
			}
			//for(m=0;m<this.filterList.length;m++){
				//if(!isNull(this.filterList[i].dataList[m].checked)){
					
			//	}
		//	}
			 
			if(!isNull(filterObj.selectedItems)){					
				if(!isEmptyString(filterObj.selectedItems.code)){
					paramList.push(filterObj.filterParam);
					paramList.push(encodeURIComponent(filterObj.selectedItems.code));
				}
				else if(!isEmptyString(filterObj.selectedItems) || angular.isNumber(filterObj.selectedItems))
				{
					paramList.push(filterObj.filterParam);
					paramList.push(encodeURIComponent(filterObj.selectedItems));
				}
			}
			
			if(paramList1.length>0){
			obj[abc] = paramList1;
			//paramList.push(obj);
			}
			//obj={}; 
			paramList1 = [];
		}
		paramList.push(obj);
		//console.log(" Filter getParamsList : " + JSON.stringify(paramList));
		return paramList;
	};
	
	FilterList.prototype.getParamsListCatalogSearch = function(itemId,pageNumber,elementID) {
		var paramList = [];
		var paramList1 =[];
		var filterParamList = [];
		var filterObj = null ;
		var obj = new Object();
		obj.type ="ID";
		obj.CAT_ID=itemId;
		obj.PAGE_NUMBER = pageNumber;
		obj.ELEMENT_ID = elementID;
		for(var j=0;j<this.filterList.length;j++){
			if(this.filterList[j].filterParam != null){
				filterParamList.push(this.filterList[j].filterParam);				
			}			
		}
		
		for(var i=0; i<this.filterList.length;i++){
			filterObj = this.filterList[i];
			var abc = filterParamList[i];
						
			if(filterObj.rangeType)
			{
				if(!isNull(filterObj.selectedItems1 ) && !isEmptyString(filterObj.selectedItems1)){	
					paramList.push(filterObj.filterParamValue1);
					paramList.push(filterObj.selectedItems1);
				}
				if(!isNull(filterObj.selectedItems2)&& !isEmptyString(filterObj.selectedItems2)){
					paramList.push(filterObj.filterParamValue2);
					paramList.push(filterObj.selectedItems2);
				}
			}
			if(filterObj.dataList.length>0){
				
				var obj1 = new Object();
				var obj2 = new Object();
				//obj.searchType = "ADV";
				
			//	paramList.push(filterObj.filterParam);
				for(var k=0;k<this.filterList[i].dataList.length;k++){
					
					if(!isNull(this.filterList[i].dataList[k].checked) && this.filterList[i].dataList[k].checked !="false" && this.filterList[i].dataList[k].checked !=null ){
						obj1.filterCode = this.filterList[i].dataList[k].checked;
						paramList1.push(obj1);
						obj1 ={};
						
						//paramList.push(filterObj.dataList[k].checked);
					}
				}	
			}
			//for(m=0;m<this.filterList.length;m++){
				//if(!isNull(this.filterList[i].dataList[m].checked)){
					
			//	}
		//	}
			 
			if(!isNull(filterObj.selectedItems)){					
				if(!isEmptyString(filterObj.selectedItems.code)){
					paramList.push(filterObj.filterParam);
					paramList.push(filterObj.selectedItems.code);
				}
				else if(!isEmptyString(filterObj.selectedItems) || angular.isNumber(filterObj.selectedItems))
				{
					paramList.push(filterObj.filterParam);
					paramList.push(filterObj.selectedItems);
				}
			}
			
			if(paramList1.length>0){
			obj[abc] = paramList1;
			//paramList.push(obj);
			}
			//obj={}; 
			paramList1 = [];
		}
		paramList.push(obj);
		//console.log(" Filter getParamsList : " + JSON.stringify(paramList));
		return paramList;
	};
	
	FilterList.prototype.hasFilterTextValues = function() {
		var filterObj = null ;
		var flag = false ; 
		for(var i=0; i<this.filterList.length;i++){
			filterObj = this.filterList[i];
            // it considers only text boxes
			if(filterObj.rangeType)
			{
				if(!isNull(filterObj.selectedItems1 ) && !isEmptyString(filterObj.selectedItems1)){	
					flag = true ;
					break;
				}
				if(!isNull(filterObj.selectedItems2)&& !isEmptyString(filterObj.selectedItems2)){
					flag = true ;
					break;
				}
			}else if(!filterObj.isInputList){	
				if(!isEmptyString(filterObj.selectedItems)){
					flag = true ;
					break;
				}
			}
		}
		return flag;
	};
	
	FilterList.prototype.hasError = function() {

		var hasErr = false; 
		var filterObj = null ;
		if(Array.isArray(this.filterList)){
			for(var i=0; i<this.filterList.length;i++){
				filterObj = this.filterList[i];
				if(!isNull(filterObj.isValidInput) && (filterObj.isValidInput == false))
				{
					hasErr = true;

				}
				if(!isNull(filterObj.isValidInput2) && (filterObj.isValidInput2 == false))
				{
					hasErr = true;

				}
			}
		}
		return hasErr;
	};