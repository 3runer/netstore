function RequestResponseInfo(rootScope,translateFunc){		
		this.requestNumber = null;
		this.lineStatusCodes = [];		
		this.UIMessageKey=null;
		this.requestLineUIMessageKey=null;
		this.messageCode = "";		
		//this.rootScope = rootScope;		
		//this.translateFunc = translateFunc;
		this.isRequestCreated = false;
	};
	RequestResponseInfo.prototype.setRequestStatus = function(requestNumber,messageCode,lineStatusCodes,Msg,isRequestCreated,reqLineMsg)
	{
		this.requestNumber = returnBlankIfNull(requestNumber);
		this.lineStatusCodes = lineStatusCodes;		
		this.UIMessageKey=returnBlankIfNull(Msg);
		this.requestLineUIMessageKey=returnBlankIfNull(reqLineMsg);
		this.messageCode = returnBlankIfNull(messageCode);			
		this.isRequestCreated = isRequestCreated;
	};