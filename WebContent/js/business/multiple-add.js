/*
 * not sure as the function in below block are unused.
 */
// get request param for Item code validation
var getItemCodeRequestParam = function(itemCode, page) {
	var requestParam = [ "ITEM_CODE", itemCode ];
	if (page != "") {
		requestParam.push("PAGE", page);
	}
	return requestParam;
};

var getProductListRequestParam = function(paramList) {
	var param = "";
	if (!isNull(paramList)) {
		param = "ValidateItemsDetails=" + JSON.stringify(paramList);
	}
	return param;
};

// to Validate the all the items details in Add Product table row
var validateAllProductsDetails = function(itemInfo, productListObject,
		translateFunc) {
	var productList = productListObject.getLineList();
	var count = 0;
	if (!isNull(productList) && (!isNull(itemInfo))) {

		for ( var i = 0; i < itemInfo.length; i++) {
			var isValidItem = true;
			productList[i].errorMsg = "";
			if (itemInfo[i].isItemCodeError) {
				if (!isEmptyString(itemInfo[i].itemCode)) {
					setUILineError(productList[i], translateFunc,
							'MSG_INVALID_ITEM');
				}
				isValidItem = false;
			} else if (!itemInfo[i].isBuyAllowed) {
				if (!isEmptyString(itemInfo[i].itemCode)) {
					setUILineError(productList[i], translateFunc,
							'MSG_BUY_NOT_ALLOWED');
				}
				isValidItem = false;
			}

			if (itemInfo[i].isUnitCodeError) {
				if (!isEmptyString(itemInfo[i].itemCode)) {
					setUILineError(productList[i], translateFunc,
							'MSG_ITEM_INVALID_UNIT');
				}
				isValidItem = false;
			}

			if (isEmptyString(itemInfo[i].itemCode)) {
				isValidItem = false;
				count++;
			}
			// check product line
			if (isValidItem) {

				productList[i].isValidProduct = true;
				productListObject.setLineObjectStatus(productList[i],
						productListObject, true);

			} else {
				productList.isValidProductList = false;

				productList[i].isValidProduct = false;

				productListObject.setLineObjectStatus(productList[i],
						productListObject, false);

			}

		}

		if (itemInfo.length == count) {
			productListObject.UIErrorKey = 'MSG_ITEM_NOT_SPECIFIED';
		}
	}
	if (!isNull(itemInfo.messageCode) && itemInfo.messageCode == 2110) {
		productListObject.UIErrorKey = 'MSG_ITEM_NOT_SPECIFIED';
	}
	// productListObject.AddProductListToCart();
};

// to Validate the item code in Add Product table row
// ADM-010
// var validateLineItemCode = function(itemInfo,line,lineObject,translateFunc)
var validateLineItemCode = function(itemInfo, line, lineObject, translateFunc,
		packPiece) {
	var isValidItem = true;
	var unitList = [];
	var errorMsg = "";
	if (!isNull(itemInfo)) {
		line.errorMsg = "";
		// if message code returns
		if (!isNull(itemInfo.messageCode)) {
			switch (itemInfo.messageCode) {
			case '114':
				// setUIError(lineObject,'MSG_INVALID_ITEM');
				addUIError(line, translateFunc, 'MSG_INVALID_ITEM');
				break;
			case '3101':
				addUIError(line, translateFunc, 'MSG_ITEM_IS_NOT_AVAILABLE');
				break;

			case '3102':
				addUIError(line, translateFunc, 'MSG_ITEM_QUANTITY_ZERO');
				break;

			case '3103':
				addUIError(line, translateFunc, 'MSG_NEGATIVE_QUANTITY');
				break;

			case '3104':
				addUIError(line, translateFunc, 'MSG_INVALID_QUANTITY');
				break;

			case '3105':
				addUIError(line, translateFunc, 'MSG_INVALID_DATE');
				break;

			case '3106':
				addUIError(line, translateFunc, 'MSG_DATE_NOT_ALLOWED');
				break;

			case '3107':
				addUIError(line, translateFunc, 'MSG_NOT_WORK_DAY');
				break;

			case '3108':
				addUIError(line, translateFunc, 'MSG_ITEM_NOT_SPECIFIED');
				break;

			case '3109':
				addUIError(line, translateFunc, 'MSG_INVALID_ITEM');
				break;

			case '3110':
				addUIError(line, translateFunc, 'MSG_ITEM_INVALID_UNIT');
				break;

			case '3111':
				addUIError(line, translateFunc, 'MSG_ITEM_NOT_ALLOWED_UNIT');
				break;

			case '3112':
				addUIError(line, translateFunc, 'MSG_ITEM_NO_PRICE');
				break;

			case '3113':
				addUIError(line, translateFunc, 'MSG_DELIVERY_NOT_POSSIBLE');
				break;

			case '3114':
				// setUIError(lineObject,'MSG_BUY_NOT_ALLOWED');
				addUIError(line, translateFunc, 'MSG_BUY_NOT_ALLOWED');
				break;

			default: // setUIError(lineObject,'MSG_INVALID_ITEM');
				addUIError(line, translateFunc, 'MSG_INVALID_ITEM');
			}
			isValidItem = false;
			line.showUnitList = false;
			unitList = [];
			line.isValidProduct = false;
		} else {
			// if item data comes
			if (itemInfo.isItemCodeError) {
				addUIError(line, translateFunc, 'MSG_INVALID_ITEM');
				// setUIError(lineObject,'MSG_INVALID_ITEM');
				line.showUnitList = false;
				isValidItem = false;
			}

			/*
			 * if(itemInfo.isUnitCodeError) {
			 * 
			 * addUIError(line,translateFunc,'MSG_ITEM_INVALID_UNIT');
			 * 
			 * //setUIError(lineObject,'MSG_ITEM_INVALID_UNIT'); isValidItem =
			 * false; } if(!itemInfo.isBuyAllowed) {
			 * addUIError(line,translateFunc,'MSG_BUY_NOT_ALLOWED');
			 * //setUIError(lineObject,'MSG_BUY_NOT_ALLOWED'); isValidItem =
			 * false;
			 *  }
			 */
			// /line.errorMsg = errorMsg;
			setUIError(lineObject, errorMsg);
			// ADM-010 Begin
			// unitList = itemInfo.activeUnitsDesc;
			// unitListArr = itemInfo.activeUnits;
			if (!isNull(packPiece)) {
				var unit = packPiece;
				var unitDesc = packPiece;
				if (!isNull(itemInfo.salesPackageQty)) {
					unitDesc = unitDesc + itemInfo.salesPackageQty;
				}
				itemInfo.activeUnitsDesc = [ {
					salesUnit : unit,
					salesUnitDesc : unitDesc
				} ];
				itemInfo.activeUnits = [ unit ];
				itemInfo.unitCode = unit;
				itemInfo.unitCodeDesc = unitDesc;
			}
			// ADM-010 End
			unitList = itemInfo.activeUnitsDesc;
			unitListArr = itemInfo.activeUnits;
			decimalNumberAllow = itemInfo.itemNumberOfDecimalsAllowed;
		}
	}
	
	if (isValidItem) {
		line.showUnitList = true;
		line.activeUnitList = unitList;
		line.unitList = unitListArr;
		line.itemNumberOfDecimalsAllowed = decimalNumberAllow;
		line.isValidProduct = true;
		line.unit = itemInfo.unitCode;
		/* ADM-010 */line.salesPackageQty = itemInfo.salesPackageQty;
		lineObject.setLineObjectStatus(line, lineObject, true);
	} else {
		lineObject.isValidProductList = false;
		line.unit = null;
		line.isValidProduct = false;
		// setfocus(line.rowNum); // to set
		lineObject.setLineObjectStatus(line, lineObject, false);
	}

	validateAllLines(lineObject);

};

var setUILineError = function(line, translateFunc, errorKey) {
	line.errorMsg = translate.getTranslation(errorKey);
	// line.errorMsg = translateFunc(errorKey);

};

var addUIError = function(line, translateFunc, errorKey) {
	if (isEmptyString(line.errorMsg) || isNull(line.errorMsg)) {
		// line.errorMsg = translateFunc(errorKey);
		line.errorMsg = translate.getTranslation(errorKey);
	} else {
		line.errorMsg = line.errorMsg + "  "
				+ translate.getTranslation(errorKey);// translateFunc(errorKey);
	}

};

var setUIError = function(UIObject, message) {
	UIObject.UIErrorKey = message;
	UIObject.UIErrorParams = [];

};

/*
 * unused methods above
 */

function MultipleAdd(prdlist, rootScope, translateFunc, orderDelimeter) {
	this.prdlist = prdlist;
	// ADM-022 start
	// this.blankLinesToAdd = 5;
	this.blankLinesToAdd = 1;
	// ADM-022 end
	this.newPrdList = [];
	this.UIErrorKey = null;
	this.delimeter = orderDelimeter;
	this.orderText = '';
	this.buyList = [];
	this.rootScope = rootScope;
	this.showUnitList = false;
	this.isValidProductList = true;
	this.translateFunc = null;
	this.itemNumberOfDecimalsAllowed = [];

}

MultipleAdd.prototype.clearAll = function() {
	// ADM-022 start
	// this.blankLinesToAdd = 5;
	this.blankLinesToAdd = 1;
	// ADM-022 end
	this.newPrdList = [];
	this.UIErrorKey = null;
	this.addMoreLines();
};

MultipleAdd.prototype.createBlankObject = function() {
	var obj = new Object();
	obj.itemCode = null;
	obj.ordered = null;
	obj.unit = null;
	obj.delDate = null;
	obj.shipmentMarking = null;
	obj.showUnitList = false;
	obj.activeUnitList = [];
	obj.unitList = [];
	obj.itemNumberOfDecimalsAllowed = [];
	obj.isValidProduct = true;
	obj.validQuantity = true;
	obj.UIErrorKey = null;
	obj.errorMsg = "";
	return obj;
};

MultipleAdd.prototype.createObject = function(itemCode, ordered, unit, delDate,
		shipmentMarking) {
	var obj = new Object();
	obj.itemCode = itemCode;
	obj.ordered = ordered;
	obj.unit = unit;
	obj.delDate = delDate;
	obj.shipmentMarking = shipmentMarking;
	obj.showUnitList = false;
	obj.activeUnitList = [];
	obj.unitList = [];
	obj.itemNumberOfDecimalsAllowed = [];
	obj.isValidProduct = true;
	obj.isBuyAllowed = true;
	obj.validQuantity = true;
	obj.UIErrorKey = null;
	obj.errorMsg = "";
	return obj;
};

MultipleAdd.prototype.addMoreLines = function() {
	for ( var i = 0; i < this.blankLinesToAdd; i++) {
		this.newPrdList.push(this.createBlankObject());
	}
	;
};

MultipleAdd.prototype.populateLines = function() {
	appendItemsToList(this.prdlist, this.newPrdList);
	this.setLineStatus(this.newPrdList);
};

MultipleAdd.prototype.addItemToList = function(Obj) {
	var isObjAdded = false;
	// to fill the blank
	for ( var i = 0; i < this.newPrdList.length; i++) {
		var item = this.newPrdList[i];
		if (isNull(item.itemCode)) {
			this.newPrdList[i] = Obj;
			isObjAdded = true;
		}
	}
	// add new row
	if (!isObjAdded) {
		this.newPrdList.push(Obj);
	}
};

MultipleAdd.prototype.array2Obj = function(list) {
	var obj = null;
	if (Array.isArray(list)) {
		var size = list.length;
		switch (size) {
		case 1:
			obj = this.createObject(list[0]);
			break;
		case 2:
			obj = this.createObject(list[0], list[1]);
			break;
		case 3:
			obj = this.createObject(list[0], list[1], list[2]);
			break;
		case 4:
			obj = this.createObject(list[0], list[1], list[2], list[3]);
			break;
		case 5:
			obj = this
					.createObject(list[0], list[1], list[2], list[3], list[4]);
			break;
		}
		;
	}
	return obj;
};

MultipleAdd.prototype.StringToList = function(copyText) {
	this.newPrdList = [];
	if (this.validateCopyPasteOrder(copyText, this.delimeter)) {

		var copyOrderLineList = copyText.split("\n");
		/* To check delimeter */
		for ( var i = 0; i < copyOrderLineList.length; i++) {
			var addItemOrderLineList = copyOrderLineList[i]
					.split(this.delimeter);
			var obj = this.array2Obj(addItemOrderLineList);
			this.addItemToList(obj);
		}
		return true;
	} else {
		return false;
	}
};

MultipleAdd.prototype.checkString = function(str) {
	if (isNull(str)) {
		return '';
	} else {
		return str;
	}
};

MultipleAdd.prototype.ListToString = function() {
	var orderText = '';

	var delimeter = this.delimeter;
	if (!isNull(this.prdlist)) {
		for ( var i = 0; i < this.prdlist.length; i++) {
			var item = this.prdlist[i];
			orderText = orderText + this.checkString(item.itemCode) + delimeter
					+ this.checkString(item.ordered) + delimeter
					+ this.checkString(item.unit);
			if (!isEmptyString(this.checkString(item.delDate))) {
				orderText = orderText + delimeter
						+ this.checkString(item.delDate);
			}
			if (i < this.prdlist.length - 1) {
				orderText = orderText + "\n";
			}
		}
	}
	this.orderText = orderText;
	return orderText;
};

// MultipleAdd.prototype.isValidOrder =function() {
// var orderList = this.createtNewOrderList(this.newPrdList);
// return this.validateOrderList(Orderlist);
// };

MultipleAdd.prototype.clearOrderText = function() {
	this.orderText = '';
};

MultipleAdd.prototype.validateCopyPasteOrder = function(copyText, delimeter) {
	var isOrderValid = true;
	// $scope.orderValidationMsg = "";

	if (delimeter == "" || delimeter == null || !isValidDelimeter(delimeter)) {
		isOrderValid = false;
		this.UIErrorKey = 'MSG_DELIMITER_MUST_BE_SINGLE_CHAR';
		return isOrderValid;
	}

	if (copyText != null) {
		var copyOrderLineList = copyText.split("\n");
		/* To check delimeter */

		for ( var i = 0; i < copyOrderLineList.length; i++) {
			if (copyOrderLineList[i].length > 0) {
				var orderLineKeyLength = copyOrderLineList[i].split(delimeter).length;
				if (orderLineKeyLength < 2) {
					isOrderValid = false;
					this.UIErrorParams = [ i + 1, 2 ];
					this.UIErrorKey = 'MSP_LINE_MIN_2_VALUES';
				} // to check minimum no. of tokens in a line
				if (orderLineKeyLength > 5) {
					isOrderValid = false;
					this.UIErrorParams = [ i + 1, orderLineKeyLength ];
					this.UIErrorKey = 'MSP_LINE_MAX_5_VALUES';
				}
				// to check maximum no. of tokens in a line
			}
		}
	} else {
		isOrderValid = false;
	}

	if (isOrderValid) {
		this.UIErrorParams = [];
		this.UIErrorKey = null;
	}
	return isOrderValid;
};

// validating Order list
MultipleAdd.prototype.validateOrderList = function(Orderlist) {
	var translateFunc = this.translateFunc;
	var isValidOrder = true;
	if (Orderlist.length > 0) {
		for ( var i = 0; i < Orderlist.length; i++) {
			var ordItem = this.newPrdList[i];

			if (isEmptyString(Orderlist[i].itemCode)) {
				// ordItem.UIErrorKey = 'MSG_ITEM_NOT_SPECIFIED';
				// ordItem.UIErrorParams = [];
				addUIError(ordItem, translateFunc, 'MSG_ITEM_INVALID_UNIT');
				ordItem.isValidProduct = false;
				isValidOrder = false;
			}

			if (isEmptyString(Orderlist[i].ordered)) {
				// ordItem.UIErrorKey = 'MSG_QUANTITY_NOT_SPECIFIED';
				// ordItem.UIErrorParams = [];
				addUIError(ordItem, translateFunc, 'MSG_QUANTITY_NOT_SPECIFIED');
				ordItem.isValidProduct = false;
				isValidOrder = false;
			} else {
				// var isNumber = isInt(Orderlist[i].ordered);
				var pattern = /^[0-9.,]*$/;
				if (!pattern.test(Orderlist[i].ordered)) {
					// ordItem.UIErrorKey = 'MSG_INVALID_QUANTITY' ;
					// ordItem.UIErrorParams = [];
					addUIError(ordItem, translateFunc, 'MSG_INVALID_QUANTITY');
					isValidOrder = false;
					ordItem.isValidProduct = false;

				}
			}

			if (isValidOrder) {

				ordItem.isValidProduct = true;

			}

		}
	} else {
		isValidOrder = false;
		this.UIErrorKey = 'MSG_ITEM_NOT_SPECIFIED';
		this.UIErrorParams = [];
	}
	// if(isValidOrder){
	// this.UIErrorKey = null;
	// this.UIErrorParams = [];
	// }
	return isValidOrder;
};

MultipleAdd.prototype.createtNewOrderList = function(orderList) {

	var itemsLinesObjList = [];
	if (!isNull(orderList)) {
		for ( var i = 0; i < orderList.length; i++) {
			var itemLineObj = new Object();
			if (this.isOrderLineValid(orderList[i].itemCode,
					orderList[i].ordered)) {
				if (orderList[i].hasOwnProperty("isValidProduct")
						&& orderList[i].isValidProduct == false) {
					continue;
				}
				itemLineObj = this.createObject(orderList[i].itemCode,
						orderList[i].ordered, orderList[i].unit,
						orderList[i].delDate, orderList[i].shipmentMarking);

				itemsLinesObjList.push(itemLineObj);
			}

		}

	}

	return itemsLinesObjList;
};

MultipleAdd.prototype.isOrderLineValid = function(itemCode, itemQuantity) {

	var isValid = true;
	if (isEmptyString(itemCode) && isEmptyString(itemQuantity)) {
		return false;
	}

	if (!isEmptyString(itemCode)) {
		itemCode = itemCode.trim();
	}

	if (!isEmptyString(itemQuantity)) {
		itemQuantity = itemQuantity.trim();
	}
	return isValid;
};

MultipleAdd.prototype.getOrderParam = function(skipInteger) {
	// var orderItemList = $scope.createAddItemOrderList();

	var orderList = this.createtNewOrderList(this.newPrdList);

	var requestParam = "";
	// if(this.validateOrderList(orderList))
	// {
	// requestParam = "inputItems="+JSON.stringify(orderList);
	//
	// }
	if (!isNull(orderList) && orderList.length > 0)
	// if(this.validateOrderList(orderList))
	{
		var buyObj = new Buy(orderList);
		buyObj.setPropNameItemCode('itemCode');
		buyObj.setPropNameOrdered('ordered');
		buyObj.setPropNameUnit('unit');
		buyObj.setPropNameDelDate('delDate');
		buyObj.setPropNameShipmentMarking('shipmentMarking');
		buyObj.setPropNameisBuyAllowed('isBuyAllowed');
		requestParam = buyObj.getParam(null, skipInteger);
	}

	return requestParam;
};

MultipleAdd.prototype.validateProductLine = function(productLine, lineObject,
		page) {

	// var rootScope = this.rootScope;
	// var requestParam = getItemCodeRequestParam(productLine.itemCode,page) ;
	// if(!isEmptyString(productLine.itemCode))
	// {
	// rootScope.fetchItemDetails(requestParam,productLine,lineObject);
	// }
	// else
	// {
	// this.UIErrorKey = null;
	// this.UIErrorParams = [];
	// }

};

MultipleAdd.prototype.getUIErrorKey = function() {
	return this.UIErrorKey;
};

MultipleAdd.prototype.getUIErrorParams = function() {
	return this.UIErrorParams;
};

MultipleAdd.prototype.getList = function() {
	return this.newPrdList;
};

MultipleAdd.prototype.getLineList = function() {
	var list = this.newPrdList;
	return list;
};

MultipleAdd.prototype.AddProductListToCart = function() {
	// var rootScope = this.rootScope;
	// rootScope.AddMultipleItemsToCart(this.getOrderParam());

};

MultipleAdd.prototype.setLineStatus = function(list) {
	if (!isNull(list)) {
		for ( var i = 0; i < list.length; i++) {
			list[i].isValidProduct = true;
			if (list[i].restricted == true) {
				var translateFunc = "";
				addUIError(list[i], translateFunc, 'MSG_INVALID_ITEM');
				list[i].isValidProduct = false;
				this.isValidProductList = false;

			}
			list[i].delDate = ''; // by default delivery date should be blank
			list[i].UIErrorKey == null;
			// list[i].ordered = ChangeToInteger(list[i].ordered);
		}
	}

};

MultipleAdd.prototype.setLineObjectStatus = function(line, lineObject, status) {
	var lineHashKey = line.$$hashKey;

	for ( var i = 0; i < lineObject.newPrdList.length; i++) {
		if (lineHashKey == lineObject.newPrdList[i].$$hashKey) {
			lineObject.newPrdList[i].isValidProduct = status;
		}
	}

};
// //
// MultipleAdd.prototype.validateProductList = function(lineListObject,page)
// {
// var rootScope = this.rootScope;
// var lineList = lineListObject.getLineList();
// var paramList = this.getProductParamList(lineList,page);
// var requestParam = getProductListRequestParam(paramList);
//		
// rootScope.fetchProductsDetails(requestParam,lineListObject);
//	   
//	   
// };

MultipleAdd.prototype.getProductParamList = function(lineObjectList, page) {
	var lineParamList = [];
	if (!isNull(lineObjectList)) {
		for ( var i = 0; i < lineObjectList.length; i++) {
			var lineParamObject = new Object();
			lineParamObject['itemCode'] = encodeURIComponent(lineObjectList[i].itemCode);
			lineParamObject['unitCode'] = returnBlankIfNull(lineObjectList[i].unit);
			lineParamObject['page'] = page;
			lineParamObject['shipmentMark'] = returnBlankIfNull(lineObjectList[i].shipmentMarking);
			if (!isNull(lineObjectList[i].itemCode)) {
				lineParamList.push(lineParamObject);
			}
		}

	}
	//
	return lineParamList;

};

var validateDate = function(dateStr, dateFormat) {
	this.validExpireDate = true;
	if (dateStr == "" || dateStr == null || isNull(dateStr)) {
		return true;
	}
	var dateValidator = new DateValidator(dateFormat);
	var isValidDate = dateValidator.isValidDate(dateStr);
	if (!isValidDate) {
		return false;
	} else {
		return true;
	}
};