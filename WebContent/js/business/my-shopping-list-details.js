function MyShoppingListDetails(translateFunc){			
		this.pageDataFound = false;
		this.UIMessageKey=null;
		this.moreRecords = false;
		this.messageCode = "";				
		this.translateFunc = translateFunc;
		this.listHeader = {};
		this.listData = {};
	};
	
	MyShoppingListDetails.prototype.getListData = function(){
		return this.listData;
	};
	
	MyShoppingListDetails.prototype.setListData = function(data,isFilterOn,isPagedData)
	{	
		
		if(isNull(data) || data.messageCode==111)
		{
			this.listData ={}; 
			this.pageDataFound = false;
		}
		else
		{
			this.pageDataFound = true;
			this.moreRecords = data.moreRecords;
			this.listHeader = data[0];		
			changeQuantityValueToInt(data.listOfLine);
			this.listData = data;		
		}
		this.setSelectedUnitObj();
	};
	
	MyShoppingListDetails.prototype.setSelectedUnitObj = function (){
		if(!isNull(this.listData) && !isNull(this.listData.listOfLine)){
			for (var i=0;i<this.listData.listOfLine.length ; i++){
				var item = this.listData.listOfLine[i];
				item.selectedUnitObject= {};
				item.selectedUnitObject = getSelectedSalesUnit(item.defaultSalesUnit,item.salesUnitsDesc);
			}
		}
	};