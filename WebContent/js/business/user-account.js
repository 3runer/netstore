function UserAccount() {
		this.userId= null;
		this.userName= null;
		this.userEmail= null;
		this.defaultCustomerName= null;
		this.defaultPayment= null;			
		this.BusinessPartnerAddressList= [];
		this.localeList = [];
		this.selectedLocale = {};
		this.allowToModify = false;
		this.startShowError = false;
		this.isValidEmail = true;
		this.UIErrorKey=null;
	}
	UserAccount.prototype.handleMessageCode = function(responseData)
	{
		var errorMsg = null;
		if(isNull(responseData)){
			errorMsg = 'MSG_ITEM_UPDATE_FAILED';
			//this.allowToModify = true;
		} else if(!isNull(responseData.messageCode))
		{
			switch (responseData.messageCode){
			case 4600 :
				errorMsg = null;
			//	this.allowToModify = false;
				break;
			
			case 4601 :
				errorMsg = 'MSG_ITEM_UPDATE_FAILED';
				//this.allowToModify = true;
				break;
			}
		}			
		this.UIErrorKey = translate.getTranslation(errorMsg);
		
	};
	
	UserAccount.prototype.validateEmail = function(){		
		var errorMsg = null;
		var isValidMail = isValidEmailAddress(this.userEmail);
		if(!isValidMail){
			if(this.startShowError){
				errorMsg = 'MSG_INVALID_EMAIL_ADDRESS';
				this.isValidEmail = false;
			}
		}else{
			 errorMsg = null;
			this.isValidEmail = true;
		}
		
		this.UIErrorKey = translate.getTranslation(errorMsg);
		//return this.isValidEmail;
	};
	
	
	UserAccount.prototype.setAccountInfo = function(accountdata) {		
		this.userId= accountdata.userID;
		this.userName= accountdata.userName;
		this.userEmail= accountdata.userEmail;
		this.defaultCustomerName= accountdata.defaultCustomerName;		
		this.defaultPayment= accountdata.defaultPayment;
		this.BusinessPartnerAddressList= accountdata.BusinessPartnerAddresses;		
		this.selectedLocale = getObjectFromList('code',webSettings.localeCode,this.localeList);
	};
	UserAccount.prototype.getUIErrorKey = function() {
		return this.UIErrorKey;
	};
	UserAccount.prototype.setLocaleList = function(list) {
		this.localeList = list;
	};
	
	UserAccount.prototype.onLocaleChange = function() {
		if(!isNull(this.selectedLocale) && !isNull(this.selectedLocale.code)){
			this.request.locale=this.selectedLocale.code;
		}
	};
	
	UserAccount.prototype.setDefaultLocale = function(localeFromSession) {
		if(!isNull(localeFromSession)){
			this.selectedLocale = getObjectFromList('code',localeFromSession.code,this.localeList);
		}else if(isNull(this.selectedLocale)){
			if(!isNull(this.localeList) && !isNull(this.localeList.length>0)){
				this.selectedLocale=this.localeList[0];
			}
		} else{
			
			this.selectedLocale = getObjectFromList('code',webSettings.localeCode,this.localeList);
			
		}
		
	};
	UserAccount.prototype.isUserAddressExists = function(addressList,addressType) {
		var addressExists = false;
		if(!isNull(addressList))
		{
			for(var i=0;i<addressList.length;i++)
			{
				if(addressList[i].addressType == addressType)
					{
						addressExists = true;
					}

			}

		}
		return addressExists;
	};
	
	UserAccount.prototype.isdefaultAddress = function(addressList,addressType) {
		var isdefaultAddress = false;
		if(!isNull(addressList))
		{
			for(var i=0;i<addressList.length;i++)
			{
				if(addressList[i].addressType == addressType)
					{
						isdefaultAddress = addressList[i].isDefaultAddress;
					}

			}

		}
		return isdefaultAddress;
	};