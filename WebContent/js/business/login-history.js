function LoginHistory(){
		this.historyList = null;
		this.maxLimit = 5;
		this.storageKey = "NS_LOGIN_LIST";
		this.lastUserKey = "LAST_LOGIN";
		this.loadFromLocalStorage();
	};
	

	LoginHistory.prototype.loadFromLocalStorage= function(){
		if(!isNull(window.localStorage)){
			var retrievedObject = window.localStorage.getItem(this.storageKey);
			if(!isNull(retrievedObject)){
				this.historyList = JSON.parse(retrievedObject);
			}
		}
	};

	LoginHistory.prototype.updateLocalStorage= function(){
		this.removeOldHistory();
		if(!isNull(window.localStorage)){
			if(!isNull(this.historyList)){
				window.localStorage.removeItem(this.storageKey);
				window.localStorage.setItem(this.storageKey, JSON.stringify(this.historyList));
			}
		}
	};

	//returns true if user exist and successfully updated, else returns false
	LoginHistory.prototype.updateUser = function (sessionId, userId){
		var isUpdated = false;
		var sessionUser = this.getObjectBySessionId(sessionId);
		if(!isNull(sessionUser)){
			sessionUser.userId=userId;
			this.updateLocalStorage();
			isUpdated = true;
		}
		return isUpdated;
	};
	
	LoginHistory.prototype.setLastUser = function (userId){
		if(!isNull(window.localStorage)){
				window.localStorage.setItem(this.lastUserKey, userId);
		}
	};
	
	LoginHistory.prototype.getLastUser = function (){
		var userId = "";
		if(!isNull(window.localStorage)){
			userId = window.localStorage.getItem(this.lastUserKey);
		}
		return userId;
	};
	
	LoginHistory.prototype.logoutUser = function (sessionId){
		var isUpdated = false;
		var sessionUser = this.getObjectBySessionId(sessionId);
		if(!isNull(sessionUser)){
			sessionUser.userId=null;
			sessionUser.token=null;
			this.updateLocalStorage();
			isUpdated = true;
		}
		return isUpdated;
	};

	LoginHistory.prototype.addUser = function(sessionId, userId){
		if(isNull(this.historyList)){
			this.historyList = [];
		}
		if(!this.updateUser(sessionId, userId)){
			var userHistoryObj = new Object();
			userHistoryObj['token'] = sessionId;
			userHistoryObj['userId'] = userId;
			this.historyList.push(userHistoryObj);
			this.updateLocalStorage();
		}
		this.setLastUser(userId);
	};

	LoginHistory.prototype.getObjectBySessionId = function(sessionId){
		this.loadFromLocalStorage();
		var obj = getObjectFromList('token',sessionId,this.historyList);
		return obj;
	};

	LoginHistory.prototype.removeOldHistory = function(){
		if(!isNull(this.historyList) && this.historyList.length>this.maxLimit){
			this.historyList.splice(0,this.historyList.length-this.maxLimit);
		}
	};