function ThankYouPage(orderResponse){
	this.orderResponse = orderResponse;
	this.isOrderPlaced = false;
	this.isViewOrder = false;
	this.orderErrorMsg = null;
	this.orderNumber="";
	this.paymentErrorMessage = null;
	this.enterpriseErrorMessage = null;
	this.transactionId = null;
	this.init();
};

ThankYouPage.prototype.init = function() {
	if(!isNull(this.orderResponse) && isNull(this.orderResponse.messageCode)){
		this.cartStatus = translate.getTranslation('MSG_SUCCESS');
		this.orderNumber = this.orderResponse.orderNumber;
		this.isViewOrder = this.orderResponse.isViewOrder;
		if(this.orderNumber > 0) {
			this.cartStatus = translate.getTranslation('MSG_SUCCESS');
			this.cartMsg =  translate.getTranslation('MSG_ORDER_RECEIVED_ASSIGNED_ORDER_NUMBER',[ this.orderNumber]);
			this.isOrderPlaced = true;
		}else{
			this.cartStatus = translate.getTranslation('MSG_FAIL');
			this.isOrderPlaced = false;
		}
		if(!isNull(this.orderResponse.errorMessage) && !isEmptyString(this.orderResponse.errorMessage)){
			this.orderErrorMsg = this.orderResponse.errorMessage;
		}else{
			this.orderErrorMsg = null;
		}
		
		if(!isNull(this.orderResponse.paymentErrorMessage) && !isEmptyString(this.orderResponse.paymentErrorMessage)){
			this.paymentErrorMessage = this.orderResponse.paymentErrorMessage;
		} 
		
		if(!isNull(this.orderResponse.transactionId) && !isEmptyString(this.orderResponse.transactionId)){
			this.transactionId = this.orderResponse.transactionId;
		} 
	}else if(!isNull(this.orderResponse.messageCode)){
		this.cartStatus = translate.getTranslation('MSG_FAIL');
		this.setErrorMsg(this.orderResponse.messageCode);
	}
};

ThankYouPage.prototype.setErrorMsg = function(messageCode){
	if(!isNull(messageCode)){
		var msgCode = messageCode +"";
		switch(msgCode){
		case "9021" :
			this.enterpriseErrorMessage =  translate.getTranslation('MSG_GIVEN_KEY_IS_MISSING_FOR_MERCHANT_ID_AT_ENTERPRISE');
			break;
		case "9022" :
			this.enterpriseErrorMessage =  translate.getTranslation('MSG_PAYMENT_MERCHANT_ID_IS_BLANK_AT_BO');
			break;
		case "9023" :
			this.enterpriseErrorMessage =  translate.getTranslation('MSG_GIVEN_KEY_IS_MISSING_FOR_TOKEN_ID_AT_ENTERPRISE');
			break;
		case "9024" :
			this.enterpriseErrorMessage =  translate.getTranslation('MSG_PAYMENT_TOKEN_ID_IS_BLANK_AT_BO');
			break;
		case "9025" :
			this.enterpriseErrorMessage =  translate.getTranslation('MSG_PAYMENT_HOST_NAME_IS_NOT_DEFINED_AT_BO');
			break;
		case "9026" :
			this.enterpriseErrorMessage =  translate.getTranslation('MSG_REMAINING_IN_ORDER_INTERFACE_IS_TRUE');
			break;
		case "9027" :
			this.enterpriseErrorMessage =  translate.getTranslation('MSG_PAYMENT_ONLINE_SUPPORT_ENTERPRISE_8_ONWARDS');
			break;
		default :
			this.enterpriseErrorMessage =null;
		}
	}
};

ThankYouPage.prototype.canViewOrder = function(){
	return this.isViewOrder;
};

ThankYouPage.prototype.setOrderResponse = function(orderResponse){
	this.orderResponse = orderResponse;
	this.init();
};

ThankYouPage.prototype.getOrderErrorMsg = function(){
	return this.orderErrorMsg;
};
ThankYouPage.prototype.getOrderDetailRequestParam = function(){
	var param = ["OrderNumber", this.orderNumber];
	return param;
};