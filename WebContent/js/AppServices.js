var userResponseObject_Key = 'userResponseObject';
var sessionTrackerObject_Key = 'sessionTrackerObject';
var languageResource_Key = 'languageResource_Key'; 
var languageCode_Key = 'languageCode_Key'; 
var  IS_AUTHENTICATED = 'isAuthenticated';
var SESSION_ID='sessionId';
var FirstPage = "first";
var NextPage = "next";
var PrevPage = "prev";

var AppServicesModule = angular.module('AppServicesModule', []);

AppServicesModule.factory('DataSharingService', function($http,$q,$log,$window,$rootScope){
	function KeyValue (key,value) {
		this.key = key;
		this.value = value;
	}
	
	var serviceObject = { 
			keyValueList:[]
	};
	var  winSession = $window.sessionStorage; 
	var  winLocalSession = $window.localStorage;
	serviceObject.setObject = function(key,ValueObject){
		//remove already existing object
		this.removeObject(key);
		//set new object
		kvObj = new KeyValue(key,ValueObject);
		this.keyValueList.push(kvObj);
	//	$log.log("DataSharingService.setObject Key= "+key +", Value= " +ValueObject);
	};

	serviceObject.setToSessionStorage = function(key,ValueObject){
		//remove existing object
		winSession.removeItem(key);
		//set object to window session
		winSession.setItem(key, JSON.stringify(ValueObject));
	};

	serviceObject.removeFromSessionStorage = function(key){
		//remove existing object
		winSession.removeItem(key);
	};

	serviceObject.getFromSessionStorage = function(key){
		var retrievedObject = winSession.getItem(key);
		var retrievedObjectJSON = null;
		if(!isNull(retrievedObject)){
			try{
				retrievedObjectJSON = JSON.parse(retrievedObject);
			}catch(err){}
		}
		return retrievedObjectJSON;
	};
	
	serviceObject.setToLocalStorage = function(key,ValueObject){
		//remove existing object
		winLocalSession.removeItem(key);
		//set object to window local storage
		winLocalSession.setItem(key, JSON.stringify(ValueObject));
	};

	serviceObject.removeFromLocalStorage = function(key){
		//remove existing object
		winLocalSession.removeItem(key);
	};

	serviceObject.getFromLocalStorage = function(key){
		var retrievedObject = winLocalSession.getItem(key);
		var retrievedObjectJSON = null;
		if(!isNull(retrievedObject)){
			try{
				retrievedObjectJSON = JSON.parse(retrievedObject);
			}catch(err){}
		}
		return retrievedObjectJSON;
	};

	serviceObject.getWebSettings = function()
	{
		return this.getFromSessionStorage('webSettingsObject');
	};

	serviceObject.setWebSettings = function(webSettings)
	{
		this.setToSessionStorage("webSettingsObject",webSettings);
	};

	serviceObject.getLanguageCode = function()
	{
		
			return this.getFromSessionStorage(languageCode_Key);
		
	};

	serviceObject.setLanguageCode = function(langCode)
	{
		
		
			return this.setToSessionStorage(languageCode_Key,langCode);
		
	};

	serviceObject.setLanguagePropJsonObject = function(propJsonObject)
	{
		this.setObject(languageResource_Key,propJsonObject);
		return this.setToSessionStorage(languageResource_Key,propJsonObject);
	};
	
	serviceObject.getLanguagePropJsonObject = function()
	{
		var languageResource = null;
		//Avoid every time read from browser session;
		languageResource = this.getObject(languageResource_Key);
		
		if(isNull(languageResource)){
			languageResource = this.getFromSessionStorage(languageResource_Key);
			$rootScope.langResourceLoaded = true;
		}

		return languageResource;
	};
	
	serviceObject.setLangList = function(langList)
	{
		this.setToSessionStorage("supported_lang_list",langList);
	};

	serviceObject.getLangList = function()
	{
		return this.getFromSessionStorage("supported_lang_list");
	};

	serviceObject.setConfiguration = function(ValueObject){
		this.setObject("ConfigData",ValueObject);
	};

	serviceObject.getConfiguration = function(){
		return this.getObject("ConfigData");
	};
	serviceObject.getFilterDetailsSettings = function()
	{
		return this.getFromSessionStorage('filterDetailsSettingsObject');
	};

	serviceObject.setFilterDetailsSettings = function(filterDetailsSettings)
	{
		this.setToSessionStorage("filterDetailsSettingsObject",filterDetailsSettings);
	};
	serviceObject.getProductFilterDetailsSettings = function()
	{
		return this.getFromSessionStorage('productSearchDetailsSettingsObject');
	};

	serviceObject.setProductFilterDetailsSettings = function(filterDetailsSettings)
	{
		this.setToSessionStorage("productSearchDetailsSettingsObject",filterDetailsSettings);
	};
	
	serviceObject.removeProductFilterDetailsSettings = function()
	{
		this.removeFromSessionStorage("productSearchDetailsSettingsObject");
	};


	serviceObject.getObject = function(key){
		var kvobj = null;
		if(key != null  && typeof(key) == "string") {
			for (var i = 0; i < this.keyValueList.length; i++) { 
				kvobj = this.keyValueList[i];
				if (kvobj.key == key ){
					kvobj = kvobj.value;
					break;
				}
				else{
					kvobj = null;
				}
			}
		}
	//	$log.log("DataSharingService.getObject Key= "+key +", Value= " +kvobj);
		return kvobj;
	};

	serviceObject.removeObject = function(key){
		if(key != null  && typeof(key) == "string") {
			var kvobj ;
			for (var i = 0; i < this.keyValueList.length; i++) { 
				kvobj = this.keyValueList[i];
				if (kvobj.key == key ){
					this.keyValueList.splice(i,1);
				}
			}
		}
	};

	return serviceObject;
});

AppServicesModule.factory('NewConfigService',['DataSharingService','Util','$rootScope','$http','$q','$log','$window',
                                              function(DataSharingService,Util,$rootScope, $http,$q,$log,$window){
	var newConfigServiceObj = {
			configObj : {}
	};

	var  winSession = $window.sessionStorage; 

	function msiecheck() 
	{
		var rv = -1; // Return value assumes failure.
	    if (navigator.appName == 'Microsoft Internet Explorer' || navigator.appVersion.indexOf('Trident') != -1){
	       rv = 1;
	    }
		return rv;   
	}
	//Get and load property file data
	newConfigServiceObj.initConfigurationData = function(){
		var deferred = $q.defer();	
		var config ;
		config = DataSharingService.getConfiguration();
		if(!angular.isDefined(config) || config == null )
		{
			//$log.info("NewConfigService.initConfigurationData() -- going to fetch config data from server");
			$http.get("./config/config.properties").success(function(data){
				DataSharingService.setConfiguration(data);
				this.configObj = data;
				//var deferred2 = $q.defer();	
				//Get and load Web Settings from WS
//				Util.getDataFromServer('webSettingsUrl',[]).then(function(data1){
//				DataSharingService.setWebSettings(data1);
//				$rootScope.webSettings = data;
//				deferred2.resolve(data1);	
				deferred.resolve(data);
//				$log.info("initConfigurationData(), web settings: " + JSON.stringify(DataSharingService.getWebSettings()));
//				});
				//newConfigServiceObj.loadLangList();
				//return  deferred2.promise;	
			}).error(function(data, status) {
				//$log.error("NewConfigService.initConfigurationData() -- could not fetch config data from server " + status);
			});
		}else{
			//$log.info("NewConfigService.initConfigurationData() -- config data already initialized");
		}
		return  deferred.promise;	
	};
	
	newConfigServiceObj.getWSBaseAddress = function(){
		configObj = DataSharingService.getConfiguration();
		var WSBaseAddress = "";
		if(Util.isNotNull(configObj)){
			var baseUrl = configObj['baseUrl'];
			var port = configObj['port'];
			WSBaseAddress =   baseUrl+ port;
		}
		return WSBaseAddress;
	};
	
	newConfigServiceObj.loadWebSettings = function(userId){
		//Load Web Settings from WS
		var deferred = $q.defer();	
		var params = [];
		if(Util.isNotNull(userId)  && userId!=''){
			params = ["UserID",userId]	;
		}
		Util.getDataFromServer('webSettingsUrl',params).then(function(data){
			DataSharingService.setWebSettings(data);
			setWebSettingObject(data);
			if(!isNull(data.languageCode) && (msiecheck() != 1)){
				DataSharingService.setLanguageCode(data.languageCode);
			}else if(!isNull(data.languageCode) && (msiecheck() == 1) && !isNull(userId)){
				DataSharingService.setLanguageCode(data.languageCode);
			}
			$rootScope.webSettings = data;
			deferred.resolve(data);	
			//console.log("initConfigurationData(), web settings: " + JSON.stringify(DataSharingService.getWebSettings()));
		});
		return  deferred.promise;	
	};
	
	newConfigServiceObj.loadCustomerDetail = function(){
		Util.getDataFromServer('loadCustomerDetailUrl',"").then(function(data){
			//return deferred.promise;
		});
	};

	newConfigServiceObj.getWebUser = function(params){
		var deferred = $q.defer();	
		Util.getDataFromServer('getWebUserUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return  deferred.promise;	
	};

	newConfigServiceObj.init = function(userId){
		var deferred = $q.defer();
		$rootScope.openLoader();
		newConfigServiceObj.initConfigurationData().then(function(data){
			newConfigServiceObj.loadWebSettings(userId).then(function(data){
				newConfigServiceObj.loadLangList().then(function(data){
					var langCode = newConfigServiceObj.getLanguageCode();
					//newConfigServiceObj.loadCustomerDetail();
					return newConfigServiceObj.loadLanguageMsgFile(langCode).then(function(data){
						$rootScope.closeLoader();
						deferred.resolve();	
						updateLocalTranslations();
					});
				});
			});
		});
		this.initFilterConfig();
		return  deferred.promise;	
	};

	//Load list of supported languages
	newConfigServiceObj.loadLangList = function(){
		var deferred = $q.defer();	
		var langList = DataSharingService.getLangList();
		if(langList==null || langList.length == 0){
			Util.getDataFromServer('getAllLanguagesUrl',[]).then(function(data){
				//var testLang = new Object();
				//testLang.code="FI";
				//testLang.description="test";
				//data.langBeanList.push(testLang);
				DataSharingService.setLangList(data.langBeanList);
				$rootScope.languageList = data.langBeanList;
				deferred.resolve(data);
				//console.log("loadLangList(): " + JSON.stringify(data));
				updateLocalTranslations();
			});
		}else{
			$rootScope.languageList = langList;
			deferred.resolve(langList);
			updateLocalTranslations();
		}
		return  deferred.promise;
	};
	
	var updateLocalTranslations = function() {
		for(var i = 0; i < $rootScope.languageList.length; i++) {
			var translated = Util.translate('TXT_LANGUAGE_NAME_'+$rootScope.languageList[i].code);
			if(!isNull(translated) && translated != "") {
				$rootScope.languageList[i].translated = translated;
			} else {
				$rootScope.languageList[i].translated = $rootScope.languageList[i].description;
			}
		}
	}

	newConfigServiceObj.getLanguageCode = function(){
		var langCodeStoreinSession = DataSharingService.getLanguageCode();
		var webSettings = DataSharingService.getWebSettings();
		var langCode = null;
		if(angular.isDefined(langCodeStoreinSession) && langCodeStoreinSession!=null){
			langCode = langCodeStoreinSession;
			/*if(langCode !=  webSettings.languageCode)
				{
					langCode = webSettings.languageCode;
				}*/
		}else{
			
			langCode = webSettings.languageCode;
			if(!angular.isDefined(langCode) || langCode==null || langCode.length == 0){
				//default is english
				langCode = "EN";
			}
		}
		return langCode;
	};

	newConfigServiceObj.setSelectedLang = function(langCode) {
		//store in session
		DataSharingService.setLanguageCode(langCode);

		var langList = $rootScope.languageList;
		for (var i=0;i<langList.length ;i++){
			if(langList[i].code == langCode){
				$rootScope.selectedLanguage = langList[i];
				break;
			}
		}
	};

	newConfigServiceObj.changeLanguage = function(langCode){
		if(angular.isDefined(langCode) && langCode!=null){
			this.loadLanguageMsgFile(langCode).then(function(data){
				DataSharingService.setLanguageCode(langCode);
				$rootScope.$eval();
				//DataSharingService.setToLocalStorage("languageChanged", 1);
				window.location.reload();
			});
		}
	};
	
	//for no default customer
	newConfigServiceObj.changeLanguageCustomerDlg = function(langCode){
		if(angular.isDefined(langCode) && langCode!=null){
			this.loadLanguageMsgFile(langCode).then(function(data){
				DataSharingService.setLanguageCode(langCode);
				$rootScope.$eval();
			});
		}
	};
	
	
	newConfigServiceObj.changeLanguagePswDlg = function(dataChangePsw,callState,langCode){
		if(angular.isDefined(langCode) && langCode!=null){
			this.loadLanguageMsgFile(langCode).then(function(data){
				DataSharingService.setLanguageCode(langCode);
				$rootScope.showChangePwdDlg(dataChangePsw.userID,dataChangePsw.isForcePasswordChange,callState,langCode);
				$rootScope.$eval();
			});
		}
	};

	newConfigServiceObj.loadLanguageMsgFile = function(langCode){
		var deferred = $q.defer();
		if(!angular.isDefined(langCode) || langCode==null || langCode.length == 0){
			//$log.error("NewConfigService.loadLanguageMsgFile() -- invalid language code ");
		}else{
			filePath = "./config/NSNLS_" + langCode +".json";
		    
			$http.get(filePath).success(function(data){
				//$log.info("NewConfigService.loadLanguageMsgFile() -- language message file loaded, " + filePath);
				newConfigServiceObj.setSelectedLang(langCode);
				//DataSharingService.setObject(languageResource_Key,data);
				DataSharingService.setLanguagePropJsonObject(data);
				newConfigServiceObj.appendMissingPropFromEngFile(data,langCode);
				deferred.resolve(data);
				$rootScope.langResourceLoaded = true;
			}).error(function(data, status) {
				//$log.error("NewConfigService.loadLanguageMsgFile() -- could not load language message file "+filePath +", status: " + status);
				deferred.resolve(data);
				newConfigServiceObj.changeLanguage("EN");
			});
			//return deferred.promise;
		}
		return deferred.promise;
	};

	newConfigServiceObj.initFilterConfig = function()
	{
		var deferred = $q.defer();
		var filePath = "./config/filterConfig.json";
		$http.get(filePath).success(function(data){
			//$log.info("NewConfigService.initFilterConfig() --  filter Config file loaded, " + filePath);
			
			DataSharingService.setObject("filterConfig",data);
			deferred.resolve(data);
			
		}).error(function(data, status) {
			//$log.error("NewConfigService.initFilterConfig() -- could not load filter Config  file "+filePath +", status: " + status);
			deferred.resolve(data);
		});
		
		return deferred.promise;
	};
	
	newConfigServiceObj.getSortedKeys=function(){
		var en_File_old = "./config/NSNLS_EN.old.json";
		var en_File_new = "./config/NSNLS_EN.json";
		var oldObj = null;
		var newObj = null;
		$http.get(en_File_old).success(function(data){
			oldObj =data;
			$http.get(en_File_new).success(function(data){
				newObj =data;
				newConfigServiceObj.getDifferentKeys(oldObj, newObj);
			});
		});
	};
	
	newConfigServiceObj.appendMissingPropFromEngFile = function(currentLangProp,selectedLangCode){
		if(!isEqualStrings(selectedLangCode,"EN")){
			var en_File = "./config/NSNLS_EN.json";
			$http.get(en_File).success(function(enPropObj){
				//$log.log("Language: " + selectedLangCode);
				for(var key in enPropObj){
					if(!currentLangProp.hasOwnProperty(key)){
						currentLangProp[key]=enPropObj[key];
						//$log.log(key+"\t"+enPropObj[key]);
					}
				}
				DataSharingService.setLanguagePropJsonObject(currentLangProp);
			});
		}
	};
	
	newConfigServiceObj.getDifferentKeys = function(oldObj,newObj){
		var keys = [];
		for(var key in newObj){
			if(!oldObj.hasOwnProperty(key)){
				obj = new Object();
				obj[key]=newObj[key];
				keys.push(obj);
			}
		}
		//console.log("diff in keys: " + JSON.stringify(keys));
	};
	return newConfigServiceObj;
}]);

AppServicesModule.filter('translate', ['Util', function(Util) {
	//usage on html page with parameters - {{ 'Key_test' | translate:(['aa','bb','cc']) }}
	//usage on html page without parameters - {{ 'Key_testAbc' | translate }}
	return function(key, parameters) {

		return Util.translate(key,parameters);
	};
}]);

AppServicesModule.factory('HomeCatalogService',['NewConfigService','DataSharingService','Util','$http','$q','$log','SessionTracking',
                                                function(NewConfigService,DataSharingService,Util,$http,$q,$log,SessionTracking){
	var serviceObject = { };

	function tableHeader (actualName,displayName,isSortable,isAddtionalColumn,clickAction) {
		this.actualName = actualName;
		this.displayName = displayName;
		this.isSortable = isSortable;
		this.isAddtionalColumn = isAddtionalColumn;
		this.clickAction = clickAction;
	};

	serviceObject.tableHeaderObjList = [new tableHeader("imageURL","",false,false),
	                                    new tableHeader("code","Product",true,false),
	                                    new tableHeader("description","Description",true,false),
	                                    new tableHeader("actualPrice","Actual Price",false,false),
	                                    new tableHeader("discountPrice","Discount Price",false,false),
	                                    new tableHeader("discountPercentage","Discount %",false,false),
	                                    new tableHeader("defaultSalesUnit","Unit",false,false),
	                                    new tableHeader("Quantity","Quantity ",false,true),
	                                    new tableHeader("Enquiry","Enquiry",false,true),
	                                    new tableHeader("WarehouseInfo","Whs",false,true),  
	                                    new tableHeader("Select","Select ",false,true)
	];

	serviceObject.getTableHeaderObject = function(actualName){
		var headObj = null;
		for(var i=0; i< this.tableHeaderObjList.length;i++){
			headObj = this.tableHeaderObjList[i];
			if(headObj.actualName == actualName){
				break;
			}
		}
		if(headObj==null){
			//console.log("HomeCatalogService.getTableHeaderObject() : Invalid header name "+actualName);
		}
		return headObj;
	};

	serviceObject.getDefaultViewTableHeaderList = function(){
		var defaultHeaders = ["code","description","actualPrice","discountPrice","discountPercentage","defaultSalesUnit","Quantity","Select"];
		var newlist = [];
		for(var i=0; i<defaultHeaders.length;i++){
			newlist[i] = this.getTableHeaderObject(defaultHeaders[i]);
			//console.log("HomeCatalogService.getDefaultViewTableHeaderList() : Header name "+ newlist[i].displayName);
		} 
		return newlist;
	};

	serviceObject.getHomeCatalog = function(params){
		var deferred = $q.defer();
		params = params.concat(SessionTracking.getLanguageParams());
		Util.getDataFromServer('homeCatelogUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};

	return serviceObject;
}]);

AppServicesModule.factory('MainCatalogService',['Util','DataSharingService','$http','$q','$log',
                                                function(Util,DataSharingService,$http,$q,$log){
	var serviceObject = { };

	function tableHeader (actualName,displayName,isSortable,isAddtionalColumn,clickAction) {
		this.actualName = actualName;
		this.displayName = displayName;
		this.isSortable = isSortable;
		this.isAddtionalColumn = isAddtionalColumn,
		this.clickAction = clickAction;
	};

	serviceObject.tableHeaderObjList = [new tableHeader("imageURL","",false,false,""),
	                                    new tableHeader("code","Product",true,false,"showProductDetail(cat)"),
	                                    new tableHeader("description","Description",true,false,""),
	                                    new tableHeader("actualPrice","Actual Price",false,false,""),
	                                    new tableHeader("discountPrice","Discount Price",false,false,""),
	                                    new tableHeader("discountPercentage","Discount %",false,false,""),
	                                    new tableHeader("defaultSalesUnit","Unit",false,false,""),
	                                    new tableHeader("Quantity","Quantity ",false,true,""),
	                                    new tableHeader("Enquiry","Enquiry",false,true,""),
	                                    new tableHeader("WarehouseInfo","Whs",false,true,""),  
	                                    new tableHeader("Select","Select ",false,true,"")
	];

	serviceObject.getTableHeaderObject = function(actualName){
		var headObj = null;
		for(var i=0; i< this.tableHeaderObjList.length;i++){
			headObj = this.tableHeaderObjList[i];
			if(headObj.actualName == actualName){
				break;
			}
		}
		if(headObj==null){
			//console.log("MainCatalogService.getTableHeaderObject() : Invalid header name "+actualName);
		}
		return headObj;
	};



	serviceObject.getDefaultViewTableHeaderList = function(){
		var defaultHeaders = ["code","description","actualPrice","discountPrice","discountPercentage","defaultSalesUnit","Quantity","Select"];
		var newlist = [];
		for(var i=0; i<defaultHeaders.length;i++){
			newlist[i] = this.getTableHeaderObject(defaultHeaders[i]);
			//console.log("MainCatalogService.getDefaultViewTableHeaderList() : Header name "+ newlist[i].displayName);
		} 
		return newlist;
	};

	serviceObject.getMainCatalogProducts = function(params){
		//$log.info("MainCatalogService.getMainCatalog() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('mainCatalogProductListUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.getCatalogProducts = function(params){
		//$log.info("CatalogService.getMainCatalog() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('catalogProductListUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.getCatalogBredCrums = function(params){
		//$log.info("CatalogService.getMainCatalog() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('catalogBredCrumUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	

	serviceObject.getFilterConfiguration = function(){
		
		return Util.getFilterConfig('mainCatalogueFilterList'); 
		
	};
	return serviceObject;
}]);

AppServicesModule.factory('Util',['DataSharingService','SessionTracking','$http','$q','$log','$rootScope',
                                  function(DataSharingService,SessionTracking,$http,$q,$log,$rootScope){

	function Pagination (objKey, pageNo, wait,stopLoading){
		this.pageNo = pageNo;
		this.objKey = objKey;
		this.wait = wait||false;
		this.stopLoading = stopLoading || false;
		this.lastPageNo = -1;
	}

	Pagination.prototype.setLastPageNo = function () {
		this.lastPageNo = this.pageNo-1;		
	};

	Pagination.prototype.isLastPageNo = function () {
		if(this.lastPageNo == -1)
		{
			return false;
		}
		else {
			if(this.lastPageNo < this.pageNo){
				return true;
			}
		}
	};

	Pagination.prototype.setFirstPageNo = function () {
		this.pageNo = 1;
		return this.pageNo;
	};
	Pagination.prototype.incrementPageNo = function () {
		this.pageNo = this.pageNo+1;
		return this.pageNo;
	};
	Pagination.prototype.decrementPageNo = function () {
		if(this.pageNo > 1)
		{this.pageNo = this.pageNo-1;}	

		return this.pageNo;
	};

	Pagination.prototype.getPageNo = function () {				
		return this.pageNo;
	};
	
	Pagination.prototype.isFirstPage = function () {	
		if(this.pageNo <= 1)
			{
			 return true;
			}
		else
			{
			 return false;
			}

		
	};


	Pagination.prototype.stopPagination = function () {
		this.stopLoading =  true;
	};

	Pagination.prototype.getString = function () {
		return "Key:" +this.objKey + ", Wait: " + this.wait.toString() + ", stopLoading: " + this.stopLoading.toString();
	};



	var utilObject = { };

	utilObject.getDataFromServer = function(urlKey,params,showLoader,encodeUrl){
		if(isNull(encodeUrl)){
			encodeUrl = true;
		}
		this.validateSession();
		var methodType = this.getHttpMethodName();
		var finalUrl = this.getURL(urlKey,params,encodeUrl);
		var urlExists = false;
		try{
			$.grep($rootScope.dataUrlArray, function(value) {
				if (value == finalUrl) {
					urlExists = true;
				}
			});
			if (!urlExists) {
				$rootScope.dataUrlArray.push(finalUrl);
			}
		}catch(e){
			//console.log("ERROR");
			$rootScope.dataUrlArray = [];
			$rootScope.dataUrlArray.push(finalUrl);
		}
		//$log.info(finalUrl + " -- going to fetch data from server");
		var deferred = $q.defer();
		if(isNull(showLoader)){
			showLoader = true;
		}
		if(showLoader == false && urlKey == "getUserAdminMessageUrl"){
			showLoader = true;
		}
		if(showLoader && urlKey != "getUserAdminMessageUrl"){ $rootScope.openLoader(); $rootScope.openInnerLoader();}
		if(urlKey != "getUserAdminMessageUrl"){$rootScope.openInnerLoader();}
		if(urlKey == "catalogProductListUrl" ){
			$rootScope.closeLoader();
			$rootScope.closeInnerLoader();
		}else if(urlKey == "getUserAdminMessageUrl"){
			//$rootScope.closeInnerLoader();
		}
		
		$http({method: methodType, url: finalUrl, timeout:'600000'}).success(function(data, status, headers, config){
			if(utilObject.isNotNull(data.sessionId)){
				DataSharingService.setToSessionStorage(SESSION_ID,data.sessionId);
			}
			$rootScope.handleSessionNotFound(data, status, headers, config);
			deferred.resolve(data);
			//$rootScope.reLogin(data.messageCode);	
			
			$rootScope.dataUrlArray = $.grep($rootScope.dataUrlArray, function(url) {
		        return url != config.url;
		    });
			if(urlKey != "getUserAdminMessageUrl"){
				$rootScope.closeLoader();
				$rootScope.closeInnerLoader();

			}
			//SIMONE
			if(utilObject.saveOnLocal()){
				if(urlKey != "getUserAdminMessageUrl"){
					var blob = new Blob([JSON.stringify(data)], {type: "text/plain;charset=utf-8"});
					var saveAs = window.saveAs;
					saveAs(blob, urlKey + ".json");				
				}
			}
		}).error(function(data, status, headers, config) {
			$rootScope.closeLoader();
			$rootScope.closeInnerLoader();
			deferred.resolve(data);
			$rootScope.dataUrlArray = $.grep($rootScope.dataUrlArray, function(url) {
		        return url != config.url;
		    });
			//$log.error("Util.getDataFromServer(), url: " +finalUrl +" - could not fetch data from server " +  JSON.stringify(status));
		});
		return deferred.promise;
	};

	utilObject.saveOnLocal = function(){
		var config = DataSharingService.getConfiguration();
		if(config!=null){
			var saveJsonOnLocalDisk = config['saveJsonOnLocalDisk'];
			if(saveJsonOnLocalDisk!=null && 'TRUE' === saveJsonOnLocalDisk.toUpperCase()){
				return true;
			}else {
				return false;
			}
		}
	};
	
	utilObject.postDataToServer = function(urlKey,Obj,showLoader){
		this.validateSession();
		//var methodType = "JSONP";
		if(urlKey=="addItemsToCartUrl"){
			$rootScope.disableCartClick = true;
		}
		var finalUrl = this.getURLForPost(urlKey,true);
		//var finalUrl = "http://10.29.21.82:8082/NS/WS/NSWS/RecalculateCart?";
		var sess = DataSharingService.getFromSessionStorage(SESSION_ID);
		finalUrl =  finalUrl+";jsessionid="+sess;
		//$log.info(finalUrl + " -- going to fetch data from server");
		$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
		var deferred = $q.defer();
		if(isNull(showLoader)){
			showLoader = true;
		}
		if(showLoader){ $rootScope.openLoader(); }
	//	$rootScope.openInnerLoader();
        $http.post(finalUrl, Obj).success(function(data) {
			//$log.log("Util.postDataToServer(), url: " +finalUrl +" " + JSON.stringify(data));
        	if(urlKey=="addItemsToCartUrl"){
    			$rootScope.disableCartClick = false;
    		}
        	data = removeDefaultCallBack(data);
			$rootScope.handleSessionNotFound(data);
			deferred.resolve(data);	
			$rootScope.closeLoader();
			//SIMONE
			if(utilObject.saveOnLocal()){
				if(urlKey != "getUserAdminMessageUrl"){
					var blob = new Blob([JSON.stringify(data)], {type: "text/plain;charset=utf-8"});
					var saveAs = window.saveAs;
					saveAs(blob, urlKey + ".json");				
				}
			}
        }).error(function(data,status) {
        	if(urlKey=="addItemsToCartUrl"){
    			$rootScope.disableCartClick = false;
    		}
        	$rootScope.closeLoader();
			//$log.error("Util.postDataToServer(), url: " +finalUrl +" - could not fetch data from server " +  JSON.stringify(status));
			//data = removeDefaultCallBack(data);
			deferred.resolve(data);	
        });
 		return deferred.promise;
	};
	
	utilObject.postDataToServerAdvancedSearch = function(urlKey,Obj,showLoader){
		this.validateSession();
		//var methodType = "JSONP";
		var finalUrl = this.getURLForPost(urlKey,true);
		//var finalUrl = "http://10.29.21.82:8082/NS/WS/NSWS/RecalculateCart?";
		var sess = DataSharingService.getFromSessionStorage(SESSION_ID);
		finalUrl =  finalUrl+";jsessionid="+sess;
		//$log.info(finalUrl + " -- going to fetch data from server");
		$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
		var deferred = $q.defer();
		if(isNull(showLoader)){
			showLoader = true;
		}
		if(showLoader){ $rootScope.openLoader(); }
	//	$rootScope.openInnerLoader();
        $http.post(finalUrl, Obj).success(function(data) {
			//$log.log("Util.postDataToServer(), url: " +finalUrl +" " + JSON.stringify(data));
			//data = removeDefaultCallBack(data);
			$rootScope.handleSessionNotFound(data);
			deferred.resolve(data);	
			$rootScope.closeLoader();
        }).error(function(data,status) {
        	$rootScope.closeLoader();
			//$log.error("Util.postDataToServer(), url: " +finalUrl +" - could not fetch data from server " +  JSON.stringify(status));
			//data = removeDefaultCallBack(data);
			deferred.resolve(data);	
        });
 		return deferred.promise;
	};
	
	utilObject.doLogout = function(urlKey,params){
		var methodType = this.getHttpMethodName();
		var finalUrl = this.getURL(urlKey,params);
		//$log.info(finalUrl + " -- going to fetch data from server");
		var deferred = $q.defer();
		$http({method: methodType, url: finalUrl, timeout:'600000'}).success(function(data){
			deferred.resolve(data);	
		}).error(function(data, status) {
			deferred.resolve(null);
			//$log.error("Util.doLogout(), url: " +finalUrl +" - could not fetch data from server " +  JSON.stringify(status) + " " + JSON.stringify(data));
		});
		return deferred.promise;
	};

	utilObject.validateSession = function(){
		if(SessionTracking.isSessionCheckRequired()){
			//getWebUserUrl

			var methodType = this.getHttpMethodName();
			var finalUrl = this.getURL('getWebUserUrl',SessionTracking.getRequestParams());
			//$log.info(finalUrl + " -- going to fetch data from server");
			//var deferred = $q.defer();
			$http({method: methodType, url: finalUrl}).success(function(data){
				//deferred.resolve(data);
				if(data!=null){
					//console.log("Web User : " + JSON.stringify(data));
					if(angular.isDefined(data.isAuthenticated) && !data.isAuthenticated){
						//$log.warn("Session time out");
						//alert(msg);
						SessionTracking.close();
						//$rootScope.loginAgain(msg);
						DataSharingService.setToSessionStorage("SHOW_SESSION_EXPIRE_MSG",true);
						$rootScope.handleSessionExpire();
						$rootScope.reloadHomePage();
					}else{
						//$log.info("Session is still live");
					}
				}
			}).error(function(data, status) {
				//$log.error("Util.validateSession(), url: " +finalUrl +" - could not fetch data from server " + status);
			});
			//return deferred.promise;
			//SessionTracking.updateRequestTime();
		}else{
			SessionTracking.updateRequestTime();
		}
	};

	utilObject.getPageData = function (urlKey, params){
		this.validateSession();
		var  pageKey  = this.getPageContextKey(urlKey, params);
		paginationObj = DataSharingService.getObject(pageKey);
		var  deferred = $q.defer();
		if(paginationObj!=null ){
			//$log.info("Util.getPageData(), PageObject status: " + paginationObj.getString());
			if(!paginationObj.wait && !paginationObj.stopLoading){
				paginationObj.wait=true;
				//page number increment 
				this.appendItemsToList(["PageNo",paginationObj.incrementPageNo()], params);
				var methodType = this.getHttpMethodName();
				var finalUrl = this.getURL(urlKey,params);
				//$log.info(finalUrl + " -- going to fetch data from server");
				 //deferred = $q.defer();
				$http({method: methodType, url: finalUrl}).success(function(data){
					deferred.resolve(data);	
					paginationObj.wait=false;
					$rootScope.handleSessionNotFound(data);
				}).error(function(data, status) {
					//$log.error("Util.getPageData(), url: " + finalUrl +" - could not fetch data from server " + status);
					paginationObj.wait=false;
					deferred.resolve(data);	
				});
			}
		}else{
			//$log.warn("Util.getPageData(), pagination object not initailized ");
			deferred.resolve(null);	
		}	
		return deferred.promise;
	};

	utilObject.initPagination  = function (urlKey, params){
		var  pageKey  = this.getPageContextKey(urlKey, params);
		DataSharingService.removeObject(pageKey);
		var paginationObj =  new Pagination(pageKey,1,false,false);
		DataSharingService.setObject(pageKey,paginationObj);
		return pageKey;
	};


	utilObject.stopLoadingPageData = function (contextKey){
		paginationObj = DataSharingService.getObject(contextKey);
		if(paginationObj!=null ){
			paginationObj.stopPagination();
		}
	};

	utilObject.isPaginationBlocked = function (contextKey){
		paginationObj = DataSharingService.getObject(contextKey);
		if(paginationObj!=null ){
			return (paginationObj.wait || paginationObj.stopLoading); 
		}else{
			return false;
		}
	};

	utilObject.getPageContextKey = function(urlKey,params){
		var contextKey = 'PaginationObjectKey_'+urlKey;
		/*if(params!=null){
			for(var i = 0; i<params.length ; i++){
				if(params[i] != null && params[i].toUpperCase() == "PAGENO"){
					//do not add page no to key
					i++;
				}else{
					contextKey = contextKey+ "_"+params[i];
				}
			}
		}
		return contextKey;*/
		if(!isNull(params)) {
			for(var i = 0; i < params.length; i++) {
				var keytype = params[i];
				var valuetype = params[i+1];
				if((keytype.toUpperCase() == "PAGENO")/* || (keytype == "LanguageCode")*/) {//PageNo
					// do not add page no or language code to key
				} else {
					contextKey += "_" + keytype;
					contextKey += "_" + valuetype;
				}
				i++;
			}
		}
		
		
		
		/*var isNumber = false;
		if(params!=null){
			for(var i = 0; i<params.length ; i++){
				isNumber = angular.isNumber(params[i]);
				//if(params[i] != null && params[i].toUpperCase() == "PAGENO"){
				if(params[i] != null && !isNumber)
				{
					if(params[i].toUpperCase() == "PAGENO")
						{
							//do not add page no to key
							i++;
						}
					else if(params[i].toUpperCase() == "LANGUAGECODE")
					{
						//do not add language code to key
						i++;
					}
					else
						{contextKey = contextKey+ "_"+params[i];}
				}
				else{
					contextKey = contextKey+ "_"+params[i];
				}
			}
		}*/
		return contextKey;
	};

	utilObject.appendItemsToList = function(fromList, toList){
		if(this.isNotNull(fromList) && this.isNotNull(toList)){
			for(var i = 0; i<fromList.length ; i++){
				toList.push(fromList[i]);
			};
		}
	};

	utilObject.getHttpMethodName = function(){
		var methodType;
		if(this.isLocalService()){
			methodType = 'GET';
		}else{
			methodType = 'JSONP';
		}
		return methodType;
	};

	utilObject.isLocalService = function(){
		var config = DataSharingService.getConfiguration();
		if(config!=null){
			var useLocalService = config['useLocalService'];
			if(useLocalService!=null && 'TRUE' === useLocalService.toUpperCase()){
				return true;
			}else {
				return false;
			}
		}
	};

	utilObject.getParamStr= function (params,encodeUrl){ 
		var paramStr = '';
		if(params!=null && params.length>0){
			for (var i=0;i<params.length;i=i+2 ){
				if(i==0){
					if(encodeUrl){
						paramStr = "?"+params[i]+"="+encodeURIComponent(params[i+1]);
					}else{
						paramStr = "?"+params[i]+"="+params[i+1];
					}
					//paramStr = "?"+params[i]+"="+params[i+1];
				}else
				{
					if(encodeUrl){
						paramStr = paramStr + "&"+params[i]+"="+encodeURIComponent(params[i+1]);
					}else{
						paramStr = paramStr + "&"+params[i]+"="+params[i+1];
					}
					//paramStr = paramStr + "&"+params[i]+"="+params[i+1];
				}
			}
		}
		return paramStr;
	};

	utilObject.isNotNull  = function(obj){
		if(angular.isDefined(obj) && obj!=null){
			return true;
		}else{
			return false; 
		}
	};
	
	utilObject.getURL = function(urlKey,params,encodeUrl){
		var config = DataSharingService.getConfiguration();
		var callBackStr = 'CallBack=JSON_CALLBACK';
		if(urlKey ==='invoiceDetailsUrl') {callBackStr = 'CallBack=JSON_CALLBACK';} // to be deleted
		//var finalurl = config[BASE_URL]+config[PORT]+config[CONTEXT]+config[urlKey];
		var finalurl = '';
		var paramStr = this.getParamStr(params,encodeUrl);
		if (!this.isLocalService()){
			finalurl = config[BASE_URL]+config[PORT]+config[CONTEXT]+config[urlKey];
			if (paramStr ==''){
				finalurl = finalurl+'?'+callBackStr;
			}else{
				finalurl = finalurl+paramStr+'&'+callBackStr;
			}
		}else{
			urlKey =urlKey + "_local";
			finalurl = config[urlKey];
		}
		//$log.info("NewConfigService.getURL() -- urlKey = "+urlKey+" "+paramStr + ", Final URL = " + finalurl);

		return finalurl;
	};
	
	utilObject.getURLForPost = function(urlKey,hasParams){
		var config = DataSharingService.getConfiguration();
		var finalurl = '';
		if (!this.isLocalService()){
			finalurl = config[BASE_URL]+config[PORT]+config[CONTEXT]+config[urlKey];
			if (hasParams){
				finalurl = finalurl;//+'?';
			}
		}else{
			urlKey =urlKey + "_local";
			finalurl = config[urlKey];
		}
		//$log.info("NewConfigService.getURL() -- urlKey = "+urlKey+", Final URL = " + finalurl);
		return finalurl;
	};

	utilObject.getObjPropList = function(responsePropertiesList){
		var names = null;
		if(angular.isDefined(responsePropertiesList) && responsePropertiesList!=null){
			names = Object.keys(responsePropertiesList );
		}else{
			//console.log("Util.getObjPropList() : Invalid Json object: ");
		}
		return names;
	};

	utilObject.isPropertyExist = function(objPropName, objPropertyList){
		var isExist = false;
		if(angular.isDefined(objPropertyList) && objPropertyList!=null && angular.isDefined(objPropName) && objPropName!=null){
			for(var i=0; i< objPropertyList.length;i++){
				if(objPropertyList[i] === objPropName){
					isExist = true;
					break;
				}
			}
		}
		//if(!isExist){
		//$log.log("Util.isPropertyExist() : Property does not exist: "+ objPropName);
		//}
		return isExist;
	};

	// pagination at click
	utilObject.getPageinationData = function (urlKey, params,page,showLoader){
		this.validateSession();
		var  pageKey  = this.getPageContextKey(urlKey, params);
		paginationObj = DataSharingService.getObject(pageKey);
		var deferred = $q.defer();
		if(isNull(showLoader)){
			showLoader = true;
		}
		if(showLoader){ $rootScope.openLoader(); }
		$rootScope.openInnerLoader();
		if(paginationObj!=null ){
			//$log.info("Util.getPageData(), PageObject status: " + paginationObj.getString());
			if(!paginationObj.wait && !paginationObj.stopLoading){
				paginationObj.wait=true;
				//page number increment				
				if(page == FirstPage)
				{
					this.appendItemsToList(["PageNo",paginationObj.setFirstPageNo()], params);
				}
				if(page == NextPage)
				{
					this.appendItemsToList(["PageNo",paginationObj.incrementPageNo()], params);
				}
				if(page == PrevPage)
				{
					this.appendItemsToList(["PageNo",paginationObj.decrementPageNo()], params);
				}
				/*
				else
				{
					if(nextPageFlag)
					{
						this.appendItemsToList(["PageNo",paginationObj.incrementPageNo()], params);
					}
					else
					{
						this.appendItemsToList(["PageNo",paginationObj.decrementPageNo()], params);
					}
				}
				*/

				var methodType = this.getHttpMethodName();
				var finalUrl = this.getURL(urlKey,params);
				//$log.info(finalUrl + " -- going to fetch data from server");
			//	var deferred = $q.defer();
				$http({method: methodType, url: finalUrl, timeout:'600000'}).success(function(data){
					deferred.resolve(data);
					$rootScope.closeLoader();
					$rootScope.handleSessionNotFound(data);
					$rootScope.closeInnerLoader();
					paginationObj.wait=false;
					//SIMONE
					if(utilObject.saveOnLocal()){
						if(urlKey != "getUserAdminMessageUrl"){
							var blob = new Blob([JSON.stringify(data)], {type: "text/plain;charset=utf-8"});
							var saveAs = window.saveAs;
							saveAs(blob, urlKey + ".json");				
						}
					}
				}).error(function(data, status) {
					//data = removeDefaultCallBack(data);
					$rootScope.closeLoader();
					$rootScope.closeInnerLoader();
					deferred.resolve(data);
					//$log.error("Util.getPageData(), url: " + finalUrl +" - could not fetch data from server " + status);
					paginationObj.wait=false;
				});
			}
		}else{
			//$log.warn("Util.getPageData(), pagination object not initailized ");
		}	
		return deferred.promise;
	};
	
	utilObject.getFilterConfig =  function(filter){
		filterConfig = DataSharingService.getObject('filterConfig');
		if(filterConfig!=null ){
			
			return   filterConfig[filter];
		}
		
	};
	//
	utilObject.translate =  function(key,params){
	//	$log.log("translate filter, parameters: "+  JSON.stringify(params));
		var languageResource = DataSharingService.getLanguagePropJsonObject(); 
		var value = "" ;
		if(angular.isDefined(languageResource ) && languageResource != null){
			value = languageResource[key+""];
			if(!angular.isDefined(value) || value==null){
				////$log.error("translate: key = "+key +", not found in message file");
				value ="";
			}
		}else{
			//$log.error("translate:  Language resource not set");
		}

		//apply parameters 
		if(angular.isDefined(params) && params!= null && params.length>0 && angular.isDefined(value) && value!=null){
			var replaceStr = "";
			var replaceWithStr = "";
			for(var i=0; i<params.length; i++){
				replaceStr = '&'+(i+1);
				replaceWithStr = params[i]+"";
				//Perform a global, case-insensitive replacement:
				var valueStr = value.replace(replaceStr,replaceWithStr);
				value = valueStr;
			}
		}
		return  value;
	};
	// GetFilterDetails
	utilObject.getFilterListDetails = function(params){
		//$log.info("utilObject.getFilterListDetails() -- going to fetch data from server");
		var deferred = $q.defer();
		utilObject.getDataFromServer('filterDetailsUrl',params).then(function(data){
			utilObject.decorateCurrencyList(data);
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	utilObject.decorateCurrencyList = function(data){
		if (!isNull(data) && !isNull(data['currencyList'])){
			var cList = data['currencyList'];
			for(var i=0; i<cList.length; i++){
				var curr = cList[i];
				curr.description = curr.code + " - " + curr.description;
			}
		}
		return;
	};
	
	// Get Product Search / main catlog serach Filter settings
	
	utilObject.getProductFilterDetails = function(params){
		//$log.info("utilObject.getProductFilterDetails() -- going to fetch data from server");
		var deferred = $q.defer();
		params = params.concat(SessionTracking.getLanguageParams());
		utilObject.getDataFromServer('itemSearchFiltersUrl',params).then(function(data){
			deferred.resolve(data);	
		});
//		var filePath = "./json/catalog/itemSearch_Filter.json";
//		$http.get(filePath).success(function(data){
//			deferred.resolve(data);	
//		});
		return deferred.promise;
	};
	
	// to get the valid product code details  
	utilObject.getItemCodeDetails = function(params){
		//$log.info("utilObject.getRequestDetails() -- going to fetch data from server");

		var deferred = $q.defer();
		utilObject.getDataFromServer('getItemDetailUrl',params,false).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};

	// to validate the all products    
	utilObject.validateItemsDetails = function(params){
		//$log.info("utilObject.validateItemsDetails() -- going to fetch data from server");

		var deferred = $q.defer();
		utilObject.postDataToServer('validateItemsUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};

	
	return utilObject;
}]);	

//invoice service
AppServicesModule.factory('InvoiceService',['Util','DataSharingService','$http','$q','$log',
                                            function(Util,DataSharingService,$http,$q,$log){
	var serviceObject = { };

	serviceObject.getInvoiceHistory = function(params,showLoading){
		//$log.info("InvoiceService.getInvoiceHistory() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('invoiceHistoryUrl',params,showLoading).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	serviceObject.getInvoiceFilterList = function(params){
		//$log.info("InvoiceService.getInvoiceFilterList() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('invoiceSearchFilterUrl',params,false).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	serviceObject.getInvoiceDetailList = function(params,showLoading){
		//$log.info("InvoiceService.getInvoiceDetailList() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('invoiceDetailsUrl',params,showLoading).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	//utilObject.getFilterConfig
	serviceObject.getFilterConfiguration = function(){
		
	 return Util.getFilterConfig('invoicefilterList'); 
		
	};
	return serviceObject;
}]);


AppServicesModule.factory('SessionTracking',['DataSharingService','$log',
                                             function(DataSharingService,$log){

	var SessionTracker = {
			"userID":"",
			"maxInactiveInterval": 30*1000,
			"lastRequestTime" : ""
	};
	var blockSessionCheck  = false;
	var sesTrkServiceObject = {};
	var copyOfSessionTrackerObject = null;
	var sessionId = null;
	var loginHistory = new LoginHistory();
	sesTrkServiceObject.isSessionCheckRequired = function(){
		var flag = false;
		var sesTrc = null;
		var diff = 0 ;
		if(!blockSessionCheck){
			blockSessionCheck = true;
			if(this.isLoggedIn()){
				sesTrc = this.getSessionTracker();
				if(sesTrc!=null && angular.isDefined(sesTrc)){
					var currentTime = (new Date()).getTime();
					diff = currentTime - sesTrc.lastRequestTime;
					diff = Math.round((diff) / (1000));
					if(diff >= sesTrc.maxInactiveInterval || this.isLoggedOutFromOtherTab()){
						flag =  true;
					}
					//$log.log("SessionTracking - isSessionCheckRequired: " + diff + " Flag: " +flag.toString() );
				}
			}
			blockSessionCheck = false;
		}
		//console.log("SessionTracking - isSessionCheckRequired(), time diff: " + diff + " Flag: " +flag.toString() );
		return flag;
	};

	sesTrkServiceObject.getUserSession = function(){
		var usrSessionObject = DataSharingService.getFromSessionStorage(userResponseObject_Key);
		if(usrSessionObject==null || !angular.isDefined(usrSessionObject)){
			return null;
		}else{
			return usrSessionObject;
		}
	};

	sesTrkServiceObject.isLoggedIn = function (usrSessionObject){
		var isLoggedIn = false;
		if(usrSessionObject==null || !angular.isDefined(usrSessionObject)){
			usrSessionObject = this.getUserSession();
		}
		if(usrSessionObject!=null){
			isLoggedIn = this.isAuthenticated(usrSessionObject);
		}
		return isLoggedIn;
	};

	sesTrkServiceObject.isAuthenticated = function(data){
		var isAuthenticatedBool = false;
		if(angular.isDefined(data) && data!=null){
			var isAuthenticated =  data[IS_AUTHENTICATED];			 
			if((isAuthenticated!=null) && angular.isDefined(isAuthenticated) && (isAuthenticated == true))
			{
				isAuthenticatedBool = true;
			}
		}
		return isAuthenticatedBool;
	};

	sesTrkServiceObject.getLoginLabel = function(translateFunc){
		var loginStatus = this.isLoggedIn();
		var loginLabel = null;
		if(loginStatus){	
			loginLabel = translateFunc('CON_SIGN_OUT');
			//return "Logout";
		}else{
			loginLabel = translateFunc('CON_SIGN_IN');
			//return "Login";
		}
		return loginLabel;
	};

	sesTrkServiceObject.getSessionTracker = function(){
		if(copyOfSessionTrackerObject==null){
			var sessionTrackerObject = DataSharingService.getFromSessionStorage(sessionTrackerObject_Key);
			if(sessionTrackerObject==null || !angular.isDefined(sessionTrackerObject)){
				return null;
			}else{
				copyOfSessionTrackerObject =  sessionTrackerObject;
			}
		}
		return copyOfSessionTrackerObject;
	};

	sesTrkServiceObject.updateRequestTime = function (){
		var sessionTrackerObject = this.getSessionTracker();
		if(sessionTrackerObject!=null){
			lastTime = new Date();
			sessionTrackerObject.lastRequestTime = lastTime.getTime();
			//console.log("Session - last WS request time: " + lastTime);
			DataSharingService.setToSessionStorage(sessionTrackerObject_Key,sessionTrackerObject);
		}
	};

	sesTrkServiceObject.getUserId = function(usrSessionObject){
		var user_Id = null;
		if(usrSessionObject==null || !angular.isDefined(usrSessionObject)){
			usrSessionObject = this.getUserSession();
		}
		if(usrSessionObject!=null){
			user_Id = usrSessionObject.userID;
		}
		return user_Id;
	};

	sesTrkServiceObject.init =  function(){
		var usrSessionObject = this.getUserSession();
		if (usrSessionObject!=null){
			if ( this.isLoggedIn(usrSessionObject)){
				userId = this.getUserId(usrSessionObject);
				maxInactiveInterval = this.getmaxInactiveInterval();
				//var sessionTracker =  new SessionTracker();
				SessionTracker.userID = userId;
				SessionTracker.lastRequestTime = (new Date()).getTime();
				if(maxInactiveInterval!=null){
					SessionTracker.maxInactiveInterval = maxInactiveInterval;
					//SessionTracker.maxInactiveInterval = 60*1000;
				}
				loginHistory.addUser(this.getSessionId(), userId);
				DataSharingService.setToSessionStorage(sessionTrackerObject_Key,SessionTracker);
				DataSharingService.removeFromSessionStorage(languageCode_Key);
				//console.log("SessionTracking.init()-  called");
			}
		}
	};

	sesTrkServiceObject.getRequestParams =  function(){
		var usrId = this.getUserId();
		params = null;
		if(usrId!=null){
			params = ["UserID",usrId];
		}
		return params;
	};

	sesTrkServiceObject.getmaxInactiveInterval =  function(){
		var maxInactiveInterval = null;
		var webSettings = DataSharingService.getWebSettings();
		if(angular.isDefined(webSettings.maxInactiveInterval) && webSettings.maxInactiveInterval !=null){
			if(isNaN(webSettings.maxInactiveInterval)){
				//$log.error("webSettings.maxInactiveInterval is not a number");
			}else{
				maxInactiveInterval = webSettings.maxInactiveInterval;
			}
		} 
		return maxInactiveInterval;
	};

	sesTrkServiceObject.getSessionId = function(){
		if(isNull(sessionId)){
			var webSettings = DataSharingService.getWebSettings();
			if(!isNull(webSettings)){
				sessionId = webSettings.sessionId;
			}
		} 
		return sessionId;
	};
	
	sesTrkServiceObject.isLoggedOutFromOtherTab = function(){
		var obj = loginHistory.getObjectBySessionId(this.getSessionId());
		if(isNull(obj)){
			return true;
		}else{
			return false;
		}
	};
	
	sesTrkServiceObject.close =  function() {
		loginHistory.logoutUser(this.getSessionId());
		DataSharingService.removeFromSessionStorage(userResponseObject_Key);
		DataSharingService.removeFromSessionStorage(sessionTrackerObject_Key);
		copyOfSessionTrackerObject = null;
		//console.log("SessionTracking.close()-  called");
	};
	

	sesTrkServiceObject.getLanguageParams = function() {
		var params = [];
		if(DataSharingService.getLanguageCode() != null){
			params.push("LanguageCode");
			params.push(DataSharingService.getLanguageCode());
		}else{
			var data = sessionStorage.getItem('webSettingsObject');
			var s = JSON.parse(data);
			params.push("LanguageCode");
			if(!isNull(s)){
			params.push(s.languageCode);
			}else{
			params.push("");
			}
		}	
		return params;
	};

	return sesTrkServiceObject;

}]);

//function Menu service
AppServicesModule.factory('MenuService',['Util','DataSharingService','$http','$q','$log',
                                         function(Util,DataSharingService,$http,$q,$log){
	var serviceObject = { };

	serviceObject.getMenuList = function(url,params){
		//$log.info("MenuService.getMenuList :"+ url + " -: -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer(url,params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	return serviceObject;

}]);

//Quotation Service


AppServicesModule.factory('QuotationService',['Util','DataSharingService','$http','$q','$log',
                                              function(Util,DataSharingService,$http,$q,$log){
	var serviceObject = { };

	serviceObject.getQuotationHistory = function(params){
		//$log.info("QuotationService.getQuotationHistory() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('quotationHistoryUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	serviceObject.getQuotationDetailList = function(params){
		//$log.info("QuotationService.getQuotationDetailList() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('quotationDetailsUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.getQuotationFilterList = function(params){
		//$log.info("QuotationService.getQuotationFilterList() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('quotationSearchFilterUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};

	//utilObject.getFilterConfig
	serviceObject.getFilterConfiguration = function(){
		
		return Util.getFilterConfig('quotationfilterList'); 
		
	};
	serviceObject.convertQuotationToOrder = function(params){
		//$log.info("QuotationService.convertQuotationToOrder() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('getQuotationToOrderUrl',params).then(function(data){
			//data = JSON.parse('{"amount":"","orderId":"","customerNumber":"","ourRef":"","orderNumber":"25096","transport":"","delivery":"","deliveryAddress":"","invoiceAddress":"","yourReference":"","favoriteCount":0,"isViewOrder":false,"isOrderInError":true}');
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	return serviceObject;
}]);
	
//ADM-003 Transport Note Service
AppServicesModule.factory('TransportNoteService',['Util','DataSharingService','$http','$q','$log',
                                              function(Util,DataSharingService,$http,$q,$log){
	var serviceObject = { };

	serviceObject.getTransportNoteHistory = function(params){
		//$log.info("TransportNoteService.getTransportNoteHistory() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('transportNoteHistoryUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	serviceObject.getTransportNoteDetailList = function(params){
		//$log.info("TransportNoteService.getTransportNoteDetailList() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('transportNoteDetailsUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.getTransportNoteFilterList = function(params){
		//$log.info("TransportNoteService.getTransportNoteFilterList() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('transportNoteSearchFilterUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};

	//utilObject.getFilterConfig
	serviceObject.getFilterConfiguration = function(){
		
		return Util.getFilterConfig('transportNotefilterList'); 
		
	};
	serviceObject.convertTransportNoteToOrder = function(params){
		//$log.info("TransportNoteService.convertTransportNoteToOrder() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('getTransportNoteToOrderUrl',params).then(function(data){
			//data = JSON.parse('{"amount":"","orderId":"","customerNumber":"","ourRef":"","orderNumber":"25096","transport":"","delivery":"","deliveryAddress":"","invoiceAddress":"","yourReference":"","favoriteCount":0,"isViewOrder":false,"isOrderInError":true}');
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	return serviceObject;
}]);

AppServicesModule.factory('ShoppingCartService',['Util','DataSharingService','$rootScope','$http','$q','$log',
                                                 function(Util,DataSharingService,$rootScope,$http,$q,$log){
	var serviceObject = { };

	serviceObject.getTemporaryOrderData = function(params){
		//$log.info("ShoppingCartService.getShoppingCart() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('temporaryOrderDataUrl',params,false).then(function(data){
			$rootScope.populateBubbleCart(data);
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	serviceObject.addToCart = function(params){
		//$log.info("ShoppingCartService.addToCart() -- going to fetch data from server");
		var deferred = $q.defer();
		$rootScope.disableCartClick = true;
		
		Util.getDataFromServer('addToCartUrl',params,false).then(function(data){
			//var data = responseData.temporaryOrderData;
			$rootScope.disableCartClick = false;
			if(angular.isDefined(data.currentOrder) && angular.isDefined(data.currentOrder.lineCount) )
			{
				//$rootScope.currentOrder_lineCount = data.currentOrder.lineCount;
				$rootScope.populateBubbleCart(data,true);
				//console.log("ShoppingCartService.addToCart() " + JSON.stringify(data));
			}
			deferred.resolve(data);	
		});
		return deferred.promise;
	};

	serviceObject.getAllCountryData = function(params){
		//$log.info("ShoppingCartService.getAllCountryData() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('getAllCountryUrl',params).then(function(data){	
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.getAllMannerOfTransport = function(params){
		//$log.info("ShoppingCartService.getAllMannerOfTransport() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('getAllMannerOfTransportUrl',params).then(function(data){	
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	//ADM-002 Missing Items Service
	serviceObject.validateCart = function(params){
		//$log.info("MissingItemService.validateCart() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('validateCartUrl',params,true).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	//updateLineCartUrl
	serviceObject.updateLineCart = function(params){
		//$log.info("ShoppingCartService.updateLineCart() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('updateLineCartUrl',params,false).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	//
	
	serviceObject.deleteLineCart = function(params){
		//$log.info("ShoppingCartService.deleteLineCart() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('deleteLineCartUrl',params,false).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};

	serviceObject.applyPromotionCode = function(params){
		//$log.info("ShoppingCartService.applyPromotionCode() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('applyPromotionUrl',params,false).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.deleteCurrentCart = function(params){
		//$log.info("ShoppingCartService.deleteCurrentCart() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('deleteCurrentCartUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.createNewCart = function(params){
		//$log.info("ShoppingCartService.createNewCart() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('createNewCartUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.closeCart = function(params){
		//$log.info("ShoppingCartService.closeCart() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('closeCartUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};

	
	serviceObject.editCart = function(params){
		//$log.info("ShoppingCartService.editCart() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('editCartUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	serviceObject.getInterruptedCart= function(params){
		//$log.info("ShoppingCartService.getInterruptedCart() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('getInterruptedCartUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	//deleteInterruptedCartUrl
	serviceObject.deleteInterruptedCarts= function(params){
		//$log.info("ShoppingCartService.deleteInterruptedCarts() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.postDataToServer('deleteInterruptedCartUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	serviceObject.getDeliveryInfo= function(params){
		//$log.info("ShoppingCartService.getDeliveryInfo() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('getDeliveryInfoUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	serviceObject.getOrderTextReference= function(params){
		//$log.info("ShoppingCartService.getDeliveryInfo() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('getOrderTextReferenceUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	serviceObject.getStateList= function(params){
		//$log.info("ShoppingCartService.getDeliveryInfo() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('getStateListForCountryUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.getCountyList= function(params){
		//$log.info("ShoppingCartService.getDeliveryInfo() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('getCountyListForStateUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	serviceObject.saveDeliveryInfo= function(params){
		//$log.info("ShoppingCartService.saveDeliveryInfo() -- going to post data to server");
		var deferred = $q.defer();
		Util.postDataToServer('saveDeliveryInfoUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	return serviceObject;
	
	
}]);
// account transaction service
AppServicesModule.factory('TransactionService',['Util','DataSharingService','$http','$q','$log',
                                              function(Util,DataSharingService,$http,$q,$log){
	var serviceObject = { };
	
	serviceObject.getTransactionDetails = function(params){
		//$log.info("TransactionService.getTransactionDetails() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('transactionDetailsUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.getTransactionFilterList = function(params){
		//$log.info("TransactionService.getTransactionFilterList() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('transactionSearchFilterUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	// 
	serviceObject.getFilterConfiguration = function(){
		
		return Util.getFilterConfig('transactionfilterList'); 
		
	};

	return serviceObject;
}]);
//Order service

AppServicesModule.factory('OrderService',['Util','DataSharingService','$http','$q','$log',
                                              function(Util,DataSharingService,$http,$q,$log){
	var serviceObject = { };
	
	serviceObject.getOrderHistory = function(params,showLoading){
		//$log.info("OrderService.getOrderHistory() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('orderHistoryUrl',params,showLoading).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.getOrderDetailList = function(params,showLoading){
		//$log.info("OrderService.getOrderDetailList() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('orderDetailsUrl',params,showLoading).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};

	serviceObject.getOrderFilterList = function(params){
		//$log.info("OrderService.getOrderFilterList() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('orderSearchFilterUrl',params,false).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	// 
	//utilObject.getFilterConfig
	serviceObject.getFilterConfiguration = function(){
		
		return Util.getFilterConfig('orderfilterList'); 
		
	};
	
	serviceObject.deleteOrder = function(params){
		var deferred = $q.defer();
		Util.getDataFromServer('deleteOrderUrl',params,false).then(function(data){
			deferred.resolve(data);	
		});
//		var filePath = "./json/catalog/deleteOrder.json";
//		$http.get(filePath).success(function(data){
//			deferred.resolve(data);	
//		});
		return deferred.promise;
	};
	
	serviceObject.deleteOrderLines = function(params){
		var deferred = $q.defer();
		Util.postDataToServer('deleteOrderLinesUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	return serviceObject;
}]);
//Product search service
AppServicesModule.factory('ProdSearchService',['Util','DataSharingService','$http','$q','$log','SessionTracking','$rootScope',
                                               function(Util,DataSharingService,$http,$q,$log,SessionTracking,$rootScope){
	var prodSearchServiceObject = { };
	
	//GetSolrSearchResults?SEARCH_TYPE=FREE&FREE_TEXT=screen&PageNo=1
	prodSearchServiceObject.getPrdSearchResult = function(params){
		//$log.info("ProdSearchService.freeTextSearch() -- going to fetch data from server");
		var deferred = $q.defer();
		$rootScope.openLoader();
		Util.getDataFromServer('solrSearchResultUrl',params).then(function(data){
			deferred.resolve(data);	
			$rootScope.closeLoader();
		});
		return deferred.promise;
	};
	
	prodSearchServiceObject.getPrdSearchResultNew = function(paramsStr){
		//$log.info("ProdSearchService.freeTextSearch() -- going to fetch data from server");
		var deferred = $q.defer();
		$rootScope.openLoader();
		Util.postDataToServerAdvancedSearch('getProductSearchNewUrl',paramsStr).then(function(data) {			
			deferred.resolve(data);	
			$rootScope.closeLoader();
		});
		return deferred.promise;
	};
	
	prodSearchServiceObject.getCatNewSearchResult = function(paramsStr){
		var deferred = $q.defer();
		Util.postDataToServerAdvancedSearch('catalogProductSearchUrl',paramsStr).then(function(data) {			
			deferred.resolve(data);	
		});
		return deferred.promise;
	};

	prodSearchServiceObject.getStaticSolrFilterConfig = function(){
		return Util.getFilterConfig('solrSearchFilterList'); 
	};
	
	prodSearchServiceObject.getDynamicSolrFilterConfig = function(){
		var deferred = $q.defer();
		Util.getDataFromServer('solrSearchFiltersUrl',SessionTracking.getLanguageParams(), true).then(function(data){
			deferred.resolve(data);	
		});
//		var filePath = "./json/catalog/solr_search_filters.json";
//		$http.get(filePath).success(function(data){
//			deferred.resolve(data);	
//		});

		return deferred.promise;
	};
	
	prodSearchServiceObject.getDynamicSolrFilterConfigLang = function(params){
		var deferred = $q.defer();
		Util.getDataFromServer('solrSearchFiltersUrl',params).then(function(data){
			deferred.resolve(data);	
		});
//		var filePath = "./json/catalog/solr_search_filters.json";
//		$http.get(filePath).success(function(data){
//			deferred.resolve(data);	
//		});

		return deferred.promise;
	};
	
	prodSearchServiceObject.getFilterDataList = function(){
		var deferred = $q.defer();
		Util.getDataFromServer('solrSearchFiltersUrl',SessionTracking.getLanguageParams(), true).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	return prodSearchServiceObject;
}]);

//product details service
AppServicesModule.factory('ProductDetailService',['Util','DataSharingService','$http','$q','$log',
                                              function(Util,DataSharingService,$http,$q,$log){
	var serviceObject = { };
	
	serviceObject.getProductDetails = function(params){
		//$log.info("ProductDetailService.getProductDetails() -- going to fetch data from server");
		var deferred = $q.defer();
		//product id is already encoded send encodeUrl as false
		Util.getDataFromServer('productDetailsUrl',params,null,false).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	

	return serviceObject;
}]);

// Request Search Service

AppServicesModule.factory('RequestSearchService',['Util','DataSharingService','$http','$q','$log',
                                              function(Util,DataSharingService,$http,$q,$log){
	var serviceObject = { };

	serviceObject.getRequestHistory = function(params){
		//$log.info("RequestSearchService.getRequestHistory() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('requestHistoryUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.getRequestDetails = function(params){
		//$log.info("RequestSearchService.getRequestDetails() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('requestDetailsUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	
	serviceObject.getRequestFilterList = function(params){
		//$log.info("RequestSearchService.getRequestFilterList() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('requestSearchFilterUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.getRequestTypeList = function(params){
		//$log.info("RequestSearchService.getRequestTypeList() -- going to fetch data from server");
		var filterSettings =  DataSharingService.getFilterDetailsSettings();
		var requestTypeList = [];
		if(filterSettings != null)
			requestTypeList =  filterSettings.requestTypeList;
		return requestTypeList;
	};
	
	//ADM-XXX
	serviceObject.getReasonCodeList = function(params){
		var filterSettings =  DataSharingService.getFilterDetailsSettings();
		var reasonCodeList = [];
		if(filterSettings != null)
			reasonCodeList =  filterSettings.reasonCodeList;
		return reasonCodeList;
	};

	//utilObject.getFilterConfig
	serviceObject.getFilterConfiguration = function(){
		
		return Util.getFilterConfig('requestfilterList'); 
		
	};
	
	// for new request
	serviceObject.submitNewRequest = function(params){
		var deferred = $q.defer();
		Util.postDataToServer('submitRequestUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	// for validating new request
	serviceObject.validateNewRequest = function(params){
		var deferred = $q.defer();
		Util.postDataToServer('validateRequestUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.deleteBPRequest = function(params){
		var deferred = $q.defer();
		Util.getDataFromServer('deleteBPRequestCartUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};

	
	// ADM-XXX
	// Validate a request line
	serviceObject.validateRequestLine = function(params){
		var deferred = $q.defer();
		Util.postDataToServer('validateRequestLineUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	return serviceObject;
}]);

//price calculation service
AppServicesModule.factory('priceCalService',['Util','DataSharingService','$http','$q','$log',
                                              function(Util,DataSharingService,$http,$q,$log){
	var serviceObject = { };

	serviceObject.getPriceCalculations = function(params){
		//$log.info("priceCalService.getPriceCalculations() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.postDataToServer('priceCalculationUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
		return serviceObject;
}]);


//Top Item service
AppServicesModule.factory('TopItemService',['Util','DataSharingService','$http','$q','$log',
                                              function(Util,DataSharingService,$http,$q,$log){
	var serviceObject = { };

	serviceObject.getTopItemList= function(params){
		//$log.info("TopItemService.getTopItemList() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('myTopItemsUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.getTopItemsFilterList = function(params){
		//$log.info("TopItemService.getTopItemsFilterList() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('myTopItemsSearchFilterUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	//utilObject.getFilterConfig
	serviceObject.getFilterConfiguration = function(){
		
		return Util.getFilterConfig('topItemsfilterList'); 
		
	};

		return serviceObject;
}]);

AppServicesModule.factory('AccountService',['Util','DataSharingService','$rootScope','$http','$q','$log',
                                            function(Util,DataSharingService,$rootScope,$http,$q,$log){
	var accountService = { };

	accountService.getUserDetails= function(params){
		//$log.info("AccountService.getUserDetails() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('getWebUserDetailsUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	accountService.changePassword = function(params){
		//$log.info("changePasswordService.changePassword() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('getChangePasswordUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	accountService.forgotPassword = function(params){
		//$log.info("forgotPassword() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('getForgotPasswordUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	accountService.getStateList = function(countryCode){
		//$log.info("getStateList() -- going to fetch data from server");
		var params = [];
		if(!isNull(countryCode) && !isEmptyString(countryCode)){
			params.push('countryCode');
			params.push(countryCode);
			var deferred = $q.defer();
			Util.getDataFromServer('getStateListUrl',params).then(function(data){
				deferred.resolve(data);	
			});
			return deferred.promise;
		}
	};
	
	
	accountService.getCountyList = function(countryCode,stateCode){
		//$log.info("getCountyList() -- going to fetch data from server");
		var params = [];
		if(!isNull(countryCode) && !isEmptyString(countryCode)){
			params.push('CountryCode');
			params.push(countryCode);
			params.push('StateCode');
			params.push(stateCode);
			
			var deferred = $q.defer();
			Util.getDataFromServer('getCountyUrl',params).then(function(data){
				deferred.resolve(data);	
			});
			return deferred.promise;
		}
	};
	
	accountService.getLocaleList = function(){
		//$log.info("getLocaleList() -- going to fetch data from server");
		var params = [];
			var deferred = $q.defer();
			Util.getDataFromServer('getLocaleListUrl',params).then(function(data){
				deferred.resolve(data);	
			});
			return deferred.promise;
	};
	
	accountService.createNewAccount = function(params){
		//console.log("createNewAccount()");
			var deferred = $q.defer();
			Util.postDataToServer('createNewAccountUrl',params).then(function(data){
				deferred.resolve(data);	
			});
			return deferred.promise;
	};
	
	accountService.getCustomerSearchFilterConfig = function(){
		return Util.getFilterConfig('customerSearchfilters'); 
	};
	
	accountService.getCustomerSearchFilterList = function(){
		var deferred = $q.defer();
		var filterList = DataSharingService.getObject("getCustomerSelectionFiltersData");
		if(isNull(filterList)){
			Util.getDataFromServer('getCustomerSelectionFiltersUrl',null).then(function(data){
				DataSharingService.setObject("getCustomerSelectionFiltersData",data);
				deferred.resolve(data);	
			});
		}else{
			deferred.resolve(filterList);	
		}
		return deferred.promise;
	};
	
	accountService.getCustomerList = function(params){
		var deferred = $q.defer();
		//$http.get("./json/catalog/customer_list.json").success(function(data){
		Util.getDataFromServer('getCustomersListUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};

	accountService.changeCustomer = function(params){
		var deferred = $q.defer();
		$rootScope.openLoader();
		//$http.get("./json/catalog/customer_list.json").success(function(data){
		Util.getDataFromServer('changeCustomerUrl',params).then(function(data){
			deferred.resolve(data);	
			$rootScope.closeLoader();
		});
		return deferred.promise;
	};	
	
	accountService.modifyUserInfo = function(params){
		var deferred = $q.defer();
		Util.getDataFromServer('getEditUserUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};	
	
	return accountService;
}]);

//Shopping List Service 
AppServicesModule.factory('ShoppingListService',['Util','DataSharingService','$rootScope','$http','$q','$log',
                                              function(Util,DataSharingService,$rootScope,$http,$q,$log){
	var serviceObject = { };

	serviceObject.getShoppingList= function(params){
		//$log.info("ShoppingListService.getShoppingList() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('getShoppingListUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	 serviceObject.getShoppingListDetails= function(params){
	   		//$log.info("ShoppingListDetailsService.getShoppingListDetails() -- going to fetch data from server");
	   		var deferred = $q.defer();
	   		Util.getDataFromServer('getShoppingListDetailsUrl',params).then(function(data){
				if(angular.isDefined(data.numOfShopListItemLines)){
					$rootScope.populateShoppingListCount(data,data.listId);
				}else if(data.messageCode==111){
					$rootScope.populateShoppingListCount(0);
				}
	   			deferred.resolve(data);	
	   		});
	   		return deferred.promise;
	 };
	 
	  serviceObject.deleteShoppingList = function(params){
			var deferred = $q.defer();
			Util.getDataFromServer('deleteShoppingListUrl',params,false).then(function(data){
				if(angular.isDefined(data.numOfShopListItemLines)){
					$rootScope.populateShoppingListCount(data,data.defaultShoppingListId);
				}
				deferred.resolve(data);	
			});
			return deferred.promise;
		};
		
		serviceObject.makeShoppingListDefault = function(params){
			var deferred = $q.defer();
			Util.getDataFromServer('getMakeDefaultShoppingListUrl',params,false).then(function(data){
				if(angular.isDefined(data.numOfShopListItemLines)){
					$rootScope.populateShoppingListCount(data,data.listId);
				}
				deferred.resolve(data);	
			});
			return deferred.promise;
		};
		
		serviceObject.createNewShoppingList = function(params){
			var deferred = $q.defer();
			Util.getDataFromServer('createNewShoppingListUrl',params,false).then(function(data){
				deferred.resolve(data);	
			});
			return deferred.promise;
		};
		serviceObject.getShoppingListFilters = function(params){
			var deferred = $q.defer();
			Util.getDataFromServer('getGetShoppingListFilterUrl',params).then(function(data){
				deferred.resolve(data);	
			});
			return deferred.promise;
		};
		
		serviceObject.getFilterConfiguration = function(){			
			return Util.getFilterConfig('shoppingListfilters'); 
		};
		
		serviceObject.deleteShoppingListLines= function(params){
			//$log.info("ShoppingListService.deleteShoppingListLines() -- going to fetch data from server");
			var deferred = $q.defer();
			Util.postDataToServer('deleteShoppingListLinesUrl',params).then(function(data){
				if(angular.isDefined(data.numOfShopListItemLines)){
					$rootScope.populateShoppingListCount(data);
				}
				deferred.resolve(data);	
			});
			return deferred.promise;
		};
		serviceObject.saveShoppingListLines= function(params){
			//$log.info("ShoppingListService.saveShoppingListLines() -- going to fetch data from server");
			var deferred = $q.defer();
			Util.postDataToServer('saveShoppingListLinesUrl',params).then(function(data){
				deferred.resolve(data);	
			});
			return deferred.promise;
		};

		serviceObject.addItemToShoppingList= function(params){
			//$log.info("ShoppingListService.addItemToShoppingList() -- going to fetch data from server");
			var deferred = $q.defer();
			Util.postDataToServer('addItemsToShoppingListUrl',params).then(function(data){
				if(angular.isDefined(data.numOfShopListItemLines)){
					$rootScope.populateShoppingListCount(data);
				}
				deferred.resolve(data);	
			});
			return deferred.promise;
		};
		return serviceObject;
}]);
// promotional items service

AppServicesModule.factory('PromotionalItemService',['Util','DataSharingService','$http','$q','$log',
                                              function(Util,DataSharingService,$http,$q,$log){
	var serviceObject = { };

	serviceObject.getPromotinalItems= function(params){
		//$log.info("PromotionalItemService.getPromotinalItems() -- going to fetch data from server");
		var deferred = $q.defer();
		var promoType = params[7];
		if(promoType == "PromotionCampaign"){
			Util.getDataFromServer('getPromotionalItemsBestOfferUrl',params).then(function(data){
				deferred.resolve(data);	
			});
		}else if(promoType == "MostPopular"){
			Util.getDataFromServer('getPromotionalItemsMostPopularUrl',params).then(function(data){
				deferred.resolve(data);	
			});
		}else if(promoType == "AlternativesItems"){
			Util.getDataFromServer('getAlternativesItemsUrl',params).then(function(data){
				deferred.resolve(data);	
			});
		}else{
			Util.getDataFromServer('getPromotionalItemsUrl',params).then(function(data){
				deferred.resolve(data);	
			});
		}
//		Util.getDataFromServer('getPromotionalItemsUrl',params).then(function(data){
//			deferred.resolve(data);	
//		});
		return deferred.promise;
	};
	serviceObject.getSingleItemDetails= function(params,showLoader){
		//$log.info("PromotionalItemService.getPromotinalItems() -- going to fetch data from server");
		var deferred = $q.defer();
		Util.getDataFromServer('getSingleItemDetailUrl',params,showLoader).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};

	serviceObject.getCatalogFilterList = function(params){
		//$log.info("PromotionalItemService.getCatalogFilterList() -- going to fetch data from server");

		var deferred = $q.defer();
		Util.getDataFromServer('mainCatelogFilterUrl',params).then(function(data){
			deferred.resolve(data);	
		});
		return deferred.promise;
	};
	
	serviceObject.getFilterConfiguration = function(){
		
		return Util.getFilterConfig('catalogueFilterList'); 
		
	};

	return serviceObject;
}]);
