
// Request Details Controller
catalougeControllers.controller('requestDetailCtrl', ['$rootScope','$scope', '$http','$log', 'DataSharingService','RequestSearchService', 'Util','$state',
                                                       function ($rootScope,$scope, $http,$log,DataSharingService,RequestSearchService,Util,$state) {
	// new code
	var requestDetailCtrlInit = function(){
		if(historyManager.isBackAction){
		historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	//	$log.log("  history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	
	var initController = function()
	{ 
			$scope.isUserSessionActive = true;
			$scope.cachedPaginationObj = null;
			$scope.paginationObj = null;
			var params = getRequestParams(); 		
			getRequestDetailPageData(params,FirstPage);
			

	};
	
	var getRequestDetailPageData =  function(requestParams,page){
		$scope.paginationObj = $rootScope.getPaginationObject('requestDetailsUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		var PageNo ="";
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('requestDetailsUrl',requestParams,PageNo ).then(function(data){
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true);
				}else{
					setUIPageData(data,false);
				}
			});
		}
	};
	
	
	var setUIPageData = function(data,isFirstPageData){
		
		$scope.requestDetails = data;
		$rootScope.changeHomePageTitle(Util.translate('TXT_PAGE_TITLE_REQUEST_WITH_NUMBER') + $scope.requestDetails.requestId, true);
		//DataSharingService.setObject("requestDetail",data);
		DataSharingService.setToSessionStorage("requestDetail",data);
		var ItemList = data.reqLineBeanList;
		if(isNull(data)){
			setRequestLineList(null,isFirstPageData); 
			if(isFirstPageData){
				$rootScope.applyPreviousPaginationObject('requestDetailsUrl' , $scope);
			}


		}else{
			if(isFirstPageData){
				$scope.requestDetails = data;
				if(ItemList.length>0){
					$scope.requestLineDataFound = true;
				}else{
					$scope.requestLineDataFound = false;
				}
			}
//			$rootScope.requestDetailProperty = data.reqLineBeanList[0];
//			$scope.propertyList = Util.getObjPropList($rootScope.requestDetailProperty);			
//			$scope.requestDetailsPropList = Util.getObjPropList(data);			
//			$log.log("requestDetailCtrl : requestLine length--  " +data.reqLineBeanList.length);
			setRequestLineList(ItemList,isFirstPageData); 
			$rootScope.cachePaginationObject('requestDetailsUrl' , ItemList, $scope);
			if(isFirstPageData && (! Array.isArray(ItemList) ||  !ItemList.length > 0 ) ){
				$rootScope.applyPreviousPaginationObject('requestDetailsUrl' , $scope);
			}

		}
			$rootScope.setPageLoadMsg(ItemList,isFirstPageData,$scope);
			//$log.log("object info : "+JSON.stringify($scope.requestDetails));
			$scope.$root.$eval();
		};
		
	var setRequestLineList = function(itemList,isFirstPageData){
		if(Array.isArray(itemList) && itemList.length > 0 ){
			$scope.requestLineData = itemList;				
			
		}else{
			if(!isFirstPageData){
				$scope.requestLineData = [];
			}
		}
		//updateFilterStatus(itemList,isFirstPageData);
	};
	$scope.previousPage = function(){		
		 var params = getRequestParams();		
		 getRequestDetailPageData(params,PrevPage);
	};
	$scope.nextPage = function(){	
		 var params = getRequestParams();		
		 getRequestDetailPageData(params,NextPage);
	};
			
	$scope.getFirstPage = function()
	{			
		 var params = getRequestParams();		
		 getRequestDetailPageData(params,FirstPage);
				
	};
	var getRequestParams = function(){
		var requestParams = [];
		//var request = DataSharingService.getObject("request_clickedDetail");
		var request = DataSharingService.getFromSessionStorage("request_clickedDetail");
		if( request !=null){
			$scope.request = request;
			requestParams = ["RequestNumber",request.requestId];
			//console.log("request detail "+ JSON.stringify(request));
			//console.log("request detail requestParams"+ JSON.stringify(requestParams));
			
		};
		return requestParams;
	};	
	
	$scope.showRequestLineDetail = function(requestLine,requestNumber){
		
	//	DataSharingService.setObject("requestLine_clickedDetail",requestLine);	
	//	DataSharingService.setObject("requestNumber",requestNumber);
		DataSharingService.setToSessionStorage("requestLine_clickedDetail",requestLine);	
		DataSharingService.setToSessionStorage("requestNumber",requestNumber);
		var needToLogin = $rootScope.isSignOnRequired('CON_REQUEST_LINE_INFORMATION','home.requestline',true);
		if(!needToLogin){
			$state.go('home.requestline');
		}
	};
	
$scope.exportRequestLineTableAsPDF = function(){
		
		var requestLines = getRequestLinesForExport($scope.requestDetails);
		var basicInfo = getBasicInfoForExport($scope.requestDetails);
		var orderText,orderFees,addresses,quotationReferences;
		var fileName = "RequestDetail_"+$scope.requestDetails.requestId+"_"+getCurrentDateTimeStr()+".pdf";
		var docHeader = Util.translate('CON_REQUEST_NUMBER') + "  " + $scope.requestDetails.requestId;
		saveDetailsPDF(requestLines,basicInfo,orderText,quotationReferences,orderFees,addresses,docHeader,fileName);
	};
	
	var getRequestReferenceHeading = function(referenceType){
		
		var referenceHeading = "";
		if(referenceType == 'Order'){
			referenceHeading = Util.translate('CON_ORDER');
		}else if(referenceType == 'Invoice'){
			referenceHeading = Util.translate('CON_INVOICE');
		}
		return referenceHeading;
	};
	
	var getBasicInfoForExport = function(requestDetails){
		var basicInfo = new Table();
		basicInfo.heading = Util.translate('CON_REQUEST_HEADER_INFORMATION') ; 
		var row = [];
		row.push(Util.translate('COH_CUSTOMER'));
		row.push(requestDetails.customer);
		basicInfo.addRow(row);
		
		row = [];
		row.push(Util.translate('CON_STATUS'));
		row.push(requestDetails.status);
		basicInfo.addRow(row);
		
		row = [];
		row.push(Util.translate('CON_HANDLER'));
		row.push(requestDetails.handlerCode);
		row.push(requestDetails.handlerDesc);
		basicInfo.addRow(row);
		
		row = [];
		row.push(Util.translate('COH_YOUR_REFERENCE'));
		row.push(requestDetails.yourReference);
		basicInfo.addRow(row);
		
		row = [];
		row.push(Util.translate('COH_DATE'));
		row.push(requestDetails.date);
		basicInfo.addRow(row);
		
		row = [];
		row.push(Util.translate('CON_TIME'));
		row.push(requestDetails.time);
		basicInfo.addRow(row);
		
		
		row = [];
		row.push(Util.translate('CON_DESTINATION'));
		row.push(requestDetails.destination);
		basicInfo.addRow(row);
		
		row = [];
		
		row.push(getRequestReferenceHeading(requestDetails.referenceType));
		row.push(requestDetails.reference);
		basicInfo.addRow(row);
		
		
		row = [];
		row.push(Util.translate('COH_REQUEST_TYPE'));
		row.push(requestDetails.requestType);
		basicInfo.addRow(row);
		
		
		row = [];
		row.push(Util.translate('CON_REQUEST_DESCRIPTION'));
		row.push(requestDetails.reqDesc);
		basicInfo.addRow(row);
		
		row = [];
		row.push(Util.translate('CON_RESOLUTION_TYPE'));
		row.push(requestDetails.resolutionType);
		basicInfo.addRow(row);
		
		
		row = [];
		row.push(Util.translate('CON_RESOLUTION_DESCRIPTION'));
		row.push(requestDetails.resolutionDesc);
		basicInfo.addRow(row);
		
		return basicInfo;
	};


	var getRequestLinesForExport = function(requestDetails){
		var requestLines = new Table();
		requestLines.heading = Util.translate('CON_REQUEST_LINES');
		
		requestLines.addHeader(Util.translate('COH_LINE'));
		requestLines.addHeader(Util.translate('COH_PRODUCT'));
		if($scope.requestDetails.descColumnFlag){
			requestLines.addHeader(Util.translate('COH_DESCRIPTION'));	
		}
		requestLines.addHeader(Util.translate('COH_UNIT'));
		requestLines.addHeader(Util.translate('COH_QUANTITY'));
		requestLines.addHeader(Util.translate('COH_DATE'));
		requestLines.addHeader(Util.translate('COH_REQUEST_TYPE'));
				

		if(!isNull($scope.requestLineData)){
			for(var i=0;i< $scope.requestLineData.length;i++){
				var row = $scope.requestLineData[i];
				var colData = [];
				colData.push(row.lineNumber);
				colData.push(row.product);
				if($scope.requestDetails.descColumnFlag){
					colData.push(row.description);
				}
				colData.push(row.unit);
				colData.push(row.quantity);
				colData.push(row.date);
				colData.push(row.requestType);
				requestLines.addRow(colData);
			}
		}
		else{
			var colData = [];
			
				for(var i=0;i<requestLines.headers.length;i++){
					colData.push("");
				}
				requestLines.addRow(colData);
		}
		
		return requestLines;
	};	
	
	requestDetailCtrlInit();
	// new code
	// old code
//	var requestParams = [];
//	$scope.dataFound = false;
//	$scope.resultMsg ="";
//	$scope.errorMessage = "";
//	$scope.propertyList = [];
//	$scope.callState = $state.current.name;
//	$scope.requestDetailProperty = [];
//
//	var request = DataSharingService.getObject("request_clickedDetail");
//	if( request !=null){
//		$scope.request = request;
//		requestParams = ["RequestNumber",request.requestId];
//		$log.log("request detail "+ JSON.stringify(request));
//		$log.log("request detail requestParams"+ JSON.stringify(requestParams));
//	};
//
//	RequestSearchService.getRequestDetails(requestParams).then(function(data){
//		$scope.requestDetails = data;
//		DataSharingService.setObject("requestDetail",data);
//		if(data.reqLineBeanList==null || data.reqLineBeanList.length == 0){
//			$scope.errorMessage = data.errorMessage;			
//			$scope.dataFound = false;
//		}
//		else
//		{
//			$log.log("requestDetailCtrl messageCode -- "+data.messageCode);					
//			$rootScope.requestLineData = data.reqLineBeanList;	
//			$rootScope.requestDetailProperty = data.reqLineBeanList[0];
//			
//			
//			$scope.errorMessage = " ";	
//			$scope.dataFound = true;
//			$scope.propertyList = Util.getObjPropList($rootScope.requestDetailProperty);			
//			$scope.requestDetailsPropList = Util.getObjPropList(data);			
//			$log.log("requestDetailCtrl : requestLine length--  " +data.reqLineBeanList.length);
//
//		}
//
//	});
//
//	
//
//	// Pagination code
//
//	var pageKey = Util.initPagination('requestDetailsUrl',requestParams);
//	var paginationObj = DataSharingService.getObject(pageKey);	
//	
//	
//	$scope.getRequestLinePageData = function(page)
//	{
//		$log.log("getRequestLinePageData() - called" );
//		$log.log("getRequestLinePageData() - called, isPaginationBlocked = "+ Util.isPaginationBlocked(pageKey).toString());		
//		if(!Util.isPaginationBlocked(pageKey))
//		{	
//		//	requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn);
//			Util.getPageinationData('requestDetailsUrl',requestParams,page ).then(function(data)
//			{
//				$scope.requestDetails = data;
//				DataSharingService.setObject("requestDetail",data);
//				if(data.reqLineBeanList==null || data.reqLineBeanList.length == 0)
//				{	
//					paginationObj.setLastPageNo();
//					$scope.dataFound = false;
//					$scope.requestLineLoadMsg = Util.translate('CON_NO_MORE_PAGE'); 
//					$log.log("requestDetailCtrl -- " + $scope.requestLoadMsg);
//				}
//				else
//				{
//					$log.log("requestDetailCtrl messageCode -- "+data.messageCode);
//				
//					$scope.dataFound = true;
//					$scope.requestLineLoadMsg = "";						
//					$rootScope.requestLineData = data.reqLineBeanList;	
//					$rootScope.requestDetailProperty = data.reqLineBeanList[0];
//					
//				//	requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn);
//					$scope.$root.$eval();					
//				}			
//				$log.log("paginationObj.isLastPageNo(): " +paginationObj.isLastPageNo());
//			});
//		}
//	};
//	
//	$scope.previousPage = function(){		
//		
//		$scope.getRequestLinePageData(PrevPage);		
//	};
//	$scope.nextPage = function(){			
//		$scope.getRequestLinePageData(NextPage);		
//	};
//			
//	$scope.getFirstPage = function()
//	{			
//		$scope.getRequestLinePageData(FirstPage);
//				
//	};
//	//  Pagination code ends
//	
//	
//	$scope.showRequestLineDetail = function(requestLine,requestNumber){
//		
//		DataSharingService.setObject("requestLine_clickedDetail",requestLine);	
//		DataSharingService.setObject("requestNumber",requestNumber);	
//		var needToLogin = $rootScope.isSignOnRequired('CON_REQUEST_LINE_INFORMATION','home.requestline',true);
//		if(!needToLogin){
//			$state.go('home.requestline');
//		}
//	};
//	
}]);