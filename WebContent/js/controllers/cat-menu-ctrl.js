catalougeControllers.controller('catMenuCtrlView', ['NewConfigService','MainCatalogService','ShoppingCartService','$rootScope','$scope','ProdSearchService' ,'$stateParams','$log', '$state','$filter','DataSharingService','Util',
                                                    function (NewConfigService,MainCatalogService,ShoppingCartService,$rootScope,$scope,ProdSearchService, $stateParams,$log,$state,$filter,DataSharingService,Util) {

		$scope.currentCatMenuPage = 1;
		$scope.filterNewSearch = true;
		$rootScope.initCommitsPromise.then(function(){
		var param = [];
		if(!isNull($stateParams.ELEMENT_ID)){
		 param = ["CAT_ID",$stateParams.ITEM_CODE, "PageNo",$scope.currentCatMenuPage,"ELEMENT_ID",$stateParams.ELEMENT_ID];
		}else{
	     param = ["CAT_ID",$stateParams.ITEM_CODE, "PageNo",$scope.currentCatMenuPage,"ELEMENT_ID","","OnlyMainCat","YES"];
		}
		Util.getDataFromServer('getCatalogCategoryUrl',param).then(function(data){
			if (data.catalogueBeanList.length > 0 ){
				$scope.catData = data.catalogueBeanList[0].subCatalogueBeanList;
				$scope.catalogueGroup = $scope.catData;
				$scope.moreRecords = data.catalogueBeanList[0].moreRecords;
				$scope.hideCatalog = false;
				$scope.filterPanelCollapsed = false;
				$scope.filterNewSearch = true;
				$scope.filterCatalog = false;
				if($scope.catData.length < 1){
					$scope.removeCatalogDisplay = true;
					$scope.filterNewSearch = true;
					$scope.filterCatalog = true;
				}
			}else{
				$scope.hideCatalog = true;
				$scope.filterPanelCollapsed = true;
				$scope.filterNewSearch = false; 
				$scope.filterCatalog = false;
				$scope.catalogue = [];
				$scope.removeCatalogDisplay = true;
			}
			
		});
	});
	
		  $scope.firstCatMenuPage = function() {
			  if ($scope.currentCatMenuPage > 1) {
			      $scope.currentCatMenuPage = 1;
			      $scope.nomorePrevRecords = false;
					$rootScope.initCommitsPromise.then(function(){
						var param = [];
						if(!isNull($stateParams.ELEMENT_ID)){
							 param = ["CAT_ID",$stateParams.ITEM_CODE, "PageNo",$scope.currentCatMenuPage,"ELEMENT_ID",$stateParams.ELEMENT_ID];
							}else{
						     param = ["CAT_ID",$stateParams.ITEM_CODE, "PageNo",$scope.currentCatMenuPage,"ELEMENT_ID","","OnlyMainCat","YES"];
							}
						Util.getDataFromServer('getCatalogCategoryUrl',param).then(function(data){
							if (data.catalogueBeanList.length > 0 ){
								$scope.catData = data.catalogueBeanList[0].subCatalogueBeanList;
								$scope.catalogueGroup = $scope.catData;
								$scope.moreRecords = data.catalogueBeanList[0].moreRecords;
								if($scope.catData.length < 1){
									$scope.removeCatalogDisplay = true;
									$scope.filterNewSearch = true;
									$scope.filterCatalog = true;
								}
							}else{
								$scope.removeCatalogDisplay = true;
								$scope.filterNewSearch = true;
								$scope.filterCatalog = true;
							}
						});
					});
			  	}
			  };  
		$scope.nextCatMenuPage = function() {
			    if ($scope.currentCatMenuPage <= $scope.catalogueGroup.length) {
			      $scope.currentCatMenuPage++;
			      if($scope.currentCatMenuPage > 1){
				    	$scope.nomorePrevRecords = true;
				    }
					$rootScope.initCommitsPromise.then(function(){
						var param = [];
						if(!isNull($stateParams.ELEMENT_ID)){
							 param = ["CAT_ID",$stateParams.ITEM_CODE, "PageNo",$scope.currentCatMenuPage,"ELEMENT_ID",$stateParams.ELEMENT_ID];
							}else{
						     param = ["CAT_ID",$stateParams.ITEM_CODE, "PageNo",$scope.currentCatMenuPage,"ELEMENT_ID","","OnlyMainCat","YES"];
							}
						 
						Util.getDataFromServer('getCatalogCategoryUrl',param).then(function(data){
							if (data.catalogueBeanList.length > 0 ){
								$scope.catData = data.catalogueBeanList[0].subCatalogueBeanList;
								$scope.catalogueGroup = $scope.catData;
								$scope.moreRecords = data.catalogueBeanList[0].moreRecords;
								if($scope.catData.length < 1){
									$scope.removeCatalogDisplay = true;
									$scope.filterNewSearch = true;
									$scope.filterCatalog = true;
								}
							}else{
								$scope.removeCatalogDisplay = true;
								$scope.filterNewSearch = true;
								$scope.filterCatalog = true;
							}
						});
					});
			    }
			  };
			  $scope.prevCatMenuPage = function() {
				    if ($scope.currentCatMenuPage > 1) {
				      $scope.currentCatMenuPage--;
						$rootScope.initCommitsPromise.then(function(){
							var param = [];
							if(!isNull($stateParams.ELEMENT_ID)){
								 param = ["CAT_ID",$stateParams.ITEM_CODE, "PageNo",$scope.currentCatMenuPage,"ELEMENT_ID",$stateParams.ELEMENT_ID];
								}else{
							     param = ["CAT_ID",$stateParams.ITEM_CODE, "PageNo",$scope.currentCatMenuPage,"ELEMENT_ID","","OnlyMainCat","YES"];
								}
						Util.getDataFromServer('getCatalogCategoryUrl',param).then(function(data){
								if (data.catalogueBeanList.length > 0 ){
									$scope.catData = data.catalogueBeanList[0].subCatalogueBeanList;
									$scope.catalogueGroup = $scope.catData;
									$scope.moreRecords = data.catalogueBeanList[0].moreRecords;
									if($scope.catData.length < 1){
										$scope.removeCatalogDisplay = true;
										$scope.filterNewSearch = true;
										$scope.filterCatalog = true;
									}
								}else{
									$scope.removeCatalogDisplay = true;
									$scope.filterNewSearch = true;
									$scope.filterCatalog = true;
								}
							});
						});
				    }
				    if($scope.currentCatMenuPage == 1){
				    	$scope.nomorePrevRecords = false;
				    }
				  };
	var tblViewObj  = {
			"name" :"tableView",
			"lable":""
	};

	var gridViewObj = {
			"name" : "gridView",
			"lable" : ""
	};
	var requestParams = [];
	var paramProd = [];
	
	var requestNew = [];
	var pageKey = null;
	
	var catMenuCtrlViewInit = function(){
		$scope.advanceSearchParamsExist = false;
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$scope.Object($scope.productList);
			$scope.$root.$eval();
		}else{
			$scope.productSearchFilterList = getProductSearchFilterList();
			if(!isNull($scope.productSearchFilterList)){
				$scope.productSearchFilterList.removeAllSelectedFiltersAdvanceSearch();
			}
			init();
			//makePrdSearchFilters();
		}
	};
	
	function removeAllSelectedFiltersAdvanceFromFilterList(list) {
		for(var i = 0; i < list.filterList.length; i++) {
			list[i].removeAllSelectedFiltersAdvanceSearch();
		}
	}
	
	var init = function(){
		$scope.sortColumn = "";
		$scope.orderType = "";
		$scope.filterOn = false;
		$scope.hideCatalog = false;
		$scope.noDataFoundMessage = false;
		$scope.filterParam = [];
		$scope.showExtendInfo='false';
		$scope.filterFormInfo = {};
		$scope.filterMsg = "";
		$scope.currentCatProdPage = 1;
		$scope.elementID = null;
		setView();
		$scope.ViewLabelName = $scope.getViewLable();
		$scope.previousView = '';
		$scope.mainCatalogId = $stateParams.ITEM_CODE;
//		$scope.catalogDesc = $stateParams.ITEM_DESC;
		$scope.catDescBreadCrum = "";
		$scope.catParentDescBreadCrum = "";  // Catalogue parent description		
		$scope.elementDesc = ""; 
		if(!isNull($stateParams.ELEMENT_ID)){
		$scope.mainCatalogElementId = $stateParams.ELEMENT_ID;
		}
		$rootScope.productfilterDetailSettings = DataSharingService.getProductFilterDetailsSettings();
		
		$scope.mainCataloguePageIndex =1;
		if((!isNull($stateParams.ELEMENT_ID))&&($stateParams.ELEMENT_ID !="")){
			requestParams = ["CAT_ID",$scope.mainCatalogId,"ELEMENT_ID",$scope.mainCatalogElementId];
			$scope.elementDesc = $stateParams.ELEMENT_DESC;
		}else{
			requestParams = ["CAT_ID",$scope.mainCatalogId];
		}
		if(!isNull($stateParams.ELEMENT_ID)){
			paramProd = ["CAT_ID",$stateParams.ITEM_CODE, "PageNo",$scope.currentCatProdPage,"ELEMENT_ID",$stateParams.ELEMENT_ID];
		}else{
			paramProd = ["CAT_ID",$stateParams.ITEM_CODE, "PageNo",$scope.currentCatProdPage,"ELEMENT_ID","","OnlyMainCat","YES"];
		}

		$scope.productPropList = null; 
		$scope.collapsed = true;
		pageKey = Util.initPagination('catalogProductListUrl',requestParams);
		$rootScope.sortSelection = '';
		$rootScope.sortSelection =  $scope.sortColumn+$scope.orderType;
		//getMainCatalogFilters();
		
		//console.log("------------- 2nd call made from init --------------------");
		getDataForFirstPage();
	};
	
	var setMessages = function (data){
		if(!isNull(data)){
			if(data.messageCode ==111){
				$scope.infoMsg = Util.translate('MSG_NO_OBJECTS_FOR_SELECTION');
				showMsgNoObjFound();
			}else if(data.messageCode ==115){
				$scope.infoMsg = Util.translate('MSG_SELECT_ANOTHER_CATALOG');
				showMsgNoObjFound(data.messageCode);
			}else if(data.messageCode == 3232){
				$scope.infoMsg = Util.translate('SP_MISSING_FOR_PRICE_IN_BULK');
				showMsgNoObjFound(data.messageCode);
			}else if(data.messageCode == 3233){
				$scope.infoMsg = Util.translate('SP_MISSING_FOR_AVAIL_IN_BULK');
				showMsgNoObjFound(data.messageCode);
			}
			else{
				setMessagesToNull();
			}
		}
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	});
	
	$scope.$on('userLoggedIn', function(event){ 
		if($state.current.name=='home.catalog.Compare') {
			$scope.SelectedProductList = DataSharingService.getFromLocalStorage("compareObjectList");
			$scope.updateCount = 0; 
			for(var i = 0; i < $scope.SelectedProductList.length; i++) {
				$rootScope.getProductsDetail($scope.SelectedProductList[i].code, $scope.updateBuy);
			}
		}
	});

	// ADM-010
	$scope.$watch('freeSearchObj.packPiece',
		function(){
			if(!isNull($scope.productList)){
				$rootScope.refreshUIforPackPiece($scope.freeSearchObj.packPiece, 
				$scope.productList);
			}
		}
	);
	
	$scope.updateBuy = function(data) {
		for(var i = 0; i < $scope.SelectedProductList.length; i++) {
			if($scope.SelectedProductList[i].code == data.itemCode) {
				$scope.SelectedProductList[i].isBuyingAllowed = data.isBuyAllowed;
				break;
			}
		}
		$scope.updateCount++;
		if($scope.updateCount >= $scope.SelectedProductList.length) {
			$scope.compare();
		}
	}
	
	$scope.getView = function(){
		var view = DataSharingService.getObject(CONST_mainCatPrevCompareView);
		if(!angular.isDefined(view) || view==null){
			view = gridViewObj;
		}
		return view;
	};

	$scope.getViewLable = function(){
		if ($scope.currentView.name == tblViewObj.name ){
			gridViewObj.lable = Util.translate('CON_GRID_VIEW');
			return gridViewObj.lable;
		}else{
			tblViewObj.lable = Util.translate('CON_TABLE_VIEW');
			return tblViewObj.lable;
		}
	};

	$scope.addToCart = function (itemCode,quantity,salesUnit){
		if(Util.isNotNull(itemCode) && Util.isNotNull(quantity) && quantity >0 && Util.isNotNull(salesUnit)) {
			var params =  ["itemCode",itemCode,"itemQty",quantity,"unitCode",salesUnit];
			ShoppingCartService.addToCart(params);
		}
	};

	$scope.gridViewBuyClicked = function(cat){
		if(!cat.validQuantity || isNull(cat.quantity) || ""==cat.quantity){
			cat.validQuantity = false;
			cat['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
		}else{
		cat.isBuyingAllowed = false;
		$rootScope.addItemToCart(cat.code,cat.quantity,cat.salesUnit,cat,"quantity");
		}
	};

	$scope.tableViewBuyClicked = function(cat){
		//console.log("buyClicked: Itemcode = " +cat.code + " quantity = " + $scope.buyItem[$rootScope.getId('quantity',cat.code)] + " salesUnit = " + $scope.buyItem[$rootScope.getId('salesUnit',cat.code)]);
		$rootScope.addItemToCart(cat.code,$scope.buyItem[$rootScope.getId('quantity',cat.code)],cat.defaultSalesUnit);
	};

	$scope.addItem = function(item)
    {
    	$rootScope.addItemToCurrentList(item);    	
    };
    
	$scope.Object = function(list){
		$scope.buyObj = new Buy(list);
		$scope.buyObj.setPropNameItemCode('code');
		$scope.buyObj.setPropNameOrdered('quantity');
		$scope.buyObj.setPropNameUnit('salesUnit');
		$scope.buyObj.setPropNameisBuyAllowed('isBuyingAllowed');
	};
	
	$scope.buyItems = function(item,buyObj){
		$rootScope.buyMultipleItems(item,$scope.buyObj);
	};
	
	$scope.addMultipleItemsToCurrentList = function(item){
		$rootScope.addMultipleItemsToList(item,$scope.buyObj);
	};
	
	$scope.submitFilter = function(){
		$scope.filterOn = true;
		var params = getSearchParamsSorting();
		//getInvoiceHistoryPageDataSorting(params,FirstPage,false);
	};
	
	var getSearchParamsSorting = function(){
		var params  = [];
		if($scope.filterOn){
			params = $scope.productSearchFilterList.getParamsList();
		}
		return params;
	};	
	
	$scope.loadCatalogItems = function(catalogueCode,catDescription,catElementCode,cat){
		//console.log("--------------------- loadCatalogItems ----------------------------");
		//$scope.catDescBreadCrum = catDescription;
		$scope.elementID = catElementCode;
		//$scope.parentCode = cat.parentCode;
		if(catDescription =="CON_MORE"){
			$scope.elementID = cat.parentCode;
		}
		var requestParams = ["CAT_ID",$scope.mainCatalogId,"ELEMENT_ID",$scope.elementID];
		pageKey = Util.initPagination('catalogProductListUrl',requestParams);
		requestNew = ["CAT_ID",catalogueCode,"ELEMENT_ID",$scope.elementID];
		var paramNew = ["CAT_ID",catalogueCode, "PageNo","1","ELEMENT_ID",$scope.elementID];
		$rootScope.initCommitsPromise.then(function(){
			$rootScope.showCatalogProdLoad = true;
			Util.getDataFromServer('catalogProductListUrl',requestNew).then(function(data){
				$rootScope.showCatalogProdLoad = false;
				if(data.productList==null || data.productList.length == 0){
					Util.getDataFromServer('getCatalogCategoryUrl',paramNew).then(function(data){
						var varLength = data.catalogueBeanList[0];
						if(!isNull(varLength.subCatalogueBeanList) && varLength.subCatalogueBeanList.length > 0 ){
							$state.go("home.catalogSub",{"ITEM_CODE":$stateParams.ITEM_CODE,"ELEMENT_ID":$scope.elementID});
						}
						else{
							showMsgNoObjFound();
						}
						
					});
					//$scope.catDescBreadCrum = data.description;
					
				}else{
					$scope.productListDataLoadItems = data;
					if(!isNull($rootScope.webSettings) && $rootScope.webSettings.showCatalogueBreadcrumb == true){
						MainCatalogService.getCatalogBredCrums(requestParams).then(function(data){
							$scope.subCatalogueBeanListBred1 = data;
							$scope.subCatalogueBeanListBred = $scope.subCatalogueBeanListBred1;
							DataSharingService.setObject("catBredCrumb", data);
							$state.go("home.catalogSub",{"ITEM_CODE":$stateParams.ITEM_CODE,"ELEMENT_ID":$scope.elementID});
							$scope.catDescBreadCrum = $scope.productListDataLoadItems.description;
							if(!isNull($scope.productListDataLoadItems) && !isNull($scope.productListDataLoadItems.searchFilterList)){
								$scope.filterDetails = $scope.productListDataLoadItems.searchFilterList;
								makePrdSearchFilters();
								
							}
							setUIData($scope.productListDataLoadItems,true,$scope.subCatalogueBeanListBred);
							
							DataSharingService.setObject("productListData", $scope.productListDataLoadItems);
							
						});
					}else{
						$state.go("home.catalogSub",{"ITEM_CODE":$stateParams.ITEM_CODE,"ELEMENT_ID":$scope.elementID});
						$scope.catDescBreadCrum = $scope.productListDataLoadItems.description;
						if(!isNull($scope.productListDataLoadItems) && !isNull($scope.productListDataLoadItems.searchFilterList)){
							$scope.filterDetails = $scope.productListDataLoadItems.searchFilterList;
							makePrdSearchFilters();
							
						}
						setUIData($scope.productListDataLoadItems,true);
						
						DataSharingService.setObject("productListData", $scope.productListDataLoadItems);
					}
						
				
					//getDataForFirstPage(requestNew);
				}
				
			});
		});
		
		
		//$state.go(CATALOG_DETAIL,{"CAT_ID": catalogueCode,"CAT_DESC": catDescription,"ELEMENT_ID":catElementCode ,"ELEMENT_DESC":catEleDescription});
		
	};
	
	if($state.current.name == 'home.catalog.Compare'){
		$scope.SelectedProductList = DataSharingService.getFromLocalStorage("compareObjectList");
		$scope.previousState = DataSharingService.getObject("prevCompareState");	

	}else{
		$scope.SelectedProductList = [];
	}

	var getDataForFirstPage = function(){
		//console.log("------------- getDataForFirstPage 2nd call to getCatalogProducts -----------------");
		$rootScope.initCommitsPromise.then(function(){
			var localDataStored = DataSharingService.getObject("productListData");
			var bredCrumbsStored = DataSharingService.getObject("catBredCrumb");
			//console.log("localDataStored-------------"+localDataStored);
			if(!isNull(localDataStored)) {
				$scope.catDescBreadCrum = localDataStored.description;
				setUIData(localDataStored, true,bredCrumbsStored);
				DataSharingService.removeObject("productListData");
				DataSharingService.removeObject("catBredCrumb");
			} else {
				$rootScope.showCatalogProdLoad = true;
				//console.log("------------- getDataForFirstPage 2nd call made to getCatalogProducts -----------------");
				Util.getDataFromServer('catalogProductListUrl',paramProd).then(function(data){
					$rootScope.showCatalogProdLoad = false;
					$scope.iscatParentDescBreadCrum = false; // Flag for Catalogue parent description
					if(!isNull(data.description)){
						$scope.catDescBreadCrum = data.description;						
						if(typeof data.parentDescription != 'undefined'){
							$scope.catParentDescBreadCrum = data.parentDescription; // Added catalogue parent description
							$scope.iscatParentDescBreadCrum = true;  // Added flag for catalogue parent description
						}						
					}
					if(data.messageCode === 111){
						$scope.noDataFoundMessage = true;
						$scope.productSearchFilterList = null;
//						var errorMsg = Util.translate('CON_NO_DATA_FOUND');
//						var dlg = $rootScope.openMsgDlg(Util.translate('CON_CATALOGUE'),[errorMsg]);
//						dlg.result.then(function () {
//							//if user clicks on ok
//						},function(){
//							//redirecting to home page.
//							$rootScope.reloadHomePageUrl();
//						});
					}else{
						$scope.noDataFoundMessage = false;
						if(!isNull(data) && !isNull(data.searchFilterList)){
							$scope.filterDetails = data.searchFilterList;
							makePrdSearchFilters();
							
						}
						setUIData(data,true);
					}
				});
				if(!isNull($rootScope.webSettings) && $rootScope.webSettings.showCatalogueBreadcrumb == true){
				MainCatalogService.getCatalogBredCrums(requestParams).then(function(data){
					$scope.subCatalogueBeanListBred1 = data;
					$scope.subCatalogueBeanListBred = $scope.subCatalogueBeanListBred1;
					
				});
				}
				
			}
		});
	};
	
	var setProductSearchFilterList = function(productSearchFilters){
		return DataSharingService.setObject("productSearchFilterList",productSearchFilters);
	};
	
	var removeAllAdvanceFilters = function(){
		if(!isNull($scope.productSearchFilterList)){
			$scope.productSearchFilterList.removeAllSelectedFilters();
		}
	};
	
	var getProductSearchFilterList = function(){
		return DataSharingService.getObject("productSearchFilterList");
	};
	
	$rootScope.bannerSearchFilterChange = function(lang){
		makePrdSearchFilters(lang);
	};
	
	var makePrdSearchFilters = function(lang){
		var params =[];
		if(isNull(getProductSearchFilterList()) || !isNull(lang) || !isNull($rootScope.languageChangeFilter || !isNull($scope.filterDetails))){
			if(!isNull(lang)){
				params = ["LanguageCode",lang.code];
				}
			var finalFilterList = new FilterList();
			var staticFilterConfig = ProdSearchService.getStaticSolrFilterConfig();
			var filterDataList = $scope.filterDetails;
			
			var dynamicFilterConfig = null;
//			ProdSearchService.getDynamicSolrFilterConfig(params).then(function(data){
//				dynamicFilterConfig = data.filterBeans;
//				finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig,'searchFilterList');
//				$scope.productSearchFilterList = finalFilterList;
//				setProductSearchFilterList(finalFilterList);
//				//console.log("makePrdSearchFilters : " + JSON.stringify(finalFilterList));	
//			});
			
			Util.getDataFromServer('mainCatelogFilterUrl',params).then(function(data){
				dynamicFilterConfig = data.filterBeans;
				finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig,'searchFilterList');
				$scope.productSearchFilterList = finalFilterList;
				setProductSearchFilterList(finalFilterList);
				//console.log("makePrdSearchFilters : " + JSON.stringify(finalFilterList));	
		});
		}else{
			$scope.productSearchFilterList = getProductSearchFilterList();
		}
		removeAllAdvanceFilters();
	};
	
	var setUIData = function(data,isFirstPageData,bredCrum){
		if(data.messageCode == 115){ 
			setMessages(data);
		}
		$scope.isMoreRecords = data.moreRecords;
		if(!hasVerticalScroll()) {
			$rootScope.catalogLoadMoreFunction();
		}
		if ($scope.currentView.name == tblViewObj.name ){
			$scope.filterNewSearch = true;
			$scope.filterCatalog = true;
		}
		if(isFirstPageData){
			if(data.productList != undefined){
			if(data.productList==null || data.productList.length == 0){
				$scope.dataFound = false;				
				$scope.filterMsg =  Util.translate('MSG_NO_OBJECTS_FOR_SELECTION',null);
				showMsgNoObjFound();
				var icon = '<a class="OrderInformation-icon" shref="" title="'+$scope.filterMsg+'"></a>';				
				$scope.filterMsg = icon + $scope.filterMsg;
				$scope.filterMsg = "";
				$scope.productList = DataSharingService.getObject("CATALOG_PRODUCT_LIST" );
			}
			}
			$rootScope.openLoader();
			$scope.mainCatalogue = data;			
			$rootScope.initHomeCatalogTableHeaders();
			$rootScope.setHomeCatalogExtendedData(data.productList);
			$scope.productList = data.productList;
			$rootScope.setHomeCatalogTableHeaders($scope.productList);
			$scope.Object($scope.productList);
			$scope.recordFound = true;
			
			if(!isNull(bredCrum)){
				var test = JSON.stringify(bredCrum);;
				test =  JSON.parse(test);
				$scope.subCatalogueBeanListBred1 = test;
				$scope.subCatalogueBeanListBred = test;
			}
			DataSharingService.setObject("CATALOG_PRODUCT_LIST",$scope.productList );
			if(!isNull($scope.$root)){
				$scope.$root.$eval();
			}
			$rootScope.closeLoader();
		}else{
			Util.appendItemsToList(data.productList, $scope.productList);
			$rootScope.setHomeCatalogExtendedData(data.productList);
			$rootScope.setHomeCatalogTableHeaders($scope.productList);
			$scope.initBuyObject($scope.productList);
			if(!isNull(data.description)){
			$scope.catDescBreadCrum = data.description;
			}
			DataSharingService.setObject("CATALOG_PRODUCT_LIST",$scope.productList );
			requestParams=  getParamKeyList($scope.filterOn,$scope.filterParam,$scope.mainCatalogId,$scope.mainCatalogElementId,$scope.orderType,$scope.sortColumn);
			if(!isNull($scope.$root)){
				$scope.$root.$eval();
			}
		}
		/* ADM-010 */$rootScope.refreshUIforPackPiece($scope.freeSearchObj.packPiece,
				$scope.productList);
	};
	var getMainCatalogFilters = function(){	
		var finalFilterList = new FilterList();
		var staticFilterConfig = MainCatalogService.getFilterConfiguration();
		var filterDataList = DataSharingService.getProductFilterDetailsSettings(); 
		var dynamicFilterConfig = null;
		Util.getDataFromServer('mainCatelogFilterUrl',[]).then(function(data){
				dynamicFilterConfig = data.filterBeans;
				finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig,'searchFilterList');
				$scope.FilterList = finalFilterList;
		});
	};
	
	$scope.removeMainCatalogFilter = function(filterItem)
	{
		$scope.FilterList.removeSelection(filterItem);
		$scope.filterParam  = $scope.FilterList.getParamsList();		
		var filterParam =  getParamKeyList($scope.filterOn,$scope.filterParam,$scope.mainCatalogId,$scope.mainCatalogElementId,$scope.orderType,$scope.sortColumn); 
		$scope.pageKey = Util.initPagination('catalogProductListUrl',filterParam); 
		$scope.paginationObj = DataSharingService.getObject($scope.pageKey );	
		MainCatalogService.getMainCatalogProducts(filterParam).then(function(data){
			if(data.productList==null || data.productList.length == 0){
				$scope.dataFound = false;				
				$scope.filterMsg =  Util.translate('MSG_NO_OBJECTS_FOR_SELECTION',null);
				showMsgNoObjFound();
				var icon = '<a class="OrderInformation-icon" shref="" title="'+$scope.filterMsg+'"></a>';				
				$scope.filterMsg = icon + $scope.filterMsg;
				$scope.recordFound = false;				
			}else{
				$scope.filterMsg = "";
				$scope.SelectedProductList = [];
				setUIData(data,true);
			}
		});
	};
	
	$scope.productSearchFilterLength = function(list){
		if(!isNull(list)){
			if(list.length >0){
				return false;
			}else{return true;}
			
		}else{
			return true;
		}
		
	};
	
	$scope.getFilterSettings = function(filterList)
	{
		var filterConfigData = MainCatalogService.getFilterConfiguration();
		//console.log("catMenuCtrlView getFilterSettings--  " +JSON.stringify(filterConfigData));			 
		$scope.FilterList =  $rootScope.getSearchFilterSettings(filterList,filterConfigData);
		if( $scope.FilterList != null )
		{
			for(var i= 0; i<$scope.FilterList.length; i++)
			{
				for(var j=0; j <$rootScope.productfilterDetailSettings.length ;j++)
				{
					if($scope.FilterList[i].filterName == $rootScope.productfilterDetailSettings[j].filterName)
					{
						$scope.FilterList[i].filterValueList = $rootScope.productfilterDetailSettings[j].searchFilterList;
					}
				}

			}
		}
	};

	var getParamKeyListElementId = function(filterOn,filterParam,catalogId,catalogElementId,orderType,sortColumn)
	{
		var i=0;
		var requestParams =[];
		if(filterOn == true)
		{
			requestParams = ["CAT_ID",catalogId,"ELEMENT_ID",catalogElementId,"OrderBy",orderType,"SortBy",sortColumn];
			if(filterParam.length > 0)
			{
				for(i=0;i<filterParam.length;i++)
				{requestParams.push(filterParam[i]);}
			}
		}
		else			
		{	requestParams = ["CAT_ID",catalogId,"ELEMENT_ID",catalogElementId];}

		return requestParams;		
	};
	
	var getParamKeyList = function(filterOn,filterParam,catalogId,orderType,sortColumn)
	{
		var i=0;
		var requestParams =[];
		if(filterOn == true)
		{
			requestParams = ["CAT_ID",catalogId,"OrderBy",orderType,"SortBy",sortColumn];
			if(filterParam.length > 0)
			{
				for(i=0;i<filterParam.length;i++)
				{requestParams.push(filterParam[i]);}
			}
		}
		else			
		{	requestParams = ["CAT_ID",catalogId];}

		return requestParams;		
	};
	
		
	$scope.submitMainCatlogFilter = function()
	{
		$scope.filterOn = true;
		//$scope.filterParam  = $scope.FilterList.getParamsList();
		var params = getSearchParamsSorting();
		if($scope.mainCatalogElementId == undefined){
			$scope.mainCatalogElementId="";
		}
		var filterParam =  getParamKeyList($scope.filterOn,params,$scope.mainCatalogId,$scope.mainCatalogElementId,$scope.orderType,$scope.sortColumn); 
		
		//console.log("Main Catalog Filter Info :  " + filterParam);
		pageKey = Util.initPagination('catalogProductListUrl',filterParam);  
		MainCatalogService.getMainCatalogProducts(filterParam).then(function(data){
			if(data.productList==null || data.productList.length == 0){
				$scope.dataFound = false;				
				$scope.filterMsg =  Util.translate('MSG_NO_OBJECTS_FOR_SELECTION',null);
				showMsgNoObjFound();
				var icon = '<a class="OrderInformation-icon" shref="" title="'+$scope.filterMsg+'"></a>';				
				$scope.filterMsg = icon + $scope.filterMsg;
				$scope.filterMsg = "";
				$scope.productList = DataSharingService.getObject("CATALOG_PRODUCT_LIST" );
				$scope.recordFound = true;
				
			}
			else
				{
				$scope.filterMsg = "";
				setUIData(data,true);
			}
		});
	};
	

	$scope.isColExist = function(colPropName){
		var isCol = false;
		var val = Util.isPropertyExist(colPropName,$scope.productPropList);
		if(val){
			isCol = true;
		}
		return isCol;
	};

	$rootScope.catalogLoadMoreFunction = function(){
		if($scope.isMoreRecords == true){
			//console.log("CatalogLoadMore() - called, isPaginationBlocked = "+ Util.isPaginationBlocked(pageKey).toString());
		if($scope.advanceSearchParamsExist == false){
			$scope.currentCatProdPage = $scope.currentCatProdPage + 1;
			paramProd[3] = $scope.currentCatProdPage;
			$rootScope.openBottomLoader();
			Util.getDataFromServer('catalogProductListUrl',paramProd ).then(function(data){ 
				$rootScope.closeBottomLoader();
				if(isNull(data) || data.productList==null || data.productList.length == 0){
					Util.stopLoadingPageData(pageKey);
				}else{
					setUIData(data,false);
				}
			});
		}
		
		if($scope.advanceSearchParamsExist == true){
			if(!Util.isPaginationBlocked(pageKey)){
				
				if(!isNull($scope.productSearchFilterList)){
				var	advanceParamList = getAdvanceParamListProductSearch();
					//setPageKey(advanceParamList);
			
				if(advanceParamList.length>0){
			     	$scope.advanceSearchParamsExist = true;
					$rootScope.openBottomLoader();

			     	var paramStringForSearch = "CatalogueProductListInputBean="+JSON.stringify(advanceParamList);
			           // $scope.pageKey = Util.initPagination('solrSearchResultUrl',params);
			           //  Util.appendItemsToList(["PageNo",1], params);
			            ProdSearchService.getCatNewSearchResult(paramStringForSearch).then(function(data){
			            	$rootScope.closeBottomLoader();
							if(data.productList==null || data.productList.length == 0){
								Util.stopLoadingPageData(pageKey);
							}else{
								setUIData(data,false);
							}

			            });
			     }
				
				
				
				}
			}
			
		}
		}
	};

	$scope.isTableView = function(){
		if ($scope.currentView.name == tblViewObj.name ){
			return true;
		}else{
			return false;
		}
	};

	$scope.isGridView = function(){
		if ($scope.currentView.name == gridViewObj.name ){
			return true;
		}else{
			return false;
		}
	};
	
	var setView = function(){
		$scope.currentView = gridViewObj;
		$scope.ViewLabelName =  $scope.getViewLable();
		$scope.filterPanelCollapsed=false;
		$scope.isList  = false;
	};
	
	$scope.changeView = function(){
		if($scope.currentView.name == gridViewObj.name){
			$scope.filterNewSearch = true;
			$scope.filterCatalog = false;
			$scope.currentView = tblViewObj;
			$scope.ViewLabelName =  $scope.getViewLable();
			$scope.filterPanelCollapsed=true;
			$scope.isList  = true;
		}else{
			$scope.filterNewSearch = true;
			$scope.filterCatalog = false;
			$scope.currentView = gridViewObj;
			$scope.ViewLabelName =  $scope.getViewLable();
			$scope.filterPanelCollapsed=false;
			$scope.isList  = false;
		}
	};
	/*	[temp]
 * $scope.changeView = function(){
		if($scope.currentView.name == gridViewObj.name){
			if(!isNull($scope.mainCatalogElementId)){
			$state.go("home.catalogSub",{"ITEM_CODE":$stateParams.ITEM_CODE,"ELEMENT_ID":$scope.mainCatalogElementId,"VIEW":tblViewObj.name});
			}else{
			$state.go("home.catalog",{"ITEM_CODE":$stateParams.ITEM_CODE,"VIEW":tblViewObj.name});	
			}
		}else{
			if(!isNull($scope.mainCatalogElementId)){
			$state.go("home.catalogSub",{"ITEM_CODE":$stateParams.ITEM_CODE,"VIEW":gridViewObj.name});
			}else{
			$state.go("home.catalog",{"ITEM_CODE":$stateParams.ITEM_CODE,"ELEMENT_ID":$scope.mainCatalogElementId,"VIEW":gridViewObj.name});	
			}
		}
	};*/

	$scope.SelectedProduct = function(productList)
	{
		$scope.selectionMsg = "";
		$scope.SelectedProductList = $filter('filter')(productList, {checked: true});
		if($scope.SelectedProductList.length <=10)
		{

			$scope.selectionMsg = " Total selected Items :  "+ $scope.SelectedProductList.length;
			$scope.Comparison = true;
		}
		else
		{
			$scope.selectionMsg = Util.translate('CON_MAX_PRODUCT_COMPARISION');
			$scope.Comparison = false;
		}
		DataSharingService.setToLocalStorage("compareObjectList",$scope.SelectedProductList);
		//console.log("addCompareListItem length -  "+	$scope.SelectedProductList.length);

	};

	$scope.RemoveProduct = function()
	{
		$scope.SelectedProductList = $filter('filter')($scope.SelectedProductList, {checked: true});
		DataSharingService.setToLocalStorage("compareObjectList",$scope.SelectedProductList);
		if($scope.SelectedProductList.length > 1)
		{
		$scope.selectionMsg = " Total selected Items :  "+ $scope.SelectedProductList.length;
		}
		else
		{
			/*$state.go($scope.previousState);					
			$scope.selectionMsg = "";*/
			//alert("showing catalog items");
			angular.forEach($scope.productList, function(a) {        
			    a.checked = false;
			});
			$scope.isCompare = false;
		}

	};
	$scope.compare = function()
	{
		var isOpenLoginDlg=$rootScope.isSignOnRequired("CON_PRODUCT_COMPARE",'home.mainCatalog');
		if(isOpenLoginDlg==true){
			return;
		}
		if(($scope.SelectedProductList.length > 1) && ($scope.Comparison == true))
		{
			$rootScope.mainSelectedProductUnit = {};
			for(var i= 0; i< $scope.SelectedProductList.length ; i++)
			{
				for(var j= 0;j<$scope.SelectedProductList[i].productUnitList.length;j++)					
					{
						if($scope.SelectedProductList[i].productUnitList[j].unit == $scope.SelectedProductList[i].defaultSalesUnit)
						{		
							$rootScope.mainSelectedProductUnit[$scope.SelectedProductList[i].code] = $scope.SelectedProductList[i].productUnitList[j];						
						}
					}			
				
			}
			addpropertyToList($scope.SelectedProductList,'validQuantity',true);		
			DataSharingService.setToLocalStorage("compareObjectList",$scope.SelectedProductList);
			DataSharingService.setObject("prevCompareState",$state.current.name);
			DataSharingService.setObject(CONST_mainCatPrevCompareView,$scope.currentView );	
			DataSharingService.setObject("prevViewLabelMain",$scope.ViewLabelName );	
/*
			$state.go('home.catalog.Compare');
			$scope.$root.$eval();*/
			
			/* Making compare template available */
			$scope.isCompare = true;
			
		}
	};

	$scope.showCatalogueItems = function(){
		$scope.SelectedProductList = [];
		DataSharingService.setToLocalStorage("compareObjectList",$scope.SelectedProductList);
		angular.forEach($scope.productList, function(a) {        
		    a.checked = false;
		});
		$scope.isCompare = false;
	};
	
	$scope.showHide = function(item){	  
		if(item === true){	return true;   }
		return false;
	};
	
	$scope.toggleNewSearch = function(){
		if($scope.filterNewSearch){
			$scope.filterNewSearch = false;
			$scope.filterCatalog = true;
		}else{
			if($scope.filterNewSearch){
				$scope.filterNewSearch = true;
			}else{
				$scope.filterNewSearch = true;
			}
		}
	};
	$scope.toggleCatalogue = function(){
		if($scope.filterCatalog){
			$scope.filterCatalog = false;
			$scope.filterNewSearch = true;
		}else{
			if($scope.filterNewSearch){
				$scope.filterCatalog = true;
			}else{
				$scope.filterNewSearch = false;
			}
		}
	};
	var getAdvanceParamListProductSearch = function(){
		var param = [];
		var itemIDSelected = null;
		var itemDesc = null;
			$scope.PageNumberAdvanceSearch = $scope.PageNumberAdvanceSearch + 1;
		if(!isEmptyString($scope.productCodeSearchText)){
			itemIDSelected = $scope.productCodeSearchText;
		}
		if(!isEmptyString($scope.productDescSearchText)){
			itemDesc = $scope.productDescSearchText; 
		}
		filterParamList = $scope.productSearchFilterList.getParamsListCatalogSearch($scope.mainCatalogId,$scope.PageNumberAdvanceSearch,$scope.mainCatalogElementId);
		param = filterParamList;
		return param;
	};
	$scope.advanceSearch = function(){
		//console.log("advanced search called...");
		$scope.PageNumberAdvanceSearch = 0;
		var paramForKeySet = [];
		paramForKeySet = ['type','ID'];
		paramList = getAdvanceParamListProductSearch();
		$scope.pageKey = Util.initPagination('catalogProductSearchUrl',paramForKeySet);
		$scope.errorMsg = null;
		getSearchResultforFirstTimeAdvanceSearch(paramList,true);
		$scope.$root.$eval();
	};
	
	$scope.removeFilter = function(filterItem){
		$scope.productSearchFilterList.removeSelectionProductSearch(filterItem);
		$scope.advanceSearch();
	};

	 var getSearchResultforFirstTimeAdvanceSearch = function(params,isAdvanceSearch){
	     setMessagesToNull();
	     if(Array.isArray(params) && params.length>0){
	     	$scope.advanceSearchParamsExist = true;
	     	var paramStringForSearch = "CatalogueProductListInputBean="+JSON.stringify(params);
	           // $scope.pageKey = Util.initPagination('solrSearchResultUrl',params);
	           //  Util.appendItemsToList(["PageNo",1], params);
	            ProdSearchService.getCatNewSearchResult(paramStringForSearch).then(function(data){
	            	if(data.productList==null || data.productList.length == 0){
						Util.stopLoadingPageData(pageKey);
						setMessages(data);
					}else{
						setUIData(data,true);
					}
	                if(isAdvanceSearch && !isNull(data.CatalogueProductListInputBean) && data.CatalogueProductListInputBean.length>0){
	                	$scope.freeSearchObj.text="";
	                }
	            });
	     }
	};	
	var setMessagesToNull = function(){
		$scope.infoMsg = null;
		$scope.errorMsg = null;
	};

	//console.log("catalogue sorting-- Current Sorting Selection : " + $scope.Selection);
	 $scope.Sorting = function(column,orderBy) {
		 	if($scope.SelectedProductList.length>1){
		 		$scope.SelectedProductList = [];
		 		angular.forEach($scope.productList, function(a) {        
				    a.checked = false;
				});
		 	}
			var	param = $rootScope.getSortingParam(column,orderBy);	
			var requestParams = [];
			if($scope.advanceSearchParamsExist){
				//requestParams = getAdvanceParamListProductSearch();
				requestParams = $scope.productSearchFilterList.getParamsListCatalogSearch($scope.mainCatalogId,$scope.PageNumberAdvanceSearch,$scope.mainCatalogElementId);
				requestParams[0]["SORT_BY"]= column;
				requestParams[0]["ORDER_BY"]= orderBy;
				var paramStringForSearch = "CatalogueProductListInputBean="+JSON.stringify(requestParams);
				 ProdSearchService.getCatNewSearchResult(paramStringForSearch).then(function(data){
		            	if(data.productList==null || data.productList.length == 0){
							Util.stopLoadingPageData(pageKey);
						}else{
							setUIData(data,true);
						}
		                if(isAdvanceSearch && !isNull(data.CatalogueProductListInputBean) && data.CatalogueProductListInputBean.length>0){
		                	$scope.freeSearchObj.text="";
		                }
		            });
			}else{
				if((!isNull($stateParams.ELEMENT_ID))&&($stateParams.ELEMENT_ID !="")){
					requestParams = ["CAT_ID",$scope.mainCatalogId,"ELEMENT_ID",$scope.mainCatalogElementId];
				}else{
					requestParams = ["CAT_ID",$scope.mainCatalogId];
				}
				if(param.length > 0){
					for(var i=0;i<param.length;i++) { requestParams.push(param[i]); }
				}
				//pageKey = Util.initPagination('catalogProductListUrl',JSON.stringify(requestParams)); 
				Util.getDataFromServer("catalogProductListUrl",requestParams).then(function(data){		 	
					setUIData(data,true);
				});
			}
	};
	 
	 
		/*$scope.Sorting = function(column,orderBy) {
			var	param = $rootScope.getSortingParam(column,orderBy);		
			var	requestParams =[];
			
			if((!isNull($stateParams.ELEMENT_ID))&&($stateParams.ELEMENT_ID !="")){
				requestParams = ["CAT_ID",$scope.mainCatalogId,"ELEMENT_ID",$scope.mainCatalogElementId];
			}else{
				requestParams = ["CAT_ID",$scope.mainCatalogId];
			}
			
						
			if(param.length > 0)
				{
					for(var i=0;i<param.length;i++) { requestParams.push(param[i]); }
				}
			
			pageKey = Util.initPagination('catalogProductListUrl',requestParams); 
			Util.getDataFromServer('catalogProductListUrl',requestParams ).then(function(data){		 	
				setUIData(data,true);
			});
		};*/
		
		$scope.onUnitChangeMainCat= function(itemCode,unitCode,oldCatalogItem){
			$rootScope.getAndUpdateCatalogItem(itemCode,unitCode,oldCatalogItem);
		};


		catMenuCtrlViewInit();

}]);