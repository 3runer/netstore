
catalougeControllers.controller('invoiceLineDetailCtrl', ['$rootScope','$scope', '$http','$log', 'DataSharingService','InvoiceService', 'Util','$state',
                                                          function ($rootScope,$scope, $http,$log,DataSharingService,InvoiceService,Util,$state) {

	var invoiceLineDetailCtrlInit = function(){
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
	};
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	//	$log.log(" invoiceLineDetailCtrl history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	var initController = function(){
		//var invoiceLine = DataSharingService.getObject("invoiceLine_clickedDetail");
		var invoiceLine  = DataSharingService.getFromSessionStorage("invoiceLine_clickedDetail");
		//var invoice = DataSharingService.getObject("invoice_clickedDetail");
		var invoice = DataSharingService.getFromSessionStorage("invoice_clickedDetail");

		if( invoiceLine !=null){
			$scope.invoiceLine = invoiceLine;
			$scope.invoiceData = invoice;
			//console.log("invoice line detail "+ JSON.stringify(invoiceLine));
		//	$log.log("invoice line detail requestParams"+ JSON.stringify(requestParams));
		};
	};
	invoiceLineDetailCtrlInit();
}]);