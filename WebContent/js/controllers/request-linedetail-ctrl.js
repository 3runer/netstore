catalougeControllers.controller('requestLineDetailCtrl', ['$rootScope','$scope', '$http','$log', 'DataSharingService','RequestSearchService', 'Util','$state',
                                                            function ($rootScope,$scope, $http,$log,DataSharingService,RequestSearchService,Util,$state) {

	var requestLineDetailCtrlInit = function(){
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
	};
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	//	$log.log(" requestLineDetailCtrl history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	var initController = function(){
		var requestParams = [];
		$scope.dataFound = false;
		$scope.resultMsg ="";
		$scope.errorMessage = "";

		//var requestLine = DataSharingService.getObject("requestLine_clickedDetail");
		var requestLine = DataSharingService.getFromSessionStorage("requestLine_clickedDetail");
//		$scope.requestNumber = DataSharingService.getObject("requestNumber");
		//$scope.requestDetails = DataSharingService.getObject("requestDetail");
		$scope.requestDetails = DataSharingService.getFromSessionStorage("requestDetail");

		if( requestLine !=null){
			$scope.requestLine = requestLine;
			
			//console.log("requestLine line detail "+ JSON.stringify(requestLine));
			//console.log("requestLine line detail requestParams"+ JSON.stringify(requestParams));
		};

	};
	requestLineDetailCtrlInit();

}]);