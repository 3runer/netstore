catalougeControllers.controller('promotionCatlogCtrl', ['PromotionalItemService','ShoppingCartService','NewConfigService','$rootScope','$scope','$stateParams', '$attrs', '$http','$log','$filter','$state','DataSharingService','Util', 
                                                function (PromotionalItemService,ShoppingCartService,NewConfigService,$rootScope,$scope,$stateParams,$attrs, $http,$log,$filter,$state,DataSharingService,Util) {		
	
	var promotionCatlogCtrlInit = function(){
		//console.log("######################promotionCatlogCtrlInit#############################");
		/*var lc = DataSharingService.getFromLocalStorage("languageChanged");
		if(lc == 1) {
			pcControllerInit();
			DataSharingService.setToLocalStorage("languageChanged", 0);
			return;
		}*/
		$scope.cartLinesUpdates=true;
		if(historyManager.isBackAction && !isNull($scope.PromotionCatalog) && !isNull($scope.PromotionCatalog.productList)){
			historyManager.loadScopeWithCache($scope);
			if($scope.PromotionCatalog != undefined){
			$scope.initBuyObject($scope.PromotionCatalog.productList);
			}
			$scope.$root.$eval();
		}else{
			pcControllerInit();
			
		}
	};
	
	function pcControllerInit() {
		if(!isNull($rootScope.initCommitsPromise) && !isNull($rootScope.initCommitsPromise.then)){
			$rootScope.initCommitsPromise.then(function(){
				$scope.cartLinesUpdates=false;
				initPromotionController();
			});	
		}
	}
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
		//$log.log(" promotionCatlogCtrl history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	
	$scope.$on('$viewContentLoaded', function(event){ 
		$scope.ViewLabelName = 	Util.translate($state.current.labelKey);
		$scope.pageLabelName =  Util.translate($state.current.pageLabel);
		if(isEmptyString($scope.ViewLabelName)){
			$scope.ViewLabelName = Util.translate('CON_GRID_VIEW');
		}
		
	});
	
	$scope.$on('userLoggedIn', function(event){ 
		if($state.current.name=='home.bestoffer.Compare') {
			$scope.SelectedProductList = getProductsForCompare();
			$scope.updateCount = 0; 
			for(var i = 0; i < $scope.SelectedProductList.length; i++) {
				$rootScope.getProductsDetail($scope.SelectedProductList[i].code, $scope.updateBuy);
			}
		}
	});

	// ADM-010
	$scope.$watch('freeSearchObj.packPiece',
		function(){
			if(!isNull($scope.PromotionCatalog)){
				$rootScope.refreshUIforPackPiece($scope.freeSearchObj.packPiece, 
				$scope.PromotionCatalog.productList);
			}
		}
	);
	
	$scope.updateBuy = function(data) {
		for(var i = 0; i < $scope.SelectedProductList.length; i++) {
			if($scope.SelectedProductList[i].code == data.itemCode) {
				$scope.SelectedProductList[i].isBuyingAllowed = data.isBuyAllowed;
				break;
			}
		}
		$scope.updateCount++;
		if($scope.updateCount >= $scope.SelectedProductList.length) {
			$scope.compare();
		}
	}
	
	
	$scope.$watch('buyItemWatch',function() {
		if(($state.current.name==SHOPPING_CART || $state.current.name==PROMOTION_LIST_SHOPPING_CART || $state.current.name==PROMOTION_GRID_SHOPPING_CART) && $scope.cartLinesUpdates){
		initPromotionController();
		}
	});
	
	$scope.$watch('deleteCartItemWatch',function() {		
		if(($state.current.name==SHOPPING_CART || $state.current.name==PROMOTION_LIST_SHOPPING_CART || $state.current.name==PROMOTION_GRID_SHOPPING_CART) && $scope.cartLinesUpdates){
			initPromotionController();
		}
		$scope.cartLinesUpdates=true;
	});
	
	var initViewLabel = function() {
//		$scope.gridViewLabelName = Util.translate('CON_GRID_VIEW');
//		$scope.tableViewLabelName = Util.translate('CON_TABLE_VIEW');
		$scope.ViewLabelName =  Util.translate($state.current.labelKey);
		$scope.pageLabelName =  Util.translate($state.current.pageLabel);
//		if(isNull($scope.ViewLabelName)){
//			$scope.ViewLabelName = $scope.tableViewLabelName;
//		}
	};
	
	var initPromotionController = function()
	{
		//$scope.SelectedProductList = DataSharingService.getObject("compareCatalogObjectList");
		$scope.SelectedProductList = getProductsForCompare();
		if(isNull($scope.SelectedProductList)){
			$scope.SelectedProductList = [];
		}
		
		if($state.current.name=='home.bestoffer.Compare'){
			DataSharingService.setObject("compareCatalogObjectList",null);
		}
		var itemCode = DataSharingService.getObject("catalog_ProductCode");	
		if($state.current.name=='home.productDetail'){
			itemCode = decodeURIComponent($stateParams.ITEM_CODE);
		}
		if(historyManager.backStateForPromo==true){
			if((historyManager.cachedScopeForBackState!=null)||historyManager.cachedScopeForBackState!=undefined){
				historyManager.backStateForPromo = false;
				var itemCodeFromHistory = historyManager.cachedScopeForBackState.productItemCode;
				if(!isNull(itemCodeFromHistory)){
					itemCode = itemCodeFromHistory;
				}
			}
		}
		$scope.bestOfferPage = 1;
		$scope.mostPopularPage = 1;
		/*ADM-008*/$scope.alternativePage = 1;
		$scope.homeCataloguePageIndex =1;
		$scope.promotionalShowExtendInfo =$scope.ExtendedInfoOption2;
		$scope.youMayLikeShowExtendInfo =$scope.ExtendedInfoOption2;
		//$scope.showExtendInfo ='false';
		//console.log("model = "+$attrs.catalogviewname);
		$scope.filterOn = false;
		$scope.promoCatalogSort = new Sort();
		$scope.promoCatalogSort.ASC('code');
		//if(isCatalogFilterVisibleOnUI()){
			//makeCatalogSearchFilters();
		//}
		//$scope.sortColumn = "code";
		//$scope.orderType = "ASC";
		$rootScope.sortSelection = '';
		$scope.filterParam = [];		
		//var paramList = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.promoCatalogSort.orderType,$scope.promoCatalogSort.sortColumn);
		var pageType = getContainerPage();

		$scope.collapsed = true;
		$scope.filterPanelCollapsed=false;
		$scope.PromotionCatalog = new Catalog(itemCode,[],DataSharingService.getWebSettings());
		$scope.PromotionCatalog.setCatalogType($attrs.catalogviewname,pageType);			
		//$scope.requestParams = $scope.PromotionCatalog.getRequestParam();
		$scope.isProductListEmpty = true;	

		//$rootScope.sortSelection =  $scope.sortColumn+$scope.orderType;	
		$scope.requestParams = getAllPromotionRequestParams();
		$scope.requestParams.push("PageNo");
		$scope.requestParams.push(1);
		$scope.pageKey = null;
		$scope.paginationObj = $rootScope.getPaginationObject('getPromotionalItemsUrl' , $scope.requestParams, $scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		$scope.pageNo = $scope.paginationObj.pageNo;
		initViewLabel();
		getPromotionalItemData($scope.requestParams);
		itemCode = DataSharingService.getObject("catalog_ProductCode");
	};
	
	var setProductsForCompare = function(productList){
		DataSharingService.setObject($state.current.name +"compareCatalogObjectList",productList);
	};
	
	var getProductsForCompare = function(){
		var productList;
		productList = DataSharingService.getObject($stateParams.ID+"compareCatalogObjectList");
		return productList;
	};
	
	var isCatalogFilterVisibleOnUI = function()
	{
		var showFilter = false;
		var currentState = $state.current.name;		
		switch(currentState)
		{	
		case MOST_POPULAR_STATE :
		case BEST_OFFERS_STATE:
		/*ADM-008*/case ALTERNATIVES_STATE:
		case PROMOTION_GRID_BEST_OFFERS: 
		case PROMOTION_LIST_BEST_OFFERS: 
		case PROMOTION_LIST_MOST_POPULAR:
		case PROMOTION_GRID_MOST_POPULAR:
		/*ADM-008*/case ALTERNATIVES_LIST_STATE:
		/*ADM-008*/case ALTERNATIVES_GRID_STATE:
			showFilter = true; 
			break;
		default : 
			showFilter = false;
		}

		return showFilter;
	};
	
	
	var showPromotionalProducts = function()
	{
		var showPromotionalProductList = false;
	
		//var webSettings = DataSharingService.getWebSettings();
		if(webSettings != undefined){
		var IS_CROSS_SELL_ITEM = webSettings.isShowCrossSellItems;
		var IS_UP_SELL_ITEM = webSettings.isShowUpSellItems;
		var IS_PROMOTIONAL_ITEM = webSettings.isShowPromotionalItems;
		var IS_POPULAR_ITEM = webSettings.isShowMostPopularItems;
		//ADM-008
		var IS_ALTERNATIVES_ITEMS = true;
		if(IS_ALTERNATIVES_ITEMS){
			showPromotionalProductList = true;
		}
		//ADM-008 end
		if(IS_CROSS_SELL_ITEM){
			showPromotionalProductList = true;
		}
		if(IS_UP_SELL_ITEM){
			showPromotionalProductList = true;
		}
		if(IS_PROMOTIONAL_ITEM){
			showPromotionalProductList = true;
		}
		if(IS_POPULAR_ITEM){
			showPromotionalProductList = true;
		}
		}

		return showPromotionalProductList;
	};
	
	var getContainerPage = function()
	{	var page = "";		
		var currentState = $state.current.name;
		switch(currentState)
		{		
			case THANK_YOU :
			case PROMOTION_LIST_THANKYOU :
			case PROMOTION_GRID_THANKYOU :	
								page = "Catalogue";								
								break;
					
			case PROMOTION_LIST_SHOPPING_CART:
			case PROMOTION_GRID_SHOPPING_CART:
			case SHOPPING_CART:
					 								page = "ShoppingCart";
					 								break;
			case PROMOTION_LIST_PRODUCT_DETAIL:
			case PROMOTION_GRID_PRODUCT_DETAIL:
			case PRODUCT_DETAIL:
					 								page = "ProductDetails";
					 								break;
			case PROMOTION_LIST_REQ_SUBMIT_RECEIVED:
			case PROMOTION_GRID_REQ_SUBMIT_RECEIVED:
			case REQ_SUBMIT_RECEIVED:
					 								page = "RequestConf";
					 								break;					
			default :
													page = "Catalogue";
		};	
		return page; 
	};
	var getRequestParam = function(requestParam)
	{		
		var tempRequestParam = getPageParameters(getContainerPage());		
		Util.appendItemsToList(tempRequestParam,requestParam);		
		return requestParam;
	};
	
	var getPageParameters = function(page)
	{
		var param =[];
		if(!isEmptyString(page)){
				param = ['Page',page];
				if(page == 'ProductDetails'){
					   var itemCode = DataSharingService.getObject("catalog_ProductCode");	
					   param.push('ItemCode');						
					   param.push(itemCode);
					}		
				if(page == 'Catalogue')
					{
					 param = $scope.PromotionCatalog.getBestOfferParam();
					}
			}
		return param;		
	};
	

	var getPromotionalItemData = function(requestParams,isFilterRemoved)
	{		
		//console.log("@@@@@@@@@@@@@@@@@@@@@@@getPromotionalItemData@@@@@@@@@@@@@@@@@@@@@@@@@");
		var s = $.inArray( "Page", requestParams );
		var requestPage = "",isProcessRequest = true;
		if(s !==-1){
			requestPage = requestParams[s+1];
		}
		if(requestPage == "ShoppingCart"){
			if($scope.isLoggedIn()){
				isProcessRequest = true;
			}else{
				isProcessRequest = false;
			}
		}
		if(showPromotionalProducts())
		{
			if(isProcessRequest)
			PromotionalItemService.getPromotinalItems(requestParams).then(function(data){
				$rootScope.openLoader();
				if(!isNull(data) && !isNull(data.searchFilterList)){
					$scope.filterDetails = data.searchFilterList;
					if(isCatalogFilterVisibleOnUI()){
						makeCatalogSearchFilters();
					}
				}
				if(!isNull(data) && !isNull(data.productList)){
					if(isFilterRemoved){
						$scope.SelectedProductList=[];	
					}
					$scope.exportInPDF = Util.translate('CON_PROMOTIONAL_DATA_PDF', [$scope.pageLabelName]);	
					$scope.exportInExcel = Util.translate('CON_PROMOTIONAL_DATA_EXCEL', [$scope.pageLabelName]);
					$scope.exportInXML = Util.translate('CON_PROMOTIONAL_DATA_XML', [$scope.pageLabelName]);
					$scope.fileName = $scope.pageLabelName.replace(/ /g,'');
					$scope.initForScrollLoading();
					$scope.moreDataArrow = data.moreRecords;
					$scope.PromotionCatalog.setCatalog(data,false);					
					$scope.PromotionCatalog.productHeadingList = Util.getObjPropList(data.productList[0]);
					if($scope.PromotionCatalog.catalogType == "MOST_POPULAR"){
						$scope.moreDataMostPopular = data.moreRecords;
					//ADM-008
					//}else{
					//	$scope.moreDataBestOffer = data.moreRecords;
					}else if($scope.PromotionCatalog.catalogType == "BEST_OFFER"){
						$scope.moreDataBestOffer = data.moreRecords;
					}else{
						$scope.moreDataAlternatives = data.moreRecords;
					//ADM-008 end
					}
					if(!isNull(data.productList[0])){$scope.isProductListEmpty = false;}
					$scope.initBuyObject($scope.PromotionCatalog.productList );
					$rootScope.initCatalogTableHeaders();
					$rootScope.setCatalogTableHeaders($scope.PromotionCatalog.productList);
					$scope.catalogSlider = $scope.PromotionCatalog.productList;
					//$scope.productPropList = Util.getObjPropList(data.productList[0]);			
					DataSharingService.setObject("CATALOG_PRODUCT_LIST",$scope.PromotionCatalog.productList );
					$scope.homeCataloguePageIndex ++;
					if(!isNull($scope.$root)){
						$scope.$root.$eval();
					}
					checkAndLoadMoreItems(data.moreRecords);
				}else if(!isNull(data) && data.messageCode==111){
					if($state.current.name=='home.bestoffer' || $state.current.name=='home.popular'){
						showMsgNoObjFound();
					}
				}else if(!isNull(data) && data.messageCode==3232){
					if($state.current.name=='home.bestoffer' || $state.current.name=='home.popular'){
						showMsgNoObjFound(data.messageCode);
					}
				}else if(!isNull(data) && data.messageCode==3233){
					if($state.current.name=='home.bestoffer' || $state.current.name=='home.popular'){
						showMsgNoObjFound(data.messageCode);
					}
				}
				$rootScope.closeLoader();
				
			});
		}
	};
	
	
	

	var getPromotionalItemDataWithFilters = function(requestParams,isFilterRemoved)
	{		
		//console.log("@@@@@@@@@@@@@@@@@@@@@@@getPromotionalItemData@@@@@@@@@@@@@@@@@@@@@@@@@");
		var s = $.inArray( "Page", requestParams );
		var requestPage = "",isProcessRequest = true;
		if(s !==-1){
			requestPage = requestParams[s+1];
		}
		if(requestPage == "ShoppingCart"){
			if($scope.isLoggedIn()){
				isProcessRequest = true;
			}else{
				isProcessRequest = false;
			}
		}
		if(showPromotionalProducts())
		{
			if(isProcessRequest)
			PromotionalItemService.getPromotinalItems(requestParams).then(function(data){
				$rootScope.openLoader();
				
				if(!isNull(data) && !isNull(data.productList)){
					if(isFilterRemoved){
						$scope.SelectedProductList=[];	
					}
					$scope.exportInPDF = Util.translate('CON_PROMOTIONAL_DATA_PDF', [$scope.pageLabelName]);	
					$scope.exportInExcel = Util.translate('CON_PROMOTIONAL_DATA_EXCEL', [$scope.pageLabelName]);
					$scope.exportInXML = Util.translate('CON_PROMOTIONAL_DATA_XML', [$scope.pageLabelName]);
					$scope.fileName = $scope.pageLabelName.replace(/ /g,'');
					$scope.initForScrollLoading();
					$scope.moreDataArrow = data.moreRecords;
					$scope.PromotionCatalog.setCatalog(data,false);					
					$scope.PromotionCatalog.productHeadingList = Util.getObjPropList(data.productList[0]);
					if(!isNull(data.productList[0])){$scope.isProductListEmpty = false;}
					$scope.initBuyObject($scope.PromotionCatalog.productList );
					$rootScope.initCatalogTableHeaders();
					$rootScope.setCatalogTableHeaders($scope.PromotionCatalog.productList);
					$scope.catalogSlider = $scope.PromotionCatalog.productList;
					//$scope.productPropList = Util.getObjPropList(data.productList[0]);			
					DataSharingService.setObject("CATALOG_PRODUCT_LIST",$scope.PromotionCatalog.productList );
					$scope.homeCataloguePageIndex ++;
					if(!isNull($scope.$root)){
						$scope.$root.$eval();
					}
					checkAndLoadMoreItems(data.moreRecords);
				}else if(!isNull(data) && data.messageCode==111){
					if($state.current.name=='home.bestoffer' || $state.current.name=='home.popular'){
						showMsgNoObjFound();
					}
				}else if(!isNull(data) && data.messageCode==3232){
					if($state.current.name=='home.bestoffer' || $state.current.name=='home.popular'){
						showMsgNoObjFound(data.messageCode);
					}
				}else if(!isNull(data) && data.messageCode==3233){
					if($state.current.name=='home.bestoffer' || $state.current.name=='home.popular'){
						showMsgNoObjFound(data.messageCode);
					}
				}
				$rootScope.closeLoader();
				
			});
		}
	};
	
	
	
	
	
	
	
	
	
	var getComparePage = function()
	{
		var comparePage = "";
		var currentView = $state.current.name;		
		switch(currentView)	{	
			case SHOPPING_CART :
			case PROMOTION_LIST_SHOPPING_CART :
			case PROMOTION_GRID_SHOPPING_CART :
								comparePage = SHOPPING_CART_COMPARE;
								break;
			case REQ_SUBMIT_RECEIVED :
			case PROMOTION_LIST_REQ_SUBMIT_RECEIVED :
			case PROMOTION_GRID_REQ_SUBMIT_RECEIVED :
								comparePage = REQ_SUBMIT_RECEIVED_COMPARE;
								break;
			case PRODUCT_DETAIL :
			case PROMOTION_LIST_PRODUCT_DETAIL :
			case PROMOTION_GRID_PRODUCT_DETAIL :
								comparePage = PRODUCT_DETAIL_COMPARE;
								break;
			case THANK_YOU :
			case PROMOTION_LIST_THANKYOU :
			case PROMOTION_GRID_THANKYOU :	
								comparePage = THANK_YOU_COMPARE;
								break;
			default:
								comparePage = "";
	
		}
		return comparePage;
	};
	
	
	$scope.initBuyObject = function(list){
		$scope.buyObj = new Buy(list);
		$scope.buyObj.setPropNameItemCode('code');
		$scope.buyObj.setPropNameOrdered('quantity');
		$scope.buyObj.setPropNameUnit('salesUnit');
		$scope.buyObj.setPropNameisBuyAllowed('isBuyingAllowed');
		/* ADM-010 */$rootScope.refreshUIforPackPiece($scope.freeSearchObj.packPiece,
				list);
	};
	
	$scope.buyItems = function(item,buyObj){
		$rootScope.buyMultipleItems(item,$scope.buyObj);
	};
	
	$scope.addMultipleItemsToCurrentList = function(item){
		$rootScope.addMultipleItemsToList(item,$scope.buyObj);
	};
	
	$rootScope.catalogLoadMore = function(){
		if($scope.moreDataArrow == true){
		/*if(!isNull($scope.PromotionCatalog) && ($scope.PromotionCatalog.catalogType == "MOST_POPULAR")) {
			return;
		}*/
		if(showPromotionalProducts())
		{
			$scope.requestParams = getAllPromotionRequestParams();
			$scope.paginationObj = $rootScope.getPaginationObject('getPromotionalItemsUrl' , $scope.requestParams, $scope.pageKey);
			$scope.pageKey = $scope.paginationObj.objKey;
			//console.log("catalogLoadMore() - called, isPaginationBlocked = "+ Util.isPaginationBlocked($scope.pageKey).toString());
			if(!Util.isPaginationBlocked($scope.pageKey)){				
				$rootScope.openBottomLoader();
				//var	requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.promoCatalogSort.orderType,$scope.promoCatalogSort.sortColumn);
				//requestParams = getRequestParam(requestParams);
				//Util.getPageData('homeCatelogUrl',requestParams ).then(function(data){
				Util.getPageData('getPromotionalItemsUrl',$scope.requestParams ).then(function(data){
					$rootScope.closeBottomLoader();
					if((data== null) || data.productList==null || data.productList.length == 0){
						Util.stopLoadingPageData($scope.pageKey);
					}else{						
						$scope.isProductListEmpty = false;
						$scope.PromotionCatalog.setCatalog(data,true);						
						$scope.initBuyObject($scope.PromotionCatalog.productList);
						DataSharingService.setObject("CATALOG_PRODUCT_LIST",$scope.PromotionCatalog.productList );						
						$scope.$root.$eval();
						$scope.moreDataArrow = data.moreRecords;
						checkAndLoadMoreItems(data.moreRecords);
					}
				});
			}
		}
		}

	};
	
	$rootScope.catalogLoadMorePromotion = function(){
		/*if($scope.PromotionCatalog.catalogType == "MOST_POPULAR") {
			return;
		}*/
		if(showPromotionalProducts() && $scope.moreDataArrow == true)
		{
			$scope.requestParams = getAllPromotionRequestParams();
			$scope.requestParams.push("PageNo");
			
			$scope.paginationObj = $rootScope.getPaginationObject('getPromotionalItemsUrl' , $scope.requestParams, $scope.pageKey);
			$scope.pageKey = $scope.paginationObj.objKey;
			$scope.pageNo = $scope.pageNo +1;
			$scope.requestParams.push($scope.pageNo);
			//console.log("catalogLoadMore() - called, isPaginationBlocked = "+ Util.isPaginationBlocked($scope.pageKey).toString());
				$rootScope.openBottomLoader();
				//var	requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.promoCatalogSort.orderType,$scope.promoCatalogSort.sortColumn);
				//requestParams = getRequestParam(requestParams);
				//Util.getPageData('homeCatelogUrl',requestParams ).then(function(data){
				PromotionalItemService.getPromotinalItems($scope.requestParams).then(function(data){
					$rootScope.closeBottomLoader();
					if((data== null) || data.productList==null || data.productList.length == 0){
						Util.stopLoadingPageData($scope.pageKey);
					}else{						
						$scope.isProductListEmpty = false;
						$scope.PromotionCatalog.setCatalog(data,true);						
						$scope.initBuyObject($scope.PromotionCatalog.productList);
						DataSharingService.setObject("CATALOG_PRODUCT_LIST",$scope.PromotionCatalog.productList );						
						$scope.$root.$eval();
						$scope.moreDataArrow = data.moreRecords;
					}
				});
			
		}

	};

	
	
	$scope.changeCatalogView = function(){
		
		var currentState = $state.current.name;
		
		switch(currentState){		
		
			case PROMOTION_LIST_THANKYOU :
										$state.go(PROMOTION_GRID_THANKYOU);
										//$scope.ViewLabelName = Util.translate('CON_TABLE_VIEW');
										$scope.isList  = false;		
										break;
			case THANK_YOU:									
			case PROMOTION_GRID_THANKYOU :			
										$state.go(PROMOTION_LIST_THANKYOU);
										//$scope.ViewLabelName = Util.translate('CON_GRID_VIEW');
										$scope.isList  = true;		
					 					break;
			case PROMOTION_LIST_PRODUCT_DETAIL:
										$state.go(PROMOTION_GRID_PRODUCT_DETAIL);
										//$scope.ViewLabelName = Util.translate('CON_TABLE_VIEW');
										$scope.isList  = false;		
										break;
			case PRODUCT_DETAIL:
			case PROMOTION_GRID_PRODUCT_DETAIL:
										$state.go(PROMOTION_LIST_PRODUCT_DETAIL);
										//$scope.ViewLabelName = Util.translate('CON_GRID_VIEW');
										$scope.isList  = true;		
										break;								
			
			case PROMOTION_LIST_SHOPPING_CART:
										$state.go(PROMOTION_GRID_SHOPPING_CART);
										//$scope.ViewLabelName = Util.translate('CON_TABLE_VIEW');
										$scope.isList  = false;		
										break;
			case SHOPPING_CART:
			case PROMOTION_GRID_SHOPPING_CART:
										$state.go(PROMOTION_LIST_SHOPPING_CART);
										//$scope.ViewLabelName = Util.translate('CON_GRID_VIEW');
										$scope.isList  = true;		
										break;
			case PROMOTION_LIST_REQ_SUBMIT_RECEIVED:
										$state.go(PROMOTION_GRID_REQ_SUBMIT_RECEIVED);
										//$scope.ViewLabelName = Util.translate('CON_TABLE_VIEW');
										$scope.isList  = true;		
										break;
			case REQ_SUBMIT_RECEIVED:
			case PROMOTION_GRID_REQ_SUBMIT_RECEIVED:
										$state.go(PROMOTION_LIST_REQ_SUBMIT_RECEIVED);
										//$scope.ViewLabelName = Util.translate('CON_GRID_VIEW');
										$scope.isList  = true;		
										break;
			case PROMOTION_LIST_BEST_OFFERS:
										$state.go(PROMOTION_GRID_BEST_OFFERS);
										$scope.isList  = false;		
										break;

			case BEST_OFFERS_STATE:
			case PROMOTION_GRID_BEST_OFFERS:
										$state.go(PROMOTION_LIST_BEST_OFFERS);
										//$scope.ViewLabelName = Util.translate('CON_GRID_VIEW');
										$scope.isList  = true;		
										break;
			case PROMOTION_LIST_MOST_POPULAR:
										$state.go(PROMOTION_GRID_MOST_POPULAR);
										$scope.isList  = false;		
										break;

			case MOST_POPULAR_STATE:
			case PROMOTION_GRID_MOST_POPULAR:
									$state.go(PROMOTION_LIST_MOST_POPULAR);									
									$scope.isList  = true;		
									break;
			//ADM-008
			case ALTERNATIVES_LIST_STATE:
				$state.go(ALTERNATIVES_GRID_STATE);
				$scope.isList  = false;		
				break;
				
			case ALTERNATIVES_STATE:
			case ALTERNATIVES_GRID_STATE:
				$state.go(ALTERNATIVES_LIST_STATE);									
				$scope.isList  = true;		
				break;
			//ADM-008 end
						
										
			default :
									//$scope.ViewLabelName = Util.translate('CON_TABLE_VIEW');
		};
		
	};

	
	$scope.initForScrollLoading = function(){
		$scope.blockDataRequest = false;
		$scope.stopLoading = false;
	};

	$scope.isLoadAllowed= function(){
		if (!$scope.blockDataRequest && !$scope.stopLoading){
			return true;
		}else{
			return false;
		}
	};
	$scope.gridViewBuyClicked = function(itemObj){
		if(!itemObj.validQuantity || isNull(itemObj.buyQuantity) || ""==itemObj.buyQuantity){
			itemObj.validQuantity = false;
			itemObj['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
		}else{
		itemObj.isBuyingAllowed = false;
		//console.log("buyClicked: Itemcode = " +itemObj.code + " quantity = " + itemObj.buyQuantity + " salesUnit = " + itemObj.selectedUnit);
		$rootScope.addItemToCart(itemObj.code,itemObj.buyQuantity,itemObj.selectedUnit,itemObj,"buyQuantity");
		}
	};
	
	
	// sorting 
	$scope.Sorting = function(column,orderBy) {		
		//var curSel = column+orderBy+'';				
		//$rootScope.sortSelection = curSel;	
		//$scope.orderType = orderBy;
		//$scope.sortColumn = column;
		if($scope.SelectedProductList.length>1){
	 		$scope.SelectedProductList = [];
	 		angular.forEach($scope.productList, function(a) {        
			    a.checked = false;
			});
	 	}
		$scope.promoCatalogSort.sorting(column,orderBy);
		$scope.requestParams = getAllPromotionRequestParams();
		//var paramsList = 	$rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.promoCatalogSort.orderType,$scope.promoCatalogSort.sortColumn);
		//$scope.requestParams = getRequestParam(paramsList);
		$scope.pageKey  = Util.initPagination('getPromotionalItemsUrl',$scope.requestParams);		
		getPromotionalItemData($scope.requestParams);
		
	};
	
	$scope.getAndUpdateCatalogItem = function(itemCode,unitCode,oldCatalogItem){
		if(!isEmptyString(itemCode) && !isEmptyString(unitCode) ){
			var prams = ["ITEM_CODE",itemCode,"UNIT_CODE",unitCode];		
			PromotionalItemService.getSingleItemDetails(prams,false).then(function(data){			
				if(!isNull(data) && Array.isArray(data.productList) && data.productList.length > 0){
					newCatalogItem = data.productList[0];
					if(angular.isDefined(oldCatalogItem.checked) && oldCatalogItem.checked!=null){
						newCatalogItem['checked']=oldCatalogItem.checked;
					}
					copyObjectWithNull(newCatalogItem,oldCatalogItem);
					setCatalogExtendedData([oldCatalogItem]);
					oldCatalogItem.salesUnit=unitCode;
					oldCatalogItem.selectedUnit = unitCode;		
					oldCatalogItem.selectedUnitObject = getSelectedSalesUnit(unitCode,newCatalogItem.salesUnitsDesc);

				}				
			});
		}
	};
	
	$scope.onUnitChange= function(itemCode,unitCode,oldCatalogItem){
			$scope.getAndUpdateCatalogItem(itemCode,unitCode,oldCatalogItem);		
	};
	

		

	$rootScope.addToCart = function (itemCode,quantity,salesUnit){
		if(Util.isNotNull(itemCode) && Util.isNotNull(quantity) && quantity >0 && Util.isNotNull(salesUnit)) {
			var params =  ["itemCode",itemCode,"itemQty",quantity,"unitCode",salesUnit];
			ShoppingCartService.addToCart(params);
		}
	};


	$scope.addItems = function(newList){
		for(var i = 0; i<newList.length ; i++){
			$scope.productList.push(newList[i]);
		}
	};

	$scope.addItemToList = function(item)
    {
    	item['quantity'] = item.buyQuantity;
    	$rootScope.addItemToCurrentList(item,"buyQuantity");    	
    };
    
	$scope.SelectedProduct = function(productList)
	{
		$scope.selectionMsg = "";
		$scope.SelectedProductList = $filter('filter')(productList, {checked: true});
		if($scope.SelectedProductList.length <=10)
		{
			$scope.selectionMsg = " Total selected Items :  "+ $scope.SelectedProductList.length;
			$scope.Comparison = true;
		}
		else
		{
			$scope.selectionMsg = Util.translate('CON_MAX_PRODUCT_COMPARISION');
			$scope.Comparison = false;
		}
		//DataSharingService.setObject("compareCatalogObjectList",$scope.SelectedProductList);
		setProductsForCompare($scope.SelectedProductList);
		//console.log("addCompareListItem length -  "+	$scope.SelectedProductList.length);
	};
	$scope.RemoveProduct = function()
	{
		var curPageThankYou = $state.current.name;
		$scope.SelectedProductList = $filter('filter')($scope.SelectedProductList, {checked: true});
		//DataSharingService.setObject("compareCatalogObjectList",$scope.SelectedProductList);
		setProductsForCompare($scope.SelectedProductList);
		if($scope.SelectedProductList.length > 1)
		{
			$scope.selectionMsg = " Total selected Items :  "+ $scope.SelectedProductList.length;
		}else if(curPageThankYou == "home.goThankYou.Compare"){
			$rootScope.goHomeCatalog();
		}
		else
		{
			var prevState = DataSharingService.getObject("prevCatalogCompareState");
			//var prevViewLabel = DataSharingService.getObject("prevViewLabel");
			$scope.selectionMsg = "";	
			$state.go(prevState, {"ITEM_CODE":DataSharingService.getObject("catalog_ProductCode")});							
//			if(prevViewLabel == $scope.gridViewLabelName){
//				$scope.ViewLabelName = $scope.gridViewLabelName;
//				$scope.isList  = false;
//			}			
//			else{			
//				$scope.ViewLabelName = $scope.tableViewLabelName;
//				$scope.isList  = true;
//			}
		}
	};
	$scope.compare = function()
	{
		var isThankYouPage = $state.current.name;
		var isOpenLoginDlg=$rootScope.isSignOnRequired("CON_PRODUCT_COMPARE",'home');
		if(isOpenLoginDlg==true){
			return;
		}
		if(($scope.SelectedProductList.length > 1) && ($scope.Comparison == true))
		{
			$rootScope.SelectedProductUnit = {};
			for(var i= 0; i< $scope.SelectedProductList.length ; i++)
			{
				for(var j= 0;j<$scope.SelectedProductList[i].productUnitList.length;j++)					
					{
						if($scope.SelectedProductList[i].productUnitList[j].unit == $scope.SelectedProductList[i].defaultSalesUnit)
						{		
							$rootScope.SelectedProductUnit[$scope.SelectedProductList[i].code] = $scope.SelectedProductList[i].productUnitList[j];						
						}
					}			
				
			}
			
			//DataSharingService.setObject("compareCatalogObjectList",$scope.SelectedProductList);
			setProductsForCompare($scope.SelectedProductList);
			DataSharingService.setObject("prevCatalogCompareState",$state.current.name);
		//	DataSharingService.setObject("prevCompareView",$scope.currentView );	
		//	DataSharingService.setObject("prevViewLabel",$scope.ViewLabelName );			
			addpropertyToList($scope.SelectedProductList,'validQuantity',true);
			var paramObj = {ID:$state.current.name};
			if(isThankYouPage == "home.goThankYou"){
				$state.go('home.goThankYou.Compare',paramObj);
			}else{
			$state.go('home.bestoffer.Compare',paramObj);
			}
			$scope.$root.$eval();
		}
	};
	$scope.viewAllBestOffer = function()
	{
		$scope.loadNewFiltersPromo("bestOffer");
		//	$state.go(BEST_OFFERS_STATE);
		
	};
	$scope.viewAllMostPopular = function()
	{
		$scope.loadNewFiltersPromo("mostPopular");
		//$state.go(MOST_POPULAR_STATE);
	};
	
	
	//ADM-008
	$scope.viewAllAlternatives = function(){
		$scope.loadNewFiltersPromo("alternatives");
	};
	
	$scope.loadNewFiltersPromo = function(promo){
		var langCode = NewConfigService.getLanguageCode();
		var prams = ["LanguageCode",langCode];
		if(promo == "bestOffer"){
			prams = ["LanguageCode",langCode,"Text","PromotionCampaign"];
		//ADM-008
		}else if(promo == "mostPopular"){
			prams = ["LanguageCode",langCode,"Text","MostPopular"];
		}else{
			prams = ["LanguageCode",langCode,"Text","AltertivesItems"];
		//ADM-008 end
		}
		
		if(promo == "bestOffer"){
			$state.go(BEST_OFFERS_STATE);
		//ADM-008
		}else if(promo == "mostPopular"){
			$state.go(MOST_POPULAR_STATE);
		}else{
			$state.go(ALTERNATIVES_STATE);
		//ADM-008 end			
		}
		
		
	};

	/*
	// Catalog Filter Starts
	var getPromotionalCatalogFilters = function(){	
		Util.getDataFromServer('mainCatelogFilterUrl',[]).then(function(data) {
			//$scope.FilterData = data;
			$scope.FilterList = data.filterBeans;
			$scope.getFilterSettings(data.filterBeans);
			//console.log("FilterListCtrl Data- items fetched : " +data.filterBeans.length);
		});
	};
	
	
		// to get filter configuration setting from "filterConfig.json"  
	$scope.getFilterSettings = function(filterList)
	{
		var filterConfigData = PromotionalItemService.getFilterConfiguration();
		//console.log("promotionalCatalogCtrl getFilterSettings--  " +JSON.stringify(filterConfigData));			 
		$scope.FilterList =  $rootScope.getSearchFilterSettings(filterList,filterConfigData);
		if( $scope.FilterList != null )
		{
			for(var i= 0; i<$scope.FilterList.length; i++)
			{
				for(var j=0; j <$rootScope.productfilterDetailSettings.length ;j++)
				{
					if($scope.FilterList[i].filterName == $rootScope.productfilterDetailSettings[j].filterName)
					{
						$scope.FilterList[i].filterValueList = $rootScope.productfilterDetailSettings[j].searchFilterList;
					}
				}

			}
		}
	};
	
	$scope.submitMainCatlogFilter = function()
	{
		$scope.filterOn = true;
		var keyList = $scope.filterFormInfo;
		
		$scope.filterParam =  $rootScope.setParamKeyList(keyList); 
		var filterParam =  getParamKeyList($scope.filterOn,$scope.filterParam,$scope.mainCatalogId,$scope.mainCatalogElementId,$scope.orderType,$scope.sortColumn); 
		
		//console.log("Main Catalog Filter Info :  " + filterParam);
		pageKey = Util.initPagination('mainCatalogProductListUrl',filterParam);
		MainCatalogService.getMainCatalogProducts(filterParam).then(function(data){
			//$rootScope.openLoader();
			if(data.productList==null || data.productList.length == 0){
				$scope.dataFound = false;				
				$scope.filterMsg =  Util.translate('MSG_NO_OBJECTS_FOR_SELECTION',null);				
				var icon = '<a class="OrderInformation-icon" shref="" title="'+$scope.filterMsg+'"></a>';				
				$scope.filterMsg = icon + $scope.filterMsg;
				
				$scope.recordFound = false;
				
			}
			else
				{
				$scope.filterMsg = "";
				setUIData(data,true);
			}
		});
	};
	
	//Catalog Filter End
	*/
	$rootScope.bestOfferFilterChange = function(lang){
		makeCatalogSearchFilters(lang);
	};
	// Promotional catalog Filter Starts
	var makeCatalogSearchFilters = function(lang){
		var params =[];
		if(!isNull(lang)){
			params = ["LanguageCode",lang.code];
			$scope.pageLabelName =  Util.translate($state.current.pageLabel);
		}
	
		var finalFilterList = new FilterList();
		var staticFilterConfig = PromotionalItemService.getFilterConfiguration();
		var filterDataList = $scope.filterDetails; 
		var dynamicFilterConfig = null;
		PromotionalItemService.getCatalogFilterList(params).then(function(data){
				dynamicFilterConfig = data.filterBeans;
				finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig,'searchFilterList');
				$scope.catalogSearchFilterList = finalFilterList;
				//$log.log("makeCatalogSearchFilters : " + JSON.stringify(finalFilterList));
		});
	};
	
	var getFilterParams = function(){
		var filterParams = [];
		if($scope.filterOn){
			if(!isNull($scope.catalogSearchFilterList)){
				filterParams = $scope.catalogSearchFilterList.getParamsList();
			}
		}
		return filterParams;
	};
	
	var getAllPromotionRequestParams  = function(){
		var params = [];// getFilterParams();
		var currentPage = $state.current.name;
		$scope.promoCatalogSort.addSortParams(params);
		appendItemsToList($scope.PromotionCatalog.getRequestParam(),params);
		appendItemsToList(getFilterParams(),params);
		if(currentPage == "home" || currentPage == "home.homeCatalogGrid" 
			/*ADM-008*/|| currentPage == "home.productDetail"){
			var appParam = ["currentPage","home"];
			appendItemsToList(appParam,params);
		}
		return params;
	};
	
	//applying filter 
	$scope.submitPromotionFilter = function()
	{
		$scope.filterOn = true;	
		$scope.SelectedProductList=[];
//		var filterParam  = $scope.catalogSearchFilterList.getParamsList();		
//		var catalogParam = $scope.PromotionCatalog.getRequestParam();
//		Util.appendItemsToList(catalogParam,filterParam);	
		var filterParam = getAllPromotionRequestParams();
		$scope.pageKey = Util.initPagination('getPromotionalItemsUrl',filterParam);
		$scope.paginationObj = DataSharingService.getObject($scope.pageKey );	
		getPromotionalItemDataWithFilters(filterParam);
	};
	
	var setUIData = function(data,isFirstPageData){
		if(!isNull(data)){
			$scope.moreDataArrow = data.moreRecords;
			if(isFirstPageData){
				$scope.PromotionCatalog.setCatalog(data,false);	
				if(!isNull(data.productList) && data.productList.length > 0 ){
					$scope.isProductListEmpty = false;
				}
				$scope.catalogSlider = $scope.PromotionCatalog.productList;
				DataSharingService.setObject("CATALOG_PRODUCT_LIST",$scope.PromotionCatalog.productList );
			}else{
				$scope.catalogSlider.appendItems(data.productList);
				$scope.catalogSlider.nextGroup();
			}
			/* ADM-010 */$rootScope.refreshUIforPackPiece($scope.freeSearchObj.packPiece,
					$scope.PromotionCatalog.productList);
		}else{
			$scope.catalogSlider.appendItems(null);
		}
	};
	
	var getSliderPromotionalItem = function(requestParams,page){
		var pageKey = $scope.pageKey;
		$scope.paginationObj = DataSharingService.getObject(pageKey );
		if(!Util.isPaginationBlocked(pageKey))
		{
			Util.getPageinationData('getPromotionalItemsUrl',requestParams,page ).then(function(data){
				if($scope.paginationObj.getPageNo()==1){
					setUIData(data,true);
				}else{
					setUIData(data,false);
				}
			});
		}
	};
	
	$scope.sliderPreviousPage = function(){	
		//var params = 	$scope.PromotionCatalog.getRequestParam();
		//getSliderPromotionalItem(params,PrevPage);	
		$scope.catalogSlider.previousGroup();
	};
	 
	$scope.sliderNextPage = function(){	
		/*if($scope.PromotionCatalog.catalogType == "MOST_POPULAR") {
			$scope.catalogSlider.nextGroup();
			return;
		}*/
		if($scope.catalogSlider.isLoadNextPage()){
			var params = getAllPromotionRequestParams();
			getSliderPromotionalItem(params,NextPage);	
		}else{
			$scope.catalogSlider.nextGroup();
		}
	};
	
	$scope.sliderNextPageBestOffer = function(){	
		var abc = $scope.moreDataBestOffer;
		$scope.bestOfferPage = $scope.bestOfferPage+1;
		$scope.moreDataBestOfferLeftArrow = true;
		$scope.requestParams = getAllPromotionRequestParams();
		$scope.requestParams.push("PageNo");
		$scope.requestParams.push($scope.bestOfferPage);
		PromotionalItemService.getPromotinalItems($scope.requestParams).then(function(data){
			if(!isNull(data) && !isNull(data.productList)){
				$scope.moreDataBestOffer = data.moreRecords;
				$scope.PromotionCatalog.setCatalog(data,false);					
				$scope.catalogSlider = $scope.PromotionCatalog.productList;
			}
		});
	};
	
	
	$scope.sliderPreviousPageBestOffer = function(){	
		$scope.bestOfferPage = $scope.bestOfferPage - 1;
		if($scope.bestOfferPage ==  1){
		$scope.moreDataBestOfferLeftArrow = false;;
		}
		$scope.requestParams = getAllPromotionRequestParams();
		$scope.requestParams.push("PageNo");
		$scope.requestParams.push($scope.bestOfferPage);
		PromotionalItemService.getPromotinalItems($scope.requestParams).then(function(data){
			if(!isNull(data) && !isNull(data.productList)){
				$scope.moreDataBestOffer = data.moreRecords;
				$scope.PromotionCatalog.setCatalog(data,false);					
				$scope.catalogSlider = $scope.PromotionCatalog.productList;
			}
		});
	};
	
	$scope.sliderNextPageMostPopular = function(){	
		$scope.moreDataMostPopularleftArrow = true;
		$scope.mostPopularPage = $scope.mostPopularPage+1;
		$scope.requestParams = getAllPromotionRequestParams();
		$scope.requestParams.push("PageNo");
		$scope.requestParams.push($scope.mostPopularPage);
		PromotionalItemService.getPromotinalItems($scope.requestParams).then(function(data){
			if(!isNull(data) && !isNull(data.productList)){
				$scope.moreDataMostPopular = data.moreRecords;
				$scope.PromotionCatalog.setCatalog(data,false);					
				$scope.catalogSlider = $scope.PromotionCatalog.productList;
			}
		});
	};
	
	$scope.sliderPreviousPageMostPopular = function(){	
		$scope.mostPopularPage = $scope.mostPopularPage - 1;
		if($scope.mostPopularPage ==  1){
			$scope.moreDataMostPopularleftArrow  = false;;
		}
		$scope.requestParams = getAllPromotionRequestParams();
		$scope.requestParams.push("PageNo");
		$scope.requestParams.push($scope.mostPopularPage);
		PromotionalItemService.getPromotinalItems($scope.requestParams).then(function(data){
			if(!isNull(data) && !isNull(data.productList)){
				$scope.moreDataMostPopular = data.moreRecords;
				$scope.PromotionCatalog.setCatalog(data,false);					
				$scope.catalogSlider = $scope.PromotionCatalog.productList;
			}
		});
	};
	
	//ADM-008
	$scope.sliderNextPageAlternatives = function(){	
		$scope.alternativePage = $scope.alternativePage + 1;
		$scope.moreDataAlternativesleftArrow = true;
		$scope.requestParams = getAllPromotionRequestParams();
		$scope.requestParams.push("PageNo");
		$scope.requestParams.push($scope.alternativePage);
		PromotionalItemService.getPromotinalItems($scope.requestParams).then(function(data){
			if(!isNull(data) && !isNull(data.productList)){
				$scope.moreDataAlternatives = data.moreRecords;
				$scope.PromotionCatalog.setCatalog(data,false);					
				$scope.catalogSlider = $scope.PromotionCatalog.productList;
			}
		});
	};
	
	//ADM-008
	$scope.sliderPreviousPageAlternatives = function(){	
		$scope.alternativePage = $scope.alternativePage - 1;
		if($scope.alternativePage ==  1){
			$scope.moreDataAlternativesleftArrow = false;
		}
		$scope.requestParams = getAllPromotionRequestParams();
		$scope.requestParams.push("PageNo");
		$scope.requestParams.push($scope.alternativePage);
		PromotionalItemService.getPromotinalItems($scope.requestParams).then(function(data){
			if(!isNull(data) && !isNull(data.productList)){
				$scope.moreDataAlternatives = data.moreRecords;
				$scope.PromotionCatalog.setCatalog(data,false);					
				$scope.catalogSlider = $scope.PromotionCatalog.productList;
			}
		});
	};
	
	$scope.removePromotionFilter = function(filterItem)
	{
		$scope.catalogSearchFilterList.removeSelection(filterItem);
//		var filterParam  = $scope.catalogSearchFilterList.getParamsList();		
//		var catalogParam = $scope.PromotionCatalog.getRequestParam();
//		Util.appendItemsToList(catalogParam,filterParam);
		var filterParam = getAllPromotionRequestParams();
		$scope.pageKey = Util.initPagination('getPromotionalItemsUrl',filterParam);
		$scope.paginationObj = DataSharingService.getObject($scope.pageKey );	
		getPromotionalItemData(filterParam,true);
	};
	
	//Promotional catalog Filter Ends
	
	$scope.exportPromotionalCatalogTableAsPDF=function(){
		var promotionalCatalogTable = setPromotionalCatalogDataforExport();
		var data = promotionalCatalogTable.getHTML();
		var title = $scope.pageLabelName;
		var fileName;
		if(title==''){
			title = "Promotional Items";
			fileName = "PromotionalItems_"+getCurrentDateTimeStr()+".pdf";
		}else{
			fileName = $scope.fileName+"_"+getCurrentDateTimeStr()+".pdf";
		}
		savePdfFromHtml(data,title,fileName);
	};
	
	$scope.exportPromotionalCatalogTableAsExcel=function(){
		var promotionalCatalogTable = setPromotionalCatalogDataforExport();
		var data = promotionalCatalogTable.getDataWithHeaders();
		var sheetName = $scope.fileName;
		var fileName;
		if(sheetName==''){
			sheetName = "PromotionalItems";
			fileName = "PromotionalItems_"+getCurrentDateTimeStr()+".xlsx";
		}else{
			fileName = $scope.fileName+"_"+getCurrentDateTimeStr()+".xlsx";
		}
		saveExcel(data,sheetName,fileName);
	};
	
	$scope.exportPromotionalCatalogTableAsXML=function(){
		var promotionalCatalogTable = setPromotionalCatalogDataforExport('XML');
		var fileName;
		var entityName;
		if($scope.fileName==''){
			fileName="PromotionalItems";
			entityName="PROMOTIONAL_ITEM";
		}else{
			fileName=$scope.fileName;
			entityName=$scope.fileName.toUpperCase()+"_ITEM";
		}
		promotionalCatalogTable.setRootElementName(fileName.toUpperCase());
		promotionalCatalogTable.setEntityName(entityName);
		var data = promotionalCatalogTable.getXML();
		var file = fileName+"_"+getCurrentDateTimeStr()+".xml";
		saveXML(data,file);
	};
	
	var setPromotionalCatalogDataforExport = function(target){
		var promotionalCatalogTable = new Table();
		if(isEqualStrings(target,"XML")){
			promotionalCatalogTable.addHeader('PRODUCT');
			promotionalCatalogTable.addHeader('DESCRIPTION');
			if($rootScope.webSettings.isShowPrice){
				promotionalCatalogTable.addHeader('ACTUAL_PRICE');
				promotionalCatalogTable.addHeader('UNIT');
				promotionalCatalogTable.addHeader('DISCOUNT_PRICE');
				promotionalCatalogTable.addHeader('DISCOUNT_PERCENTAGE');
			}
		}else{
			promotionalCatalogTable.addHeader(Util.translate('COH_ITEM'));
			promotionalCatalogTable.addHeader(Util.translate('COH_DESCRIPTION'));
			if($rootScope.webSettings.isShowPrice){
				promotionalCatalogTable.addHeader(Util.translate('COH_ACTUAL_PRICE')+" "+ webSettings.currencyCode);
				promotionalCatalogTable.addHeader(Util.translate('COH_UNIT'));
				promotionalCatalogTable.addHeader(Util.translate('COH_DISCOUNT_PRICE')+" "+ webSettings.currencyCode);
				promotionalCatalogTable.addHeader(Util.translate('CON_DISCOUNT_%'));
			}
		}
		//set row data
		data = $scope.PromotionCatalog.productList;

		if(!isNull(data)){
			for(var i=0;i<data.length;i++){
				var item = data[i];
				var row = [];
				row.push(item.code); 
				row.push(item.description);
				if($rootScope.webSettings.isShowPrice){
					row.push(item.actualPrice);
					row.push(item.salesUnit);
					row.push(item.discountPrice);
					row.push(item.discountPercentageText);
				}
				promotionalCatalogTable.addRow(row);
			}
		}else{
			var row = [];			
			for(var i=0;i<promotionalCatalogTable.headers.length;i++){
				row.push("");
			}
			promotionalCatalogTable.addRow(row);
		}
		
		return promotionalCatalogTable;
	};
	
	/*
	 * checks and loads more items based on need
	 * hasMoreItems: does more items exists
	 */
	function checkAndLoadMoreItems(hasMoreItems) {
		if(!hasVerticalScroll() && hasMoreItems) {
			$rootScope.catalogLoadMore();
		}
	}
	
	promotionCatlogCtrlInit();
}]);