//Shopping Cart controller

catalougeControllers.controller('shoppingCartCtrl', ['$rootScope','$scope', '$http','$q','$log','$stateParams', '$window','DataSharingService','ShoppingCartService', 'OrderService','Util','$state',
                                                     function ($rootScope,$scope, $http, $q, $log, $stateParams, $window, DataSharingService,ShoppingCartService,OrderService,Util,$state) {
	var requestParams = [];
	var updateParams = [];
	$rootScope.cartUnit = {};
	$rootScope.lineDate = {};
	$rootScope.shipmentMark = {};
	$rootScope.lineText = {};
	$scope.cartOptions = "cart-options";
	$rootScope.orderLineEmpty = false;
	$scope.validDateShoppinglist=true;
	$scope.validDateAllowUpdate = true;
	$scope.validDateAllowUpdateOrderLevel = true;
	$scope.isInterruptedCartEmpty = false;
	$scope.thankYouObj=DataSharingService.getFromSessionStorage("thankYouObj");	
	$scope.CartDeliveryInfo = DataSharingService.getFromSessionStorage("CartDeliveryInfo");
	$scope.isOrderPlaced = DataSharingService.getFromSessionStorage("isOrderPlaced");
	$rootScope.cartMsg = DataSharingService.getFromSessionStorage("cartMsg");
	$rootScope.cartStatus = DataSharingService.getFromSessionStorage("cartStatus");
	$scope.callState = $state.current.name;
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	});
	
	
	
	if($scope.callState == 'home.shoppingcart.interruptedCart'){
		
			ShoppingCartService.getInterruptedCart([]).then(function(data){
				$scope.isInterruptedCartEmpty = false;
				if(Util.isNotNull(data)) {
					if(data.cartList !== undefined){
						$scope.isInterruptedCartEmpty = false;
					}else{
						$scope.isInterruptedCartEmpty = true;
					}
					$scope.interruptedCarts = data.cartList;
					$scope.$root.$eval();
					// $state.go('home.shoppingcart.interruptedCart');
				}else{
					// Todo
					$scope.isInterruptedCartEmpty = true;
				}
			});
		
	}
	if($rootScope.currentOrder_lineCount==0){
		$scope.isCartEmpty = true;
	}else{
		$scope.isCartEmpty = false;
	}
	

	
	var createThankYouObj = function (){
		// {PAY_MODE}/{MESSAGE_CODE}/{ORDER_NUM}
		// this logic is only for online payment
		if(!isNull($stateParams.PAY_MODE) && isEqualStrings("ONLINE",$stateParams.PAY_MODE,true)){
			getPaymentOrderStatus ($stateParams.ORDER_NUM);
			$scope.getShoppingCart();
		}
	};
	
	
	
	var getPaymentOrderStatus = function (tempOrderNum){
		var params = ["tempOrderNumber",tempOrderNum];
		var deferred = $q.defer();
		if($scope.isLoggedIn()){
		Util.getDataFromServer('getPaymentOrderStatusUrl',params,true).then(function(data){
			if(!isNull(data)){
				 $scope.thankYouObj = new ThankYouPage(data);
			}
			deferred.resolve(data);	
		});
		}
	};
	
	$scope.onUnitChange= function(itemCode,unitCode,oldCatalogItem,orderLine){
		oldCatalogItem.unitCode = unitCode;
		oldCatalogItem.salesUnit=unitCode;
		oldCatalogItem.selectedUnit = oldCatalogItem.salesUnit;
		$rootScope.checkValidQuantity(orderLine,$rootScope.lineQuantity[$rootScope.getId('quantity',orderLine.ivOrderLine.lineNumber)]);
	};
	
	$rootScope.initCommitsPromise.then(function(){
		createThankYouObj();
	});
	
	var showDeletedOrderLineMsg = function(orderLineList){
		if(!isNull(orderLineList) && orderLineList.length > 0){
			$rootScope.openDeletedOrderLineMsgDlg("",orderLineList);	
		}
				
	};
	
	
	$scope.changeFormatToLocale = function(value){
		var newVal = value;
		if(!isNull($rootScope.webSettings.localeCode) && $rootScope.webSettings.localeCode !=""){
			if(!isNull($rootScope.webSettings.allowDecimalsForQuantities) && $rootScope.webSettings.allowDecimalsForQuantities == true){
			var langCode = $rootScope.webSettings.localeCode;
			langCode = langCode.replace("_","-");
			newVal = newVal.toLocaleString(langCode);
		}}
		return newVal;
	};
	
	$scope.filterValue = function($event){
		if($event.keyCode == 32){
            $event.preventDefault();
        }
	};
	
	$scope.validateDateRangeFormatShoppingCart = function(dateStr,secondMsg)	{
		// //console.log(filter);
		if(!isNull(dateStr)){
		dateStr = dateStr.trim();}
		var dateFormat = $rootScope.webSettings.defaultDateFormat;
		// var isFilterTextEmpty = isFilterRangeEmpty(filter);
		this.validDateShoppinglist = true;
		if(isNull(secondMsg)){
			secondMsg = true;}
		
		if(Util.isNotNull(dateStr)){
// var isValidDate = isValidDateFormat(dateStr,dateFormat);
			var dateValidator =new DateValidator(dateFormat);			
			var isValidDate =dateValidator.isValidDate(dateStr);
			if(!isValidDate){
				this.validDateShoppinglist = false;	
				$scope.validDateAllowUpdate = false;
					
			}else{
				this.validDateShoppinglist = true;
				$scope.validDateAllowUpdate = true;
			}
			if(dateStr==""||dateStr==null){
				this.validDateShoppinglist = false;
				$scope.validDateAllowUpdate = false;
			}
			
		}
		
	};
	
	
	
	$scope.validateDateRangeFormatOrderLevel = function(dateStr)	{
		// //console.log(filter);
		dateStr = dateStr.trim();
		var dateFormat = $rootScope.webSettings.defaultDateFormat;
		// var isFilterTextEmpty = isFilterRangeEmpty(filter);
		
		if(Util.isNotNull(dateStr)){
// var isValidDate = isValidDateFormat(dateStr,dateFormat);
			var dateValidator =new DateValidator(dateFormat);			
			var isValidDate =dateValidator.isValidDate(dateStr);
			if(!isValidDate){
				$scope.validDateAllowUpdateOrderLevel = false;
					
			}else{
				$scope.validDateAllowUpdateOrderLevel = true;
			}
			if(dateStr==""||dateStr==null){
				$scope.validDateAllowUpdateOrderLevel = true;
			}
			
		}
		
	};
	
	$scope.populateShoppingCartData = function (data){
		// data.messageCode = 3103;
		// var data = responseData.temporaryOrderData;
		if(!Util.isNotNull(data)){
			$scope.dataFound = false;
			$scope.shoppingCartLoadMsg = " ";
			// console.log("shoppingCartCtrl -- no data fetched");
		} else  {
			switch(data.messageCode) {
				case 3888:
					var itemCode = Util.translate('CON_SELECT_MANNER_OF_TRANSPORT');			
					$rootScope.openEnterMOT("" , [itemCode]);
					$scope.showDeliveryPaymentInfo();
					break;
					
				case 3103:
					var itemCode = Util.translate('MSG_NEGATIVE_QUANTITY');			
					$rootScope.openMsgDlg("" , [itemCode]);
					break;
					
				case 3104:
					var itemCode = Util.translate('MSG_QUANTITY_EXCEEDS_LIMIT');			
					$rootScope.openMsgDlg("" , [itemCode]);
					break;
					
				case 2110:
					var itemCode = Util.translate('INPUT_PARAMETER_NOT_VALID');			
					$rootScope.openMsgDlg("" , [itemCode]);
					break;
				
				case 0:
					var itemCode = Util.translate('MSG_INVALID_QUANTITY');			
					$rootScope.openMsgDlg("" , [itemCode]);
					break;
				
				default:
					//console.log("shoppingCartCtrl messageCode -- "+data.messageCode);
					$rootScope.populateBubbleCart(data);
					$scope.tempOrderData = data;
					if($scope.tempOrderData.displayMOTPopUpCode == 3888){
						$rootScope.reviewOrder = true;
						var msg = "";
						var itemCode = Util.translate('CON_SELECT_MANNER_OF_TRANSPORT');			
						$rootScope.openEnterMOT(msg , [itemCode]);
						// $scope.showDeliveryPaymentInfo();
					}else{
					
					if(!isNull($scope.tempOrderData.nsOrderValueInfo)){
						if(isNull($scope.tempOrderData.nsOrderValueInfo.emDisCount)){
							$scope.tempOrderData.nsOrderValueInfo.emDisCount="0";
						}
					}
					$scope.currentOrder = data.currentOrder;
					$scope.orderLines =  data.orderLines;
					// ADM-010
					// updateQuantityTextBox(data.orderLines);
					if(!$rootScope.webSettings.allowSelectUnitOfMeasure){
						updateQuantityTextBox(data.orderLines);
					}else{
						refreshUIforPackPiece(); 
					}
					// ADM-010 end
					var orderLine = Util.getObjPropList(data.orderLines);
					if(Util.isNotNull(orderLine) && orderLine.length > 0 ) {
						$rootScope.orderLineEmpty = false;
					}else{
						$rootScope.orderLineEmpty = true;
					}
					$scope.isCartEmpty = isEmptyObject(data.orderLines);
					
					//$log.log("Util.getObjPropList(null) " +JSON.stringify(Util.getObjPropList(null)));		
					//$log.log("Util.getObjPropList({}) " +JSON.stringify(Util.getObjPropList({})));	
					//$log.log("Util.getObjPropList(data.orderLines) " +JSON.stringify(Util.getObjPropList(data.orderLines)));	
		
					$scope.dataFound = true;
					$scope.shoppingCartLoadMsg = "";
					showDeletedOrderLineMsg(data.listDeleteItemWithZero);
					break;
					}
			}
		}
		
	};
	
	$scope.isShowThisField = function(value){
		if(value == undefined){
			return false;
		}else{
			return true;
		}
		
	};
	
	$scope.showInvoiceFreeTextIcon = function(noInvoiceFeeText){
		if(noInvoiceFeeText !== undefined && noInvoiceFeeText.length >=1){
			return true;
		}else{
			return false;
		}
	}
	
	$scope.goReviewOrder= function(){
		// ADM-002 Begin
		// $state.go('home.reviewOrder');
		$rootScope.validateCart();
		// ADM-002 end
	};
	
	$scope.NEWexportReviewOrderTableAsPDF=function(){
		window.print();
	}
	

	
	$scope.callOrderDetail=function(orderNumber){
		if( orderNumber !=null){
			requestParams = ["OrderNumber",orderNumber];
			OrderService.getOrderDetailList(requestParams).then(function(data){
				if(data.orderLineList==null || data.orderLineList.length == 0){
					$scope.errorMessage = data.errorMessage;		
				}else{
					$scope.orderDetailsData=data;
				}
			});		

		} 
		
	}
	
	if($scope.thankYouObj != null && $scope.thankYouObj.orderNumber!=null){
		$scope.callOrderDetail($scope.thankYouObj.orderNumber);
	}
	
	$scope.getShoppingCart = function()	{
		
		if($scope.isLoggedIn()){
			//console.log("@@@@@@@@@@@@@@@@@@@@@@@@@getShopping cart with login @@@@@@@@@@@@@@@@@");
		ShoppingCartService.getTemporaryOrderData(requestParams).then(function(data){	
			// $http.get('./json/catalog/shopping_cart.json').success(function(data){
			$scope.populateShoppingCartData(data);
			addpropertyToList(data.orderLines,'validQuantity',true);
		});
		}else{
			//console.log("@@@@@@@@@@@@@@@@@@@@@@@@@getShopping cart without login @@@@@@@@@@@@@@@@@");
			showLoader = true;
			if(isNull(showLoader)){
				showLoader = true;
			}
			if(showLoader){ $rootScope.openLoader(); }
			$rootScope.openInnerLoader();
			var id = setInterval(function(){
				if($rootScope.dataUrlArray.length === 0){
					clearInterval(id);
					if(!$rootScope.isShoppingCartPageEnabled){
						var translatedMsgList = [];
						translatedMsgList.push(Util.translate('TXT_CF_041'));
						var msg = '';
						if(isNull($rootScope.isSessionExpireMsgPopUpOpen) || $rootScope.isSessionExpireMsgPopUpOpen){
							$rootScope.isSessionExpireMsgPopUpOpen = true;
							var dlg = $rootScope.openMsgDlg(msg,translatedMsgList);
							dlg.result.then(function () {
								$rootScope.isSessionExpireMsgPopUpOpen=false;
								$rootScope.reloadHomePage();
							}, function () {
								$rootScope.isSessionExpireMsgPopUpOpen=false;
								$rootScope.reloadHomePage();
							});
						}
						$rootScope.closeLoader();
						$rootScope.closeInnerLoader();
						return;
					}
					var needToLogin = $rootScope.isSignOnRequired("CON_SHOPPINGCART",'home.shoppingcart',true);
					//console.log("############## setInterval ##################");
					if(!needToLogin){
						$state.go('home.shoppingcart'); 	
					}
					if($scope.isLoggedIn()){
						//console.log("@@@@@@@@@@@@@@@@@@@@@@@@@getShopping cart with login from bookmark @@@@@@@@@@@@@@@@@");
						ShoppingCartService.getTemporaryOrderData(requestParams).then(function(data){	
							$scope.populateShoppingCartData(data);
							addpropertyToList(data.orderLines,'validQuantity',true);
						});
					}
					$rootScope.closeLoader();
					$rootScope.closeInnerLoader();
				}
			}, 1000);
		}
	};
	
	$scope.recalculateLineCart = function(lineCart){
		if(lineCart.validQuantity){
			var orderQty =  $rootScope.lineQuantity[$rootScope.getId('quantity',lineCart.ivOrderLine.lineNumber)];
			if($rootScope.webSettings.allowDecimalsForQuantities == true){
				orderQty = orderQty.replace(",",".");
			}
			updateParams = ["temporaryOrderNumber",lineCart.ivOrderLine.temporaryOrderNumber,"lineNumber",lineCart.ivOrderLine.lineNumber,"itemQuantity",orderQty ];
			// ADM-010
			if($rootScope.webSettings.allowSelectUnitOfMeasure){
				addParam("unitCode", $scope.freeSearchObj.packPiece);
			}
			// ADM-010 end
			ShoppingCartService.updateLineCart(updateParams).then(function(data){
				// $log.log("updated cart : " +JSON.stringify(data));
				if(Util.isNotNull(data)){
					// ADM-002
					// $scope.populateShoppingCartData(data);
					if(!isNull(data.messageCode) && (data.messageCode == 9001 ||
							data.messageCode == 9003)){
						$scope.showNotAvailableDlg(data, lineCart.ivItem, orderQty);
					}else{
						$scope.populateShoppingCartData(data);
					}
					// ADM-002 end
				}					
			});
		}else{
			var msg = Util.translate('CON_RECALCULATE_ERROR_HEADER');
			var itemCode = Util.translate('CON_RECALCULATE_ERROR_TEXT');			
			$rootScope.openQuantityZeroDlg(msg , [itemCode]);
			
		}
			
	};

	// ADM-002
	$scope.showNotAvailableDlg = function(data, itemObj, quantity){
		var selectedUnit = itemObj.selectedUnit;
		 if(isNull(selectedUnit)){
			 selectedUnit = itemObj.defaultSalesUnit;
		 }
		var notAvailableObj = {
				messageText: "",
				itemWebText: itemObj.itemWebText, 
				imageURL: itemObj.imageUrl, 
				description: itemObj.description,
				minsanCode: itemObj.minsanCode,
				selectedUnit: selectedUnit,
				actualPrice: itemObj.actualPrice,
				discountPercentage: itemObj.discountPercentage,
				discountPrice: itemObj.discountPrice,
				availableQuantity: data.availableQty,
				quantity: quantity,
				salesPackageQty: data.salesPackageQty,
				unitCode: data.unitCode};
		if(data.messageCode==9001){
			notAvailableObj.messageText = Util.translate('MSG_ITEM_IS_NOT_TEMPORARY_AVAILABLE');
		}else if(data.messageCode==9003){
			notAvailableObj.messageText = Util.translate('MSG_ITEM_IS_NOT_COMPLETELY_AVAILABLE');
		}
		$rootScope.openNotAvailableDlg("", notAvailableObj);
	}

	$scope.reInitializeUnit = function(unitSelected){
		$scope.selectedUnitObject = unitSelected;
	};
	
	$scope.cancelCart = function(){
		
		$scope.getShoppingCart();
	};
	
	// ShoppingCartService.updateLineCart
	$scope.updateCart = function(lineCart,selectedUnit){
		// ADM-010
		// $scope.makeUpdateParam(lineCart,selectedUnit);
		if(!$rootScope.webSettings.allowSelectUnitOfMeasure){
			$scope.makeUpdateParam(lineCart,selectedUnit);
		}else{
			$scope.makeUpdateParam(lineCart,$scope.freeSearchObj.packPiece);
		}
		// ADM-010 end
		ShoppingCartService.updateLineCart(updateParams).then(function(data){
			// console.log("updated cart : " +JSON.stringify(data));
			if(data.messageCode != null || data.messageCode != undefined){
				// ADM-002
				//var messageText = $rootScope.getBuyStatusMsg(data.messageCode.toString());
				// var dlg = $rootScope.openMsgDlg(messageText);
				// dlg.result.then(function () {
					
				// }, function () {
				// $scope.getShoppingCart();
				// });
				if(!isNull(data.messageCode) && (data.messageCode == 9001 ||
						data.messageCode == 9003)){
					var orderQty =  $rootScope.lineQuantity[$rootScope.getId('quantity',lineCart.ivOrderLine.lineNumber)];
					if($rootScope.webSettings.allowDecimalsForQuantities == true){
						orderQty = orderQty.replace(",",".");
					}
					$scope.showNotAvailableDlg(data, lineCart.ivItem, orderQty);
				}else{
					var messageText = $rootScope.getBuyStatusMsg(data.messageCode.toString());
					var dlg = $rootScope.openMsgDlg(messageText);
					dlg.result.then(function () {
						
					}, function () {
						$scope.getShoppingCart();
					});
				}
				// ADM-002 end
			}else{
				if(Util.isNotNull(data)){			
					$scope.populateShoppingCartData(data);
					// window.location.reload();
				}
			}
		});
	};
	
	
	$scope.editCart = function(lineCart){
		var requestParam = ["OrderNumber",lineCart.orderNumber];
		ShoppingCartService.editCart(requestParam).then(function(data){
			if(Util.isNotNull(data)){			
				$scope.populateShoppingCartData(data);
			}
			//$log.log("interrupted cart edit successfully  : " +JSON.stringify(data));
			$state.go('home.shoppingcart');
		});
	};
	
	$scope.makeUpdateParam = function(lineCart,selectedUnit){
		var orderQty =  $rootScope.lineQuantity[$rootScope.getId('quantity',lineCart.ivOrderLine.lineNumber)];
		if($rootScope.webSettings.allowDecimalsForQuantities == true){
		orderQty = orderQty.replace(",",".");
		}
		//var deliveryDate =  $rootScope.lineDate[$rootScope.getId('del_Date',lineCart.ivOrderLine.lineNumber)];
		var deliveryDate = lineCart.ivEnteredValues.Date;
		var shipmentMark =  $rootScope.shipmentMark[$rootScope.getId('shipMark',lineCart.ivOrderLine.lineNumber)];
		var lineText =  lineCart.ivEnteredValues.Text;
		updateParams = ["temporaryOrderNumber",lineCart.ivOrderLine.temporaryOrderNumber,"lineNumber",lineCart.ivOrderLine.lineNumber,"itemQuantity",orderQty,"unitCode",selectedUnit];
		addParam("shipmentMark",shipmentMark);
		addParam("lineText",lineText);
		addParam("deliveryDate",deliveryDate);
		// console.log("cart update Clicked: updateParams = "+updateParams);
	};
	
	var addParam = function(paramKey,paramValue){
		if(Util.isNotNull(paramValue) &&  paramValue.length > 0){
			updateParams.push(paramKey);
			updateParams.push(paramValue);
		}
	};
	$scope.deleteCartItem = function(lineCart){
		var msg = Util.translate('CON_DELETE_CART_LINE',null);
		var mode = $rootScope.openConfirmDlg(msg , [lineCart.ivItem.code+' - '+lineCart.ivItem.description]);
		mode.result.then(function () {
			// console.log('Modal Ok clicked');
			$scope.deleteCart(lineCart);
		}, function () {
			// console.log('Modal dismissed at: ' + new Date());
		});
	};
	
	$scope.deleteCart = function(lineCart){
		var requestParam = ["temporaryOrderNumber",lineCart.ivOrderLine.temporaryOrderNumber,"lineNumber",lineCart.ivOrderLine.lineNumber];
		ShoppingCartService.deleteLineCart(requestParam).then(function(data){
			// console.log("deleted cart : " +JSON.stringify(data));
			if(Util.isNotNull(data)){			
				$scope.populateShoppingCartData(data);
				$scope.deleteCartItemWatch = "Item Deleted : "+getCurrentDateTimeStr();
			}	
		});
	};
	//
	var setDisplayAddressNameList = function(addressNameList){
		if(!isNull(addressNameList)){
			for(var i=0;i<$rootScope.customerDeliveryAddressList.length;i++){
				address= $rootScope.customerDeliveryAddressList[i];
				address['displayAddress'] = addressNameList[i];
			}
		}
	};
	
	$scope.setDisplayAddress = function(addressNameList){
		var address;
		// var displayAddress;
		setDisplayAddressNameList(addressNameList);		
		address= $rootScope.customerDeliveryAddressList[$scope.defaultAddressIndex];
		$scope.delivInfo = address;
		if($rootScope.SelectedDeliveryAddress == null){
			$rootScope.SelectedDeliveryAddress = address;
			var countryAddress =$scope.getCountry(address.countryCode);
			DataSharingService.setObject("countryName",countryAddress);
			DataSharingService.setObject("Delivery_Address",address);
			DataSharingService.setObject("delivPaymentInfo",$scope.delivInfo);
		}		
	};
	$scope.getCountry = function(countryCode){
		// console.log("countryCode : " +countryCode);
		var countryName = "";
		for(var i=0;i<$scope.countryList.length;i++){
			if(countryCode == $scope.countryList[i].code){
				countryName =  $scope.countryList[i].description;
			}
		}
		return countryName;
	};

	$scope.onAddressChange = function(){
		selectedAddress = $scope.delivInfo;
		var countryAddress =$scope.getCountry(selectedAddress.countryCode);
		DataSharingService.setObject("Delivery_Address",selectedAddress);
		DataSharingService.setObject("countryName",countryAddress);
		//console.log("on Delivery Address Change,  delivInfo: " +JSON.stringify($scope.delivInfo));
		// console.log("countryAddress : " +countryAddress);
	};
	$scope.submitAdrressData = function(){
		
	};
	
	$scope.showDeliveryPaymentInfo = function(){
		if($rootScope.isShowDisableMenuItem('CON_ORDER_REFERENCE')){
			DataSharingService.setToSessionStorage("CartDeliveryInfo",null);
			var needToLogin = $rootScope.isSignOnRequired('CON_ORDER_REFERENCE','home.shoppingcart.information',true);
			if(!needToLogin){
				$state.go('home.shoppingcart.information');
			}
			// getDeliveryPaymentDetails();
		}
	};
	
	$rootScope.showDeliveryPaymentInfoPopUp = function(){
		DataSharingService.setToSessionStorage("CartDeliveryInfo",null);
		var needToLogin = $rootScope.isSignOnRequired('CON_ORDER_REFERENCE','home.shoppingcart.information',true);
		if(!needToLogin){
			$state.go('home.shoppingcart.information');
		}
	};
// var getDeliveryPaymentDetails = function(){
// ShoppingCartService.getDeliveryInfo([]).then(function(data){
// if(!isNull(data)){
// $scope.CartDeliveryInfo = new CartDelivery(data);
// $state.go('home.shoppingcart.information');
// }
// });
// };
	
	$scope.applyPromotionCode = function (promoCode){
		$scope.errorMsg ="";
		if(Util.isNotNull(promoCode)){
			var param = ["promotionCode",promoCode];
			ShoppingCartService.applyPromotionCode(param).then(function(data){
				//console.log("applyPromotion response  : " +JSON.stringify(data));
				if(Util.isNotNull(data) && Util.isNotNull(data.messageCode) && (data.messageCode==3002)){
					$scope.errorMsg = Util.translate('CON_INVALID_PROMOTION_CODE',null);
					//console.log("Promotion Code could not apply, messageCode=" +data.messageCode);
				}else if(Util.isNotNull(data)) {
					$scope.populateShoppingCartData(data);
				}
			});
		}
	};
	
	$scope.getDateStr = function (obj){
		var str = "";
		if(Util.isNotNull(obj)){
			str = obj.year +"-"+obj.month+"-"+obj.dayOfMonth;
		}
		return str;
	};
	
	$scope.deleteCurrentCart_Click = function (){
		var data = $scope.tempOrderData;
		var title = Util.translate('CONF_DELETE_CART',null);
		var cust = Util.translate('CON_CUSTOMER',null); 
		cust = cust + ": " + data.customer.code + " " + data.customer.name;
		var ordDate = Util.translate('COH_ORDER_DATE',null);  
		ordDate = ordDate +": "+ $scope.getDateStr(data.currentOrder.orderDate);
		var lines  = Util.translate('COH_LINES',null); 
		lines  = lines + ": "+ data.currentOrder.lineCount;
		var orderValue = Util.translate('COH_ORDER_VALUE',null); 
		orderValue = orderValue +": " + data.nsOrderValueInfo.emOrderTotal + " "+data.customer.currencyCode;
			var dlg = $rootScope.openConfirmDlg(title , [cust,ordDate,lines,orderValue]);
			dlg.result.then(function () {
				// console.log('Modal Ok clicked');
				$scope.deleteCurrentCart();
			}, function () {
				// console.log('Modal dismissed at: ' + new Date());
			});
	};
	
	$scope.deleteCurrentCart = function (){
		ShoppingCartService.deleteCurrentCart([]).then(function(data){
			if(Util.isNotNull(data)) {
				if(angular.isDefined(data.messageCode) && data.messageCode=='3008'){
					var msg = '';
					var translatedMsgList = [];
					translatedMsgList.push(Util.translate('MSG_ERROR_IN_DELETING_NEW_CART'));
					$rootScope.openMsgDlg(msg,translatedMsgList);
				}else{
					$rootScope.setBubbleCartToZero();
					$scope.populateShoppingCartData(data);
				}
			}
		});
	};
	
	
	$scope.createNewCart = function (){
		ShoppingCartService.createNewCart([]).then(function(data){
			if(Util.isNotNull(data)) {
				data['cartMsg'] = "";
				
				if(angular.isDefined(data.messageCode) && data.messageCode=='3009'){
					var msg = '';
					var translatedMsgList = [];
					translatedMsgList.push(Util.translate('MSG_ERROR_IN_CREATING_NEW_CART'));
					$rootScope.openMsgDlg(msg,translatedMsgList);
					}else{
						
						$rootScope.setBubbleCartToZero();
						$scope.populateShoppingCartData(data);
					}
				
			};
		});
	};
	
	$scope.newCart_Click = function (){
		var title = Util.translate('CONF_CREATE_NEW_CART',null);
		var msg = Util.translate('MSG_USE_INTERRUPTED_CART_TO_RELOAD',null);
		var dlg = $rootScope.openConfirmDlg(title , [msg]);
		dlg.result.then(function () {
			// console.log('Modal Ok clicked');
			$scope.createNewCart();
		}, function () {
			// console.log('Modal dismissed at: ' + new Date());
		});
		// MSG_USE_INTERRUPTED_CART_TO_RELOAD
	};
// $scope.goThankYou= function(){
// requestParams = ["YourReference","1","addressNumber","1","freeText","22"];
// ShoppingCartService.closeCart(requestParams).then(function(data) {
// if(Util.isNotNull(data)) {
// $scope.cartOrderNumber = data.orderNumber;
// if($scope.cartOrderNumber > 0){
// $rootScope.cartStatus = "success";
//					 	// DataSharingService.setObject("shoppingCart_orderNumber",$scope.cartOrderNumber);
// } else {
// $rootScope.cartStatus = "fail";
// }
//				 $rootScope.cartMsg =  Util.translate('MSG_ORDER_RECEIVED_ASSIGNED_ORDER_NUMBER',[ $scope.cartOrderNumber]);
// DataSharingService.setObject("shoppingCart_orderNumber",$scope.cartOrderNumber);
// $state.go('home.goThankYou');
// }
//			
// });
//		
// };
	
	$scope.$watch('cartUpdatedWatch',function() {
		$scope.getShoppingCart();
	   });

	$scope.$watch('buyItemWatch',function() {
		$scope.collapsed_additem1 = false;
	   });
	
	var getOrderInfo = function(orderNumber){
			if( orderNumber !=null){
				requestParams = ["OrderNumber",orderNumber];
				OrderService.getOrderDetailList(requestParams).then(function(data){
					if(data.orderLineList==null || data.orderLineList.length == 0){
						$scope.errorMessage = data.errorMessage;		
					}else{
						//console.log("orderDetailCtrl messageCode -- "+data.messageCode);
						 $scope.printOrder = new PrintOrder(data);
						 $rootScope.openOrderViewDlg("Order Print",  $scope.printOrder );
					}

				});		

			} 
		
	};
	
	$scope.printOrderInfo = function(orderNumber){		
		getOrderInfo(orderNumber);
		//var cartInfo = DataSharingService.getFromSessionStorage("CartDeliveryInfo_Print"); 
		// $rootScope.openOrderViewDlg("Order Print", cartInfo);
	};
	
	$scope.showOrderInfo = function(orderNumber){
		$rootScope.fromCart = true;
		$rootScope.fromRequestPage = false;
		DataSharingService.setToSessionStorage("fromCart",$rootScope.fromCart);
		DataSharingService.setToSessionStorage("fromRequestPage",$rootScope.fromRequestPage);
		 //var orderNumber =  DataSharingService.getObject("shoppingCart_orderNumber");
		// var orderNumber =  DataSharingService.getFromSessionStorage("shoppingCart_orderNumber");
		
		 if(Util.isNotNull(orderNumber)) {
			 var needToLogin = $rootScope.isSignOnRequired('CON_ORDER_INFORMATION','home.order.detail',true);
				if(!needToLogin){
					 $state.go('home.order.detail',{"ORDER_NUM":orderNumber});
				}
		 }
	
	};
	
	$scope.showOrders = function(){
		$rootScope.fromCart = true;
		$rootScope.fromRequestPage = false;
		DataSharingService.setToSessionStorage("fromCart",$rootScope.fromCart);
		DataSharingService.setToSessionStorage("fromRequestPage",$rootScope.fromRequestPage);
		 var needToLogin = $rootScope.isSignOnRequired('CON_ORDER_INFORMATION','home.order',true);
			if(!needToLogin){
				 $state.go('home.order');
			}	
	};
	
	// $rootScope.resetList();
	/* New Add Section Start */
	$scope.showAddProductTable = function()
	{
		if(!$rootScope.isShowDisableMenuItem('CON_ADD_PRODUCTS_TO_YOUR_CART')){
			return;
		}
		$scope.collapsed_additem1 = !$scope.collapsed_additem1;
		$scope.collapsed_copypasteArea1 = false;
		var copyOrderObject = DataSharingService.getObject("copyOrder");
		if(isNull(copyOrderObject) && isNull($scope.copyOrder)){
			$scope.copyOrder =  new MultipleAdd(null,$rootScope,Util.translate,$rootScope.CopyOrderDelimeter);	
			$scope.copyOrder.clearAll();
		}else if(!isNull(copyOrderObject)){
			  var orderListLength = copyOrderObject.getList();
				if(orderListLength <=0){
					$scope.copyOrder =  new MultipleAdd(null,$rootScope,Util.translate,$rootScope.CopyOrderDelimeter);
						$scope.copyOrder.clearAll();
				}
		}
	};
	
	/* New Add Section End */
	
	/* New Copy Paste Section Start */ 
	$scope.showCopyPaste = function(){
		if(!$rootScope.isShowDisableMenuItem('CON_COPY/PASTE_ORDER_LIN')){
			return;
		}
		$scope.collapsed_copypasteArea1 = !$scope.collapsed_copypasteArea1;
		$scope.collapsed_additem1 = false;
		$scope.copyOrder =  new MultipleAdd(null,$rootScope,Util.translate,$rootScope.CopyOrderDelimeter);
		$scope.copyOrder.ListToString();
		DataSharingService.setObject("copyOrder",$scope.copyOrder);
	};
	
	$rootScope.$on('itemsBuyAddItem', function(){ 
		$scope.resetCopyOrder();
	});
	
	$scope.resetCopyOrder = function() {
		$scope.copyOrder =  new MultipleAdd(null,$rootScope,Util.translate,$rootScope.CopyOrderDelimeter);
		$scope.copyOrder.ListToString();
		DataSharingService.setObject("copyOrder",$scope.copyOrder);
	};
	
	$scope.showAddTable = function(id,orderText){
		  var validOrder = $scope.copyOrder.StringToList(orderText);
		  if(validOrder){
			  	$scope.collapsed_additem1 = true;
			  	$scope.collapsed_copypasteArea1 = false;
		  }
	  };
	  
	  /* New Copy Paste Section End */
	
	/* Add Item Section start */
	/* ADM-022 start */
	// $scope.addRowCount = 5;
	// $scope.lastIndex = 4;
	// $rootScope.addItemRowList = [0,1,2,3,4];
	$scope.addRowCount = 1;
	$scope.lastIndex = 0;	
	$rootScope.addItemRowList = [0];
	/* ADM-022 end */
	$rootScope.showUnitList = {};
	var makeTableRows = function(lastRowNumber,intervalSet){
		var lastRowIndex = 0;
		if(intervalSet) {//  more rows into the Add Item section  will be added as per addRowCount value   
			lastRowIndex =  parseInt(lastRowNumber) + parseInt($scope.addRowCount) ;
			for(var i=lastRowNumber+1;i<=lastRowIndex;i++){
				$rootScope.addItemRowList.push(i);
			}
			$scope.lastIndex = lastRowIndex;
		} else{//  total rows in Add Item section will be equla to copy paste area order lines count and  existing filled order rows in the add item section
			$rootScope.addItemRowList = [];
			for(var i=0;i<=lastRowNumber;i++){
				$rootScope.addItemRowList.push(i);
			}
			$scope.lastIndex = lastRowNumber;
		}
	};
	$scope.collapsed_additem1 = false;
	$scope.goToAddItem = function(id){
		// $scope.collapsed_additem1 = true;
		$scope.showAddProductTable();
		$rootScope.scrollToId(id);
	};
	
	$scope.collapsed_copypasteArea1 = false;
	$scope.goToCopyPaste = function(id){
		$scope.showCopyPaste();
		$rootScope.scrollToId(id);
	};
	$scope.addMoreRows = function(){
		makeTableRows($scope.lastIndex,true);	    
	  }; 
	
	 var resetList = function(){
		 	$scope.lineItemCode = {};
			$scope.lineItemQuantity = {};
			$scope.lineItemDelDate = {};
			$scope.lineItemUnitCode = {};
			$scope.lineItemShipmentMark = {};
		// $scope.lineItemText = {};
		};
		
		resetList();
	  
		$scope.clearAddItemList = function(){
			$rootScope.addItemRowList = [];
			for(var i=0;i<$scope.addRowCount;i++){
					$rootScope.addItemRowList.push(i);
			}
			$scope.lastIndex = parseInt($rootScope.addItemRowList.length)-1;
			resetList();
		};
		
		// posting Add Item section data to cart
		$scope.addItemsToCart = function (){
			var orderItemList = $scope.createAddItemOrderList();
			if(validateAddItemList(orderItemList)){
				var param =  "inputItems="+JSON.stringify(orderItemList);
				// console.log('Add Items to cart : ' + param);
				Util.postDataToServer('addItemsToCartUrl',param);
			}
		};
		// createAddItemObj
		$scope.createAddItemOrderList = function(){
			$scope.emptyOrderLine = 0;
			var itemsLinesObjList = [];		
			var itemListLength = $rootScope.addItemRowList.length;
			if(itemListLength!=null){
				for(var i=0;i<itemListLength;i++){
					var itemLineObj = new Object();
					itemLineObj.itemCode = $scope.lineItemCode[$rootScope.getId('itemCode',i)];		
					itemLineObj.quantity= $scope.lineItemQuantity[$rootScope.getId('itemQuantity',i)];
					if(isOrderLineValid(itemLineObj.itemCode,itemLineObj.quantity)){
							itemLineObj.deliveryDate = $scope.lineItemDelDate[$rootScope.getId('itemDelDate',i)];
							itemLineObj.unitCode = $scope.lineItemUnitCode[$rootScope.getId('itemUnitCode',i)];
							itemLineObj.shipmentMark = $scope.lineItemShipmentMark[$rootScope.getId('itemShipMark',i)];
							//itemLineObj.lineText = $scope.lineItemText[$rootScope.getId('itemFreeText',i)];
							itemsLinesObjList.push(itemLineObj);
					}else{
							$scope.emptyOrderLine = $scope.emptyOrderLine +1;
					}
				};
			}
			return itemsLinesObjList;			
		};
		
		var isOrderLineValid = function(itemCode, itemQuantity){
			$scope.orderListValidationMsg = "";
			var isValid = true;
			if(itemCode == null && itemQuantity == null){
					return false;
			}
			if(itemCode == "" && itemQuantity == "") {
				return false;}
			if(itemCode != null){
				itemCode = itemCode.trim();}
			if(itemQuantity != null){
				itemQuantity = itemQuantity.trim();}
			return isValid;
		};
		var validateAddItemList = function(Orderlist){
			var isValidOrder = true;
			$scope.orderListValidationMsg = "";
			if(Orderlist.length > 0){
				for(var i=0; i<Orderlist.length;i++ ){
					if((Orderlist[i].itemCode == null)||(Orderlist[i].itemCode == "")){
						$scope.orderListValidationMsg = Util.translate('MSG_ITEM_NOT_SPECIFIED'); 
						isValidOrder = false;
					}
					if((Orderlist[i].quantity == null)||(Orderlist[i].quantity == "")){
						$scope.orderListValidationMsg = Util.translate('MSG_QUANTITY_NOT_SPECIFIED'); 
						isValidOrder = false;
					}else {
						var isNumber = $rootScope.isInt(Orderlist[i].quantity);
						if(!isNumber){
							$scope.orderListValidationMsg = Util.translate('MSG_INVALID_QUANTITY'); 
							isValidOrder = false;
						}
					}
				};
			}
			// $scope.orderListValidationMsg = "ok";
			return isValidOrder;
		};
		
		// for product line validation
		$scope.validateProductCode = function(itemCode,page){
			var requestParam = getItemCodeRequestParam(itemCode,page) ;
			var orderItemList = $scope.createAddItemOrderList();
			$rootScope.fetchProductDetails(requestParam,orderItemList);	
		};
		
		$scope.fetchProductDetails = function(requestParams,productLineList){		
			Util.getItemCodeDetails(requestParams).then(function(data){
				$scope.productCodeInfo  = data;			
				$scope.validateLineProductCode(data,productLineList) ;				
			});	
		};
		
		$scope.validateLineProductCode = function(itemInfo,lineList){
			var isValidItem = true;
			var unitList = [];
			if(!isNull(itemInfo)){
				// if message code returns
				if(!isNull(itemInfo.messageCode)){
					switch(itemInfo.messageCode){
						case '114' : 
							setUIError(lineObject,'MSG_INVALID_ITEM');		
							break;

						case '3101' : 
							setUIError(lineObject,'MSG_ITEM_IS_NOT_AVAILABLE');		
							break;

						case '3102' : 
							setUIError(lineObject,'MSG_ITEM_QUANTITY_ZERO');		
							break;

						case '3103' : 
							setUIError(lineObject,'MSG_NEGATIVE_QUANTITY');		
							break;

						case '3104' : 
							setUIError(lineObject,'MSG_INVALID_QUANTITY');		
							break;

						case '3105' : 
							setUIError(lineObject,'MSG_INVALID_DATE');		
							break;

						case '3106' : 
							setUIError(lineObject,'MSG_DATE_NOT_ALLOWED');		
							break;

						case '3107' : 
							setUIError(lineObject,'MSG_NOT_WORK_DAY');		
							break;

						case '3108' : 
							setUIError(lineObject,'MSG_ITEM_NOT_SPECIFIED');		
							break;

						case '3109' : 
							setUIError(lineObject,'MSG_INVALID_ITEM');		
							break;

						case '3110' : 
							setUIError(lineObject,'MSG_ITEM_INVALID_UNIT');		
							break;

						case '3111' : 
							setUIError(lineObject,'MSG_ITEM_NOT_ALLOWED_UNIT');		
							break;

						case '3112' : 
							setUIError(lineObject,'MSG_ITEM_NO_PRICE');		
							break;

						case '3113' : 
							setUIError(lineObject,'MSG_DELIVERY_NOT_POSSIBLE');		
							break;
							
						case '3114' : 
							setUIError(lineObject,'MSG_BUY_NOT_ALLOWED');					
							break;
						case '3117' : 
							setUIError(lineObject,'MSG_BUY_NOT_ALLOWED');					
							break;

						default : 	setUIError(lineObject,'MSG_INVALID_ITEM');
					}
					isValidItem = false;
					line.showUnitList = false;
					unitList = [];
					line.isValidProduct = false;
				}else{
					// if item data comes
						if(itemInfo.isItemCodeError) {
							setUIError(lineObject,'MSG_INVALID_ITEM');		
							line.showUnitList = false;
							isValidItem = false;
						}
	
						if(itemInfo.isUnitCodeError) {
							setUIError(lineObject,'MSG_ITEM_INVALID_UNIT');						
							isValidItem = false;
						}
						if(!itemInfo.isBuyAllowed) {
							setUIError(lineObject,'MSG_BUY_NOT_ALLOWED');						
							isValidItem = false;					
	
						}
						unitList = itemInfo.activeUnitsDesc;				
				}
			}
			if(isValidItem){
				line.showUnitList = true;
				line.activeUnitList = unitList;
				line.isValidProduct = true;
				lineObject.setLineObjectStatus(line,lineObject,true);
			}else {
				lineObject.isValidProductList = false;
				line.unit = null;
				line.isValidProduct = false;
				// setfocus(line.rowNum);
				lineObject.setLineObjectStatus(line,lineObject,false);
			}
		};
			
	/* Add Item Section end */
	
	/* Copy Paste Section Start */
	
	  $scope.copyPasteText = {};
	  $scope.orderDelimeter = {};
	  $scope.itemMsg = {};
	  $scope.itemMsg['msg2'] = Util.translate('TXT_OE_005_01');
	  $scope.itemMsg['msg3'] = Util.translate('TXT_OE_005_02');
	
	  $scope.clearCopyPasteArea = function(){
		  $scope.copyPasteText = {};
	  };
	  
	  $scope.convertOrderLines = function(){
			 var copyPasteOrderText = $scope.copyPasteText['orderText'];
			 var delimeter = $scope.orderDelimeter['orderDelimeter'];
			
			 if( validateCopyPasteOrder( copyPasteOrderText,delimeter)){
				 $scope.copyPasteText['orderText'] = null;
				 var addItemOrderLineList = $scope.createAddItemOrderList();
				 var copyPasteOrderList =  copyPasteOrderText.split("\n");
				 var itemCount_addSection =  parseInt($rootScope.addItemRowList.length) - parseInt($scope.emptyOrderLine); // to count no of rows already filled in add item section
				 var totalOrderLines = itemCount_addSection + copyPasteOrderList.length; // to make final count of add section rows + new items from copy section
				 $scope.lastIndex = totalOrderLines - 1;			 		 	 
				 makeTableRows($scope.lastIndex,false);
				 // populate add item section table rows with the existing valid order lines				 
				 for( var i = 0; i<  addItemOrderLineList.length;i++){
					 $scope.lineItemCode[$rootScope.getId('itemCode',i)] =  addItemOrderLineList[i].itemCode;
					 $scope.lineItemQuantity[$rootScope.getId('itemQuantity',i)] =  addItemOrderLineList[i].quantity;
					 $scope.lineItemUnitCode[$rootScope.getId('itemUnitCode',i)] =  addItemOrderLineList[i].unitCode;
					 $scope.lineItemDelDate[$rootScope.getId('itemDelDate',i)] =  addItemOrderLineList[i].deliveryDate;
					 $scope.lineItemShipmentMark[$rootScope.getId('itemShipMark',i)] =  addItemOrderLineList[i].shipmentMark;
				 }
				 
				 // populate add item section table rows with the copyPaste order  lines
				 for( var i = 0; i<  copyPasteOrderList.length;i++){
					 var itemIndex = i+itemCount_addSection  ;
					 $scope.copyOrderLine =  copyPasteOrderList[i].split($scope.orderDelimeter['orderDelimeter']);
					 $scope.lineItemCode[$rootScope.getId('itemCode',itemIndex)] =  $scope.copyOrderLine[0];
					 $scope.lineItemQuantity[$rootScope.getId('itemQuantity',itemIndex)] =  $scope.copyOrderLine[1];
					 $scope.lineItemUnitCode[$rootScope.getId('itemUnitCode',itemIndex)] =  $scope.copyOrderLine[2];
					 $scope.lineItemDelDate[$rootScope.getId('itemDelDate',itemIndex)] =  $scope.copyOrderLine[3];
					 $scope.lineItemShipmentMark[$rootScope.getId('itemShipMark',itemIndex)] =  $scope.copyOrderLine[4];
				 }
			 }
			 
		};
		
	  
	var validateCopyPasteOrder = function(copyText,delimeter){
		var isOrderValid = true;
		$scope.orderValidationMsg =  "";
		if(delimeter == "" || delimeter == null ){
			isOrderValid = false; $scope.orderValidationMsg = Util.translate('MSG_DELIMITER_MUST_BE_SINGLE_CHAR'); 
			return isOrderValid; 
		}		
		
		if(copyText != null){
			var copyOrderLineList =  copyText.split("\n");
			// To check delimeter
			for( var i = 0; i< copyOrderLineList.length;i++){
				if(copyOrderLineList[i].length > 0){
					var orderLineKeyLength = copyOrderLineList[i].split(delimeter).length;
					if(orderLineKeyLength < 2){ isOrderValid = false; $scope.orderValidationMsg = Util.translate('MSP_LINE_MIN_2_VALUES',[i+1,2]);} // to check minimum no. of tokens in a line
					if(orderLineKeyLength  > 5){ isOrderValid = false; $scope.orderValidationMsg = Util.translate('MSP_LINE_MAX_5_VALUES',[i+1,orderLineKeyLength]);} // to check maximum no. of tokens in a line
				}
			};
		}else {
			isOrderValid = false;
		}
		return isOrderValid;
	};
	
	 /* Copy Paste Section End */  
 	
	$scope.recalculate_Click = function (orderLevelDate){
		isValid = true;
		angular.forEach($scope.orderLines, function(line) {        
		    if(isValid){
		    	if(!line.validQuantity){
		    		isValid = false;
		    	}
		    }
		});
		if(isValid){
			var obj = $scope.createRecalculateOrderLinesObj(orderLevelDate);
			var param=  "orderLineData="+JSON.stringify(obj);
			var validQuantity = true;		
			for(var i=0;i<obj.length;i++){
				if((!isValidQuantity(obj[i].quantity,$rootScope.webSettings.maxQuantity,$rootScope.webSettings.allowDecimalsForQuantities) || (obj[i].quantity==0))){	
					validQuantity=false;
				}
			};
			
			if(validQuantity==true){
				Util.postDataToServer('recalculateCartUrl',param,true).then(function(data){
					
					if(data.messageCode != null || data.messageCode != undefined){
						var messageText = $rootScope.getBuyStatusMsg(data.messageCode.toString());
						var dlg = $rootScope.openMsgDlg(messageText);
						dlg.result.then(function () {
							
						}, function () {
							$scope.getShoppingCart();
						});
					}else{
					$scope.populateShoppingCartData(data);
					}
				});
			}else{
				var msg = Util.translate('CON_RECALCULATE_ERROR_HEADER');
				var itemCode = Util.translate('CON_RECALCULATE_ERROR_TEXT');			
				$rootScope.openQuantityZeroDlg(msg , [itemCode]);
			}
		}else{
			var msg = Util.translate('CON_RECALCULATE_ERROR_HEADER');
			var itemCode = Util.translate('CON_RECALCULATE_ERROR_TEXT');			
			$rootScope.openQuantityZeroDlg(msg , [itemCode]);
		}
	};
	
	$scope.createRecalculateOrderLinesObj =function(orderLevelDate){
		var recalculateOrderLinesObjList = [];
		var orderLinesNumbers = Object.keys($scope.orderLines );
		if(orderLinesNumbers!=null){
			for(var i=0;i<orderLinesNumbers.length;i++){
				var recalculateOrderLinesObj = new Object();
				recalculateOrderLinesObj.lineNumber = orderLinesNumbers[i];
				//recalculateOrderLinesObj.quantity = $scope.orderLines[orderLinesNumbers[i]+""].ivEnteredValues.Quantity;
				recalculateOrderLinesObj.quantity=$scope.lineQuantity[$rootScope.getId('quantity',orderLinesNumbers[i])];
				if($rootScope.webSettings.allowDecimalsForQuantities == true){
					recalculateOrderLinesObj.quantity = recalculateOrderLinesObj.quantity.replace(",",".");
				}
				if(!isNull(orderLevelDate)){
				recalculateOrderLinesObj.deliveryDate = orderLevelDate;
				}
				// ADM-010
				if($rootScope.webSettings.allowSelectUnitOfMeasure){
					recalculateOrderLinesObj.unitCode = $scope.freeSearchObj.packPiece;
				}
				// ADM-010 end
				
				recalculateOrderLinesObjList.push(recalculateOrderLinesObj);
			};
		}
		return recalculateOrderLinesObjList;
	};
	
	var updateQuantityTextBox = function(orderLines){
		if(Util.isNotNull(orderLines)){
			var orderLine = null;
			var orderLinesNumbers = Object.keys(orderLines );
			for(var i=0;i<orderLinesNumbers.length;i++){
				orderLine = orderLines[orderLinesNumbers[i]];
				orderLine.validQuantity =true;
				$rootScope.lineQuantity[$rootScope.getId('quantity',orderLinesNumbers[i])]= $scope.changeFormatToLocale(orderLine.ivOrderLine.orderedQuantity);
			};
		};
	};
	$scope.goInterruptedCart= function(){
			$scope.getInterruptedCart();
			var needToLogin = $rootScope.isSignOnRequired('CON_INTERRUPTED_CARTS','home.shoppingcart.interruptedCart',true);
			if(!needToLogin){
				$state.go('home.shoppingcart.interruptedCart');
			}
	};
	
	$scope.loadCarOptionClass = function(){
		$scope.callState = $state.current.name;
		if($scope.callState == "home.shoppingcart"){
			$scope.cartOptions = "cart-options-shopCart";
		}else{
			$scope.cartOptions = "cart-options";
		}
		return true;
	};
	
	$scope.getInterruptedCart = function(){
		ShoppingCartService.getInterruptedCart([]).then(function(data){
			$scope.isInterruptedCartEmpty = false;
			if(Util.isNotNull(data)) {
				if(data.cartList !== undefined){
					$scope.isInterruptedCartEmpty = false;
				}else{
					$scope.isInterruptedCartEmpty = true;
				}
				$scope.interruptedCarts = data.cartList;
				$scope.$root.$eval();
				// $state.go('home.shoppingcart.interruptedCart');
			}else{
				// Todo
				$scope.isInterruptedCartEmpty = true;
			}
		});
	};
	
	var getSelectedInterruptedCarts = function(){
		var selectedList = [];
		if($scope.interruptedCarts != null){
		var list = $scope.interruptedCarts;
		for(var i=0;i<list.length ;i++){
			var item = list[i];
			if(item.checked){
				var paramObject = new Object();
				paramObject.orderNumber = item.orderNumber;
				selectedList.push(paramObject);
			}
		}
		}
		return selectedList;
	};
	
	$scope.toggleSelectAll = function(flag){
	if($scope.interruptedCarts != null){
	 var list = $scope.interruptedCarts;
		for(var i=0;i<list.length ;i++){
			var item = list[i];
			item.checked = flag;
		}}
	};
	
	$scope.deleteInterruptedCarts = function(){
		//$log.log("selected interrupted carts = "  + JSON.stringify(getSelectedInterruptedCarts()));
		var list = getSelectedInterruptedCarts();
		if(Array.isArray(list) && list.length>0){
			var msg = Util.translate('CONF_DELETE_SELECTED_INTERRUPTED_CARTS',null);
			var dlg = $rootScope.openConfirmDlg(msg);
			dlg.result.then(function () {
				$scope.selectAll = false;
				// console.log('Delete interrupted carts modal Ok clicked');
				var param = "interruptedCart="+JSON.stringify(list);
				ShoppingCartService.deleteInterruptedCarts(param).then(function(data){
					handleDeleteInterruptedCartsMsgCode(data);
				});
			}, function () {
				// console.log('Delete interrupted carts modal Cancel clicked');
			});
		}
	};
	
	var handleDeleteInterruptedCartsMsgCode = function(msgList){
		//var msgList =  [{"orderNumber":"50468","messageCode":"3115"},{"orderNumber":"50467","messageCode":"3115"}];
		var errorMsgList = [];
		var anyCartgotDeleted = false ;
		for(var i=0 ; i<msgList.length ; i++){
			var item = msgList[i];
			if(item.messageCode == '3115' || item.messageCode == 3115){
				var msg = Util.translate('MSG_UNABLE_TO_DELETE_INTERRUPTED_CART',[item.orderNumber]);
				errorMsgList.push(msg);
			}else if(item.messageCode == '3555' || item.messageCode == 3555){
				anyCartgotDeleted = true;
			}
		}
		showDeleteInterruptedCartsMsg(errorMsgList);
		if(anyCartgotDeleted){
			$scope.getInterruptedCart();
		}
	};
	
	var showDeleteInterruptedCartsMsg = function(errorMsgList){
		// openMsgDlg
		if(errorMsgList.length > 0){
			var msg = "";
			var dlg = $rootScope.openMsgDlg(msg,errorMsgList);
			dlg.result.then(function () {
				////$log.info('DeleteInterruptedCartsMsg modal Ok clicked');
			}, function () {
				////$log.info('DeleteInterruptedCartsMsg modal Cancel clicked');
			});
		}
	};
	
	$scope.getConfirmedOrderNo = function(){
		return $scope.thankYouObj.orderNumber;
	};
	
	$scope.exportShoppingCartTableAsPDF=function(){
		var shoppingCartTable = setShoppingCartDataforExport();
		var data = shoppingCartTable.getHTML();
		var title = Util.translate('CON_SHOPPINGCART');
		var fileName = "ShoppingCart_"+getCurrentDateTimeStr()+".pdf";
		savePdfFromHtml(data,title,fileName);
	};
	
	$scope.exportShoppingCartTableAsExcel=function(){
		var shoppingCartTable = setShoppingCartDataforExport();
		var data = shoppingCartTable.getDataWithHeaders();
		var sheetName = "ShoppingCart";
		var fileName = "ShoppingCart_"+getCurrentDateTimeStr()+".xlsx";
		saveExcel(data,sheetName,fileName);
	};
	
	$scope.exportShoppingCartTableAsXML=function(){
		var shoppingCartTable = setShoppingCartDataforExport('XML');
		shoppingCartTable.setRootElementName("SHOPPINGCART");
		shoppingCartTable.setEntityName("SHOPPINGCART_ITEM");
		var data = shoppingCartTable.getXML();
		var fileName = "ShoppingCart_"+getCurrentDateTimeStr()+".xml";
		saveXML(data,fileName);
	};
	
	var setShoppingCartDataforExport = function(target){
		var shoppingCartTable = new Table();
		if(isEqualStrings(target,"XML")){
			shoppingCartTable.addHeader('PRODUCT');
			shoppingCartTable.addHeader('DESCRIPTION');
			shoppingCartTable.addHeader('DELIVERY_DATE');
			shoppingCartTable.addHeader('UNIT');
			if($rootScope.webSettings.showOrderLineDiscount){
				shoppingCartTable.addHeader('DISCOUNT');
			}
			if($rootScope.webSettings.isShowPrice){
				shoppingCartTable.addHeader('PRICE');
			}
			shoppingCartTable.addHeader('QUANTITY');
			shoppingCartTable.addHeader('TOTAL_AMOUNT');
		}else{
			shoppingCartTable.addHeader(Util.translate('CON_ITEM'));
			shoppingCartTable.addHeader(Util.translate('COH_DESCRIPTION'));
			shoppingCartTable.addHeader(Util.translate('CON_DELIVERY_DATE'));
			shoppingCartTable.addHeader(Util.translate('CON_UNIT'));
			if($rootScope.webSettings.showOrderLineDiscount){
				shoppingCartTable.addHeader(Util.translate('CON_DISCOUNT')+" "+ webSettings.currencyCode);
			}
			if($rootScope.webSettings.isShowPrice){
				shoppingCartTable.addHeader(Util.translate('CON_PRICE')+" "+ webSettings.currencyCode);
			}
			shoppingCartTable.addHeader(Util.translate('COH_QUANTITY'));
			shoppingCartTable.addHeader(Util.translate('COH_TOTAL_AMOUNT'));
		}
		// set row data
		data = objectToArray($scope.orderLines);
		if(!isNull(data)){
			for(var i=0;i<data.length;i++){
				var item = data[i];
				var row = [];
				row.push(item.ivItem.code);
				row.push(item.ivItem.description); 
				row.push(item.ivEnteredValues.Date);
				row.push(item.ivEnteredValues.Unit); 
				if($rootScope.webSettings.showOrderLineDiscount){
					if(item.ivOrderLine.lineDisCount!=""){
						row.push(item.ivOrderLine.lineDisCount);
					}else{
						row.push("");
					}
				}
				if($rootScope.webSettings.isShowPrice){
					row.push(item.ivOrderLine.price);
				}
				row.push(item.ivOrderLine.orderedQuantity);
				row.push(item.ivOrderLine.lineVal); 
				shoppingCartTable.addRow(row);
				if(item.focListItems.length > 0){
					row = [];
					row.push(item.focListItems[0].focItemCode);
					row.push(item.focListItems[0].focItemDesc); 
					row.push(""); 
					row.push(item.focListItems[0].focItemUnit); 
					if($rootScope.webSettings.isShowPrice){
						row.push(""); 
						row.push(""); 
					}
					row.push(item.ivOrderLine.orderedQuantity);
					row.push(""); 
					shoppingCartTable.addRow(row);
				}
			}
		}else{
			var row = [];			
			for(var i=0;i<shoppingCartTable.headers.length;i++){
				row.push("");
			}
			shoppingCartTable.addRow(row);
		}
		
		return shoppingCartTable;
	};
	
	function objectToArray( object ) {
	    var length = 0;
	    var data=[];
	    for( var key in object ) {
	        if( object.hasOwnProperty(key) ) {
	            data[length]=object[key];
	            ++length;
	        }
	    }
	    return data;
	};

	// ADM-010
	$scope.$watch('freeSearchObj.packPiece',
		function(){
			refreshUIforPackPiece();
		}
	);
	
	// ADM-010
	function refreshUIforPackPiece(){
		if(!isNull($rootScope.webSettings) && $rootScope.webSettings.allowSelectUnitOfMeasure){
			if (Util.isNotNull($scope.orderLines)) {
				var orderLine = null;
				var orderLinesNumbers = Object.keys($scope.orderLines);
				for ( var i = 0; i < orderLinesNumbers.length; i++) {
					orderLine = $scope.orderLines[orderLinesNumbers[i]];
					$rootScope.updatePackPieceFromShoppingCart(
							$scope.freeSearchObj.packPiece, orderLine);
				}
				updateQuantityTextBox($scope.orderLines);
			}
			var copyOrderObject = $scope.copyOrder;
			if(!isNull(copyOrderObject)){
			  var orderListLength = copyOrderObject.getList().length;
				if(orderListLength > 0){
					for(var i=0; i< orderListLength; i++ ){
						var line = copyOrderObject.getList()[i];
						var unit = $scope.freeSearchObj.packPiece;
						var unitDesc = $scope.freeSearchObj.packPiece;
						if(!isNull(line.salesPackageQty)){
							unitDesc = unitDesc + line.salesPackageQty;
						}
						line.activeUnitList = [{salesUnit: unit, salesUnitDesc: unitDesc}];
						line.unit = unit;
						line.unitList = [unit];
					}
				}
			}
		}
	};
}]);