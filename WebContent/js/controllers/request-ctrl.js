catalougeControllers.controller('requestCtrl', ['$rootScope','$scope','$stateParams', '$http','$log', '$q','DataSharingService','RequestSearchService', 'Util','$filter','$state', '$timeout',
                                                function ($rootScope,$scope,$stateParams, $http,$log, $q,DataSharingService,RequestSearchService,Util,$filter,$state,$timeout) {

	/*
	 * The initController() method is for initialise the
	 * controller variables
	 */
	var webSettings = {};
	$scope.selectRequestLine = {};
	var customer = "";
	var customerCode = "";
	var yourReference = "";
	var yourReferenceCode = "";

	var fromState = DataSharingService
			.getObject("fromState");
	if (fromState == REQ_SUBMIT_CONFIRMATION) {
		historyManager.isBackAction = false;
		$scope.requestLineObj = DataSharingService
				.getObject("requestLineObject");
		$scope.requestObject = DataSharingService
				.getObject("requestBPObject");
		$scope.requestForlabel = DataSharingService
				.getObject("requestForlabel");
		DataSharingService.setObject("fromState", "");
	} else {
		DataSharingService.setObject(
				"requestLineObject", null);
		DataSharingService.setObject("requestBPObject",
				null);
		DataSharingService.setObject(
				"requestObjectInfo", null);
		DataSharingService.setObject("requestForlabel",
				null);
		DataSharingService.setObject("newRequestParam",
				null);
		$scope.requestLineObj = null;
		$scope.requestObject = null;
	}

	$scope.initController = function() {
		if (!$rootScope.isSignedIn) {
			$rootScope.reloadHomePage();
		}
 
		angular.element('#button-testata').removeClass('collapsed');
		angular.element('#headingrequest').addClass('in');
		
		var state = $scope.requestState;
		$rootScope.showRequestBase = false;
		webSettings = DataSharingService
				.getWebSettings();
		$rootScope.isCollapsed_Products = false;
		if (isNull($rootScope.reqTemplate)
				|| Object
						.getOwnPropertyNames($rootScope.reqTemplate).length === 0) {
			// ADM-XXX
			// $rootScope.reqTemplate.checked = "order";
			// $scope.requestForlabel = "CON_ORDER";
			// state = REQ_SUBMIT_ORDER_VIEW;
			$rootScope.reqTemplate.checked = "transportNote";
			$scope.requestForlabel = "CON_TRANSPORTNOTE";
			state = REQ_SUBMIT_TRANSPORTNOTE_VIEW;
			// ADM-XXX End
		} else {
			switch ($rootScope.reqTemplate.checked) {
			case 'order':
				state = REQ_SUBMIT_ORDER_VIEW;
				$scope.requestForlabel = "CON_ORDER";
				break;
			case 'invoice':
				state = REQ_SUBMIT_INVOICE_VIEW;
				$scope.requestForlabel = "CON_INVOICE";
				break;
			// ADM-XXX
			case 'transportNote':
				state = REQ_SUBMIT_TRANSPORTNOTE_VIEW;
				$scope.requestForlabel = "CON_TRANSPORTNOTE";
				break;
			default:
				state = REQ_SUBMIT_VIEW;
				$scope.requestForlabel = "CON_REFERENCE";

			}
			;
		}

		$scope.listofRequestNotNull = function() {
			var listRequest = false;
			var list = $rootScope.listOfRequest;
			$scope.requestForlabel = "CON_REFERENCE";
			$rootScope.reqTemplate.checked = "product";
			$scope.requestObject
					.updateRequestBase($rootScope.reqTemplate.checked);
			if (list == undefined) {
				listRequest = false;
			} else {
				listRequest = true;
			}
			return listRequest;
		};

		$scope.requestState = state; 
		$rootScope.requestSubmit = true; 

		/*
		 * getting the logged in user name and default
		 * customer name from web settings
		 */
		if (!isNull(webSettings)) {
			if (!isNull(webSettings.sessionDefaultCustomerName)) {
				customer = webSettings.sessionDefaultCustomerName;
			} else {
				customer = webSettings.defaultCustomerName;
			}
			if (!isNull(webSettings.sessionDefaultCustomerCode)) {
				customerCode = webSettings.sessionDefaultCustomerCode;
			} else {
				customerCode = webSettings.defaultCustomerCode;
			}

			yourReference = webSettings.userName;
			yourReferenceCode = webSettings.userCode;
		}
	};

	var getCachedBPRequestData = function() {
		var isDataCached = false;
		$scope.requestLineObj = DataSharingService
				.getObject("requestLineObject");
		$scope.requestObject = DataSharingService
				.getObject("requestBPObject");
		if (!isNull($scope.requestLineObj)
				|| !isNull($scope.requestObject)) {
			isDataCached = true;
		}
		return isDataCached;
	};

	var cacheBPRequestData = function() {
		DataSharingService.setObject(
				"requestLineObject",
				$scope.requestLineObj);
		DataSharingService.setObject("requestBPObject",
				$scope.requestObject);
		DataSharingService.setObject(
				"requestObjectInfo",
				$scope.requestObject);
	};

	$scope.updateLineObj = function(reqLine) {
		for ( var i = 0; i < reqLine.newRequestLineList.length; i++) {
			if (!isNull(reqLine.newRequestLineList[i].selctedProbType)) {
				reqLine.newRequestLineList[i].probType = reqLine.newRequestLineList[i].selctedProbType;
				reqLine.newRequestLineList[i].selctedProbType = reqLine.newRequestLineList[i].requestTypeList;
			}
			if (!isNull(reqLine.newRequestLineList[i].selectedReasonCode)) {
				reqLine.newRequestLineList[i].reasonCode = reqLine.newRequestLineList[i].selectedReasonCode;
				reqLine.newRequestLineList[i].selectedReasonCode = reqLine.newRequestLineList[i].reasonCodeList;
			}
		}
		$rootScope.validateProductList(reqLine,'REQUEST');
	};

	var clearRequestObjectError = function() {
		if (!isNull($scope.requestObject)) {
			$scope.requestObject.UIErrorKey = null;
			$scope.requestObject.UIErrorParams = [];
			$scope.requestObject.ErrOnControl = {};
		}

	};
	$scope.$on('$destroy', function(event) {
		clearRequestObjectError();
		historyManager.pushScopeObject($scope);
		cacheBPRequestData();

	});

	$rootScope.deleteCachedBPRequestData = function() {
		DataSharingService.setObject(
				"requestLineObject", null);
		DataSharingService.setObject("requestBPObject",
				null);
		DataSharingService.setObject(
				"requestObjectInfo", null);
		$scope.requestLineObj = null;
		$scope.requestObject = null;
		$scope.requestObject = null;
	};

	/*
	 * The createRequest() method will create
	 * requestObject and requestLineObj
	 */
	var createRequest = function() {

		getCachedBPRequestData();

		if (isNull($scope.requestLineObj)) {
			$scope.requestTypeList = RequestSearchService
					.getRequestTypeList();
			$scope.reasonCodeList = RequestSearchService
					.getReasonCodeList(); // ADM-XXX
			if (!isNull($scope.requestTypeList)/* ADM-XXX */
					&& !isNull($scope.reasonCodeList)) {
				$scope.requestObject = new Request(
						customer, customerCode,
						yourReference,
						yourReferenceCode,
						$scope.requestTypeList,
						$rootScope.reqTemplate.checked);
				$scope.requestLineObj = new MutlipleRequestLine(
						$scope.requestTypeList, null,
						$rootScope,
						Util.translate/* ADM-XXX */,
						$scope.reasonCodeList);
				DataSharingService.setObject(
						"requestLineObject",
						$scope.requestLineObj);
				DataSharingService.setObject(
						"requestBPObject",
						$scope.requestObject);
			} else {
				Util
						.getFilterListDetails([])
						.then(
								function(data) {
									DataSharingService
											.setFilterDetailsSettings(data);
									$rootScope.filterDetailSettings = data;
									$scope.requestTypeList = $rootScope.filterDetailSettings.requestTypeList;
									$scope.reasonCodeList = $rootScope.filterDetailSettings.reasonCodeList; // ADM-XXX
									$scope.requestObject = new Request(
											customer,
											customerCode,
											yourReference,
											yourReferenceCode,
											$scope.requestTypeList,
											$rootScope.reqTemplate.checked);
									$scope.requestLineObj = new MutlipleRequestLine(
											$scope.requestTypeList,
											null,
											$rootScope,
											Util.translate/* ADM-XXX */,
											$scope.reasonCodeList);
									DataSharingService
											.setObject(
													"requestLineObject",
													$scope.requestLineObj);
									DataSharingService
											.setObject(
													"requestBPObject",
													$scope.requestObject);
								});
			}
		}
	};

	$rootScope.initCommitsSecondNextPromise
			.then(function() {
				$scope.initController();
				createRequest();
			});

	/*
	 * The getSelectedRequestLines() method will return
	 * the selected order / invoice lines list to make
	 * the new request lines
	 */
	var getSelectedRequestLines = function(
			requestLineList) {
		var selectedList = [];
		var list = requestLineList;
		for ( var i = 0; i < list.length; i++) {
			var item = list[i];
			if (item.checked) {
				selectedList.push(item);
			}
		}
		return selectedList;
	};

	$scope.SelectedInvoiceLine = function(list) {
		var selectedInvoiceLineList = $filter('filter')
				(list, {
					checked : true
				});
		if (selectedInvoiceLineList.length < list.length) {
			$scope.selectRequestLine.AllVisible = false;
		}
	};

	$scope.SelectedShoppingListLine = function(list) {
		var selectedShoppingListLine = $filter('filter')
				(list, {
					checked : true
				});
		if (selectedShoppingListLine.length < list.length) {
			$scope.selectRequestLine.AllVisible = false;
		}
	};

	$scope.SelectedOrderLine = function(list) {
		var selectedOrderLineList = $filter('filter')(
				list, {
					checked : true
				});
		if (selectedOrderLineList.length < list.length) {
			$scope.selectRequestLine.AllVisible = false;
		}
	};

	// ADM-XXX
	$scope.SelectedTransportNoteLine = function(list) {
		var selectedTransportNoteLineList = $filter(
				'filter')(list, {
			checked : true
		});
		if (selectedTransportNoteLineList.length < list.length) {
			$scope.selectRequestLine.AllVisible = false;
		}
	};

	/* */
	$scope.selectAllVisible = function(requestLine) {
		if ($scope.selectRequestLine.AllVisible) {
			setAllRequestLineSelection(requestLine,
					false);
		} else {
			setAllRequestLineSelection(requestLine,
					true);
		}
	};

	var setAllRequestLineSelection = function(
			requestLine, isSelected) {
		if (!isNull(requestLine)) {

			for ( var i = 0; i < requestLine.length; i++) {
				if (isSelected) {
					requestLine[i].checked = true;
				} else {
					requestLine[i].checked = false;
				}

			}
		}
	};

	/*
	 * The showAddProductSection() method is used to
	 * show Add Product Table view for Product request
	 * base
	 */
	$scope.showAddProductSection = function() {
		if (!$rootScope
				.isShowDisableMenuItem('CON_ADD_PRODUCTS_TO_YOUR_CART')) {
			return;
		}
		var needToLogin = $rootScope.isSignOnRequired(
				'CON_ADD_PRODUCTS_TO_YOUR_REQUEST',
				null, false);
		if (!needToLogin) {
			$rootScope.isCollapsed_Products = !$rootScope.isCollapsed_Products;
			$rootScope.reqTemplate.checked = "product";
			$scope.requestForlabel = "CON_REFERENCE";
			$scope.requestObject
					.updateRequestBase($rootScope.reqTemplate.checked);
			$scope.requestLineObj.showRequestLines = false;
			if ($rootScope.isCollapsed_Products) {
				$scope.requestLineObj.init();
				$scope.requestLineObj
						.updateRequestLines();
				$rootScope.showRequestBase = false;
			}
		}
	};

	var isRequestInfoExist = function() {
		var requestInfoExist = false;
		if ($scope.requestObject.isReferenceAvailable()) {
			requestInfoExist = true;
		}
		if ($scope.requestLineObj.isRequestLineEmpty()) {
			requestInfoExist = true;
		}
		;
		return requestInfoExist;
	};

	$scope.onReqTypeChange = function() {
		setSelected(
				document
						.getElementById("reqTypeDropDown"),
				$scope.requestObject.probType.description,
				$timeout);
	};

	$scope.onAddProdReqTypeChange = function(selectRef,
			selectRefModel) {
		setSelected(
				angular.element($('#' + selectRef)).context.activeElement,
				selectRefModel.description, $timeout);
	};

	/*
	 * The changeView() is to change the label/control
	 * for the selected request base
	 */
	$scope.changeView = function() {
		var previousState = $scope.requestState;
		var previousRequestFor = $scope.requestForlabel;
		var PreviousChecked = "";
		$scope.requestObject.clearUIErrorKey();
		if (previousRequestFor == "CON_ORDER") {
			PreviousChecked = "order";
		} else if (previousRequestFor == "CON_INVOICE") {
			PreviousChecked = "invoice";
			// ADM-XXX
		} else if (previousRequestFor == "CON_TRANSPORTNOTE") {
			PreviousChecked = "transportNote";
		} else {
			PreviousChecked = "product";
		}
		var state = REQ_SUBMIT_VIEW;
		switch ($rootScope.reqTemplate.checked) {
		case 'order':
			state = REQ_SUBMIT_ORDER_VIEW;
			$scope.requestForlabel = "CON_ORDER";
			break;
		// ADM-XXX
		case 'transportNote':
			state = REQ_SUBMIT_TRANSPORTNOTE_VIEW;
			$scope.requestForlabel = "CON_TRANSPORTNOTE";
			break;
		case 'invoice':
			state = REQ_SUBMIT_INVOICE_VIEW;
			$scope.requestForlabel = "CON_INVOICE";
			break;
		default:
			state = REQ_SUBMIT_VIEW;
			$scope.requestForlabel = "CON_REFERENCE";

		}
		;
		$scope.requestState = state;
		if ($state.current.name != REQ_SUBMIT_VIEW) {
			if (isRequestInfoExist()) {
				/*
				 * ADM-XX var msg =
				 * Util.translate('CON_DATALOST_MSG');
				 * var mode =
				 * $rootScope.openConfirmDlg(msg);
				 * mode.result.then(function () {
				 * //$scope.selectedRequest.selected =
				 * false; $scope.selectedRequest = {};
				 * $scope.requestState = state;
				 * $scope.selectRequestLine = {};
				 * $scope.requestObject.clear();
				 * $scope.requestLineObj.clear();
				 * $scope.requestLineObj.showRequestLines =
				 * false;
				 * $scope.requestLineObj.requestParam="";
				 * $rootScope.requestLineSelectionMsg =
				 * null;
				 * $scope.requestObject.updateRequestBase($rootScope.reqTemplate.checked);
				 * $state.go(REQ_SUBMIT_VIEW); },
				 * function () { $scope.requestState
				 * =previousState;
				 * $scope.requestForlabel =
				 * previousRequestFor;
				 * $rootScope.reqTemplate.checked=PreviousChecked;
				 * 
				 * });
				 */
			}

		} else {

			// $scope.selectedRequest.selected = false;
			$scope.requestState = state;
			$scope.selectRequestLine = {};
			$scope.selectedRequest = {};
			$scope.requestObject.clear();
			$scope.requestObject
					.updateRequestBase($rootScope.reqTemplate.checked);
		}

	};

	/*
	 * The changeState() is to open the order list /
	 * invoice list based on the request base selection
	 */
	$scope.changeState = function() {
		$scope.requestObject.clearUIErrorKey();
		if ($rootScope.reqTemplate.checked != "product") {
			$rootScope.showRequestBase = true;
			$rootScope.isCollapsed_Products = false;
			DataSharingService.setObject(
					"requestObjectInfo",
					$scope.requestObject);
			DataSharingService.setObject(
					"requestLineObj",
					$scope.requestLineObj);
			if (isReferenceBlank($scope.requestObject)) {
				$state.go($scope.requestState);
			} else {
				var stateParam = getStateParamObject($scope.requestObject);
				$state.go($scope.requestState,
						stateParam);
			}
		}
	};

	var isReferenceBlank = function(referenceObject) {
		var viewSelection = $rootScope.reqTemplate.checked;
		var bool = false;
		switch (viewSelection) {
		case "order":
			if (isNull(referenceObject.reference)) {
				bool = true;
			}

			break;

		case "invoice":
			if (isNull(referenceObject.reference)
					&& isNull(referenceObject.docType)) {
				bool = true;
			}

			break;

		case "transportNote":
			if (isNull(referenceObject.reference) || referenceObject.reference == "") {
				bool = true;
			}

			break;
		}
		return bool;

	};

	var getStateParamObject = function(referenceObject) {
		var stateObject = {};
		if ($rootScope.reqTemplate.checked == "invoice") {
			if (!isNull(referenceObject.reference)) {
				stateObject['INVOICE_NUM'] = referenceObject.reference;
			}
			if (!isNull(referenceObject.docType))
				stateObject['DOC_TYPE'] = referenceObject.docType;

		}
		;
		if ($rootScope.reqTemplate.checked == "order") {
			if (!isNull(referenceObject.reference)) {
				stateObject['ORDER_NUM'] = referenceObject.reference;
			}
		}
		;
		// ADM-XXX
		if ($rootScope.reqTemplate.checked == "transportNote") {
			if (!isNull(referenceObject.reference)) {
				//stateObject['TRANSPORTNOTE_NUM'] = referenceObject.reference;
			}
		}
		;
		return stateObject;

	};

	/*
	 * The showAddProduct() is to show the Add product
	 * table for the selected request base
	 */
	$scope.showAddProduct = function(reqLineList,
			isInvoiceType,
			isShoppingListType /* ADM-XXX */,
			isTransportNoteType) {
		var list = getSelectedRequestLines(reqLineList);
		$rootScope.requestLineSelectionMsg = "";
		$rootScope.requestLineSelected = true;
		if (list.length > 0) {

			var reference = "";
			var docType = "";
			if (isInvoiceType && !isShoppingListType /* ADM-XXX */
					&& !isTransportNoteType) {

				refernceObject = DataSharingService
						.getFromSessionStorage("invoice_clickedDetail");
				reference = refernceObject.invoiceNumber;
				docType = refernceObject.documentType;

			}
			if (!isInvoiceType && !isShoppingListType /* ADM-XXX */
					&& !isTransportNoteType) {
				refernceObject = DataSharingService
						.getFromSessionStorage("order_clickedDetail");
				reference = refernceObject.orderNumber;
				docType = "";

			}
			// ADM-XXX
			if (!isInvoiceType && !isShoppingListType
					&& isTransportNoteType) {
				refernceObject = DataSharingService
						.getFromSessionStorage("transportNote_clickedDetail");
				reference = "" //refernceObject.TransportNoteNumber;
				docType = "";

			}
			if (!isInvoiceType && isShoppingListType) {

			}

			$scope.requestObject.updateReference(
					reference, docType);
			$scope.requestLineObj.isValidProductList = true;
			$scope.requestLineObj
					.createListFromSelection(
							list,
							isInvoiceType,
							isShoppingListType/* ADM-XXX */,
							isTransportNoteType);

			$state.go(REQ_ADD_PRODUCT_VIEW);

		} else {
			$rootScope.requestLineSelected = false;
			$rootScope.requestLineSelectionMsg = Util
					.translate('MSG_NO_SELECTION');

		}

	};

	/*
	 * The showNewRequestLine() is to show the newly
	 * created request lines for the selected request
	 * base
	 */
	// $scope.showNewRequestLine =
	// function(requestLineList)
	$rootScope.showNewRequestLine = function(
			requestLineList) {
		// console.log("new request line = "+
		// $scope.requestLineObj.requestParam);
		DataSharingService.setObject(
				"requestLineObject",
				$scope.requestLineObj);
		if (isNull($scope.requestLineObj.UIErrorKey)) {
			$rootScope.isCollapsed_Products = false;
			$scope.requestLineObj.showRequestLines = true;
			$state.go(REQ_NEW_LINE_VIEW);
		}

	};

	/*
	 * The submitRequest() method will be used for
	 * submitting the new request
	 */
	var submitRequest = function(requestParam) {
		// var requestParam =
		// $scope.requestObject.getRequestParam($scope.requestLineObj.requestParam);
		DataSharingService.setObject("requestBPObject",
				$scope.requestObject);
		DataSharingService.setObject("requestForlabel",
				$scope.requestForlabel);
		DataSharingService.setObject("newRequestParam",
				requestParam);

		DataSharingService.setToSessionStorage(
				"pageRequestBPObject",
				$scope.requestObject);
		DataSharingService.setToSessionStorage(
				"pageRequestForlabel",
				$scope.requestForlabel);
		DataSharingService.setToSessionStorage(
				"pageNewRequestParam", requestParam);
		DataSharingService.setToSessionStorage(
				"pageRequestLineObject",
				$scope.requestLineObj);

		// $log.log("new request param : "+
		// requestParam);

		$state.go(REQ_SUBMIT_CONFIRMATION);

		// RequestSearchService.submitNewRequest(requestParam);

	};

	/* validating the request parameters */
	var setValidationMessage = function(data,
			requestParam) {

		var requestReference = $scope.requestObject.reference;
		var requestBase = $scope.requestObject.requestBase;
		var invalidStr = "";
		if (!isNull(data)) {
			var messageCode = data[0].messageCode;
			// console.log("request error code :
			// "+messageCode);
			switch (messageCode) {
			case 4302:
				$scope.requestObject.UIErrorKey = 'MSG_REQUEST_ORDER_NUMBER_MUST_BE_ENTERED';
				$scope.requestObject.UIErrorParams = [];
				$scope.requestObject.ErrOnControl['orderNum'] = true;
				break;
			case 4303:
				$scope.requestObject.UIErrorKey = 'MSG_REQUEST_TYPE_MUST_BE_SELECTED';
				$scope.requestObject.UIErrorParams = [];
				$scope.requestObject.ErrOnControl['orderNum'] = true;
				break;
			case 4305:
				$scope.requestObject.UIErrorKey = 'MSP_INVALID';
				invalidStr = Util
						.translate('CON_ORDER');
				$scope.requestObject.UIErrorParams = [ invalidStr ];
				$scope.requestObject.ErrOnControl['orderNum'] = true;
				$scope.requestObject.clear();
				break;
			case 4306:
				$scope.requestObject.UIErrorKey = 'MSP_INVALID';
				invalidStr = Util
						.translate('CON_INVOICE');
				$scope.requestObject.UIErrorParams = [ invalidStr ];
				$scope.requestObject.ErrOnControl['orderNum'] = true;
				$scope.requestObject.clear();
				break;
			case 4307:
				$scope.requestObject.UIErrorKey = 'MSG_REQUEST_INVOICE_NUMBER_MUST_BE_ENTERED';
				$scope.requestObject.UIErrorParams = [];
				$scope.requestObject.ErrOnControl['orderNum'] = true;
				$scope.requestObject.clear();
				break;
			case 4310:
				submitRequest(requestParam);
				$scope.requestObject.ErrOnControl = {};
				break;
			default:
				$scope.requestObject.ErrOnControl = {};

			}
			;

		} else {
			$scope.requestObject.UIErrorKey = 'CON_BP_REQUEST';
			$scope.requestObject.UIErrorParams = [];

		}
	};

	$scope.validateNewRequest = function() {
		var requestParam = $scope.requestObject
				.getRequestParam($scope.requestLineObj.requestParam);
		if (!isEmptyString(requestParam)) {
			RequestSearchService.validateNewRequest(
					requestParam).then(
					function(data) {
						setValidationMessage(data,
								requestParam);
					});
		}

	};

	$scope.clearAllData = function() {
		var msg = Util.translate('CON_CLEAR_INPUT');
		var mode = $rootScope.openConfirmDlg(msg);
		mode.result.then(function() {
			$scope.clearData();
		}, function() {
		});
	};

	$scope.clearData = function() {
		$scope.requestObject.clear();
		$rootScope.listOfRequest = null;
		$rootScope.isCollapsed_Products = false;
		$scope.selectRequestLine.AllVisible = false;
		$scope.requestLineObj.showRequestLines = false;
		$scope.requestLineObj.clear(); //ADM-XXX
	};

	$scope.lineRemoved = function() {
		if ($scope.requestLineObj.getLineList().length <= 0) {
			$scope.clearData();
		}
	};

	$scope.deleteSelectedRequestLine = function(
			requestItemIndex, requestItem) {
		var msg = Util.translate(
				'CON_DELETE_REQUEST_LINE', null);
		var mode = $rootScope.openConfirmDlg(msg,
				[ requestItem.product ]);
		mode.result.then(function() {
			$scope.requestLineObj
					.deleteLine(requestItemIndex);
			$scope.lineRemoved();
		}, function() {
			//console.log('Modal dismissed at: ' + new Date());
		});
	};

	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////

	//ADM-XXX
	// Validate the entered data of each line 
	$scope.validateEnteredData = function(
			lineListObject) {
		var lineList = lineListObject.getLineList();
		for ( var i = 0; i < lineList.length; i++) {

			line = lineList[i];
			line.isValidProduct = true;
			line.validDate = true;
			line.validQuantity = true;
			line.invalidUnit = false;

			// Validate unit
			if (isNull(line.unit)
					|| isEmptyString(line.unit)) {
				line.invalidUnit = true;
				line.isValidProduct = false;
			}
			
			// Validate quantity
			var qtn = line.ordered || line.quantity;
			if (isEmptyString(qtn)) {
				line.errorMsg = Util
						.translate('MSG_QUANTITY_NOT_SPECIFIED');
				line.validQuantity = false;
				line.isValidProduct = false;
			} else {
				if ($rootScope.checkValidQuantity(line,
						qtn)) {
					line.errorMsg = line.ququantityBoxMsg;
					line.validQuantity = false;
					line.isValidProduct = false;
				} else {
					$scope.validateRequestLineData(line, lineListObject);
				}
			}
		}
	};

	//ADM-XXX
	$scope.validateDateBpr = function(reqLine) {
		var dateFormat = $rootScope.webSettings.defaultDateFormat;
		var expireDate = reqLine.expireDate;
		if (expireDate == "" || expireDate == null) {
			reqLine.validDate = true;
			$scope.requestLineObj.isValidProductList = true;
		} else {
			var dateValidator = new DateValidator(
					dateFormat);
			var isValidDate = dateValidator
					.isValidDate(expireDate);
			if (!isValidDate) {
				reqLine.validDate = false;
				reqLine.errorMsg = Util
						.translate('MSG_INVALID_DATE');
				$scope.requestLineObj.isValidProductList = false;
			} else {
				reqLine.validDate = true;
				$scope.requestLineObj.isValidProductList = true;
			}
		}
	};

	// ADM-XXX
	$scope.validateRequestLineData = function(line, lineListObject) {
		var requestParam = "requestLineData=["
				+ JSON.stringify(line) + "]";
		if (!isEmptyString(requestParam)) {
			RequestSearchService.validateRequestLine(
					requestParam).then(function(data) {
				evaluateStatus(data, line);
				validateAllLines(lineListObject);
			});
		}
	};

	//ADM-XXX
	var evaluateStatus = function(data, line) {
		if (!isNull(data)
				&& !isNull(data[0].messageCode)) {
			var messageCode = data[0].messageCode;
			switch (messageCode) {
			case 4310:
				line.isValidProduct = true;
				line.errorMsg = "";
				break;
			case 4402:
				line.isValidProduct = false;
				line.validQuantity = false;
				line.errorMsg = Util.translate('MSG_ENTERED_QTY_IS_GREATER_THAN_EXPECTED', [line.quantity]);
				break;
			case 4403:
				line.isValidProduct = false;
				line.validQuantity = false;
				line.errorMsg = Util.translate('MSG_NARCOTIC_ITEM_NOT_ALLOWED');
				break;
			case 4404:
				line.errorMsg = Util.translate('MSG_TRANSPORT_NOTE_DATE_TOO_OLD');
				break;
			default:
				line.isValidProduct = false;
				line.validQuantity = false;
				line.errorMsg = "Error during request line validation";//Util.translate('MSG_INVALID_DATE');
			}
		} else {
			line.isValidProduct = false;
			line.validQuantity = false;
			line.errorMsg = "Unable to validate the request line";//Util.translate('MSG_INVALID_DATE');
		}
	};

}]);
