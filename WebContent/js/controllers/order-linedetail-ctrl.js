
//
catalougeControllers.controller('orderLineDetailCtrl', ['$rootScope','$scope', '$http','$log', 'DataSharingService','OrderService', 'Util','$state',
                                                            function ($rootScope,$scope, $http,$log,DataSharingService,OrderService,Util,$state) {
	var orderLineDetailCtrlInit = function(){
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
	};
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	//	$log.log(" orderLineDetailCtrl history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	var initController = function(){
		var requestParams = [];
		$scope.dataFound = false;
		$scope.resultMsg ="";
		$scope.errorMessage = "";
		$scope.callState = $state.current.name;
		//var orderLine = DataSharingService.getObject("orderLine_clickedDetail");
		var orderLine = DataSharingService.getFromSessionStorage("orderLine_clickedDetail");
		//var order = DataSharingService.getObject("orderDetail");
		var order = DataSharingService.getFromSessionStorage("order_clickedDetail");
				
		if( orderLine !=null){
			$scope.orderLine = orderLine;
			$scope.order = order;
			if(orderLine.vatFlag && !orderLine.isShowLineDiscount){
				$scope.headingPrice = Util.translate('COH_PRICE_INCL_VAT',null);	
			}else{	
				$scope.headingPrice = Util.translate('CON_DISCOUNTED_PRICE_EXCL_VAT',null);	
			}
			
			//console.log("orderLine line detail "+ JSON.stringify(orderLine));
			//console.log("orderLine line detail requestParams"+ JSON.stringify(requestParams));
		};
	};
	orderLineDetailCtrlInit();
}]);