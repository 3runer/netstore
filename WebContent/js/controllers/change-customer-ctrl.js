catalougeControllers.controller('changeCustomerCtrl', ['$rootScope','$scope','$stateParams', '$http','$log', '$q','authentication','NewConfigService','DataSharingService','AccountService','ShoppingCartService', 'Util','$state',
                                                            function ($rootScope,$scope,$stateParams, $http,$log, $q,authentication,NewConfigService,DataSharingService,AccountService,ShoppingCartService,Util,$state) {
	var changeCustomerCtrlInit = function(){
 		/*if(historyManager.isBackAction){
 			historyManager.loadScopeWithCache($scope);
 			$scope.$root.$eval();
 		}else{
 			*/setupChangeCustomerCtrl();
 		//}
 	};
            	
 	$scope.$on('$destroy', function(event){ 
 		historyManager.pushScopeObject($scope);
 		//$log.log(" changeCustomerCtrl history manager " + JSON.stringify(historyManager.stateCacheList));
 	});

 	var setupChangeCustomerCtrl = function(){ 
 		makeCustomerSearchFilters();
 		$scope.changeCustObj = new ChangeCustomer();
 		if($scope.changeCustObj.typeList[0].value==""){
 			$state.go(CONST_State_Home);
 		}
		$scope.cachedPaginationObj = null;
 		$scope.changeCustObj.sort.ASC('CustomerCode');
 		var params = $scope.changeCustObj.getSearchCustParams();
 		//getPaginationObject('getCustomersListUrl',params);
		//$scope.changeCustObj.setPaginationObj($scope.paginationObj);
		getCustomerList(params,FirstPage);
 	};
 	
	// Customer search Filter 
	var makeCustomerSearchFilters = function(){
		var finalFilterList = new FilterList();
		var staticFilterConfig = AccountService.getCustomerSearchFilterConfig();
		var filterDataList = DataSharingService.getFilterDetailsSettings();
		 
		var dynamicFilterConfig = null;
		AccountService.getCustomerSearchFilterList().then(function(data){
				dynamicFilterConfig = data.filterBeans;
				finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
				$scope.changeCustObj.setFilterList(finalFilterList);
				//console.log("makeCustomerSearchFilters : " + JSON.stringify(finalFilterList));
		});
	};
	
	$scope.sortASC = function(column){
		$scope.changeCustObj.sort.ASC(column);
		sorting();
	};
	
	$scope.sortDESC = function(column){
		$scope.changeCustObj.sort.DESC(column);
		sorting();
	};
	

	
	var sorting = function(){
		var params = $scope.changeCustObj.getSearchCustParams();
		//initPaginationObject(params);
		//$scope.changeCustObj.setPaginationObj($scope.paginationObj);
		getCustomerList(params,FirstPage);
	};
	
	var setUIData = function(data,isFirstPageData){
		if(!isNull(data)){
			$scope.changeCustObj.setDatabaseDefaultCustCode($rootScope.webSettings.defaultCustomerCode);
			if(!isNull(data.defaultCustomer)){
				$scope.changeCustObj.setDefaultCustomer(data.defaultCustomer);
			}
			$scope.changeCustObj.setCustomerList(data.customerList,isFirstPageData);
			$scope.changeCustObj.sort.sorting(data.sortBy, data.orderBy);
			
			$rootScope.cachePaginationObject('getCustomersListUrl' , data.customerList, $scope);
			if(isFirstPageData && (! Array.isArray(data.customerList) ||  !data.customerList.length > 0 ) ){
				$rootScope.applyPreviousPaginationObject('getCustomersListUrl' , $scope);
			} else {
				if(!isNull(data.moreRecords)) $scope.changeCustObj.moreDataArrow = data.moreRecords;
			}
		}else{
			$scope.changeCustObj.setCustomerList(null,isFirstPageData);
			if(isFirstPageData){
				$rootScope.applyPreviousPaginationObject('getCustomersListUrl' , $scope);
			}
		}
	};
	
	var getCustomerList = function(requestParams,page){

		$scope.paginationObj = $rootScope.getPaginationObject('getCustomersListUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('getCustomersListUrl',requestParams,page ).then(function(data){
				if($scope.paginationObj.getPageNo()==1){
					setUIData(data,true);
				}else{
					setUIData(data,false);
				}
			});
		}
	};
	
	$scope.previousPage = function(){		
		var params = $scope.changeCustObj.getSearchCustParams();
		getCustomerList(params,PrevPage);		
	};
	$scope.nextPage = function(){	
		var params = $scope.changeCustObj.getSearchCustParams();
		getCustomerList(params,NextPage);		
	};
			
	$scope.getFirstPage = function()
	{	var params = $scope.changeCustObj.getSearchCustParams();		
		getCustomerList(params,FirstPage);
	};
	
	$scope.changeCustomer = function(){
		var params = $scope.changeCustObj.getChangeCustomerParams();
		if(Array.isArray(params) && params.length > 0){
			AccountService.changeCustomer(params).then(function(data){
				$scope.changeCustObj.setResponse(data);
				if($scope.changeCustObj.isChangeCustomerSuccessful){
					if(params[3] == "default"){
						showMsgForLoginAfterChangeCust();
					}else{
						refreshData();
						$rootScope.scrollToId("result_set");
					}
				}
			});
		}
		refreshData();
	};
	
	var showMsgForLoginAfterChangeCust = function(){
		var translatedMsgList = [];
		translatedMsgList.push(Util.translate('MSG_LOGIN_AFTER_CHANGE_CUSTOMER'));
		var dlg = $rootScope.openMsgDlg("",translatedMsgList);
		dlg.result.then(function () {
			authentication.closeSession();
			$rootScope.reloadHomePage();
		}, function () {
			authentication.closeSession();
			$rootScope.reloadHomePage();
		});
	};
	
	var refreshData = function (){
		NewConfigService.loadWebSettings().then(function(data){
			getCustomerList($scope.changeCustObj.getSearchCustParams(),FirstPage);
			authentication.loadFilterDetailsSettings();
			ShoppingCartService.getTemporaryOrderData([]);
			$rootScope.shoppingList_lineCount =$rootScope.webSettings.numOfShopListItemLines;
		});
	};
	
	$scope.selectCustomer = function(){
		if(!isNull($scope.changeCustObj.selectedCustomer)){
			$scope.changeCustObj.selectedCustomer.changeType = $scope.changeCustObj.typeSession;
		}
		var params = $scope.changeCustObj.getChangeCustomerParams();
		if(Array.isArray(params) && params.length > 0){
			$rootScope.openLoader();
			AccountService.changeCustomer(params).then(function(data){
				$scope.changeCustObj.setResponse(data);
				if($scope.changeCustObj.isChangeCustomerSuccessful){
					var loginResponse = DataSharingService.getObject("SELECT_CUSTOMER_USER_OBJECT");
					var callState = DataSharingService.getObject("SELECT_CUSTOMER_CALL_STATE");
					authentication.initSession(loginResponse);
					ShoppingCartService.getTemporaryOrderData([]);
					
					$rootScope.shoppingList_lineCount =$rootScope.webSettings.numOfShopListItemLines;
					if(!isNull($rootScope.closeSelectCustomerDlgOnsuccess)){
					$rootScope.closeSelectCustomerDlgOnsuccess();
					NewConfigService.loadCustomerDetail();
					$rootScope.closeLoader();
					}else{
						$rootScope.closeLoader();
					}
					if(isNull(callState)){
						$state.go($state.current.name,{},{reload:true});
					}else if (callState == 'home'){
						$rootScope.reloadHomePage();
					}else{
						$state.go(callState,{},{reload:true});
					}
				}
			});
		}
	};
	
	//applying filter 
	$scope.submitFilter = function(){
		$scope.changeCustObj.sort.ASC('CustomerCode');
		$scope.changeCustObj.isfilterOn = true;
		var params = $scope.changeCustObj.getSearchCustParams();
		//initPaginationObject(params);
		//$scope.changeCustObj.setPaginationObj($scope.paginationObj);
		getCustomerList(params,FirstPage);
	};
	
 	changeCustomerCtrlInit();
}]);
