
//Order Detail 
catalougeControllers.controller('orderDetailCtrl', ['$rootScope','$scope', '$http', '$stateParams', '$log', 'DataSharingService','OrderService', 'Util','$state',
                                                    function ($rootScope,$scope, $http, $stateParams, $log,DataSharingService,OrderService,Util,$state) {
	// new code
	var showLoading = true;
	var order =null;
	var orderDetailCtrlInit = function(){
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			order = DataSharingService.getFromSessionStorage("order_clickedDetail");
			$scope.$root.$eval();
		}else{
			initController();
		}
		$rootScope.requestLineSelectionMsg = null;
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	});
	
	var initController = function()
	{ 
		$scope.isUserSessionActive = true;
		$scope.cachedPaginationObj = null;
		$scope.paginationObj = null;
		$scope.isCollapsed = false;
		$scope.propertyList = [];	
		$scope.orderDetailProperty = [];
		$scope.callState = $state.current.name;
		$scope.itemSelected=false;
		if($scope.callState == REQ_SUBMIT_ORDER_DETAIL_VIEW){
			showLoading = false;
		}
		order = DataSharingService.getFromSessionStorage("order_clickedDetail");
		//var order = DataSharingService.getObject("order_clickedDetail");
		if(!isNull(order)){
			$scope.order = order;
		}
		var params = getRequestParams(); 	
		getOrderDetailPageData(params,FirstPage,showLoading);
//		});
		
	};
	
	$scope.showInsurancePercent = function(detail){
		var insurancePercent = false;
		if(!isNull(detail)){
			insurancePercent = true;
		}else{
			insurancePercent = false;
		}
		return insurancePercent;
	};
	
	var getRequestParams = function(){
		var param = [];
		var fromCart = DataSharingService.getFromSessionStorage("fromCart");
		var fromRequestPage = DataSharingService.getFromSessionStorage("fromRequestPage");
		if(fromCart){
			//var orderNumber = DataSharingService.getFromSessionStorage("shoppingCart_orderNumber");
			//param = ["OrderNumber",orderNumber];
			param = ["OrderNumber", $stateParams.ORDER_NUM];
		}else if(fromRequestPage){
		//	var request = DataSharingService.getObject("requestRef_clickedDetail");
			var request = DataSharingService.getFromSessionStorage("requestRef_clickedDetail");
			if(!isNull(request)){
				param = ["OrderNumber", $stateParams.ORDER_NUM];
			}
			
		} else{
		//param = ["OrderNumber", $stateParams.ORDER_NUM];
		param = ["OrderNumber",order.orderNumber];
		}
		//console.log("order detail requestParams"+ JSON.stringify(param));
		return param;
	};
	
	/*
	 * getRequestParams_old is deprecated 
	 */
	var getRequestParams_old = function(){
		var param = [];
		var fromCart = DataSharingService.getFromSessionStorage("fromCart");
		var fromRequestPage = DataSharingService.getFromSessionStorage("fromRequestPage");
		
		if(fromCart){
//			var orderNumber = DataSharingService.getObject("shoppingCart_orderNumber");
			var orderNumber = DataSharingService.getFromSessionStorage("shoppingCart_orderNumber");
			param = ["OrderNumber",orderNumber];
		} else if(fromRequestPage){
		//	var request = DataSharingService.getObject("requestRef_clickedDetail");
			var request = DataSharingService.getFromSessionStorage("requestRef_clickedDetail");
			if(!isNull(request)){
				param = ["OrderNumber", request.reference];
			}
			
		} else {
			//var order = DataSharingService.getObject("order_clickedDetail");
			var order = DataSharingService.getFromSessionStorage("order_clickedDetail");
			if(!isNull(order)){
				param = ["OrderNumber",order.orderNumber];
			}
			
		}
		//console.log("order detail requestParams"+ JSON.stringify(param));
		return param;
	};
	
	var getOrderDetailRequestParam = function(orderNumber){
		var param = ["OrderNumber", orderNumber];
		return param;
	};
	
	var getOrderDetailPageData =  function(requestParams,page,showLoading){
		$scope.paginationObj = $rootScope.getPaginationObject('orderDetailsUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		var pageNo = "";
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('orderDetailsUrl',requestParams,pageNo,showLoading ).then(function(data){
				$scope.itemSelected=false;
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true);
				}else{
					setUIPageData(data,false);
				}
			});
		}
	};
	
	var setUIPageData = function(data,isFirstPageData){
		var ItemList = data.orderLineList;
		if(isNull(data)){
			setOrderLineList(null,isFirstPageData); 
			if(isFirstPageData){
				$rootScope.applyPreviousPaginationObject('orderDetailsUrl' , $scope);
			}

		}else{
			//$rootScope.orderLineData = [];
			if(angular.isDefined(data.orderLineList) && !isNull(data.orderLineList)){
				setOrderDetailExtendedData(data.orderLineList); 
				ItemList =  data.orderLineList;
				$scope.initBuyObject(ItemList);
				$rootScope.orderDetailProperty = data.orderLineList[0];
				$scope.propertyList = Util.getObjPropList(data.orderLineList[0]);	
				addpropertyToList(ItemList,'validQuantity',true);
				//console.log("orderDetailCtrl : orderLineData length--  " +ItemList.length);

			}
			$scope.orderDetailsPropList = Util.getObjPropList(data);	
			$scope.orderDetails = data;
			$rootScope.changeHomePageTitle(Util.translate('TXT_PAGE_TITLE_ORDER_WITH_NUMBER') + $scope.orderDetails.orderNumber, true);
			DataSharingService.setObject("orderDetail",data);
			setOrderLineList(ItemList,isFirstPageData); 
			$rootScope.cachePaginationObject('orderDetailsUrl' , ItemList, $scope);
			if(isFirstPageData && (! Array.isArray(ItemList) ||  !ItemList.length > 0 )){
				$rootScope.applyPreviousPaginationObject('orderDetailsUrl' , $scope);
			}

		}
		$rootScope.setPageLoadMsg(ItemList,isFirstPageData,$scope);
		//$log.log("object info : "+JSON.stringify($scope.invoiceDetails));
		$scope.$root.$eval();
	};

	var setOrderLineList = function(itemList,isFirstPageData){
		if(Array.isArray(itemList) && itemList.length > 0 ){
			$scope.orderLineData = itemList;		
		}else{
			if(!isFirstPageData){
				$scope.orderLineData = [];
			}
		}
		if(isNull(itemList)){
			$scope.orderLineData = [];
		}
	};
	
	$scope.previousPage = function(){		
		 var params = getRequestParams();		
		 getOrderDetailPageData(params,PrevPage,true);
	};
	
	$scope.nextPage = function(){	
		 var params = getRequestParams();		
		 getOrderDetailPageData(params,NextPage,true);
	};
			
	$scope.getFirstPage = function()
	{			
		 var params = getRequestParams();		
		 getOrderDetailPageData(params,FirstPage,true);
				
	};
	// old code
//	var requestParams = [];
//	$scope.dataFound = false;
//	$scope.resultMsg ="";
//	$scope.errorMessage = "";
//	$scope.propertyList = [];
//	$scope.callState = $state.current.name;
////	$scope.prevState = 'home.quotation';
////	DataSharingService.setObject("pageState",$state.current.name);
//	$scope.orderDetailProperty = [];
//	var showLoading = true;
//	if($scope.callState == REQ_SUBMIT_ORDER_DETAIL_VIEW){
//		showLoading = false;	}
//	else
//		{
//			showLoading = true;
//		};
//
//	var order = DataSharingService.getObject("order_clickedDetail");
//	$log.log("order detail "+ JSON.stringify(order));
//	if( order !=null){
//		$scope.order = order;
//		requestParams = ["OrderNumber",order.orderNumber];
//		$log.log("order detail "+ JSON.stringify(order));
//		$log.log("order detail requestParams"+ JSON.stringify(requestParams));
//	};
//	
//	if($rootScope.fromCart)
//		{
//			var orderNumber = DataSharingService.getObject("shoppingCart_orderNumber");
//			requestParams = ["OrderNumber",orderNumber];
//		}
//	if($rootScope.fromRequestPage)
//		{
//			var request = DataSharingService.getObject("requestRef_clickedDetail");
//			//var orderNumber = request.reference;
//			requestParams = ["OrderNumber", request.reference];
//		}
//	
//	var getOrderDetailRequestParam = function(orderNumber){
//		var requestParams = ["OrderNumber", orderNumber];
//		return requestParams;
//	};
//	
//	var getOrderDetails = function (requestParams){
//
//		OrderService.getOrderDetailList(requestParams,showLoading).then(function(data){
//			//if(data.orderLineList==null || data.orderLineList.length == 0){
//			if(isNull(data)){
//				$scope.errorMessage = data.errorMessage;			
//			}
//			else
//			{
//				$log.log("orderDetailCtrl messageCode -- "+data.messageCode);
//				//$rootScope.reLogin(data.messageCode);	
//				$rootScope.orderLineData = [];
//				if(angular.isDefined(data.orderLineList))
//					{
//					setOrderDetailExtendedData(data.orderLineList);
//					$rootScope.orderLineData = data.orderLineList;	
//					$scope.initBuyObject($rootScope.orderLineData);
//					$rootScope.orderDetailProperty = data.orderLineList[0];
//					$scope.propertyList = Util.getObjPropList(data.orderLineList[0]);							
//					$log.log("orderDetailCtrl : orderLine length--  " +data.orderLineList.length);
//					addpropertyToList(data.orderLineList,'validQuantity',true);
//					}
//				$scope.orderDetailsPropList = Util.getObjPropList(data);	
//				$scope.orderDetails = data;		
//				DataSharingService.setObject("orderDetail",data);
//				$scope.errorMessage = " ";	
//				$scope.dataFound = true;
//				
//				//only for testing remove it
//				//testOnly_allow_delete($scope.orderDetails);
//			}
//		});
//	};

	$scope.exportOrderDetail = function(){
		
		var ordersLines = getOrderLinesForExport($scope.orderDetails);
		var basicInfo = getBasicInfoForExport($scope.orderDetails);
		var orderText = getOrderTextForExport($scope.orderDetails);
		var orderReferences = getOrderReferencesForExport($scope.orderDetails);
		var orderFees = getOrderFeesForExport($scope.orderDetails);
		var addresses = getAddressesForExport($scope.orderDetails);

		var fileName = "OrderDetail_"+$scope.orderDetails.orderNumber+"_"+getCurrentDateTimeStr()+".pdf";
		var docHeader = Util.translate('CON_ORDER_NUMBER') + " " + $scope.orderDetails.orderNumber;
		saveDetailsPDF(ordersLines,basicInfo,orderText,orderReferences,orderFees,addresses,docHeader,fileName);
	};
	
	var getBasicInfoForExport = function(orderDetails){
		var basicInfo = new Table();
		basicInfo.heading = Util.translate('CON_BASIC_INFORMATION') ; 
		var row = [];
		row.push(Util.translate('COH_CUSTOMER'));
		row.push(orderDetails.customerCode);
		row.push(orderDetails.customerDesc);
		row.push("");
		row.push("");
		basicInfo.addRow(row);

		row = [];
		row.push(Util.translate('CON_ORDER_NUMBER'));
		row.push(orderDetails.orderNumber);
		row.push("");
		row.push(Util.translate('CON_ORDER_DATE'));
		row.push(orderDetails.orderDate);
		basicInfo.addRow(row);

		row = [];
		row.push(Util.translate('CON_ORDER_STATUS'));
		row.push(orderDetails.orderStatus);
		row.push("");
		row.push(Util.translate('CON_HELD'));
		row.push(Util.translate(orderDetails.held));
		basicInfo.addRow(row);

		row = [];
		row.push(Util.translate('COH_CURRENCY'));
		row.push(orderDetails.currencyCodeOrSymbol);
		row.push("");
		row.push("");
		row.push("");
		basicInfo.addRow(row);

		row = [];
		if(!isNull(orderDetails.orderValueExclVAT)){
			row.push(Util.translate('CON_ORDER_VALUE_EXCLUDING_VAT'));
			row.push(orderDetails.orderValueExclVAT);
		}
		row.push("");
		
		if(!isNull(orderDetails.orderValueInclVAT)){
			row.push(Util.translate('CON_ORDER_VALUE_INCLUDING_VAT'));
			row.push(orderDetails.orderValueInclVAT);
		}
		
		basicInfo.addRow(row);

		if(orderDetails.isCreditCardInfoFlag){
			row = [];
			row.push(Util.translate('CON_ORDER_PAYMENT'));
			row.push(orderDetails.creditCardCode);
			row.push("");
			row.push(Util.translate('COH_AMOUNT'));
			row.push(orderDetails.creditCardAmount);
			basicInfo.addRow(row);
		}

		if(!isNull(orderDetails.goodsMarking)){
			row = [];
			row.push(Util.translate('COH_GOODS_MARKING'));
			row.push(orderDetails.goodsMarking);
			row.push("");
			row.push("");
			row.push("");
			basicInfo.addRow(row);
		}

		if(!isNull(orderDetails.motCode)){
			row = [];
			row.push(Util.translate('CON_MANNER_OF_TRANSPORT'));
			row.push(orderDetails.motCode);
			row.push(orderDetails.motDesc);
			row.push("");
			row.push("");
			basicInfo.addRow(row);
		}
		if(!isNull(orderDetails.todCode)){
			row = [];
			row.push(Util.translate('CON_TERMS_OF_DELIVERY'));
			row.push(orderDetails.todCode);
			row.push(orderDetails.todDesc);
			row.push("");
			row.push("");
			basicInfo.addRow(row);
		}
		if(!isNull(orderDetails.topCode)){
			row = [];
			row.push(Util.translate('CON_TERMS_OF_PAYMENT'));
			row.push(orderDetails.topCode);
			row.push(orderDetails.topDesc);
			row.push("");
			row.push("");
			basicInfo.addRow(row);
		}
		return basicInfo;
	};

	var getOrderLinesForExport = function(orderDetails){
		var orderLines = new Table();
		orderLines.heading = Util.translate('CON_ORDER_LINES');
		
		orderLines.addHeader(Util.translate('COH_LINE'));
		orderLines.addHeader(Util.translate('COH_PRODUCT'));
		if(orderDetails.isDescFlag){
			orderLines.addHeader(Util.translate('COH_DESCRIPTION'));
		}
		orderLines.addHeader(Util.translate('COH_ORDERED_QUANTITY'));
		orderLines.addHeader(Util.translate('CON_CONFIRMED'));
		orderLines.addHeader(Util.translate('COH_UNIT'));
		if($rootScope.webSettings.allowDiscountDisplay =="*ALL" || $rootScope.webSettings.allowDiscountDisplay =="*ACTUAL_PRICE" || $rootScope.webSettings.allowDiscountDisplay =="*ALL_PRICE "){
			orderLines.addHeader(Util.translate('COH_ACTUAL_PRICE'));
		}
		if($rootScope.webSettings.allowDiscountDisplay =="*ALL" || $rootScope.webSettings.allowDiscountDisplay =="*DISCOUNT_PRICE " || $rootScope.webSettings.allowDiscountDisplay =="*ALL_PRICE "){
			orderLines.addHeader(Util.translate('COH_DISCOUNT_PRICE'));
		}
		if($rootScope.webSettings.allowDiscountDisplay =="*ALL"){
			orderLines.addHeader(Util.translate('COH_DISCOUNT_PERCENTAGE'));
		}

		orderLines.addHeader(Util.translate('COH_DELIVERY_DATE'));
		
		if($rootScope.webSettings.isShowPrice){
			var val = Util.translate('CON_VALUE');
			if(orderDetails.pricingUnitFlag){
				val = val + " " + orderDetails.currencyCodeOrSymbol;
			}
			orderLines.addHeader(val);
		}
		if(!isNull($scope.orderLineData)){
			for(var i=0;i< $scope.orderLineData.length;i++){
				var row = $scope.orderLineData[i];
				var colData = [];
				colData.push(row.lineNumber);
				colData.push(row.itemCode);
				if(orderDetails.isDescFlag){
					colData.push(row.itemDescription);
				}
				colData.push(row.ordered);
				
				colData.push(row.confirmed);
				colData.push(row.unit);
				if($rootScope.webSettings.allowDiscountDisplay =="*ALL" || $rootScope.webSettings.allowDiscountDisplay =="*ACTUAL_PRICE" || $rootScope.webSettings.allowDiscountDisplay =="*ALL_PRICE "){
					colData.push(row.actualPrice);
				}
				if($rootScope.webSettings.allowDiscountDisplay =="*ALL" || $rootScope.webSettings.allowDiscountDisplay =="*DISCOUNT_PRICE " || $rootScope.webSettings.allowDiscountDisplay =="*ALL_PRICE "){
					colData.push(row.discountPrice);
				}
				if($rootScope.webSettings.allowDiscountDisplay =="*ALL"){
					colData.push(row.discountPercent);
				}
				colData.push(row.delDate);
				if($rootScope.webSettings.isShowPrice){
					if(!row.lineErrorFlag){
						colData.push(row.value);
					}
					if(row.lineErrorFlag){
						colData.push(row.lineError);
					}
				}
				orderLines.addRow(colData);
			}
		}
		else{
			var colData = [];
			
				for(var i=0;i<orderLines.headers.length;i++){
					colData.push("");
				}
			orderLines.addRow(colData);
		}
		
		return orderLines;
	};	
	
	var getOrderReferencesForExport = function(orderDetails){
		var orderReferences = new Table();

		orderReferences.heading = Util.translate('CON_ORDER_REFERENCES') ; 

		/*var row = [];
		row.push(Util.translate('CON_HANDLER'));
		row.push(orderDetails.handlerCode);
		row.push(orderDetails.handlerDesc);
		orderReferences.addRow(row);

		row = [];
		row.push(Util.translate('CON_SALES_PERSON'));
		row.push(orderDetails.salespersonCode);
		row.push(orderDetails.salespersonDesc);
		orderReferences.addRow(row);
*/
		if(!isNull(orderDetails.yourOrder)){
			row = [];
			row.push(Util.translate('CON_YOUR_ORDER'));
			row.push(orderDetails.yourOrder);
			row.push("");
			orderReferences.addRow(row);
		}

		if(!isNull(orderDetails.yourReference)){
			row = [];
			row.push(Util.translate('COH_YOUR_REFERENCE'));
			row.push(orderDetails.yourReference);
			row.push("");
			orderReferences.addRow(row);
		}

		/*if(!isNull(orderDetails.ourReference)){
			row = [];
			row.push(Util.translate('CON_OUR_REFERENCE'));
			row.push(orderDetails.ourReference);
			row.push("");
			orderReferences.addRow(row);
		}*/

		return orderReferences;
	};

	var getOrderTextForExport = function(orderDetails){
		var orderText = null;
		if(orderDetails.orderTextFlag){
			orderText = new Table();
			orderText.heading = Util.translate('CON_ORDER_TEXT') ; 
			var row = [];
			row.push(orderDetails.orderText);
			orderText.addRow(row);
		}

		return orderText;
	};

	var getAddressesForExport = function(orderDetails){
		var orderAddresses = new Table(); 
		orderAddresses.heading = Util.translate('CON_ADDRESSES') ; 

		orderAddresses.addHeader(Util.translate('CON_CONFIRMATION_ADDRESS'));
		orderAddresses.addHeader(Util.translate('CON_DELIVERY_ADDRESS'));

		var row = [];
		row.push(orderDetails.confAddress.split('</br>'));
		row.push(orderDetails.delAddress.split('</br>'));
		orderAddresses.addRow(row);
		return orderAddresses;
	};

	var getOrderFeesForExport = function(orderDetails){
		var orderFees = null;
		if(orderDetails.isOrderFeesFlag){
			orderFees = new Table();
			orderFees.heading = Util.translate('CON_ORDER_FEES') ; 

			var row = [];
			row.push(Util.translate('CON_ADMINISTRATION_FEE'));
			row.push(orderDetails.administrationFee);
			orderFees.addRow(row);

			var row = [];
			row.push(Util.translate('CON_FREIGHT'));
			row.push(orderDetails.freight);
			orderFees.addRow(row);

			var row = [];
			row.push(Util.translate('CON_INVOICE_FEE'));
			row.push(orderDetails.invoiceFee);
			orderFees.addRow(row);

			var row = [];
			row.push(Util.translate('CON_POSTAGE'));
			row.push(orderDetails.postage);
			orderFees.addRow(row);

			var row = [];
			row.push(Util.translate('CON_INSURANCE')+": (%)");
			row.push(orderDetails.insurance);
			orderFees.addRow(row);

			var row = [];
			row.push(Util.translate('CON_SURCHARGE'));
			row.push(orderDetails.surcharge);
			orderFees.addRow(row);
		}

		return orderFees;
	};
	
// // Pagination code
	
////	requestParams = ["OrderBy",$scope.orderType,"SortBy",$scope.sortColumn];
//	var pageKey = Util.initPagination('orderDetailsUrl',requestParams);
//	
//	paginationObj = DataSharingService.getObject(pageKey);	
//	$rootScope.orderLineDataLoadMore = function(nextPageFlag)
//	{
//		$log.log("orderLineDataLoadMore() - called, isPaginationBlocked = "+ Util.isPaginationBlocked(pageKey).toString());		
//		if(!Util.isPaginationBlocked(pageKey))
//		{	
//			$log.log("orderLineDataLoadMore() - called" );
//			requestParams = ["OrderNumber",order.orderNumber];
//			Util.getPageinationData('orderDetailsUrl',requestParams,nextPageFlag ).then(function(data)
//			{
//				if(data.orderLineList==null || data.orderLineList.length == 0)
//				{	paginationObj.setLastPageNo();
//					$scope.dataFound = false;
//					$scope.orderDataLoadMsg = "No more data loaded";
//					$log.log("orderDetailCtrl --  No more data loaded");
//				}
//				else
//				{
//					$log.log("orderDetailCtrl messageCode -- "+data.messageCode);
//					//$rootScope.reLogin(data.messageCode);
//					$scope.dataFound = true;
//					$scope.orderDataLoadMsg = "";
//					setOrderDetailExtendedData(data.orderLineList);
//					$scope.orderData = data.orderLineList;
//					$scope.initBuyObject($scope.orderData);
//					requestParams = ["OrderNumber",order.orderNumber];
//					$scope.$root.$eval();					
//				}			
//				$log.log("paginationObj.isLastPageNo(): " +paginationObj.isLastPageNo());
//			});
//		}
//	};
//
//	$scope.dataPaging = function(nextPageFlag)
//	{
//		$rootScope.orderLineDataLoadMore(nextPageFlag);		
//	};
//	//  Pagination code ends
//	//
////
	

	$scope.isCollapsed = false;
	$scope.toggleCollapse = function()
	{
		var isCollapsed = !$scope.isCollapsed;
		return isCollapsed;
	};	


	$scope.isColExist = function(colPropName,propertyList){
		var isCol = false;
		var val = Util.isPropertyExist(colPropName,propertyList);
		if(val){
			isCol = true;

		}
		return isCol;
	};

	$scope.showOrderLineDetail = function(orderLine){
		//DataSharingService.setObject("orderLine_clickedDetail",orderLine);
		if($rootScope.isShowDisableMenuItem('CON_ORDER_LINE_INFORMATION')){
			if($scope.order != undefined){
			orderLine["orderNumber"] = $scope.order.orderNumber;
			}else{
			orderLine["orderNumber"] = $stateParams.ORDER_NUM;
			}
			DataSharingService.setToSessionStorage("orderLine_clickedDetail",orderLine);
			var needToLogin = $rootScope.isSignOnRequired('CON_ORDER_LINE_INFORMATION','home.orderline',true);
			if(!needToLogin){
				$state.go('home.orderline');
			}
		}
	};
	
	var testOnly_allow_delete  = function (orderDetails){
		orderDetails.isDeleteAllowedFlag = true;
		orderDetails.isDeleteLineAllowedFlag = true;
	};
	
	var handleDeleteOrderMessageCode =function (data,orderNumber){
		if(!isNull(data.messageCode)){
			var msgCode = data.messageCode+"";
			switch (msgCode){
			case '4000' :
				//DELETE SUCCESS FULL
				$state.go('home.order');
				break;
			case '4001' :
				//ORDER NOT FOUND
				$scope.errorMessage = Util.translate('MSG_ERROR_ORDER_NOT_FOUND',[orderNumber]);
				break;
			case '4002' :
				// IN USE 
				$scope.errorMessage = Util.translate('MSG_ERROR_ORDER_IN_USE',[orderNumber]);
				break;
			case '4003' :
				//DELETE NOT ALLOWED
				$scope.errorMessage = Util.translate('MSG_ERROR_ORDER_DELETE_NOT_ALLOWED',[orderNumber]);
				break;
			case '4004' :
				//ERROR
				$scope.errorMessage = Util.translate('MSG_ERROR_ORDER_DELETE',[orderNumber]);
				break;
			}
		}
	};
	
	var handleDeleteOrderLinesMsgCode =function (msgList,orderNumber){
		var translatedMsgList = [];
		var selectedLines = getSelectedOrderLines();
		for(var i=0;i<msgList.length;i++){
			var item =  msgList[i];
			if(!isNull(item.messageCode)){
				var msgCode = item.messageCode+"";
				switch (msgCode){
				case '4000' :
					//DELETE SUCCESS FULL
					//var param = getOrderDetailRequestParam(orderNumber);
					//getOrderDetails(param);
					translatedMsgList.push(selectedLines[i].lineNumber + " "+Util.translate('MSG_ERROR_ORDER_LINES_DELETED_SUCCESSFULLY')+ " ");
					break;
				case '4001' :
					//ORDER NOT FOUND
					translatedMsgList.push(selectedLines[i].lineNumber + " "+Util.translate('MSG_ERROR_ORDER_LINES_NOT_FOUND')+ " ");
					break;
				case '4002' :
					// IN USE 
					translatedMsgList.push(selectedLines[i].lineNumber + " "+Util.translate('MSG_ERROR_ORDER_LINES_IN_USE')+ " ");
					break;
				case '4003' :
					//DELETE NOT ALLOWED
					translatedMsgList.push(selectedLines[i].lineNumber + " "+Util.translate('MSG_ERROR_ORDER_LINES_DELETE_NOT_ALLOWED')+ " ");
					break;
				case '4004' :
					//ERROR
					translatedMsgList.push(selectedLines[i].lineNumber + " "+Util.translate('MSG_ERROR_ORDER_LINES_DELETE')+ " ");
					break;
				}
			}
		}
		//openMsgDlg
		var msg = Util.translate('CON_STATUS_DELETE_ORDER_LINE',null);
		var dlg = $rootScope.openMsgDlg(msg,translatedMsgList);
		dlg.result.then(function () {
			//console.log('Delete order modal Ok clicked');
		}, function () {
			//console.log('Delete order modal Cancel clicked');
			var param = getOrderDetailRequestParam(orderNumber);
//			getOrderDetails(param);
			getOrderDetailPageData(param,FirstPage,true);
			$scope.$root.$eval();
		});
		
		return messageString;
	};
	
	$scope.deleteOrder = function(){
		params = ["orderNum",order.orderNumber];
		var msg = Util.translate('CON_DELETE_ORDER',null);
		var dlg = $rootScope.openConfirmDlg(msg,[order.orderNumber]);
		dlg.result.then(function () {
			//console.log('Delete order modal Ok clicked');
			OrderService.deleteOrder(params).then(function(data){
				handleDeleteOrderMessageCode(data,order.orderNumber);
			});
		}, function () {
			//console.log('Delete order modal Cancel clicked');
		});
	};
	
	$scope.deleteOrderFromOrderPage = function(orderNumber){
		params = ["orderNum",orderNumber];
		var msg = Util.translate('CON_DELETE_ORDER',null);
		var dlg = $rootScope.openConfirmDlg(msg,[orderNumber]);
		dlg.result.then(function () {
			//console.log('Delete order modal Ok clicked');
			OrderService.deleteOrder(params).then(function(data){
				handleDeleteOrderMessageCode(data,orderNumber);
			});
		}, function () {
			//console.log('Delete order modal Cancel clicked');
		});
	};
	
	var createDeleteOrderLineList = function(orderNumber, selectedLines){
		var lines = [];
		for(var i=0;i<selectedLines.length ;i++){
			var lineObject = new Object();
			lineObject.orderNum = orderNumber;
			lineObject.lineNumber= selectedLines[i].lineNumber;
			lines.push(lineObject);
		}
		return lines;
	};
	
	var getDeleteOrderLinesParam = function(){
		var param = null;
		var selectedList = getSelectedOrderLines();
		if(selectedList.length > 0){
			ObjList =  createDeleteOrderLineList(order.orderNumber,selectedList);
			param = "deleteOrderLineData="+JSON.stringify(ObjList);
		}
		
		return param;
	};
	
	$scope.checkSelectedItems= function(){	
		var selectedOrderLines =getSelectedOrderLines();
	    if(selectedOrderLines.length>0){
	   		$scope.itemSelected=true;
	    }else{
	    	$scope.itemSelected=false;
	    }
	};
	
	var getSelectedOrderLines = function(){
		var selectedList = [];
		var list = $scope.orderLineData;
		for(var i=0;i<list.length ;i++){
			var item = list[i];
			if(item.checked){
				selectedList.push(item);
			}
		}
		//console.log("number of selected order lines = "  + selectedList.length );
		return selectedList;
	};
	
	$scope.deleteSelectedOrderLines = function(){
		var param = getDeleteOrderLinesParam();
		if(param!=null){
			var linesStr =  getCommaSeparatedStrings(getSelectedOrderLines(),'lineNumber');
			var msg = Util.translate('CON_DELETE_ORDER_LINE',null);
			var dlg = $rootScope.openConfirmDlg(msg,[linesStr]);
			dlg.result.then(function () {
				//console.log('Delete order modal Ok clicked');
				OrderService.deleteOrderLines(param).then(function(data){
					data = removeDefaultCallBack(data);
					handleDeleteOrderLinesMsgCode(data,order.orderNumber);
				});
			}, function () {
				//console.log('Delete order modal Cancel clicked');
			});
		}
	};
	
	var setOrderDetailExtendedData = function(list){
		//$log.log("setHomeCatalogExtendedData : product list : " +JSON.stringify($scope.productList));
		if(Array.isArray(list)){
			for (var i=0;i<list.length ; i++){
				var  item  = list[i];
				item.showAvailabilityAsLink = $rootScope.showAvailabilityAsLink();
				item.showAvailabilityAsImage = $rootScope.showAvailabilityAsImage();
				item.availabilityImageName = $rootScope.getAvailabilityImage(item.availabilityOfItem);
				item.availabilityText = $rootScope.getAvailabilityText(item.availabilityOfItem);
				item.showEnquiryImage = $rootScope.showEnquiryImage(item.enquiryImage);
				item.enquiryImage = $rootScope.getEnquiryImageName(item.enquiryImage);
				handleEmptyAvailabilityImage(item);
			}
		}
	};
	
	$scope.initBuyObject = function(list){
		$scope.buyObj = new Buy(list);
		$scope.buyObj.setPropNameItemCode('itemCode');
		$scope.buyObj.setPropNameOrdered('quantity');
		$scope.buyObj.setPropNameUnit('unit');
		$scope.buyObj.setPropNameShipmentMarking('shipmentMarking');
		$scope.buyObj.setPropNameDelDate('hereDelDateNotRequired');
		$scope.buyObj.setPropNameisBuyAllowed('isBuyAllowed');
	};
	
	$scope.orderDetailBuyItems = function(item){
		$rootScope.buyMultipleItems(item,$scope.buyObj);
	};
	
	$scope.addMultipleItemsToCurrentList = function(item){
		$rootScope.addMultipleItemsToList(item,$scope.buyObj);
	};
	
	$scope.showOrderReferences = function(){ 
		var needToLogin = $rootScope.isSignOnRequired('CON_ORDER_REFERENCE',null,false);	
		if(!needToLogin){
			$scope.collapsed_orderReferences= !$scope.collapsed_orderReferences;
		}
	};
//	getOrderDetails(requestParams);
	orderDetailCtrlInit();


}]);