//function menu controller
catalougeControllers.controller('catFunctionMenuCtrl', ['$rootScope','$scope', '$http','$log','$state','NewConfigService','DataSharingService','authentication','Util','MenuService',
                                                        function ($rootScope,$scope, $http,$log,$state,NewConfigService,DataSharingService,authentication,Util,MenuService) {

	$scope.menuData = [];
	var requestParams = [];
	//  to get function menu from server side
	$rootScope.initCommitsPromise.then(function(){
//		$http.get('./json/catalog/function_menu.json').success(function(data) { 
//
//			if (data.FunctionMenu.length > 0 ){
//				$scope.menuData = data.FunctionMenu;
//				$scope.menuList = $scope.menuData;
//			}else{
//				$scope.menuList = [];
//			}
//			DataSharingService.setObject("functionMenus",$scope.menuList);
//			$log.log("menu list size = " + $scope.menuList.length);
//		});
		
		$rootScope.getMenuList();
	
	});
	
	
	$rootScope.getMenuList = function(){
		Util.getDataFromServer('functionMenuUrl',[]).then(function(data){
			if (data.length > 0 ){
				$scope.menuList = data;
			}else{
				$scope.menuList = [];
			}
			DataSharingService.setObject("functionMenus",$scope.menuList);
			$rootScope.displayProductSearchBar = $rootScope.isShowDisableMenuItem('CON_PRODUCT');
			var isShowInit = DataSharingService.getFromSessionStorage("SHOW_WELCOME_MSG_INIT");;
			var isShow = DataSharingService.getFromSessionStorage("SHOW_WELCOME_MSG");
			var emailFromHome = DataSharingService.getFromSessionStorage("FROM_HOME_EMAIL");
			if(!isShowInit){
			if(isShow && $scope.menuList.length >1){
				$rootScope.showWelComeMsg();
			}else if(!isNull(emailFromHome) && $scope.menuList.length >1){
				$rootScope.resolvePendingSendEmail();
			}}
			// is price calculation enabled
			$rootScope.isPriceCalculationEnabled = false;
			$rootScope.showChangePassword = false;
			$rootScope.showForgotPassword = false;
			$rootScope.showNewAcc = false;
			$rootScope.showProductDetailSignon = false;
			$rootScope.showProductCatalogSignon = false;
			$rootScope.showCompare = false;
			for(var i = 0; i < $scope.menuList.length; i++) {
				if($scope.menuList[i].name == 'CON_SHOPPINGCART') {
					$rootScope.isShoppingCartPageEnabled = $scope.menuList[i].active;
				}
				if($scope.menuList[i].name == 'CON_WAREHOUSE_INFORMATION') {
					$rootScope.isWarehouseTabDisplay = $scope.menuList[i].active;
				}
				if($scope.menuList[i].name == 'CON_PRODUCT_COMPARE') {
					$rootScope.isCompareProductShowDisable = $scope.menuList[i].active;
				}
				if($scope.menuList[i].name == 'CON_PRICE_CALCULATION') {
					$rootScope.isPriceCalculationEnabled = $scope.menuList[i].active;
				}
				if($scope.menuList[i].name == 'CON_CHANGE_PASSWORD') {
					$rootScope.showChangePassword = $scope.menuList[i].active;
				}
				if($scope.menuList[i].name == 'CON_USER_PASSWORD_REQUEST') {
					$rootScope.showForgotPassword = $scope.menuList[i].active;
				}
				if($scope.menuList[i].name == 'CON_COPY/PASTE_ORDER_LIN') {
					$rootScope.showCopyPasteArea = $scope.menuList[i].active;
				}
				if($scope.menuList[i].name == 'CON_PRODUCT_INFORMATION') {
					$rootScope.showProductDetailSignon = $scope.menuList[i].signonRequired;
					$rootScope.isProductDetailPageEnabled = $scope.menuList[i].active;
				}
				if(($scope.menuList[i].name == 'CON_USER') && !isNull($scope.menuList[i].subMenu) && ($scope.menuList[i].subMenu.length > 0)) {
					for(var j = 0; j < $scope.menuList[i].subMenu.length; j++) {
						if($scope.menuList[i].subMenu[j].name == 'CON_NEW_ACCOUNT') {
							$rootScope.showNewAcc = $scope.menuList[i].subMenu[j].active;
							break;
						}
					}
				}
				if($scope.menuList[i].name == 'CON_PRODUCT_CATALOGUE') {
					$rootScope.showProductCatalogSignon = $scope.menuList[i].signonRequired;
				}
				if($scope.menuList[i].name == 'CON_PRODUCT_COMPARE') {
					$rootScope.showCompare = $scope.menuList[i].active;
				}
			}
			
			$rootScope.$broadcast('menuUpdated');
		});
	};
	
	
	$rootScope.shoppingCart_signOnRequired = true;
	
	$rootScope.getCustomerName = function() {
		if(!isNull($rootScope.webSettings) && !isNull($rootScope.webSettings.defaultCustomerName)) {
			return $rootScope.webSettings.defaultCustomerName;
		}
		if(!isNull($scope.menuList) && ($scope.menuList.length > 0)) {
			for(var i = 0; i < $scope.menuList.length; i++) {
				if(($scope.menuList[i].name == 'CON_USER') && (!isNull($scope.menuList[i].customerName))) {
					return $scope.menuList[i].customerName;
				}
			}
		} else {
			return "";
		}
		return "";
	}
	
	$rootScope.hasActiveSubMenu = function(menuItem) {
		if(!isNull(menuItem) && !isNull(menuItem.subMenu) && (menuItem.subMenu.length > 0)) {
			for(var i = 0; i < menuItem.subMenu.length; i++) {
				if(menuItem.subMenu[i].active == true) {
					return true;
				}
			}
		} else {
			return menuItem.active;//true;
		}
		return false;
	}
	
$rootScope.isLoginRequired = function (signonRequired,menuItem){
	    var stateParameter={};
		if(menuItem.active){
			var loginStatus = authentication.isLoggedIn();
			//callState = CONST_State_Home; 
			switch (menuItem.name) { 
			case "CON_INVOICE": 
				callState = 'home.invoice'; 
				break; 
			case "CON_QUOTATION": 
				callState = 'home.quotation'; 
				break; 
			case "CON_TRANSPORTNOTE": //ADM-003
				callState = 'home.transportNote'; 
				break; 
			case "CON_SHOPPINGCART": 
				callState = 'home.shoppingcart'; 
				break;
			case "CON_TRANSACTIONS": 
				callState = 'home.account'; 
				break;
			case "CON_ORDER": 
				callState = 'home.order'; 
				break;
			case "CON_BP_REQUEST": 
				callState = 'home.request'; 
				break;
			case "CON_PRODUCT": 
				callState = 'home.prodSearchResultMenu'; 
				stateParameter = {"FREE_TEXT":""};
				break;
			case "CON_SUBMIT_REQUEST" :
				callState = REQ_SUBMIT_VIEW; 
//				$rootScope.reqTemplate.checked = "order";
				DataSharingService.setObject("fromState",REQ_SUBMIT_VIEW);
				break;
			case "CON_GENERAL_ENQUIRY": 
				callState="GENERAL_ENQUIRY";
				var emailObj = $rootScope.getGeneralEmailObject();
				if(signonRequired && loginStatus == true){
					$rootScope.sendEmail($rootScope.sendEmailEnquiry,emailObj,null,true);
				}else if(signonRequired && loginStatus == false) {
					//callState = CONST_State_Home;
					DataSharingService.setToSessionStorage("FROM_HOME_EMAIL",true);
					$rootScope.sendEmail($rootScope.sendEmailEnquiry,emailObj,null,true);
				}else if(!signonRequired){
					$rootScope.sendEmail($rootScope.sendEmailEnquiry,emailObj,null,true);
				}
				break;
			case "CON_MY_TOP_ITEMS":
				callState = 'home.topitem';			
				break;
			case "CON_EDIT_ACCOUNT":
				callState='home.editaccount' ;
				break;
			case "CON_CHANGE_CUSTOMER":
				callState='home.changeCustomer' ;
				break;	
			case "CON_NEW_ACCOUNT" : 
				callState=$state.current.name;
				if(signonRequired && loginStatus == true){
					$rootScope.showNewAccountPopup();
				}else if(signonRequired && loginStatus == false) {
					callState = CONST_State_Home; 
				}else if(!signonRequired){
					$rootScope.showNewAccountPopup();
				}
				break;
			case "CON_CHANGE_PASSWORD" : 
				// CON_CHANGE_PASSWORD
				$rootScope.changePasswordClick();
				break;	
			case "CON_SIGNOFF" : 
				// CON_CHANGE_PASSWORD
				$rootScope.loginLogout();
				break;
			default: 
				callState = CONST_State_Home; 
			} 
			if(loginStatus == false)
			{	if(signonRequired == true)  
			{
				$rootScope.open(callState,stateParameter);
			}
			else
			{
				$state.go(callState,stateParameter);
			}
			}
			else if(callState=="GENERAL_ENQUIRY")
			{		
				

			}else if (callState == "home.requestsubmit"){
				$scope.goToSubmitReq(callState,stateParameter);
				
			}	else{
				$state.go(callState,stateParameter);
			}
		}
	};

	$scope.changeLanguage = function (lang){
		$rootScope.languageChangeFilter = lang;
		$scope.selectedLanguage = lang;
		var prams = ["LanguageCode",lang.code];
		////console.log("inChangeLanguage,  selectedLanguage: " + $scope.selectedLanguage.description);
		Util.getProductFilterDetails(prams).then(function(data){
			DataSharingService.removeProductFilterDetailsSettings();
			DataSharingService.setProductFilterDetailsSettings(data);	
			$rootScope.productfilterDetailSettings = data;
			if($state.current.name == "home.mainCatalog"){
				$rootScope.mainSearchFilterChange(lang);
			}else if ($state.current.name == "home.prodSearchResult"){
				//$rootScope.prdSearchFilterChange(lang);
			} else if ($state.current.name == "home.bestoffer" ||$state.current.name == "home.popular"){
				$rootScope.bestOfferFilterChange(lang);
			}else if($state.current.name == "home.catalog"){
				$rootScope.bannerSearchFilterChange(lang);
			}
			
		});

		
		$rootScope.menuLanguageChange(lang);
		
		NewConfigService.changeLanguage($scope.selectedLanguage.code);
	};
	
	$scope.goToSubmitReq = function(callState,stateParameter){
		var filterDetails = DataSharingService.getFilterDetailsSettings();
		//currencyList
		if(!isNull(filterDetails) && !isNull(filterDetails['requestTypeList'])){
			$state.go(callState,stateParameter);
		}else{
			Util.getFilterListDetails([]).then(function(data){
				DataSharingService.setFilterDetailsSettings(data);
				$rootScope.filterDetailSettings = data;	
				$state.go(callState,stateParameter);
			});
		}
	};
	
	$rootScope.getSenderEmailAddress = function(){
		var senderEmailId = "";
		if($rootScope.isSignedIn){
			senderEmailId = returnBlankIfNull($rootScope.webSettings.userEmail);
		}
		return senderEmailId ;
	};
	
	$rootScope.getGeneralEmailObject = function(){
		var senderEmail = $rootScope.getSenderEmailAddress();
		var msgBody = "";
		if($rootScope.isSignedIn){
			msgBody = Util.translate('COH_MAIL_GENERATED_FROM') +" : "+senderEmail;
		}
		var emailObj = new Email(senderEmail,"",msgBody);
		return emailObj;
	};
	
	$rootScope.getMenuItemByName = function (menuName){
		var menuList = DataSharingService.getObject("functionMenus");
		var menuItem = null;
		
		for(var i=0;i<menuList.length;i++){
			var menu = menuList[i];
			if(isEqualStrings(menu.name,menuName)){
				menuItem = menu;
				break;
			}
			var subMenuList = menu.subMenu;
			if(!isNull(subMenuList)){
			for(var j=0;j<subMenuList.length;j++){
				var subMenu = subMenuList[j];
				if(isEqualStrings(subMenu.name,menuName)){
					menuItem = subMenu;
					break;
				}
			}}
		}
		return menuItem;
	};
	
	$rootScope.getMenuItemByNameOpenWindow = function (menuName){
		var menuList = DataSharingService.getObject("functionMenus");
		var menuItem = null;
		if(!isNull(menuList)){
		for(var i=0;i<menuList.length;i++){
			var menu = menuList[i];
			if(isEqualStrings(menu.name,menuName)){
				menuItem = menu;
				break;
			}
			var subMenuList = menu.subMenu;
			for(var j=0;j<subMenuList.length;j++){
				var subMenu = subMenuList[j];
				if(isEqualStrings(subMenu.name,menuName)){
					menuItem = subMenu;
					break;
				}
			}
		}
		return menuItem;
		}else{
			var translatedMsgList = [];
			translatedMsgList.push(Util.translate('MSG_ACCESS_DENIED_SIGN_ON_REQUIRED'));
			var msg = '';
			if(isNull($rootScope.isSessionExpireMsgPopUpOpen) || $rootScope.isSessionExpireMsgPopUpOpen){
				$rootScope.isSessionExpireMsgPopUpOpen = true;
				var dlg = $rootScope.openMsgDlg(msg,translatedMsgList);
				dlg.result.then(function () {
					$rootScope.isSessionExpireMsgPopUpOpen=false;
					$rootScope.reloadHomePage();
				}, function () {
					$rootScope.isSessionExpireMsgPopUpOpen=false;
					$rootScope.reloadHomePage();
				});
			}
		}
	};
	
	$rootScope.getSignInRequirement = function(menuName){
		var menuItem = $rootScope.getMenuItemByName(menuName);
		var signOnRequired = false;
		if(!isNull(menuItem)){
			signOnRequired = menuItem.signonRequired;
		}
		return signOnRequired;
	};
	$rootScope.getSignInRequirementOpenWindow = function(menuName){
		var menuItem = $rootScope.getMenuItemByNameOpenWindow(menuName);
		var signOnRequired = false;
		if(!isNull(menuItem)){
			signOnRequired = menuItem.signonRequired;
		}
		return signOnRequired;
	};
}]);