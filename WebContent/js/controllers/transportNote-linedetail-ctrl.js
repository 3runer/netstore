
catalougeControllers.controller('transportNoteLineDetailCtrl', ['$rootScope','$scope', '$http','$log', 'DataSharingService','TransportNoteService', 'Util','$state',
                                                            function ($rootScope,$scope, $http,$log,DataSharingService,TransportNoteService,Util,$state) {

	var transportNoteLineDetailCtrlInit = function(){
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	});
	
	var initController = function(){
		var requestParams = [];
		$scope.dataFound = false;
		$scope.resultMsg ="";
		$scope.errorMessage = "";
		$scope.callState = $state.current.name;
		var transportNoteLine = DataSharingService.getFromSessionStorage("transportNoteLine_clickedDetail");
		var transportNote = DataSharingService.getFromSessionStorage("transportNote_clickedDetail");
		if( transportNoteLine !=null){
			$scope.transportNoteLine = transportNoteLine;
			$scope.transportNoteData = transportNote;
			$rootScope.headingPrice = "Price";	
			$rootScope.Price = transportNoteLine.unitPrice;
			//console.log("transportNote line detail "+ JSON.stringify(transportNoteLine));
			//console.log("transportNote line detail requestParams"+ JSON.stringify(requestParams));
		};
	};
	transportNoteLineDetailCtrlInit();
	
}]);