catalougeControllers.controller('editAccountCtrl', ['$rootScope','$scope','$stateParams', '$http','$log', '$q','DataSharingService','AccountService','authentication','Util','$state','$window',
                                                function ($rootScope,$scope,$stateParams, $http,$log, $q,DataSharingService,AccountService,authentication,Util,$state,$window) {
	
	var requestParam = [];
	
	var initController = function()
	{
		$scope.defaultAddressType = false;
		$scope.dataLoaded = false;
		$scope.userAccountData=new UserAccount();	
		var currentLocaleList = DataSharingService.getFromSessionStorage("currentLocaleList");
		if(isNull(currentLocaleList)){
			$rootScope.currentLocaleList = [];
			var newvarToCopy =$rootScope.currentLocaleList;
				AccountService.getLocaleList().then(function(data){
					if(!isNull(data)){
						angular.copy(data,newvarToCopy);
						$rootScope.currentLocaleList = newvarToCopy;
						DataSharingService.setToSessionStorage("currentLocaleList",$rootScope.currentLocaleList);
						var currentLocaleListNewWindow = DataSharingService.getFromSessionStorage("currentLocaleList");
						$scope.userAccountData.setLocaleList(currentLocaleListNewWindow);
						getUserAccountData(requestParam);
						$scope.userPasswordInfo = new CredentialInfo(Util.translate);
					}
				});
		}else{
		$scope.userAccountData.setLocaleList(currentLocaleList);
		getUserAccountData(requestParam);
		$scope.userPasswordInfo = new CredentialInfo(Util.translate);
		}
	};
	
	var getUserAccountData = function(requestParam){
		var defaultLocaleFromSession = DataSharingService.getFromSessionStorage("changed_locale_code");
		AccountService.getUserDetails(requestParam).then(function(data){
			//$http.get('./json/catalog/user_account.json').success(function(data) {	
			if(!isNull(data)){			
				$scope.userAccountData.setAccountInfo(data);
				if(!isNull(defaultLocaleFromSession)){
					$scope.userAccountData.setDefaultLocale(defaultLocaleFromSession);
				}else{
					$scope.userAccountData.setDefaultLocale();
				}
				$scope.defaultAddressType = hasDefaultAddressType(data);
			}else{
				
			}
			$scope.dataLoaded = true;
		});
	};
	
	function hasDefaultAddressType(userAccData) {
		for(var i = 0; i < userAccData.BusinessPartnerAddresses.length; i++) {
			if(($scope.userAccountData.isdefaultAddress(userAccData.BusinessPartnerAddresses[i].addressTypeList, 
					'CON_CONFIRMATION_ADDRESS') == true) || 
					($scope.userAccountData.isdefaultAddress(userAccData.BusinessPartnerAddresses[i].addressTypeList, 
							'CON_DELIVERY_ADDRESS') == true) || 
							($scope.userAccountData.isdefaultAddress(userAccData.BusinessPartnerAddresses[i].addressTypeList, 
									'CON_INVOICE_ADDRESS') == true)) {
				return true;
			}
		}
		return false;
	}
	
	var modifyUserData = function(requestParam){
		AccountService.modifyUserInfo(requestParam).then(function(data){
			$scope.userAccountData.handleMessageCode(data);
			$rootScope.getMenuList();
			
		});
		
	};
	
	
	$scope.saveUserData = function(){
	//	if($scope.userAccountData.allowToModify){
			var requestParam = ["UserName",$scope.userAccountData.userName,"Emailid",$scope.userAccountData.userEmail,"Locale",$scope.userAccountData.selectedLocale.code];
			if(webSettings.localeCode != $scope.userAccountData.selectedLocale.code) {
				DataSharingService.setToSessionStorage("changed_locale_code",$scope.userAccountData.selectedLocale);
				$window.alert(Util.translate('MSG_CHANGES_IN_EFFECT_AFTER_SIGNON'));
			}
			modifyUserData(requestParam);
	//	}
	};
	$scope.changeUserPassword= function(credentialInfo){
//		var requestParams = ["oldPW",credentialInfo.oldPassword,"newPW",credentialInfo.newPassword,"confPW",credentialInfo.confirmPassword];		
//		AccountService.changePassword(requestParams).then(function(data){
//			if(!isNull(data)){	
//				$scope.userPasswordInfo.updateInfo(data);
//			}else{
//
//			}
//		});	
		var changePwdObj = new ChangePassword(null, false,null);
		changePwdObj.oldPwd = credentialInfo.oldPassword;
		changePwdObj.newPwd = credentialInfo.newPassword;
		changePwdObj.confNewPwd = credentialInfo.confirmPassword;
		var reqParam = changePwdObj.getRequestParamForPost();
		if(!isNull(reqParam)){
			authentication.changePassword(reqParam).then(function(data){
				if(!isNull(data)){	
					$scope.userPasswordInfo.updateInfo(data);
				}else{

				}
			});
		}
	};
	
	initController();
}]);