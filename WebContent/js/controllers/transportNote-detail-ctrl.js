
//TransportNote detail ctrl
catalougeControllers.controller('transportNoteDetailCtrl', ['$rootScope','$scope', '$http', '$stateParams', '$log', 'DataSharingService','TransportNoteService', 'Util','$state',
                                                        function ($rootScope,$scope, $http, $stateParams, $log,DataSharingService,TransportNoteService,Util,$state) {
	// new code
	var showLoading = true;
	var transportNoteDetailCtrlInit = function(){
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
		$rootScope.requestLineSelectionMsg = null;
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	});
	
	var initController = function()
	{ 
		$scope.isUserSessionActive = true;
		$scope.cachedPaginationObj = null;
		$scope.paginationObj = null;
		$scope.isCollapsed = false;
		$scope.propertyList = [];	
		$scope.sort = new Sort();
		$scope.sort.ASC('lineNumber');
		$scope.sortType = 'lineNumber';
		$scope.transportNoteDetailProperty = [];
		$scope.callState = $state.current.name;
		$scope.itemSelected=false;
		if($scope.callState == REQ_SUBMIT_TRANSPORTNOTE_DETAIL_VIEW){
			showLoading = false;
		}
		order = DataSharingService.getFromSessionStorage("transportNote_clickedDetail");
		//var order = DataSharingService.getObject("order_clickedDetail");
		if(!isNull(order)){
			$scope.order = order;
		}
		var params = getRequestParams();
		if(webSettings == undefined){
			setWebSettingObject(DataSharingService.getWebSettings());
		}
		getTransportNoteDetailPageData(params,FirstPage,showLoading);
		
	};
	var getRequestParams = function(){
		var param = [];
		var transportNote = DataSharingService.getFromSessionStorage("transportNote_clickedDetail");
	
		if( !isNull(transportNote)){
			$scope.transportNote = transportNote;
			param = ["TransportNoteNumber",transportNote.TransportNoteNumber];
		};
		return param;
	};
	var getTransportNoteDetailPageData =  function(requestParams,page,showLoading){
		$scope.paginationObj = $rootScope.getPaginationObject('transportNoteDetailsUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		var pageNo = "";
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('transportNoteDetailsUrl',requestParams,pageNo,showLoading ).then(function(data){
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true);
				}else{
					setUIPageData(data,false);
				}
			});
		}
	};
	var setUIPageData = function(data,isFirstPageData){
		var ItemList = data.transportNoteLineBean;
		if(isNull(data)){
			setTransportNoteLineList(null,isFirstPageData); 
			if(isFirstPageData){
				$rootScope.applyPreviousPaginationObject('transportNoteDetailsUrl' , $scope);
			}

		}else{
			$scope.transportNoteLineData = [];
			if(angular.isDefined(data.transportNoteLineBean) && !isNull(data.transportNoteLineBean)){
				ItemList =  data.transportNoteLineBean;
				$scope.transportNoteDetailProperty = data.transportNoteLineBean[0];
				$scope.propertyList = Util.getObjPropList(data.transportNoteLineBean[0]);	
				addpropertyToList(ItemList,'validQuantity',true);
				$scope.initBuyObject(ItemList);
			}
			$scope.transportNoteDetailsPropList = Util.getObjPropList(data);	
			$scope.transportNoteDetails = data;
			$rootScope.changeHomePageTitle(Util.translate('TXT_PAGE_TITLE_TRANSPORTNOTE_WITH_NUMBER') + $scope.transportNoteDetails.transportNoteNumber, true);
			$scope.currency = data.currency;
			setTransportNoteLineList(ItemList,isFirstPageData); 
			$rootScope.cachePaginationObject('transportNoteDetailsUrl' , ItemList, $scope);
			if(isFirstPageData && (! Array.isArray(ItemList) ||  !ItemList.length > 0 )){
				$rootScope.applyPreviousPaginationObject('transportNoteDetailsUrl' , $scope);
			}
		}
		$rootScope.setPageLoadMsg(ItemList,isFirstPageData,$scope);
		$scope.$root.$eval();
	};

	var setTransportNoteLineList = function(itemList,isFirstPageData){
		if(Array.isArray(itemList) && itemList.length > 0 ){
			setQuotationExtendedData(itemList);
			$scope.transportNoteLineData = itemList;				

		}else{
			if(!isFirstPageData){
				$scope.transportNoteLineData = [];
			}
		}
	};
	$scope.previousPage = function(){		
		 var params = getRequestParams();		
		 getTransportNoteDetailPageData(params,PrevPage,true);
	};
	$scope.nextPage = function(){	
		 var params = getRequestParams();		
		 getTransportNoteDetailPageData(params,NextPage,true);
	};
			
	$scope.getFirstPage = function(){			
		 var params = getRequestParams();		
		 getTransportNoteDetailPageData(params,FirstPage,true);
	};
	
	$scope.sortASC = function(column){
		$scope.sort.ASC(column);
	};
	
	$scope.sortDESC = function(column){
		$scope.sort.DESC(column);
	};

	$scope.getProperty = function(property) {  
		var data = $scope.transportNoteDetailProperty;
		return data[property];
	};

	$scope.isListEmpty = function(list) {
		var isEmpty = false;	
		if( list == null)
		{ 	isEmpty = true;
			$scope.resultMsg = "No information found...";
		}
		return isEmpty;
	};

	$scope.isCollapsed = false;
	$scope.toggleCollapse = function()
	{
		var isCollapsed = !$scope.isCollapsed;
		return isCollapsed;
	};	

	$scope.showTransportNoteLineDetail = function(transportNoteLine){
		if($rootScope.isShowDisableMenuItem('CON_TRANSPORTNOTE_LINE_INFORMATION')){
			DataSharingService.setToSessionStorage("transportNoteLine_clickedDetail",transportNoteLine);
			var needToLogin = $rootScope.isSignOnRequired('CON_TRANSPORTNOTE_LINE_INFORMATION','home.transportNoteline',true);
			if(!needToLogin){
				$state.go('home.transportNoteline');
			}
		}
	};

	$scope.isColExist = function(colPropName,propertyList)
	{
		var isCol = false;
		var val = Util.isPropertyExist(colPropName,propertyList);
		if(val){
			isCol = true;
		}
		return isCol;
	};
	
	/* Buy From TransportNote */
	$scope.initBuyObject = function(list){
		$scope.buyObj = new Buy(list);
		$scope.buyObj.setPropNameItemCode('itemCode');
		$scope.buyObj.setPropNameOrdered('quantity1');
		$scope.buyObj.setPropNameUnit('unit');
		$scope.buyObj.setPropNameisBuyAllowed('buyAllowed');
		$scope.buyObj.setpropNameQuotationPrice('price');
	};
	
	$scope.buyItems = function(item,buyObj){
		if($rootScope.webSettings.isUseTransportNotePrice){
			$rootScope.buyMultipleItemsQuo(item,$scope.buyObj);
		}else{
			$rootScope.buyMultipleItems(item,$scope.buyObj);
		}
		
	};
	
	$scope.addItem = function(item)
    {
    	$rootScope.addItemToCurrentList(item);    	
    };
	
	$scope.addMultipleItemsToCurrentList = function(item){
		$rootScope.addMultipleItemsToList(item,$scope.buyObj);
	};
	
	$scope.exportTransportNoteLineTableAsPDF = function(){
		
		var transportNoteLines = getTransportNoteLinesForExport($scope.transportNoteDetails);
		var basicInfo = getBasicInfoForExport($scope.transportNoteDetails);
		var addresses = getAddressesForExport($scope.transportNoteDetails);
		var text,references,fees;
		
		var fileName = "TransportNoteDetail_"+$scope.transportNoteDetails.transportNoteNumber+"_"+getCurrentDateTimeStr()+".pdf";
		var docHeader = Util.translate('CON_TRANSPORTNOTE_NUMBER') + " " + $scope.transportNoteDetails.transportNoteNumber;
		//saveDetailsPDF(transportNoteLines,basicInfo,text,references,fees,addresses,docHeader,fileName);
		var base64 = $scope.transportNoteDetails.pdfFile;
		var data = base64;
		
		if (window.navigator && window.navigator.msSaveOrOpenBlob) { // IE workaround
		    var byteCharacters = atob(data);
		    var byteNumbers = new Array(byteCharacters.length);
		    for (var i = 0; i < byteCharacters.length; i++) {
		        byteNumbers[i] = byteCharacters.charCodeAt(i);
		    }
		    var byteArray = new Uint8Array(byteNumbers);
		    var blob = new Blob([byteArray], {type: 'application/pdf'});
		    window.navigator.msSaveOrOpenBlob(blob, fileName);
		}
		else { // much easier if not IE
			//download(base64, fileName, "application/pdf");
			download(base64, docHeader + "pdf", "application/pdf");
		}
		
	};
	
	var getBasicInfoForExport = function(transportNoteDetails){
		var basicInfo = new Table();
		basicInfo.heading = Util.translate('CON_TRANSPORTNOTE_HEADER_INFORMATION') ; 
		var row = [];
		row.push(Util.translate('COH_CUSTOMER'));
		row.push(transportNoteDetails.customerCode);
		row.push(transportNoteDetails.customerDesc);
		row.push("");
		row.push("");
		basicInfo.addRow(row);
		
		row = [];
		row.push(Util.translate('COH_DEBTOR'));
		row.push(transportNoteDetails.debtorCode);
		row.push(transportNoteDetails.debtorDesc);
		row.push("");
		row.push("");
		basicInfo.addRow(row);
		
		row = [];
		row.push(Util.translate('CON_TRANSPORTNOTE_DATE'));
		row.push(transportNoteDetails.transportNoteDate);
		basicInfo.addRow(row);
		
		if(!isNull(transportNoteDetails.motCode)){
			row = [];
			row.push(Util.translate('CON_MANNER_OF_TRANSPORT'));
			row.push(transportNoteDetails.motCode);
			row.push(transportNoteDetails.motDesc);
			row.push("");
			row.push("");
			basicInfo.addRow(row);
		}
		if(!isNull(transportNoteDetails.todCode)){
			row = [];
			row.push(Util.translate('CON_TERMS_OF_DELIVERY'));
			row.push(transportNoteDetails.todCode);
			row.push(transportNoteDetails.todDesc);
			row.push("");
			row.push("");
			basicInfo.addRow(row);
		}
		if(!isNull(transportNoteDetails.shippingAgentCode)){
			row = [];
			row.push(Util.translate('CON_SHIPPING_AGENT'));
			row.push(transportNoteDetails.shippingAgentCode);
			row.push(transportNoteDetails.shippingAgentDesc);
			row.push("");
			row.push("");
			basicInfo.addRow(row);
		}
		return basicInfo;
	};

	var getAddressesForExport = function(transportNoteDetails){
		var addresses = new Table(); 
		addresses.heading = Util.translate('CON_ADDRESSES') ; 
		addresses.addHeader(Util.translate('CON_DELIVERY_ADDRESS'));

		var row = [];
		row.push(transportNoteDetails.delAddress.split('</br>'));
		addresses.addRow(row);
		return addresses;
	};

	var getTransportNoteLinesForExport = function(transportNoteDetails){
		
		var transportNoteLines = new Table();
		transportNoteLines.heading = Util.translate('CON_TRANSPORTNOTE');
		transportNoteLines.addHeader(Util.translate('COH_LINE'));
		transportNoteLines.addHeader(Util.translate('COH_PRODUCT'));
		transportNoteLines.addHeader(Util.translate('COH_DESCRIPTION'));
		transportNoteLines.addHeader(Util.translate('COH_DISPATCH_DATE'));
		transportNoteLines.addHeader(Util.translate('COH_ORDER_NUMBER'));
		transportNoteLines.addHeader(Util.translate('COH_ORDER_LINE_NUMBER'));
		transportNoteLines.addHeader(Util.translate('CON_WAREHOUSE'));
		transportNoteLines.addHeader(Util.translate('CON_PHYSICAL_CONTAINER'));
		transportNoteLines.addHeader(Util.translate('COH_QUANTITY'));
		transportNoteLines.addHeader(Util.translate('CON_ORDER_QUANTITY'));
		transportNoteLines.addHeader(Util.translate('COH_UNIT'));
		transportNoteLines.addHeader(Util.translate('CON_PRICE'));
		transportNoteLines.addHeader(Util.translate('COH_AMOUNT'));

		if(!isNull($scope.transportNoteLineData)){
			for(var i=0;i< $scope.transportNoteLineData.length;i++){
				var row = $scope.transportNoteLineData[i];
				var colData = [];
				colData.push(row.lineNumber);
				colData.push(row.itemCode);
				colData.push(row.itemDescription);
				colData.push(row.dispatchDate);
				colData.push(row.orderNumber);
				colData.push(row.orderLineNumber);
				colData.push(row.warehouse);
				colData.push(row.physicalContainer);
				colData.push(row.quantity);
				colData.push(row.orderedQuantity);
				colData.push(row.unit);
				colData.push(row.unitPrice);
				colData.push(row.lineAmount);
			
				transportNoteLines.addRow(colData);
			}
		}
		else{
			var colData = [];
			
				for(var i=0;i<transportNoteLines.headers.length;i++){
					colData.push("");
				}
				transportNoteLines.addRow(colData);
		}
		
		return transportNoteLines;
	};	

	// ADM-012
	function download(strData, strFileName, strMimeType) {
		var D = document, A = arguments, a = D
				.createElement("a"), d = A[0], n = A[1], t = A[2]
				|| "application/pdf";

		var newdata = "data:" + strMimeType
				+ ";base64," + escape(strData);

		// build download link:
		a.href = newdata;
		if ('download' in a) {
			a.setAttribute("download", n);
			a.innerHTML = "downloading...";
			D.body.appendChild(a);
			setTimeout(function() {
				var e = D.createEvent("MouseEvents");
				e.initMouseEvent("click", true, false,
						window, 0, 0, 0, 0, 0, false,
						false, false, false, 0, null);
				a.dispatchEvent(e);
				D.body.removeChild(a);
			}, 66);
			return true;
		};
	};
	
	transportNoteDetailCtrlInit();
}]);