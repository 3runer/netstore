//Account Transactions controller

catalougeControllers.controller('accountTransactionCtrl', ['$rootScope','$scope', '$http','$log', 'DataSharingService','TransactionService', 'Util','$state',
                                                           function ($rootScope,$scope, $http,$log,DataSharingService,TransactionService,Util,$state) {
	//	new code starts
	
	var transactionHistoryCtrlInit = function(){
		if(historyManager.isBackAction){
		historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	//	$log.log(" topItemCtrl history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	
	var initController = function()
	{ 
		$scope.sortColumn = "DocumentDate";
		$scope.orderType = "DESC";	
		$scope.isUserSessionActive = true;
		$scope.cachedPaginationObj = null;
		$scope.sort = new Sort();
		$scope.sort.DESC('DocumentDate');
		$scope.filterOn = false;
		$scope.filterList =  null;
		$scope.paginationObj = null;
		$scope.isCollapsed = false;
		$scope.filterFormInfo = {};
		makeTransactionFilters();
		var params = getSearchParams(); 		
		getTransactionPageData(params,FirstPage);
		// to be code
	};
	
	var getSearchParams = function(){
		var params  = [];
		if($scope.filterOn){
			params = $scope.sort.addSortParams($scope.filterList.getParamsList());
		}else{
			params = $scope.sort.getParam();
		}
		return params;
	};	
	
	var makeTransactionFilters = function(){
		var finalFilterList = new FilterList();
		var staticFilterConfig = TransactionService.getFilterConfiguration();
		var filterDataList = DataSharingService.getFilterDetailsSettings();
		
		var filterExist = false;
		if(isNull(filterDataList.customerList)){
			filterExist = false;
		}else{
			filterExist = true;
		}
		
		
		if(filterExist ==  true){
		var dynamicFilterConfig = null;
		TransactionService.getTransactionFilterList([]).then(function(data){
				dynamicFilterConfig = data.filterBeans;
				finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
				$scope.filterList = finalFilterList;
				//$scope.ShoppingList.setFilterList(finalFilterList);
				////console.log("Transaction Filters : " + JSON.stringify(finalFilterList));
		});
		}else{
			Util.getFilterListDetails([]).then(function(data){
				DataSharingService.setFilterDetailsSettings(data);
				filterDataList = DataSharingService.getFilterDetailsSettings();
				var dynamicFilterConfig = null;
				TransactionService.getTransactionFilterList([]).then(function(data){
					dynamicFilterConfig = data.filterBeans;
					finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
					$scope.filterList = finalFilterList;
					//$scope.ShoppingList.setFilterList(finalFilterList);
					////console.log("Transaction Filters : " + JSON.stringify(finalFilterList));
			});
			});
		}
	};
	
	$scope.submitFilter = function(){
		$scope.sort.DESC('DocumentDate');
		$scope.filterOn = true;
		var params = getSearchParams();		
		getTransactionPageData(params,FirstPage);
	};
	var getTransactionPageData =  function(requestParams,page){
		$scope.paginationObj = $rootScope.getPaginationObject('transactionDetailsUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('transactionDetailsUrl',requestParams,page ).then(function(data){
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true);
				}else{
					setUIPageData(data,false);
				}
			});
		}
	};
	
	
	var setUIPageData = function(data,isFirstPageData){
		var ItemList = [];
		
			if(!isNull(data) && (data.messageCode == 2100 || data.messageCode == 2110 || data.messageCode==111)){
				setItemList(null,isFirstPageData); 
				if(isFirstPageData){
					$rootScope.applyPreviousPaginationObject('transactionDetailsUrl' , $scope);
				}
				
				
			}else{
				ItemList =	data.transactionBean;
				if(isFirstPageData){
					$scope.accountData = data;
				}
				setItemList(ItemList,isFirstPageData); 
				$scope.sortColumn = data.sortColumn;
				$scope.orderType = data.orderBy;
				$scope.moreDataArrow = data.moreRecords;
				$rootScope.cachePaginationObject('transactionDetailsUrl' , ItemList, $scope);
				if(isFirstPageData && (! Array.isArray(ItemList) ||  !ItemList.length > 0 ) ){
					$rootScope.applyPreviousPaginationObject('transactionDetailsUrl' , $scope);
				}
				
				
			}
			$rootScope.setPageLoadMsg(ItemList,isFirstPageData,$scope);
			$rootScope.scrollToId("result_set");
			$scope.$root.$eval();
		};
		
	var setItemList = function(itemList,isFirstPageData){
		if(Array.isArray(itemList) && itemList.length > 0 ){
			$scope.transactionData = itemList;				
			
		}else{
			if(!isFirstPageData){
				$scope.transactionData = [];
			}
		}
		updateFilterStatus(itemList,isFirstPageData);
	};
	
	var updateFilterStatus = function(list,isFirstPageData){
		if(Array.isArray(list) && list.length > 0 && isFirstPageData){
			$scope.filterList.cacheFilters();			
		}else if($scope.filterOn && isFirstPageData){			
			$scope.filterList.applyPreviousFilters();			
			showMsgNoObjFound();
		}
	};
	
	// sorting 
    $scope.sortASC = function(column){
		$scope.sort.ASC(column);
		sorting();
	};
	
	$scope.sortDESC = function(column){
		$scope.sort.DESC(column);
		sorting();
	};
	
	var sorting = function(){
		var params = getSearchParams();		
		getTransactionPageData(params,FirstPage);
	};
	
	$scope.previousPage = function(){		
		 var params = getSearchParams();		
		 getTransactionPageData(params,PrevPage);
	};
	$scope.nextPage = function(){	
		 var params = getSearchParams();		
		 getTransactionPageData(params,NextPage);
	};
			
	$scope.getFirstPage = function()
	{			
		 var params = getSearchParams();		
		 getTransactionPageData(params,FirstPage);
				
	};
	//  Pagination code ends
	$scope.showTransactionInvoiceDetail = function(transaction){
	//	DataSharingService.setObject("transaction_clickedDetail",transaction);
		DataSharingService.setToSessionStorage("transaction_clickedDetail",transaction);
		$rootScope.fromInvoice = false;
		$rootScope.fromRequestPage = false;
		DataSharingService.setToSessionStorage("fromInvoice",$rootScope.fromInvoice);
		DataSharingService.setToSessionStorage("fromRequestPage",$rootScope.fromRequestPage);
		var needToLogin = $rootScope.isSignOnRequired('CON_INVOICE_DETAIL','home.invoice.detail',true);
		if(!needToLogin){
			$state.go('home.invoice.detail');
		}
	};

	$scope.showAgreementNotes = function(){ 
		var needToLogin = $rootScope.isSignOnRequired('CON_AGREEMENT_NOTES',null,false);	
		if(!needToLogin){
			$scope.isCollapsed = !$scope.isCollapsed;
		}
	};
	
	$scope.exportTransactionsTableAsPDF=function(){
		var accountInformationTable = setTransactionsDataforExport();
		var data = accountInformationTable.getHTML();
//		var customerInfo=getCustomerInfoForExport($scope.accountData);
		var title = Util.translate('CON_ACCOUNT_INFORMATION');
		var fileName = "AccountInformation_"+getCurrentDateTimeStr()+".pdf";
		savePdfFromHtml(data,title,fileName);
	};
	
	$scope.exportTransactionsTableAsExcel=function(){
		var accountInformationTable = setTransactionsDataforExport();
		var data = accountInformationTable.getDataWithHeaders();
		var sheetName = "AccountInformation";
		var fileName = "AccountInformation_"+getCurrentDateTimeStr()+".xlsx";
		saveExcel(data,sheetName,fileName);
	};
	
	$scope.exportTransactionsTableAsXML=function(){
		var accountInformationTable = setTransactionsDataforExport('XML');
		accountInformationTable.setRootElementName("ARLEDGER");
		accountInformationTable.setEntityName("CUSTOMER");
		var data = accountInformationTable.getXML();
		var fileName = "AccountInformation_"+getCurrentDateTimeStr()+".xml";
		saveXML(data,fileName);
	};
	
	var setTransactionsDataforExport = function(target){
		var accountInformationTable = new Table();
		if(isEqualStrings(target,"XML")){
			accountInformationTable.addHeader('CUSTOMER');
			accountInformationTable.addHeader('DOCUMENT');
			accountInformationTable.addHeader('DOCUMENT_DATE');
			accountInformationTable.addHeader('DUE_DATE');
			accountInformationTable.addHeader('CURRENCY');
			accountInformationTable.addHeader('ORIGINAL_AMOUNT');
			accountInformationTable.addHeader('REMAINING_AMOUNT');		
			accountInformationTable.addHeader('DESCRIPTION');	
			if(!isNull($scope.accountData) && ($scope.accountData.debtorFlag)){
				accountInformationTable.addHeader('DEBTOR_CODE');
			}
		}else{
			accountInformationTable.addHeader(Util.translate('CON_CUSTOMER'));
			accountInformationTable.addHeader(Util.translate('COH_DOCUMENT'));
			accountInformationTable.addHeader(Util.translate('CON_DOCUMENT_DATE'));
			accountInformationTable.addHeader(Util.translate('CON_DUE_DATE'));
			accountInformationTable.addHeader(Util.translate('COH_CURRENCY'));
			accountInformationTable.addHeader(Util.translate('CON_ORIGINAL_AMOUNT'));
			accountInformationTable.addHeader(Util.translate('CON_REMAINING_AMOUNT'));
			accountInformationTable.addHeader(Util.translate('CON_DESCRIPTION'));
			if(!isNull($scope.accountData) && ($scope.accountData.debtorFlag)){
				accountInformationTable.addHeader(Util.translate('CON_DEBTOR'));
			}
		}
		//set row data
		data = $scope.transactionData;
		

		if(!isNull(data)){
			for(var i=0;i<data.length;i++){
				var row=[];
				var document=data[i].documentTypeCode+" "+data[i].documentNumber;
				row.push(data[i].customerCode);
				row.push(document);
				row.push(data[i].documentDate);
				row.push(data[i].dueDate);
				row.push(data[i].transactionCurrencyCode);
				row.push(data[i].originalAmount);
				row.push(data[i].remainingAmount);
				row.push(data[i].description);
				if($scope.accountData.debtorFlag){
					row.push(data[i].debtorCode);
				}
				accountInformationTable.addRow(row);
			}
		}else{
			var row = [];			
			for(var i=0;i<accountInformationTable.headers.length;i++){
				row.push("");
			}
			accountInformationTable.addRow(row);
		}
		
		return accountInformationTable;
	};
	
	var getCustomerInfoForExport = function(accountData){
		var customerInfo = new Table();

//		orderReferences.heading = Util.translate('CON_DEBTOR') ; 

		var row = [];
		row.push(Util.translate('CON_DEBTOR')+":");
		row.push(accountData.debtorCode+", "+accountData.debtorDesc);
		customerInfo.addRow(row);

		row = [];
		row.push(Util.translate('CON_BALANCE')+":");
		row.push(accountData.balance);
		row.push(Util.translate('CON_CREDIT_LIMIT')+":");
		row.push(accountData.creditLimit);
		customerInfo.addRow(row);

		return customerInfo;
	};
	
	transactionHistoryCtrlInit();
	/// new code ends
	/// old code
//	$scope.sortColumn = "documentDate";
//	$scope.orderType = "DESC";	
//	$scope.param = [];
//	$scope.filterParam = [];
//	$scope.filterOn = false;	
//	$scope.dataFound = false;
//	$scope.recordFound = true;
//	$scope.propertyList = [];
//	$scope.isUserSessionActive = true;
//	$scope.responseMessageCode = "";
//
//	var requestParams = [];	
//	$scope.callState = $state.current.name;
//	TransactionService.getTransactionDetails(requestParams).then(function(data){				
//		if(data==null)
//		{
//			$scope.dataFound = false;
//			$scope.transactionDataLoadMsg = " ";
//			$log.log("accountTransactionCtrl --  no data fetched");		
//
//		}
//		else
//		{
//			$log.log("accountTransactionCtrl messageCode -- "+data.messageCode);
//			//$rootScope.reLogin(data.messageCode);
//			$scope.transactionData = data.transactionBean;	
//			$scope.accountData = data;
//			$scope.sortColumn = data.sortColumn;
//			$scope.orderType = data.orderBy;	
//			$scope.dataFound = true;
//			$scope.transactionDataLoadMsg = "";	
//			$log.log("accountTransactionCtrl --  " +data.transactionBean.length);
//			
//		}
//
//	});
//	$scope.isCollapsed = false;
//	$scope.toggleCollapse = function()
//	{
//		var isCollapsed = !$scope.isCollapsed;
//		return isCollapsed;
//	};
//
//	// sorting
//	 $rootScope.sortSelection = '';
//	 $rootScope.sortSelection =  $scope.sortColumn+$scope.orderType;
//	 //$log.log("Request Search sorting-- Current Sorting Selection : " + $scope.Selection);
//
//	$scope.Sorting = function(column,orderBy) {
//			
//		var	requestParams = $rootScope.getSortingParam(column,orderBy);		
//		pageKey = Util.initPagination('transactionDetailsUrl',requestParams);
//		TransactionService.getTransactionDetails(requestParams).then(function(data){
//			$scope.transactionData = data.transactionBean;				
//			$log.log("accountTransactionCtrl --  " +data.transactionBean.length);
//
//		});
//	};
//	//
//	requestParams = ["OrderBy",$scope.orderType,"SortBy",$scope.sortColumn];
//	var pageKey = Util.initPagination('transactionDetailsUrl',requestParams);
//	// Pagination code
//	paginationObj = DataSharingService.getObject(pageKey);	
//	$rootScope.getTransactionPageData = function(page)
//	{
//		$log.log("getTransactionPageData() - called, isPaginationBlocked = "+ Util.isPaginationBlocked(pageKey).toString());		
//		if(!Util.isPaginationBlocked(pageKey))
//		{	
//			$log.log("getTransactionPageData() - called" );
//			requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn); 
//			Util.getPageinationData('transactionDetailsUrl',requestParams,page ).then(function(data)
//			{
//				if(data.transactionBean==null || data.transactionBean.length == 0)
//				{	
//					paginationObj.setLastPageNo();
//					$scope.dataFound = false;
//					$scope.transactionDataLoadMsg =Util.translate('CON_NO_MORE_PAGE'); 
//					$log.log("accountTransactionCtrl --  "+ $scope.accountTransactionCtrl);
//					
//					
//				}
//				else
//				{
//					$log.log("accountTransactionCtrl messageCode -- "+data.messageCode);
//					//$rootScope.reLogin(data.messageCode);
//					$scope.dataFound = true;
//					$scope.transactionDataLoadMsg = "";						
//					$scope.transactionData = data.transactionBean;
//					requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn); 
//					$scope.$root.$eval();					
//				}			
//				$log.log("paginationObj.isLastPageNo(): " +paginationObj.isLastPageNo());
//			});
//		}
//	};
//	
//	


//
//	// Search Filters 
//	$scope.filterFormInfo = {};
//	$scope.filterMsg = "";
//	$scope.submitFilter = function()
//	{
//		$scope.filterOn = true;
//		var keyList = $scope.filterFormInfo;
//		$scope.filterParam =  $rootScope.setParamKeyList(keyList); 
//		var filterParam =  $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn); 
//		$log.log("accountTransactionCtrl filterParam--  " + filterParam);
//		pageKey = Util.initPagination('transactionDetailsUrl',filterParam);
//		TransactionService.getTransactionDetails(filterParam).then(function(data){
//
//			if(data.transactionBean==null || data.transactionBean.length == 0){			
//				$scope.dataFound = false;				
//				$scope.filterMsg =  Util.translate('MSG_NO_OBJECTS_FOR_SELECTION',null);
//				showMsgNoObjFound();
//				$scope.recordFound = false;
//				
//			}
//			else{
//				$scope.transactionData = data.transactionBean;
//				$scope.dataFound = true;
//				$scope.recordFound = true;
//				$scope.transactionDataLoadMsg = "";	
//				$scope.filterMsg = "";
//				$scope.propertyList = Util.getObjPropList(data.transactionBean[0]);		
//				$log.log("accountTransactionCtrl --  " +data.transactionBean.length);				
//				$scope.$root.$eval();
//				$rootScope.scrollToId("result_set");	
//			}
//		});
//	};
//	
//	var requestFilterParams = [];	
//	// to get transaction search filter key list 
//	TransactionService.getTransactionFilterList(requestFilterParams).then(function(data){	
//	//	$http.get('./json/catalog/transaction_search_filter.json').success(function(data){
//		$scope.searchFilterList = data.filterBeans;		
//		$scope.getFilterSettings(data.filterBeans);		
//		$log.log("accountTransactionCtrl: searchFilterList --  " +data.filterBeans.length);
//
//	});
//	
//	$rootScope.filterDetailSettings = DataSharingService.getFilterDetailsSettings();
//
//	// to get filter configuration setting from "filterConfig.json"  
//	$scope.getFilterSettings = function(filterList)
//	{
//		 var filterConfigData = TransactionService.getFilterConfiguration();
//		 $log.log("accountTransactionCtrl getFilterSettings--  " +JSON.stringify(filterConfigData));			 
//		 $scope.searchFilterList =  $rootScope.getSearchFilterSettings($scope.searchFilterList,filterConfigData);
//	};

	// Search Filters
	
	
	
	
}]);