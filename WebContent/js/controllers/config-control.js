catalougeControllers.controller('configCtrl', ['NewConfigService','DataSharingService','ShoppingListService','Util','ShoppingCartService','authentication','AccountService',
                                               '$rootScope','$window','$location','$filter','$state','$scope', '$log', '$q','$sce','$interval','$location', '$anchorScroll','$modal','$timeout','ProductDetailService',
                                               function (NewConfigService, DataSharingService,ShoppingListService,Util,ShoppingCartService,authentication,AccountService,
                                            		   $rootScope,$window,$location,$filter,$state,$scope,$log,$q,$sce,$interval,$location, $anchorScroll, $modal, $timeout,ProductDetailService) {
	
	$scope.freeSearchObj = {};
	$scope.freeSearchObj.text="";
	$scope.freeSearchObj.andOr = "OR";
	$scope.freeSearchObj.enableAndOrRadioBool = false;
	$rootScope.spForBulkExists = true;
	
	$scope.ExtendedInfoOption1 = true;
	$scope.ExtendedInfoOption2 = false;
	$rootScope.enableAndOrRadioBool = false;
	$rootScope.feedbackPerDialog ="";
	
	$rootScope.hospital = function(){
		if($rootScope.webSettings.salesOrderType=="M2"){
			return true;
		}	
		return false;
	};	
	/* ADM-010 */

	$scope.freeSearchObj.packPiece = window.localStorage['tipoVendita']? window.localStorage['tipoVendita']:'CF';
	$rootScope.setTipoVendita = function(textVal){
		window.localStorage['tipoVendita'] = textVal;
	};
	
	// ADM-008
	$rootScope.hasAlternative = function(item){
		if(isNull(item)){
			return false;
		}else{
			if(!isNull(item.federfarmaBean) &&
					!isNull(item.federfarmaBean.substanceCode) && 
					!isEmptyString(item.federfarmaBean.substanceCode) &&
					item.federfarmaBean.substanceCode != "000000"){
				return true;
			}else{
				return false;
			}
		}
	};

	// ADM-008
	$rootScope.alternativeClicked = function(itemCode){
		if(isNull(itemCode) || isEmptyString(itemCode)) return;
		DataSharingService.setObject("catalog_ProductCode",itemCode);
		$state.go(ALTERNATIVES_STATE);
	};
	
	$rootScope.setFreeSearchObj = function(textVal){
		$scope.freeSearchObj.text = textVal;
		$rootScope.$broadcast('searchTextChange', $scope.freeSearchObj);
	};
	
	$rootScope.setFreeSearchObjAndOrBool = function(textVal){
		$scope.freeSearchObj.enableAndOrRadioBool = textVal;
	};
	
	$rootScope.setAndOr = function(textVal){
		$rootScope.enableAndOrRadioBool = false;
		$scope.freeSearchObj.andOr = textVal;
	};
	
	$rootScope.getFreeSearchObj = function(){
		return $scope.freeSearchObj.text;
	};
	
	
	
	$rootScope.disableCatalogBuy = function(isBuyAllowed,isMatrixItem){
		var disabled = true;
		if($rootScope.isSignedIn){
			if(isBuyAllowed && !isMatrixItem){
				disabled = false;
			}
		}
		return disabled;
		
	};
	$rootScope.disableShoppingList = function(isMatrixItem){
		var disabled = true;
		if($rootScope.isSignedIn){
			if(!isMatrixItem){
				disabled = false;
			}
		}
		return disabled;
		
	};
	
	$rootScope.initDisclaimer = function(){
		if(!isNull($rootScope.webSettings.isShowCookiesDisclaimer)){
			$rootScope.disclaimerObj = new Disclaimer($rootScope.webSettings.isShowCookiesDisclaimer);
		}else{
			$rootScope.disclaimerObj = new Disclaimer(true);
		}
		$rootScope.showDisclaimerSection =  $rootScope.disclaimerObj.showCookieDisclaimer();
	};
	
	$rootScope.updateDisclaimerStatus = function(){
		if(isNull($rootScope.disclaimerObj)){
			$rootScope.disclaimerObj = new Disclaimer($rootScope.webSettings.isShowCookiesDisclaimer);
		}
		$rootScope.disclaimerObj.updateCookieDisclaimer(null,false);
		$rootScope.showDisclaimerSection =  $rootScope.disclaimerObj.showCookieDisclaimer();
	};
	
	$rootScope.getDisclaimerFileName = function(){
		var fileName= "disclaimer_";
		if(!isNull($rootScope.selectedLanguage)){
			fileName = fileName+$rootScope.selectedLanguage.code+".html";
		}else{
			fileName = fileName+"EN"+".html";
		}
		return fileName;
	};
	
	$rootScope.currentOrder_lineCount =0;
	$rootScope.shoppingList_lineCount=0;
	$rootScope.reviewOrder = false;
	$scope.selectedRequest = {};
	historyManager.init($state);
	translate.setTranslationFunc(Util.translate);
	translate.setSanitizerObj($sce);
	//$log.log(" historyManager.getPreviousScopeObject()" +JSON.stringify( historyManager.getPreviousScopeObject()));
	$scope.ShoppingList =  new MyShoppingList(ShoppingListService,Util.translate); 
	$scope.$watch('currentList',function() {
		if(isEmptyString($rootScope.currentList)){
			$rootScope.addToListText=Util.translate('CON_CLICK_TO_ADD_TO_SHOPPING_LIST');
		}else{
			$rootScope.addToListText=Util.translate('CON_CLICK_TO_ADD_TO_SHOPPING_LIST')+": "+$rootScope.currentList;
		}
	   });
	
	$rootScope.validateProductList = function(lineListObject,page)
	{
	   var  lineList = lineListObject.getLineList();
		var buyAllowed=true;
		for(var i=0;i<lineList.length;i++){
			var item=lineList[i];
			var itemProductCode = item.itemCode || item.product;
			var qtn = item.ordered || item.quantity; 
			if(itemProductCode != null && itemProductCode !=""){
				if(isEmptyString(qtn)){
					item['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');		
					item.validQuantity= false;
				}else{
					$rootScope.checkValidQuantity(item,qtn);
				}
				if(isNull(item.unit) || isEmptyString(item.unit)){
					item.invalidUnit = true;
					buyAllowed=false;
					return;
				}
				if(!item.validQuantity){
					buyAllowed=false;
					return;
				}
				//ADM-002 Begin
				if(page !=='REQUEST'){
				if ($rootScope.isShopCartItem(itemProductCode)) {
					item['quantityBoxMsg'] = Util.translate('MSG_ITEM_ALREADY_ENTERED');
					buyAllowed = false;
					return;
				}
				}
				//ADM-002 End
			}
		}
		if(buyAllowed){
			var paramList =	 lineListObject.getProductParamList(lineList,page);
			var requestParam = getProductListRequestParam(paramList);		
			//console.log("add items to cart = " + requestParam);
			$rootScope.fetchProductsDetails(requestParam,lineListObject,page);	      	
		}
	};

	//ADM-XXX
 	$rootScope.validateDateBpr = function(dateStr)	{
		var dateFormat = $rootScope.webSettings.defaultDateFormat;
		this.validExpireDate = true;
		if(Util.isNotNull(dateStr)){
			var dateValidator =new DateValidator(dateFormat);			
			var isValidDate =dateValidator.isValidDate(dateStr);
			if(!isValidDate){
				this.validExpireDate = false;	
			}else{
				this.validExpireDate = true;
			}
			if(dateStr==""||dateStr==null){
				this.validExpireDate = true;
			}
		}
	};

	$rootScope.showHomePageLoader  = function(){
		var flag = false;
		//if ($state.current.name=='home'){
			if(isNull($rootScope.homeCatalogLoaded) || !$rootScope.homeCatalogLoaded){
				flag = true;
			}else{
				flag = false;
			}
		//}
		return false;
	};
	
	$rootScope.changeFormatToLocale = function(value){
		var newVal = value;
		if(!isNull($rootScope.webSettings.localeCode) && $rootScope.webSettings.localeCode !=""){
			if(!isNull($rootScope.webSettings.allowDecimalsForQuantities) && $rootScope.webSettings.allowDecimalsForQuantities == true){
			var langCode = $rootScope.webSettings.localeCode;
			langCode = langCode.replace("_","-");
			newVal = newVal.toLocaleString(langCode);
		}}
		return newVal;
	};
	$rootScope.isEmptyValue = function(str){
		return isEmptyString(str);
	};
	$rootScope.registerWelComeMsg = function(){
		DataSharingService.setToSessionStorage("SHOW_WELCOME_MSG",true);
		DataSharingService.setToSessionStorage("SHOW_WELCOME_MSG_INIT",false);
	};
	
	$rootScope.getAdminMessage = function(){
		var userId = authentication.getUserId();
		if(isNull(userId)){
			userId = $rootScope.webSettings.userCode;
		}
		var params = ["UserID",userId];
		Util.getDataFromServer('getUserAdminMessageUrl', params,false).then(function(data){
			if(!isNull(data.adminMessage) && data.adminMessage != ""){
				alert(data.adminMessage);} 
		});
		
		
	};
	
	$rootScope.registerSendEmailLogin = function(){
		DataSharingService.setToSessionStorage("FROM_EMAIL",true);
	};
	
	$rootScope.removeWelComeMsg = function(){
		DataSharingService.removeFromSessionStorage("SHOW_WELCOME_MSG");
	};
	
	$rootScope.removeSendEmailLogin = function(){
		DataSharingService.removeFromSessionStorage("FROM_HOME_EMAIL");
		DataSharingService.removeFromSessionStorage("sendEmailPending");
		DataSharingService.removeFromSessionStorage("FROM_EMAIL");
	};
	
	$rootScope.showWelComeMsg = function(){

		//$rootScope.initCommitsPromise.promise.then(function(){
			var transitionInfo = DataSharingService.getFromLocalStorage("transitionTo");
			DataSharingService.removeFromLocalStorage("transitionTo");
			//{"callState":callState, "reload":reloadState, "param":params};
			if(!isNull(transitionInfo) && !isNull(transitionInfo.callState)) {
				$state.go(transitionInfo.callState,transitionInfo.param,{reload: transitionInfo.reloadState});
				$rootScope.closeLoader();
			}
		//});
		
		$rootScope.deferredSecondNext.promise.then(function(){
			var isShow = DataSharingService.getFromSessionStorage("SHOW_WELCOME_MSG");
			if(isShow){
				if(!isNull(webSettings) && !isNull(webSettings.showWelcomeText) && webSettings.showWelcomeText) {
					//console.log("------------- calling Dlg---------------");
					if(!isNull($rootScope.openWelComeMsgDlg)){
						var mdl = $rootScope.openWelComeMsgDlg("",["Welcome to NetStore"]);
						mdl.result.then(function() {$rootScope.resolvePendingSendEmail();},
								function() {$rootScope.resolvePendingSendEmail();});
						//test = false;
						var regExNum = /^[0-9]*$/;
						var delayTimeWebSet = webSettings.popUpWindowOpenTime;
						var delayForPopUp = regExNum.test(delayTimeWebSet);
						if(delayTimeWebSet == " " || delayTimeWebSet == null ){
							delayForPopUp = false;
						}
						
						if(webSettings.popUpWindowOpenTime != 0 && delayForPopUp == true){
							var delay =webSettings.popUpWindowOpenTime *1000;
							setTimeout(function() {
								mdl.dismiss('cancel');
								$rootScope.resolvePendingSendEmail();
							}, delay);
						}else{
						mdl.result.then(function() {$rootScope.resolvePendingSendEmail();},
								function() {$rootScope.resolvePendingSendEmail();});}
					} else {
						$rootScope.resolvePendingSendEmail();
					}
					DataSharingService.setToSessionStorage("SHOW_WELCOME_MSG",false);
					DataSharingService.setToSessionStorage("SHOW_WELCOME_MSG_INIT",true);
					historyManager.loginStatusChanged();
				} else {
					$rootScope.resolvePendingSendEmail();
				}
				
			}
		});
	};
	
	$rootScope.showSendMailPopUp = function(){
		$rootScope.deferredSecondNext.promise.then(function(){
			var isShow = DataSharingService.getFromSessionStorage("FROM_EMAIL");
			if(isShow){
				var emailObj = $rootScope.getGeneralEmailObject();
				$rootScope.sendEmail($rootScope.sendEmailEnquiry,emailObj);
				DataSharingService.setToSessionStorage("FROM_EMAIL",false);
			}
		});
	};
	
	$rootScope.validateRequestLines = function(reqLine,page,lineObject)
	{
		var isOpenLoginDlg=$rootScope.isSignOnRequired("CON_CHANGE_REQUEST_LINE",null,false);
		if(!isOpenLoginDlg){
			var requestParam = getItemCodeRequestParam(reqLine.product,page) ;
			if(!isEmptyString(reqLine.product))
			{
				$rootScope.fetchItemDetails(requestParam,reqLine,lineObject);
			}

			else
			{
				lineObject.UIErrorKey = null; 
				lineObject.UIErrorParams = [];
			}
		}
			
	};
	
	$rootScope.getObjectFromList = function(propertyName, value, list){
		return getObjectFromList(propertyName, value, list);
	};
	
	$rootScope.validateCustomerNewAccount = function(custCode,accObj){
		if(accObj.request.accountType == accObj.CONST_INTERNET_ENABLE_CUSTOMER && !isEmptyString(custCode)){
			var params = ["CustomerCode",custCode];
			Util.getDataFromServer('validateCustomerNewAccountUrl',params).then(function(data) {
				accObj.setValidateCustomerResponse(data);
			});
		}
	};
	
	$rootScope.ValidateProductLines = function(productLine,lineObject,page)
	{
		var requestParam = getItemCodeRequestParam(productLine.itemCode,page) ;
		if(!isEmptyString(productLine.itemCode))
		{
			$rootScope.fetchItemDetails(requestParam,productLine,lineObject);
		}
		else
			{
				lineObject.UIErrorKey = null; 
				lineObject.UIErrorParams = [];
			}
		
	};
	
	showMsgNoObjFound = function (msgCode){
		if(isNull($rootScope.isOpenMsgNoObjFound) || !$rootScope.isOpenMsgNoObjFound ){
			var translatedMsgList = [];
			var translatedMessage = Util.translate('MSG_NO_OBJECTS_FOR_SELECTION');
			if(msgCode == "2001"){
				translatedMessage = Util.translate('MSG_SELECT_ANOTHER_CATALOG');
			}
			if(msgCode == "3232"){
				translatedMessage = Util.translate('SP_MISSING_FOR_PRICE_IN_BULK');
			}
			if(msgCode == "3233"){
				translatedMessage = Util.translate('SP_MISSING_FOR_AVAIL_IN_BULK');
			}
			if(msgCode == "115"){
				translatedMessage = Util.translate('MSG_SELECT_ANOTHER_CATALOG');
			}
			
			translatedMsgList.push(translatedMessage);
			var popup = $rootScope.openMsgDlg("",translatedMsgList);
			$rootScope.isOpenMsgNoObjFound = true;
			popup.result.then(function () {
				$rootScope.isOpenMsgNoObjFound = false;
			}, function () {
				$rootScope.isOpenMsgNoObjFound = false;
			});
		}
	};
	
	searchInstanceIssueMsgFound = function (msgList){
		if(isNull($rootScope.isSearchInstanceIssueMsgFound) || !$rootScope.isSearchInstanceIssueMsgFound ){
			var translatedMsgList = [];
			translatedMsgList.push(Util.translate(msgList));
			var popup = $rootScope.openMsgDlg("",translatedMsgList);
			$rootScope.isSearchInstanceIssueMsgFound = true;
			popup.result.then(function () {
				$rootScope.isSearchInstanceIssueMsgFound = false;
			}, function () {
				$rootScope.isSearchInstanceIssueMsgFound = false;
			});
		}
	};
	
	$rootScope.getPaginationObject = function(urlKey , params, scopePageKey){
		var newPageKey  = Util.getPageContextKey(urlKey , params);
		var paginationObj =  null;
		if(isNull(scopePageKey)){
			paginationObj = $rootScope.initPaginationObject(urlKey,params,scopePageKey);
		}else if(newPageKey!=scopePageKey){
			//try to get cached page object 
			paginationObj = DataSharingService.getObject("CACHED_PAGE_OBJECT_"+newPageKey);
			if(isNull(paginationObj)){
				paginationObj = $rootScope.initPaginationObject(urlKey,params,scopePageKey);
			}
		}else{
			paginationObj = DataSharingService.getObject(scopePageKey );
		}
		return paginationObj ;
	};
	
	$rootScope.initPaginationObject = function(urlKey , params, scopePageKey){
		scopePageKey = Util.initPagination(urlKey, params);
		var paginationObj = DataSharingService.getObject(scopePageKey );
		return paginationObj ; 
	};
	
	$rootScope.cachePaginationObject = function(urlKey, dataList, scopeObj){
		var paginationObj = scopeObj['paginationObj'];
		if(!isNull(paginationObj) && ( (Array.isArray(dataList) && dataList.length > 0)  || paginationObj.getPageNo()>1 ) ){
			scopeObj[urlKey+'_cachedPaginationObj'] = deepCopyWithFunctions(paginationObj);
			//console.log("scopeCachePageObj = " + JSON.stringify(scopeObj[urlKey+'_cachedPaginationObj'] ));
				DataSharingService.setObject("CACHED_PAGE_OBJECT_"+paginationObj.objKey,scopeObj[urlKey+'_cachedPaginationObj'] );
		}
	};
	
	$rootScope.setPageLoadMsg = function(dataList,isFirstPageData,scopeObj){
		var paginationObj = scopeObj['paginationObj'];
		if((!Array.isArray(dataList) ||  dataList.length == 0) && !isFirstPageData){
	//	if((!Array.isArray(dataList) ||  dataList.length == 0)){
            paginationObj.setLastPageNo();      
            scopeObj.dataFound = false;
            scopeObj.pageLoadMsg = translate.getTranslation('CON_NO_MORE_PAGE');
      }else if((!Array.isArray(dataList) ||  dataList.length == 0) && isFirstPageData && !scopeObj.filterOn){
    	  paginationObj.setLastPageNo();      
          scopeObj.dataFound = false;
          scopeObj.pageLoadMsg = translate.getTranslation('CON_NO_DATA_FOUND');
      }else if(Array.isArray(dataList) &&  dataList.length > 0){
			scopeObj.dataFound = true;
			scopeObj.pageLoadMsg = "";
		}

	};
	
	$rootScope.applyPreviousPaginationObject = function(urlKey , scopeObj){
		if(!isNull(scopeObj[urlKey+'_cachedPaginationObj'])){
			scopeObj['paginationObj'] = deepCopyWithFunctions(scopeObj[urlKey+'_cachedPaginationObj']) ;
		}
	};
	
	$rootScope.isFirstPageData = function(paginationObj){
		var isFirstPageData = true;
		if(!isNull(paginationObj) && paginationObj.getPageNo()==1){
			isFirstPageData = true;
		}else{
			isFirstPageData = false;
		}
	};
	
	$rootScope.sendEmail =  function (sendEmailFunc,email,dataObject,skipUpdate){
		var isLoginRequired = $rootScope.getSignInRequirement("CON_GENERAL_ENQUIRY");
		var pageState=$state.current.name;
		
		if(dataObject!=undefined){
			if(pageState=='home.order.detail'){
				email.subject = Util.translate('CON_EMAILTMPL_ORDER_SUB') + dataObject.orderNumber ;
			}else if(pageState=='home.invoice.detail'){
				email.subject = Util.translate('CON_EMAILTMPL_INVOICE_SUB') + dataObject.invoiceNumber ;
			}
		}
		
		if(isLoginRequired && !authentication.isLoggedIn()){
			var sep = {
					param1: sendEmailFunc,
					param2: email,
					param3: dataObject
			};
			DataSharingService.setToSessionStorage("sendEmailPending", sep);
			
			//$rootScope.loginRequiredPopup();
			if(!skipUpdate) {
				if(pageState=='home.Compare' || pageState=='home.mainCatalog.Compare' || pageState=='home.prodSearchResult.Compare'){
					$rootScope.open(CONST_State_Home);
				}else{
					$rootScope.open($state.current.name);
				}
			}
			
		}else{
			
			$rootScope.sendEmailWithoutCheck(sendEmailFunc,email,dataObject);
		}
	};
	
	$rootScope.sendEmailWithoutCheck = function(sendEmailFunc,email,dataObject) {
		$rootScope.deferredSecondNext.promise.then(function(){
			if(!isNull($rootScope.webSettings.recipientEmailAddress)){
				if(isNull(email)){
					email = new Email("","","");
				}
				var mode = $rootScope.openEmailDlg("" , email);
				mode.result.then(function () {
					//console.log('email send clicked');
					if(!isNull(sendEmailFunc))
					sendEmailFunc(email,dataObject);
					else
						$rootScope.sendEmailEnquiry(email,dataObject);
				}, function () {
					//console.log('email dialog cancel');
				});
			}else{
				$rootScope.showErrorRecipientEmail();
			}
		});
		
	}
	
	$rootScope.loginRequiredPopup = function(){
		var translatedMsgList = [];
		translatedMsgList.push(Util.translate('MSG_ACCESS_DENIED_SIGN_ON_REQUIRED'));
		$rootScope.openMsgDlg("",translatedMsgList);
	};
	
	// scroll to id
	$rootScope.scrollToId = function(id)
	{
        $location.hash(id);

        $anchorScroll();
	};
	
	
	$rootScope.showForgotPwdDlg = function(){
		var forgotPwdObj = new ForgotPassword();
		var mode = $rootScope.openForgotPwdDlg("" , forgotPwdObj);
		mode.result.then(function () {
			//console.log('showForgotPwdDlg submmit clicked');
		}, function () {
			//console.log('showForgotPwdDlg dialog cancel');
		});
	};
	
	$rootScope.showChangePwdDlg = function(userId,isForcePasswordChange,callState,langCode){
		var changePwdObj = new ChangePassword(userId,isForcePasswordChange,callState,langCode);
		var mode = $rootScope.openChangePwdDlg("" , changePwdObj);
		mode.result.then(function () {
			//console.log('showChangePwdDlg ok clicked');
		}, function () {
			//console.log('showChangePwdDlg cancel clicked');
		});
	};
	
	$rootScope.showSelectCustomerDlg = function(userObject,callState){
		DataSharingService.setObject("SELECT_CUSTOMER_USER_OBJECT",userObject);
		DataSharingService.setObject("SELECT_CUSTOMER_CALL_STATE",callState);
		var mode = $rootScope.openSelectCustomerDlg("");
		mode.result.then(function () {			
			//$log.log('showSelectCustomerDlg ok clicked');
		}, function () {
			//$log.log('showSelectCustomerDlg cancel clicked');
			authentication.slientLogout(userObject.userID).then(function(data){
				$rootScope.reloadHomePage();
			});
		});
	};
	
	$rootScope.submitPasswordReq = function (forgotPwdObj){
		if(!isNull(forgotPwdObj)){
			params = forgotPwdObj.getParams();
			if (params.length > 0){
				AccountService.forgotPassword(params).then(function(data){
					//$log.log('submitPasswordReq ' +  JSON.stringify(data));
					if(!isNull(data) && !isNull(data.messageCode)){
						if(data.messageCode==4100){
							forgotPwdObj.setSuccessMsg(Util.translate('TXT_CF_012'));
						}
						if(data.messageCode==4103){
							forgotPwdObj.setErrorMsg(Util.translate('MSG_PASSWORD_REQUEST_ERROR'));
						}
						if(data.messageCode==4104){
							forgotPwdObj.setErrorMsg(Util.translate('MSG_NOT_VALID_USERID'));
						}
						if(data.messageCode==4105){
							forgotPwdObj.setErrorMsg(Util.translate('MSG_NOT_VALID_EMAIL'));
						}
					}
				});
			}
		}
	};
	
	$rootScope.submitPasswordReqButton = function (forgotPwdObj){
		if(forgotPwdObj.userId != undefined && forgotPwdObj.userId != ""){
			return false;
		}else if(forgotPwdObj.emailId != "" && forgotPwdObj.userId != undefined ){
			
			var returnDisableValue = true;
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		    var returnValue = re.test(forgotPwdObj.emailId);
		    if(returnValue == true){
		    	returnDisableValue = false;
		    }else if(returnValue == false){
		    	returnDisableValue = true;
		    }
		    return returnDisableValue;
		}else {
			return true;
		}
		
	};
	
	$rootScope.sendEmailEnquiry =  function (emailObj){
		var param = [emailObj] ;
		var paramStr = "enquiryMailData="+JSON.stringify(param);
		//console.log("mail param= "+paramStr);
		Util.postDataToServer('sendMailEnquiryUrl',paramStr).then(function(data) {
			data = removeDefaultCallBack(data);
			handleSendMailEnquiryMessageCode(data);
		});
	};
	
	$rootScope.sendEmailEnquiryNEW =  function (emailObj){
		var param = [emailObj] ;
		var paramStr = "enquiryMailData="+JSON.stringify(param);
		//console.log("mail param= "+paramStr);
		Util.postDataToServer('sendMailEnquiryUrl',paramStr).then(function(data) {
			data = removeDefaultCallBack(data);
			handleSendMailEnquiryMessageCodeNEW(data);
		});
	};
	
	$rootScope.postInvoiceEmail =  function (invoiceEmailObj,invoiceEmailDataObj){
		//angular.extend(invoiceEmailObj, $rootScope.invoiceMailData);
		invoiceEmailDataObj.setEmailProperties(invoiceEmailObj);
		var param = [invoiceEmailDataObj] ;		
		var paramStr = "invoiceMailData="+JSON.stringify(param);
		//console.log("invoice mail = "+paramStr);
		
		Util.postDataToServer('sendInvoiceMailUrl',paramStr).then(function(data) {
			data = removeDefaultCallBack(data);
			handleSendMailEnquiryMessageCode(data);
		});
	};
	
	$rootScope.postOrderEmail =  function (orderEmailObj, orderEmailDataObj){
		//angular.extend(invoiceEmailObj, $rootScope.invoiceMailData);
		orderEmailDataObj.setEmailProperties(orderEmailObj);
		var param = [orderEmailDataObj] ;		
		var paramStr = "salesOrderMailData="+JSON.stringify(param);
		//console.log("order mail = "+paramStr);
		
		Util.postDataToServer('sendOrderMailUrl',paramStr).then(function(data) {
			data = removeDefaultCallBack(data);
			handleSendMailEnquiryMessageCode(data);
		});
	};	
	
	$rootScope.showErrorRecipientEmail = function(){
		var msg = '';
		var translatedMsgList = [];
		translatedMsgList.push(Util.translate('NO_RECEPEINT_EMAIL_ID'));
		var dlg = $rootScope.openMsgDlg(msg,translatedMsgList);
		dlg.result.then(function () {
			////$log.info('Send email error dailog Ok clicked');
		}, function () {
			//console.log('Send email error dailog - Cancel clicked');
		});
	};
	$scope.updateOrderDelimeter = function(copyOrderDelimeter){
		
		$rootScope.CopyOrderDelimeter = copyOrderDelimeter;
	};

	var handleSendMailEnquiryMessageCode =function (data){
		var errorMsg = null;
		if(!isNull(data[0].messageCode)){
			var msgCode = data[0].messageCode+"";
			switch (msgCode){
			case '4100' :
				// SUCCESS FULL
				//errorMsg = "mmmmmmmmmmmmmmm";
				
				break;
			case '4101' :
				//Try Later
				errorMsg = Util.translate('MSG_MAIL_NOT_ACTIVE_TRY_LATER');
				break;
			case '4102' :
				// Data Missing
				errorMsg = Util.translate('MSG_MANDATORY_DATA_MISSING');
				break;
			case '4103' :
				//Error
				errorMsg = Util.translate('MSG_UNEXPECTED_ERROR');
				break;
			}
		}
		if(errorMsg!=null){
			var dlg = $rootScope.openMsgDlg(Util.translate('CON_STATUS_EMAIL_SENT'),[errorMsg]);
		}
	};
	
	var handleSendMailEnquiryMessageCodeNEW=function (data){
		var errorMsg = null;
		if(!isNull(data[0].messageCode)){
			var msgCode = data[0].messageCode+"";
			switch (msgCode){
			case '4100' :
				// SUCCESS FULL
				errorMsg = Util.translate('MSG_MAIL_SENT');
				break;
			case '4101' :
				//Try Later
				errorMsg = Util.translate('MSG_MAIL_NOT_ACTIVE_TRY_LATER');
				break;
			case '4102' :
				// Data Missing
				errorMsg = Util.translate('MSG_MANDATORY_DATA_MISSING');
				break;
			case '4103' :
				//Error
				errorMsg = Util.translate('MSG_UNEXPECTED_ERROR');
				break;
			}
		}
		
		if(errorMsg!=null){
			var dlg = $rootScope.openMsgDlg(Util.translate('CON_STATUS_EMAIL_SENT_PROCURA'),[errorMsg]);
		}
	};
	
	$rootScope.sendPrdEnquiry = function(itemCode,itemDesc){
		var isLoginRequired = $rootScope.getSignInRequirement("CON_GENERAL_ENQUIRY");
		var custCode = "";
		var custName = "";
		if($rootScope.isSignedIn){
			custCode = Util.translate('COH_CUSTOMER_NUMBER')+" : "+returnBlankIfNull($rootScope.webSettings.defaultCustomerCode);
			custName = Util.translate('CON_CUSTOMER')+" : "+returnBlankIfNull($rootScope.webSettings.defaultCustomerName);
		}
		var prdCode = Util.translate('COH_ITEM')+" : "+returnBlankIfNull(itemCode);
		var prdDesc = Util.translate('COH_DESCRIPTION') +" : "+returnBlankIfNull(itemDesc);
		var userEmail = $rootScope.getSenderEmailAddress();
		var userMailId = Util.translate('COH_MAIL_GENERATED_FROM') +" : "+userEmail;
		var subject = Util.translate('MSG_SUBJECT_FOR_ENQUIRY_PRODUCT_2');
		var msgBody = "";
		if($rootScope.isSignedIn){
			msgBody = prdCode+"\n"+prdDesc+"\n"+custCode+"\n"+custName+"\n"+userMailId+"\n";
		}else if( isLoginRequired){
			DataSharingService.setToSessionStorage("SEND_PRD_ENQUIRY_CODE",itemCode);
			DataSharingService.setToSessionStorage("SEND_PRD_ENQUIRY_DESC",itemDesc);
			DataSharingService.setToSessionStorage("SEND_PRD_ENQUIRY",true);
		}else{
			msgBody = prdCode+"\n"+prdDesc+"\n";
		}
		email = new Email(userEmail,subject,msgBody);
		$rootScope.sendEmail($rootScope.sendEmailEnquiry,email);
		
	};
	
	$rootScope.sendOrderEmail = function(orderNumber){
		
	//	var orderNo=orderNumber;
		var orderMailData = new OrderMailData(orderNumber);	
		//var subject = Util.translate('CON_ORDER_CONFIRMATION');		
		var subject = Util.translate('CON_EMAILTMPL_ORDER_SUB') + orderNumber;
		var msgBody ="";
		var senderEmail = returnBlankIfNull($rootScope.webSettings.senderEmailAddress);
		var mailTo = returnBlankIfNull($rootScope.webSettings.userEmail);
		var orderEmail = new Email(senderEmail,subject,msgBody,mailTo);
		orderEmail.showMailFrom=false
		orderEmail.showMailTo=true;
		orderMailData.setEmailProperties(orderEmail);
		$rootScope.sendEmail($rootScope.postOrderEmail,orderEmail,orderMailData);
		
	};
	
	$rootScope.sendInvoiceMail = function(){
		var invoice = DataSharingService.getFromSessionStorage("invoice_clickedDetail");
		var invoiceMailData = new InvoiceMailData(invoice.orderNumber,invoice.documentType,invoice.invoiceNumber,invoice.invoiceYear);
		
		var subject = Util.translate('CON_INVOICE');		
		var msgBody ="";
		var senderEmail = returnBlankIfNull($rootScope.webSettings.senderEmailAddress);
		var mailTo = returnBlankIfNull($rootScope.webSettings.userEmail);
		var invoiceEmail = new Email(senderEmail,subject,msgBody,mailTo);
		invoiceEmail.showMailFrom=false;
		invoiceEmail.showMailTo=true;
		invoiceMailData.setEmailProperties(invoiceEmail);
		$rootScope.sendEmail($rootScope.postInvoiceEmail,invoiceEmail,invoiceMailData);
		
	};
		
	$rootScope.resolvePendingSendEmail = function() {
		var emailFromHome = DataSharingService.getFromSessionStorage("FROM_HOME_EMAIL");
		var sendEmailPendingData = DataSharingService.getFromSessionStorage("sendEmailPending");
		var isShow = DataSharingService.getFromSessionStorage("SEND_PRD_ENQUIRY");
		if(!isNull(isShow) && !isNull(sendEmailPendingData)){
		var itemCode =	DataSharingService.getFromSessionStorage("SEND_PRD_ENQUIRY_CODE");
		var itemDesc =	DataSharingService.getFromSessionStorage("SEND_PRD_ENQUIRY_DESC");
			$rootScope.sendPrdEnquiry(itemCode,itemDesc);
			DataSharingService.removeFromSessionStorage("sendEmailPending");
			DataSharingService.removeFromSessionStorage("SEND_PRD_ENQUIRY_CODE");
			DataSharingService.removeFromSessionStorage("SEND_PRD_ENQUIRY_DESC");
			DataSharingService.removeFromSessionStorage("SEND_PRD_ENQUIRY");
		}else if(!isNull(sendEmailPendingData)) {
			if(!isNull(emailFromHome) && authentication.isLoggedIn()){
				DataSharingService.removeFromSessionStorage("FROM_HOME_EMAIL");
				DataSharingService.removeFromSessionStorage("sendEmailPending");
				var emailObj = $rootScope.getGeneralEmailObjectHome();
				$rootScope.sendEmail($rootScope.sendEmailEnquiry,emailObj);
			}else{
			$rootScope.sendEmailWithoutCheck(sendEmailPendingData.param1, sendEmailPendingData.param2,
					sendEmailPendingData.param3);
			DataSharingService.removeFromSessionStorage("sendEmailPending");
			}
		}else if(!isNull(emailFromHome) && authentication.isLoggedIn()){
			DataSharingService.removeFromSessionStorage("FROM_HOME_EMAIL");
			var emailObj = $rootScope.getGeneralEmailObjectHome();
			$rootScope.sendEmail($rootScope.sendEmailEnquiry,emailObj);
		}
	};
	

	$rootScope.getSenderEmailAddressHome = function(){
		var senderEmailId = "";
		if($rootScope.isSignedIn){
			senderEmailId = returnBlankIfNull($rootScope.webSettings.userEmail);
		}
		return senderEmailId ;
	};
	
	$rootScope.getGeneralEmailObjectHome = function(){
		setWebSettingObject(DataSharingService.getWebSettings());
		var senderEmail = $rootScope.getSenderEmailAddressHome();
		var msgBody = "";
		if($rootScope.isSignedIn){
			msgBody = Util.translate('COH_MAIL_GENERATED_FROM') +" : "+senderEmail;
		}
		var emailObj = new Email(senderEmail,"",msgBody);
		return emailObj;
	};
	/*
	 * changes title of the page in title bar of browser
	 * titleString: title to be displayed in title bar of browser
	 * noTranslate: whether translation is needed
	 */
	$rootScope.changeHomePageTitle = function(titleString, noTranslate) {
		$rootScope.homePageTitle = noTranslate ? titleString : Util.translate(titleString);
		//console.log(titleString+"------------ title changed to --- "+$rootScope.homePageTitle);
	}
	
	
	
	$rootScope.$on('$stateChangeStart', 
			function(event, toState, toParams, fromState, fromParams){ 
		
	});
	
	$scope.$on('$stateChangeSuccess', 
			function(event,toState, toParams, fromState, fromParams){
		$rootScope.changeHomePageTitle($state.current.title);
		//$rootScope.resolvePendingSendEmail();
		if(!isNull($rootScope.showWelComeMsg)){
			////console.log("-------------------- calling from configcontrol - state change success--------------------");
			$rootScope.showWelComeMsg();
		} else {
			$rootScope.resolvePendingSendEmail();
		}
		
				var scrollTop = function(){
					window.scrollTo(0, 0);
				};
				var isChildState = false;
				switch(toState.name) 
				{
				
				case REQ_SUBMIT_ORDER_VIEW :
				case REQ_SUBMIT_ORDER_DETAIL_VIEW :
				case REQ_SUBMIT_INVOICE_VIEW :
				case REQ_SUBMIT_INVOICE_DETAIL_VIEW :	       
				case REQ_ADD_PRODUCT_VIEW :
				case REQ_NEW_LINE_VIEW : 
							isChildState = true;
							break;
				case PROMOTION_LIST_PRODUCT_DETAIL:					
				case PROMOTION_GRID_PRODUCT_DETAIL:
				//case PROMOTION_LIST_THANKYOU:
			//	case PROMOTION_GRID_THANKYOU:
				case PROMOTION_LIST_SHOPPING_CART :
				case PROMOTION_GRID_SHOPPING_CART :
							
							isChildState = true;
							$rootScope.scrollToId("result_set");
									break;

				default: 
							isChildState = false;
								
				}
				
			
		if(!isChildState){
			$interval(scrollTop,1000,1);
		}

		historyManager.updateState(fromState.name,fromParams);
	});
	
	$scope.$on('$viewContentLoaded', 
			function(event){ 
		//$rootScope.closeLoader();
		
		
	});
	
	$rootScope.openLoader = function (){
		$rootScope.showLoader  = true ;
		if(isNull($rootScope.loaderCalls)){
			$rootScope.loaderCalls = [];
			$rootScope.loaderCalls.push(new Date());
		}else{
			$rootScope.loaderCalls.push(new Date());
		}
		return $rootScope.showLoader;

	};

	$rootScope.closeLoader = function (delay){
		if(isNull(delay)){
			delay = 500;
		}
		var f1 = function(){
			//$rootScope.showLoader  = false ;
			$rootScope.loaderCalls.pop();
			if($rootScope.loaderCalls.length==0){
			$rootScope.showLoader  = false ;
			}else{
				// do nothing, there still pending calls
			}
			return $rootScope.showLoader; 
		};
		$interval(f1,delay,1);
	};
	
	$rootScope.openInnerLoader = function (){
		$rootScope.showInnerLoader  = true ;
		return $rootScope.showInnerLoader;

	};

	$rootScope.closeInnerLoader = function (delay){
		if(isNull(delay)){
			delay = 500;
		}
		var f1 = function(){
			$rootScope.showInnerLoader  = false ;
			return $rootScope.showInnerLoader; 
		};
		$interval(f1,delay,1);
	};
	

	$rootScope.browserIE9orLower = function(){
		return isIE9orLower();
	};
	
	var isProdSearchView = function (){
		var name = $state.current.name;
		var bool = false;
		switch (name){
		case PROD_SEARCH_RESULT_VIEW :
		case PROD_SEARCH_RESULT_LISTVIEW :
		case PROD_SEARCH_RESULT_GRIDVIEW :
			bool = true;
			break;
		}
		return bool;
	};
	
	$rootScope.enableAndOrRadio = function(value){
		var str = value;
		if($rootScope.webSettings.showOperatorAndOr){
		if(!isNull(str) && str != ""){
			if (/\s/.test(str)) {
			    // It has any kind of whitespace
				$scope.freeSearchObj.enableAndOrRadioBool = true;
			}else{
				$scope.freeSearchObj.enableAndOrRadioBool = false;
			}
			
		}else{
			$scope.freeSearchObj.enableAndOrRadioBool = false;
		}
		}
		$rootScope.setFreeSearchObjAndOrBool($scope.freeSearchObj.enableAndOrRadioBool);
	};
	
	$rootScope.searchClick = function(){
		var freeSearchObj = $scope.freeSearchObj;
		var val=$scope.freeSearchObj.text;
		var logicAndOrBool = $scope.freeSearchObj.enableAndOrRadioBool;
		var andOrLogic = "";
		if($rootScope.webSettings.showOperatorAndOr){
			if(logicAndOrBool){
				andOrLogic = $scope.freeSearchObj.andOr;
			}else{
				andOrLogic = "OR";
			}
		}
		$rootScope.setFreeSearchObj($scope.freeSearchObj.text);
		if($rootScope.webSettings.showOperatorAndOr){
		$rootScope.setAndOr($scope.freeSearchObj.andOr);
		}
		historyManager.removeCache("prodSearchCtrl");
		$rootScope.$broadcast('searchTextChange', freeSearchObj);

		if(isProdSearchView()){
			$rootScope.freeTextSearchSearchPage(val,andOrLogic,logicAndOrBool);
		}else{
			if(!isNull($scope.freeSearchObj.enableAndOrRadioBool)){
				$state.go(PROD_SEARCH_RESULT_VIEW,{FREE_TEXT:val,AND_OR:andOrLogic,BOOL_VALUE:$scope.freeSearchObj.enableAndOrRadioBool});
			}else{
			$state.go(PROD_SEARCH_RESULT_VIEW,{FREE_TEXT:val,AND_OR:andOrLogic});
			}
		}
//		$state.go(PROD_SEARCH_RESULT_VIEW,{FREE_TEXT:val});
	};

	$rootScope.isShowLoader = function (){
		return $rootScope.showLoader; 
	};
	$rootScope.isShowInnerLoader = function (){
		return $rootScope.showInnerLoader; 
	};

	$rootScope.searchInputValid = true;
	
	$rootScope.isFilterRangeEmpty = function(filter)	{
		var isFilterEmpty = isFilterRangeEmpty(filter);
	};
	$rootScope.validateDateFormat = function(filter,dateStr,secondMsg)	{
		
		var dateFormat = $rootScope.webSettings.defaultDateFormat;
		var isFilterTextEmpty = false;
		
		if(isNull(secondMsg)){
			secondMsg = false;}
		if(isNull(dateStr ) ||isEmptyString(dateStr)){	
			isFilterTextEmpty = true;
		}
		
		if(!isFilterTextEmpty){
//			var isValidDate = isValidDateFormat(dateStr,dateFormat);
			var dateValidator =new DateValidator(dateFormat);			
			var isValidDate =dateValidator.isValidDate(dateStr);
			if(Util.isNotNull(dateStr)){
				if(isValidDate){
					resetFilterMessage(filter,secondMsg);
					
				}else{
					setDateMessage(filter,secondMsg);
				}
			}
		}else{
			resetFilterMessage(filter,secondMsg);			
		}
		
	};
	$rootScope.validateDateRangeFormat = function(filter,dateStr,secondMsg)	{
		var dateFormat = $rootScope.webSettings.defaultDateFormat;
		var isFilterTextEmpty = isFilterRangeEmpty(filter);
		
		if(isNull(secondMsg)){
			secondMsg = false;}
		
		if(!isFilterTextEmpty){
//			var isValidDate = isValidDateFormat(dateStr,dateFormat);
			var dateValidator =new DateValidator(dateFormat);			
			var isValidDate =dateValidator.isValidDate(dateStr);
			if(Util.isNotNull(dateStr)){
				if(isValidDate){
					resetFilterMessage(filter,secondMsg);
					
				}else{
					setDateMessage(filter,secondMsg);
				}
			}
		}
		
	};
	
	$rootScope.filterValue = function($event){
		//console.log("event: "+$event.which);
		var keyCode = [45,46,47,48,49,50,51,52,53,54,55,56,57];
		//console.log($.inArray($event.which,keyCode));
		if($.inArray($event.which,keyCode) == -1) {
			$event.preventDefault();
        }
	}
	
	$rootScope.validateDateRangeFormatDialog = function(dateStr,secondMsg)	{
		var dateFormat = $rootScope.webSettings.defaultDateFormat;
		this.validDatePriceCalc = true;
		if(isNull(secondMsg)){
			secondMsg = true;}
		if(Util.isNotNull(dateStr)){
			var dateValidator =new DateValidator(dateFormat);			
			var isValidDate =dateValidator.isValidDate(dateStr);
			if(!isValidDate){
				this.validDatePriceCalc = false;	
			}else{
				this.validDatePriceCalc = true;
			}
			if(dateStr==""||dateStr==null){
				this.validDatePriceCalc = true;
			}
		}
	};
	
	
	
	$rootScope.validateAmount = function(filter,dateStr,secondMsg)	{
		
		if(isNull(secondMsg)){
			secondMsg = false;}
		var isFilterTextEmpty = isFilterRangeEmpty(filter);
		if(!isFilterTextEmpty){
			var isValidNumber = $rootScope.isInt(dateStr);
			resetFilterMessage(filter,secondMsg);
			//removed all validation for amount field
//			if(Util.isNotNull(dateStr)){
//				if(isValidNumber){
//					resetFilterMessage(filter,secondMsg);
//					
//				}else{
//					setAmountMessage(filter,secondMsg);
//				}
//			}
		}
		
	};
	//
	$rootScope.validateNumber = function(filter,data,secondMsg)	{
		if(filter.numericType){
			//$rootScope.searchInputValid = true;
			if(isNull(secondMsg)){
				secondMsg = false;}
			if(secondMsg){
				filter.validationMsg2  = "";
			}else{
				filter.validationMsg  = "";
			}
			
			var isNumber = $rootScope.isInt(data);
			if(Util.isNotNull(data)){
				if(data.length > 0)	{
					if(!isNumber){
						
						var filterName = Util.translate(filter.filterName,null);
						if(secondMsg){
							filter.isValidInput2 = false;
							filter.validationMsg2 =   Util.translate('MSP_INVALID_NUMERIC_VALUE',[filterName, data]);
						}else{
							filter.isValidInput = false;
							filter.validationMsg  =  Util.translate('MSP_INVALID_NUMERIC_VALUE',[filterName, data]);	
						}
						
					}else{
						if(secondMsg){
							filter.isValidInput2 = true;	
							filter.validationMsg2  = "";	
						}else{
							filter.isValidInput = true;	
							filter.validationMsg = "";//	
						}
						
					}	
					
				}else{
					if(secondMsg){
						filter.isValidInput2 = true;
						filter.validationMsg2  = "";	
					}else{
						filter.isValidInput = true;
						filter.validationMsg = "";//	
					}
					
				}
			}
		}
		
	};
	$rootScope.isFilterHasError  = function(filterList)
	{
		var isSearchInputInvalid = false;
		if(Util.isNotNull(filterList))
		{
			for(var i=0;i<filterList.length;i++)
				{
					if(filterList[i].isValidInput == false) {
						isSearchInputInvalid = true	;
					}
					if( filterList[i].isValidInput2 == false){
						isSearchInputInvalid = true	;
					}
					if(isNull(filterList[i].isValidInput)){
						filterList[i].isValidInput = true;
					}
					if(isNull(filterList[i].isValidInput2)){
						filterList[i].isValidInput2 = true;
					}
				}
			
		}
		return isSearchInputInvalid;
	};

	$rootScope.isInt = function(n){
		 return /^-?[\d.]+(?:e-?\d+)?$/.test(n); 
	};
	
	$rootScope.isIntTransFilter = function(n){
		 return /^-?(\$|)([0-9]\d{0,2}(\,\d{3})*|([0-9]\d*))(\.\d{2})?$/.test(n); 
	};
	
	$rootScope.openBottomLoader = function (){
		$rootScope.showBottomLoader  = true ;
		return $rootScope.showBottomLoader;
	};

	$rootScope.closeBottomLoader = function (){
		$rootScope.showBottomLoader  = false ;
		return $rootScope.showBottomLoader; 
	};

	$rootScope.isShowBottomLoader = function (){
		return $rootScope.showBottomLoader; 
	};
	
	$rootScope.getTranslatedHTML = function (key){
		val = Util.translate(key);
	    return $sce.trustAsHtml(val);
	};

//	$rootScope.deferredSecondNext = $q.defer();
//	$rootScope.initCommitsSecondNextPromise = $rootScope.deferredSecondNext.promise;
//	$rootScope.deferredNext = $q.defer();
//	$rootScope.initCommitsNextPromise = $rootScope.deferredNext.promise;
	
	
	$rootScope.deferredSecondNext = $q.defer();
	$rootScope.initCommitsSecondNextPromise = $rootScope.deferredSecondNext.promise;
	
	var deferred = $q.defer();
	//initCommitsPromise is promise that all required initialization is done.
	$rootScope.initCommitsPromise = deferred.promise;
	$rootScope.openLoader();
	NewConfigService.init(authentication.getUserId()).then(function(data){
		deferred.resolve();
		$rootScope.initDisclaimer();
		$rootScope.closeLoader();
		authentication.syncUISession();
		$rootScope.changeHomePageTitle($state.current.title);
		if(authentication.isLoggedIn()){
			$rootScope.isSignedIn = true;
			ShoppingCartService.getTemporaryOrderData([]);
			/*Util.getFilterListDetails([]).then(function(data){
				DataSharingService.setFilterDetailsSettings(data);
				$rootScope.filterDetailSettings = data;				
			});*/
			setWebSettingObject(DataSharingService.getWebSettings());
			$rootScope.shoppingList_lineCount =$rootScope.webSettings.numOfShopListItemLines;
			$rootScope.currentList=$rootScope.webSettings.defaultShoppingListId;
			$rootScope.sendMailOnRequest= !isEqualStrings($rootScope.webSettings.sendMailOnRequest,"*NO");
			$rootScope.CopyOrderDelimeter = $rootScope.webSettings.defaultDelimiter;
			$rootScope.getLocaleList($rootScope.currentLocaleList); 
			$rootScope.deferredSecondNext.resolve();
			$rootScope.showWelComeMsg();
		}else{
			$rootScope.isSignedIn = false;
			Util.getFilterListDetails([]).then(function(data){
				DataSharingService.setFilterDetailsSettings(data);
				$rootScope.filterDetailSettings = data;
				setWebSettingObject(DataSharingService.getWebSettings());
				$rootScope.deferredSecondNext.resolve();
			});
			$rootScope.showSessionExpireMSGAfterPageLoad();
		}
		//console.log("--------------------- hey filters calling in progress-----------------");
//		Util.getProductFilterDetails([]).then(function(data){
//			//console.log("--------------------- hey filters received-----------------");
//			DataSharingService.setProductFilterDetailsSettings(data);
//			$rootScope.productfilterDetailSettings = data;
//			$rootScope.filterDataUpdated = true;
//			$rootScope.$broadcast('filterDataUpdated');
//		});
		$rootScope.BASE_ADDRESS = NewConfigService.getWSBaseAddress();
		//NewConfigService.getSortedKeys();
	});
	
	var bubbleCartAnimation = function (){
		$rootScope.showCartAnimation = true;
		var animate = function(){
			$rootScope.showCartAnimation = false;
		};
		
		$interval(animate,2000,1);
	};
	
	var shoppingListAnimation = function (){
		$rootScope.showShoppingListAnimation = true;
		var animate = function(){
			$rootScope.showShoppingListAnimation = false;
		};
		jQuery(document).ready(function(){
			jQuery(".wishlist-icon").effect('shake', 1000 );  
		  });
		  
		$interval(animate,2000,1);
	};
	
	$rootScope.populateBubbleCart = function(data, showCartPopUp){
		var temp  = $rootScope.currentOrder_lineCount;
        if(Util.isNotNull(data.currentOrder) && Util.isNotNull(data.currentOrder.lineCount))
        {
        	if(!isNull(data) && !isNull(data.currentOrder.lineCount)){
        		$rootScope.currentOrder_lineCount = data.currentOrder.lineCount;
        	}else{
        		$rootScope.currentOrder_lineCount = 0;
        	}
        	if(!isInt($rootScope.currentOrder_lineCount)){
        		$rootScope.currentOrder_lineCount = 0;
        	}
        	if(temp!=$rootScope.currentOrder_lineCount){
        		bubbleCartAnimation();
            	if(showCartPopUp){
    				$scope.showPopUp = true;
    				$timeout(function(){$scope.showPopUp = false}, 5000);
    				if($('#advSearch .form-control').size()>0){
    					$('#advSearch .form-control').each(function(){
    						$(this).val('');
    					});
    				};
    				
    				$('.searchBoxheader').val('');
            	}
        	}
       //     updateShopCartItemList(data);
        }
        else{
        	$rootScope.setBubbleCartToZero();
        }
       	 updateShopCartItemList(data);
       
  };
  
  $rootScope.populateShoppingListCount=function(data, currentList){
	  var temp  = $rootScope.shoppingList_lineCount;
	  if(!isNull(data.defaultShoppingListId) && !isNull(data.numOfShopListItemLines)){
		  if(isEmptyString(data.defaultShoppingListId) && data.numOfShopListItemLines==0){
			  $rootScope.currentList="";
		  }
	  }
	  
	  if(!isNull(currentList)){
		  $rootScope.currentList=currentList;
	  }
      	if(!isNull(data) && !isNull(data.numOfShopListItemLines)){
      		$rootScope.shoppingList_lineCount = data.numOfShopListItemLines;
      	}else{
      		$rootScope.shoppingList_lineCount = 0;
      	}
      	if(!isInt($rootScope.shoppingList_lineCount)){
      		$rootScope.shoppingList_lineCount = 0;
      	}
      	if(temp!=$rootScope.shoppingList_lineCount){
      		shoppingListAnimation();
      	}
  };
  $rootScope.setBubbleCartToZero = function(){	  
	 var data =  {};	 
	 data.currentOrder ={};
	 data.currentOrder.lineCount=0;
	 $rootScope.populateBubbleCart(data);
  };
 	var updateShopCartItemList = function(data){
  		if(!isNull(data) && !isNull(data.currentOrder) && !isNull(data.orderLines)){
  			$rootScope.orderLinesData =  data;
  			var orderLinesNumbers = Object.keys(data.orderLines );
  			if(Array.isArray(orderLinesNumbers)){
  				var shopCartItemList = [];
  				for(var i=0;i<orderLinesNumbers.length;i++){
  					var ordLine = data.orderLines[orderLinesNumbers[i]+""];
  					//ADM-010
  					if($rootScope.webSettings.allowSelectUnitOfMeasure){
  						$rootScope.updatePackPieceFromShoppingCart(
  								$scope.freeSearchObj.packPiece, ordLine);
  					}
  					//ADM-010 end
  					if(!isNull(ordLine.ivItem) && !isNull(ordLine.ivItem.code)){
  						var shopItem = new Object();
  						shopItem.itemCode =ordLine.ivItem.code;
  						shopCartItemList.push(shopItem);
  					}
  				};
  	  			$rootScope.shopCartItemList = shopCartItemList;
  			}
  		}
  		else{
  			$rootScope.shopCartItemList = [];
  		}
  	};
  	
  	$rootScope.isShopCartItem = function(itemCode){
  		var item = getObjectFromList("itemCode",itemCode,$rootScope.shopCartItemList);
   		if(isNull(item)){
  			return false;
  		}else{
  			return true;
  		}
  	};



	$rootScope.convertAsNumber = function(numStr){
		var num = 0;
		if(angular.isDefined(numStr) && numStr!=null){
			num = numStr.replace(/,/gi, "");
			num = num.replace(/\s/g, '');
		}
		return num;
	};

	$rootScope.getId = function(prefix,suffix){
		var id ="" ;
		if(angular.isDefined(prefix) && prefix!=null){
			id = prefix;
		}
		if(angular.isDefined(suffix) && suffix!=null){
			id = id + suffix;
		}
		return id;
	};

	$rootScope.showAvailability = function(){
		webSettings = $rootScope.webSettings;
		itemAvailabilityType = webSettings.itemAvailabilityType;
		var show = false; 
		//to do - implement logic here
		switch (itemAvailabilityType)
		{
		case '*LINK': show = true;
		break;
		//default: 
		}
		return show;
	};

	$rootScope.goHome= function(){
		$state.go(CONST_State_Home);
	};

	$rootScope.goHomeCatalog= function(){
		$state.go('home.homeCatalogGrid');
	};

	
	$scope.goShoppingCart= function(){
		$state.go('home.shoppingcart');
	};

	$rootScope.displayAvailabilityLinkImage = function (item){
		return displayAvailabilityLinkImage(item);
	};
	
	$rootScope.displayAvailabilityLinkText = function (item){
		return displayAvailabilityLinkText(item);
	};
	
	$rootScope.displayAvailabilityImage = function (item){
		return displayAvailabilityImage(item);
	};
	
	$rootScope.displayAvailabilityText = function (item){
		return displayAvailabilityText(item);
	};
	
	$rootScope.getHomePageURL = function(){
		var currentURL = $location.absUrl();
		var baseUrl ="";
		var homePage= "home.html";
		var n = currentURL.indexOf(homePage);
		var index = n + homePage.length;
		if(n!=-1){
			baseUrl = currentURL.substring(0,index);
		}
		return baseUrl ;
	};
	
	$rootScope.reloadHomePage = function(){
		var currentURL = $location.absUrl();
		var homePage= "home.html";
		var n = currentURL.indexOf(homePage);
		var index = n + homePage.length;
		if(n!=-1){
			var baseUrl = currentURL.substring(0,index);
			$window.location.assign(baseUrl);
		}else{
			$rootScope.goHome();
		}
	};
	
	$rootScope.reloadHomePageUrl = function(){
		var currentURL = $location.absUrl();
		var homePage= "home.html";
		var n = currentURL.indexOf(homePage);
		var index = n + homePage.length;
		if(n!=-1){
			var baseUrl = currentURL.substring(0,index);
			$window.location.assign(baseUrl);
		}else{
			$window.location.reload();
		}
	};

	$rootScope.displayWhsAvailabilityImage = function (itemWhsAvailability){
		return displayWhsAvailabilityImage(itemWhsAvailability);
	};

	$rootScope.getWhsAvailabilityImage = function(itemWhsAvailability){
			return getWhsAvailabilityImage(itemWhsAvailability);
	};
		
	$rootScope.getWhsAvailabilityText = function(itemWhsAvailability){
			return getWhsAvailabilityText(itemWhsAvailability);
	};
	
	
	$rootScope.displayWhsOnHand = function (itemWhsOnHand){
		return displayWhsOnHand(itemWhsOnHand);
	};

	$rootScope.getWhsOnHandImage = function(itemWhsOnHand){
			return getWhsOnHandImage(itemWhsOnHand);
	};
		
	$rootScope.getWhsOnHandText = function(itemWhsOnHand){
			return getWhsOnHandText(itemWhsOnHand);
	};
	
	$rootScope.showAvailabilityAsLink = function(){
		//webSettings = $rootScope.webSettings;
		if(webSettings == undefined){
			setWebSettingObject(DataSharingService.getWebSettings());
		}
		itemAvailabilityType = webSettings.itemAvailabilityType;
		var showAsLink = true; 
		switch(itemAvailabilityType){
		case '*LINK' :
		case '*MAX_100' :	
		case '*MIN' :
			showAsLink = true;
			break;
		}
	//	$log.log("showAvailabilityAsLink "+ showAsLink);
		return showAsLink;
	};
	
	$rootScope.showAvailabilityAsImage = function(){
		setWebSettingObject(DataSharingService.getWebSettings());
		itemAvailabilityType = webSettings.itemAvailabilityType;
		var showAsImage = false; 
		switch(itemAvailabilityType){
		case '*YES_NO_ICON' :
		case '*YES_NO_ICONS' :
		case '*NO_ICON'	:
		case '*YES_ICON' :
		case '*LINK' :	
			showAsImage = true;
			break;
		}
		//$log.log("showAvailabilityAsImage "+ showAsImage);
		return showAsImage;
	};
	
	$rootScope.getAvailabilityImage = function(availabilityValue){
//		webSettings = $rootScope.webSettings;
		if(webSettings == undefined){
			setWebSettingObject(DataSharingService.getWebSettings());
		}
		itemAvailabilityType = webSettings.itemAvailabilityType;
		var imageName = ''; 
		var val = '';
		if(Util.isNotNull(availabilityValue) ){
			val = availabilityValue.toUpperCase();
		}
		switch(itemAvailabilityType){
		case '*YES_NO_ICON' :
		case '*YES_NO_ICONS' :
		case '*NO_ICON'	:
		case '*YES_ICON' :
			if(isEqualStrings(val,'YESIMAGE',true)){
				imageName = 'YesImage.png';
			}if(isEqualStrings(val,'NOIMAGE',true)){
				imageName = 'NoImage.png';
			}
			break;
		case '*LINK' :
			//show ware house image in this case
			imageName = 'InfoIcon.png';
			break;
		};

	//	$log.log("getAvailabilityImage "+ imageName);
		return imageName;
	};
	
	$rootScope.getAvailabilityText = function(availabilityValue){
//		webSettings = $rootScope.webSettings;
		if(webSettings == undefined){
			setWebSettingObject(DataSharingService.getWebSettings());
		}
		itemAvailabilityType = webSettings.itemAvailabilityType;
		var val = null;
		var returnVal = null;
		if(Util.isNotNull(availabilityValue)){
			switch(itemAvailabilityType){
			case '*YES_NO_TEXT' :
				val = availabilityValue.toUpperCase();
				if(val=='YES'){
					returnVal = Util.translate('CON_YES');
				}if(val=='NO'){
					returnVal = Util.translate('CON_NO');
				}
				break;
			case '*MAX_100' :	
				if(val > 100){
					returnVal = ">100";
				}
				break;
			}
		}
		if (returnVal==null){
			returnVal = availabilityValue;
		}
		//$log.log("getAvailabilityText "+ returnVal);
		return returnVal;
	};
	
	$rootScope.getEnquiryImageName = function(enquiryImage){
		var imageName = null;
		if(Util.isNotNull(enquiryImage)){
			imageName = 'Email.png';
		}
		return imageName;
	};
	$rootScope.getFavImageName = function(enquiryImage){
		var imageName = null;
		if(Util.isNotNull(enquiryImage)){
			imageName = 'ButtonFav.png';
		}
		return imageName;
	};
	
	$rootScope.showEnquiryImage = function(enquiryImage){
		var bool = false;
		if(Util.isNotNull(enquiryImage) && isEqualStrings(enquiryImage,'InquiryImage',true)){
			bool = true;
		}
		return bool;
	};
	$rootScope.buyItem = {};
	$rootScope.lineQuantity ={};
	$rootScope.toggleSortOrderType = function(sortOrder){
		if(sortOrder == "ASC" )
		{sortOrder = "DESC";}
		else
		{sortOrder = "ASC";}

		return sortOrder;
	};
	$rootScope.goBackPage = function(){
		historyManager.goBack();
	};
	

	$rootScope.goBack = function()
	{
//		$log.log("$window.history : " +Object.keys($window.history));
//		$log.log("$window.history1 : " +$window.history.state);
//		$log.log("$window.history2 : " +$window.history.length);
//		$window.history.back();
		historyManager.goBack();
	};
	
//	$rootScope.reLogin = function(messageCode)
//	{
//		if(Util.isNotNull(messageCode) &&  (messageCode == "5000" || messageCode == 5000))
//		{
//			if(!Util.isNotNull($scope.callState))
//			{		$scope.callstate = 'home';		}
//			$rootScope.open($scope.callState);			
//		}
//		$log.log("messageCode -- " + messageCode);	
//	};

	$rootScope.showSessionExpireMSGAfterPageLoad = function(){
		var isShowMsg = DataSharingService.getFromSessionStorage("SHOW_SESSION_EXPIRE_MSG");
		if(isShowMsg){
			DataSharingService.setToSessionStorage("SHOW_SESSION_EXPIRE_MSG",false);
			var translatedMsgList = [];
			translatedMsgList.push(Util.translate('MSG_SESSION_EXPIRED_LOGIN_AGAIN'));
			$rootScope.openMsgDlg("",translatedMsgList);
		}
	};
	
	$rootScope.handleSessionExpire = function(data){
		var translatedMsgList = [];
		translatedMsgList.push(Util.translate('MSG_SESSION_EXPIRED_LOGIN_AGAIN'));
		showSessionExpirePopUp(translatedMsgList);
	};

	$rootScope.handleSessionNotFound = function(data){
		var translatedMsgList = [];
		if(!isNull(data) && !isNull(data.messageCode)){
			var messageCode=data.messageCode+"";
			switch(messageCode){
			case "5000" :
				translatedMsgList.push(Util.translate('MSG_ACCESS_DENIED_SIGN_ON_REQUIRED'));
				showSessionExpirePopUp(translatedMsgList);
				break;
			case "5001" :
				translatedMsgList.push(Util.translate('MSG_ERROR_IN_PROCESSING_SIGN_ON_REQUIRED'));
				showSessionExpirePopUp(translatedMsgList);
				break;
			}
		}
	};

	var showSessionExpirePopUp = function(msgList){
		var msg = '';
		if(isNull($rootScope.isSessionExpireMsgPopUpOpen) || $rootScope.isSessionExpireMsgPopUpOpen){
			$rootScope.isSessionExpireMsgPopUpOpen = true;
			if(msgList[0] == ""){
				msgList[0] = "Access to function denied. You must log in to use this function.";
			}
			var dlg = $rootScope.openMsgDlg(msg,msgList);
			dlg.result.then(function () {
				$rootScope.isSessionExpireMsgPopUpOpen=false;
				if(location.hash.indexOf('shoppingCart') == -1){
					$rootScope.reloadHomePage();
				}else{
					var needToLogin = $rootScope.isSignOnRequired("CON_SHOPPINGCART",'home.shoppingcart',true);
					//console.log("############## setInterval ##################");
					if(!needToLogin){
						$state.go('home.shoppingcart'); 	
					}
				}
			}, function () {
				$rootScope.isSessionExpireMsgPopUpOpen=false;
				if(location.hash.indexOf('shoppingCart') == -1){
					$rootScope.reloadHomePage();
				}else{
					var needToLogin = $rootScope.isSignOnRequired("CON_SHOPPINGCART",'home.shoppingcart',true);
					//console.log("############## setInterval ##################");
					if(!needToLogin){
						$state.go('home.shoppingcart'); 	
					}
				}
			});
		}
	};
	
	$scope.renderHtml = function (htmlCode) {
        return $sce.trustAsHtml(htmlCode);
    };
	$scope.renderHtmlTranslation = function (htmlCode) {
		
        return $sce.trustAsHtml(Util.translate(htmlCode));
    };
// to update filter response with filter configuration setting
    $rootScope.getSearchFilterSettings = function(responseFilter,filterSettings)
	{
    	if(responseFilter != null)
    	{


    		for(var i=0;i<responseFilter.length;i++)
    		{
    			for(var j=0;j<filterSettings.length;j++)
    			{
    				if(filterSettings[j].filterName == responseFilter[i].filterName)
    				{
    					responseFilter[i]['filterParam'] = filterSettings[j].filterParam;
    					responseFilter[i]['isInputList'] = filterSettings[j].isInputList;	
    					responseFilter[i]['dataList'] = filterSettings[j].dataList;	
    					responseFilter[i]['numericType'] = filterSettings[j].numericType;
    					responseFilter[i]['rangeType'] = filterSettings[j].rangeType;
    					if(filterSettings[j].rangeType)
    					{

    						responseFilter[i]['infoType'] = filterSettings[j].infoType;		 							
    						responseFilter[i]['filterParamValue1'] = filterSettings[j].filterParamValue1;
    						responseFilter[i]['filterParamValue2'] = filterSettings[j].filterParamValue2;

    					}
    				}	 		
    			}
    		}
    	}
    	//console.log(" filter Data--  " +JSON.stringify(responseFilter));
		return responseFilter;
	};
	// making web service param list
	$rootScope.setParamKeyList = function(searchKeyList)
	{		
		var star= '*';
		var comma =',';			
		var  slash= '/'; 
		 slash = new RegExp(slash, 'g');
		 //star = new RegExp(star, 'g');
		 comma = new RegExp(comma, 'g');
		 
			var requestParams = [];	
		for(listItem in searchKeyList)
		{			
			var listVal =	searchKeyList[listItem];
			var isNumber = angular.isNumber(listVal);
			if(listVal == null) {  isNumber = true;	}		
			
			if(!isNumber)
			{
				listVal = listVal.replace(/ /g,"%20");
				listVal = listVal.replace(star,"%2A");
				listVal = listVal.replace(comma,"%2C");
				listVal = listVal.replace(slash,"%2F");
				
			
				if(listVal.length > 0)
				{					
					requestParams.push(listItem);		
					requestParams.push(listVal);
				}
			}
			else if(listVal != null) 			
			{							
				requestParams.push(listItem);		
				requestParams.push(listVal);
			}				
		}			
		return requestParams;

	};
	
	
	$rootScope.getParamKeyList = function(filterOn,filterParam,orderType,sortColumn)
	{
		var i=0;
		var requestParams =[];
		if(filterOn == true)
		{
			requestParams = ["OrderBy",orderType,"SortBy",sortColumn];
			if(filterParam.length > 0)
			{
				for(i=0;i<filterParam.length;i++)
					{requestParams.push(filterParam[i]);}
			}
		}
		else			
		{	requestParams = ["OrderBy",orderType,"SortBy",sortColumn];}
			
		return requestParams;		
	};
	
	// to check property existance 
	$rootScope.isColExist = function(colPropName,propertyList){
		var isCol = false;
		var val = Util.isPropertyExist(colPropName,propertyList);
		if(val){
			isCol = true;

		}
		return isCol;
	};
	
	// bubble cart signOn check
    $rootScope.isShoppingListSignOnRequired = function()
    {
    	if($rootScope.isShowDisableMenuItem('CON_SHOPPING_LIST')){
	    	var signonRequired = $rootScope.getSignInRequirement("CON_SHOPPING_LIST");    	
	    	
	    	var loginStatus = authentication.isLoggedIn();
	    	var call_State = 'home.shoppinglist';
	    	
	    	if(loginStatus == false)
			{	if(signonRequired == true)                		  		
				{$rootScope.open(call_State);}
			}
			else
			{				
				$state.go(call_State);
	
			}
    	}
    	
    };
    
    
    
    $rootScope.isShoppingListSignOnRequiredOpen = function()
    {
    	var signonRequired = $rootScope.getSignInRequirementOpenWindow("CON_SHOPPING_LIST");    	
    	
    	var loginStatus = authentication.isLoggedIn();
    	var call_State = 'home.shoppinglist';
    	
    	if(loginStatus == false)
		{
    		
    		if(signonRequired == true)                		  		
			{
    			var translatedMsgList = [];
    			translatedMsgList.push(Util.translate('MSG_ACCESS_DENIED_SIGN_ON_REQUIRED'));
    			var msg = '';
    			if(isNull($rootScope.isSessionExpireMsgPopUpOpen) || $rootScope.isSessionExpireMsgPopUpOpen){
    				$rootScope.isSessionExpireMsgPopUpOpen = true;
    				var dlg = $rootScope.openMsgDlg(msg,translatedMsgList);
    				dlg.result.then(function () {
    					$rootScope.isSessionExpireMsgPopUpOpen=false;
    					$rootScope.reloadHomePage();
    				}, function () {
    					$rootScope.isSessionExpireMsgPopUpOpen=false;
    					$rootScope.reloadHomePage();
    				});
    			}	
			}
		}
		else
		{				
			$state.go(call_State);

		}
    	
    };
    
	// bubble cart signOn check
    $rootScope.isShoppingCartSignOnRequired = function()
    {
    	var needToLogin = $rootScope.isSignOnRequired("CON_SHOPPINGCART",'home.shoppingcart',true);
		if(!needToLogin){
			if(isNull($rootScope.disableCartClick) || $rootScope.disableCartClick == false)
			{			$state.go('home.shoppingcart');} 	
		}
		
//    	var signonRequired = $rootScope.getSignInRequirement("CON_SHOPPINGCART");
//    	
//    	var loginStatus = authentication.isLoggedIn();
//    	var call_State = 'home.shoppingcart';
//
//    	if(loginStatus == false)
//		{	if(signonRequired == true)                		  		
//			{$rootScope.open(call_State);}
//		}
//		else
//		{				
//			$state.go(call_State);
//
//		}
    	
    };
    
    
	$rootScope.isEmptyString = function (str){
		return isEmptyString(str);
	};
	
   
	$rootScope.resultPerPageList = [1,2,3,4,5,6,7,8,9,10,12,15,20,30,50,100]; // Search Filters 'ResultPerPage' drop-down values
	$rootScope.reqTemplate = {};
	
	//
	$rootScope.getExportFileName = function(fileName,fileExtn)
	{
	 var file_Name = fileName +"_" +getCurrentDateTimeStr()+fileExtn;
	 return file_Name;
	};
	//to show product detail
	$rootScope.showProductDetail = function(productCode,isWhsTabOpen){
		if(!$rootScope.isShowDisableMenuItem('CON_PRODUCT_INFORMATION')){
			return;
		}
		if(isNull(productCode) || isEmptyString(productCode)) return;
		var item_code = productCode; 
		var paramObj = {ITEM_CODE:item_code};
		paramObj = encodeObjectData(paramObj);
			if(isNull(isWhsTabOpen)){
					isWhsTabOpen = false;
			}
			if(isWhsTabOpen){
				if(!$rootScope.isShowDisableMenuItem('CON_WAREHOUSE_INFORMATION')){
					return;
				}
			}
			DataSharingService.setObject("whstabstatus",isWhsTabOpen);
			//var item_code = encodeURIComponent(productCode); 

			var needToLogin = $rootScope.isSignOnRequired("CON_PRODUCT_INFORMATION",PRODUCT_DETAIL,true,paramObj);
			if(!needToLogin){
				$state.go(PRODUCT_DETAIL,paramObj);
			}
		};
		
	$rootScope.showCatalogDetail = function(catalogCode,catalogDesc){
		var item_code = catalogCode; 
		var item_desc = decodeURI(catalogDesc);
		var paramObj = {ITEM_CODE:item_code,VIEW:item_desc};
		paramObj = encodeObjectData(paramObj);
		//	var needToLogin = $rootScope.isSignOnRequired("CON_PRODUCT_INFORMATION",PRODUCT_DETAIL,true,paramObj);
			//if(!needToLogin){
				$state.go(CATALOG_DETAIL,paramObj);
			//}
		};
		
	$rootScope.showCatalogFromProd = function(menu){
		if(isNull(menu.catalogueCode)){
			$rootScope.showCatalogDetail(menu.code,menu.description);
		}else{
			//$rootScope.showMainCatalog(menu);
			var item_code = menu.catalogueCode;
			var element_id = menu.elementCode;
			item_code = item_code.replace(/\//g,'%4F');
			element_id = element_id.replace(/\//g,'%4F');
			$state.go("home.catalogSub",{"ITEM_CODE":item_code,"ELEMENT_ID":element_id});
		}
	
	};
	
	$rootScope.showCatalogFromMainCat = function(menu){
		if(isNull(menu.catalogueCode)){
			$rootScope.showCatalogDetail(menu.code,menu.description);
		}else{
			var item_code = menu.catalogueCode;
			var element_id = menu.elementCode;
			item_code = item_code.replace(/\//g,'%4F');
			element_id = element_id.replace(/\//g,'%4F');
			$state.go("home.catalogSub",{"ITEM_CODE":item_code,"ELEMENT_ID":element_id});
		}
	
	};
	

	$rootScope.isLoggedIn = function(){
		return authentication.isLoggedIn();
	};
	$rootScope.getHomeCatalogColumnsToCheck = function(){
		var  columnsToCheck = ['actualPrice','discountPrice','discountPercentage','isBuyingAllowed','availabilityOfItem','enquiryImage'];
		return columnsToCheck;
	};
	
	$rootScope.initHomeCatalogTableHeaders = function(){
		$rootScope.tableHeaders = {};
	};
	
	$rootScope.getHomeCatalogTableHeaders = function(){
		return $rootScope.tableHeaders;
	};
	
	$rootScope.setHomeCatalogTableHeaders = function(list){
		var columnsToCheck = $rootScope.getHomeCatalogColumnsToCheck();
		var isBuyAllowed = null;
		if(Util.isNotNull(list)){
			for (var i=0;i<list.length ; i++){
				var  item  = list[i];
				setTableHeader(item,columnsToCheck,$rootScope.getHomeCatalogTableHeaders());
				if(isBuyAllowed==null && item.isBuyingAllowed){
					isBuyAllowed = true;
				}
			}
			//set table header for buy
			if (isBuyAllowed){
				$rootScope.getHomeCatalogTableHeaders().isBuyingAllowed=true;
			}else{
				$rootScope.getHomeCatalogTableHeaders().isBuyingAllowed=false;
			}
		}
		//console.log("Catalog Table headers : " +JSON.stringify($rootScope.getHomeCatalogTableHeaders()));
	};
	
	$rootScope.setHomeCatalogExtendedData = function(list){
		//$log.log("setHomeCatalogExtendedData : product list : " +JSON.stringify($scope.productList));
		if(Util.isNotNull(list)){
			for (var i=0;i<list.length ; i++){
				var  item  = list[i];
				item.showAvailabilityAsLink = $rootScope.showAvailabilityAsLink();
				item.showAvailabilityAsImage = $rootScope.showAvailabilityAsImage();
				item.availabilityImageName = $rootScope.getAvailabilityImage(item.availabilityOfItem);
				item.availabilityText = $rootScope.getAvailabilityText(item.availabilityOfItem);
				item.showEnquiryImage = $rootScope.showEnquiryImage(item.enquiryImage);
				item.enquiryImage = $rootScope.getEnquiryImageName(item.enquiryImage);
				handleEmptyAvailabilityImage(item);
				//append % if discountPercentage has value
				if(!isEmptyString(item.discountPercentage)){
					item.discountPercentageText = item.discountPercentage + " %";
				}
				if(!item.hasOwnProperty("salesUnit")){
					item.salesUnit=item.defaultSalesUnit;
				}
				if(!item.hasOwnProperty("salesUnitObject")){
					item.selectedUnitObject= {};
					item.selectedUnitObject = getSelectedSalesUnit(item.defaultSalesUnit,item.salesUnitsDesc);
				}
				
			}
			addpropertyToList(list,'validQuantity',true);
		}
	};
	// enabling sorting indicator 
	$scope.showSelection = function(code)
	{
		if(code === $rootScope.sortSelection ) 	{  	return true;}
		else
			{ 	return false; 	}
	};
	
	$scope.exportCatalogTableAsPDF=function(catName){
		var tablData = $scope.setCatalogDataforExport();
		var data = tablData.getData();
		var title = Util.translate('CON_CATALOGUE') + " : " + catName;
		var fileName = "Catalogue_"+getCurrentDateTimeStr()+".pdf";
		savePdfFromHtml(tablData.getHTML(),title,fileName);
		//console.log("exportOrderTableAsPDF -  table Object "+ JSON.stringify(data));
	};
	
	$scope.exportCatalogTableAsExcel=function(){
		var tablData = $scope.setCatalogDataforExport();
		var data = tablData.getDataWithHeaders();
		var sheetName = "Catalogue";
		var fileName = "Catalogue_"+getCurrentDateTimeStr()+".xlsx";
		//$rootScope.exportTableAsExcel(data,title,fileName);
		saveExcel(data,sheetName,fileName);
		//console.log("exportOrderTableAsExcel -  table Object "+ JSON.stringify(data));
	};
	
	$scope.exportCatalogTableAsXML=function(){
		var tablData = $scope.setCatalogDataforExport('XML');
		tablData.setRootElementName("CATALOGUE");
		tablData.setEntityName("CATALOGUE_ITEM");
		var data = tablData.getXML();
		var fileName = "Catalogue_"+getCurrentDateTimeStr()+".xml";
		saveXML(data,fileName);
		//console.log("exportOrderTableAsXML -  table Object "+ JSON.stringify(data));
	};
	
	$scope.setCatalogDataforExport = function(target){
		var tablData = new Table();
		if(isEqualStrings(target,"XML")){
			tablData.addHeader('PRODUCT');
			tablData.addHeader('DESCRIPTION');
			if($rootScope.webSettings.isShowPrice){
				tablData.addHeader('ACTUAL_PRICE');
				tablData.addHeader('UNIT');
				tablData.addHeader('DISCOUNT_PRICE');
				if($rootScope.webSettings.allowDiscountDisplay !== "*ALL_PRICE")
					tablData.addHeader('DISCOUNT_PERCENTAGE');
			}
		}else{
			tablData.addHeader(Util.translate('COH_ITEM'));
			tablData.addHeader(Util.translate('COH_DESCRIPTION'));
			if($rootScope.webSettings.isShowPrice){
				tablData.addHeader(Util.translate('COH_ACTUAL_PRICE'));
				tablData.addHeader(Util.translate('COH_UNIT'));
				tablData.addHeader(Util.translate('COH_DISCOUNT_PRICE'));
				if($rootScope.webSettings.allowDiscountDisplay !== "*ALL_PRICE")
					tablData.addHeader(Util.translate('CON_DISCOUNT_%'));
			}
		}
		
		//DataSharingService.setObject("CATALOG_PRODUCT_LIST",$scope.productList );	
		//set row data
		data = DataSharingService.getObject("CATALOG_PRODUCT_LIST");	
		if(!isNull(data)){
			for(var i=0;i<data.length;i++){
				var item = data[i];
				var row = [];
				row.push(item.code); 
				row.push(item.description);
				if($rootScope.webSettings.isShowPrice){
					row.push(item.actualPrice);
					row.push(item.salesUnit);
					row.push(item.discountPrice);
					if($rootScope.webSettings.allowDiscountDisplay !== "*ALL_PRICE")
						row.push(item.discountPercentageText);
				}
				tablData.addRow(row);
			}
		}else{
			var row = [];			
			for(var i=0;i<tablData.headers.length;i++){
				row.push("");
			}
			tablData.addRow(row);
		}

		//console.log("order export table  "+ JSON.stringify(tablData));
		return tablData;
	};
	
	$rootScope.getSortingParam = function(column,orderBy) {
		
		var curSel = column+orderBy+'';
		//console.log("Search sorting-- Previous Sorting Selection " + $rootScope.sortSelection );			
		$rootScope.sortSelection = curSel;		
		
		//console.log("Search sorting -- Current Sorting Selection : " + curSel);		
		
		var	requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,orderBy,column); 
		
		return 	requestParams;	

	};
	
	$rootScope.buyMultipleItems = function(item,buyObj){
		var needToLogin = $rootScope.isSignOnRequired("CON_ADD_PRODUCTS_TO_YOUR_CART",$state.current.name,true);
		 if(!needToLogin){
			 if(item === null || item === undefined){
//				 buyObj.prdlist.forEach(function(value){
//					 var qtn = value[buyObj.propNameOrdered];
//					 if(qtn == undefined || qtn == null || qtn == "" ){
//					 		value.validQuantity = true;
//				 			value['quantityBoxMsg'] = "";
//					 	}else if(!isValidQuantity(qtn,$rootScope.webSettings.maxQuantity, $rootScope.webSettings.allowDecimalsForQuantities) || !$rootScope.isValidDecimals(value, qtn)){
//					 		value.validQuantity = false;
//				 			value['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
//					 	}else{
//					 	}
//				 });
				 	//ADM-002 Begin
				 	var isInList = false;
					for (var i=0;i<buyObj.prdlist.length ; i++){
						var itemInList = buyObj.prdlist[i];
						 var code = itemInList.code;
						 if(isNull(code)){
							 code = itemInList.itemCode;
						 }
						 if($rootScope.isShopCartItem(code)){
							isInList = true;
							itemInList.validQuantity = false;
							itemInList['quantityBoxMsg'] = Util.translate('MSG_ITEM_ALREADY_ENTERED');
						}
					}
					if(!isInList){
					//ADM-002 end
			 		var param = buyObj.getParam(item, $rootScope.webSettings.allowDecimalsForQuantities,$rootScope.webSettings.maxQuantity);
			 		if(isNull(param) && !isNull(item)){
			 			value.validQuantity = false;
			 			value['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
			 		}
			 		$rootScope.AddMultipleItemsToCart(param,buyObj,null,item);
					/*ADM-002*/}
			 }else{
				 //ADM-002 Begin
				 var code = item.code;
				 if(isNull(code)){
					 code = item.itemCode;
				 }
				 if($rootScope.isShopCartItem(code)){
					 item.validQuantity = false;
					 item['quantityBoxMsg'] = Util.translate('MSG_ITEM_ALREADY_ENTERED');
				 }else{ /*ADM-002 end */
				 var qtn = item[buyObj.propNameOrdered];
				 if(!isValidQuantity(qtn,$rootScope.webSettings.maxQuantity, $rootScope.webSettings.allowDecimalsForQuantities) || !$rootScope.isValidDecimals(item, qtn)){
					 	item.validQuantity = false;
						item['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
				 }else{
					 var param = buyObj.getParam(item, $rootScope.webSettings.allowDecimalsForQuantities);
					 if(isNull(param) && !isNull(item)){
						 item.validQuantity = false;
						 item['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
					 }
					 $rootScope.AddMultipleItemsToCart(param,buyObj,null,item);
				 }
				 /*ADM-002*/}
			 }
			 //clearQuantityBox(buyObj,item);
		 }
	};
	
	$rootScope.buyMultipleItemsQuo = function(item,buyObj){
		var needToLogin = $rootScope.isSignOnRequired("CON_ADD_PRODUCTS_TO_YOUR_CART",$state.current.name,true);
		 if(!needToLogin){
				var param = buyObj.getParamQuo(item, $rootScope.webSettings.allowDecimalsForQuantities, webSettings.isUseQuotationPrice);
				if(isNull(param) && !isNull(item)){
					item.validQuantity = false;
					item['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
				}
				$rootScope.AddMultipleItemsToCart(param,buyObj,null,item);
		 }
	};
	
	$rootScope.AddMultipleItemsToCart = function(param,buyObj,lineList,item){
		if(!isNull(param) && !isEmptyString(param)){
			//console.log("going to buy items: "+param);
			Util.postDataToServer('addItemsToCartUrl',param).then(function(data){
				handleBuyMsgCode(data);
				if(!isNull(lineList)&& !isNull(data.addItemStatusList)){
					clearListLines(lineList,data.addItemStatusList);	
					$rootScope.$broadcast('itemsBuyAddItem');
					if($scope.successfullBuyCount==data.addItemStatusList.length){
						$rootScope.buyItemWatch = "Item Buyed : "+getCurrentDateTimeStr();
					}
				}
				$rootScope.populateBubbleCart(data.temporaryOrderData,true);
				clearQuantityBox(buyObj,item);
				/*ADM-002*/setBuyNotAllowed(data.addItemStatusList,buyObj);
				$rootScope.cartUpdatedWatch = "Cart updated : "+getCurrentDateTimeStr();
				$rootScope.feedbackPerDialog = Util.translate('DIALOG_ADD_PROD_TO_CART');
			});
		}
	};
	
	//ADM-002
	var setBuyNotAllowed = function (addItemStatusList, buyObj){
		for(var i=0; i < addItemStatusList.length ; i++){
			var status = addItemStatusList[i];
			if(!isNull(status.itemCode) && !isNull(status.messageCode)){
				var itemCode = status.itemCode;
				var messageCode = status.messageCode;
				switch (messageCode) {
				case 9001:
				//case 9003:
					if(!isNull(buyObj) && Array.isArray(buyObj.prdlist)) {
						for(var y=0; y< buyObj.prdlist.length ; y++){
							var item = buyObj.prdlist[y];
							if(!isNull(item.code) && item.code == itemCode){
								item[buyObj.propNameisBuyAllowed] = false;
							}
						}
					}
					break;
				default:
					break;
				}
			}
		}
	}
	
	var clearQuantityBox = function (buyObj,item){
		if(!isNull(item)){
			if(!isNull(item.validQuantity) && item.validQuantity){
				item[buyObj.propNameOrdered] = "";
			}
		}else if(!isNull(buyObj) && Array.isArray(buyObj.prdlist)) {
			for(var i=0; i < buyObj.prdlist.length ; i++){
				var item = buyObj.prdlist[i];
				if(!isNull(item.validQuantity) && item.validQuantity){
					item[buyObj.propNameOrdered] = "";
				}
			}
		}
	};

	var clearAddToListMultiQuantityBox = function (buyObj,items){
		if(!isNull(items)){
			if(items instanceof Array){
				for(var i=0; i < items.length ; i++){
					var item = items[i];
					if(!isNull(item.validQuantity) && item.validQuantity){
					item[buyObj.propNameOrdered] = "";
					}
				}
			}else{
				if(!isNull(items.validQuantity) && items.validQuantity){
				items[buyObj.propNameOrdered] = "";
				}
			}

		}else if(!isNull(buyObj) && Array.isArray(buyObj.prdlist)) {
			for(var i=0; i < buyObj.prdlist.length ; i++){
				var item = buyObj.prdlist[i];
				if(!isNull(item.validQuantity) && item.validQuantity){
				item[buyObj.propNameOrdered] = "";
				}
			}
		}
	};
	
	$rootScope.getBuyStatusMsg = function (msgCode,msgItem){
		var msg = null;
		
		var itemCodeStr = "";
		if(!isNull(msgItem) && !isNull(msgItem.itemCode)){
			itemCodeStr = msgItem.itemCode + " : ";
		}
		switch (msgCode){
		case '3014' :
			// on successfull addtion to cart do not show message
			break;
		case '3013' :
		case '3003' :
			//unable to add item to cart
			msg = itemCodeStr  +Util.translate('MSG_UNABLE_TO_ADD_ITEM_TO_CART');
			break;
		case '3101' :
			msg = itemCodeStr  +Util.translate('MSG_ITEM_IS_NOT_AVAILABLE');
			break;
		case '3102' :
			msg = itemCodeStr  +Util.translate('MSG_ITEM_QUANTITY_ZERO');
			break;
		case '3103' :
			msg = itemCodeStr  +Util.translate('MSG_NEGATIVE_QUANTITY');
			break;
		case '3104' :
			msg = itemCodeStr  +Util.translate('MSG_QUANTITY_EXCEEDS_LIMIT');
			break;
		case '3105' :
			msg = itemCodeStr  +Util.translate('MSG_INVALID_DATE');
			break;
		case '3106' :
			msg = itemCodeStr  +Util.translate('MSG_INVALID_DATE');
			break;
		case '3107' :
			msg = itemCodeStr  +Util.translate('MSG_NOT_WORK_DAY');
			break;
		case '3113' :
			msg = itemCodeStr  +Util.translate('MSG_DELIVERY_NOT_POSSIBLE');
			break;
		case '3114' :
			msg = itemCodeStr  +Util.translate('MEX_CUSTOMER_HAS_SALES_RESTRICTIONS');
			break;
		case '3109' :
			msg = itemCodeStr  +Util.translate('MSG_NO_ITEM_FOUND_FOR_CODE');
			break;
		case '3110' :
			msg = itemCodeStr  +Util.translate('MSG_ITEM_INVALID_UNIT');
			break;
		case '3111' :
			msg = itemCodeStr  +Util.translate('MSG_ITEM_NOT_ALLOWED_UNIT');
			break;
		case '3112' :
			msg = itemCodeStr  +Util.translate('MSG_ITEM_PRICE_NOT_AVAILABLE');
			break;
		case '3116' :
			msg = itemCodeStr  +Util.translate('MSG_ITEM_PRICE_NOT_AVAILABLE');
			break;
		case '3121' :
			msg = itemCodeStr +Util.translate('MSG_INVALID_QUANTITY');
			break;
		case '3122' :
			msg = itemCodeStr +Util.translate('MSG_DELIVERY_DATE_NOT_POSSIBLE');
			break;
		case '5001' :
			msg = itemCodeStr  +Util.translate('MSG_UNEXPECTED_BUY_ERROR');
			break;
		case '3117' :
			msg = itemCodeStr  +Util.translate('MSG_BUY_NOT_ALLOWED');
			break;
		case '3334' :
			msg = itemCodeStr  +Util.translate('MSG_SHIPMENT_LENGTH_EXCEEDS');
			break;
		case '3234' :
			msg = itemCodeStr  +Util.translate('MSG_SP_MISSING_FOR_SOURCING_INFO');
			break;
		//ADM-002
		case '9001' :
			msg = itemCodeStr + Util.translate('MSG_ITEM_IS_NOT_TEMPORARY_AVAILABLE');
			break;
		case '9003' :
			msg = itemCodeStr + Util.translate('MSG_ITEM_IS_NOT_COMPLETELY_AVAILABLE');
			break;
		
		}
		return msg;
	};
	
	var handleBuyMsgCode = function (data){
		var translatedMsgList = [];
		var msgStr = null;
		$scope.successfullBuyCount=0;
		if(!isNull(data) && Array.isArray(data.addItemStatusList) && data.addItemStatusList.length > 0){
			for(var i=0; i < data.addItemStatusList.length ; i++){
				var msgItem = data.addItemStatusList[i];
				var msgCode = msgItem.messageCode+"";
				msgStr = $rootScope.getBuyStatusMsg(msgCode,msgItem);
				if(isEmptyString(msgStr)){
					$scope.successfullBuyCount++ ;
				}
				if(!isNull(msgStr)){
					translatedMsgList.push(msgStr);
				}
			}
		}
		if(!isNull(data.messageCode)){
			var msgCode = data.messageCode+"";
			msgStr = $rootScope.getBuyStatusMsg(msgCode,msgItem);
			if(!isNull(msgStr)){
				translatedMsgList.push(msgStr);
			}
		}
		//openMsgDlg
		if(translatedMsgList.length > 0){
			var msg = Util.translate('CON_BUY',null) + " : " + Util.translate('COH_ORDER_STATUS',null);
			var dlg = $rootScope.openMsgDlg(msg,translatedMsgList);
			dlg.result.then(function () {
				//console.log('Delete order modal Ok clicked');
			}, function () {
				//console.log('Delete order modal Cancel clicked');
				$scope.$root.$eval();
			});
		}

		return translatedMsgList;
	};
	
   	var getAddItemStatusMsg = function (msgCode,msgItem){
		var msg = null;
		switch (msgCode){
		case '2258' :
			// on successfull addtion to cart do not show message
			msg="";
			break;
		case '2256' :
			msg = Util.translate('MSG_LINE_IN_ERROR_ADDING_ITEM_TO_LIST',[item.itemCode]);
			break;
		case '2267' :
			msg = Util.translate('MSG_NO_LINES_HAVE_ADDED_TO_LIST');
			break; 
		case '114' :
			msg = Util.translate('MSG_INVALID_ITEM');
			break; 
		}
		return msg;
	};
	
	$rootScope.handleAddItemMsgCode = function (data,copyOrder){
		var msgStr = null;
		var translatedMsgList = [];
		var anyItemAddedToList = false ;
		var countForClear=0;
		if(data.messageCode== 114){
			translatedMsgList.push(Util.translate('MSG_INVALID_ITEM'));
		}else{
			for(var i=0; i < data.addListBean.length ; i++){
				var msgItem = data.addListBean[i];			
				var msgCode = msgItem.messageCode+"";
				msgStr = getAddItemStatusMsg(msgCode,msgItem);
				if(isEmptyString(msgStr)){
					anyItemAddedToList = true;
					countForClear=countForClear + 1;
				}
				if(!isNull(msgStr) && msgStr=="2267"){
					anyItemAddedToList = false;
					translatedMsgList.push(msgStr);
				}
			}
			if(data.addListBean.length==countForClear){
				if(!isNull(copyOrder)){
					copyOrder.clearAll();	
				}
			}
		}

		if(translatedMsgList.length > 0){
			var dlg = $rootScope.openMsgDlg("",translatedMsgList);
		}
		return anyItemAddedToList;
	};
		
	$rootScope.getAndUpdateCatalogItem = function(itemCode,unitCode,oldCatalogItem){
		if(!isEmptyString(itemCode) && !isEmptyString(unitCode) ){
			var prams = ["ITEM_CODE",itemCode,"UNIT_CODE",unitCode];
			var deferred = $q.defer();
			Util.getDataFromServer('getSingleItemDetailUrl',prams,false).then(function(data){
				if(!isNull(data) && Array.isArray(data.productList) && data.productList.length > 0){
					newCatalogItem = data.productList[0];
					if(angular.isDefined(oldCatalogItem.checked) && oldCatalogItem.checked!=null){
						newCatalogItem['checked']=oldCatalogItem.checked;
					}
					copyObjectWithNull(newCatalogItem,oldCatalogItem);
					$rootScope.setHomeCatalogExtendedData([oldCatalogItem]);
					oldCatalogItem.selectedUnitObject = getSelectedSalesUnit(unitCode,oldCatalogItem.salesUnitsDesc);
					
					if(oldCatalogItem.selectedUnitObject !== null){
						oldCatalogItem.salesUnit=unitCode;
						oldCatalogItem.selectedUnit = oldCatalogItem.salesUnit;
					}else{
						oldCatalogItem.selectedUnitObject = oldCatalogItem.salesUnitsDesc[0];
						oldCatalogItem.salesUnit=oldCatalogItem.salesUnitsDesc[0].salesUnit;
						oldCatalogItem.selectedUnit = oldCatalogItem.salesUnit;
						var translatedMsgList = [];
						translatedMsgList.push(Util.translate('CON_NO_DATA_FOUND'));
						$rootScope.openMsgDlg("",translatedMsgList);
					}
				
				}
				deferred.resolve(data);	
			});
		}
	};
	
	
	$rootScope.onUnitChangeQuickView= function(itemCode,unitCode,oldCatalogItem,quickViewObject){
			
		$rootScope.getAndUpdateCatalogItemQuickView(itemCode,unitCode,oldCatalogItem,quickViewObject);
	};
	
	
	$rootScope.getAndUpdateCatalogItemQuickView = function(itemCode,unitCode,oldCatalogItem,quickViewObject){
		if(!isEmptyString(itemCode) && !isEmptyString(unitCode) ){
			var prams = ["ITEM_CODE",itemCode,"UNIT_CODE",unitCode];
		
			var deferred = $q.defer();
			Util.getDataFromServer('getSingleItemDetailUrl',prams,false).then(function(data){
				if(!isNull(data) && Array.isArray(data.productList) && data.productList.length > 0){
					newCatalogItem = data.productList[0];
					if(angular.isDefined(oldCatalogItem.checked) && oldCatalogItem.checked!=null){
						newCatalogItem['checked']=oldCatalogItem.checked;
					}
					
					//copyObjectWithNull(newCatalogItem,oldCatalogItem);
					copyConsideringSearch(oldCatalogItem, newCatalogItem);
					
					$rootScope.setHomeCatalogExtendedData([oldCatalogItem]);
					oldCatalogItem.salesUnit=unitCode;
					oldCatalogItem.validQuantityPriceCalc = true;
					oldCatalogItem.selectedUnit = oldCatalogItem.salesUnit;
					oldCatalogItem.selectedUnitObject = getSelectedSalesUnit(unitCode,oldCatalogItem.salesUnitsDesc);
				}
				deferred.resolve(data);	
			//	quickViewObject.setResponseUnitChange(data);
				
			});
		}
	};
	
	
	
	//updateControl

	$scope.updateControl = function(item)
	{
		var showTextBox = false;
		if((item == '*BETWEEN') || (item == '*NOT_BETWEEN') )
			{
			
				showTextBox = true;
			}
		else
			{
				
				showTextBox = false;
			}
		
		return showTextBox;
	};
	$rootScope.addItemToCart = function (itemCode,quantity,salesUnit,itemObj,quantityPropName){
		var needToLogin = $rootScope.isSignOnRequired("CON_ADD_PRODUCTS_TO_YOUR_CART",$state.current.name,true);
		if(!needToLogin){
			var fltQuantity = quantity;
			if($rootScope.webSettings.allowDecimalsForQuantities == true){
			   fltQuantity = fltQuantity.replace(",",".");
			}
			if(!itemObj.validQuantity || isNull(quantity) || ""==quantity){
				itemObj.validQuantity = false;
				itemObj['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
			//ADM-002 Begin
			} else if($rootScope.isShopCartItem(itemObj.code)){
					 itemObj.validQuantity = false;
					 itemObj['quantityBoxMsg'] = Util.translate('MSG_ITEM_ALREADY_ENTERED');
			//ADM-002 end
			} else if(Util.isNotNull(itemCode) && Util.isNotNull(fltQuantity) && fltQuantity >0 && Util.isNotNull(salesUnit)) {
				var params =  ["itemCode",itemCode,"itemQty",fltQuantity,"unitCode",salesUnit];
				ShoppingCartService.addToCart(params).then(function(data){				
					// ADM-015 Start
					//handleBuyMsgCode(data);
					//clear quantity text box
					//clearSingleQuantityTextBox(itemObj,quantityPropName);
					//$rootScope.cartUpdatedWatch = "Cart updated : "+getCurrentDateTimeStr();
					
					// Create object to store data for the not available dialog
					// This is necessary because the item objects passed from eg grid view and
					// detail view are not the same ! 
					var selectedUnit = itemObj.selectedUnit;
					 if(isNull(selectedUnit)){
						 selectedUnit = itemObj.salesUnit;
					 }
					var notAvailableObj = {
											messageText: "",
											itemWebText: itemObj.itemWebText, 
											imageURL: itemObj.imageUrl, 
											description: itemObj.description,
											minsanCode: itemObj.minsanCode,
											selectedUnit: selectedUnit,
											actualPrice: itemObj.actualPrice,
											discountPercentage: itemObj.discountPercentage,
											discountPrice: itemObj.discountPrice,
											availableQuantity: data.availableQty,					
											quantity:itemObj.quantity,
											salesPackageQty: data.salesPackageQty,
											unitCode: data.unitCode};
					
					var isBuyAllowed = $rootScope.checkIfAvailable(data, notAvailableObj);
					clearSingleQuantityTextBox(itemObj,quantityPropName,isBuyAllowed);
					$rootScope.cartUpdatedWatch = "Cart updated : "+getCurrentDateTimeStr();
					// ADM-015 End
				});
			}
		}
		
	};
	
	
	$rootScope.addItemToCartPrdDetail = function (itemCode,quantity,salesUnit,itemObj,itemObjPrdInfo,quantityPropName){
		var needToLogin = $rootScope.isSignOnRequired("CON_ADD_PRODUCTS_TO_YOUR_CART",$state.current.name,true);
		if(!needToLogin){
			var fltQuantity = quantity;
			if($rootScope.webSettings.allowDecimalsForQuantities == true){
			   fltQuantity = fltQuantity.replace(",",".");
			}
			if(!itemObj.validQuantity || isNull(quantity) || ""==quantity){
				itemObj.validQuantity = false;
				itemObj['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
			//ADM-002 Begin
			} else if($rootScope.isShopCartItem(itemCode)){
					 itemObj.validQuantity = false;
					 itemObj['quantityBoxMsg'] = Util.translate('MSG_ITEM_ALREADY_ENTERED');
			//ADM-002 End
			} else if(Util.isNotNull(itemCode) && Util.isNotNull(fltQuantity) && fltQuantity >0 && Util.isNotNull(salesUnit)) {
				var params =  ["itemCode",itemCode,"itemQty",fltQuantity,"unitCode",salesUnit];
				ShoppingCartService.addToCart(params).then(function(data){				
					//ADM-015 Start
					//handleBuyMsgCode(data);
					//clear quantity text box
					//clearSingleQuantityTextBoxPrdInfo(itemObj,itemObjPrdInfo,quantityPropName);
					
					// Fetch image list (since this is not stored in current item object.
					// The first entry will be seen as the "default" image
					var imageList =  getItemResourceList(itemObj.prdBaseDetail.resourceBeanList,"IMAGE");
					
					// Create object to store data for the not available dialog
					// This is necessary because the item objects passed from eg grid view and
					// detail view are not the same ! 
					var selectedUnit = itemObj.selectedUnit;
					 if(isNull(selectedUnit)){
						 selectedUnit = itemObj.selectedUnit;
					 }
					var notAvailableObj = {
											messageText: "",
											itemWebText: itemObj.prdBaseDetail.itemWebText, 
											imageURL: imageList[0], 
											description: itemObj.prdBaseDetail.itemDesc,
											minsanCode: itemObj.prdBaseDetail.minsanCode,
											selectedUnit: selectedUnit,
											actualPrice: itemObj.prdBaseDetail.actualPrice,
											discountPercentage: itemObj.prdBaseDetail.discountPercentage,
											discountPrice: itemObj.prdBaseDetail.discountPrice,
											availableQuantity: data.availableQty,
											quantity:itemObj.quantity,
											salesPackageQty: data.salesPackageQty,
											unitCode: data.unitCode};
					
					var isBuyAllowed = $rootScope.checkIfAvailable(data, notAvailableObj);
					clearSingleQuantityTextBoxPrdInfo(itemObj,itemObjPrdInfo,quantityPropName,isBuyAllowed);
					// ADM-015 End
				});
			}
		}
		
	};
	
	var clearSingleQuantityTextBoxPrdInfo = function (itemObj,itemObjPrdInfo,quantityPropName/*ADM-015*/,isBuyAllowed){
		if(!isNull(itemObj) ){
			if(!isNull([quantityPropName]) && !isNull(itemObj.validQuantity) && itemObj.validQuantity){
				itemObj[quantityPropName]="";
				//ADM-015 Start
				//itemObjPrdInfo['isBuyAllowed']=true;
				itemObjPrdInfo['isBuyAllowed']=isBuyAllowed;
				//ADM-015 End
			}
		}
	};
	
	var clearSingleQuantityTextBox = function (itemObj,quantityPropName/*ADM-015*/,isBuyAllowed){
		if(!isNull(itemObj) ){
			if(!isNull([quantityPropName]) && !isNull(itemObj.validQuantity) && itemObj.validQuantity){
				itemObj[quantityPropName]="";
				//ADM-015 Start
				//itemObj['isBuyingAllowed']=true;
				itemObj['isBuyingAllowed']=isBuyAllowed;
				//ADM-015 End
			}
		}
	};

	var clearAddToListQuantityTextBox = function (itemObj,quantityPropName){
		for(var i=0; i < itemObj.length ; i++){
			var item = itemObj[i];
			if(!isNull(item.quantity)){
				item['quantity']="";	
			}
			if(!isNull(item.validQuantity) && item.validQuantity){
			item[quantityPropName] = "";
			}
		}
	};
	
	var clearAddToListQuantityTextBoxQuot = function (itemObj,quantityPropName){
		for(var i=0; i < itemObj.length ; i++){
			var item = itemObj[i];
			if(!isNull(item.quantity1)){
				item['quantity1']="";	
			}
			if(!isNull(item.validQuantity) && item.validQuantity){
			item[quantityPropName] = "";
			}
		}
	};
	
	$(document).ready(function() {
		$(this).click(function(event) {
			var ele = event.target;
			var divId = $( ele).closest("div").attr("id");
			if(divId == 'cartPopUp'){ 
				$scope.showPopUp = true;
				$timeout(function(){$scope.showPopUp = false}, 5000);
			}else{
				$scope.showPopUp = false;
			}
		});
	});
	/*  to validate the product code*/
			
	// get Product Code details
	$rootScope.fetchItemDetails = function(requestParams,line,lineObject){		

		Util.getItemCodeDetails(requestParams).then(function(data){		
			$rootScope.ItemCodeInfo  = data; 
			//$rootScope.itemInfoList = Util.getObjPropList(data);
			//ADM-010 Begin
			//validateLineItemCode(data,line,lineObject,Util.translate) ;
			if(!isNull($rootScope.webSettings) && $rootScope.webSettings.allowSelectUnitOfMeasure){
				validateLineItemCode(data,line,lineObject,Util.translate, $scope.freeSearchObj.packPiece) ;
			}else{
				validateLineItemCode(data,line,lineObject,Util.translate, null) ;
			}
			//ADM-010 End

		});	
		
	};
	
	$rootScope.fetchProductsDetails = function(requestParams,lineListObject,page){	
		if(!isEmptyString(page)){
			Util.validateItemsDetails(requestParams).then(function(data){			
				//console.log("post data "+data);
//				$rootScope.productsInfo  = data;		
				validateAllProductsDetails(data,lineListObject,Util.translate);	   
				$rootScope.AddProductsFromList(lineListObject,page);			   
			});	
		}else{
			$rootScope.AddProductsFromList(lineListObject,page);		
		}		
	};
	
	$rootScope.AddProductsFromList = function(lineListObject,page)
	{
		switch(page)
		{
		case 'CART':
			$rootScope.AddMultipleItemsToCart(lineListObject.getOrderParam($rootScope.webSettings.allowDecimalsForQuantities),null,lineListObject.newPrdList);
			break;
		case '':
			$rootScope.AddMultipleItemsToCart(lineListObject.getOrderParam($rootScope.webSettings.allowDecimalsForQuantities),null,lineListObject.newPrdList);
			break;
		case 'REQUEST':
			$rootScope.showNewRequestLine(lineListObject.getRequestLines());
			break;
		default:	
			$rootScope.AddMultipleItemsToCart(lineListObject.getOrderParam($rootScope.webSettings.allowDecimalsForQuantities));
		}

	};
	
	$rootScope.getCurrencyList = function(){
		var list = [];
		var filterDetails = DataSharingService.getFilterDetailsSettings();
		//currencyList
		if(!isNull(filterDetails) && !isNull(filterDetails['currencyList'])){
			list = filterDetails['currencyList'];
		}
		return list;
	};
	
	$rootScope.getPriceCalculations = function(params,priceCalcObject){
		var deferred = $q.defer();
		Util.postDataToServer('priceCalculationUrl',params).then(function(data){
			deferred.resolve(data);	
			priceCalcObject.setResponse(data);
		});
		return deferred.promise;
	};
	

	
//	$rootScope.getQuickViewChanges = function(params,priceCalcObject){
//		var deferred = $q.defer();
//		Util.postDataToServer('priceCalculationUrl',params).then(function(data){
//			deferred.resolve(data);	
//			priceCalcObject.setResponse(data);
//		});
//		return deferred.promise;
//	};
	
	$rootScope.showPriceCalcDlg = function(item){
		var tempIsValidQuantity = item.validQuantity;
		if(!$rootScope.isPriceCalculationEnabled) return;
		var needToLogin = $rootScope.isSignOnRequired("CON_PRICE_CALCULATION",$state.current.name);
		if(!needToLogin){
				$rootScope.priceCalDate = new Date();
				$rootScope.priceCalDate = $filter('date')($rootScope.priceCalDate, $rootScope.webSettings.defaultDateFormat);
				var priceCalcObj = new PriceCalculation (item,$rootScope);
				if(item.componentCode) priceCalcObj.itemCodePropName = "componentCode";
				if(item.unitCode) priceCalcObj.unitPropName = "unitCode";
				if(item.unitCodesDesc) priceCalcObj.salesUnitsPropName = "unitCodesDesc";
				priceCalcObj.init();
				item.validQuantity = tempIsValidQuantity;
				$rootScope.validateDateRangeFormatDialog("",true);
				$rootScope.openPriceCalcDlg("Price cal", priceCalcObj);
		}
	};
	
//quickView open start
	
	$rootScope.showQuickViewDlg = function(item){
		var needToLogin = $rootScope.isSignOnRequired("CON_QUICKINFO",$state.current.name);
		if(!needToLogin){
				var tempIsValidQuantity = item.validQuantity;
				var priceCalcObj1 = new QuickViewProduct (item,$rootScope);
				priceCalcObj1.init();
				item.validQuantity = tempIsValidQuantity;
				$rootScope.openQuickViewDlg("QuickView cal", priceCalcObj1);
		}
	};
	
	
	//quick view open end
	
//quickView matrix open start
	
	$rootScope.showQuickViewMatrixDlg = function(item){
		////console.log(item);
		var tempItem = {};
		tempItem.$$hashKey = item.$$hashKey;
		tempItem.selectedUnit = item.matrixUnitCode;
		tempItem.selectedCurrency = webSettings.currencyCode;
		tempItem.code = item.itemMatrixCode;
		tempItem.description = item.verticalTemplateName+" "+item.columnTemplateName;
		tempItem.availabilityImageName = item.availabilityImageName;
		tempItem.availabilityOfItem = item.availabilityMatrixItem;
		tempItem.availabilityText = item.availabilityText;
		tempItem.defaultSalesUnit = item.matrixUnitCode;
		tempItem.defaultSalesUnitDesc = item.matrixUnitCode;
		tempItem.enquiryImage = item.enquiryImage;
		tempItem.actualPrice = item.matrixItemPrice;
		tempItem.discountPrice = item.matrixItemPrice;
		tempItem.isBuyingAllowed = item.isMatrixBuyAllowed;
		tempItem.showAvailabilityAsImage = item.showAvailabilityAsImage;
		tempItem.showAvailabilityAsLink = item.showAvailabilityAsLink;
		tempItem.validQuantity = item.validQuantity;
		tempItem.isRelative = true;
		var needToLogin = $rootScope.isSignOnRequired("CON_QUICKINFO",$state.current.name);
		if(!needToLogin){
			var tempIsValidQuantity = item.validQuantity;
				var priceCalcObj1 = new QuickViewProduct (tempItem,$rootScope);
				priceCalcObj1.init();
				item.validQuantity = tempIsValidQuantity;
				$rootScope.openQuickViewMatrixDlg("QuickView cal", priceCalcObj1);
		}
	};
	
	
	//quick view matrix open end
	
	//Quick Info MyTopItem Starts
	
	
	$rootScope.showQuickViewMyTopItemDlg = function(item){
		var tempItem = {};
		tempItem.$$hashKey = item.$$hashKey;
		tempItem.selectedUnit = item.selectedUnitObject.salesUnit;
		tempItem.selectedCurrency = webSettings.currencyCode;
		tempItem.code = item.item_code;
		tempItem.description = item.description;
		tempItem.availabilityImageName = item.availabilityImageName;
		tempItem.availabilityOfItem = item.availabilityOfItem;
		tempItem.availabilityText = item.availabilityText;
		tempItem.defaultSalesUnit = item.unit_code;
		tempItem.defaultSalesUnitDesc = item.unitCodeDesc;
		tempItem.enquiryImage = item.enquiryOnMail;
		tempItem.actualPrice = item.actualPrice;
		tempItem.discountPrice = item.discountPrice;
		tempItem.isBuyingAllowed = item.isBuyAllowed;
		tempItem.showAvailabilityAsImage = item.showAvailabilityAsImage;
		tempItem.showAvailabilityAsLink = item.showAvailabilityAsLink;
		tempItem.validQuantity = item.validQuantity;
		tempItem.isRelative = true;
		var needToLogin = $rootScope.isSignOnRequired("CON_QUICKINFO",$state.current.name);
		if(!needToLogin){
			var tempIsValidQuantity = item.validQuantity;
				var priceCalcObj1 = new QuickViewProduct (tempItem,$rootScope);
				priceCalcObj1.init();
				item.validQuantity = tempIsValidQuantity;
				$rootScope.openQuickViewMatrixDlg("QuickView cal", priceCalcObj1);
		}
	};
	
	//Quick Info MyTopItem Ends
	
	$rootScope.showPriceCalcDlgFromPrdSearch = function(item){
		if(!$rootScope.isPriceCalculationEnabled) return;
		var needToLogin = $rootScope.isSignOnRequired("CON_PRICE_CALCULATION",$state.current.name);
		if(!needToLogin){
				$rootScope.priceCalDate = new Date();
				$rootScope.priceCalDate = $filter('date')($rootScope.priceCalDate, $rootScope.webSettings.defaultDateFormat);
				var priceCalcObj = new PriceCalculation (item,$rootScope);
				
				priceCalcObj.itemCodePropName =  "solrItemCode";
				priceCalcObj.descCodePropName =  "itemDescription";
				priceCalcObj.unitPropName =  "itemUnitCode";
				priceCalcObj.pricePropName =  "actualPrice";
				priceCalcObj.salesUnitsPropName =  "salesUnitsDesc";
				priceCalcObj.buyAllowedPropName =  "isBuyingAllowed";
				priceCalcObj.init();
				$rootScope.validateDateRangeFormatDialog("",true);
				$rootScope.openPriceCalcDlg("Price cal", priceCalcObj);
		}
	};
	
	$rootScope.showPriceCalcDlgFromListEntry = function(item){
		if(!$rootScope.isPriceCalculationEnabled) return;
		var needToLogin = $rootScope.isSignOnRequired("CON_PRICE_CALCULATION",$state.current.name);
		if(!needToLogin){
				$rootScope.priceCalDate = new Date();
				$rootScope.priceCalDate = $filter('date')($rootScope.priceCalDate, $rootScope.webSettings.defaultDateFormat);

				var priceCalcObj = new PriceCalculation (item,$rootScope);				
				priceCalcObj.itemCodePropName =  "code";
				priceCalcObj.descCodePropName =  "description";
				priceCalcObj.unitPropName =  "defaultSalesUnit";
				priceCalcObj.pricePropName =  "actualPrice";
				priceCalcObj.salesUnitsPropName =  "salesUnitsDesc";
				priceCalcObj.buyAllowedPropName =  "isBuyingAllowed";
				priceCalcObj.init();		
				$rootScope.validateDateRangeFormatDialog("",true);
				$rootScope.openPriceCalcDlg("Price cal", priceCalcObj);
		}
	};
	
	$rootScope.showPriceCalcDlgFromOrderHistory = function(item){
		if(!$rootScope.isPriceCalculationEnabled) return;
		var needToLogin = $rootScope.isSignOnRequired("CON_PRICE_CALCULATION",$state.current.name);
		if(!needToLogin){
				$rootScope.priceCalDate = new Date();
				$rootScope.priceCalDate = $filter('date')($rootScope.priceCalDate, $rootScope.webSettings.defaultDateFormat);

				var priceCalcObj = new PriceCalculation (item,$rootScope);				
				priceCalcObj.itemCodePropName =  "itemCode";
				priceCalcObj.descCodePropName =  "itemDescription";
				priceCalcObj.unitPropName =  "unit";
				priceCalcObj.pricePropName =  "actualPrice";
				priceCalcObj.salesUnitsPropName =  "itemUnitsDesc";
				priceCalcObj.buyAllowedPropName =  "isBuyAllowed";
				priceCalcObj.init();		
				$rootScope.validateDateRangeFormatDialog("",true);
				$rootScope.openPriceCalcDlg("Price cal", priceCalcObj);
		}
	};

	
	$rootScope.isSignOnRequired = function(menuName, callState, reload,paramObj)
	{
		var needToLogin = false;
		var signonRequired = $rootScope.getSignInRequirement(menuName);
		var loginStatus = authentication.isLoggedIn();

		if(loginStatus == false){
			if(signonRequired == true){
				needToLogin=true;
				$rootScope.open(callState,reload,paramObj);
			}else{
				needToLogin=false;
			}
		}else{
			needToLogin=false;
		}
		return needToLogin;
	};
	
	$rootScope.isValidDecimals = function(itemObj, itemQty) {
		var flag = true;
		if(!isNull($rootScope.webSettings) && !isNull($rootScope.webSettings.allowDecimalsForQuantities) && $rootScope.webSettings.allowDecimalsForQuantities) {
			var reqNumDecimalPlaces = null;
			
			var suosu;
			if(!isNull(itemObj.selectedUnitObject) && !isNull(itemObj.selectedUnitObject.salesUnit)) {
				suosu = itemObj.selectedUnitObject.salesUnit;
			} else if(!isNull(itemObj.unit)) {
				suosu = itemObj.unit;
			} else if(!isNull(itemObj.unit_code)) {
				suosu = itemObj.unit_code;
			} else if(!isNull(itemObj.ivOrderLine) && !isNull(itemObj.ivOrderLine.unitCode)) {
				suosu = itemObj.ivOrderLine.unitCode;
			}
			
			
			var su;
			if(!isNull(itemObj.salesUnits)) {
				su = itemObj.salesUnits;
			} else if(!isNull(itemObj.itemUnits)) {
				su = itemObj.itemUnits;
			} else if(!isNull(itemObj.unitCodes)) {
				su = itemObj.unitCodes;
			} else if(!isNull(itemObj.unitList)) {
				su = itemObj.unitList;
			} else if(!isNull(itemObj.activeUnitList)) {
				su = itemObj.activeUnitList;
			} else if(!isNull(itemObj.prdBaseDetail) && !isNull(itemObj.prdBaseDetail.unitCodes)) {
				su = itemObj.prdBaseDetail.unitCodes;
			} else if(!isNull(itemObj.ivOrderLine) && !isNull(itemObj.ivOrderLine.item) && !isNull(itemObj.ivOrderLine.item.salesUnits)) {
				su = itemObj.ivOrderLine.item.salesUnits;
			}
			
			var inoda;
			if(!isNull(itemObj.itemNumberOfDecimalsAllowed)) {
				inoda = itemObj.itemNumberOfDecimalsAllowed;
			} else if(!isNull(itemObj.prdBaseDetail) && !isNull(itemObj.prdBaseDetail.itemNumberOfDecimalsAllowed)) {
				inoda = itemObj.prdBaseDetail.itemNumberOfDecimalsAllowed;
			} else if(!isNull(itemObj.ivOrderLine) && !isNull(itemObj.ivOrderLine.item) && !isNull(itemObj.ivOrderLine.item.itemNumberOfDecimalsAllowed)) {
				inoda = itemObj.ivOrderLine.item.itemNumberOfDecimalsAllowed;
			}
			if(!isNull(itemObj) && !isNull(suosu) && !isNull(su) && !isNull(inoda) && (su.length == inoda.length)) {
				// test code below
				//itemObj.itemNumberOfDecimalsAllowed = [2];
				// test code above
				for(var i = 0; i < su.length; i++) {
					if(suosu == su[i]) {
						reqNumDecimalPlaces = inoda[i];
						break;
					}
				}
			}
			var temp = (typeof itemQty == "string") ? itemQty : (itemQty + ""),temp2;
			if(!isNull(reqNumDecimalPlaces) && !isNaN(reqNumDecimalPlaces)) {
				if(temp.indexOf(',') > 0){
					temp2 = temp.split(',');
				}else{
					temp2 = temp.split(".");
				}
				if(temp2.length == 2) {
					var temp3 = temp2[1];
					if(temp3.length > reqNumDecimalPlaces || temp3.length <= 0) {
						flag = false;
					}
				}else if(temp2.length >2){flag = false;}
			}else{
				if((temp.indexOf(',') > 0) || (temp.indexOf('.') > 0)){
					if($rootScope.webSettings.allowDecimalsForQuantities){
						flag = true;
					}else{
						flag = false;
					}
				}
			}
		}else{
			var temp = (typeof itemQty == "string") ? itemQty : (itemQty + ""),temp2;
			if((temp.indexOf(',') > 0) || (temp.indexOf('.') > 0)){
				flag = false;
			}else{
				flag = true;
			}
		}
		
		return flag;
		
		
	};
	
	$rootScope.checkValidQuantity = function(itemObj,itemQty){
		if(itemQty === ""){
			itemObj['quantityBoxMsg'] = Util.translate('MSG_INVALID_QUANTITY');			
			itemObj.validQuantity= false;
		}else if(!isValidQuantity(itemQty,$rootScope.webSettings.maxQuantity, $rootScope.webSettings.allowDecimalsForQuantities) || !$rootScope.isValidDecimals(itemObj, itemQty)){
					itemObj['quantityBoxMsg'] = Util.translate('MSG_INVALID_QUANTITY');			
					itemObj.validQuantity= false;				
		}else{
		    itemObj['quantityBoxMsg'] = "";
		    itemObj.validQuantity = true;
		}	
	};
	
	$rootScope.checkValidUnit = function(itemObj, itemUnt) {
		if(isNull(itemUnt) || isEmptyString(itemUnt)) {
			itemObj.invalidUnit = true;
		} else {
			itemObj.invalidUnit = false;
		}
	};
	
	
	$rootScope.checkValidQuantityQuickInfo = function(itemObj,itemQty){
		
		if(!isValidQuantity(itemQty,$rootScope.webSettings.maxQuantity, $rootScope.webSettings.allowDecimalsForQuantities) || !$rootScope.isValidDecimals(itemObj, itemQty)){
					itemObj['quantityBoxMsg'] = Util.translate('MSG_INVALID_QUANTITY');			
				//	itemObj.validQuantity= false;	
					itemObj.validQuantityPriceCalc=false;
					
		}else{
		    itemObj['quantityBoxMsg'] = "";
		   // itemObj.validQuantity = true;
		    itemObj.validQuantityPriceCalc = true;
		   
		}	
	};
	
	$rootScope.checkValidQuantityPriceCal = function(itemObj,itemQty){
		if(!isValidQuantity(itemQty,$rootScope.webSettings.maxQuantity, $rootScope.webSettings.allowDecimalsForQuantities) || !$rootScope.isValidDecimals(itemObj, itemQty)){
					itemObj['quantityBoxMsg'] = Util.translate('MSG_INVALID_QUANTITY');			
					itemObj.validQuantityPriceCalc= false;				
		}else{
		    itemObj['quantityBoxMsg'] = "";
		    itemObj.validQuantityPriceCalc = true;
		}	
	};
	
	$rootScope.checkNshowNewAccountPopup = function() {
		var menuItem = $rootScope.getMenuItemByName('CON_NEW_ACCOUNT');
		if(!isNull(menuItem)){
			$rootScope.isLoginRequired(menuItem.signonRequired,menuItem);
		}
	};
	
	
	$rootScope.showNewAccountPopup = function(){
		
		var newAccObj = new NewAccount($rootScope.webSettings.newCustomerPolicy, $rootScope.webSettings.newWebUserPolicy,
				$rootScope.webSettings.newCustomerTypePolicy);
		ShoppingCartService.getAllCountryData([]).then(function(data){
			if(!isNull(data)){
				angular.copy(data,newAccObj.countryList);
				if(newAccObj.countryList.length>0){
					//country = newAccObj.countryList[0];
					//newAccObj.selectedCountry = country;
					//$rootScope.getStateList(country.code,newAccObj.stateList);
				}
			}
		});
		$rootScope.getLocaleList(newAccObj.localeList);
		$rootScope.openNewAccountDlg ("NeWAccount", newAccObj);
	};
	
	$rootScope.getStateList = function(countryCode,targetlist){
		AccountService.getStateList(countryCode).then(function(data){
			if(!isNull(data)){
				angular.copy(data,targetlist);
			}
		});
	};
	
	$rootScope.getCountyList = function(countryCode,stateCode,targetlist){
		AccountService.getCountyList(countryCode,stateCode).then(function(data){
			if(!isNull(data)){
				angular.copy(data,targetlist);
				
			}
		});
	};
	
	$rootScope.currentLocaleList = [];
	$rootScope.getLocaleList = function(targetlist){
		AccountService.getLocaleList().then(function(data){
			if(!isNull(data)){
				angular.copy(data,targetlist);
				$rootScope.currentLocaleList = targetlist;
				DataSharingService.setToSessionStorage("currentLocaleList",$rootScope.currentLocaleList);
			}
		});
	};
	
	
	$rootScope.createNewAccount = function(accObj){
		if(!isNull(accObj)){
			var reqParam = accObj.getRequestParam();
			if(!isNull(reqParam)){
				AccountService.createNewAccount(reqParam).then(function(data){
					if(Array.isArray(data) && data.length > 0){
						accObj.setResponse(data[0]);
					}
					if(Array.isArray(data) && data.length>0){
						var messageCodeNewAcc = data[0].messageCode;
						if(messageCodeNewAcc == "4500"){
							var msgTxtnewAcc = Util.translate('MSG_NEW_ACCOUNT_CREATED');
							var dlg = $rootScope.openMsgDlg(msgTxtnewAcc);
							dlg.result.then(function () {
								
							}, function () {
							});
						}
					}
				});
			}
		}
	};
	
	$rootScope.changePassword = function(changePwdObj){
		if(!isNull(changePwdObj)){
			var reqParam = changePwdObj.getRequestParamForPost();
			if(!isNull(reqParam)){
				authentication.changePassword(reqParam).then(function(data){
					if(!isNull(data)){
						changePwdObj.setResponse(data);
						if(	changePwdObj.isPwdChangedSuccessfully){
							//auto login
							authentication.getUserData(changePwdObj.userId,changePwdObj.newPwd,changePwdObj.languageCode).then(function(data){
								var loggedIn = authentication.isUserAuthenticated(data);
								if(loggedIn){
									if(!isNull($rootScope.closeChangePwdDlgOnsuccess)){
										$rootScope.closeChangePwdDlgOnsuccess();
									}
								 if(!isNull(changePwdObj.languageCode) && changePwdObj.isPwdChangedSuccessfully){
										var errorMsg = Util.translate('MSG_LOGIN_NEW_CREDENTIALS');
										var dlgClose = $rootScope.openMsgDlg(Util.translate('CON_PSW_CHANGE_SUCCESSFUL'),[errorMsg]);
										dlgClose.result.then(function () {
											//if user clicks on ok
										},function(){
											//redirecting to LogOut.
											$rootScope.loginLogout();
										});
									}else if(!isNull(changePwdObj.callState)){
										if(changePwdObj.callState =='home' || changePwdObj.callState =='GENERAL_ENQUIRY'){
											$rootScope.reloadHomePage();
										}else{
										$state.go(changePwdObj.callState);
										}
									}
									else{
									$rootScope.reloadHomePage();
								}
								}
							});
						}
					}
				});
			}
		}
	};
	
	$rootScope.changePasswordClick = function(){
		var needToLogin = $rootScope.isSignOnRequired("CON_CHANGE_PASSWORD",$state.current.name);
		if(!needToLogin){
			$rootScope.showChangePwdDlg(null,false);
		}
	};
	
	$rootScope.addItemToCurrentList = function(item,quantityPropName){
		if(isNull(item.quantity) || isEmptyString(item.quantity)){
			item['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');		
			item.validQuantity= false;				
		}
		var fltQuantity = item.quantity;
		if($rootScope.webSettings.allowDecimalsForQuantities == true){
		   fltQuantity = fltQuantity.replace(",",".");
		}
		if(Util.isNotNull(item.code) && Util.isNotNull(item.salesUnit) && fltQuantity >0 && Util.isNotNull(item.quantity) && !isNull(item.validQuantity) && item.validQuantity) {
			var items=[item];
			if(isNull(quantityPropName)){
				quantityPropName="quantity";
			}
			if(!isEmptyString($rootScope.currentList)){
				$rootScope.addItemsToShoppingList(items,null,quantityPropName);	
			}else {
				$scope.ShoppingList.UIMessageKey="";
				$scope.ShoppingList.ErrOnControl.ListID=false;
				$scope.ShoppingList.setNewList();
				$scope.modalInstance = $rootScope.openCreateDefaultListDlg("" , $scope.ShoppingList,items,null,null,quantityPropName);	
			}	
		}
	};
	
	
	//Add to List from Quotation
	
	$rootScope.addItemToCurrentListQuotation = function(item,quantityPropName){
		if(isNull(item.quantity1) || isEmptyString(item.quantity1)){
			item['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');		
			item.validQuantity= false;				
		}
		if(Util.isNotNull(item.itemCode) && Util.isNotNull(item.unit) && item.quantity1 >0 && Util.isNotNull(item.quantity1) && !isNull(item.validQuantity) && item.validQuantity) {
			var items=[item];
			if(isNull(quantityPropName)){
				quantityPropName="quantity1";
			}
			if(!isEmptyString($rootScope.currentList)){
				$rootScope.addItemsToShoppingListQuotation(items,null,quantityPropName);	
			}else {
				$scope.ShoppingList.UIMessageKey="";
				var fromQuotation = true;
				$scope.ShoppingList.ErrOnControl.ListID=false;
				$scope.ShoppingList.setNewList();
				$scope.modalInstance = $rootScope.openCreateDefaultListDlg("" , $scope.ShoppingList,items,null,null,quantityPropName,fromQuotation);	
			}	
		}
	};
	

	$rootScope.addMultipleItemsToList = function(item,buyObj){
		var checkValidQuantity=false;
		var params = buyObj.getShoppingListParam(item, $rootScope.webSettings.allowDecimalsForQuantities);
		if(Util.isNotNull(item) && isEmptyString(item[buyObj.propNameOrdered])){
			item['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');		
			item.validQuantity= false;		
		}
		if(Util.isNotNull(item) &&  item[buyObj.propNameOrdered] >0  && !isNull(item.validQuantity) && item.validQuantity) {
			checkValidQuantity=true;
		}else if(Util.isNotNull(params)){
			checkValidQuantity=true;
		}

        if(checkValidQuantity){
    		if(!isEmptyString($rootScope.currentList)){
    			$rootScope.addItemsToShoppingListService(item,params,buyObj);
    		}else {
    			$scope.ShoppingList.UIMessageKey="";
    			$scope.ShoppingList.ErrOnControl.ListID=false;
    			$scope.ShoppingList.setNewList();
    			$scope.modalInstance = $rootScope.openCreateDefaultListDlg("" , $scope.ShoppingList,item, params,buyObj,null);				
    		}
        }
	};
	
	$rootScope.addOrderLinesToShoppingList = function(items){
		if(!isEmptyString($rootScope.currentList)){
			$rootScope.addItemsToShoppingList(items);	
		}else {
			$scope.ShoppingList.UIMessageKey="";
			$scope.ShoppingList.ErrOnControl.ListID=false;
			$scope.ShoppingList.setNewList();
			$scope.modalInstance = $rootScope.openCreateDefaultListDlg("" , $scope.ShoppingList,items,null);	
		}	
	};
	
	
	$rootScope.addItemsToShoppingListService = function(items,params,buyObj,quantityPropName){
		
		if(!isNull(params)){
			ShoppingListService.addItemToShoppingList(params).then(function(data){
				if(!isNull(data)){	
					$rootScope.handleAddItemMsgCode(data);	
					$rootScope.feedbackPerDialog = Util.translate('DIALOG_ADD_PROD_TO_LIST');
				}
				if(!isNull(buyObj) && !isNull(buyObj.shoppingListItems)){
					clearAddToListMultiQuantityBox(buyObj,items);
				}else if(!isNull(buyObj)){
					clearAddToListQuantityTextBoxQuot(buyObj,quantityPropName);
				}
		    	$rootScope.addToListWatch = "shopping list updated : "+getCurrentDateTimeStr();
			});		
		}
	};
	
	//Add to List from Quotation
	$rootScope.addItemsToShoppingListServiceQuotation = function(items,params,buyObj,quantityPropName){
		if(!isNull(params)){
			ShoppingListService.addItemToShoppingList(params).then(function(data){
				if(!isNull(data)){	
					$rootScope.handleAddItemMsgCode(data);	
				}
				if(!isNull(buyObj) && !isNull(buyObj.shoppingListItems)){
					clearAddToListMultiQuantityBox(buyObj,items);
				}else if(!isNull(buyObj)){
					clearAddToListQuantityTextBox(buyObj,quantityPropName);
				}
		    	$rootScope.addToListWatch = "shopping list updated : "+getCurrentDateTimeStr();
			});		
		}
	};

	
	$rootScope.createNewShoppingListAndAddItem = function(list,items,addToListParams, buyObj, quantityPropertyName,fromQuotation){
   	 var isSharedList = 'NO';
   	 var listID=null;
   	 var listDescription=null;
   	 
   	 if(Util.isNotNull(list.listId)){
   		listID=list.listId;
   	 }
   	 if(Util.isNotNull(list.listDesc)){
   		listDescription=list.listDesc;
   	 }

   	 var params = ["ListId",listID,"ListDesc",listDescription,"CustomerId",$rootScope.webSettings.defaultCustomerCode,"ListOwnerCode",$rootScope.webSettings.selectedListOwner];
   	 if($rootScope.webSettings.allowedSharedList =='Y')
   	 {
   		 if(list.checked){
   			 isSharedList='YES';
   		 }
   		 params.push('Shared');
   		 params.push(isSharedList);
   	 }
   	 
   	 ShoppingListService.createNewShoppingList(params).then(function(data){
   		if(!isNull(data) && data.messageCode==2266){		
   			    $rootScope.currentList=list.listId;
   			    $rootScope.populateShoppingListCount(0);
   			    if(!isNull(items) && Array.isArray(items) && fromQuotation){
   	   			    $rootScope.addItemsToShoppingListQuotation(items,buyObj,quantityPropertyName,fromQuotation);
   			    }else if(!isNull(items) && Array.isArray(items)){
   	   			    $rootScope.addItemsToShoppingList(items,buyObj,quantityPropertyName);
   			    }else {
   			    	$rootScope.addItemsToShoppingListService(null,addToListParams,buyObj,quantityPropertyName);
				}
    			$scope.modalInstance.close();
   		 }else{
   			 $scope.ShoppingList.updateMsg(data,list);
   		 }
   	 });		
    };
    
    $rootScope.addItemsToShoppingList = function(ObjLst,buyObj,quantityPropName)
	{
		var itemObj;
		var items=[];	
		for(var i=0;i<ObjLst.length ;i++){
			itemObj = ObjLst[i];
			var paramObject = new Object();
			var myStr = itemObj.code;
			myStr = myStr.replace(/"/g, '\\"');
			paramObject.itemCode=encodeURIComponent(myStr);
			//paramObject.itemCode = encodeURIComponent(myStr);
			paramObject.unitCode = itemObj.salesUnit;
			paramObject.quantity = itemObj.quantity;
			if($rootScope.webSettings.allowDecimalsForQuantities == true){
				paramObject.quantity = itemObj.quantity.replace(",",".");
				}
			items.push(paramObject);
		}
		var params = "AddItemsToListDetails="+JSON.stringify(items);
		if(isNull(quantityPropName)){
			quantityPropName="quantity";
		}
		$rootScope.addItemsToShoppingListService(items,params,ObjLst,quantityPropName);
	};
	
	//Add to List Quotation
	 $rootScope.addItemsToShoppingListQuotation = function(ObjLst,buyObj,quantityPropName)
		{
			var itemObj;
			var items=[];	
			for(var i=0;i<ObjLst.length ;i++){
				itemObj = ObjLst[i];
				var paramObject = new Object();
				paramObject.itemCode = encodeURIComponent(itemObj.itemCode);
				paramObject.unitCode = itemObj.unit;
				paramObject.quantity = itemObj.quantity1;
				if($rootScope.webSettings.allowDecimalsForQuantities == true){
					paramObject.quantity = itemObj.quantity1.replace(",",".");
					}
				items.push(paramObject);
			}
			var params = "AddItemsToListDetails="+JSON.stringify(items);
			if(isNull(quantityPropName)){
				quantityPropName="quantity1";
			}
			$rootScope.addItemsToShoppingListService(items,params,ObjLst,quantityPropName);
		};
	
	
	///
	$rootScope.getCatalogColumnsToCheck = function(){
		var  columnsToCheck = ['actualPrice','discountPrice','discountPercentage','isBuyingAllowed','availabilityOfItem','enquiryImage'];
		return columnsToCheck;
	};
	
	$rootScope.initCatalogTableHeaders = function(){
		$rootScope.CatalogtableHeaders = {};
	};
	
	$rootScope.getCatalogTableHeaders = function(){
		return $rootScope.CatalogtableHeaders;
	};
	
	$rootScope.setCatalogTableHeaders = function(list){
		var columnsToCheck = $rootScope.getCatalogColumnsToCheck();
		var isBuyAllowed = null;
		if(Util.isNotNull(list)){
			for (var i=0;i<list.length ; i++){
				var  item  = list[i];
				setTableHeader(item,columnsToCheck,$rootScope.getCatalogTableHeaders());
				if(isBuyAllowed==null && item.isBuyingAllowed){
					isBuyAllowed = true;
				}
			}
			//set table header for buy
			if (isBuyAllowed){
				$rootScope.getCatalogTableHeaders().isBuyingAllowed=true;
			}else{
				$rootScope.getCatalogTableHeaders().isBuyingAllowed=false;
			}
		}
		//console.log("Catalog Table headers : " +JSON.stringify($rootScope.getCatalogTableHeaders()));
	};
	
	$rootScope.getProductsDetail = function(itemCode, callback) {
		var requestParams = ["ITEM_CODE", itemCode];
		ProductDetailService.getProductDetails(requestParams,false).then(function(data){
			if(!isNull(data) && data.messageCode==114){
				var msg = '';
				var translatedMsgList = [];
				translatedMsgList.push(Util.translate('MSG_INVALID_ITEM'));
				$rootScope.openMsgDlg(msg,translatedMsgList);
			}else if(data == null ){
				//console.log(data.errorMessage);
			} else {
				callback(data);
			}
		});
	};
	
	$rootScope.loadNewSearchFilters = function(){
		var langCode = NewConfigService.getLanguageCode();
		var prams = ["LanguageCode",langCode];
		Util.getProductFilterDetails(prams).then(function(data){
			DataSharingService.removeProductFilterDetailsSettings();
			DataSharingService.setProductFilterDetailsSettings(data);	
			$rootScope.productfilterDetailSettings = data;
		});
	};
	
	$rootScope.isShowDisableMenuItem = function(menuName){
		var menuItem = $rootScope.getMenuItemByName(menuName);
		if(menuItem != null || menuItem != undefined){
			return menuItem.active;
		}else{
			return false;
		}
	};
	
	// ADM-013
	$rootScope.showBtbDlg = function(item){
		var needToLogin = $rootScope.getSignInRequirement("CON_GENERAL_ENQUIRY");
		if(!needToLogin){
				var tempIsValidQuantity = item.validQuantity;
				var priceCalcObj1 = new QuickViewProduct (item,$rootScope);
				priceCalcObj1.init();
				item.validQuantity = tempIsValidQuantity;
				// ADM-020 START
				priceCalcObj1.mailFrom = $rootScope.getSenderEmailAddress();
				//$rootScope.openQuickViewDlg("QuickView cal", priceCalcObj1);
				var mode = $rootScope.openProcureQuickViewDlg("QuickView procure", priceCalcObj1);
				mode.result.then(function () {
					var custCode = "";
					var custName = "";
					if($rootScope.isSignedIn){
						custCode = Util.translate('COH_CUSTOMER_NUMBER')+" : "+returnBlankIfNull($rootScope.webSettings.defaultCustomerCode);
						custName = Util.translate('CON_CUSTOMER')+" : "+returnBlankIfNull($rootScope.webSettings.defaultCustomerName);
					}
					
					/*calcolo del numero di unità posologiche da richiedere*/
					var pezziRichiesti = priceCalcObj1.itemObject.salesPackageQty;
					//Faccio questo perchè mi rende un dato strano, se il pezzo corrisponde alla confezione in salesPackageQty viene messo 0 anzichè 1
					if(pezziRichiesti==0) pezziRichiesti=1;
					var unitaPosologiche = priceCalcObj1.itemObject.newQuantity*pezziRichiesti;
					/*calcolo del numero di unità posologiche da richiedere*/
					
					var prdCode = Util.translate('COH_ITEM')+" : "+returnBlankIfNull(priceCalcObj1.getItemCode());
					var prdDesc = Util.translate('COH_DESCRIPTION') +" : "+returnBlankIfNull(priceCalcObj1.getDesc());					
					var prodUnit = Util.translate('COH_UNIT') + " : PZ";
					var prdQuantity = Util.translate('COH_QUANTITY') +" : "+unitaPosologiche;
					var userEmail = returnBlankIfNull(priceCalcObj1.mailFrom);
					var userMailId = Util.translate('COH_MAIL_GENERATED_FROM') +" : "+userEmail;
					var subject = Util.translate('MSG_SUBJECT_FOR_PROCURE_THE_ITEM');
					var msgBody = "";
					if($rootScope.isSignedIn){
						msgBody = prdCode+"\n"+prdDesc+"\n"+prdQuantity+"\n"+prodUnit+"\n"+custCode+"\n"+custName+"\n"+userMailId+"\n";
					}else{
						msgBody = prdCode+"\n"+prdDesc+"\n"+prdQuantity+"\n"+prodUnit+"\n"+userMailId+"\n";
					}
					email = new Email(userEmail,subject,msgBody);					
					$rootScope.sendEmailEnquiryNEW(email);			
					
				}, function () {
					//console.log('Procure dialog cancel');
				});				
				// ADM-020 END	
		}
	};
	
	// ADM-020
	$rootScope.showBtbDlgItemDetails = function(item, imageUrl){
		var needToLogin = $rootScope.getSignInRequirement("CON_GENERAL_ENQUIRY");
		if(!needToLogin){
				var tempIsValidQuantity = item.validQuantity;
				var priceCalcObj1 = new QuickViewProduct (item,$rootScope);
				priceCalcObj1.initForItemDetail(imageUrl);
				priceCalcObj1.mailFrom = $rootScope.getSenderEmailAddress();
				item.validQuantity = tempIsValidQuantity;
				var mode = $rootScope.openProcureQuickViewDlg("QuickView procure", priceCalcObj1);
				mode.result.then(function () {
					//console.log('Procure clicked');					
					var custCode = "";
					var custName = "";
					if($rootScope.isSignedIn){
						custCode = Util.translate('COH_CUSTOMER_NUMBER')+" : "+returnBlankIfNull($rootScope.webSettings.defaultCustomerCode);
						custName = Util.translate('CON_CUSTOMER')+" : "+returnBlankIfNull($rootScope.webSettings.defaultCustomerName);
					}
					
					/*calcolo del numero di unità posologiche da richiedere*/
					var pezziRichiesti = priceCalcObj1.itemObject.salesPackageQty;
					//Faccio questo perchè mi rende un dato strano, se il pezzo corrisponde alla confezione in salesPackageQty viene messo 0 anzichè 1
					if(pezziRichiesti==0) pezziRichiesti=1;
					var unitaPosologiche = priceCalcObj1.itemObject.newQuantity*pezziRichiesti;
					/*calcolo del numero di unità posologiche da richiedere*/
					
					var prdCode = Util.translate('COH_ITEM')+" : "+returnBlankIfNull(priceCalcObj1.getItemCode());
					var prdDesc = Util.translate('COH_DESCRIPTION') +" : "+returnBlankIfNull(priceCalcObj1.getDesc());					
					var prodUnit = Util.translate('COH_UNIT') + " : PZ";
					var prdQuantity = Util.translate('COH_QUANTITY') +" : "+unitaPosologiche;
					var userEmail = returnBlankIfNull(priceCalcObj1.mailFrom);
					var userMailId = Util.translate('COH_MAIL_GENERATED_FROM') +" : "+userEmail;
					var subject = Util.translate('MSG_SUBJECT_FOR_PROCURE_THE_ITEM');
					var msgBody = "";
					if($rootScope.isSignedIn){
						msgBody = prdCode+"\n"+prdDesc+"\n"+prdQuantity+"\n"+prodUnit+"\n"+custCode+"\n"+custName+"\n"+userMailId+"\n";
					}else{
						msgBody = prdCode+"\n"+prdDesc+"\n"+prdQuantity+"\n"+prodUnit+"\n"+userMailId+"\n";
					}
					email = new Email(userEmail,subject,msgBody);					
					$rootScope.sendEmailEnquiryNEW(email);
				}, function () {
					//console.log('Procure dialog cancel');
				});				
		}
	};
	
	$rootScope.showPriceCalcDlgFromItemDetail = function(item){
		var tempIsValidQuantity = item.validQuantity;
		if(!$rootScope.isPriceCalculationEnabled) return;
		var needToLogin = $rootScope.isSignOnRequired("CON_PRICE_CALCULATION",$state.current.name);
		if(!needToLogin){
				$rootScope.priceCalDate = new Date();
				$rootScope.priceCalDate = $filter('date')($rootScope.priceCalDate, $rootScope.webSettings.defaultDateFormat);
				var priceCalcObj = new PriceCalculation (item,$rootScope);
				priceCalcObj.itemCodePropName =  "itemCode";
				priceCalcObj.descCodePropName =  "itemDesc";
				if(item.componentCode) priceCalcObj.itemCodePropName = "componentCode";
				if(item.unitCode) priceCalcObj.unitPropName = "unitCode";
				if(item.unitCodesDesc) priceCalcObj.salesUnitsPropName = "unitCodesDesc";
				priceCalcObj.init();
				item.validQuantity = tempIsValidQuantity;
				$rootScope.validateDateRangeFormatDialog("",true);
				$rootScope.openPriceCalcDlg("Price cal", priceCalcObj);
		}
	};
	
	// ADM-015	
	$rootScope.checkIfAvailable = function(data, notAvailableObj) {
		
		switch(data.messageCode)
		{
			case 9001:
				if(!isNull(data.replacingItemBean)){
					notAvailableObj.messageText = Util.translate('MSG_REPLACEMENT_ITEM_IS_NOT_TEMPORARY_AVAILABLE', [notAvailableObj.minsanCode, data.replacingItemBean.minsanCode, data.replacingItemBean.minsanCode]);
					notAvailableObj.minsanCode = data.replacingItemBean.minsanCode;
					notAvailableObj.itemWebText = data.replacingItemBean.itemWebText; 
					notAvailableObj.description = data.replacingItemBean.description;
				}else{
					notAvailableObj.messageText = Util.translate('MSG_ITEM_IS_NOT_TEMPORARY_AVAILABLE');
				}
				$rootScope.openNotAvailableDlg("", notAvailableObj);
				return false;
				break;
			case 9003:
				if(!isNull(data.replacingItemBean)){
					notAvailableObj.messageText = Util.translate('MSG_REPLACEMENT_IS_NOT_COMPLETELY_AVAILABLE', [notAvailableObj.minsanCode, data.replacingItemBean.minsanCode, data.replacingItemBean.minsanCode]);
					notAvailableObj.minsanCode = data.replacingItemBean.minsanCode;
					notAvailableObj.itemWebText = data.replacingItemBean.itemWebText; 
					notAvailableObj.description = data.replacingItemBean.description;
				}else{
					notAvailableObj.messageText = Util.translate('MSG_ITEM_IS_NOT_COMPLETELY_AVAILABLE');
				}
				$rootScope.openNotAvailableDlg("", notAvailableObj);
				//return false;
				break;
			default:
				handleBuyMsgCode(data);
		}
		return true;
	};
	
	// ADM-015	
	var getItemResourceList = function(resourceList,
			resourceType) {
		var resourceItemList = [];
		if (resourceList != null) {
			for ( var i = 0; i < resourceList.length; i++) {
				if (resourceList[i].resourceType == resourceType) {
					resourceItemList = resourceList[i].listOfRT;
				}

			}
		}
		return resourceItemList;
	};
	
	// ADM-010
	$rootScope.refreshUIforPackPiece = function(packOrPiece, productList){
		if(!isNull($rootScope.webSettings) &&$rootScope.webSettings.allowSelectUnitOfMeasure){
			if (Util.isNotNull(productList)) {
				for (var i = 0; i < productList.length; i++) {
					$rootScope.updatePackPiece(
							packOrPiece, productList[i]);
					 var prod = productList[i];
					 prod.priceForSort = getPriceForSort(prod.discountPrice, prod.actualPrice);
				}
			}
		}else{
			if (Util.isNotNull(productList)) {
				for (var i = 0; i < productList.length; i++) {
				 var prod = productList[i];
				 prod.priceForSort = getPriceForSort(prod.discountPrice, prod.actualPrice);
				}
			}
		}
	};

	//ADM-010
	getPriceForSort = function(discountPrice, actualPrice){
		var priceForSort = 0;
		if (Util.isNotNull(discountPrice)) {
			priceForSort = convertStringToFloat(discountPrice); 
		}else{
			priceForSort = convertStringToFloat(actualPrice); 
		}
		return priceForSort;
	};
	
	// ADM-010
	$rootScope.updatePackPiece = function(packOrPiece,
			productBean) {
		if (!isEmptyString(packOrPiece) && Util.isNotNull(productBean.salesUnit)) {

			// Update the unit description
			if(!isNull(productBean.salesPackageQty) && productBean.salesPackageQty > 1){
				if(packOrPiece == "CF"){
					productBean.defaultSalesUnitDesc = packOrPiece + productBean.salesPackageQty;
				}else{
					productBean.defaultSalesUnitDesc = packOrPiece + "/" + productBean.salesPackageQty;
				}
			}else{
				productBean.defaultSalesUnitDesc = packOrPiece;
			}
			productBean.selectedUnitObject.salesUnitDesc = productBean.defaultSalesUnitDesc;
			
			// Update the unit and the data
			if(productBean.salesUnit != packOrPiece){
				productBean.salesUnit = packOrPiece;
				productBean.selectedUnit = productBean.salesUnit;
				productBean.defaultSalesUnit = packOrPiece;
				productBean.selectedUnitObject.salesUnit = packOrPiece;

				if(!isNull(productBean.salesPackageQty) && productBean.salesPackageQty > 1){
					if(packOrPiece == "CF"){
						productBean.actualPrice = $rootScope.convertToPack(productBean.actualPrice, productBean.salesPackageQty);
						if(!isNull(productBean.discountPrice)){
							productBean.discountPrice = $rootScope.convertToPack(productBean.discountPrice, productBean.salesPackageQty);
						}
					}else{
						productBean.actualPrice = $rootScope.convertToPiece(productBean.actualPrice, productBean.salesPackageQty);
						if(!isNull(productBean.discountPrice)){
							productBean.discountPrice = $rootScope.convertToPiece(productBean.discountPrice, productBean.salesPackageQty);
						}
					}
				}
			}
		}
	};
	
	// ADM-010
	$rootScope.updatePackPieceFromShoppingCart = function(packOrPiece,
			orderLine) {
		var enteredValues = orderLine.ivEnteredValues;
		var item = orderLine.ivItem;
		var line = orderLine.ivOrderLine;
		
		if (!isEmptyString(packOrPiece)) {

			// Update the unit description
			if(!isNull(item.salesPackageQty) && item.salesPackageQty > 1){
				if(packOrPiece == "CF"){
					enteredValues.UnitDesc = packOrPiece + item.salesPackageQty;
				}else{
					enteredValues.UnitDesc = packOrPiece + "/" + item.salesPackageQty;
				}
			}else{
				enteredValues.UnitDesc = packOrPiece;
			}

			if(enteredValues.Unit != packOrPiece){
				enteredValues.Unit = packOrPiece;
				
				item.defaultSalesUnit = packOrPiece;
				item.defaultSalesUnitDesc = packOrPiece;
				item.salesUnits = [packOrPiece];
				item.salesUnitsDesc = [{"salesUnit" : packOrPiece, "salesUnitDesc" : packOrPiece}];
//				item.productUnitList = [{"unit" : packOrPiece, "unitDesc" : packOrPiece}];
				
				line.unitCode = packOrPiece;
				line.item = item;
				if(!isNull(item.salesPackageQty) && item.salesPackageQty > 1){
					if(packOrPiece == "CF"){
						line.price = $rootScope.convertToPack(line.price, item.salesPackageQty);
						line.orderedQuantity = line.orderedQuantity / item.salesPackageQty;
					}else{
						line.price = $rootScope.convertToPiece(line.price, item.salesPackageQty);
						line.orderedQuantity = line.orderedQuantity * item.salesPackageQty;
					}
					enteredValues.Quantity = convertFloatToString(line.orderedQuantity);
				}
			}
		}
	};
		
	// ADM-010
	// Convert from pack to piece
	$rootScope.convertToPiece = function(price, quantity){
		var priceN = convertStringToFloat(price);
		var quantityN = (quantity);
		if(quantity > 0){
			priceN = priceN / quantityN;
		}
		return convertFloatToString(priceN);
	};
	
	// ADM-010
	// Convert from piece to pack
	$rootScope.convertToPack = function(price, quantity){
		var priceN = convertStringToFloat(price);
		var quantityN = (quantity);
		if(quantity > 0){
			priceN = priceN * quantityN;
		}
		return convertFloatToString(priceN);
	};
	
	// ADM-010
	convertFloatToString = function(value){
		if(!isNull($rootScope.webSettings.localeCode) && $rootScope.webSettings.localeCode !=""){
			var langCode = $rootScope.webSettings.localeCode;
			langCode = langCode.replace("_","-");
			newVal = value.toLocaleString(langCode);
		}
		return newVal;
	};
	
	// ADM-010
	convertStringToFloat = function(value){
		var newVal = value;
		// newVal = newVal.replace(getThousandSeparator(),"");
		newVal = newVal.replace(getDecimalSeparator(),".");
		newVal = parseFloat(newVal); 
		return newVal;
	};
	
	// ADM-010
	function getThousandSeparator() {
		if(getDecimalSeparator() == "."){
			return ",";
		}else{
			return ".";
		}
	};

	// ADM-010
	function getDecimalSeparator() {
		var decSep = ",";

		try {
			// this works in FF, Chrome, IE, Safari and Opera
			var sep = parseFloat(3 / 2).toLocaleString().substring(1, 2);
			if (sep === '.') {
				sep = decSep;
			}
		} catch (e) {
		}

		return sep;
	};
	
	//ADM-002
	$rootScope.validateCart= function(){
		ShoppingCartService.validateCart([]).then(function(data){
			if(Util.isNotNull(data)){
				handleValidateCode(data);
				if(isNull(data.validateLineStatusList)){
					$state.go('home.reviewOrder');
				}
			}					
		});
	};
	
	//ADM-002
	var handleValidateCode = function (data){
		var translatedMsgList = [];
		var msg = null;
		if(!isNull(data) && Array.isArray(data.validateLineStatusList) && data.validateLineStatusList.length > 0){
			for(var i=0; i < data.validateLineStatusList.length ; i++){
				var msgItem = data.validateLineStatusList[i];
				var msgCode = msgItem.messageCode + "";
				var itemCodeStr = "";
				if(!isNull(msgItem) && !isNull(msgItem.itemCode)){
					itemCodeStr = msgItem.itemCode + " " + msgItem.itemDescription + " : ";
				}				
				switch (msgCode){
				case '3014' :
					break;
				case '3013' :
				case '3003' :
					msg = itemCodeStr  +Util.translate('MSG_UNABLE_TO_ADD_ITEM_TO_CART');
					break;
				case '3101' :
					msg = itemCodeStr  +Util.translate('MSG_ITEM_IS_NOT_AVAILABLE');
					break;
				case '3102' :
					msg = itemCodeStr  +Util.translate('MSG_ITEM_QUANTITY_ZERO');
					break;
				case '3103' :
					msg = itemCodeStr  +Util.translate('MSG_NEGATIVE_QUANTITY');
					break;
				case '3104' :
					msg = itemCodeStr  +Util.translate('MSG_QUANTITY_EXCEEDS_LIMIT');
					break;
				case '3105' :
					msg = itemCodeStr  +Util.translate('MSG_INVALID_DATE');
					break;
				case '3106' :
					msg = itemCodeStr  +Util.translate('MSG_INVALID_DATE');
					break;
				case '3107' :
					msg = itemCodeStr  +Util.translate('MSG_NOT_WORK_DAY');
					break;
				case '3113' :
					msg = itemCodeStr  +Util.translate('MSG_DELIVERY_NOT_POSSIBLE');
					break;
				case '3114' :
					msg = itemCodeStr  +Util.translate('MEX_CUSTOMER_HAS_SALES_RESTRICTIONS');
					break;
				case '3109' :
					msg = itemCodeStr  +Util.translate('MSG_NO_ITEM_FOUND_FOR_CODE');
					break;
				case '3110' :
					msg = itemCodeStr  +Util.translate('MSG_ITEM_INVALID_UNIT');
					break;
				case '3111' :
					msg = itemCodeStr  +Util.translate('MSG_ITEM_NOT_ALLOWED_UNIT');
					break;
				case '3112' :
					msg = itemCodeStr  +Util.translate('MSG_ITEM_PRICE_NOT_AVAILABLE');
					break;
				case '3116' :
					msg = itemCodeStr  +Util.translate('MSG_ITEM_PRICE_NOT_AVAILABLE');
					break;
				case '3121' :
					msg = itemCodeStr +Util.translate('MSG_INVALID_QUANTITY');
					break;
				case '3122' :
					msg = itemCodeStr +Util.translate('MSG_DELIVERY_DATE_NOT_POSSIBLE');
					break;
				case '5001' :
					msg = itemCodeStr  +Util.translate('MSG_UNEXPECTED_BUY_ERROR');
					break;
				case '3117' :
					msg = itemCodeStr  +Util.translate('MSG_BUY_NOT_ALLOWED');
					break;
				case '3334' :
					msg = itemCodeStr  +Util.translate('MSG_SHIPMENT_LENGTH_EXCEEDS');
					break;
				case '3234' :
					msg = itemCodeStr  +Util.translate('MSG_SP_MISSING_FOR_SOURCING_INFO');
					break;
				case '9001' :
				case '9003' :
					var packOrPiece = $scope.freeSearchObj.packPiece;
					if(packOrPiece == "CF"){
						if(!isNull(msgItem.salesPackageQty) && msgItem.salesPackageQty > 1){
							msgItem.orderedQuantity = msgItem.orderedQuantity / msgItem.salesPackageQty;
							msgItem.availableQuantity = msgItem.availableQuantity / msgItem.salesPackageQty;
						}
					}
					msg = itemCodeStr + Util.translate('MSG_QTY_IS_NOT_AVAILABLE',[msgItem.orderedQuantity, msgItem.availableQuantity, packOrPiece, packOrPiece]);
					break;
				}
				if(!isNull(msg)){
					translatedMsgList.push(msg);
				}
			}
		}
		//openMsgDlg
		if(translatedMsgList.length > 0){
			var msg = Util.translate('CON_WARNING',null);
			var dlg = $rootScope.openMsgDlg(msg,translatedMsgList);
			dlg.result.then(function () {
				//console.log('Delete order modal Ok clicked');
			}, function () {
				//console.log('Delete order modal Cancel clicked');
				$scope.$root.$eval();
			});
		}
		return translatedMsgList;
	};
}]);