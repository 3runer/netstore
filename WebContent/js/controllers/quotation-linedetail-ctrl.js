
catalougeControllers.controller('quotationLineDetailCtrl', ['$rootScope','$scope', '$http','$log', 'DataSharingService','QuotationService', 'Util','$state',
                                                            function ($rootScope,$scope, $http,$log,DataSharingService,QuotationService,Util,$state) {

	var quotationLineDetailCtrlInit = function(){
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
	};
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	//	$log.log(" quotationLineDetailCtrl history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	var initController = function(){
		var requestParams = [];
		$scope.dataFound = false;
		$scope.resultMsg ="";
		$scope.errorMessage = "";
		$scope.callState = $state.current.name;
//		$scope.prevState = 'home.quotation.detail';
//		DataSharingService.setObject("pageState",$state.current.name);
	//	var quotationLine = DataSharingService.getObject("quotationLine_clickedDetail");
		var quotationLine = DataSharingService.getFromSessionStorage("quotationLine_clickedDetail");
	//	var quotation = DataSharingService.getObject("quotation_clickedDetail");
		var quotation = DataSharingService.getFromSessionStorage("quotation_clickedDetail");
		if( quotationLine !=null){
			$scope.quotationLine = quotationLine;
			$scope.quotationData = quotation;
			if(quotationLine.vatFlag == false)
			{	
				$rootScope.headingPrice = "Price excl VAT";	
				$rootScope.Price = quotationLine.price;
			}
			else
			{
				$rootScope.headingPrice = "Price Incl VAT";
				$rootScope.Price = quotationLine.price;
			}
			//console.log("quotation line detail "+ JSON.stringify(quotationLine));
			//console.log("quotation line detail requestParams"+ JSON.stringify(requestParams));
		};

	};
	quotationLineDetailCtrlInit();
	
}]);