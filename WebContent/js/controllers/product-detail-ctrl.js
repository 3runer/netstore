// product detail controller
//product search controller
catalougeControllers.controller('productDetailCtrl', ['$rootScope','$scope','$stateParams', '$http','$log', '$state','ProductDetailService','DataSharingService','Util','authentication',
                                                            function ($rootScope,$scope,$stateParams, $http,$log,$state, ProductDetailService,DataSharingService,Util,authentication) {
	var productDetailCtrlInit = function(){
		/*var lc = DataSharingService.getFromLocalStorage("languageChanged");
		if(lc == 1) {
			prdDetailInit();
			DataSharingService.setToLocalStorage("languageChanged", 0);
			return;
		}*/
		/*if(historyManager.isBackAction && !isNull($scope.prdDetail)){
			historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			//initController();
			*/
			var menuList = DataSharingService.getObject("functionMenus");
			if(!isNull(menuList)) {
				initWithCheck();
			} else {
				$rootScope.$on('menuUpdated', function(){ 
					initWithCheck();
				});
			}
			
		//}
	};
	
	function initWithCheck() {
		if(!$rootScope.isProductDetailPageEnabled){
			var translatedMsgList = [];
			translatedMsgList.push(Util.translate('TXT_CF_041'));
			var msg = '';
			if(isNull($rootScope.isSessionExpireMsgPopUpOpen) || $rootScope.isSessionExpireMsgPopUpOpen){
				$rootScope.isSessionExpireMsgPopUpOpen = true;
				var dlg = $rootScope.openMsgDlg(msg,translatedMsgList);
				dlg.result.then(function () {
					$rootScope.isSessionExpireMsgPopUpOpen=false;
					$rootScope.reloadHomePage();
				}, function () {
					$rootScope.isSessionExpireMsgPopUpOpen=false;
					$rootScope.reloadHomePage();
				});
			}
			$rootScope.closeLoader();
			$rootScope.closeInnerLoader();
			return;
		}
		if($rootScope.showProductDetailSignon && !authentication.isLoggedIn()) {
			$rootScope.open($state.current.name, true, $stateParams);
		} else {
			prdDetailInit();
		}
	}
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	//	$log.log(" productDetailCtrl history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	
	
	// old code
	//var requestParams = [];
	var isWhsTabOpen = DataSharingService.getObject("whstabstatus");
	
	var setWhsExtendedData = function(whs){		 
		whs.showAvailabilityAsLink = showAvailabilityAsLink();
		whs.showAvailabilityAsImage = showAvailabilityAsImage();
		whs.availabilityImageName = getAvailabilityImage(whs.available);
		whs.availabilityText = getAvailabilityText(whs.available);
		handleEmptyAvailabilityImage(whs);
	};
	var setWareHouseList = function(whsList){
		if(Array.isArray(whsList) && whsList.length > 0){
			for(var i=0;i<whsList.length;i++){
				var obj=whsList[i];				
				setWhsExtendedData(obj);
			}
		}
		
	};
	
	var prdDetailInit = function(){
		$scope.ready = true;
		$scope.prdDetail = new ProductDetailUI($rootScope);
		$scope.prdDetail.setRequestItemCode($stateParams.ITEM_CODE);
		$scope.prdDetail.validQuantity =true;
		$scope.dataFound = false;
		$scope.resultMsg ="";
		$scope.errorMessage = "";
		$scope.slides = [];
		//to be binded to subCatalogueBeanList after server side changes
		$scope.subCatalogueBeanList = "";			
		$scope.propertyList = [];
		$scope.product = DataSharingService.getObject("catalog_clickedProduct");
		DataSharingService.setObject("catalog_ProductCode",$stateParams.ITEM_CODE);
		$scope.productItemCode = $stateParams.ITEM_CODE;
		$scope.isNormalItem = true;
		$scope.isComponentItem = false;
		//changes to display whstab and to consider restriction 
		$rootScope.initCommitsPromise.then(function(){
			$scope.getProductDetail($scope.prdDetail.getParams(),isWhsTabOpen);
		});
	};
	
	$scope.showHide = function(item){		
		if(item === true){	return true;   }
		return false;
	};
	
	$scope.buyItems = function(item){
		$rootScope.buyMultipleItems(item,$scope.buyObj);
	};
	
	$scope.addMatrixItemsToCurrentList = function(item){
		$rootScope.addMultipleItemsToList(item,$scope.buyObj);
	};
	
	$scope.addComponentItemsToCurrentList = function(item){
		//console.log("addComponentItemsToCurrentList");
		$rootScope.addMultipleItemsToList(item,$scope.componentBuyObj);
	};
	//prdDetailInit();
	// to fetch the child matrix detail
	$scope.getTranslatedUnit = function(unitCode){
		var unitObj =  getSelectedSalesUnit(unitCode,$scope.productDetails.unitCodesDesc);
		if(isNull(unitObj) || isNull(unitObj.salesUnitDesc)){
			return "";
		}
		return unitObj.salesUnitDesc; 
	};
	
	$scope.showPriceOnQuantity = function(item,quantity,prdDetail,validQuantity){
		if(validQuantity == true && !isNull(quantity)){
		
			$scope.showAutoPriceCal = $rootScope.webSettings.showPriceCalculation;
			if($scope.showAutoPriceCal ==  true){
				var param=[];
				var paramObject = new Object();
				var myStr = item.itemCode;
				myStr = myStr.replace(/"/g, '\\"');
				paramObject.itemCode=encodeURIComponent(myStr);
				paramObject.unitCode = prdDetail.selectedUnit;
				paramObject.currencyCode = $rootScope.webSettings.actualCurrencyCode;
				paramObject.dateString = "";
				paramObject.quantity = quantity;
				param.push(paramObject);
				var paramSting = "PriceCalculationData=" + JSON.stringify(param);
				//var newData = $rootScope.getPriceForQuantityChange(paramSting);
				Util.postDataToServer('priceCalculationUrl',paramSting).then(function(data){
					setPriceDataQuantity(data,true);
				});
			}
		}
	};
	
	$scope.showPriceOnQuantityProductChange = function(item,quantity,validQuantity){
		if(validQuantity == true && !isNull(quantity)){
		$scope.showAutoPriceCal = $rootScope.webSettings.showPriceCalculation;
		if($scope.showAutoPriceCal ==  true){
		var param=[];
		var paramObject = new Object();
		paramObject.itemCode = encodeURIComponent(item.itemCode);
		paramObject.unitCode = item.salesUnit;
		paramObject.currencyCode = $rootScope.webSettings.actualCurrencyCode;
		paramObject.dateString = "";
		paramObject.quantity = quantity;
		param.push(paramObject);
		var paramSting = "PriceCalculationData=" + JSON.stringify(param);
		//var newData = $rootScope.getPriceForQuantityChange(paramSting);
		Util.postDataToServer('priceCalculationUrl',paramSting).then(function(data){
			setPriceDataQuantity(data,true);
      });}}
	};
	
	var setPriceDataQuantity = function(data){
		$scope.netValue = data[0].netValue; 
	};
	
	$scope.getMatrixChildDetail = function(requestParams,quantity,validQuantity)
	{
		ProductDetailService.getProductDetails(requestParams).then(function(data){
			if(!isNull(data) && data.messageCode==114){
				var msg = '';
				var translatedMsgList = [];
				translatedMsgList.push(Util.translate('MSG_INVALID_ITEM'));
				$rootScope.openMsgDlg(msg,translatedMsgList);
			}else if(data == null ){
				$scope.errorMessage = data.errorMessage;	
				$scope.dataFound = false;
			}
			else
			{
				//console.log("productDetailCtrl messageCode -- "+data.messageCode);						
				$scope.productDetails = data;	
				/* ADM-010 */refreshUIforPackPiece($scope.freeSearchObj.packPiece);
				$scope.errorMessage = " ";	
				
				$scope.dataFound = true;				
				$scope.isMatrixBasedItem = true;
				$scope.propertyList = Util.getObjPropList(data);
				$scope.selectedUnit = data.defaultSalesUnit;
				if(!isNull(data.subCatalogueBeanList)){
					$scope.subCatalogueBeanList = data.subCatalogueBeanList;
				}
				//setWhsTabSettings($scope.productDetails.isWarehouseDetailsShown);
				setWhsTabSettings($rootScope.isWarehouseTabDisplay);
				var isWarehouseBeanList = $rootScope.isColExist('warehouseBeanList',$scope.propertyList);
				var imageList =  getItemResourceList(data.resourceBeanList,"IMAGE");					
				var rList = data.resourceBeanList;
				$scope.resourceBeanList = makeResourceList(rList);   
				var showAvailabilityAsLink = $rootScope.showAvailabilityAsLink();
				var showAvailabilityAsImage = $rootScope.showAvailabilityAsImage();
				if((showAvailabilityAsLink) && (showAvailabilityAsImage))
				{
				 	$scope.showAvailabilityLinkImage = true; 
				}
				if((showAvailabilityAsLink) && (!showAvailabilityAsImage))
				{
					$scope.showAvailabilityLinkText = true;
				}
				if(!showAvailabilityAsLink)
				{
					$scope.showAvailabilityText = true;
				}  
				
				
				
				if(isWarehouseBeanList)
				{	$scope.warehouseBeanList = data.warehouseBeanList;
					setWareHouseList($scope.warehouseBeanList);
				}
				
				
				var webLinkListBeanList = $rootScope.isColExist('webLinkListBean',$scope.propertyList); 
					
				if(webLinkListBeanList)
					{ $scope.webLink = data.webLinkListBean;}
				else
				{
					$scope.webLink = {};
				}
				
				makeImageSlider(imageList,true);
				
				//$scope.isMatrixOption2 = false;
				$scope.prdDetail.setChildDataResponse(data);
				$scope.showPriceOnQuantityProductChange($scope.productDetails,quantity,validQuantity);
			}

		});

	};
	
	$scope.getSegmentationDesc3 = function()
	{
		$scope.prdDetail['selectedSeg3']  = {};
		
	};
 $scope.getProductDetail = function(requestParams,whsTabSelected,isUnitChangeMatrix)
 {
	//changes to display whstab and to consider restriction
	 if(!isNull(whsTabSelected) && whsTabSelected ==  true){
	 whsTabSelected = $rootScope.isWarehouseTabDisplay;
	 }
	 if(isNull(isUnitChangeMatrix) || (isUnitChangeMatrix == undefined)){
		 isUnitChangeMatrix = false;
	 }
	 if(!$scope.isNormalItem && !isUnitChangeMatrix) {
		 $scope.repeats = true;
		 historyManager.updateState("home.productDetail", requestParams);
		 historyManager.pushScopeObject($scope);
		 $rootScope.showProductDetail(requestParams[1], whsTabSelected);
	 }
	 if(!isNull(requestParams[0])){
		 
	 
	ProductDetailService.getProductDetails(requestParams,false).then(function(data){
		if(!isNull(data) && data.messageCode==114){
			var msg = '';
			var translatedMsgList = [];
			translatedMsgList.push(Util.translate('MSG_INVALID_ITEM'));
			$rootScope.openMsgDlg(msg,translatedMsgList);
		}else if(data == null ){
			$scope.errorMessage = data.errorMessage;	
			$scope.dataFound = false;
		}
		else
		{
			//console.log("productDetailCtrl messageCode -- "+data.messageCode);						
			
			$scope.productDetails = data;			
			$scope.errorMessage = " ";	
			/* ADM-010 */refreshUIforPackPiece($scope.freeSearchObj.packPiece);
			$scope.dataFound = true;				
			//setWhsTabSettings($scope.productDetails.isWarehouseDetailsShown);
			setWhsTabSettings($rootScope.isWarehouseTabDisplay);
			$scope.propertyList = Util.getObjPropList(data);
			$scope.selectedUnit = data.defaultSalesUnit;
			if(!isNull(data.catalogueBeanList)){
				$scope.subCatalogueBeanList = data.catalogueBeanList;
			}
			$scope.isMatrixBasedItem = $rootScope.isColExist('matrixItemDetailList',$scope.propertyList);
			var isWarehouseBeanList = $rootScope.isColExist('warehouseBeanList',$scope.propertyList);
			var imageList =  getItemResourceList(data.resourceBeanList,"IMAGE");					
			var rList = data.resourceBeanList;
		    $scope.resourceBeanList = makeResourceList(rList);   			
			var showAvailabilityAsLink = $rootScope.showAvailabilityAsLink();
			var showAvailabilityAsImage = $rootScope.showAvailabilityAsImage();
			if((showAvailabilityAsLink) && (showAvailabilityAsImage))
			{
			 	$scope.showAvailabilityLinkImage = true; 
			}
			if((showAvailabilityAsLink) && (!showAvailabilityAsImage))
			{
				$scope.showAvailabilityLinkText = true;
			}
			if(!showAvailabilityAsLink)
			{
				$scope.showAvailabilityText = true;
			}  
			
			var webLinkListBeanList = $rootScope.isColExist('webLinkListBean',$scope.propertyList); 
				
			if(webLinkListBeanList)
				{ $scope.webLink = data.webLinkListBean;}
			else
			{
				$scope.webLink = {};
			}
			
			makeImageSlider(imageList,false);
			if(isWarehouseBeanList)
			{	$scope.warehouseBeanList = data.warehouseBeanList;
				setWareHouseList($scope.warehouseBeanList);
			
			}
			
			if($scope.isMatrixBasedItem)
				{
					$scope.matrixItemList = data.matrixItemDetailList;				
					$scope.isMatrixOption2 = data.matrixItemDetailList[0].isShowTableViewForMatrixItems;
					for(var i=0;i<data.matrixItemDetailList[0].childMatrixList.length;i++){
						var obj=data.matrixItemDetailList[0].childMatrixList[i];
						obj['validQuantity'] =true;
						$scope.prdDetail.setMatrixChildExtendedData(obj);
					}
					$scope.childMatrixList =  data.matrixItemDetailList[0].childMatrixList;
					$scope.matrixColumnList = getUniqueItemsList('columnTemplateName',data.matrixItemDetailList[0].childMatrixList);
					$scope.matrixRowList = getUniqueItemsList('verticalTemplateName',data.matrixItemDetailList[0].childMatrixList);
					$scope.matrixList = makeMatrixListRows($scope.matrixRowList,$scope.matrixColumnList);
					initMatrixBuyObject($scope.matrixList);
					$scope.columnTemplateName = data.matrixItemDetailList[0].columnTemplateName;
					$scope.verticalTemplateName = data.matrixItemDetailList[0].verticalTemplateName;
					
					$scope.prdDetail.setMatrixColumnList($scope.matrixColumnList);
					$scope.prdDetail.setMatrixRowList($scope.matrixRowList);
					//$log.log(" Matrix row data--  " +JSON.stringify($scope.matrixRowList));
				//	$log.log(" Matrix col data--  " +JSON.stringify($scope.matrixColumnList));
					//console.log(" final matrix data--  " +JSON.stringify($scope.matrixList));
				}
			
			if(data.hasComponentItems)
				{
				for(var i=0;i<data.componentBeans.length;i++){
					var obj=data.componentBeans[i];
					obj['validQuantity'] =true;
					$scope.prdDetail.setExtendedData(obj);
				}
					$scope.componentList = data.componentBeans;
					initComponentBuyObject($scope.componentList);
					$scope.isComponentItem = true;
				}
			
			
			if(($scope.isComponentItem) || ($scope.isMatrixBasedItem)){
				$scope.isNormalItem = false;
			}
			else
			{
				$scope.isNormalItem = true;
			}
			
			//console.log(" product Details data--  " +JSON.stringify($scope.productDetails));
			//$scope.prdDetail = new ProductDetailUI($rootScope);
			$scope.prdDetail.setRequestItemCode($stateParams.ITEM_CODE);
			$scope.prdDetail.validQuantity =true;
			$scope.prdDetail.setPrdBaseDetail(data);
			$scope.netValue = "";
			$rootScope.changeHomePageTitle(Util.translate('TXT_PAGE_TITLE_ITEM') + $scope.prdDetail.prdInfo.itemDesc, true);
			if($scope.isMatrixBasedItem)
				{
					var unitCode = $scope.matrixList[0][0].matrixUnitCode;
					var itemCode = $scope.matrixList[0][0].itemMatrixCode;
					
					var requestParam = $scope.prdDetail.makeParams(itemCode,unitCode);
					$scope.getMatrixChildDetail(requestParam);
				//	$scope.prdDetail.prdInfo.itemCode =   itemCode;
				}
			$scope.showWhsTab(whsTabSelected);
		}

	});}
 };
 
    $scope.buyItem = function()
    {
    	var itemCode = $scope.prdDetail.prdInfo.itemCode;
    	var salesUnit = $scope.prdDetail.selectedUnit;
    	var quantity = $scope.prdDetail.quantity;
    	if(!$scope.prdDetail.validQuantity || isNull(quantity) || ""==quantity){
    		$scope.prdDetail.validQuantity = false;
    		$scope.prdDetail['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
		}else{
		$scope.prdDetail.prdInfo.isBuyAllowed  = false;
    	$rootScope.addItemToCartPrdDetail(itemCode,quantity,salesUnit,$scope.prdDetail,$scope.prdDetail.prdInfo,"quantity");
    	$scope.netValue = null;
		}
    };
    
    $scope.addItem = function(item)
    {
    	if(!($scope.prdDetail.validQuantity) || isNull($scope.prdDetail.quantity) || ""==($scope.prdDetail.quantity)){
			$scope.prdDetail.validQuantity = false;
			$scope.prdDetail['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
		}else{
    	var item={};
    	item['code'] = $scope.prdDetail.prdInfo.itemCode;
    	item['salesUnit'] = $scope.prdDetail.selectedUnit;
    	item['quantity'] = $scope.prdDetail.quantity;
    	item['validQuantity'] = $scope.prdDetail.validQuantity;
    	$rootScope.addItemToCurrentList(item);
    	if($scope.prdDetail.validQuantity){
    		$scope.prdDetail.quantity='';
    	}
		}
    };
    
    $scope.addComponentItem = function(item)
    {
    	if(!(item.validQuantity) || isNull(item.newQuantity) || ""==(item.newQuantity)){
    		item.validQuantity = false;
    		item.quantityBoxMsg = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
		}else{
    	var componentItem={};
    	componentItem['code'] = item.componentCode;
    	componentItem['salesUnit'] = item.unitCode;
    	componentItem['quantity'] = item.newQuantity;
    	componentItem['validQuantity'] = item.validQuantity;
    	$rootScope.addItemToCurrentList(componentItem);
    	if(item.validQuantity){
    		item.newQuantity='';
    	}
		}
    };
 
	var initMatrixBuyObject = function(matrixList){
		var list = [];
		for(var i=0;i<matrixList.length;i++){
			var row = matrixList[i];
			for(var j=0;j<row.length;j++){
				var item = row[j];
				item = list.push(item);
			}
		}
		$scope.buyObj = new Buy(list);
		$scope.buyObj.setPropNameItemCode('itemMatrixCode');
		$scope.buyObj.setPropNameOrdered('newQuantity');
		$scope.buyObj.setPropNameUnit('matrixUnitCode');
		$scope.buyObj.setPropNameisBuyAllowed('isMatrixBuyAllowed');
	};
	
	var initComponentBuyObject = function(componentList){
		
		$scope.componentBuyObj = new Buy(componentList);
		$scope.componentBuyObj.setPropNameItemCode('componentCode');
		$scope.componentBuyObj.setPropNameOrdered('newQuantity');
		$scope.componentBuyObj.setPropNameUnit('unitCode');
		$scope.componentBuyObj.setPropNameisBuyAllowed('isBuyAllowed');
	};
	
	$scope.buyComponentItems = function(item){
		$rootScope.buyMultipleItems(item,$scope.componentBuyObj);
	};
    
 	var makeResourceList = function(rList)
 	{
 		var resource = {};
 		var listRT = [];
 		var listResDesc = [];
 		var resourceList = [];
 		var linkResourceList = [];
 		if(angular.isDefined(rList) && rList!=null)
 		{

 			if(rList.length > 0)
 			{
 				for(var i = 0; i< rList.length; i++ )
 				{

 					if(rList[i].resourceType == "IMAGE")  { 	
 						continue;   }
  					resource = new Object();	
 					listRT = rList[i].listOfRT;
 					listResDesc = rList[i].resourceListDesc;
 					if(listRT.length>0)
 					{
 						for(var j = 0; j< listRT.length; j++ )
 						{
 							if(!rList[i].isRelative){
 								listRT[j]= listRT[j];
 							}else{
 								listRT[j]= $rootScope.BASE_ADDRESS + listRT[j];
 							}
  							//listRT[j]= $rootScope.BASE_ADDRESS+listRT[j].replace("/NS/","/NS8/"); // to be removed
  						}   			  		 		
 					}
 					
 					if(listResDesc.length>0)
 					{
 						for(var j = 0; j< listResDesc.length; j++ )
 						{
 							listResDesc[j]= listResDesc[j];
 							//listRT[j]= $rootScope.BASE_ADDRESS+listRT[j].replace("/NS/","/NS8/"); // to be removed
 						}   			  		 		
 					}
 					if(listResDesc.length>0){
 						for(var j = 0; j< listResDesc.length; j++ )
 						{
 							linkResourceList[j]= {liOfRT:listRT[j], liResDesc:listResDesc[j]};
 							//listRT[j]= $rootScope.BASE_ADDRESS+listRT[j].replace("/NS/","/NS8/"); // to be removed
 						}   			  		 		
 					}
 					resource['resourceType'] = rList[i].resourceType;
 					resource['resourceDescName']=listResDesc;
 					resource['listOfRT'] = listRT;
 					resource['resourceListDesc'] = linkResourceList;
 					resourceList.push(resource);
 				}

 			}

 		}
 		return resourceList;
 	};
	
	var getItemResourceList = function(resourceList,resourceType)
	{
		var resourceItemList = [];
		if(resourceList != null)
			{
				for(var i=0;i<resourceList.length;i++)
				{
					if(resourceList[i].resourceType == resourceType)
						{
							resourceItemList = resourceList[i].listOfRT;
						}
			
				}
			}
		return resourceItemList;
	};
	
	var makeImageSlider = function(imageList,isChildMatrixImageList)
	{	
		for(var i = 0 ;i< imageList.length;i++)
		{
		  imageUrl = $rootScope.BASE_ADDRESS+imageList[i]; 
		  //imageUrl =  imageUrl.replace("/NS","/NS8"); //remove this line when WS is ready with correct image url
		  imageList[i] = imageUrl;
		}
		if(isChildMatrixImageList)
			{
				$scope.smallSlider.setMatrixImageList(imageList);
				var index = $scope.smallSlider.getImageIndex(imageList[0]);
				//$log.log("smallSlider : child image index = " + index);
				$scope.smallSlider.setMasterIndex(index);
			}
		else
			{
				var gs = new GroupImageSlider(imageList,3);
				$scope.smallSlider = gs;
			}
		//$log.log("smallSlider : " + JSON.stringify($scope.smallSlider));
	};
	
	var makeMatrixListRows = function(rowList,colList)
	{
		var childList = $scope.childMatrixList;
		var matrixList = [];
		var itemList = [];
		var obj = {};
		 matrixIndex = [];
		for(var j =0; j< rowList.length;j++)
			{
				var itemIndex = 0;
				matrixIndex = [];
				var colArray = [];
				for(var i =0; i< colList.length;i++)
				{
					obj = new Object();					
					itemIndex = parseInt(rowList[j].index) + parseInt(colList[i].index);
					obj = childList[itemIndex];				
					matrixIndex.push(itemIndex);
					colArray.push(obj);
				}
				matrixList.push(colArray);
				itemList.push(matrixIndex);
			}
			//$log.log("Rows "  + JSON.stringify(matrixList));
		
		return  matrixList;	
	};
	// to get Unique key value
	var getUniqueItemsList = function(baseKey,listArray)
	{
		var unique = {};
		var distinct = [];
		    for( var i in listArray ){
		    	var key = listArray[i][baseKey];
		     if( typeof(unique[key]) == "undefined"){
		    	 var obj= new Object();
		    	 obj.key = key;
		    	 obj.index= i;
		      distinct.push(obj);
		     }
		     unique[key] = 0;
		    }
		return distinct ;
	};
   
	/*
	if(isWhsTabOpen)
	{
		$scope.currentTab = 'warehouse.html';}
	else
	{
		$scope.currentTab = 'overview.html';
	}
 Commento perchè il cliente vuole che venga aperto sempre il tab specifiche*/
	$scope.currentTab = 'specifications.html';
	
	var setWhsTabSettings = function(showWhsTab)
	{
		//ADM-021 start
		//var isShowCatTab = $rootScope.webSettings.showCatalogueBreadcrumb;
		//if(!isNull(isShowCatTab) && isShowCatTab){
		//	isShowCatTab = true;
		//}else{
		//	isShowCatTab = false;
		//}
		var isShowCatTab = false;
		//ADM-021 end
		$scope.productInfoTabs = [{
			title: Util.translate('CON_SPECIFICATIONS',null),
			url: 'specifications.html',
			isEnabled:true
		},{
			title: Util.translate('CON_OVERVIEW',null),
			url: 'overview.html',
			isEnabled:true	
		},{
			title:  Util.translate('CON_WAREHOUSE_DETAILS',null),
			url: 'warehouse.html',
			isEnabled:showWhsTab
		},{
			title: Util.translate('CON_CATALOGUE',null),
			url: 'catalogProdDetail.html',
			isEnabled:isShowCatTab
				
		}];
		
	    
		
	};

	$scope.showWhsTab = function(show){
		//changes to display whstab and to consider restriction
		if(!isNull(show) && show == true){
		show = $rootScope.isWarehouseTabDisplay;
		}
		if(show){
			setWhsTabSettings(show);
			$scope.currentTab = 'warehouse.html';
		}
		
	};
	
	$scope.productInfoTabs = [
	{
		title: Util.translate('CON_SPECIFICATIONS',null),
		url: 'specifications.html',
		isEnabled:true
	},{
		title: Util.translate('CON_OVERVIEW',null),
		url: 'overview.html',
		isEnabled:true
			
	},{
		title:  Util.translate('CON_WAREHOUSE_DETAILS',null),
		url: 'warehouse.html',
		isEnabled:false
	}];
	
	$scope.onClickTab = function (tab) {
		$scope.currentTab = tab.url;
	};

	$scope.isActiveTab = function(tabUrl) {
		return tabUrl == $scope.currentTab;
	};

//	$rootScope.initCommitsPromise.then(function(){
//		$scope.getProductDetail($scope.prdDetail.getParams());
//	});

	// ADM-010
	$scope.$watch('freeSearchObj.packPiece',
		function(){
			refreshUIforPackPiece($scope.freeSearchObj.packPiece);
		}
	);
	
	// ADM-010
	function refreshUIforPackPiece(packOrPiece){
		if($rootScope.webSettings.allowSelectUnitOfMeasure){
			if(Util.isNotNull($scope.productDetails)){
				var item = $scope.productDetails;
				if(!isNull(item.salesPackageQty) && item.salesPackageQty > 1){
					if(packOrPiece == "CF"){
						item.salesUnitDesc = packOrPiece + item.salesPackageQty;
					}else{
						item.salesUnitDesc = packOrPiece + "/" + item.salesPackageQty;
					}
				}else{
					item.salesUnitDesc = packOrPiece;
				}

				if(item.salesUnit != packOrPiece){
					item.salesUnit = packOrPiece;
					item.defaultSalesUnit = item.salesUnit;
					item.defaultSalesUnitDesc = item.salesUnitDesc;
					item.unitCodes = [item.salesUnit];
					item.unitCodesDesc = [{"salesUnit": item.salesUnit, "salesUnitDesc" : item.salesUnitDesc}];
					
					if(!isNull(item.salesPackageQty) && item.salesPackageQty > 1){
						if(packOrPiece == "CF"){
							item.actualPrice = $rootScope.convertToPack(item.actualPrice, item.salesPackageQty);
							if(!isNull(item.discountPrice)){
								item.discountPrice = $rootScope.convertToPack(item.discountPrice, item.salesPackageQty);
							}
						}else{
							item.actualPrice = $rootScope.convertToPiece(item.actualPrice, item.salesPackageQty);
							if(!isNull(item.discountPrice)){
								item.discountPrice = $rootScope.convertToPiece(item.discountPrice, item.salesPackageQty);
							}
						}
					}
					$scope.prdDetail.setPrdBaseDetail(item);
				}
			}
		}
	}
	
	productDetailCtrlInit();
}]);