var ModalLoginCtrl = function ($rootScope,$scope, $modal,Util, $log,authentication,$state,NewConfigService,DataSharingService) {

	$scope.selectedLanguageForLogin = {};
	$rootScope.labelLogin =  authentication.getLoginLabel(); 
	$rootScope.user = authentication.getUserName();
	$scope.user = {
			user: 'User ID',
			password: ''
	};

					$scope.submit = function (username, password,selectedLangForLogin) {
						var paramObj={};
						
						var callState = $state.current.name;
						username = returnBlankIfNull(username);
						password = returnBlankIfNull(password);
						langCode = null;
						if ((!isNull(selectedLangForLogin)) && (!isNull(selectedLangForLogin.code))){			
							langCode = selectedLangForLogin.code;
						}
						//$rootScope.openLoader();
						authentication.userLoginByPost(username,password,langCode).then(function (data) {
							//$rootScope.closeLoader();
							//
							if(!isNull(data) && data.isForcePasswordChange) {
								DataSharingService.setLanguageCode(data.languageCode);
								//closeDailog($modalInstance);
								NewConfigService.changeLanguagePswDlg(data , callState,data.languageCode);
								
							//	$scope.changePwdRequired(data,callState);
							}
							//
							var isAuthenticated =authentication.isUserAuthenticated(data);
							DataSharingService.setLanguageCode(data.languageCode);
							if(!isNull(data) && data.isForceSelectCustomer){
								//NewConfigService.changeLanguage(data.languageCode);
								DataSharingService.setLanguageCode(data.languageCode);
								NewConfigService.changeLanguageCustomerDlg(data.languageCode);
								$scope.selectCustomerRequired(data,callState);
							}else if(isAuthenticated == true){
								NewConfigService.changeLanguage(data.languageCode);
								$rootScope.isSignedIn = true;
								//closeDailog($modalInstance);
								$rootScope.$broadcast('userLoggedIn');
								/*if(callState != "GENERAL_ENQUIRY"){
									$rootScope.resolvePendingSendEmail();
								}*/
								if(callState == 'home.mail'){
									$rootScope.registerSendEmailLogin();
								}
								$rootScope.user = authentication.getUserName();	
								$rootScope.registerWelComeMsg();
								if(callState == CONST_State_Home ){
									$rootScope.reloadHomePage();
								}else if(!isNull(callState)){
									//$rootScope.openLoader();
									
									var transitionInfo = {"callState":callState, "reload":reloadState, "param":params};
									DataSharingService.setToLocalStorage("transitionTo", transitionInfo);
									
									$rootScope.userSettingsLoaded.then(function (){
										//historyManager.loginStatusChanged();
										$state.go(callState,paramObj,{reload: reloadState});
										//$rootScope.closeLoader();
									});	
								}
							}else{
								$rootScope.isSignedIn = false;
								if (!isNull(data) && !isNull(data.errorCode) && data.errorCode==4206){
									$scope.loginMsg = Util.translate('MSG_USER_NO_CUSTOMER');	
								}else if (!isNull(data) && !isNull(data.errorCode) && data.errorCode==4205){
									$scope.loginMsg = Util.translate('MSG_USER_INACTIVE');	
									//ADM-006
								}else if (!isNull(data) && !isNull(data.errorCode) && data.errorCode==9002){
									$scope.loginMsg = Util.translate('MSG_ACCESS_DENIED_FOR_CUSTOMER_RESTRICTION');	
								}else{
									$scope.loginMsg = Util.translate('MSG_INCORRECT_USERID_PASSWORD');	
								}
								
								if (!isNull(data) && !isNull(data.errorCode) && data.errorCode==4602){
									$scope.loginMsgInactive = Util.translate('MSG_USER_WARNING');	
								}
								if (!isNull(data) && !isNull(data.errorCode) && data.errorCode==4603){
									$scope.loginMsgInactive = Util.translate('MSG_USER_INACTIVATE');	
								}
							}
							$rootScope.labelLogin = authentication.getLoginLabel();
						});
					};

	$rootScope.open = function (callState,reloadState,params) {
		if(!isNull($rootScope.selectCustomerDlg)){
			try{
				$rootScope.selectCustomerDlg.close('result');
			}catch(err){

			}
			//console.log("select customer dlg closed");
		}
		
		//if(isNull($rootScope.isLoginPopUPOpen)  || !$rootScope.isLoginPopUPOpen){
			$rootScope.isLoginPopUPOpen = true;
			$modal.open({
				templateUrl: 'loginForm.html',
				backdrop: 'static',
				windowClass: 'modal',
				controller: function ($rootScope,$scope,Util, $modalInstance, $log, user, DataSharingService) {
					
					$scope.user = user;
					var loginHistory = new LoginHistory();
					$scope.username = loginHistory.getLastUser();
					$scope.selectedLanguageForLogin = $scope.selectedLanguage;
					
					$scope.cancel = function () {
						cancelDailog($modalInstance);
					};
					$scope.removeMailAndWelcome = function (){
						$rootScope.removeSendEmailLogin();
						$rootScope.removeWelComeMsg();
					};
					$scope.gotoForgotPassword = function(){
						$rootScope.showForgotPwdDlg();
						$scope.cancel();
					};
					$scope.gotoNewAccount = function(){
						$rootScope.checkNshowNewAccountPopup();
						$scope.cancel();
					};
					
					$scope.changePwdRequired = function(data,callState){
						$rootScope.showChangePwdDlg(data.userID,data.isForcePasswordChange,callState);
					};
					
					$scope.selectCustomerRequired = function(data,callState){
						//	$scope.cancel();
						if(!isNull(data) && !isNull(data.isForceSelectCustomer)){
							if(data.isForceSelectCustomer && data.isAuthenticated){
								$scope.cancel();
								$rootScope.showSelectCustomerDlg(data,callState);
							}
						}
					};
				},
				resolve: {
					user: function () {
						return $scope.user;
					}
				}
			});
		//}

		var closeDailog = function (modalDailog){
			try {
				$rootScope.isLoginPopUPOpen = false;
				modalDailog.close('result');
			}catch(err) { } 		    
			finally {
				$rootScope.isLoginPopUPOpen = false;
			}
		};
		var cancelDailog = function (modalDailog){
			try {
				$rootScope.isLoginPopUPOpen = false;
				modalDailog.dismiss('cancel');
			}catch(err) { } 		    
			finally {
				$rootScope.isLoginPopUPOpen = false;
			}
		};
	};
	
	$rootScope.isOnlinePayAllowed = function (){
		var isOnlinePayActivated = false;
		if(!isNull($rootScope.webSettings) && $rootScope.webSettings.isPaymentModeActivated ){
			//isOnlinePayActivated = true;
			if($rootScope.webSettings.paymentMethod === "ONLINE" || $rootScope.webSettings.paymentMethod === "INVOICE,ONLINE"){
				isOnlinePayActivated = true;
			}else{
				isOnlinePayActivated = false;
			}
		}
		//console.log("isOnlinePayActivated : "+isOnlinePayActivated);
		return isOnlinePayActivated;
	};
	
	$rootScope.loginLogout = function()
	{  	 
		var loginStatus = authentication.isLoggedIn();
		if(loginStatus)
		{
			authentication.closeSession();		
			//$state.go(CONST_State_Home);
			//$rootScope.labelLogin = authentication.getLoginLabel();
			//	$rootScope.user = authentication.getUserName();
		}else{	
			state = $state.current.name;
			$rootScope.open(state);	
		}
		$rootScope.labelLogin = authentication.getLoginLabel();
	};
};