// request search controller
//quotation page controller
catalougeControllers.controller('requestHistoryCtrl', ['$rootScope','$scope', '$http','$log', 'DataSharingService','RequestSearchService', 'Util','$state',
                                                         function ($rootScope,$scope, $http,$log,DataSharingService,RequestSearchService,Util,$state) {
//	new code starts
	
	var requestHistoryCtrlInit = function(){
		if(historyManager.isBackAction){
		historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	//	$log.log("  history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	
	var initController = function()
	{ 
		$rootScope.initCommitsSecondNextPromise.then(function(){
			$scope.sortColumn = "RequestNumber";
			$scope.orderType = "DESC";	
			$scope.isUserSessionActive = true;
			$scope.cachedPaginationObj = null;
			$scope.sort = new Sort();
			$scope.sort.DESC('RequestNumber');
			$scope.filterOn = false;
			$scope.filterList =  null;
			$scope.paginationObj = null;
			$scope.isCollapsed = false;
			$scope.filterFormInfo = {};
			makeRequestSearchFilters();
			var params = getSearchParams(); 		
			getRequestHistoryPageData(params,FirstPage);

		});

				// to be code
	};
	
	var getSearchParams = function(){
		var params  = [];
		if($scope.filterOn){
			params = $scope.sort.addSortParams($scope.filterList.getParamsList());
		}else{
			params = $scope.sort.getParam();
		}
		return params;
	};		
	
	var getRequestHistoryPageDataSorting =  function(requestParams,page,showNoDataMsg){
		$scope.paginationObj = $rootScope.getPaginationObject('requestHistoryUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('requestHistoryUrl',requestParams,page ).then(function(data){
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true,showNoDataMsg);
				}else{
					setUIPageData(data,false,showNoDataMsg);
				}
			});
		}
		if($scope.filterOn){
		if(requestParams[0] == 'ReferenceNumber'){
			$scope.sort.DESC('Reference');
			}else if(requestParams[0]=='RequestNumber'){
				$scope.sort.ASC('RequestNumber');			
			}else if(requestParams[0]=='Customer'){
				$scope.sort.DESC('RequestNumber');
			}else if(requestParams[0]=='YourReference'){
				$scope.sort.DESC('YourReference');
			}else{
				$scope.sort.DESC(requestParams[0]);
			}
		}		
	};
	
	var getSearchParamsSorting = function(){
		var params  = [];
		if($scope.filterOn){
			params = $scope.sort.addSortParamsSorting($scope.filterList.getParamsList());
		}else{
			params = $scope.sort.getParam();
		}
		return params;
	};
	
	var makeRequestSearchFilters = function(){
		//$rootScope.initCommitsSecondNextPromise.then(function(){
			var finalFilterList = new FilterList();
			var staticFilterConfig = RequestSearchService.getFilterConfiguration();
			var filterDataList = DataSharingService.getFilterDetailsSettings();
			 
			var dynamicFilterConfig = null;

			var filterExist = false;
			if(isNull(filterDataList.customerList)){
				filterExist = false;
			}else{
				filterExist = true;
			}
			
			
			if(filterExist ==  false){
				Util.getFilterListDetails([]).then(function(data){
					DataSharingService.setFilterDetailsSettings(data);
					filterDataList = DataSharingService.getFilterDetailsSettings();
					RequestSearchService.getRequestFilterList().then(function(data){
						dynamicFilterConfig = data.filterBeans;
						finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
						$scope.filterList = finalFilterList;
						//console.log("makeRequestSearchFilters : " + JSON.stringify(finalFilterList));	
				});
				});
			} else{
			RequestSearchService.getRequestFilterList().then(function(data){
					dynamicFilterConfig = data.filterBeans;
					finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
					$scope.filterList = finalFilterList;
					//console.log("makeRequestSearchFilters : " + JSON.stringify(finalFilterList));	
			});
		//});
			}
	};
	
	$scope.submitFilter = function(){
		if($scope.filterList.hasFilterTextValues()){
			$scope.sort.ASC('RequestNumber');
		}else{
			$scope.sort.DESC('RequestNumber');
		}
		$scope.filterOn = true;
		var params = getSearchParamsSorting();		
		getRequestHistoryPageDataSorting(params,FirstPage,true);
	};
	
	var getRequestHistoryPageData =  function(requestParams,page,showNoDataMsg){
		$scope.paginationObj = $rootScope.getPaginationObject('requestHistoryUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('requestHistoryUrl',requestParams,page ).then(function(data){
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true,showNoDataMsg);
				}else{
					setUIPageData(data,false,showNoDataMsg);
				}
			});
		}
	};
	
	
	
	
	var setUIPageData = function(data,isFirstPageData,showNoDataMsg){
		var ItemList = data.requestHistoryBean;
			if(isNull(data)){
				setItemList(null,isFirstPageData); 
				if(isFirstPageData){
					$rootScope.applyPreviousPaginationObject('requestHistoryUrl' , $scope);
				}
				
				
			}else{
				if(isFirstPageData){
					$scope.requestData = data;
				}
				$scope.requestDataExport = data;
				setItemList(ItemList,isFirstPageData,showNoDataMsg); 
				$scope.sortColumn = data.sortColumn;
				$scope.orderType = data.orderBy;
				
				$rootScope.cachePaginationObject('requestHistoryUrl' , ItemList, $scope);
				if(isFirstPageData && (! Array.isArray(ItemList) ||  !ItemList.length > 0 ) ){
					$rootScope.applyPreviousPaginationObject('requestHistoryUrl' , $scope);
				} else {
					if(!isNull(data.moreRecords)) $scope.moreDataArrow = data.moreRecords;
				}
				
				
			}
			$rootScope.setPageLoadMsg(ItemList,isFirstPageData,$scope);
			$rootScope.scrollToId("result_set");
			$scope.$root.$eval();
		};
		
	var setItemList = function(itemList,isFirstPageData,showNoDataMsg){
		if(Array.isArray(itemList) && itemList.length > 0 ){
			$scope.requestHistoryList = itemList;				
			
		}else{
			if(!isFirstPageData){
				$scope.requestHistoryList = [];
			}
		}

		if(showNoDataMsg){
			updateFilterStatus(itemList,isFirstPageData);	
		}
		if(!showNoDataMsg && itemList.length==0){
			$scope.requestHistoryList = itemList;
		}
	};
	
	var updateFilterStatus = function(list,isFirstPageData){
		if(Array.isArray(list) && list.length > 0 && isFirstPageData){
			$scope.filterList.cacheFilters();			
		}else if($scope.filterOn && isFirstPageData){			
			$scope.filterList.applyPreviousFilters();			
			showMsgNoObjFound();
		}
	};
	
	// sorting 
    $scope.sortASC = function(column){
		$scope.sort.ASC(column);
		sorting();
	};
	
	$scope.sortDESC = function(column){
		$scope.sort.DESC(column);
		sorting();
	};
	
	var sorting = function(){
		var params = getSearchParams();		
		getRequestHistoryPageData(params,FirstPage);
	};
	
	$scope.previousPage = function(){		
		 var params = getSearchParams();		
		 getRequestHistoryPageData(params,PrevPage);
	};
	$scope.nextPage = function(){	
		 var params = getSearchParams();		
		 getRequestHistoryPageData(params,NextPage);
	};
			
	$scope.getFirstPage = function()
	{			
		 var params = getSearchParams();		
		 getRequestHistoryPageData(params,FirstPage);
				
	};
	//  Pagination code ends

	
	// old code

//	$scope.sortColumn = "RequestNumber";
//	$scope.orderType = "DESC";
//	$scope.param = [];
//	$scope.filterParam = [];
//	$scope.filterOn = false;	
//	$scope.dataFound = false;
//	$scope.recordFound = true;
//	$scope.propertyList = [];
//	$scope.isUserSessionActive = true;
//	$scope.responseMessageCode = "";
//	var requestParams = [];
//	$scope.callState = $state.current.name;
//	$scope.filterMsg = "";
//
//	requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn); 
//	
//	
//	var getRequestSearchList = function(requestParams)
//	{
//		//requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn); 
//		RequestSearchService.getRequestHistory(requestParams).then(function(data)
//				{
//					if(isNull(data)){$log.log("requestHistoryCtrl --  no data fetched");}
//					else{
//						if(data.requestHistoryBean==null || data.requestHistoryBean.length == 0){
//							$scope.dataFound = false;
//							$scope.requestLoadMsg = " ";
//							$log.log("requestHistoryCtrl --  no data fetched");
//							if($scope.filterOn)
//								{
//									$scope.filterMsg =  Util.translate('MSG_NO_OBJECTS_FOR_SELECTION',null);	
//									showMsgNoObjFound();
//									var icon = '<a class="OrderInformation-icon" shref="" title="'+$scope.filterMsg+'"></a>';				
//									$scope.filterMsg = icon + $scope.filterMsg;
//							
//									$scope.recordFound = false;
//								}
//
//						}
//						else {				
//							
//							$scope.requestHistoryList = data.requestHistoryBean;
//							$scope.requestData = data;
//							$scope.dataFound = true;
//							$scope.requestLoadMsg = "";
//							if($scope.filterOn)
//							{$scope.filterMsg = "";
//							$scope.recordFound = true;}
//							
//							$scope.propertyList = Util.getObjPropList(data.requestHistoryBean[0]);	
//							$log.log("requestHistoryCtrl messageCode -- "+data.messageCode);
//							$log.log("Request Search results ->  " +data.requestHistoryBean.length);
//						}
//					}
//
//				});
//		
//	};
//	
//	getRequestSearchList(requestParams);
//
//	
//	// sorting
//	
//	 $rootScope.sortSelection = '';
//	 $rootScope.sortSelection =  $scope.sortColumn+$scope.orderType;
//	 //$log.log("Request Search sorting-- Current Sorting Selection : " + $scope.Selection);
//
//	$scope.Sorting = function(column,orderBy) {
//			
//			var	requestParams = $rootScope.getSortingParam(column,orderBy);		
//			pageKey = Util.initPagination('requestHistoryUrl',requestParams);
//			
//			getRequestSearchList(requestParams);		
//
//		};
//			
//	//
//		// Pagination code
//	
//		var pageKey = Util.initPagination('requestHistoryUrl',requestParams);
//		var paginationObj = DataSharingService.getObject(pageKey);	
//		
//		//$rootScope.requestHistoryLoadMore = function(page)
//		$scope.getRequestPageData = function(page)
//		{
//			$log.log("requestHistoryLoadMore() - called" );
//			$log.log("requestHistoryLoadMore() - called, isPaginationBlocked = "+ Util.isPaginationBlocked(pageKey).toString());		
//			if(!Util.isPaginationBlocked(pageKey))
//			{	
//				requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn);
//				Util.getPageinationData('requestHistoryUrl',requestParams,page ).then(function(data)
//				{
//					if(data.requestHistoryBean==null || data.requestHistoryBean.length == 0)
//					{	
//						paginationObj.setLastPageNo();
//						$scope.dataFound = false;
//						$scope.requestLoadMsg = Util.translate('CON_NO_MORE_PAGE'); 
//						$log.log("requestHistoryCtrl -- " + $scope.requestLoadMsg);
//					}
//					else
//					{
//						$log.log("requestHistoryCtrl messageCode -- "+data.messageCode);
//					
//						$scope.dataFound = true;
//						$scope.requestLoadMsg = "";						
//						$scope.requestHistoryList = data.requestHistoryBean;
//						$scope.requestData = data;
//						requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn);
//						$scope.$root.$eval();					
//					}			
//					$log.log("paginationObj.isLastPageNo(): " +paginationObj.isLastPageNo());
//				});
//			}
//		};
//		
//		$scope.previousPage = function(){		
//			
//			$scope.getRequestPageData(PrevPage);		
//		};
//		$scope.nextPage = function(){			
//			$scope.getRequestPageData(NextPage);		
//		};
//				
//		$scope.getFirstPage = function()
//		{			
//			$scope.getRequestPageData(FirstPage);
//					
//		};
//		//  Pagination code ends
		
		$rootScope.showRequestReferenceDetail = function(request, refType)
		{
			//DataSharingService.setObject("requestRef_clickedDetail",request);
			DataSharingService.setToSessionStorage("requestRef_clickedDetail",request);
			$rootScope.fromRequestPage = true;
			$rootScope.fromInvoice = false;
			DataSharingService.setToSessionStorage("fromRequestPage",$rootScope.fromRequestPage );
			DataSharingService.setToSessionStorage("fromInvoice",$rootScope.fromInvoice);
			if(refType == Util.translate('COH_ORDER_NUMBER')){
				if($rootScope.isShowDisableMenuItem('CON_ORDER_INFORMATION')){
					var needToLogin = $rootScope.isSignOnRequired('CON_ORDER_INFORMATION','home.order.detail',true);
					if(!needToLogin){
						$state.go('home.order.detail',{"ORDER_NUM":request.reference});
					}
				}
			}

			if(refType == Util.translate('COH_INVOICE')) {	
				if($rootScope.isShowDisableMenuItem('CON_INVOICE_DETAIL')){
				var pageNo = "";

				var reqParamInv = requestParams = ["InvoiceNumber",request.reference,"OrderNumber",request.orderNo,"DocumentType",request.docType,"InvoiceType",1];
				$scope.paginationObj = $rootScope.getPaginationObject('invoiceDetailsUrl',reqParamInv,$scope.pageKey);
				$scope.pageKey = $scope.paginationObj.objKey;
				Util.getPageinationData('invoiceDetailsUrl',reqParamInv,pageNo,false ).then(function(data){
					if(!isNull(data.invoiceLineBeanList)){
						if(data.invoiceLineBeanList.length>0){
							var needToLogin = $rootScope.isSignOnRequired('CON_INVOICE_DETAIL','home.invoice.detail',true);
							if(!needToLogin){
								$state.go('home.invoice.detail');
							}
						}else{
							var msg = '';
							var translatedMsgList = [];
							translatedMsgList.push(Util.translate('MSG_INVOICE_NOT_FOUND'));
							$rootScope.openMsgDlg(msg,translatedMsgList);
						} 
					}
					
				});
				
			}
			}
		};
				
		$rootScope.showRequestNewOrderDetail = function(request, newOrderNumber){
			DataSharingService.setToSessionStorage("requestRef_clickedDetail",request);
			$rootScope.fromRequestPage = true;
			$rootScope.fromInvoice = false;
			DataSharingService.setToSessionStorage("fromRequestPage",$rootScope.fromRequestPage );
			DataSharingService.setToSessionStorage("fromInvoice",$rootScope.fromInvoice);

			if(newOrderNumber !=""|| newOrderNumber !=null){
				var needToLogin = $rootScope.isSignOnRequired('CON_ORDER_INFORMATION','home.order.detail',true);
				if(!needToLogin){
					$state.go('home.order.detail',{"ORDER_NUM":newOrderNumber});
				}
			}
		};
		
		$scope.showRequestDetail = function(request){
			//DataSharingService.setObject("request_clickedDetail",request);	
			if($rootScope.isShowDisableMenuItem('CON_REQUEST_INFORMATION')){
				DataSharingService.setToSessionStorage("request_clickedDetail",request);	
				var needToLogin = $rootScope.isSignOnRequired('CON_REQUEST_INFORMATION','home.request.detail',true);
				if(!needToLogin){	
					$state.go('home.request.detail');
				}
			}
		};
		
		// Exporting request data
		$scope.exportRequestHistory = function(fileType)
		{
			var requestSearchTable = new Table();
			var data;
			var filePrefix = "RequestSearch";
			var title = Util.translate('CON_REQUEST_SEARCH');
			var sheetName = filePrefix ;
			switch(fileType){
			
			case 'pdf' :
						 requestSearchTable = setRequestSearchDataforExport();
						 data = requestSearchTable.getHTML();												
						 var fileName = $rootScope.getExportFileName (filePrefix,".pdf");
						 savePdfFromHtml(data,title,fileName);
						 //console.log("exportRequestTableAsPDF -  table Object "+ JSON.stringify(data));
						 break;
				 
			case 'xls' :
						requestSearchTable = setRequestSearchDataforExport();
						var data = requestSearchTable.getDataWithHeaders();
						var fileName = $rootScope.getExportFileName (filePrefix,".xlsx");						
						//$rootScope.exportTableAsExcel(data,title,fileName);
						saveExcel(data,sheetName,fileName);
						//console.log("exportRequestTableAsExcel -  table Object "+ JSON.stringify(data));
				
				
						break;
			case 'xml' :
						requestSearchTable = setRequestSearchDataforExport('XML');
						requestSearchTable.setRootElementName("REQUEST_SEARCH");
						requestSearchTable.setEntityName("REQUEST_DETAIL");
						var data = requestSearchTable.getXML();
						var fileName = $rootScope.getExportFileName (filePrefix,".xml");						
						saveXML(data,fileName);
						//console.log("exportRequestTableAsXML -  table Object "+ JSON.stringify(data));
						break;
						
			default :
					
				//console.log("Exporting Request Search - failed");
			 
			
			}
			
		};
				
		var setRequestSearchDataforExport = function(target){
			var requestSearchTable = new Table();
			if(isEqualStrings(target,"XML")){
				requestSearchTable.addHeader('ID');
				if($scope.requestData.customerFlag )
				{
					requestSearchTable.addHeader('CUSTOMER');
				}
				requestSearchTable.addHeader('STATUS');
				requestSearchTable.addHeader('REQUEST_TYPE');
				
				requestSearchTable.addHeader('RESOLUTION_TYPE');
				requestSearchTable.addHeader('REFERENCE_TYPE');
				
				requestSearchTable.addHeader('REFERENCE');
				requestSearchTable.addHeader('DATE');		
				
				requestSearchTable.addHeader('HANDLER');
				requestSearchTable.addHeader('YOUR_REFERENCE');
				
			}else{
				requestSearchTable.addHeader(Util.translate('COH_ID'));
				if($scope.requestData.customerFlag )
				{

					requestSearchTable.addHeader(Util.translate('COH_CUSTOMER'));
				}
				requestSearchTable.addHeader(Util.translate('CON_STATUS'));
				requestSearchTable.addHeader(Util.translate('COH_REQUEST_TYPE'));
				
				requestSearchTable.addHeader(Util.translate('CON_RESOLUTION_TYPE'));
				requestSearchTable.addHeader(Util.translate('COH_REFERENCE_TYPE'));
				
				requestSearchTable.addHeader(Util.translate('COH_REFERENCE'));
				requestSearchTable.addHeader(Util.translate('COH_DATE'));
				requestSearchTable.addHeader(Util.translate('CON_HANDLER'));
				requestSearchTable.addHeader(Util.translate('COH_YOUR_REFERENCE'));
				
			}
			//set row data
			
			// 
			
			data = $scope.requestDataExport.requestHistoryBean;
			
			if(!isNull(data)){
				for(var i=0;i<data.length;i++){
					var row=[];
					var references="";
					if(!data[i].refLinkFlag){
						if(!isEmptyString(data[i].docType)){
							references=data[i].docType;
						}
						references=references+data[i].reference;
					}
					row.push(data[i].requestId);
					if($scope.requestData.customerFlag ){			
						row.push(data[i].customer);
					}
					row.push(data[i].status);
					row.push(data[i].requestType);
					row.push(data[i].resolutionType);
					row.push(data[i].refType);
					row.push(data[i].reference);
					row.push(data[i].date);
					row.push(data[i].handler);
					row.push(data[i].yourReference);
					requestSearchTable.addRow(row);
				}
			}else{
				var row = [];			
				for(var i=0;i<requestSearchTable.headers.length;i++){
					row.push("");
				}
				requestSearchTable.addRow(row);
			}
			
			//console.log("request export table  "+ JSON.stringify(requestSearchTable));
			return requestSearchTable;
		};
		// Exporting request data ends
		// Request Filter Starts
//		$rootScope.filterDetailSettings = DataSharingService.getFilterDetailsSettings();
//		
//		var makeRequestSearchFilters = function(){
//			$rootScope.initCommitsSecondNextPromise.then(function(){
//				var finalFilterList = new FilterList();
//				var staticFilterConfig = RequestSearchService.getFilterConfiguration();
//				var filterDataList = DataSharingService.getFilterDetailsSettings();
//				 
//				var dynamicFilterConfig = null;
//				RequestSearchService.getRequestFilterList().then(function(data){
//						dynamicFilterConfig = data.filterBeans;
//						finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
//						$scope.requestSearchFilterList = finalFilterList;
//						$log.log("makeRequestSearchFilters : " + JSON.stringify(finalFilterList));	
//				});
//			});
//			
//		};
		
//		makeRequestSearchFilters();
//		
//		//
//	//	$scope.filterFormInfo = {};
//		$scope.filterMsg = "";
//		$scope.submitFilter = function()
//		{
//			$scope.filterOn = true;
//			$scope.filterParam  = $scope.requestSearchFilterList.getParamsList();		
//			var filterParam =  $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn); 
//			$log.log("requestHistoryUrl filterParam--  " + filterParam);
//			
//			pageKey = Util.initPagination('requestHistoryUrl',filterParam);
//			
//			getRequestSearchList(filterParam);
//			
//			$rootScope.scrollToId("result_set");	
//		};
//		
		var confrimDeleteRequest = function(requestId){
			var msg = Util.translate('CONF_DELETE_REQUEST',null);
			var reqNum = Util.translate('CON_REQUEST_NUMBER') + " "+requestId;
			var mode = $rootScope.openConfirmDlg(msg , [reqNum]);
			mode.result.then(function () {
				//console.log('Delete request modal- Ok clicked');
				deleteRequest(requestId);
			}, function () {
				//console.log('Delete request modal dismissed at: ' + new Date());
			});
		};
		
		var showDeleteRequestError= function(){
			var msg = '';
			var translatedMsgList = [];
			translatedMsgList.push(Util.translate('MSG_UNEXPECTED_ERROR_OCCURED_WHEN_DELETING_REQUEST'));
			$rootScope.openMsgDlg(msg,translatedMsgList);
		};
		
		var deleteRequest =  function(requestId){
			if(!isNull(requestId)){
				var params = ["requestId",requestId];
				//console.log('deleteRequest() called');
				RequestSearchService.deleteBPRequest(params).then(function(data){
					handleDeleteRequestMsgCode(data);
				});
			}
		};
		
		var handleDeleteRequestMsgCode = function(data){
			if(!isNull(data) && !isNull(data.messageCode)){
				switch(data.messageCode){
				case 4200: 
					//Successfully Deleted
//					var requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn); 
//					getRequestSearchList(requestParams);
					var params = getSearchParams(); 		
					getRequestHistoryPageData(params,FirstPage);
					break;
				case 4201: 
					// Error while deleteing request
					showDeleteRequestError();
				}
			}
		};
		
		$scope.deleteRequestAction =  function(requestId){
			if(!isNull(requestId)){
				confrimDeleteRequest(requestId);
			}
		};
		requestHistoryCtrlInit();
		// Request Filter Ends
}]);