catalougeControllers.controller('invoiceDetailCtrl', ['$rootScope','$scope', '$http','$log', 'DataSharingService','InvoiceService', 'Util','$state',
                                                      function ($rootScope,$scope, $http,$log,DataSharingService,InvoiceService,Util,$state) {
	//	new code
	var showLoading = true;
	var invoiceDetailCtrlInit = function(){
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
		$rootScope.requestLineSelectionMsg = null;
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	//	$log.log(" invoiceDetailCtrl Init history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	var initController = function()
	{ 
//		$rootScope.initCommitsSecondNextPromise.then(function(){
		$scope.isUserSessionActive = true;
		$scope.cachedPaginationObj = null;
		$scope.paginationObj = null;
		$scope.isCollapsed = false;
		if(webSettings == undefined){
			setWebSettingObject(DataSharingService.getWebSettings());
		}	
		$scope.callState = $state.current.name;
		if($scope.callState == REQ_SUBMIT_INVOICE_DETAIL_VIEW){
			showLoading = false;
		}
		//var invoice = DataSharingService.getObject("invoice_clickedDetail");
		var invoice =DataSharingService.getFromSessionStorage("invoice_clickedDetail");
		if(!isNull(invoice)){	$scope.isMergeInvoice = invoice.mergeInvoiceFlag;}
		
		var params = getRequestParams();
		getInvoiceDetailPageData(params,FirstPage,showLoading);
//		});
		
	};
	
	var getInvoiceDetailPageData =  function(requestParams,page,showLoading){
		$scope.paginationObj = $rootScope.getPaginationObject('invoiceDetailsUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		var pageNo = "";
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('invoiceDetailsUrl',requestParams,pageNo,showLoading ).then(function(data){
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true);
				}else{
					setUIPageData(data,false);
				}
			});
		}
	};
	var setUIPageData = function(data,isFirstPageData){
		var ItemList = data.invoiceLineBeanList;
		if(isNull(data)){
			setInvoiceLineList(null,isFirstPageData); 
			if(isFirstPageData){
				$rootScope.applyPreviousPaginationObject('invoiceDetailsUrl' , $scope);
			}

		}else{
			if(angular.isDefined(data.invoiceLineBeanList) && !isNull(data.invoiceLineBeanList)){
				setInvoiceDetailExtendedData(data.invoiceLineBeanList);
				//$scope.invoiceLineData = data.invoiceLineBeanList;	
				ItemList =  data.invoiceLineBeanList;
				$scope.initBuyObject(ItemList);
				$scope.propertyList = Util.getObjPropList(data.invoiceLineBeanList[0]);	
				addpropertyToList(ItemList,'validQuantity',true);
				//console.log("invoiceDetailCtrl : invoiceLineData length--  " +ItemList.length);

			}
			$scope.invoiceDetails = data;
			$rootScope.changeHomePageTitle(Util.translate('TXT_PAGE_TITLE_INVOICE_WITH_NUMBER') + $scope.invoiceDetails.invoiceNumber, true);
			$scope.invoiceAddressList = data.invoiceAddressList;
			$scope.currency = $scope.invoiceDetails.primeCurrency;		
			setInvoiceLineList(ItemList,isFirstPageData); 
			$scope.invoiceDetailsPropList = Util.getObjPropList(data);	
			$rootScope.cachePaginationObject('invoiceDetailsUrl' , ItemList, $scope);
			if(isFirstPageData && (! Array.isArray(ItemList) ||  !ItemList.length > 0 )){
				$rootScope.applyPreviousPaginationObject('invoiceDetailsUrl' , $scope);
			}

		}
		$rootScope.setPageLoadMsg(ItemList,isFirstPageData,$scope);
		//$log.log("object info : "+JSON.stringify($scope.invoiceDetails));
		$scope.$root.$eval();
	};

	var setInvoiceLineList = function(itemList,isFirstPageData){
		if(Array.isArray(itemList) && itemList.length > 0 ){
			$scope.invoiceLineData = itemList;				

		}else{
			if(!isFirstPageData){
				$scope.invoiceLineData = [];
			}
		}
	};
	//
	// making invoice detail web service paramaters
	var getInvoiceParams = function(invoice){
		var requestParams = [];
		if(!isNull(invoice)){
			requestParams  = ["InvoiceNumber",invoice.invoiceNumber,"OrderNumber",invoice.orderNumber,"DocumentType",invoice.documentType,"InvoiceType",invoice.typeCode,"InvoiceYear",invoice.invoiceYear];
			//console.log("invoice detail "+ JSON.stringify(invoice));
			//console.log("invoice detail requestParams"+ JSON.stringify(requestParams));	
		}
		return requestParams;
	};
	var getRequestInvoiceParams = function(requestInvoice){
		var requestParams = [];
		if( !isNull(requestInvoice)){
			$scope.invoice = requestInvoice;
			//requestParams = ["InvoiceNumber",requestInvoice.reference,"OrderNumber",requestInvoice.orderNo,"DocumentType",requestInvoice.docType];
			requestParams = ["InvoiceNumber",requestInvoice.reference,"OrderNumber",requestInvoice.orderNo,"DocumentType",requestInvoice.docType,"InvoiceType",1];
		//	requestParams = ["InvoiceNumber",transactionInvoice.documentNumber,"DocumentType",transactionInvoice.documentTypeCode,"OrderNumber",transactionInvoice.orderNumber,"InvoiceYear",transactionInvoice.invoiceYear];		
			//console.log("Request invoice detail "+ JSON.stringify(requestInvoice));
			//console.log("Request invoice detail requestParams"+ JSON.stringify(requestParams));
		}
		return requestParams;
	};
	var getTransactionInvoiceParams = function(transactionInvoice){
		var requestParams = []; 
		
		if(!isNull(transactionInvoice))
		{
			$scope.invoice = transactionInvoice;
			requestParams = ["InvoiceNumber",transactionInvoice.documentNumber,"DocumentType",transactionInvoice.documentTypeCode];
			//	requestParams = ["InvoiceNumber",transactionInvoice.documentNumber,"DocumentType",transactionInvoice.documentTypeCode,"OrderNumber",transactionInvoice.orderNumber,"InvoiceYear",transactionInvoice.invoiceYear];		
			//console.log("transaction invoice detail "+ JSON.stringify(transactionInvoice));
			//console.log("transaction invoice detail requestParams"+ JSON.stringify(requestParams));
		};
		return requestParams;
	};
	var getRequestParams = function(){
		var requestParams = [];
		//var transactionInvoice = DataSharingService.getObject("transaction_clickedDetail");
		var transactionInvoice = DataSharingService.getFromSessionStorage("transaction_clickedDetail");
		var requestInvoice = DataSharingService.getFromSessionStorage("requestRef_clickedDetail");
		//var requestInvoice = DataSharingService.getObject("requestRef_clickedDetail");
		//var invoice = DataSharingService.getObject("invoice_clickedDetail");
		var invoice =DataSharingService.getFromSessionStorage("invoice_clickedDetail");
		$rootScope.fromInvoice = DataSharingService.getFromSessionStorage("fromInvoice");
		$rootScope.fromRequestPage = DataSharingService.getFromSessionStorage("fromRequestPage");
		
		if($rootScope.fromInvoice){
			requestParams =	getInvoiceParams(invoice);
		}
		else{
			if(!$rootScope.fromRequestPage){
				requestParams =	getTransactionInvoiceParams(transactionInvoice);
			}
			if($rootScope.fromRequestPage){
				requestParams =	getRequestInvoiceParams(requestInvoice);
			}
		}
		return requestParams;
	};	
	$scope.previousPage = function(){		
		 var params = getRequestParams();		
		 getInvoiceDetailPageData(params,PrevPage,false);
	};
	$scope.nextPage = function(){	
		 var params = getRequestParams();		
		 getInvoiceDetailPageData(params,NextPage,false);
	};
			
	$scope.getFirstPage = function()
	{			
		 var params = getRequestParams();		
		 getInvoiceDetailPageData(params,FirstPage,false);
				
	};
	
	// old code
//	var requestParams = [];
//	$scope.dataFound = false;
//	$scope.resultMsg ="";
//	$scope.errorMessage = "";
//	$scope.propertyList = [];
//	$scope.callState = $state.current.name;
//	var showLoading = true;
//	if($scope.callState == REQ_SUBMIT_INVOICE_DETAIL_VIEW){
//		showLoading = false;	}
//	else
//		{
//			showLoading = true;
//		};

	
//	//DataSharingService.setObject("pageState",$state.current.name);
//	var invoice = DataSharingService.getObject("invoice_clickedDetail");
//	if(!isNull(invoice)){
//	$scope.isMergeInvoice = invoice.mergeInvoiceFlag;}
//	var transactionInvoice = DataSharingService.getObject("transaction_clickedDetail");
//	var requestInvoice = DataSharingService.getObject("requestRef_clickedDetail");
//	if($rootScope.fromInvoice)
//		{
//			if( invoice !=null){
//				$scope.invoice = invoice;
//				requestParams = ["InvoiceNumber",invoice.invoiceNumber,"OrderNumber",invoice.orderNumber,"DocumentType",invoice.documentType,"InvoiceType",invoice.typeCode,"InvoiceYear",invoice.invoiceYear];
//				$log.log("invoice detail "+ JSON.stringify(invoice));
//				$log.log("invoice detail requestParams"+ JSON.stringify(requestParams));
//			};
//		}
//	else
//		{
//		   if(!$rootScope.fromRequestPage)
//			   {
//			   
//			   	if( transactionInvoice !=null)
//				{
//					$scope.invoice = transactionInvoice;
//					requestParams = ["InvoiceNumber",transactionInvoice.documentNumber,"DocumentType",transactionInvoice.documentTypeCode];
//				//	requestParams = ["InvoiceNumber",transactionInvoice.documentNumber,"DocumentType",transactionInvoice.documentTypeCode,"OrderNumber",transactionInvoice.orderNumber,"InvoiceYear",transactionInvoice.invoiceYear];		
//					$log.log("transaction invoice detail "+ JSON.stringify(transactionInvoice));
//					$log.log("transaction invoice detail requestParams"+ JSON.stringify(requestParams));
//				};
//			   
//			   }
//			if($rootScope.fromRequestPage)
//				{
//				if( requestInvoice !=null)
//				{
//					$scope.invoice = requestInvoice;
//					requestParams = ["InvoiceNumber",requestInvoice.reference,"OrderNumber",requestInvoice.orderNo,"DocumentType",requestInvoice.docType];
//				//	requestParams = ["InvoiceNumber",transactionInvoice.documentNumber,"DocumentType",transactionInvoice.documentTypeCode,"OrderNumber",transactionInvoice.orderNumber,"InvoiceYear",transactionInvoice.invoiceYear];		
//					$log.log("Request invoice detail "+ JSON.stringify(requestInvoice));
//					$log.log("Request invoice detail requestParams"+ JSON.stringify(requestParams));
//				};
//				
//				}
//		}
//		
//	
//
//
//	InvoiceService.getInvoiceDetailList(requestParams,showLoading).then(function(data){
//
//		$log.log("invoiceDetailCtrl : error message --  " + data.errorMessage);
//		//if(data.invoiceLineBeanList==null || data.invoiceLineBeanList.length == 0){
//		if(isNull(data)){
//			$scope.errorMessage = data.errorMessage;	
//		}
//		else
//		{
//			if(angular.isDefined(data.invoiceLineBeanList) && !isNull(data.invoiceLineBeanList))
//				{
//				setInvoiceDetailExtendedData(data.invoiceLineBeanList);
//				$scope.invoiceLineData = data.invoiceLineBeanList;	
//				$scope.initBuyObject($scope.invoiceLineData);
//				$scope.propertyList = Util.getObjPropList(data.invoiceLineBeanList[0]);	
//				addpropertyToList(data.invoiceLineBeanList,'validQuantity',true);
//				$log.log("invoiceDetailCtrl : invoiceLineData length--  " +data.invoiceLineBeanList.length);
//				}
//			
//			$scope.invoiceDetails = data;
//			$scope.invoiceAddressList = data.invoiceAddressList;
//			$scope.currency = $scope.invoiceDetails.primeCurrency;			
//			$scope.errorMessage = " ";	
//			$scope.dataFound = true;
//			
//			$scope.invoiceDetailsPropList = Util.getObjPropList(data);			
//			
//			$log.log("invoiceDetailCtrl : invoiceDetails length--  " +data.length);
//		}
//
//	});
//
//	//Pagination
//	var pageKey = Util.initPagination('invoiceDetailsUrl',requestParams);
//
//
//	$rootScope.getInvoiceDetailPageData = function(page){
//		$log.log("getInvoiceDetailPageData() - called, isPaginationBlocked = "+ Util.isPaginationBlocked(pageKey).toString());
//		if(!Util.isPaginationBlocked(pageKey)){	
//			$log.log("getInvoiceDetailPageData() - called" );
//
//			requestParams = ["InvoiceNumber",$scope.invoice.invoiceNumber,"OrderNumber",$scope.invoice.orderNumber,"DocumentType",$scope.invoice.documentType,"InvoiceType",$scope.invoice.typeCode,"InvoiceYear",$scope.invoice.invoiceYear];
//			Util.getPageData('invoiceDetailsUrl',requestParams ).then(function(data){
//				if(data.invoiceLineBeanList==null || data.invoiceLineBeanList.length == 0){
//					Util.stopLoadingPageData(pageKey);
//					$scope.dataFound = false;
//				}else{
//					$scope.dataFound = true;
//					setInvoiceDetailExtendedData(data.invoiceLineBeanList);
//					Util.appendItemsToList(data.invoiceLineBeanList, $scope.invoiceLineData);
//					$scope.initBuyObject($scope.invoiceLineData);
//					requestParams = ["InvoiceNumber",$scope.invoice.invoiceNumber,"OrderNumber",$scope.invoice.orderNumber,"DocumentType",$scope.invoice.documentType,"InvoiceType",$scope.invoice.typeCode,"InvoiceYear",$scope.invoice.invoiceYear];
//					$scope.$root.$eval();
//				}
//			});
//		}
//	};
//	
//	$scope.previousPage = function(){		
//		
//		$scope.getInvoiceDetailPageData(PrevPage);		
//	};
//	$scope.nextPage = function(){			
//		$scope.getInvoiceDetailPageData(NextPage);		
//	};
//			
//	$scope.getFirstPage = function()
//	{			
//		$scope.getInvoiceDetailPageData(FirstPage);
//				
//	};
//
	$scope.isListEmpty = function(list)
	{
		var isEmpty = false;	
		if( list == null)
		{ isEmpty = true;
		$scope.resultMsg = "No information found...";
		}
		return isEmpty;
	};

//	$scope.isCollapsed = false;
	$scope.toggleCollapse = function()
	{
		var isCollapsed = !$scope.isCollapsed;
		return isCollapsed;
	};	

	$scope.showInvoiceLineDetail = function(invoiceLine){
		if($rootScope.isShowDisableMenuItem('CON_INVOICE_LINE_DETAIL')){
		//DataSharingService.setObject("invoiceLine_clickedDetail",invoiceLine);	
		DataSharingService.setToSessionStorage("invoiceLine_clickedDetail",invoiceLine);	
		var needToLogin = $rootScope.isSignOnRequired("CON_INVOICE_LINE_DETAIL",'home.invoiceline',true);
		if(!needToLogin){	
			$state.go('home.invoiceline');
		}	
		}
	};

	$scope.isColExist = function(colPropName,propertyList){
		var isCol = false;
		var val = Util.isPropertyExist(colPropName,propertyList);
		if(val){
			isCol = true;

		}
		return isCol;
	};
	
	$scope.initBuyObject = function(list){
		$scope.buyObj = new Buy(list);
		$scope.buyObj.setPropNameItemCode('item');
		$scope.buyObj.setPropNameOrdered('newQuantity');
		$scope.buyObj.setPropNameUnit('unit');
		$scope.buyObj.setPropNameShipmentMarking('shipmentMarking');
		$scope.buyObj.setPropNameDelDate('hereDelDateNotRequired');
		$scope.buyObj.setPropNameisBuyAllowed('isBuyAllowed');
	};
	
	$scope.invoiceDetailBuyItems = function(item){
		$rootScope.buyMultipleItems(item,$scope.buyObj);
	};
	
	$scope.addMultipleItemsToCurrentList = function(item){
		$rootScope.addMultipleItemsToList(item,$scope.buyObj);
	};
	
	var setInvoiceDetailExtendedData = function(list){
		//$log.log("setHomeCatalogExtendedData : product list : " +JSON.stringify($scope.productList));
		if(Array.isArray(list)){
			for (var i=0;i<list.length ; i++){
				var  item  = list[i];
				item.showAvailabilityAsLink = $rootScope.showAvailabilityAsLink();
				item.showAvailabilityAsImage = $rootScope.showAvailabilityAsImage();
				item.availabilityImageName = $rootScope.getAvailabilityImage(item.availabilityOfItem);
				item.availabilityText = $rootScope.getAvailabilityText(item.availabilityOfItem);
				item.showEnquiryImage = $rootScope.showEnquiryImage(item.enquiryImage);
				item.enquiryImage = $rootScope.getEnquiryImageName(item.enquiryImage);
				handleEmptyAvailabilityImage(item);
			}
		}
	};

	$scope.showOrderReferences = function(){ 
		var needToLogin = $rootScope.isSignOnRequired('CON_ORDER_REFERENCE',null,false);	
		if(!needToLogin){
			$scope.collapsed_orderInformation= !$scope.collapsed_orderInformation;
		}
	};
	
$scope.exportInvoiceLineTableAsPDF = function(){
		
		var invoiceLines = getinvoiceLinesForExport($scope.invoiceDetails);
		var basicInfo = getBasicInfoForExport($scope.invoiceDetails);
		var orderReferences = getOrderReferencesForExport($scope.invoiceDetails);
		var orderText,orderFees;
		var addresses = getAddressesForExport($scope.invoiceDetails);
		var fileName = "InvoiceDetail_"+$scope.invoiceDetails.orderNumber+"_"+getCurrentDateTimeStr()+".pdf";
		var docHeader = Util.translate('CON_INVOICE_NUMBER') + " " + $scope.invoiceDetails.invoiceNumber;
		//saveDetailsPDF(invoiceLines,basicInfo,orderText,orderReferences,orderFees,addresses,docHeader,fileName);
		// ADM-012 
		// invoiceDownload(invoiceLines,basicInfo,orderText,orderReferences,orderFees,addresses,docHeader,fileName);
		var base64 = $scope.invoiceDetails.pdfFile;
		
		var data = base64;
		
		if (window.navigator && window.navigator.msSaveOrOpenBlob) { // IE workaround
		    var byteCharacters = atob(data);
		    var byteNumbers = new Array(byteCharacters.length);
		    for (var i = 0; i < byteCharacters.length; i++) {
		        byteNumbers[i] = byteCharacters.charCodeAt(i);
		    }
		    var byteArray = new Uint8Array(byteNumbers);
		    var blob = new Blob([byteArray], {type: 'application/pdf'});
		    window.navigator.msSaveOrOpenBlob(blob, fileName);
		}
		else { // much easier if not IE
			download(base64, fileName, "application/pdf");
		}

		//download(base64, docHeader + "pdf", "application/pdf");
		// ADM-012 end
	};

	var getBasicInfoForExport = function(invoiceDetails){
		var basicInfo = new Table();
		basicInfo.heading = Util.translate('CON_BASIC_INFORMATION') ; 
		var row = [];
		row.push(Util.translate('COH_CUSTOMER'));
		row.push(invoiceDetails.customerCode);
		row.push(invoiceDetails.customerDesc);
		row.push("");
		row.push("");
		basicInfo.addRow(row);

		row = [];
		
		row.push(Util.translate('CON_DOCUMENT_DATE'));
		row.push(invoiceDetails.documentDate);
		basicInfo.addRow(row);

		row = [];
		row.push(Util.translate('COH_CURRENCY'));
		row.push(invoiceDetails.primeCurrency);
		
		basicInfo.addRow(row);

		
		row = [];
		if(!isNull(invoiceDetails.amountExcludingVAT)){
			row.push(Util.translate('CON_INVOICE_VALUE_EXCLUDING_VAT'));
			row.push(invoiceDetails.amountExcludingVAT);
		}
		row.push("");
		
		if(!isNull(invoiceDetails.amountIncludingVAT)){
			row.push(Util.translate('CON_INVOICE_VALUE_INCLUDING_VAT'));
			row.push(invoiceDetails.amountIncludingVAT);
		}
		
		basicInfo.addRow(row);

		

		if(!isNull(invoiceDetails.goodsMarking)){
			row = [];
			row.push(Util.translate('COH_GOODS_MARKING'));
			row.push(invoiceDetails.goodsMarking);
			row.push("");
			row.push("");
			row.push("");
			basicInfo.addRow(row);
		}

		if(!isNull(invoiceDetails.motCode)){
			row = [];
			row.push(Util.translate('CON_MANNER_OF_TRANSPORT'));
			row.push(invoiceDetails.motCode);
			row.push(invoiceDetails.motDesc);
			row.push("");
			row.push("");
			basicInfo.addRow(row);
		}
		if(!isNull(invoiceDetails.todCode)){
			row = [];
			row.push(Util.translate('CON_TERMS_OF_DELIVERY'));
			row.push(invoiceDetails.todCode);
			row.push(invoiceDetails.todDesc);
			row.push("");
			row.push("");
			basicInfo.addRow(row);
		}
		if(!isNull(invoiceDetails.topCode)){
			row = [];
			row.push(Util.translate('CON_TERMS_OF_PAYMENT'));
			row.push(invoiceDetails.topCode);
			row.push(invoiceDetails.topDesc);
			row.push("");
			row.push("");
			basicInfo.addRow(row);
		}
		return basicInfo;
	};


	var getinvoiceLinesForExport = function(invoiceDetails){
		var invoiceLines = new Table();
		invoiceLines.heading = Util.translate('CON_INVOICE_LINE');
		
		invoiceLines.addHeader(Util.translate('COH_LINE'));
		invoiceLines.addHeader(Util.translate('COH_PRODUCT'));
		invoiceLines.addHeader(Util.translate('COH_DESCRIPTION'));
		invoiceLines.addHeader(Util.translate('COH_ORDERED_QUANTITY'));
		invoiceLines.addHeader(Util.translate('COH_UNIT'));
		invoiceLines.addHeader(Util.translate('CON_PRICE'));
		invoiceLines.addHeader(Util.translate('CON_VALUE'));
		invoiceLines.addHeader(Util.translate('COH_DELIVERY_DATE'));
		invoiceLines.addHeader(Util.translate('COH_SHIPMENT_MARKING'));
		

		if(!isNull($scope.invoiceLineData)){
			for(var i=0;i< $scope.invoiceLineData.length;i++){
				var row = $scope.invoiceLineData[i];
				var colData = [];
				colData.push(row.invoiceLineNumber);
				colData.push(row.item);
				
				colData.push(row.itemDescription);
				colData.push(row.quantity);
				colData.push(row.unit);
				colData.push(row.salesPrice);
				colData.push(row.amount);
				colData.push(row.dispatchDate);
				colData.push(row.shipmentMarking);
			
				invoiceLines.addRow(colData);
			}
		}
		else{
			var colData = [];
			
				for(var i=0;i<invoiceLines.headers.length;i++){
					colData.push("");
				}
				invoiceLines.addRow(colData);
		}
		
		return invoiceLines;
	};	
	
	
	
	var getOrderReferencesForExport = function(invoiceDetails){
		var orderReferences = new Table();

		orderReferences.heading = Util.translate('CON_ORDER_REFERENCES') ; 

		var row = [];
		row.push(Util.translate('CON_ORDER_NUMBER'));
		row.push(invoiceDetails.orderNumber);
		row.push("");
		row.push(Util.translate('CON_ORDER_DATE'));
		row.push(invoiceDetails.orderDate);
		orderReferences.addRow(row);

		row = [];
		row.push(Util.translate('CON_HANDLER'));
		row.push(invoiceDetails.handlerCode);
		row.push(invoiceDetails.handlerDesc);
		orderReferences.addRow(row);
		
		row = [];
		row.push(Util.translate('CON_SALES_PERSON'));
		row.push(invoiceDetails.salespersonCode);
		row.push(invoiceDetails.salespersonDesc);
		orderReferences.addRow(row);

		if(!isNull(invoiceDetails.orderReference)){
			row = [];
			row.push(Util.translate('CON_ORDER_REFERENCES'));
			row.push(invoiceDetails.orderReference);
			row.push("");
			orderReferences.addRow(row);
		}

		if(!isNull(invoiceDetails.yourReference)){
			row = [];
			row.push(Util.translate('CON_YOUR_REFERENCE'));
			row.push(invoiceDetails.yourReference);
			row.push("");
			orderReferences.addRow(row);
		}

		return orderReferences;
	};

	var getAddressesForExport = function(invoiceDetails){
		var confirmAddresses = new Table(); 
		confirmAddresses.heading = Util.translate('CON_ADDRESSES') ; 

		confirmAddresses.addHeader(Util.translate('CON_ADDRESSES'));
		//orderAddresses.addHeader(Util.translate('CON_DELIVERY_ADDRESS'));

		var row = [];
		row.push(invoiceDetails.address);
	
		confirmAddresses.addRow(row);
		return confirmAddresses;
	};
	
	/* used for downloading pdf from pdfmake */
	var invoiceDownload = function(invoiceLines,basicInfo,orderText,orderReferences,orderFees,addresses,docHeader,fileName){
		var hr = [];
		invoiceLines.headers.forEach(function(value){
			hr.push({ 'text': value, 'style': 'tableHeader' });
		});
		var invoiceTableBody = [];
		invoiceTableBody.push(hr);
		invoiceLines.rows.forEach(function(row){
			var invoiceDataRow = [];
			row.forEach(function(value){
				invoiceDataRow.push(value);
			});
			invoiceTableBody.push(invoiceDataRow);
		});
		basicInfoTableBody = [];
		basicInfo.rows.forEach(function(row){
			var basicInfoDataRow = [];
			for(var i = 0; i<5; i++){
				if(row[i] != undefined){
					basicInfoDataRow.push({'text':row[i],'width':'*'})
				}else{
					basicInfoDataRow.push({'text':'','width':'*'})
				}
			}
			basicInfoTableBody.push(basicInfoDataRow);
		});
		orderReferencesTableBody = [];
		orderReferences.rows.forEach(function(row){
			var orderReferencesDataRow = [];
			for(var i = 0; i<5; i++){
				if(row[i] != undefined){
					orderReferencesDataRow.push({'text':row[i],'width':'*'})
				}else{
					orderReferencesDataRow.push({'text':'','width':'*'})
				}
			}
			orderReferencesTableBody.push(orderReferencesDataRow);
		});
	
		var hr = [];
		addresses.headers.forEach(function(value){
			hr.push({ 'text': value, 'style': 'tableHeader' });
		});
		var addressesTableBody = [];
		addressesTableBody.push(hr);
		addresses.rows.forEach(function(row){
			var addressesDataRow = [];
			row.forEach(function(value){
				value = value.replace(new RegExp('</br>', 'g'), ',');;
				addressesDataRow.push(value);
			});
			addressesTableBody.push(addressesDataRow);
		});
				//image:'../images/NetStore_Logo.png' ,//'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKkAAABNCAYAAADdAES7AAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAADx0aVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/Pgo8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjUtYzAyMSA3OS4xNTU3NzIsIDIwMTQvMDEvMTMtMTk6NDQ6MDAgICAgICAgICI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIKICAgICAgICAgICAgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iCiAgICAgICAgICAgIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiCiAgICAgICAgICAgIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIKICAgICAgICAgICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgICAgICAgICAgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iPgogICAgICAgICA8eG1wOkNyZWF0b3JUb29sPkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE0IChXaW5kb3dzKTwveG1wOkNyZWF0b3JUb29sPgogICAgICAgICA8eG1wOkNyZWF0ZURhdGU+MjAxNS0wMS0xOVQxNTo1MToxNyswNTozMDwveG1wOkNyZWF0ZURhdGU+CiAgICAgICAgIDx4bXA6TWV0YWRhdGFEYXRlPjIwMTUtMDEtMTlUMTU6NTE6MTcrMDU6MzA8L3htcDpNZXRhZGF0YURhdGU+CiAgICAgICAgIDx4bXA6TW9kaWZ5RGF0ZT4yMDE1LTAxLTE5VDE1OjUxOjE3KzA1OjMwPC94bXA6TW9kaWZ5RGF0ZT4KICAgICAgICAgPHhtcE1NOkluc3RhbmNlSUQ+eG1wLmlpZDo5NzBkODQ1Zi01ODNkLTE0NDctYWRkZC01Y2M4ZmM4YjRmMTE8L3htcE1NOkluc3RhbmNlSUQ+CiAgICAgICAgIDx4bXBNTTpEb2N1bWVudElEPmFkb2JlOmRvY2lkOnBob3Rvc2hvcDpkZDg2MWZiMC05ZmM0LTExZTQtOTFjNS1lZTdkN2MwYzMwNGM8L3htcE1NOkRvY3VtZW50SUQ+CiAgICAgICAgIDx4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ+eG1wLmRpZDpjNWM0ODYzMy1iYzUyLWEzNDItOGRhOS04NDk0MGMwNDg0NDU8L3htcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD4KICAgICAgICAgPHhtcE1NOkhpc3Rvcnk+CiAgICAgICAgICAgIDxyZGY6U2VxPgogICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmFjdGlvbj5jcmVhdGVkPC9zdEV2dDphY3Rpb24+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDppbnN0YW5jZUlEPnhtcC5paWQ6YzVjNDg2MzMtYmM1Mi1hMzQyLThkYTktODQ5NDBjMDQ4NDQ1PC9zdEV2dDppbnN0YW5jZUlEPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6d2hlbj4yMDE1LTAxLTE5VDE1OjUxOjE3KzA1OjMwPC9zdEV2dDp3aGVuPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6c29mdHdhcmVBZ2VudD5BZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cyk8L3N0RXZ0OnNvZnR3YXJlQWdlbnQ+CiAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmFjdGlvbj5zYXZlZDwvc3RFdnQ6YWN0aW9uPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6aW5zdGFuY2VJRD54bXAuaWlkOjk3MGQ4NDVmLTU4M2QtMTQ0Ny1hZGRkLTVjYzhmYzhiNGYxMTwvc3RFdnQ6aW5zdGFuY2VJRD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OndoZW4+MjAxNS0wMS0xOVQxNTo1MToxNyswNTozMDwvc3RFdnQ6d2hlbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OnNvZnR3YXJlQWdlbnQ+QWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpPC9zdEV2dDpzb2Z0d2FyZUFnZW50PgogICAgICAgICAgICAgICAgICA8c3RFdnQ6Y2hhbmdlZD4vPC9zdEV2dDpjaGFuZ2VkPgogICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgPC9yZGY6U2VxPgogICAgICAgICA8L3htcE1NOkhpc3Rvcnk+CiAgICAgICAgIDxwaG90b3Nob3A6RG9jdW1lbnRBbmNlc3RvcnM+CiAgICAgICAgICAgIDxyZGY6QmFnPgogICAgICAgICAgICAgICA8cmRmOmxpPmFkb2JlOmRvY2lkOnBob3Rvc2hvcDoxZDQxNDQyNy05ZmJkLTExZTQtOTFjNS1lZTdkN2MwYzMwNGM8L3JkZjpsaT4KICAgICAgICAgICAgICAgPHJkZjpsaT5hZG9iZTpkb2NpZDpwaG90b3Nob3A6OWVlMjQ3ZjAtOWZiZC0xMWU0LTkxYzUtZWU3ZDdjMGMzMDRjPC9yZGY6bGk+CiAgICAgICAgICAgICAgIDxyZGY6bGk+YWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOmM0MDc4YmRmLTlmYzAtMTFlNC05MWM1LWVlN2Q3YzBjMzA0YzwvcmRmOmxpPgogICAgICAgICAgICAgICA8cmRmOmxpPmFkb2JlOmRvY2lkOnBob3Rvc2hvcDpjYmMwZTZjZi05ZmJjLTExZTQtOTFjNS1lZTdkN2MwYzMwNGM8L3JkZjpsaT4KICAgICAgICAgICAgICAgPHJkZjpsaT5hZG9iZTpkb2NpZDpwaG90b3Nob3A6ZjIxMGFlMDEtOWZiZi0xMWU0LTkxYzUtZWU3ZDdjMGMzMDRjPC9yZGY6bGk+CiAgICAgICAgICAgIDwvcmRmOkJhZz4KICAgICAgICAgPC9waG90b3Nob3A6RG9jdW1lbnRBbmNlc3RvcnM+CiAgICAgICAgIDxwaG90b3Nob3A6Q29sb3JNb2RlPjM8L3Bob3Rvc2hvcDpDb2xvck1vZGU+CiAgICAgICAgIDxwaG90b3Nob3A6SUNDUHJvZmlsZT5zUkdCIElFQzYxOTY2LTIuMTwvcGhvdG9zaG9wOklDQ1Byb2ZpbGU+CiAgICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2UvcG5nPC9kYzpmb3JtYXQ+CiAgICAgICAgIDx0aWZmOk9yaWVudGF0aW9uPjE8L3RpZmY6T3JpZW50YXRpb24+CiAgICAgICAgIDx0aWZmOlhSZXNvbHV0aW9uPjcyMDAwMC8xMDAwMDwvdGlmZjpYUmVzb2x1dGlvbj4KICAgICAgICAgPHRpZmY6WVJlc29sdXRpb24+NzIwMDAwLzEwMDAwPC90aWZmOllSZXNvbHV0aW9uPgogICAgICAgICA8dGlmZjpSZXNvbHV0aW9uVW5pdD4yPC90aWZmOlJlc29sdXRpb25Vbml0PgogICAgICAgICA8ZXhpZjpDb2xvclNwYWNlPjE8L2V4aWY6Q29sb3JTcGFjZT4KICAgICAgICAgPGV4aWY6UGl4ZWxYRGltZW5zaW9uPjE2OTwvZXhpZjpQaXhlbFhEaW1lbnNpb24+CiAgICAgICAgIDxleGlmOlBpeGVsWURpbWVuc2lvbj43NzwvZXhpZjpQaXhlbFlEaW1lbnNpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgIAo8P3hwYWNrZXQgZW5kPSJ3Ij8+FjfD0QAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAZM0lEQVR42uydeXgUZbr2f1Xd1Uu6O519NXsgICiMLBJ3QWcOcnBkwoAHUGFwdBAdr2/cHcFlHGfc5oi44HW+ERjXoyLoqKPfOeqIIiDIKiIhBAwhCVk66TW9Vn1/dNKk0p0QFiGXU/d19R+1vlXPe7/Pcr9V1cK4sWP9gBENGgYngqJGUA2DHAZRs4GGwQ6NpBo0kmrQoJFUg0ZSDRo0kmrQoJFUg0ZSDRo0kmrQSKpBg0ZSDRo0kp4S2C1hctKC6HWKZoxTDL1mgv5hNUc4q8SDNSnCnrokZI2jGkkHCySdQuUIJxeNasfp0fPRpnTqmk2aYTSSDg5ccFYHsy9rItUW5t11Gbz6cY5mFI2kgwPjh7mouqiZC8/u4PMdKSxeXkpLh0EzjEbS048JZzq5YkIbl53joKHVyAMrSvl/m9M0w2gkHRxh/arzWzhvpBOA1z7JZsWHebh9Oo0ZGklPL342ro2pla0ML/ZiNsh8+72FJasK2Flr1RihkfT0VuuTJ7QyeXwbZxZ5kfQKsgL/9X4+qz7LxOXTMh+NpKcJVnOEiec4qLqwheLcTqQuMX5nrZUn3ihkb32SxgKNpKcHGfYQE3/ioOriFvIz/IhCdH2HR8+qtVm89D85BEPahJtG0tOAkpxOzhvpZPrFzWSnBlXbtu+z8p9vFlKteU+NpKcDw4u8VJ7pZM7lTZgMsmpbm0virc+yWPlRrtbjP1aS6nXRIkOWhUF3AyNLPFw+1sG/V7Zi7kVOgK+rbSx79wx2HbBovf1jJmlRth+AfQ3mQXPhI0q8TL+omfNHdmA1R+K2e/06/u/7ebz9eRahsKD19I+dpCaDTF5GAIdbot09ODKEW6Yd5OxST8Jt63fZefHDPHbt17znjwEDKm/rW4wY9DLjKlyI4ul/Vk0UICM5FLc+FBZ44o1CFi8v1Qj6r0ZSp1dPfYuJ4UXemM54OmExR4go6hD+5Td2bvzLcN5em4XXf+LTmklJSSQnJyMIP3yqkJmZiU6nTcWecHX/zX4Lsy9rYniRl201ttN60SmWEAZ9tEhy+fSs+DCXd9Zl0hk4Md2zqKiISy+9lOLiYsaNH8+zzz7LPz744KRfvyRJXHTRRYwYOZJhFRWUlpVx5ZVXEolENEaeCEkjsoAgKMy4pPmYSWoymUhKSiIcDiMIAoqi4HK5YtutVis2m42srCxSUlLwBwI0NjTg8XhwOp1xnZdsiWAyyLS79fzuuaHsOXhydE+73c6Cm26Kec9IJIKinPzIYbPZuP7Xv6asrAwARVHQ6/UEAwGNkSdCUoAvdqZw7U+bEAUG/BqFKIr82+TJzJo1C7fbjSiKOJ1Obrn5ZnQ6HZdccglVVVWMGDkSq1X9gEdNTQ0rVqzgy3Vf4PF4j3hSaxijJLOvwUzNIXOf7ep0OsLh8ICJ5vF6VeG9urr62Iyp16MoylE9osvlItCDkLt37yY9LQ2f13vsHTjANo8FBoOBYDA44P4VRZFwODw4SPrlLjsLr6pnyoRW/r4+Y0DHKIqC3W6nuLg4tm7Xrl3o9XqmTZvG3ffc0+ex5eXlPPzwwzid7VRV/RKXM/pIXVpWCINBBgX0Ooh0yaN5eXlUVFRQVFREVnY2OlGkra2N7Tt2cPDgQdodDjo7O1Vt6HQ6ysrKSEpK4swRI1Tb0tLSiEQiJNtsCIJAU1MTLS0tqn2GDh3KsOHDKSoqwmaz4fP5cDqdtLe303z4MN9++y0dHR0ApKamkpeXR2ZmJsnJybFzuN1uysvLsdls0YEVibDnu++QZTkhgYYNG0b5kCHk5eZit9vxdXbScOgQn61di7+zM9ZeX7l2eXk5FqsVORJBlmU2bdoEwPjx4xk1ahT5+fls2ryZTz7+OM5eAMXFxZSVl1NcXExmRgYIAo2NjXyzcyd1dXU4nc4Bk/ykk7TDI+Hy6bnyvJYBk1Sn02G1qCttt9vNb2+9ldmzZw8wDKey5OlnmD1rLgYpQkpOGJ0BhIhIIBT1fL+oquLqmTMpLinp8zzLly9n9erVNDU2qjr9/gceYMiQIXH7P//88zEPpSgKLyxbxsqVK2PbZ8yYwY033oitB+EStfnCsmXIssxll1/OwoULSUpSpydjxozhrLPOQq/XI0kSHR0dXDl1qsrbAlRWVnLlz3/OpEmTErZ12+2343A4uP/++9n97beqlKqn511w002MGTMGgMOHD1P1i19w+U9/yv333x/bb8TIkdQfPMj27dtVx1599dXMmDmTnJy+X6l59plnWLNmDc4up3JKSRoKC3z0VRpXnt9Kqi08IM00Eong9qj1zHHjxjFhwoTY8ssvvcT+AweQ9HomVFZyySWXxJ1nxJnDSM8ow+2sxpYaAREaW6Kh/s677mL69Omq/dvb29m5cydlZWXk5+cDMG/ePObMmcOc2bPZv38/AIIgUFBQ0O8g60ZO7pFp1UmTJnH7HXcc9f5Hjx5NJBJBEAQuvvjiOIJ2d75ef8SWqamp+P1+Vepx3dy5LFy48KjtpaWlsXTpUrZs2cJjjz5KbW2tarssy3h69IckSdx2++387Gc/i1McDh48qLrGxx9/nPMvuEC1X3NzM9999x0VFRVkZ2cDsPDmm5l//fVMnjwZr8dzakkaDfkpzJvcyJzLGlm6umBA+U12VlbCjt+/fz933nEHzc3NsbCydu1aPvvnP7nzrrswm9X5ZpIlG7ezGqs1ghKEwx4zs2ZeoSKox+Ph1ltvpaW5GVmWsVqtlJSW8qc//SnWKcteeIHLL7sMURSRJIm3336b/Px8xo8fr2qzra2NLV9/TUSWkSSJTz7+OJoTp6QwY8aMhJ533Rdf4A8EMBoMnHX22YwfPx6fz4fFYmHH9u04nU4qKiooLCxUHbtx40aampqw2+10dHTQ2dkZI/T06dMTEvTVV19l8+bNKIrC9KoqFYHOOecc/vjII8ydO5eA36/KIVVKSUoK06ZNiy37fD42btzIpZdeSltbW2ygLFq8WHX+9vZ2brzxRjweD4IgkJaayrBhw/j9fffFiuWVK1Ywe/bsuIjwg5O0sc1ARBYYP9yF9K5CKCIcNbHW6eObaWpqoqqqCn0vfbC1tZUPPviAsvJy5syZo1YJLNHzGIIRdBJMurCUzMrfqwy8dOlStm/bFuuM5uZmamtr+YPZzKLFi2OeauKkSfzz009xuVwsf/FFLFYrd999t8rD37RgAYcOHcJms6EoSix82Ww2zL084sMPP8z7772nKmD279/PP/7xD4zG6H9nrFy5ErPZTJLFwpo1a2L7ffrppzz11FP4vN5Ysdc9WCoqKvjtrbfGea9fX389DocjSgBBYMP69ZSUlvLKK6/EiFVWVsa8efNY9vzzqsjWs8jptlM4HObqmTNxtLeTlprK4489RigUwmAwMHbsWCZPnqzqo4cefJADXdEIoKW5merqakpLS/mPWbMAKCwq4pwxY1j/5Zc/vJjfE76Ajs17bBRl+6kccfScw2A0kpKSEldM/fGPf4wjaM/tH3zwAf4eHiDqgaMj0hCOIOghd8hsDIYjb3Ped999rH777ThvAbBu3TrV8qSJE0lOTo6Rr7GhAZvNFpcyBINB2tracDgcMQI6HA52796t2reluTmuwg6Hw3T6fLFQHggE6OjoQNfr+pqammhtacHpdOJwOHC5XAiCgCiKXDFlCiaT+n3/GTNm0NjYeMRDdVX3NXv38v5776n2nTZtmiplMZvNqqKt+z6vu+466urq8Ljd1NXV0draGrPtNddeq9r/rrvuYsOGDQn7bcmSJap15557LuFwOGER+IOR1B8U+eo7O3qdwsWj2o8+c2M2k5amfvPS5XSyYf36/geD1xvX6d35TUpSBCQzQuqkOGNLkkRFRQXlQ4bEfvn5+aSlp6sKifIhQ0hLT1elIOk9lgH0kpTw2rxeL//9+uuqdQ8+9BAjR45MOEB6o3cOHAwGE8pkFoslrkh69dVXcScoiLrx0ksvUVNTE1s2Go1ceOGFquXeefHLL7/M3j7kNqvVSlFRUWy5e6BJkhRTGcqHDKG8vJz8/HxKS0tVnnrEiBHo9foTIulxPS3SLZ6fXeahMMvf75c9DAZDXHX/fV1dLJT0OStj0Md1eDAQzVvtJhmkEaC3x1XSxyLcm3t4KJ1OF+fxA708eU/s27ePxx57jDvvvDOW2y15+mmWLl3KRx9+mFC66S7UrL08ttvtTqhzmkwmsnrl82+99Va/U6jff/89e/fupby8PCY5pfZyEj1zRK/Xywf9zKrl5uaqPLnRaGTVqlUDtnNubm4shTmlJD3sMODp1JGfEeCiszt4+X/7liOMRmNc/lZdvadfggIYTXZ6T5sH/dH0wmSUQcwHUVKFmu3btlG7fz/GBOcWRBE5EoGuMLpt61ZV9SqKYlxY9fdDUkEQWLN6NcFAgPsWLYrlqvfeey8FBQU888wzKAm8hyiKccTz+XzIioKQgKS90dpLp+1LUemJnsVgst2uGoyhUAiHw9G3s5AklbMIhUJs3bqV+vr6/u0MiDodGzdupNPv50SegDgukjpcEpv3JHPJ6HYuHtXOqrVZdAbFPsVjSy9P6uzoQK+XCIdDfbYRliVk5QgJg2GF3EwjprRWslKCyAETgiCqQv3Cm28mFAzGJJ/+Jhi6c76eg6k3QW02W78dGA6Heffdd/luzx4eeeSRWMV+zTXXMGbMGOb/6lcJPWTvcBsMBEBR6D0q3W53wjb7g8lkIiNDrWH39OoGSVLJXeFwmHAo1Kd39nq9qjZrampY2DV1PFA7G/pIm36wnBSgMyiyuToaskaUeBk7rO8cyWxJRdKrDbD9m4MJCZpmC1Ge18m5o1xMrASdeMSY/jC8cG8KLy46gNkbQQ67EYSIKlx3e2edThebrkv0697elx4KUF9f369grYoMe/Zw5x13sGPHjti6M888k2efey6hB7b3Klz8gUDCzvYkmCY9WgQqKipSze6FQiEONzWpSNzTaXR2dvabLyqKosqXjUZjzHbHY+dT5kkBans8pX/uMBfrdqYkns8PO9CZ1HpnW1srKRY/Q4YEyZKC5GQGKc71k5MaINseIisjiHj2LdCjP6wmATE9AAeBMOjCO0AIq3LMsrIydvSYITkW9O78YDDYZ16Z0B61tSxetIgHH3qIUaNGxbTK884/ny97KAuiKJKamqomYx+CdyAQwOl0Yrcfyb0nVFbG9NpEuHTiRNXgam9vZ92XX8bCrdFoVA1In8/X7301NDSo7FBaWkpuXh4Nhw5xqnDcNG/uMNDQFg2RPx3XRk5aYsG2IN2L3EsnveNXGfxhfg2P3bCP319zgPlTGpj0EwcjSryk24PgBTyWXiFL4f0P3az8LJOnNhZw+wsmPvtkPcEe0W/u3LlYrMf3FZKeRAA444wzjvkJqIaGBtauXataV1paGpeT9i6c9PrEvkInijzwwAOqdYsWLerzHouKipg6dapq3a5du1S5scfjUXnSbjG+v5SmtbVVtW7+/PkYjMbBT9LDDgPfdr3cZjVH+PfzWjlnqJvpFzdza9VBHv11Dctu+47rZg+N+xvo4SUy5w71Yw5GQAd0cUEJg6iHDiTanOqOq2+O8OKqNp5dVcTra7L5fFMK992/BIEjRLrgggv429/+lnAevrsT582bx9ldnk7ltXo9EJGcnMyEykpMJhN2u50xY8eSlJSEwWhk7rx5FBYVxeWbEyorueKKK+LCe+/l3pLcggULyMrKwmQyUVJSwujRo2MheOvWrapwbLFYeOSRRygoKIid25aczM+vuoqnliyJk9EeevBBVcj1BwJIPXLEzqN4UoAnn3xSNWCnTp3KX//6V/Ly8hKmM+VDhrBgwYKYwnDawn1EFqg+mMRlY6KFxayJh5l2QQu2pDCiCASBEOjT7fEeAj+yAooIvk4ddc0m9jeY+L7FTHVDEoeajVTN9PDLIpC67RuoJyhmAkfCjL/TwdIlf+F3t92m0iD//OijbNywAZfbjaVLgikqKiIlJYXs7GwW/OY3cdfU0twct+6GG25gYpfon2yzUVVVhb+zk3nz5jFlyhT27t2LLMsoikJOTg4lJSVxQvnmzZtVy7Is09LSwtChQ2PrysrKeHH5chobG8nNzWXTpk18/fXXMb34V/PmsaLHgy2VlZU89/zzVFdXk2K3k2y3k5+fH+eRb7/ttjhPmdM1v36kH4+uX+6rqeG1115jVtdMUvdM2BNPPsmGDRsIBgJYrVbSMzIoKirCbreTmZnJ33tNLJxykgLsbzL3qBplDNKRG1Zk0Jn0YCuMO+7NjzzU7i6g0W2m/pARr1+HPygS6PFFEXvm6CMEBSxmAbPF3GvUwpo1axBEkRtuuCEWxgoKCvp9aKS+vj5uncvlora2VhWerVYro0ePjuVuwVAIm83G1i1bOO/881Uid6J88vXXXmX7tu0YDJJq/fPPPUdlZaXKw2VlZcWkqS1btqi27dq1i+uuvZbnly1DkiQkSSI7Ozv2QEeie1m8eDEb1m+I8+SmXs9D5ORkH7WfZVnmxb++SCQSYfr06TFJq7y8vE9vGQqFEg78U07Sg81GWjoMZKYcCZWyAoGAiM+vp7HRSmv7QWwZOwkFPeglPXq9iederiHg7984bS11bNlqptPnR28w8f2BOtzu+Bkuv9/PG//9Bv/zv59zyy2/pbQkm9TUdAwGIzq9DhQFWZbx+/14PB6++OIL2jtcCfXLa6+dz1NLniM3x4rNZo/JLD6fj23bthMMBJEjMi+98iZWm40UewpJlqRo0SUIKLKMx+2iw+njiSeeZvv2jSqCHpFx9nHv7//A9fNnkZ6eEXtwORgM4vV62bBBTS5BENi9+zvmzLmO0T+p5OqZV2GxGLBYrOh0utj9tbY2s/bzzaxcsRw54j+SR/XUWVtb2b17N7IsIwgCe/fuZSCpt8vl5IVlL/DOOx9xyy2/JS/PToo9DaPJiCCKKLLcw85e3n//PUKh0EkhqTBu7Njjfj9CJyrcM/sA44e5aOkwUNds4kCjiQMtZr49mERLq0Gll3WPSgUFndj/i2fRHEiISYeKEkFRhH4lDUEQcLuDnFFQTGpaOiaTGVlR8HnctLc7aG1rRicEVTlZnM5oNNAZNFNRMQSDZMDtdtHYdIiO9maSzD09YhCTOYW09Ezs9lRMJhM+XycH9u9Dkb0IwtHN6g8IlJVVkJKaSjgcxtHWSmNjPQL+fmeVBFGHqLNRWFiI2ZxEwB/g8OEGDh8+gCWp/28jhEIyoVBEJckajccmFYmiSIczRHFxCampaRiMRiIRGa/XjcPRiqOtBUkf6bMgPKUkBchJC2IxRfr8cESyRU9+lpc2V4hwSI9eb6bZcfQmU5NFRKETSQqRnKTH6UnisENGw78eTpjqTY6+xWWjQWLuf3iYM6UWzD6QJfYfyGLRo4VUf9/36wXpdjN339LAxZUNIAYgYuCTjQU8uTSDlo6QKic16OUjXkHo+h0NSsJIePRzdB83kHaONp6Odg75ODWZvu6t+7rF47zeY7h2vV4hEBAJhU7O6+An7En74/89c+uZ9pt6lN2guAEJxDwIuwWm3jyBto5QghuUeP3RbRSO6IR6kDsBE4jDoO4rK7+8fTSKEp1TL8nv5A/X1WKSZAQ9UfFf34cRe3ZeEBR/tF9iBFdA1EXbip2j97EBUAIgmKL3Emsn0b7B6E+WE2wHRCNgTHC83PXrBDnSY5tCVDUxdLUt9tFuOKqqKOHoTGtsHxkEHQhJCYgaAvxdL1f2M0AFGJCd9aLC+u12/ry8aHB40r5QkKMwbUo9yi4ItXd1bBiUWtCXKcybcognXsmKO27iaDeFFZ0oByDsj16h4gbpGygc6eHSn7j4ZIsRUPD4dXy5y37kX+r685AD9QTyAI4bSDtH85RHO4d4nMf11W73dcvHca3Heg1AbZP5JLq7H2KGQIThJdF5Z8UdJajQZQxFApxQnutGEHNRZLUgXprnA0/X8xbdtYOhy4N5YNgQD5/vMBIKQ0ubgefeOUNL2n7k+EE+dawo0NpugEhXaFB6jUIJOjxGlAST/e0eCXS9HghSiM5MidDWYYi9wqxBI+kJkfSbfSYaGs2IeSB25zMC6M1Rkq5el5sw9ny+IxVPUEJIBkGMHiMoIOZEvennX6cga3/wqZH0ZCAYCnLbE2fhcBnRFYIuCXTJEEoX+dsbJXz1TeIHFBpaI/z56WH4RB26DNCZQZ8HXknP3Y+dTUOr9r2kfzX8gNV9FFlpEjf8op4zCztweE288uEZbNhhRFHC/Y6d8kKYP/UgBZleGhwW/uudAmrqQFG0WK+R9GQ3IIik2UVKz/Dj9OjZV6cnIocHcJyO/GzISQ/S2GKgsRVkWfOiGkk1aPhXykk1aNBIqkEjqQYNGkk1aDgGkro1M2gYxAiKQKNmBw2DGI0isEOzg4ZBjM0isEazg4ZBjDUi8CZQr9lCwyDEIeBN8atNm4LA7zR7aBiE+D9fbdoU/Qu5rzZtehN4XLOJhkGEx7t4qdJJ7wGe0WyjYRDg2S4+AiD0/MbP+HHjAKqAPwPlmq00nGLUAPd8tWnTWz1XJnrHaRXwd+CXwDRgDJBP9B1FDRpOJkJdxdHXwGrgza4aSYX/PwD/Au2TqHc7PAAAAABJRU5ErkJggg==',
		var img = new Image();
	    img.src = 'images/exportLogo.png';
	    convertImgToDataURLviaCanvas(img.src,function(base64Img){
	    	var docDefinition = {
					content:[
					         	{
					         		image: base64Img,
					         		width:150,
					         		height:70
					         	},
					         	{ 	text: docHeader, style: 'subheader' ,margin: [0, 8, 0, 5] },
					         	{
					         		text: invoiceLines.heading, style: 'subheader' ,margin: [0, 5, 0, 10] 
					         	},
					         	{
					         		style: 'tableExample',
					         		table: {
					         			widths: [ 'auto','*',150,'*','auto','*','*','*',200 ],
										body: invoiceTableBody
					         		},
					         		layout: {
					                    hLineColor: function(i, node) {
					                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
					                    },
					                    vLineColor: function(i, node) {
					                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
					                    },
					                    paddingLeft: function(i, node) { return 2; },
					                    paddingRight: function(i, node) { return 2; },
					                    paddingTop: function(i, node) { return 2; },
					                    paddingBottom: function(i, node) { return 2; }
					                }
					         	},
					         	{	text: basicInfo.heading, style: 'subheader' ,margin: [0, 10, 0, 10] },
					         	{
					         		style:'tableExample',
					         		table:{
					         			body:basicInfoTableBody
					         		},
					         		layout: 'noBorders'
					         	},
					         	{	text: orderReferences.heading, style: 'subheader' ,margin: [0, 10, 0, 10] },
					         	{
					         		style:'tableExample',
					         		table:{
					         			body:orderReferencesTableBody
					         		},
					         		layout: 'noBorders'
					         	},
					         	{	text: addresses.heading, style: 'subheader' ,margin: [0, 10, 0, 10] },
					         	{
					         		style:'tableExample',
					         		table:{
					         			body:addressesTableBody
					         		},
					         	}
					         ],
					         defaultStyle: {
					             alignment: 'justify'
					         },
					         pageSize: { width: 700, height: 900 },
					         pageOrientation: 'landscape',
					         pageMargins: [40,10,40,10],
					         styles: {
					        	 header: {
					     			fontSize: 14,
					     			bold: true,
					     			margin: [0, 10, 0, 10]
					     		},
					     		subheader: {
					    			fontSize: 12,
					    			bold: true,
					    			margin: [0, 10, 0, 5]
					    		},
					     		tableHeader: {
					     			bold: true,
					     			fontSize: 12,
					     			color: 'black',
					     			fillColor:'#a5a5a5'
					     		}
					     	}
			};
			pdfMake.createPdf(docDefinition).download(fileName);
	    });
		
	};
	convertImgToDataURLviaCanvas = function(url, callback, outputFormat){
	    var img = new Image();
	    img.crossOrigin = 'Anonymous';
	    img.onload = function(){
	        var canvas = document.createElement('CANVAS');
	        var ctx = canvas.getContext('2d');
	        var dataURL;
	        canvas.height = this.height;
	        canvas.width = this.width;
	        ctx.drawImage(this, 0, 0);
	        dataURL = canvas.toDataURL(outputFormat);
	        callback(dataURL);
	        canvas = null; 
	    };
	    img.src = url;
	};

	// ADM-012
	function download(strData, strFileName, strMimeType) {
		var D = document, A = arguments, a = D
				.createElement("a"), d = A[0], n = A[1], t = A[2]
				|| "application/pdf";

		var newdata = "data:" + strMimeType
				+ ";base64," + escape(strData);

		// build download link:
		a.href = newdata;
		if ('download' in a) {
			a.setAttribute("download", n);
			a.innerHTML = "downloading...";
			D.body.appendChild(a);
			setTimeout(function() {
				var e = D.createEvent("MouseEvents");
				e.initMouseEvent("click", true, false,
						window, 0, 0, 0, 0, 0, false,
						false, false, false, 0, null);
				a.dispatchEvent(e);
				D.body.removeChild(a);
			}, 66);
			return true;
		};
	};
	
	invoiceDetailCtrlInit();
	
}]);