//order controller

catalougeControllers.controller('orderHistoryCtrl', ['$rootScope','$scope', '$http','$log', 'DataSharingService','OrderService', 'Util','$state',
                                                     function ($rootScope,$scope, $http,$log,DataSharingService,OrderService,Util,$state) {
	
	var showLoading = true;
	 var delimeter = ";";
	var orderHistoryCtrlInit = function(){
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	});

	var initController = function()
	{ 
//		$rootScope.initCommitsSecondNextPromise.then(function(){
		$scope.sortColumn = "OrderNumber";
		$scope.orderType = "DESC";	
		$scope.isUserSessionActive = true;
		$scope.cachedPaginationObj = null;
		$scope.sort = new Sort();
		$scope.sort.DESC('OrderNumber');
		$scope.filterOn = false;
		$scope.filterList =  null;
		$scope.paginationObj = null;
		$scope.isCollapsed = false;
		 
		  $scope.copyPasteText= {};
		  $scope.addOrderRowList = {};
		  $scope.rowList = [0,1,2,3,4,5];
		  //
		 
		  $scope.showAddSection = false;
		  $scope.showCopySection = false;
		  
		makeOrderHistoryFilters();
		var params = getSearchParams(); 		
		$scope.callState = $state.current.name;
		if($scope.callState == REQ_SUBMIT_ORDER_VIEW){
			showLoading = false;
			var requestObject = DataSharingService.getObject("requestObjectInfo");
			if(!isNull(requestObject)){
			if(!isNull(requestObject.reference) && !isEmptyString(requestObject.reference)){
				$scope.sort.ASC('OrderNumber');
				params = ["OrderNumber",requestObject.reference];
				$scope.sort.addSortParams(params);
			}}
		}
	 	getOrderHistoryPageData(params,FirstPage);
	};

	var getSearchParamsSorting = function(){
		var params  = [];
		if($scope.filterOn){
			params = $scope.sort.addSortParamsSortingOrderSort($scope.filterList.getParamsList());
		}else{
			params = $scope.sort.getParam();
		}
		return params;
	};	
	
	var getSearchParams = function(){
		var params  = [];
		if($scope.filterOn){
			params = $scope.sort.addSortParams($scope.filterList.getParamsList());
		}else{
			params = $scope.sort.getParam();
		}
		return params;
	};	
	
	var makeOrderHistoryFilters = function(){
		//$rootScope.initCommitsSecondNextPromise.then(function(){
			var finalFilterList = new FilterList();
			var staticFilterConfig = OrderService.getFilterConfiguration();
			var filterDataList = DataSharingService.getFilterDetailsSettings();
			
			var filterExist = false;
			if(isNull(filterDataList) || isNull(filterDataList.customerList)){
				filterExist = false;
			}else{
				filterExist = true;
			}
			
			
			if(filterExist ==  false){
			Util.getFilterListDetails([]).then(function(data){
				DataSharingService.setFilterDetailsSettings(data);
				filterDataList = DataSharingService.getFilterDetailsSettings();
				var dynamicFilterConfig = null;
				OrderService.getOrderFilterList().then(function(data){
					dynamicFilterConfig = data.filterBeans;
					finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
					$scope.filterList = finalFilterList;
					//console.log("makeOrderHistoryFilters : " + JSON.stringify(finalFilterList));	
				});
			});
		//});
			}else{
				OrderService.getOrderFilterList().then(function(data){
					dynamicFilterConfig = data.filterBeans;
					finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
					$scope.filterList = finalFilterList;
					//console.log("makeOrderHistoryFilters : " + JSON.stringify(finalFilterList));	
				});
			}
	};
	
	$scope.submitFilter = function(){
		if($scope.filterList.hasFilterTextValues()){
			$scope.sort.ASC('OrderNumber');
		}else{
			$scope.sort.DESC('OrderNumber');
		}		
		$scope.filterOn = true;
		var params = getSearchParamsSorting();		
		getOrderHistoryPageDataSorting(params,FirstPage);
	};
	
	var getOrderHistoryPageDataSorting =  function(requestParams,page){
		$scope.paginationObj = $rootScope.getPaginationObject('orderHistoryUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('orderHistoryUrl',requestParams,page, showLoading ).then(function(data){
				$scope.selectedRequest = {};
				//$scope.selectedRequest.selected = false;
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true);
				}else{
					setUIPageData(data,false);
				}
			});
		}
		if(requestParams[0]=='YourOrderNumber'||requestParams[0]=='Customer'||requestParams[0]=='YourReference'){
			$scope.sort.DESC('OrderNumber');
		}

	};
	
	var getOrderHistoryPageData =  function(requestParams,page){
		$scope.paginationObj = $rootScope.getPaginationObject('orderHistoryUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('orderHistoryUrl',requestParams,page, showLoading ).then(function(data){
				$scope.selectedRequest = {};
				//$scope.selectedRequest.selected = false;
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true);
				}else{
					setUIPageData(data,false);
				}
			});
		}
		
	};
	
	
	var setUIPageData = function(data,isFirstPageData){
		var ItemList = [];
			if(isNull(data)){
				setItemList(null,isFirstPageData); 
				if(isFirstPageData){
					$rootScope.applyPreviousPaginationObject('orderHistoryUrl' , $scope);
				}
				
				
			}else{
				ItemList = data.salesOrderBean;
				setItemList(ItemList,isFirstPageData); 
				$scope.sortColumn = data.sortColumn;
				$scope.orderType = data.orderBy;
				
				$rootScope.cachePaginationObject('orderHistoryUrl' , ItemList, $scope);
				if(isFirstPageData && (! Array.isArray(ItemList) ||  !ItemList.length > 0 ) ){
					$rootScope.applyPreviousPaginationObject('orderHistoryUrl' , $scope);
				} else {
					if(!isNull(data.moreRecords)) $scope.moreDataArrow = data.moreRecords;
				}
				
				
			}
			$rootScope.setPageLoadMsg(ItemList,isFirstPageData,$scope);
			$rootScope.scrollToId("result_set");
			if(!isNull($scope.$root)){
			$scope.$root.$eval();
			}
		};
		
	var setItemList = function(itemList,isFirstPageData){
		if(Array.isArray(itemList) && itemList.length > 0 ){

			$scope.orderData = itemList;				
			
		}else{
			if(!isFirstPageData){
				$scope.orderData = [];
			}
		}
		updateFilterStatus(itemList,isFirstPageData);
	};
	
	var updateFilterStatus = function(list,isFirstPageData){
		if(Array.isArray(list) && list.length > 0 && isFirstPageData){
			if(!isNull($scope.filterList)){
				$scope.filterList.cacheFilters();
				}
		}else if($scope.filterOn && isFirstPageData){			
			$scope.filterList.applyPreviousFilters();			
			showMsgNoObjFound();
		}
	};
	
	$scope.validateDateRangeFormatShoppingCart = function(dateStr,secondMsg)	{
		////console.log(filter);
		
		var dateFormat = $rootScope.webSettings.defaultDateFormat;
		//var isFilterTextEmpty = isFilterRangeEmpty(filter);
		this.validDateShoppinglist = true;
		if(isNull(secondMsg)){
			secondMsg = true;}
		
		if(Util.isNotNull(dateStr)){
//			var isValidDate = isValidDateFormat(dateStr,dateFormat);
			var dateValidator =new DateValidator(dateFormat);			
			var isValidDate =dateValidator.isValidDate(dateStr);
			if(!isValidDate){
				this.validDateShoppinglist = false;	
				$scope.validDateAllowUpdate = false;
					
			}else{
				this.validDateShoppinglist = true;
				$scope.validDateAllowUpdate = true;
			}
			if(dateStr==""||dateStr==null){
				this.validDateShoppinglist = true;
				$scope.validDateAllowUpdate = true;
			}
			
		}
		
	};

	
	// sorting 
    $scope.sortASC = function(column){
		$scope.sort.ASC(column);
		sorting();
	};
	
	$scope.sortDESC = function(column){
		$scope.sort.DESC(column);
		sorting();
	};
	
	var sorting = function(){
		var params = getSearchParams();		
		getOrderHistoryPageData(params,FirstPage);
	};
	
	$scope.previousPage = function(){		
		 var params = getSearchParams();		
		 getOrderHistoryPageData(params,PrevPage);
	};
	$scope.nextPage = function(){	
		 var params = getSearchParams();		
		 getOrderHistoryPageData(params,NextPage);
	};
			
	$scope.getFirstPage = function()
	{			
		 var params = getSearchParams();		
		 getOrderHistoryPageData(params,FirstPage);
				
	};
	//  Pagination code ends
	$scope.showOrderDetail = function(order,fromRequest){
		if($rootScope.isShowDisableMenuItem('CON_ORDER_INFORMATION')){
			DataSharingService.setToSessionStorage("order_clickedDetail",order);
			$rootScope.fromCart = false;
			$rootScope.fromRequestPage = false;
			DataSharingService.setToSessionStorage("fromCart",$rootScope.fromCart);
			DataSharingService.setToSessionStorage("fromRequestPage",$rootScope.fromRequestPage);
		
			if(fromRequest){
				$state.go(REQ_SUBMIT_ORDER_DETAIL_VIEW);
			}
			else
			{
				$state.go(ORDER_DETAIL_VIEW,{"ORDER_NUM": order.orderNumber});
			}
		}
	};


	// new code ends
	// old code
//	$scope.sortColumn = "OrderNumber";
//	$scope.orderType = "DESC";	
//	$scope.param = [];
//	$scope.filterParam = [];
//	$scope.filterOn = false;	
//	$scope.dataFound = false;
//	$scope.recordFound = true;
//	$scope.propertyList = [];
//	$scope.isUserSessionActive = true;
//	$scope.responseMessageCode = "";
//	var showLoading = true;
//
//	var requestParams = [];	
//	$scope.callState = $state.current.name;
//	if($scope.callState == REQ_SUBMIT_ORDER_VIEW){
//		showLoading = false;
//		var requestObject = DataSharingService.getObject("requestObjectInfo");
//		if(!isNull(requestObject.reference))
//		{requestParams = ["OrderNumber",requestObject.reference];}
//	}
//	else
//		{
//			showLoading = true;
//		};
//	$scope.getOrderList = function(requestParams)
//	{	$scope.selectedRequest.selected = false;
//		OrderService.getOrderHistory(requestParams,showLoading).then(function(data){				
//			if(data==null)
//			{
//				$scope.dataFound = false;
//				$scope.orderDataLoadMsg = " ";
//				$log.log("orderHistoryCtrl --  no data fetched");			
//
//			}
//			else
//			{			
//				$log.log("orderHistoryCtrl messageCode -- "+data.messageCode);
//				//$rootScope.reLogin(data.messageCode);
//				$scope.orderData = data.salesOrderBean;				
//				$scope.dataFound = true;
//				$scope.orderDataLoadMsg = "";	
//				
//				$log.log("orderHistoryCtrl --  " +data.salesOrderBean);
//
//			}
//
//		});
//		
//	};
//	
//	$scope.getOrderList(requestParams);
//
//	// sorting
//	 $rootScope.sortSelection = '';
//	 $rootScope.sortSelection =  $scope.sortColumn+$scope.orderType;
//	 //$log.log("Request Search sorting-- Current Sorting Selection : " + $scope.Selection);
//
//	$scope.Sorting = function(column,orderBy) {
//		$scope.selectedRequest.selected = false;
//			var	requestParams = $rootScope.getSortingParam(column,orderBy);		
//		pageKey = Util.initPagination('orderHistoryUrl',requestParams);
//		OrderService.getOrderHistory(requestParams,showLoading).then(function(data){
//			$scope.orderData = data.salesOrderBean;				
//			$log.log("orderHistoryCtrl --  " +data.salesOrderBean.length);
//
//		});
//	};
//	/*$scope.getParamData = function()
//	{
//		var requestParams =[];
//		if($scope.filterOn == true)	{
//			requestParams = $scope.filterParam;	}
//		else
//		{requestParams = ["OrderBy",$scope.orderType,"SortBy",$scope.sortColumn];}		
//		return requestParams;		
//	};*/
//
//	// sorting ends
//	//
//	requestParams = ["OrderBy",$scope.orderType,"SortBy",$scope.sortColumn];
//	var pageKey = Util.initPagination('orderHistoryUrl',requestParams);
//	// Pagination code
//	paginationObj = DataSharingService.getObject(pageKey);	
//	$rootScope.getOrderPageData = function(page)
//	{	$scope.selectedRequest.selected = false;
//		$log.log("getOrderPageData() - called, isPaginationBlocked = "+ Util.isPaginationBlocked(pageKey).toString());		
//		if(!Util.isPaginationBlocked(pageKey))
//		{	
//			$log.log("getOrderPageData() - called" );
//			requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn); 
//			Util.getPageinationData('orderHistoryUrl',requestParams,page ).then(function(data)
//			{
//				if(data.salesOrderBean==null || data.salesOrderBean.length == 0)
//				{	paginationObj.setLastPageNo();
//					$scope.dataFound = false;
//					$scope.orderDataLoadMsg = Util.translate('CON_NO_MORE_PAGE');
//					$log.log("orderHistoryCtrl -- "+ $scope.orderDataLoadMsg);
//				}
//				else
//				{
//					$log.log("orderHistoryCtrl messageCode -- "+data.messageCode);
//					//$rootScope.reLogin(data.messageCode);
//					$scope.dataFound = true;
//					$scope.orderDataLoadMsg = "";						
//					$scope.orderData = data.salesOrderBean;
//					
//					requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn); 
//					$scope.$root.$eval();					
//				}			
//				$log.log("paginationObj.isLastPageNo(): " +paginationObj.isLastPageNo());
//			});
//		}
//	};
//
//	$scope.previousPage = function(){		
//		
//		$scope.getOrderPageData(PrevPage);		
//	};
//	$scope.nextPage = function(){			
//		$scope.getOrderPageData(NextPage);		
//	};
//			
//	$scope.getFirstPage = function()
//	{			
//		$scope.getOrderPageData(FirstPage);
//				
//	};
//	//  Pagination code ends	// Search Filters 
//	$scope.filterFormInfo = {};
//	$scope.filterMsg = "";
//	$scope.submitFilter = function()
//	{
//		$scope.selectedRequest.selected = false;
//		$scope.filterOn = true;
//		var keyList = $scope.filterFormInfo;
//		$scope.filterParam =  $rootScope.setParamKeyList(keyList); 
//		var filterParam =  $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn); 
//		$log.log("orderHistoryCtrl filterParam--  " + filterParam);
//		pageKey = Util.initPagination('orderHistoryUrl',filterParam);
//		OrderService.getOrderHistory(filterParam,showLoading).then(function(data){
//			if(data.salesOrderBean==null || data.salesOrderBean.length == 0){
//				$scope.dataFound = false;
//				
//				$scope.filterMsg =  Util.translate('MSG_NO_OBJECTS_FOR_SELECTION',null);	
//				showMsgNoObjFound();
//				var icon = '<a class="OrderInformation-icon" shref="" title="'+$scope.filterMsg+'"></a>';				
//				$scope.filterMsg = icon + $scope.filterMsg;
//				
//				$scope.recordFound = false;
//				
//			}
//			else{
//				$scope.orderData = data.salesOrderBean;		
//				$scope.orderDataLoadMsg = "";					
//				$scope.filterMsg = "";
//				$log.log("orderHistoryCtrl filter--  " +data.salesOrderBean.length);
//				$log.log("orderHistoryCtrl orderData--  " +$scope.orderData.length);
//				$scope.dataFound = true;
//				$scope.recordFound = true;
//				$scope.$root.$eval();
//				$rootScope.scrollToId("result_set");		
//			}
//		});
//	};
//
//	var requestFilterParams = [];	
//	// to get Invoice search filter key list 
//	OrderService.getOrderFilterList(requestFilterParams).then(function(data){
//		//$http.get('./json/catalog/order_search_filter.json').success(function(data){
//		$rootScope.searchFilterList = data.filterBeans;		
//		$scope.getFilterSettings(data.filterBeans);		
//		$log.log("orderHistoryCtrl: searchFilterList --  " +data.filterBeans.length);
//
//	});
//		
//	$rootScope.filterDetailSettings = DataSharingService.getFilterDetailsSettings();
//		
//	// to get filter configuration setting from "filterConfig.json"  
//	$scope.getFilterSettings = function(filterList)
//	{
//		 var filterConfigData = OrderService.getFilterConfiguration();
//		 $log.log("orderHistoryCtrl getFilterSettings--  " +JSON.stringify(filterConfigData));			 
//		 $rootScope.searchFilterList =  $rootScope.getSearchFilterSettings($rootScope.searchFilterList,filterConfigData);
//	};
//	
//	// Search Filters 
//
//	$scope.showOrderDetail = function(order,fromRequest){
//		DataSharingService.setObject("order_clickedDetail",order);
//		$rootScope.fromCart = false;
//		$rootScope.fromRequestPage = false;
//		
//		if(fromRequest){
//			$state.go(REQ_SUBMIT_ORDER_DETAIL_VIEW);
//		}
//		else
//		{
//			$state.go(ORDER_DETAIL_VIEW);
//		}
//	};
//
//	$scope.isCollapsed = false;
//	$scope.toggleCollapse = function()
//	{
//		var isCollapsed = !$scope.isCollapsed;
//		return isCollapsed;
//	};

	$scope.exportAsHTML = function(){
		var orderSearchTable = setOrderSearchDataforExport();
		var str = orderSearchTable.getHTML();
		//console.log("order export html table  "+ JSON.stringify(str));
		if(/MSIE [1-9]\./.test(navigator.userAgent)){
			//downloadBySWF("testDNonIE.txt",str);
		}else{
			var blob = new Blob([str], {
	            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	        });
	        saveAs(blob, "OrderSearch.xls");
		};
	};
	
	$scope.exportOrderTableAsPDF=function(){
		var orderSearchTable = setOrderSearchDataforExport();
		var data = orderSearchTable.getHTML();
		var title = Util.translate('CON_ORDER_SEARCH');
		var fileName = "OrderSearch_"+getCurrentDateTimeStr()+".pdf";
		savePdfFromHtml(data,title,fileName);
		//console.log("exportOrderTableAsPDF -  table Object "+ JSON.stringify(data));
	};
	$scope.exportOrderTableAsExcel=function(){
		var orderSearchTable = setOrderSearchDataforExport();
		var data = orderSearchTable.getDataWithHeaders();
		var sheetName = "OrderSearch";
		var fileName = "OrderSearch_"+getCurrentDateTimeStr()+".xlsx";
		//$rootScope.exportTableAsExcel(data,title,fileName);
		saveExcel(data,sheetName,fileName);
		//console.log("exportOrderTableAsExcel -  table Object "+ JSON.stringify(data));
	};
	
	$scope.exportOrderTableAsXML=function(){
		var orderSearchTable = setOrderSearchDataforExport('XML');
		orderSearchTable.setRootElementName("SALES_ORDER_SEARCH");
		orderSearchTable.setEntityName("ORDER_DETAIL");
		var data = orderSearchTable.getXML();
		var fileName = "OrderSearch_"+getCurrentDateTimeStr()+".xml";
		saveXML(data,fileName);
		//console.log("exportOrderTableAsXML -  table Object "+ JSON.stringify(data));
	};
	
	$scope.getDataforExport = function(){
		var orderSearchTable = setOrderSearchDataforExport();
		var str = orderSearchTable.getHTML();
		//$log.log("order export html table  "+ JSON.stringify(str));
		
		return str;
	};
	
	
	//$scope.orderData
	var setOrderSearchDataforExport = function(target){
		var orderSearchTable = new Table();
		if(isEqualStrings(target,"XML")){
			orderSearchTable.addHeader('ORDER');
			orderSearchTable.addHeader('CUSTOMER');
			orderSearchTable.addHeader('ORDER_DATE');
			orderSearchTable.addHeader('STATUS');
			orderSearchTable.addHeader('ORDER_VALUE');
			orderSearchTable.addHeader('HANDLER');
			orderSearchTable.addHeader('YOUR_ORDER_NUMBER');		
		}else{
			orderSearchTable.addHeader(Util.translate('CON_ORDER'));
			orderSearchTable.addHeader(Util.translate('COH_CUSTOMER'));
			orderSearchTable.addHeader(Util.translate('CON_ORDER_DATE'));
			orderSearchTable.addHeader(Util.translate('CON_STATUS'));
			orderSearchTable.addHeader(Util.translate('CON_ORDER_VALUE'));
			orderSearchTable.addHeader(Util.translate('CON_HANDLER'));
			orderSearchTable.addHeader(Util.translate('CON_YOUR_ORDER_NUMBER'));
		}
		//set row data
		data = $scope.orderData;
		for(var i=0;i<data.length;i++){
			if(data[i].isOrderValueFlag){
				var row = [data[i].orderNumber, data[i].customer,data[i].orderDate,data[i].status ,'' ,data[i].handler ,data[i].yourOrderNo];
				orderSearchTable.addRow(row);
			}else{
				var row = [data[i].orderNumber, data[i].customer,data[i].orderDate,data[i].status ,data[i].orderValue ,data[i].handler ,data[i].yourOrderNo];
				orderSearchTable.addRow(row);
			}
		}
		//console.log("order export table  "+ JSON.stringify(orderSearchTable));
		return orderSearchTable;
	};
  
	
// 
//  $scope.copyPasteText= {};
//  $scope.addOrderRowList = {};
//  $scope.rowList = [0,1,2,3,4,5];
//  //
//  var delimeter = ";";
//  $scope.showAddSection = false;
//  $scope.showCopySection = false;
  $scope.getCopyOrderLines = function(id)
  {
	  if(!$rootScope.isShowDisableMenuItem('CON_COPY/PASTE_ORDER_LIN')){
			return;
	  }
	  $scope.validateDateRangeFormatShoppingCart("",true);
	  $scope.selectedIdForOrder = id;
	  $scope.showAddSection = !$scope.showAddSection;
	  $scope.showCopySection = false;
	  if($scope.showAddSection)
	  {
		  getPrePopulatedOrder(id);
	  }
	  //console.log("$scope.showAddSection : " + $scope.showAddSection);  
  };
  
  $scope.showCopyOrderLines = function(id,orderText)
  {
		var validOrder = $scope.copyOrder.StringToList(orderText);
	  if(validOrder)
		  {
		  	$scope.selectedIdForOrder = id;
		  	$scope.showAddSection = true;
		  	$scope.showCopySection = false;
		  	$scope.validateDateRangeFormatShoppingCart("",true);
		  }
	  
	  //console.log("$scope.showAddSection : " + $scope.showAddSection);  
  };

  
$scope.showCopyPaste = function(id)
{
	if(!$rootScope.isShowDisableMenuItem('CON_COPY/PASTE_ORDER_LIN')){
		return;
	}  
	var needToLogin = $rootScope.isSignOnRequired("CON_COPY/PASTE_ORDER_LIN",null,false);
	  if(!needToLogin){
		  $scope.selectedIdForCP = id;
		  $scope.showCopySection = !$scope.showCopySection;  
		  $scope.showAddSection = false;
		  if($scope.showCopySection){
			  getPrePopulatedOrder(id);
		  }
	  }

  };

$scope.isCopyOrderVisible = function(orderNumber)
{ var visible;
  if($scope.selectedIdForOrder == orderNumber){ 
	   visible =   $scope.showAddSection;
	
  }else{
	  visible  = false;
	
  }
  return visible;
};


$scope.isCopyPasteVisible = function(orderNumber)
{ var visible;

  if($scope.selectedIdForCP == orderNumber){	
	  visible =   $scope.showCopySection;	
  }else{
	  visible  = false;	
  }
  return visible;
};
  //

$scope.addItemsToCart = function (param){
//	$log.info('Order Add Items to cart : ' + param);
//	
//	Util.postDataToServer('addItemsToCartUrl',param);
	
};


var getPrePopulatedOrder = function(orderNumber)
{
	

		if( orderNumber !=null){
			requestParams = ["OrderNumber",orderNumber];
			OrderService.getOrderDetailList(requestParams).then(function(data){
				if(data.orderLineList==null || data.orderLineList.length == 0){
					$scope.copyOrder = null;
					$scope.errorMessage = data.errorMessage;		
					$scope.addOrderRowList[orderNumber] = $scope.rowList;
				}
				else
				{
					//console.log("orderDetailCtrl messageCode -- "+data.messageCode);


					if(data.orderLineList != null)
					{
						addpropertyToList(data.orderLineList,'validQuantity',true);
						$scope.copyOrder =  new MultipleAdd(data.orderLineList,$rootScope,Util.translate,$rootScope.CopyOrderDelimeter);
						$scope.copyOrder.populateLines();
						if($scope.showCopySection)
						{  	$scope.copyOrder.ListToString();

						}



					}

				}

			});		

		} 
	
};

$scope.addOrderLinesToCurrentList = function(orderNumber){
	var itemObj;
	var items=[];	
	if( orderNumber !=null){
		requestParams = ["OrderNumber",orderNumber];
		OrderService.getOrderDetailList(requestParams).then(function(data){
			if(data.orderLineList==null || data.orderLineList.length == 0){
				var msg = '';
				var translatedMsgList = [];
				translatedMsgList.push(Util.translate('MSG_ORDER_HAS_NO_ORDER_LINES'));
				$rootScope.openMsgDlg(msg,translatedMsgList);
			}
			else
			{
				if(data.orderLineList != null)
				{
					for(var i=0;i< data.orderLineList.length;i++){
						itemObj=data.orderLineList[i];
						var paramObject = new Object();
						paramObject.code = itemObj.itemCode;
						paramObject.salesUnit = itemObj.unit;
						paramObject.quantity = itemObj.ordered;
						if($rootScope.webSettings.allowDecimalsForQuantities == true){
							paramObject.quantity = paramObject.quantity.replace(",",".");
						}else{
							paramObject.quantity = parseIntegerQuantity(paramObject.quantity);
						}
						
						
						items.push(paramObject);
					}
					$rootScope.addOrderLinesToShoppingList(items);
				}

			}
		});		

	} 
	
};

$scope.$watch('buyItemWatch',function() {
	$scope.showAddSection=false;
});

orderHistoryCtrlInit();
}]);