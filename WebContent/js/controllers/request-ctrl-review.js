catalougeControllers.controller('requestCtrlReview', ['$rootScope','$scope', '$http','$log', '$q','DataSharingService','RequestSearchService', 'Util','$state','$filter',
                                                function ($rootScope,$scope, $http,$log, $q,DataSharingService,RequestSearchService,Util,$state,$filter) {
	
	setWebSettingObject(DataSharingService.getWebSettings());
	$scope.requestDate = new Date();
	$scope.requestDate = $filter('date')($scope.requestDate, webSettings.defaultDateFormat);
	$scope.showRequestLine = true;
//	$scope.requestLineObj =	DataSharingService.getObject("requestLineObject");
//	$scope.requestObject =	 DataSharingService.getObject("requestBPObject");
//	$scope.requestForlabel = DataSharingService.getObject("requestForlabel");
	

	
	$scope.requestLineObj = DataSharingService.getFromSessionStorage("pageRequestLineObject");
	$scope.requestObject = DataSharingService.getFromSessionStorage("pageRequestBPObject");
	$scope.requestForlabel = DataSharingService.getFromSessionStorage("pageRequestForlabel");
	
	$scope.responseMsgObject = DataSharingService.getFromSessionStorage("responseMsgObject");
	
	//var requestParam = DataSharingService.getObject("newRequestParam");	
	var requestParam = DataSharingService.getFromSessionStorage("pageNewRequestParam");	
	var requestLineParam = $scope.requestLineObj.requestParam;
	
	
	if(isEmptyString(requestLineParam))
		{
			$scope.showRequestLine = false;
		}
	
	$scope.requestMessage = "";
	$scope.backToRequest = function()
	{
		DataSharingService.setObject("fromState",REQ_SUBMIT_CONFIRMATION);
		$rootScope.goBack();
		
	};
	
	
	var getRequestReceivedMessage = function(data,isSucess)
	{
		var requestMsg = "";
		if(isSucess){
		requestMsg = Util.translate('MSG_REQUEST_YOUR_NUMBER')+" : "+ data.requestNumber+" "+ Util.translate('MSG_WAS_SUBMITTED_SUCCESSFULLY');
		}
		else{
			requestMsg = Util.translate('MSG_REQUEST_ERROR');
		}
			
		return requestMsg;
	};
	
	var validateNewRequestResponse = function(data)
	{
		var Msg = "";
		var Msg1 ="";
		var isRequestCreated = false;
		$scope.requestLineError = false;
		var lineStatusCodes = [];
		responseMsgObject = new RequestResponseInfo($rootScope,Util.translate);		
		if(!isNull(data) && !isEmptyObject(data)) {
			if(angular.isDefined(data.messageCode))	{
				if(data.messageCode == INPUT_PARAMETER_NOT_VALID)
				{	Msg = Util.translate('MSG_REQUEST_PARAMETERS_INVALID');
					isRequestCreated = false;
					lineStatusCodes = [];
				}
				responseMsgObject.setRequestStatus(null,null,lineStatusCodes,Msg,isRequestCreated,null);
			}
			else
			{
				if(data[0].messageCode == REQUEST_SUCCESSFULL)
				{
					Msg = getRequestReceivedMessage(data[0],true);	
					isRequestCreated = true;
					if(data[0].lineStatusCodes == 4401){
						Msg1 = Util.translate('CON_REQUEST_LINE_ERROR');
						$scope.requestLineError = true;
					}
					lineStatusCodes = data[0].lineStatusCodes;
                    if(!isNull($rootScope.deleteCachedBPRequestData)){
                    	$rootScope.deleteCachedBPRequestData();
                    }
				}
				else if(data[0].messageCode == REQUEST_ERROR)
				{
					isRequestCreated = false;
					Msg = getRequestReceivedMessage(data[0],false);
					lineStatusCodes = data[0].lineStatusCodes;

				}
				else if(data[0].messageCode == REQUEST_LINE_ERROR)
				{
					isRequestCreated = false;
					Msg = getRequestReceivedMessage(data[0],false);
					lineStatusCodes = data[0].lineStatusCodes;

				}

				responseMsgObject.setRequestStatus(data[0].requestNumber,data[0].messageCode,lineStatusCodes,Msg,isRequestCreated,Msg1);
			}
		}
		else
		{
			lineStatusCodes = [];
			isRequestCreated = false;
			Msg = Util.translate('MSG_REQUEST_ERROR');
			responseMsgObject.setRequestStatus(null,null,lineStatusCodes,Msg,isRequestCreated,null);

		}
		
		DataSharingService.setToSessionStorage("responseMsgObject",responseMsgObject);
		
	};
	$scope.createRequest = function()
	{
		var needToLogin = $rootScope.isSignOnRequired('CON_REQUEST_RECEIVED_CONFIRMATION',null,false);
		if(!needToLogin){
			RequestSearchService.submitNewRequest(requestParam).then(function(data){
				validateNewRequestResponse(data);
				$state.go(REQ_SUBMIT_RECEIVED);
			});
		}

	};
	
		
	$scope.backToRequestSearch =  function()
	{
		var callState = 'home.request';
		$state.go(callState);		
		
	};
	

}]);