catalougeControllers.controller('topItemCtrl', ['$rootScope','$filter','$scope','$stateParams', '$http','$log', '$q','DataSharingService','TopItemService', 'Util','$state',
                                                function ($rootScope,$filter,$scope,$stateParams, $http,$log, $q,DataSharingService,TopItemService,Util,$state) {

	var topItemCtrlInit = function(){
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	//	$log.log(" topItemCtrl history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	
	var initController = function()
	{ 
		$scope.filterOn = false;
		$rootScope.initCommitsSecondNextPromise.then(function(){
			$scope.topItemShowExtendInfo ='false';
			makeTopSearchFilters();
	 		$scope.TopItems = new MyTopItems();
			$scope.cachedPaginationObj = null;
	 		$scope.TopItems.sort.DESC('TotalAmount');
	 		var params = $scope.TopItems.getSearchParams(); 		
	 		getTopItemsPageData(params,FirstPage);
		});
		
	};
	
	// Top Items Filter Starts
	var makeTopSearchFilters = function(){
		var finalFilterList = new FilterList();
		var staticFilterConfig = TopItemService.getFilterConfiguration();
		var filterDataList = DataSharingService.getFilterDetailsSettings();
		var filterExist = false;
		if(isNull(filterDataList.requestStatusList)){
			filterExist = false;
		}else{
			filterExist = true;
		}
		
		
		if(filterExist ==  true){
			var dynamicFilterConfig = null;
			TopItemService.getTopItemsFilterList().then(function(data){
					dynamicFilterConfig = data.filterBeans;
					finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
					finalFilterList.setDateRangeFilterValidityMessage();
					$scope.TopItems.setFilterList(finalFilterList);
					//console.log("makeTopSearchFilters : " + JSON.stringify(finalFilterList));
			});
		}else{
			Util.getFilterListDetails([]).then(function(data){
				DataSharingService.setFilterDetailsSettings(data);
				filterDataList = DataSharingService.getFilterDetailsSettings();
				var dynamicFilterConfig = null;
				TopItemService.getTopItemsFilterList().then(function(data){
					dynamicFilterConfig = data.filterBeans;
					finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
					finalFilterList.setDateRangeFilterValidityMessage();
					$scope.TopItems.setFilterList(finalFilterList);
					//console.log("makeTopSearchFilters : " + JSON.stringify(finalFilterList));
			});
			});
		}
		
	};
	//applying filter 
	$scope.submitFilter = function(){
		$scope.TopItems.sort.DESC('TotalAmount');
		$scope.TopItems.isfilterOn = true;
		$scope.filterOn = true;
		var params = $scope.TopItems.getSearchParams();		
		getTopItemsPageData(params,FirstPage);
	};
	// sorting 
		
	$scope.sortASC = function(column){
		$scope.TopItems.sort.ASC(column);
		sorting();
	};
	$scope.sortDESC = function(column){
		$scope.TopItems.sort.DESC(column);
		sorting();
	};
	var sorting = function(){
		var params = $scope.TopItems.getSearchParams();		
		getTopItemsPageData(params,FirstPage);
	};
	
	

//	var setUIData = function(data,isFilterOn,isPageData){
//		if(isNull(data[0].recordSet) || data[0].recordSet[0].messageCode==7001 || data[0].messageCode==7001)		
//		{						
//			$scope.paginationObj.setLastPageNo();				
//			$scope.dataFound = false;
//			$scope.TopItems.pageDataFound = false;
//			$scope.pageLoadMsg = Util.translate('CON_NO_MORE_PAGE'); 
//			$log.log("topItemCtrl -- " + $scope.pageLoadMsg);
//			$scope.topItemSearchFilterList.applyPreviousFilters();
//		}
//		else
//		{
//			$scope.dataFound = true;
//			$scope.pageLoadMsg = "";
//			$scope.sortColumn = data[0].sortingBean.sortColumn;  // to be used after final web service deployed
//			$scope.orderType = data[0].sortingBean.orderBy;  // to be used after final web service deployed
//			addpropertyToList(data[0].recordSet,'validQuantity',true);		
//			$scope.topItemSearchFilterList.cacheFilters();
//			$scope.TopItems.filterParam = $scope.topItemSearchFilterList.getParamsList();
//
//		}
//	
//		$scope.TopItems.setItemsList(data[0].recordSet,isFilterOn,isPageData); // to be used after final web service deployed				
//		$scope.setTopItemExtendedData($scope.TopItems.getItemsList());		
//		$scope.initBuyObject($scope.TopItems.getItemsList());
//		
//	};
	
	var setUIPageData = function(data,isFirstPageData){
		 var ItemList = data[0].recordSet;
		 var messageCode =  null;
		if(isNull(ItemList) || ItemList.length == 0){
			if(data[0].statusBean) messageCode = data[0].statusBean.messageCode;
		}
		if(!isNull(messageCode) && messageCode==7001){
			$scope.TopItems.setItemList(null,isFirstPageData); 
			if(isFirstPageData){
				$rootScope.applyPreviousPaginationObject('myTopItemsUrl' , $scope);
			}
			//$rootScope.setPageLoadMsg(data[0].recordSet[0],isFirstPageData,$scope);
			
		}else{
			addpropertyToList(data[0].recordSet,'validQuantity',true);		
			$scope.TopItems.setItemList(data[0].recordSet,isFirstPageData); 
			$scope.moreDataArrow = data[0].moreRecords;
			$rootScope.cachePaginationObject('myTopItemsUrl' , ItemList, $scope);
			if(isFirstPageData && (! Array.isArray(ItemList) ||  !ItemList.length > 0 ) ){
				$rootScope.applyPreviousPaginationObject('myTopItemsUrl' , $scope);
			}
			//$rootScope.setPageLoadMsg(ItemList,isFirstPageData,$scope);
		}
		$rootScope.setPageLoadMsg(ItemList,isFirstPageData,$scope);
		$scope.setTopItemExtendedData($scope.TopItems.getItemsList());		
		$scope.initBuyObject($scope.TopItems.getItemsList());
	};
	
//	// fetching the top item service data
//	var getMyTopItems = function(requestParam,isFilterOn){
//		TopItemService.getTopItemList(requestParam).then(function(data){ // to be used after final web service deployed	 
////		
//			setUIData(data,isFilterOn,false);
//			$scope.$root.$eval();
//			$rootScope.scrollToId("result_set");
//
//		});
//	};
	// Pagination code Starts	
	
	var getTopItemsPageData = function(requestParams,page)
	{
		$scope.paginationObj = $rootScope.getPaginationObject('myTopItemsUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('myTopItemsUrl',requestParams,page ).then(function(data){
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true);
				}else{
					setUIPageData(data,false);
				}
			});
		}
	};
	
	$scope.previousPage = function(){		
		var params = $scope.TopItems.getSearchParams();		
		getTopItemsPageData(params,PrevPage);		
	};
	$scope.nextPage = function(){	

		var params = $scope.TopItems.getSearchParams();		
		getTopItemsPageData(params,NextPage);	
		
	};
			
	$scope.getFirstPage = function()
	{	
		var params = $scope.TopItems.getSearchParams();		
		getTopItemsPageData(params,FirstPage);
		
				
	};
	//  Pagination code ends
	
	
	// Exporting top items data
	$scope.exportTopItemsList = function(fileType)
	{
		var topItemsList = new Table();
		var data;
		var filePrefix = "MyTopItemsList";
		var title = Util.translate('CON_MY_TOP_ITEMS');
		var sheetName = filePrefix ;
		switch(fileType){
		
		case 'pdf' :
					 topItemsList = setTopItemsDataforExport();
					 data = topItemsList.getHTML();												
					 var fileName = $rootScope.getExportFileName (filePrefix,".pdf");
					 savePdfFromHtml(data,title,fileName);
					 //console.log("exportTopItemsListAsPDF -  table Object "+ JSON.stringify(data));
					 break;
			 
		case 'xls' :
					topItemsList = setTopItemsDataforExport();
					var data = topItemsList.getDataWithHeaders();
					var fileName = $rootScope.getExportFileName (filePrefix,".xlsx");						
					//$rootScope.exportTableAsExcel(data,title,fileName);
					saveExcel(data,sheetName,fileName);
					//console.log("exportTopItemsListAsExcel -  table Object "+ JSON.stringify(data));
			
			
					break;
		case 'xml' :
					topItemsList = setTopItemsDataforExport('XML');
					topItemsList.setRootElementName("MY_TOP_ITEMS");
					topItemsList.setEntityName("MY_TOP_ITEMS_DETAIL");
					var data = topItemsList.getXML();
					var fileName = $rootScope.getExportFileName (filePrefix,".xml");						
					saveXML(data,fileName);
					//console.log("exportTopItemsListAsXML -  table Object "+ JSON.stringify(data));
					break;
					
		default :
				
			//console.log("Exporting Top Items List - failed");
		 
		
		}
		
	};
			
	var setTopItemsDataforExport = function(target){
		var topItemsList = new Table();
		if(isEqualStrings(target,"XML")){
			topItemsList.addHeader('COH_TOTAL_AMOUNT');
			topItemsList.addHeader('COH_QUANTITY');
			topItemsList.addHeader('COH_LINES');
			topItemsList.addHeader('COH_ITEM');
			topItemsList.addHeader('COH_DESCRIPTION');
			topItemsList.addHeader('COH_ACTUAL_PRICE');
			topItemsList.addHeader('COH_DISCOUNT_PRICE');
			topItemsList.addHeader('COH_DISCOUNT_PERCENTAGE');
			topItemsList.addHeader('COH_UNIT');		
			
			
			
		}else{
			
			topItemsList.addHeader(Util.translate('COH_TOTAL_AMOUNT'));		
			topItemsList.addHeader(Util.translate('COH_QUANTITY'));
			topItemsList.addHeader(Util.translate('COH_LINES'));
			topItemsList.addHeader(Util.translate('COH_ITEM'));		
			topItemsList.addHeader(Util.translate('COH_DESCRIPTION'));		
			topItemsList.addHeader(Util.translate('COH_ACTUAL_PRICE') +" "+ webSettings.currencyCode);		
			topItemsList.addHeader(Util.translate('COH_DISCOUNT_PRICE') +" "+ webSettings.currencyCode);		
			topItemsList.addHeader(Util.translate('COH_DISCOUNT_PERCENTAGE'));
			topItemsList.addHeader(Util.translate('COH_UNIT'));		
			
								
		}
		//set row data
		
		var data = 	$scope.TopItems.getItemsList();
		if(!isNull(data)){
			for(var i=0;i<data.length;i++){						
				var row = [data[i].totalAmount,  data[i].totalQuantity , data[i].lines, data[i].item_code,data[i].description, data[i].actualPrice ,data[i].discountPrice,data[i]. discountPercentage,data[i].unit_code	];				
				topItemsList.addRow(row);
			}
		}else{
			var row = [];			
			for(var i=0;i<topItemsList.headers.length;i++){
				row.push("");
			}
			topItemsList.addRow(row);
		}
		
		//console.log("top items export table  "+ JSON.stringify(topItemsList));
		return topItemsList;
	};
	// Exporting top items ends
	
	//on Unit Change Starts
	
	$rootScope.onUnitMyTopItem= function(itemCode,unitCode,oldCatalogItem){
		//uncomment below code after Serverside changes
		$rootScope.getAndUpdateCatalogItem(itemCode,unitCode,oldCatalogItem);
	};
	
	
	$scope.getAndUpdateMyTopItem = function(itemCode,unitCode,oldCatalogItem){
		if(!isEmptyString(itemCode) && !isEmptyString(unitCode) ){
			var prams = ["ITEM_CODE",itemCode,"UNIT_CODE",unitCode];
		
			var deferred = $q.defer();
			Util.getDataFromServer('getSingleItemDetailUrl',prams,false).then(function(data){
				if(!isNull(data) && Array.isArray(data.productList) && data.productList.length > 0){
					newCatalogItem = data.productList[0];
					if(angular.isDefined(oldCatalogItem.checked) && oldCatalogItem.checked!=null){
						newCatalogItem['checked']=oldCatalogItem.checked;
					}
					
					copyConsideringSearch(oldCatalogItem, newCatalogItem);
					oldCatalogItem.salesUnit=unitCode;
					oldCatalogItem.isBuyAllowed = newCatalogItem.isBuyingAllowed;
					oldCatalogItem.selectedUnit = oldCatalogItem.salesUnit;
					oldCatalogItem.selectedUnitObject = getSelectedSalesUnit(unitCode,oldCatalogItem.salesUnitsDesc);
				}
				deferred.resolve(data);	
				
			});
		}
	};

	
	
	//On Unit Change Ends
	
	
	
	
	// buying top items from list 
	$scope.initBuyObject = function(list){
		$scope.buyObj = new Buy(list);
		$scope.buyObj.setPropNameItemCode('item_code');
		$scope.buyObj.setPropNameOrdered('quantity');
		$scope.buyObj.setPropNameUnit('unit_code');
		$scope.buyObj.setPropNameisBuyAllowed('isBuyAllowed');
	};
	
	$scope.buyTopItems = function(item){
		$rootScope.buyMultipleItems(item,$scope.buyObj);
	};
	
	$scope.addMultipleItemsToCurrentList = function(item){
		$rootScope.addMultipleItemsToList(item,$scope.buyObj);
	};
	
	var setSalesUnitProperty = function(list)
	{
		
		for (var i=0;i<list.length ; i++){
			var  item  = list[i];
			var unitList = [];
			unitList[0] = item.unit_code; 
			item['salesUnitsList'] = unitList;
			
		}
		return list;

	};
	$scope.setTopItemExtendedData = function(list){
		//$log.log("setHomeCatalogExtendedData : product list : " +JSON.stringify($scope.productList));
		if(Util.isNotNull(list)){
			list = setSalesUnitProperty(list);
			for (var i=0;i<list.length ; i++){
				var  item  = list[i];
				item.showAvailabilityAsLink = $rootScope.showAvailabilityAsLink();
				item.showAvailabilityAsImage = $rootScope.showAvailabilityAsImage();
				if(!isNull(item.availabilityOfItem)){
					item.availabilityImageName = $rootScope.getAvailabilityImage(item.availabilityOfItem);
					item.availabilityText = $rootScope.getAvailabilityText(item.availabilityOfItem);
				}else{
					item.availabilityImageName = $rootScope.getAvailabilityImage(item.whs);
					item.availabilityText = $rootScope.getAvailabilityText(item.whs);
				}
				handleEmptyAvailabilityImage(item);
				//append % if discountPercentage has value
				if(!isEmptyString(item. discountPercentage)){
					item.discountPercentageText = item. discountPercentage + " %";
				}
				if(!item.hasOwnProperty("unit_code")){
					item.salesUnit=item.defaultSalesUnit;
				}
				
				if(!item.hasOwnProperty("salesUnitObject")){
					item.selectedUnitObject= {};
					item.selectedUnitObject = getSelectedSalesUnit(item.unit_code,item.salesUnitsDesc);
				}
			}
		}
	};
	$rootScope.showPriceCalcDlgFromTopItems = function(item){
		if(!$rootScope.isPriceCalculationEnabled) return;
		var needToLogin = $rootScope.isSignOnRequired("CON_PRICE_CALCULATION",$state.current.name);
		if(!needToLogin){
				$rootScope.priceCalDate = new Date();
				$rootScope.priceCalDate = $filter('date')($rootScope.priceCalDate, $rootScope.webSettings.defaultDateFormat);
				var priceCalcObj = new PriceCalculation (item,$rootScope);
				
				priceCalcObj.itemCodePropName =  "item_code";
				priceCalcObj.descCodePropName =  "description";
				priceCalcObj.unitPropName =  "unit_code";
				priceCalcObj.pricePropName =  "actualPrice";
				priceCalcObj.salesUnitsPropName =  "salesUnitsDesc";
				priceCalcObj.buyAllowedPropName =  "isBuyAllowed";
				priceCalcObj.init();
				$rootScope.validateDateRangeFormatDialog("",true);
				$rootScope.openPriceCalcDlg("Price cal", priceCalcObj);
		}
	};
	
	topItemCtrlInit();
}]);