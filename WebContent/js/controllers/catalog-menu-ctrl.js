catalougeControllers.controller('catMenuCtrl', ['NewConfigService','DataSharingService','MainCatalogService','Util','$rootScope','$scope', '$http','$log','$timeout','$state',
                                                function (NewConfigService,DataSharingService,MainCatalogService,Util,$rootScope,$scope, $http,$log,$timeout,$state) {
	
	//to prevent twice call main catalog ctrl. use gridView as default view for main catalog
	if(isNull($rootScope.mainCatView) || $rootScope.mainCatView ==""){
		$rootScope.mainCatView = "gridView";
	}
	$scope.clicked  = false;
	$scope.currentMainMenuPage = 1;
	$scope.nextArrow = true;
	$scope.nomorePrevRecords = false;
	$scope.catDescription  = "";
	$rootScope.initCommitsPromise.then(function(){
		var langCode =NewConfigService.getLanguageCode();
		var prams = ["LanguageCode",langCode,"PageNo",$scope.currentMainMenuPage];
		Util.getDataFromServer('mainCatelogUrl',prams).then(function(data){
			if (data.catalogueBeanList.length > 0 ){
				$scope.catData = data.catalogueBeanList[0].subCatalogueBeanList;
				$scope.catalogue = $scope.catData;
				$scope.nextArrow =data.catalogueBeanList[0].moreRecords;
			}else{
				$scope.catalogue = [];
			}
			//$rootScope.showCanvas = true;
			var groupMenuSlider = new MenuSlider($scope.catalogue);
			//$scope.menuSlider = $scope.catalogue;
			$scope.menuSlider = $scope.catalogue ;
		});
	});
	
	$scope.hideSubmenu = function(){
		$scope.selected = null;
	}
	
	$scope.callLoadFunction = function(mainCat,ix,loader){
		
		$scope.selected = null;
		if(ix!=null && typeof(ix!="undefined")){
			$scope.selected = ix;
		}
		$scope.samleData= "";
	//	var currPage = $state.current.name;
		if(($scope.catDescription != mainCat.description) ){
			$scope.catCodeMainCat = "";
			$scope.catalogData="";
			$scope.catDescription = "";
			if(!isNull(mainCat.elementCode)){
				$scope.catCodeMainCat = mainCat.elementCode;
			}

			var langCode =NewConfigService.getLanguageCode();
			var prams = ["LanguageCode",langCode, "ElementId",$scope.catCodeMainCat, "ElementDesc",mainCat.description];
			Util.getDataFromServer('mainCatelogSubCatUrl',prams,loader).then(function(data){
					$scope.catData = data;
					$scope.catDescription = data.elementRootDesc;
					$scope.catalogData = $scope.catData;
					$scope.clicked = false;
					if(!isNull($scope.catalogData.subCatalogueBeanList)){
						var catLength = $scope.catalogData.subCatalogueBeanList;
					if(catLength.length > 1){
						$scope.clicked = true;
					} else if(catLength.length == 0){
						$rootScope.showCatalogFromMain(mainCat);
					} else{$scope.clicked = false;}
		
					}
					
				
			});

		}
		
		
		
		

	};
	

	$rootScope.nextMainMenuPage = function(moreRecord) {
		if(moreRecord == true){
	      $scope.currentMainMenuPage++;
	      if($scope.currentMainMenuPage > 1){
		    	$scope.nomorePrevRecords = true;
		    }
			$rootScope.initCommitsPromise.then(function(){
				var langCode =NewConfigService.getLanguageCode();
				var param = ["LanguageCode",langCode,"PageNo",$scope.currentMainMenuPage];
				Util.getDataFromServer('mainCatelogUrl',param).then(function(data){
					if (data.catalogueBeanList.length > 0 ){
						$scope.catData = data.catalogueBeanList[0].subCatalogueBeanList;
						$scope.nextArrow = data.catalogueBeanList[0].moreRecords;
						$scope.catalogue = $scope.catData;
						$scope.menuSlider = $scope.catalogue;
					}else{
						//$scope.catalogue = [];
					}
					//$rootScope.showCanvas = true;
					//var groupMenuSlider = new MenuSlider($scope.catalogue);
					$scope.menuSlider = $scope.catalogue;
					//$scope.$root.$eval();
					//$rootScope.showCanvas = false;
				});
			});
		}
	  };
	  
	  
	  $scope.prevMainMenuPage = function() {
		    if ($scope.currentMainMenuPage > 1) {
		      $scope.currentMainMenuPage--;
		      if($scope.currentMainMenuPage <= 1){
		    	  $scope.nomorePrevRecords = false;
		      }
				$rootScope.initCommitsPromise.then(function(){
					var langCode =NewConfigService.getLanguageCode();
					var param = ["LanguageCode",langCode,"PageNo",$scope.currentMainMenuPage];
					Util.getDataFromServer('mainCatelogUrl',param).then(function(data){
						if (data.catalogueBeanList.length > 0 ){
							$scope.catData = data.catalogueBeanList[0].subCatalogueBeanList;
							$scope.nextArrow = data.catalogueBeanList[0].moreRecords;
							$scope.catalogue = $scope.catData;
							$scope.menuSlider = $scope.catalogue;
						}else{
							//$scope.catalogue = [];
						}
						//$rootScope.showCanvas = true;
						//var groupMenuSlider = new MenuSlider($scope.catalogue);
						$scope.menuSlider = $scope.catalogue;
						//$scope.$root.$eval();
						//$rootScope.showCanvas = false;
					});
				});
		    }
		    
		  };
	
		  
    
	$rootScope.menuLanguageChange = function(languageCode){
		
		prams = ["LanguageCode",languageCode.code];
		Util.getDataFromServer('mainCatelogUrl',prams).then(function(data){
			if (data.catalogueBeanList.length > 0 ){
				$scope.catData = data.catalogueBeanList[0].subCatalogueBeanList;
				$scope.catalogue = $scope.catData;
			}else{
				$scope.catalogue = [];
			}
			//$rootScope.showCanvas = true;
			var groupMenuSliderLang = new MenuSlider($scope.catalogue);
			$scope.menuSlider = $scope.catalogue ;
			//$scope.$root.$eval();
			//$rootScope.showCanvas = false;
		});
	
		
	};
	
//	var iscatalogView = function (){
//		var name = $state.current.name;
//		var bool = false;
//		if(name == "home.mainCatalog"){
//			bool = true;
//		}
//		return bool;
//	};
	
	$scope.loadNewFilters = function(menu){
		var langCode = NewConfigService.getLanguageCode();
		var prams = ["LanguageCode",langCode,"CatalogueCode",menu.catalogueCode,"ElementCode",menu.elementCode];
		Util.getProductFilterDetails(prams).then(function(data){
			DataSharingService.removeProductFilterDetailsSettings();
			DataSharingService.setProductFilterDetailsSettings(data);	
			$rootScope.productfilterDetailSettings = data;
			var paramObj = {CAT_ID: menu.catalogueCode, ELEMENT_ID: menu.elementCode,ELEMENT_DESC: menu.description, VIEW: $rootScope.mainCatView};
			paramObj = encodeObjectData(paramObj);
			var needToLogin = $rootScope.isSignOnRequired("CON_PRODUCT_CATALOGUE","home.mainCatalog",true,paramObj);
			var requestParams = ["PageNo","1","CAT_ID",menu.catalogueCode,"ELEMENT_ID",menu.elementCode,"OrderBy","","SortBy",""];
			MainCatalogService.getMainCatalogProducts(requestParams).then(function(data){
				if(data.messageCode == "2001"){
					$scope.filterMsg =  Util.translate('MSG_SELECT_ANOTHER_CATALOG',null);
					showMsgNoObjFound(data.messageCode);
					}else if(!needToLogin){
						$state.go("home.mainCatalog",paramObj);
					}
			});
		});
	};
	
	$rootScope.showCatalogFromMain = function(menu) {
		cbpHorizontalMenu.close();
		$rootScope.showCatalogFromMainCat(menu);
	};
	
	
	$rootScope.showMainCatalog = function(menu) {
		cbpHorizontalMenu.close();
		$scope.loadNewFilters(menu);
		
		
		
//		if(iscatalogView()){
//			$rootScope.showCatalog(paramObj);
//		}else if(!needToLogin){
//			$state.go("home.mainCatalog",paramObj);
//		}
	};
}]);