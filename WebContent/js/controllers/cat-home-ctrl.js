catalougeControllers.controller('catHomeCtrl', ['HomeCatalogService','ShoppingCartService','$rootScope','$scope', '$http','$log','$filter','$state','DataSharingService','Util','SessionTracking', 
                                                function (HomeCatalogService,ShoppingCartService,$rootScope,$scope, $http,$log,$filter,$state,DataSharingService,Util,SessionTracking) {
	//$http.get('./json/catalog/home_catalogue.json').success(function(data) {	
	//$http.jsonp($rootScope.HOME_CAT_URL).success(function(data) {\

	var homeCatalogCtrlSetup = function(){
		/*var lc = DataSharingService.getFromLocalStorage("languageChanged");
		if(lc == 1) {
			homeCatalogCtrlInit();
			DataSharingService.setToLocalStorage("languageChanged", 0);
			return;
		}*/
		if(historyManager.isBackAction && !isNull($scope.productList)){
			historyManager.loadScopeWithCache($scope);
			$scope.initBuyObject($scope.productList);
			$scope.SelectedProductList = DataSharingService.getFromLocalStorage("compareObjectList");
			if(isNull($scope.SelectedProductList)){
				$scope.SelectedProductList=[];
			}
			$scope.previousState = DataSharingService.getObject("prevCompareState");	
			$scope.$root.$eval();
		}else{
			homeCatalogCtrlInit();
		}
	};
	
	
	var homeCatalogCtrlInit = function(){
		$scope.TableViewName = 'tableView';
		$scope.GridViewName = 'gridView';
		$scope.disableBuyTillProcess = false;
		$scope.isMoreRecoreds = false;
		$scope.currentView = $scope.GridViewName;
		$scope.gridViewLabelName = Util.translate('CON_GRID_VIEW');
		$scope.tableViewLabelName = Util.translate('CON_TABLE_VIEW');
		$scope.sortColumn = "code";
		$scope.orderType = "ASC";
		$scope.filterOn = false;
		$scope.filterParam = [];
		$scope.homeCatalogShowExtendInfo='false';
		$scope.homeCatalogSort =  new Sort();
		$scope.productList = [];
		
		$rootScope.sortSelection = '';
		$rootScope.sortSelection =  $scope.sortColumn+$scope.orderType;
		
		$scope.homeCataloguePageIndex =1;
		if($state.current.name == 'home.Compare'){
			$scope.SelectedProductList = DataSharingService.getFromLocalStorage("compareObjectList");
			$scope.previousState = DataSharingService.getObject("prevCompareState");	

		}else{
			$scope.SelectedProductList = [];
		}

		$scope.productPropList = null;
		
		
		$rootScope.initCommitsPromise.then(function(){
			//$rootScope.changeHomePageTitle('TXT_PAGE_TITLE_HOME');
			initViewLabel();
			getBanner();

			getFirstPageData();
		});
		
		if($rootScope.isHomeCatalogShowExtendedInfo && $state.current.name == 'home.homeCatalogTable'){
			$scope.homeCatalogShowExtendInfo ='true';
		}else{
			$scope.homeCatalogShowExtendInfo ='false';
		}
	};
	
	$scope.onChangeExtendedInfo = function(){
		if($scope.homeCatalogShowExtendInfo == 'true'){
			$scope.homeCatalogShowExtendInfo ='false';
			$rootScope.isHomeCatalogShowExtendedInfo = false;
		}else{
			$scope.homeCatalogShowExtendInfo ='true';
			$rootScope.isHomeCatalogShowExtendedInfo = true;
		}
		//console.log(" homeCatalogShowExtendInfo : " + $scope.homeCatalogShowExtendInfo);
	};
	


	$scope.$on('$viewContentLoaded', function(event){ 
		$scope.ViewLabelName = 	Util.translate($state.current.labelKey);
		if(isEmptyString($scope.ViewLabelName)){
			$scope.ViewLabelName = Util.translate('CON_GRID_VIEW');
		}
	});
	
	$scope.$on('userLoggedIn', function(event){ 
		if($state.current.name=='home.Compare') {
			$scope.SelectedProductList = DataSharingService.getFromLocalStorage("compareObjectList");
			$scope.updateCount = 0; 
			for(var i = 0; i < $scope.SelectedProductList.length; i++) {
				$rootScope.getProductsDetail($scope.SelectedProductList[i].code, $scope.updateBuy);
			}
		}
	});

	// ADM-010
	$scope.$watch('freeSearchObj.packPiece',
		function(){
			if(!isNull($scope.productList)){
				$rootScope.refreshUIforPackPiece($scope.freeSearchObj.packPiece, 
				$scope.productList);
			}
		}
	);
	
	$scope.updateBuy = function(data) {
		for(var i = 0; i < $scope.SelectedProductList.length; i++) {
			if($scope.SelectedProductList[i].code == data.itemCode) {
				$scope.SelectedProductList[i].isBuyingAllowed = data.isBuyAllowed;
				break;
			}
		}
		$scope.updateCount++;
		if($scope.updateCount >= $scope.SelectedProductList.length) {
			$scope.compare();
		}
	}
	
	var initViewLabel = function() {
		$scope.gridViewLabelName = Util.translate('CON_GRID_VIEW');
		$scope.tableViewLabelName = Util.translate('CON_TABLE_VIEW');
		$scope.ViewLabelName =  DataSharingService.getObject("prevViewLabel" );	
		if(isNull($scope.ViewLabelName)){
			$scope.ViewLabelName = $scope.tableViewLabelName;
			$scope.isList = false;
		}else{
			$scope.ViewLabelName = $scope.gridViewLabelName;
			$scope.isList = true;
		}
	};
	
	var setHomeCatMessages = function (data){
		if(!isNull(data)){
			if(data.messageCode ==111){
				$scope.infoMsg = Util.translate('MSG_NO_OBJECTS_FOR_SELECTION');
				showMsgNoObjFound();
			}else if(data.messageCode == 3232){
				$scope.infoMsg = Util.translate('SP_MISSING_FOR_PRICE_IN_BULK');
				showMsgNoObjFound(data.messageCode);
			}else if(data.messageCode == 3233){
				$scope.infoMsg = Util.translate('SP_MISSING_FOR_AVAIL_IN_BULK');
				showMsgNoObjFound(data.messageCode);
			}
			else{
				setMessagesToNull();
			}
		}
	};

	$scope.bannerInterval = 3000;
	  $scope.bannerSlides = [
	    {
	      image: 'images/TestImg1.jpg'
	    },
	    {
	      image: 'images/TestImg2.jpg'
	    },
	    {
	      image: 'images/TestImg3.jpg'
	    },
	    {
	      image: 'images/TestImg2.jpg'
	    }
	  ];
	
	
	
	$scope.initBuyObject = function(list){
		$scope.buyObj = new Buy(list);
		$scope.buyObj.setPropNameItemCode('code');
		$scope.buyObj.setPropNameOrdered('quantity');
		$scope.buyObj.setPropNameUnit('salesUnit');
		$scope.buyObj.setPropNameisBuyAllowed('isBuyingAllowed');
	};
	
	$scope.buyItems = function(item,buyObj){
		$rootScope.buyMultipleItems(item,$scope.buyObj);
	};
	
	$scope.addMultipleItemsToCurrentList = function(item){
		$rootScope.addMultipleItemsToList(item,$scope.buyObj);
	};
	
	
	var getBanner = function(){
		$scope.emptyBannerList = false;
		if(!isNull($rootScope.webSettings) && !isNull($rootScope.webSettings.showBanner) && $rootScope.webSettings.showBanner){
			var bannerObj = DataSharingService.getObject("BANNER_OBJECT");
			if(isNull(bannerObj)){
				Util.getDataFromServer('getBannerUrl',[]).then(function(data){
					//$http.get('./json/catalog/banner.json').success(function(data){
					if(isNull(data) || isNull(data.bannerList)) {
						$scope.emptyBannerList = true;
					}
					$scope.banner = data;
					
					DataSharingService.setObject("BANNER_OBJECT",data );
				});
			}else{
				$scope.banner = bannerObj;
			}
		}	
	};
	var setHomeCatalogUIData = function (isFirstPageData, data) {
		if(data.moreRecords != undefined){
		$scope.isMoreRecoreds = data.moreRecords;
		}
		
		if(isFirstPageData){
			$scope.homeCataloguePageIndex ++;
			$scope.homeCatalogue = data;
			$scope.initForScrollLoading();
			$rootScope.initHomeCatalogTableHeaders();
			$rootScope.setHomeCatalogExtendedData(data.productList);
			$scope.productList = data.productList;
			$rootScope.setHomeCatalogTableHeaders($scope.productList);
			$scope.initBuyObject($scope.productList);
			$scope.productPropList = Util.getObjPropList(data.productList[0]);
			//$log.log("homeCatalogue -  items fetched : " +JSON.stringify(data));
			DataSharingService.setObject("CATALOG_PRODUCT_LIST",$scope.productList );	
		}else{
			$rootScope.setHomeCatalogExtendedData(data.productList);
			Util.appendItemsToList(data.productList, $scope.productList);
			$rootScope.setHomeCatalogTableHeaders($scope.productList);
			$scope.initBuyObject($scope.productList);
			DataSharingService.setObject("CATALOG_PRODUCT_LIST",$scope.productList );
		}
		/* ADM-010 */$rootScope.refreshUIforPackPiece($scope.freeSearchObj.packPiece,
				$scope.productList);

		if($state.current.name == 'home.mail'){
			$rootScope.showSendMailPopUp();
		}
	};
	
	var getFirstPageData = function(){
		$scope.homeCataloguePageIndex = 1;
		var params = $scope.homeCatalogSort.addSortParams(["PageNo",$scope.homeCataloguePageIndex]);
		params = params.concat(SessionTracking.getLanguageParams());
		var pageObj = $rootScope.getPaginationObject('homeCatelogUrl',params,null);
		$scope.pageKey = pageObj.objKey;
		
		$rootScope.openLoader();
		Util.getDataFromServer('homeCatelogUrl',params).then(function(data){
			$rootScope.closeLoader();
			if(!isNull(data.messageCode)){
				if(data.messageCode == '3232'){
					$rootScope.homeCatalogLoaded = true;
					$rootScope.spForBulkExists = false;
				setHomeCatMessages(data);
				}else{
					$rootScope.homeCatalogLoaded = true;
					setHomeCatalogUIData(true,data);
				}
			}else{
				$rootScope.homeCatalogLoaded = true;
				setHomeCatalogUIData(true,data);
			}
			
		});
	};	
	
	
	//$rootScope.webSettings = DataSharingService.getWebSettings();

	$rootScope.addToCart = function (itemCode,quantity,salesUnit){
		if(Util.isNotNull(itemCode) && Util.isNotNull(quantity) && quantity >0 && Util.isNotNull(salesUnit)) {
			var params =  ["itemCode",itemCode,"itemQty",quantity,"unitCode",salesUnit];
			ShoppingCartService.addToCart(params);
		}
	};

	$scope.gridViewBuyClicked = function(itemObj){
		//$scope.disableBuyTillProcess = true;
		if(!itemObj.validQuantity || isNull(itemObj.quantity) || ""==itemObj.quantity){
			itemObj.validQuantity = false;
			itemObj['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
		}else{
		itemObj.isBuyingAllowed = false;
		//console.log("buyClicked: Itemcode = " +itemObj.code + " quantity = " + itemObj.quantity + " salesUnit = " + itemObj.selectedUnitObject.salesUnit);
		//$rootScope.addItemToCart(itemObj.code, itemObj.quantity,$scope.buyItem[$rootScope.getId('salesUnit',itemObj.code)],itemObj,"quantity");
		$rootScope.addItemToCart(itemObj.code, itemObj.quantity,itemObj.salesUnit,itemObj,"quantity");
		//clearSingleQuantityTextBox(itemObj,"quantity",test);
		}
		
	};

	$scope.tableViewBuyClicked = function(itemObj){
		//console.log("buyClicked: Itemcode = " +itemObj.code + " quantity = " + $scope.buyItem[$rootScope.getId('quantity',itemObj.code)] + " salesUnit = " + itemObj.defaultSalesUnit);
		$rootScope.addItemToCart(itemObj.code,$scope.buyItem[$rootScope.getId('quantity',itemObj.code)],itemObj.defaultSalesUnit);
	};
	
	$scope.addItem = function(item)
    {
    	//item['quantity'] = $scope.buyItem[$rootScope.getId('quantity',item.code)];
    	$rootScope.addItemToCurrentList(item);    	
    };
    
	$scope.isColExist = function(colPropName){
		var isCol = false;
		var val = Util.isPropertyExist(colPropName,$scope.productPropList);
		if(val){
			isCol = true;
		}
		return isCol;
	};

	$scope.showHide = function(item){	  
		if(item === true){	return true;   }
		return false;
	};

	//$scope.defaultViewHeader =  HomeCatalogService.getDefaultViewTableHeaderList();
	//$scope.defaultHeaderList
	//$log.log("homeCatalogue.description = " + $scope.defaultViewHeader[0].displayName);
	$scope.sort = {
			column:'',
			descending: false
	};

	$scope.initForScrollLoading = function(){
		$scope.blockDataRequest = false;
		$scope.stopLoading = false;
	};

	$scope.isLoadAllowed= function(){
		if (!$scope.blockDataRequest && !$scope.stopLoading){
			return true;
		}else{
			return false;
		}
	};

	$scope.addItems = function(newList){
		for(var i = 0; i<newList.length ; i++){
			$scope.productList.push(newList[i]);
		}
	};

	$rootScope.homeCatalogLoadMore = function(){
		if($scope.isMoreRecoreds == true){
		var params = $scope.homeCatalogSort.addSortParams([]);
		params = params.concat(SessionTracking.getLanguageParams());
		var pageObj = $rootScope.getPaginationObject('homeCatelogUrl',params,$scope.pageKey);
		$scope.pageKey = pageObj.objKey;
		//console.log("homeCatalogLoadMore() - called, isPaginationBlocked = "+ Util.isPaginationBlocked($scope.pageKey).toString());
		if(!Util.isPaginationBlocked($scope.pageKey)){
			//requestParams = [];
			$rootScope.openBottomLoader();
			
			Util.getPageData('homeCatelogUrl',params ).then(function(data){
				$rootScope.closeBottomLoader();
				if(data.productList==null || data.productList.length == 0){
					Util.stopLoadingPageData($scope.pageKey);
				}else{
//					$rootScope.setHomeCatalogExtendedData(data.productList);
//					Util.appendItemsToList(data.productList, $scope.productList);
//					$rootScope.setHomeCatalogTableHeaders($scope.productList);
//					$scope.initBuyObject($scope.productList);
//					DataSharingService.setObject("CATALOG_PRODUCT_LIST",$scope.productList );	
					setHomeCatalogUIData(false,data);
					$scope.$root.$eval();
				}
			});
		}
		}
	};



 //$log.log("home catelog sorting1-- Current Sorting Selection : " + $scope.Selection);
 // home catelog sorting
	$scope.Sorting = function(column,orderBy) {
				
		//var	requestParams = $rootScope.getSortingParam(column,orderBy);		
		//$scope.orderType = orderBy;
		//$scope.sortColumn = column;
		$scope.homeCatalogSort.sorting(column,orderBy);
		$rootScope.sortSelection =  column+orderBy;
//		var params = $scope.homeCatalogSort.addSortParams([]);
//		
//		pageKey = Util.initPagination('homeCatelogUrl',params);
//		Util.getDataFromServer('homeCatelogUrl',params ).then(function(data){
//			$scope.homeCataloguePageIndex ++;
//			$scope.homeCatalogue = data;
//			$scope.productList = data.productList;
//			$scope.initBuyObject($scope.productList);
//			DataSharingService.setObject("CATALOG_PRODUCT_LIST",$scope.productList );	
//			$scope.initForScrollLoading();
//			$scope.productPropList = Util.getObjPropList(data.productList[0]);
//		});
		if($scope.SelectedProductList.length>1){
	 		$scope.SelectedProductList = [];
	 		angular.forEach($scope.productList, function(a) {        
			    a.checked = false;
			});
	 	}
		getFirstPageData();
	};
	

	$scope.changeSorting = function(column) {
		if(column.isSortable){
			var sort = $scope.sort;
			if (sort.column == column.actualName) {
				sort.descending = !sort.descending;
			} else {
				sort.column = column.actualName;
				sort.descending = false;
			}
		}
	};
	$scope.SelectedProduct = function(productList)
	{
		$scope.selectionMsg = "";
		$scope.SelectedProductList = $filter('filter')(productList, {checked: true});
		if($scope.SelectedProductList.length <=10)
		{
			$scope.selectionMsg = " Total selected Items :  "+ $scope.SelectedProductList.length;
			$scope.Comparison = true;
		}
		else
		{
			$scope.selectionMsg = Util.translate('CON_MAX_PRODUCT_COMPARISION');
			$scope.Comparison = false;
		}
		DataSharingService.setToLocalStorage("compareObjectList",$scope.SelectedProductList);
		//console.log("addCompareListItem length -  "+	$scope.SelectedProductList.length);

	};
	$scope.RemoveProduct = function()
	{
		$scope.SelectedProductList = $filter('filter')($scope.SelectedProductList, {checked: true});
		DataSharingService.setToLocalStorage("compareObjectList",$scope.SelectedProductList);
		if($scope.SelectedProductList.length > 1)
		{	
		$scope.selectionMsg = " Total selected Items :  "+ $scope.SelectedProductList.length;
		}
		else
		{
			$state.go($scope.previousState);					
			$scope.selectionMsg = "";
			if($state.current.name == 'home.homeCatalogTable'){
				$scope.ViewLabelName = $scope.gridViewLabelName;
				$scope.isList  = false;
			}
			else{			
				$scope.ViewLabelName = $scope.tableViewLabelName;
				$scope.isList  = true;
			}
		}
	};
	$scope.compare = function()
	{
		var isOpenLoginDlg=$rootScope.isSignOnRequired("CON_PRODUCT_COMPARE",'home');
		if(isOpenLoginDlg==true){
			return;
		}
		if($scope.Comparison == false) {
			$rootScope.openMsgDlg("", [$scope.selectionMsg]);
		} else if(($scope.SelectedProductList.length > 1) && ($scope.Comparison == true)) {
			$rootScope.SelectedProductUnit = {};
			for(var i= 0; i< $scope.SelectedProductList.length ; i++)
			{
				for(var j= 0;j<$scope.SelectedProductList[i].productUnitList.length;j++)					
					{
						if($scope.SelectedProductList[i].productUnitList[j].unit == $scope.SelectedProductList[i].defaultSalesUnit)
						{		
							$rootScope.SelectedProductUnit[$scope.SelectedProductList[i].code] = $scope.SelectedProductList[i].productUnitList[j];						
						}
					}			
				
			}
			addpropertyToList($scope.SelectedProductList,'validQuantity',true);					
			DataSharingService.setToLocalStorage("compareObjectList",$scope.SelectedProductList);
			DataSharingService.setObject("prevCompareState",$state.current.name);
			DataSharingService.setObject("prevCompareView",$scope.currentView );	
			DataSharingService.setObject("prevViewLabel",$scope.ViewLabelName );	
			//$state.go('home.mainCatalog.Compare');
			$state.go('home.Compare');
			$scope.$root.$eval();
		}
	};
	//
	$scope.changeView = function(){
		if($state.current.name == 'home.homeCatalogTable'){
			$state.go('home.homeCatalogGrid');			
			//$scope.ViewLabelName = Util.translate('CON_TABLE_VIEW');
			$scope.isList  = false;
		}else{
			$state.go('home.homeCatalogTable');
			//$scope.ViewLabelName = Util.translate('CON_GRID_VIEW');;
			$scope.isList  = true;
		}
	};

	$scope.getUnitDetail  = function(productUnitList,unit)
	{
	//	var unit ="",productUnitList = [];
		$scope.unitIndex = -1;
		if(productUnitList.length > 0)
			{
			for( var i = 0; i< productUnitList.length;i++ )
				{
					if(unit['unit'] == productUnitList[i].unit)
						{$scope.unitIndex = i;}
				}
			}
		
		
		
	};
	
	$scope.onUnitChange= function(itemCode,unitCode,oldCatalogItem){
		//var oldCatalogItem = getObjectFromList("code", itemCode, $scope.productList);
		$rootScope.getAndUpdateCatalogItem(itemCode,unitCode,oldCatalogItem);
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
		//$log.log("history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	
	homeCatalogCtrlSetup();
	
}]);
