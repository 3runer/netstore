//transport note page controller

catalougeControllers.controller('transportNoteHistoryCtrl', ['$rootScope','$scope', '$http','$log', 'DataSharingService','TransportNoteService', 'Util','$state',
                                                         function ($rootScope,$scope, $http,$log,DataSharingService,TransportNoteService,Util,$state) {

	var showLoading = true;
	var delimeter = ";";
	var transportNoteHistoryCtrlInit = function(){
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	});

	var initController = function()
	{ 
//		$rootScope.initCommitsSecondNextPromise.then(function(){
		$scope.sortColumn = "TransportNoteDate";
		$scope.orderType = "DESC";	
		$scope.isUserSessionActive = true;
		$scope.cachedPaginationObj = null;
		$scope.sort = new Sort();
		$scope.sort.DESC('TransportNoteDate');
		$scope.filterOn = false;
		$scope.filterList =  null;
		$scope.paginationObj = null;
		$scope.isCollapsed = false;

		makeTransportNoteHistoryFilters();
		var params = getSearchParams();
		$scope.callState = $state.current.name;
		if($scope.callState == REQ_SUBMIT_TRANSPORTNOTE_VIEW){
			showLoading = false;
			var requestObject = DataSharingService.getObject("requestObjectInfo");
			if(!isNull(requestObject)){
			if(!isNull(requestObject.reference) && !isEmptyString(requestObject.reference)){
				$scope.sort.ASC('TransportNoteDate');
				params = ["TransportNoteNumber",requestObject.reference];
				$scope.sort.addSortParams(params);
			}}
		}
		getTransportNoteHistoryPageData(params,FirstPage);
	};

	var getSearchParamsSorting = function(){
		var params  = [];
		if($scope.filterOn){
			params = $scope.sort.addSortParamsSortingOrderSort($scope.filterList.getParamsList());
		}else{
			params = $scope.sort.getParam();
		}
		return params;
	};	
	
	var getSearchParams = function(){
		var params  = [];
		if($scope.filterOn){
			params = $scope.sort.addSortParams($scope.filterList.getParamsList());
		}else{
			params = $scope.sort.getParam();
		}
		return params;
	};	
	
	var makeTransportNoteHistoryFilters = function(){
		//$rootScope.initCommitsSecondNextPromise.then(function(){
			var finalFilterList = new FilterList();
			var staticFilterConfig = TransportNoteService.getFilterConfiguration();
			var filterDataList = DataSharingService.getFilterDetailsSettings();
			
			var filterExist = false;
			if(isNull(filterDataList) || isNull(filterDataList.customerList)){
				filterExist = false;
			}else{
				filterExist = true;
			}
			
			
			if(filterExist ==  false){
				Util.getFilterListDetails([]).then(function(data){
					DataSharingService.setFilterDetailsSettings(data);
					filterDataList = DataSharingService.getFilterDetailsSettings();
					var dynamicFilterConfig = null;
					TransportNoteService.getTransportNoteFilterList().then(function(data){
						dynamicFilterConfig = data.filterBeans;
						finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
						$scope.filterList = finalFilterList;
						//console.log("makeTransportNoteHistoryFilters : " + JSON.stringify(finalFilterList));	
					});
				});
			}else{
				TransportNoteService.getTransportNoteFilterList().then(function(data){
					dynamicFilterConfig = data.filterBeans;
					finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
					$scope.filterList = finalFilterList;
					//console.log("makeTransportNoteHistoryFilters : " + JSON.stringify(finalFilterList));	
				});
			}
	};
	
	$scope.submitFilter = function(){
		if($scope.filterList.hasFilterTextValues()){
			$scope.sort.ASC('TransportNoteDate');
		}else{
			$scope.sort.DESC('TransportNoteDate');
		}
		$scope.filterOn = true;
		var params = getSearchParamsSorting();		
		getTransportNotePageDataSorting(params,FirstPage);
	};
	
	var getTransportNotePageDataSorting =  function(requestParams,page){
		$scope.paginationObj = $rootScope.getPaginationObject('transportNoteHistoryUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('transportNoteHistoryUrl',requestParams,page, showLoading ).then(function(data){
				$scope.selectedRequest = {};
				//$scope.selectedRequest.selected = false;
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true);
				}else{
					setUIPageData(data,false);
				}
			});
		}

	};
	
	var getTransportNoteHistoryPageData =  function(requestParams,page){
		$scope.paginationObj = $rootScope.getPaginationObject('transportNoteHistoryUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('transportNoteHistoryUrl',requestParams,page, showLoading ).then(function(data){
				$scope.selectedRequest = {};
				//$scope.selectedRequest.selected = false;
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true);
				}else{
					setUIPageData(data,false);
				}
			});
		}
	};
	
	var setUIPageData = function(data,isFirstPageData){
		var ItemList = [];
			if(isNull(data)){
				setItemList(null,isFirstPageData); 
				if(isFirstPageData){
					$rootScope.applyPreviousPaginationObject('transportNoteHistoryUrl' , $scope);
				}
				
				
			}else{
				ItemList = data.transportNoteHistoryBean;
				setItemList(ItemList,isFirstPageData); 
				$scope.sortColumn = data.sortColumn;
				$scope.orderType = data.orderBy;
				
				$rootScope.cachePaginationObject('transportNoteHistoryUrl' , ItemList, $scope);
				if(isFirstPageData && (! Array.isArray(ItemList) ||  !ItemList.length > 0 ) ){
					$rootScope.applyPreviousPaginationObject('transportNoteHistoryUrl' , $scope);
				} else {
					if(!isNull(data.moreRecords)) $scope.moreDataArrow = data.moreRecords;
				}
				
				
			}
			$rootScope.setPageLoadMsg(ItemList,isFirstPageData,$scope);
			$rootScope.scrollToId("result_set");
			if(!isNull($scope.$root)){
			$scope.$root.$eval();
			}
		};
		
	var setItemList = function(itemList,isFirstPageData){
		if(Array.isArray(itemList) && itemList.length > 0 ){
			$scope.transportNoteData = itemList;				
		}else{
			if(!isFirstPageData){
				$scope.transportNoteData = [];
			}
		}
		updateFilterStatus(itemList,isFirstPageData);
	};
	
	var updateFilterStatus = function(list,isFirstPageData){
		if(Array.isArray(list) && list.length > 0 && isFirstPageData){
			if(!isNull($scope.filterList)){
				$scope.filterList.cacheFilters();
				}
		}else if($scope.filterOn && isFirstPageData){			
			$scope.filterList.applyPreviousFilters();			
			showMsgNoObjFound();
		}
	};
	
	// sorting 
    $scope.sortASC = function(column){
		$scope.sort.ASC(column);
		sorting();
	};
	
	$scope.sortDESC = function(column){
		$scope.sort.DESC(column);
		sorting();
	};
	
	var sorting = function(){
		var params = getSearchParams();		
		getTransportNoteHistoryPageData(params,FirstPage);
	};
	
	$scope.previousPage = function(){		
		 var params = getSearchParams();		
		 getTransportNoteHistoryPageData(params,PrevPage);
	};
	$scope.nextPage = function(){	
		 var params = getSearchParams();		
		 getTransportNoteHistoryPageData(params,NextPage);
	};
			
	$scope.getFirstPage = function()
	{			
		 var params = getSearchParams();		
		 getTransportNoteHistoryPageData(params,FirstPage);
				
	};
	//  Pagination code ends
	$scope.showTransportNoteDetail = function(transportNote,fromRequest){
		if($rootScope.isShowDisableMenuItem('CON_TRANSPORTNOTE_INFORMATION')){
			DataSharingService.setToSessionStorage("transportNote_clickedDetail",transportNote);
			$rootScope.fromCart = false;
			$rootScope.fromRequestPage = false;
			DataSharingService.setToSessionStorage("fromCart",$rootScope.fromCart);
			DataSharingService.setToSessionStorage("fromRequestPage",$rootScope.fromRequestPage);
			var needToLogin = $rootScope.isSignOnRequired("CON_TRANSPORTNOTE_INFORMATION",'home.transportNote.detail',true);
			if(!needToLogin){
				if(fromRequest){
					$state.go(REQ_SUBMIT_TRANSPORTNOTE_DETAIL_VIEW);
				}
				else
				{
					$state.go('home.transportNote.detail');
				}
				//collapse testata
				angular.element('#button-testata').addClass('collapsed');
				angular.element('#headingrequest').removeClass('in');
				
			}
		}
	};
	
	$scope.exportTransportNoteTableAsPDF=function(){
		var transportNoteSearchTable = setTransportNoteSearchDataforExport();
		var data = transportNoteSearchTable.getHTML();
		var title = Util.translate('CON_TRANSPORTNOTE_SEARCH');
		var fileName = "TransportNoteSearch_"+getCurrentDateTimeStr()+".pdf";
		savePdfFromHtml(data,title,fileName);
	};
	
	$scope.exportTransportNoteTableAsExcel=function(){
		var transportNoteSearchTable = setTransportNoteSearchDataforExport();
		var data = transportNoteSearchTable.getDataWithHeaders();
		var sheetName = "TransportNoteSearch";
		var fileName = "TransportNoteSearch_"+getCurrentDateTimeStr()+".xlsx";
		saveExcel(data,sheetName,fileName);
	};
	
	$scope.exportTransportNoteTableAsXML=function(){
		var transportNoteSearchTable = setTransportNoteSearchDataforExport('XML');
		transportNoteSearchTable.setRootElementName("TRANSPORTNOTE_SEARCH");
		transportNoteSearchTable.setEntityName("TRANSPORTNOTE_DETAIL");
		var data = transportNoteSearchTable.getXML();
		var fileName = "TransportNoteSearch_"+getCurrentDateTimeStr()+".xml";
		saveXML(data,fileName);
	};
	
	var setTransportNoteSearchDataforExport = function(target){
		var transportNoteSearchTable = new Table();
		if(isEqualStrings(target,"XML")){
			transportNoteSearchTable.addHeader('TRANSPORTNOTE');
			transportNoteSearchTable.addHeader('TRANSPORTNOTE_DATE');
			transportNoteSearchTable.addHeader('CUSTOMER');
			transportNoteSearchTable.addHeader('DEBTOR');
		}else{
			transportNoteSearchTable.addHeader(Util.translate('CON_TRANSPORTNOTE'));
			transportNoteSearchTable.addHeader(Util.translate('CON_TRANSPORTNOTE_DATE'));
			transportNoteSearchTable.addHeader(Util.translate('CON_CUSTOMER'));
			transportNoteSearchTable.addHeader(Util.translate('CON_DEBTOR'));
		}
		data = $scope.transportNoteData;
		if(!isNull(data)){
			for(var i=0;i<data.length;i++){
				var row = [data[i].TransportNoteNumber, data[i].TransportNoteDate, data[i].CustomerDesc, data[i].DebtorDesc];
				transportNoteSearchTable.addRow(row);
			}
		}else{
			var row = [];			
			for(var i=0;i<transportNoteSearchTable.headers.length;i++){
				row.push("");
			}
			transportNoteSearchTable.addRow(row);
		}
		return transportNoteSearchTable;
	};
	
	transportNoteHistoryCtrlInit();
}]);