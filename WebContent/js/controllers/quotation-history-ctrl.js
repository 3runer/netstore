//quotation page controller
catalougeControllers.controller('quotationHistoryCtrl', ['$rootScope','$scope', '$http','$log', 'DataSharingService','QuotationService', 'Util','$state',
                                                         function ($rootScope,$scope, $http,$log,DataSharingService,QuotationService,Util,$state) {

	var quotationHistoryCtrlInit = function(){
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	//	$log.log(" topItemCtrl history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	
	var initController = function()
	{ 
//		$rootScope.initCommitsSecondNextPromise.then(function(){
		$scope.sortColumn = "QuotationNumber";
		$scope.orderType = "DESC";	
		$scope.isUserSessionActive = true;
		$scope.cachedPaginationObj = null;
		$scope.sort = new Sort();
		$scope.sort.DESC('QuotationNumber');
		$scope.filterOn = false;
		$scope.filterList =  null;
		$scope.paginationObj = null;
		$scope.isCollapsed = false;
		$scope.filterFormInfo = {};
		makeQuotationHistoryFilters();
		var params = getSearchParams(); 				
	 	getQuotationHistoryPageData(params,FirstPage);
//		});
		
	};
	var getSearchParams = function(){
		var params  = [];
		if($scope.filterOn){
			params = $scope.sort.addSortParams($scope.filterList.getParamsList());
		}else{
			params = $scope.sort.getParam();
		}
		return params;
	};	
	
	var makeQuotationHistoryFilters = function(){
		//$rootScope.initCommitsSecondNextPromise.then(function(){
			var finalFilterList = new FilterList();
			var staticFilterConfig = QuotationService.getFilterConfiguration();
			var filterDataList = DataSharingService.getFilterDetailsSettings();
			var dynamicFilterConfig = null;
			
			var filterExist = false;
			if(isNull(filterDataList.customerList)){
				filterExist = false;
			}else{
				filterExist = true;
			}
			
			
			if(filterExist ==  false){
				Util.getFilterListDetails([]).then(function(data){
					DataSharingService.setFilterDetailsSettings(data);
					$scope.filterDetailSettingsNew = data;
					setWebSettingObject(DataSharingService.getWebSettings());
					$rootScope.deferredSecondNext.resolve();
					filterDataList = DataSharingService.getFilterDetailsSettings();
					QuotationService.getQuotationFilterList().then(function(data){
						dynamicFilterConfig = data.filterBeans;
						finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
						$scope.filterList = finalFilterList;
						//console.log("makeQuotationHistoryFilters : " + JSON.stringify(finalFilterList));	
					});
				});
			}else{
				QuotationService.getQuotationFilterList().then(function(data){
					dynamicFilterConfig = data.filterBeans;
					finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
					$scope.filterList = finalFilterList;
					//console.log("makeQuotationHistoryFilters : " + JSON.stringify(finalFilterList));	
				});
			}
	};
	
	$scope.submitFilter = function(){
		if($scope.filterList.hasFilterTextValues()){
			$scope.sort.ASC('QuotationNumber');
		}else{
			$scope.sort.DESC('QuotationNumber');
		}
		$scope.filterOn = true;
		var params = getSearchParams();		
		getQuotationHistoryPageData(params,FirstPage);
	};
	var getQuotationHistoryPageData =  function(requestParams,page){
		$scope.paginationObj = $rootScope.getPaginationObject('quotationHistoryUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('quotationHistoryUrl',requestParams,page ).then(function(data){
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true);
				}else{
					setUIPageData(data,false);
				}
			});
		}
	};
	
	
	var setUIPageData = function(data,isFirstPageData){
		var ItemList = data.quotationHistoryBean;
			if(isNull(data)){
				setItemList(null,isFirstPageData); 
				if(isFirstPageData){
					$rootScope.applyPreviousPaginationObject('quotationHistoryUrl' , $scope);
				}
				
				
			}else{
				
				setItemList(ItemList,isFirstPageData); 
				$scope.sortColumn = data.sortColumn;
				$scope.orderType = data.orderBy;
				
				$rootScope.cachePaginationObject('quotationHistoryUrl' , ItemList, $scope);
				if(isFirstPageData && (! Array.isArray(ItemList) ||  !ItemList.length > 0 ) ){
					$rootScope.applyPreviousPaginationObject('quotationHistoryUrl' , $scope);
				} else {
					if(!isNull(data.moreRecords)) $scope.moreDataArrow = data.moreRecords;
				}
				
				
			}
			$rootScope.setPageLoadMsg(ItemList,isFirstPageData,$scope);
			$rootScope.scrollToId("result_set");
			$scope.$root.$eval();
		};
		
	var setItemList = function(itemList,isFirstPageData){
		if(Array.isArray(itemList) && itemList.length > 0 ){
			$scope.quotationData = itemList;				
			
		}else{
			if(!isFirstPageData){
				$scope.quotationData = [];
			}
		}
		updateFilterStatus(itemList,isFirstPageData);
	};
	
	var updateFilterStatus = function(list,isFirstPageData){
		if(Array.isArray(list) && list.length > 0 && isFirstPageData){
			$scope.filterList.cacheFilters();			
		}else if($scope.filterOn && isFirstPageData){			
			$scope.filterList.applyPreviousFilters();			
			showMsgNoObjFound();
		}
	};
	
	// sorting 
    $scope.sortASC = function(column){
		$scope.sort.ASC(column);
		sorting();
	};
	
	$scope.sortDESC = function(column){
		$scope.sort.DESC(column);
		sorting();
	};
	
	var sorting = function(){
		var params = getSearchParams();		
		getQuotationHistoryPageData(params,FirstPage);
	};
	
	$scope.previousPage = function(){		
		 var params = getSearchParams();		
		 getQuotationHistoryPageData(params,PrevPage);
	};
	$scope.nextPage = function(){	
		 var params = getSearchParams();		
		 getQuotationHistoryPageData(params,NextPage);
	};
			
	$scope.getFirstPage = function()
	{			
		 var params = getSearchParams();		
		 getQuotationHistoryPageData(params,FirstPage);
				
	};
	//  Pagination code ends
	$scope.showQuotationDetail = function(quotation){
	//	DataSharingService.setObject("quotation_clickedDetail",quotation);
		if($rootScope.isShowDisableMenuItem('CON_QUOTATION_INFORMATION')){
			DataSharingService.setToSessionStorage("quotation_clickedDetail",quotation);
			var needToLogin = $rootScope.isSignOnRequired("CON_QUOTATION_INFORMATION",'home.quotation.detail',true);
			if(!needToLogin){
				$state.go('home.quotation.detail');
			}
		}
	};
	
	$scope.exportQuotationTableAsPDF=function(){
		var quotationSearchTable = setQuotationSearchDataforExport();
		var data = quotationSearchTable.getHTML();
		var title = Util.translate('CON_QUOTATION_SEARCH');
		var fileName = "QuotationSearch_"+getCurrentDateTimeStr()+".pdf";
		savePdfFromHtml(data,title,fileName);
	};
	
	$scope.exportQuotationTableAsExcel=function(){
		var quotationSearchTable = setQuotationSearchDataforExport();
		var data = quotationSearchTable.getDataWithHeaders();
		var sheetName = "QuotationSearch";
		var fileName = "QuotationSearch_"+getCurrentDateTimeStr()+".xlsx";
		saveExcel(data,sheetName,fileName);
	};
	
	$scope.exportQuotationTableAsXML=function(){
		var quotationSearchTable = setQuotationSearchDataforExport('XML');
		quotationSearchTable.setRootElementName("QUOTAION_SEARCH");
		quotationSearchTable.setEntityName("QUOTAION_DETAIL");
		var data = quotationSearchTable.getXML();
		var fileName = "QuotationSearch_"+getCurrentDateTimeStr()+".xml";
		saveXML(data,fileName);
	};
	
	var setQuotationSearchDataforExport = function(target){
		var quotationSearchTable = new Table();
		if(isEqualStrings(target,"XML")){
			quotationSearchTable.addHeader('QUOTATION');
			quotationSearchTable.addHeader('VERSION');
			quotationSearchTable.addHeader('CUSTOMER');
			quotationSearchTable.addHeader('VALUE');
			quotationSearchTable.addHeader('HANDLER');
			quotationSearchTable.addHeader('EXPIRE_DATE');
		}else{
			quotationSearchTable.addHeader(Util.translate('CON_QUOTATION'));
			quotationSearchTable.addHeader(Util.translate('CON_VERSION'));
			quotationSearchTable.addHeader(Util.translate('CON_CUSTOMER'));
			quotationSearchTable.addHeader(Util.translate('CON_VALUE'));
			quotationSearchTable.addHeader(Util.translate('CON_HANDLER'));
			quotationSearchTable.addHeader(Util.translate('COH_EXPIRE_DATE'));
		}
		data = $scope.quotationData;
		if(!isNull(data)){
			for(var i=0;i<data.length;i++){
	            var amount=data[i].OrderValue;
				var row = [data[i].QuotationNumber, data[i].VersionNumber,data[i].CustomerDesc,amount,data[i].HandlerDesc,data[i].DueDate];
				quotationSearchTable.addRow(row);
			}
		}else{
			var row = [];			
			for(var i=0;i<quotationSearchTable.headers.length;i++){
				row.push("");
			}
			quotationSearchTable.addRow(row);
		}
		
		return quotationSearchTable;
	};
	
	quotationHistoryCtrlInit();
	//old code
//	$scope.sortColumn = "QuotationNumber";
//	$scope.orderType = "DESC";
//	$scope.param = [];
//	$scope.filterParam = [];
//	$scope.filterOn = false;	
//	$scope.dataFound = false;
//	$scope.recordFound = true;
//	$scope.propertyList = [];
//	$scope.isUserSessionActive = true;
//	$scope.responseMessageCode = "";
//	var requestParams = [];
//	$scope.callState = $state.current.name;
//
//	QuotationService.getQuotationHistory(requestParams).then(function(data)
//	{
//		
//		if(data.quotationHistoryBean==null || data.quotationHistoryBean.length == 0){
//			$scope.dataFound = false;
//			$scope.quotationLoadMsg = " ";
//			$log.log("quotationHistoryCtrl --  no data fetched");
//
//		}
//		else {
//		
//			$log.log("quotationHistoryCtrl messageCode -- "+data.messageCode);
//			//$rootScope.reLogin(data.messageCode);		
//			$scope.quotationData = data.quotationHistoryBean;
//			$scope.dataFound = true;
//			$scope.quotationLoadMsg = "";
//			$scope.propertyList = Util.getObjPropList(data.quotationHistoryBean[0]);		
//			$log.log("quotationHistoryCtrl --  " +data.quotationHistoryBean.length);
//		}
//
//	});
//	// Utility Code
//	requestParams = ["OrderBy",$scope.orderType,"SortBy",$scope.sortColumn];
//	var pageKey = Util.initPagination('quotationHistoryUrl',requestParams);
//	// Pagination code
//	paginationObj = DataSharingService.getObject(pageKey);	
//	$rootScope.getQuotationPageData = function(page)
//	{
//		$log.log("getQuotationPageData() - called, isPaginationBlocked = "+ Util.isPaginationBlocked(pageKey).toString());		
//		if(!Util.isPaginationBlocked(pageKey))
//		{	
//			$log.log("getQuotationPageData() - called" );
//			requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn);
//			Util.getPageinationData('quotationHistoryUrl',requestParams,page ).then(function(data)
//			{
//				if(data.quotationHistoryBean==null || data.quotationHistoryBean.length == 0)
//				{	
//					paginationObj.setLastPageNo();
//					$scope.dataFound = false;
//					$scope.quotationLoadMsg = Util.translate('CON_NO_MORE_PAGE'); 
//					$log.log("quotationHistoryCtrl --  "+ $scope.quotationLoadMsg);
//					
//					//$log.log("requestDetailCtrl -- " + $scope.requestLoadMsg);
//					
//				}
//				else
//				{
//					$log.log("quotationHistoryCtrl messageCode -- "+data.messageCode);
//					//$rootScope.reLogin(data.messageCode);
//					$scope.dataFound = true;
//					$scope.quotationLoadMsg = "";						
//					$scope.quotationData = data.quotationHistoryBean;
//					requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn);
//					$scope.$root.$eval();					
//				}			
//				$log.log("paginationObj.isLastPageNo(): " +paginationObj.isLastPageNo());
//			});
//		}
//	};
//
//	$scope.previousPage = function(){		
//		
//		$scope.getQuotationPageData(PrevPage);		
//	};
//	$scope.nextPage = function(){			
//		$scope.getQuotationPageData(NextPage);		
//	};
//			
//	$scope.getFirstPage = function()
//	{			
//		$scope.getQuotationPageData(FirstPage);
//				
//	};
//	//  Pagination code ends
//
//	//  Pagination code ends
//
//
//	// Search Filters 
//	$scope.filterFormInfo = {};
//	$scope.filterMsg = "";
//	$scope.submitFilter = function()
//	{
//		$scope.filterOn = true;
//		var keyList = $scope.filterFormInfo;
//		$scope.filterParam =  $rootScope.setParamKeyList(keyList); 
//		var filterParam =  $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn); 
//		$log.log("quotationHistoryCtrl filterParam--  " + filterParam);
//		pageKey = Util.initPagination('quotationHistoryUrl',filterParam);
//		QuotationService.getQuotationHistory(filterParam).then(function(data){
//
//			if(data.quotationHistoryBean==null || data.quotationHistoryBean.length == 0){
//				
//				$scope.dataFound = false;				
//				$scope.filterMsg =  Util.translate('MSG_NO_OBJECTS_FOR_SELECTION',null);
//				showMsgNoObjFound();
//				var icon = '<a class="OrderInformation-icon" shref="" title="'+$scope.filterMsg+'"></a>';				
//				$scope.filterMsg = icon + $scope.filterMsg;
//				
//				$scope.recordFound = false;
//				
//			}
//			else{
//				$scope.quotationData = data.quotationHistoryBean;
//				$scope.dataFound = true;
//				$scope.quotationLoadMsg = "";
//				$scope.recordFound = true;
//				$scope.filterMsg = "";
//				$scope.propertyList = Util.getObjPropList(data.quotationHistoryBean[0]);		
//				$log.log("quotationHistoryCtrl --  " +data.quotationHistoryBean.length);				
//				$scope.$root.$eval();
//				$rootScope.scrollToId("result_set");	
//			}
//		});
//	};
//	
//	var requestFilterParams = [];	
//	// to get quotation search filter key list 
//	QuotationService.getQuotationFilterList(requestFilterParams).then(function(data){	
//	//	$http.get('./json/catalog/quotation_search_filter.json').success(function(data){
//		$scope.searchFilterList = data.filterBeans;		
//		$scope.getFilterSettings(data.filterBeans);		
//		$log.log("quotationHistoryCtrl: searchFilterList --  " +data.filterBeans.length);
//
//	});
//	
//	$rootScope.filterDetailSettings = DataSharingService.getFilterDetailsSettings();
//
//	// to get filter configuration setting from "filterConfig.json"  
//	$scope.getFilterSettings = function(filterList)
//	{
//		 var filterConfigData = QuotationService.getFilterConfiguration();
//		 $log.log("quotationHistoryCtrl getFilterSettings--  " +JSON.stringify(filterConfigData));			 
//		 $scope.searchFilterList =  $rootScope.getSearchFilterSettings($scope.searchFilterList,filterConfigData);
//	};
//
//	// Search Filters
//	
//	$scope.toggleSortOrderType = function(sortOrder){
//		if(sortOrder == "ASC" )
//			{sortOrder = "DESC";}
//		else 
//			{sortOrder = "ASC";}
//		return sortOrder;
//	};
//
//	 $rootScope.sortSelection = '';
//	 $rootScope.sortSelection =  $scope.sortColumn+$scope.orderType;
//	 //$log.log("Request Search sorting-- Current Sorting Selection : " + $scope.Selection);
//
//	$scope.Sorting = function(column,orderBy) {
//			
//			var	requestParams = $rootScope.getSortingParam(column,orderBy);		
//		pageKey = Util.initPagination('quotationHistoryUrl',requestParams);
//		QuotationService.getQuotationHistory(requestParams).then(function(data)
//		{
//			$scope.quotationData = data.quotationHistoryBean;				
//			$log.log("quotationHistoryCtrl --  " +data.quotationHistoryBean.length);
//		});
//	};
//
//	
//	$scope.showQuotationDetail = function(quotation){
//		DataSharingService.setObject("quotation_clickedDetail",quotation);
//		var needToLogin = $rootScope.isSignOnRequired("CON_QUOTATION_INFORMATION",'home.quotation.detail',true);
//		if(!needToLogin){
//			$state.go('home.quotation.detail');
//		}
//	};
//
//
//	$scope.isColExist = function(colPropName,propertyList){
//		var isCol = false;
//		var val = Util.isPropertyExist(colPropName,propertyList);
//		if(val){
//			isCol = true;
//
//		}
//		return isCol;
//	};

}]);