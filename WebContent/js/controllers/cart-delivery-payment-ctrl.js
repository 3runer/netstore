// DeliveryPaymentController
catalougeControllers.controller('CartDeliveryPaymentCtrl', ['$rootScope','$scope', '$http','$log','$window', 'DataSharingService','ShoppingCartService', 'Util','$state',
                                                     function ($rootScope,$scope, $http,$log, $window, DataSharingService,ShoppingCartService,Util,$state) {
	
	var cartDeliveryPaymentCtrlInit = function(){
		/*if(historyManager.isBackAction){
			historyManager.loadScopeWithCacheReviewOrder($scope);
			$scope.$root.$eval();
		}*/

		$scope.changesMadeToPage = false;
		$scope.cartDeliveryTermsAndCondition = false;
		$rootScope.initCommitsPromise.then(function(){
			$scope.selectedPayMethod = {
					name : "INVOICE"
			};
			if(isNull($scope.CartDeliveryInfo)){
				getDeliveryPaymentDetails();
			}
		});
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);		
	});
	
	var getDeliveryPaymentDetails = function()
	{
		ShoppingCartService.getDeliveryInfo([]).then(function(data){			
			if(!isNull(data)){
				
				$scope.CartDeliveryInfo = new CartDelivery(data,$rootScope.webSettings);
				setMannerOfTransport($scope.CartDeliveryInfo);
				setCountryList($scope.CartDeliveryInfo);
//				ShoppingCartService.getOrderTextReference([]).then(function(data){
//					$scope.CartDeliveryInfo.orderTextReference = data[0];
//					$scope.CartDeliveryInfo.orderReference.freeText = data[0];
//				});
			}
			
		});
	};
	
	$scope.saveDeliveryInfo = function(cart){
		var CartDeliveryInfo = $scope.CartDeliveryInfo;
		if(!isNull(CartDeliveryInfo)){
			$scope.changesMadeToPage = false;
			CartDeliveryInfo.updateSelectedAddress();
			if(CartDeliveryInfo.validateDeliveryUpdates(CartDeliveryInfo)){
			var reqParam = CartDeliveryInfo.getDeliveryParam();
			////console.log('Address change : ' + reqParam);
			if(!isNull(reqParam)){
				// to be open later 
				ShoppingCartService.saveDeliveryInfo(reqParam).then(function(data){
					CartDeliveryInfo.setDeliveryInfoResponse(data);
					if(CartDeliveryInfo.isSavedSuccessfully){
						$rootScope.reviewOrder = false;
						$scope.CartDeliveryInfo_ForReviewOder = deepCopyWithFunctions($scope.CartDeliveryInfo);
					}
					if(data.messageCode==4411){
						$rootScope.reviewOrder = false;
					}else if(data.messageCode==4411){
						$rootScope.reviewOrder = true;
					}
				});
			}
			}
		}
	};
	
	$scope.showCountyOrState = function(val){
		var show;
		if(!isNull(val)&& val !=""){
			show = true;
		}else {
			show = false;
		}
		return show;
	};	
	
	var setMannerOfTransport = function(CartDeliveryInfo)
	{
		ShoppingCartService.getAllMannerOfTransport([]).then(function(data)
				{
					if(data!=null){
						
						CartDeliveryInfo.setMannerOfTransportList(data);
					}		
				});
	};
	
	$scope.fetchStateList = function(CartDeliveryInfo){
		CartDeliveryInfo.setCountryForSelectedAddress();
		var requestParam = ["CountryCode",CartDeliveryInfo.selectedCountry.code];
		ShoppingCartService.getStateList(requestParam).then(function(data)
				{
					if(data!=null){						
						CartDeliveryInfo.setStateList(data);
						if($scope.changesMadeToPage) {
							CartDeliveryInfo.setCountyList([]);
						}
						$scope.fetchCountyList($scope.CartDeliveryInfo);
					}		
				});
		
	};
	$scope.fetchCountyList = function(CartDeliveryInfo){
		CartDeliveryInfo.setStateForSelectedCountry();
		var requestParam = ["CountryCode",CartDeliveryInfo.selectedCountry.code,"StateCode",CartDeliveryInfo.selectedState.stateProvincecode];
		ShoppingCartService.getCountyList(requestParam).then(function(data)
				{
					if(data!=null){						
						CartDeliveryInfo.setCountyList(data);
					}		
				});
		
	};
	
	$scope.selectCloseCart = function(cartDeliveryTermsAndCondition){
		$scope.cartDeliveryTermsAndCondition = !cartDeliveryTermsAndCondition;
	}
	
	$scope.changeCalled = function(){
		$scope.changesMadeToPage = true;
	};
	
	$scope.changeFormatToLocale = function(value){
		var newVal = value;
		if(!isNull($rootScope.webSettings.localeCode) && $rootScope.webSettings.localeCode !=""){
			if(!isNull($rootScope.webSettings.allowDecimalsForQuantities) && $rootScope.webSettings.allowDecimalsForQuantities == true){
			var langCode = $rootScope.webSettings.localeCode;
			langCode = langCode.replace("_","-");
			newVal = newVal.toLocaleString(langCode);
		}}
		return newVal;
	};
	
	$scope.isShowThisField = function(value){
		if(value == undefined){
			return false;
		}else{
			return true;
		}
		
	};

	
	$scope.hideAddress = function(allowDispatchAddressOverride,address3){
		if(allowDispatchAddressOverride==false && address3=="" ){
			return false;
		}else{
			return true;
		}
			
	};
	
	var setCountryList = function(CartDeliveryInfo){
		ShoppingCartService.getAllCountryData([]).then(function(data){
			if(data!=null){
				CartDeliveryInfo.setCountryList(data);
				if($scope.changesMadeToPage) {
					CartDeliveryInfo.setStateList([]);
					CartDeliveryInfo.setCountyList([]);
				}
				$scope.fetchStateList($scope.CartDeliveryInfo);
			}		
		});
	};
	
	$scope.goReviewOrder= function(){		
		//$scope.CartDeliveryInfo.setFreeText($scope.CartDeliveryInfo.orderText);
		if($scope.changesMadeToPage == true){
		var msg = Util.translate('CON_DATALOST_MSG');
		var mode = $rootScope.openConfirmDlg(msg);
		mode.result.then(function () {
			DataSharingService.setToSessionStorage("CartDeliveryInfo",$scope.CartDeliveryInfo_ForReviewOder);
			$state.go('home.reviewOrder');
		}, function () {
		});
		}else{
			DataSharingService.setToSessionStorage("CartDeliveryInfo",$scope.CartDeliveryInfo_ForReviewOder);
			$state.go('home.reviewOrder');
			
		}
			
	};
	
	$scope.clearChanges = function(){
		window.location.reload();
	};
	
	$scope.gobackAlert = function(){
		if($scope.changesMadeToPage == true){
		var msg = Util.translate('CON_DATALOST_MSG');
		var mode = $rootScope.openConfirmDlg(msg);
		mode.result.then(function () {
			//$scope.selectedRequest.selected = false;
			//$rootScope.goBack();
			$state.go('home.shoppingcart');
			
		}, function () {
			 
			
		});
		}else{
			$state.go('home.shoppingcart');
		}
	};

	$scope.getTermsNConditionFileName = function(){
		var fileName= "termsAndConditions_";
		if(!isNull($rootScope.selectedLanguage)){
			fileName = fileName+$rootScope.selectedLanguage.code+".html";
		}else{
			fileName = fileName+"EN"+".html";
		}
		return fileName;
	};
	
	$scope.goThankYou= function(){
		var needToLogin = $rootScope.isSignOnRequired("CON_ORDER_RECEIVED_CONFIRMATION",null,false);
		if(!needToLogin){
			if($rootScope.isOnlinePayAllowed() && isEqualStrings($scope.selectedPayMethod.name,CONST_PAY_METHOD_ONLINE,true)) {
				$scope.payOnline();
			}else{
				requestParams = ["YourReference",$scope.CartDeliveryInfo.orderReference.yourReference,"addressNumber",$scope.CartDeliveryInfo.selectedAddress.AddressNumber,"freeText",$scope.CartDeliveryInfo.orderReference.freeText,"emailInvoiceDispatch",$scope.CartDeliveryInfo.emailInvoiceDispatch,"emailInvoiceDispCheckBox",$scope.CartDeliveryInfo.sendInvoiceEmailOnDispatch];
				ShoppingCartService.closeCart(requestParams).then(function(data) {
					if(!isNull(data)) {
						$scope.thankYouObj = new ThankYouPage(data);
						DataSharingService.setToSessionStorage("thankYouObj",$scope.thankYouObj);	
						if($scope.thankYouObj.isOrderPlaced){
							$rootScope.setBubbleCartToZero();
							$scope.CartDeliveryInfo = null;
							DataSharingService.removeFromSessionStorage("CartDeliveryInfo");
						}
						DataSharingService.setToSessionStorage("shoppingCart_orderNumber",$scope.thankYouObj.orderNumber);
						DataSharingService.setToSessionStorage("CartDeliveryInfo",$scope.CartDeliveryInfo);
						 $state.go('home.goThankYou',{"PAY_MODE":"INVOICE","MESSAGE_CODE":"NA","ORDER_NUM":$scope.thankYouObj.orderNumber});
					}
				});
			}
		}
	};
	
	$scope.payOnline = function(){
		
		var uiURL = $rootScope.getHomePageURL();
		var requestParams = ["YourReference",$scope.CartDeliveryInfo.orderReference.yourReference,"addressNumber",$scope.CartDeliveryInfo.selectedAddress.AddressNumber,
		                     "freeText",$scope.CartDeliveryInfo.orderReference.freeText,"nsuiURI", uiURL , "nsuiBaseFragment" , "thankYou"];
		var nsuiRedirectURL = Util.getURL("resgisterPaymentUrl",requestParams);
		////console.log(nsuiRedirectURL);
		//var queryString = Util.getParamStr(requestParams);
		//$window.location.assign("http://localhost:9001/postTest/rest/WSR/registerOrderPayment"+queryString);
		$rootScope.openLoader();
		//nsuiRedirectURL = "http://localhost:8080/NetStoreUI/home.html#/thankYou/ONLINE/3/50575"
		$window.location.assign(nsuiRedirectURL);
	};

	$scope.selectedPaymnetMode = function(paymentMethod){
		if(($rootScope.webSettings.isPaymentModeActivated && paymentMethod == 'INVOICE')|| ($rootScope.webSettings.isPaymentModeActivated && paymentMethod == 'INVOICE,ONLINE') ){
			return true;
		}else{
			return false;
		}
	};
	
	$scope.selectedPaymnetModeOnline = function(paymentMethod){
		if(($rootScope.webSettings.isPaymentModeActivated && paymentMethod == 'ONLINE')||($rootScope.webSettings.isPaymentModeActivated && paymentMethod == 'INVOICE,ONLINE')){
			return true;
		}else{
			return false;
		}
	};
	
	$scope.NEWexportReviewOrderTableAsPDF=function(){
		window.print();
	}
	
	$scope.exportReviewOrderTableAsPDF=function(){
		var reviewOrderTable = setReviewOrderDataforExport();
		
		var data = reviewOrderTable.getHTML();
		var basicInfo = getBasicInfoForExportReview();
		var addresses = getAddressesForExportReview();
		var orderText,orderFees,quotationReferences,docHeader;
		var title = Util.translate('CON_REVIEW_ORDER');
		var fileName = "OrderConfirmation_"+getCurrentDateTimeStr()+".pdf";
		//var docHeader = "Review Order Detail";
		saveDetailsPDF(reviewOrderTable,basicInfo,orderText,quotationReferences,orderFees,addresses,docHeader,fileName);
		//savePdfFromHtml(data,title,fileName);
	};
	
	$scope.exportReviewOrderTableAsExcel=function(){
		var reviewOrderTable = setReviewOrderDataforExport();
		var data = reviewOrderTable.getDataWithHeaders();
		var sheetName = "OrderConfirmation";
		var fileName = "OrderConfirmation_"+getCurrentDateTimeStr()+".xlsx";
		saveExcel(data,sheetName,fileName);
	};
	
	$scope.exportReviewOrderTableAsXML=function(){
		var reviewOrderTable = setReviewOrderDataforExport('XML');
		reviewOrderTable.setRootElementName("ORDER_CONFIRMATION");
		reviewOrderTable.setEntityName("ORDER_CONFIRMATION_ITEM");
		var data = reviewOrderTable.getXML();
		var fileName = "OrderConfirmation_"+getCurrentDateTimeStr()+".xml";
		saveXML(data,fileName);
	};
	
	var getAddressesForExportReview = function(target){
		var reviewAddresses = new Table(); 
		data = $scope.CartDeliveryInfo;
		reviewAddresses.heading = Util.translate('CON_ADDRESSES') ; 
		reviewAddresses.addHeader(Util.translate('CON_DELIVERY_ADDRESS'));
		reviewAddresses.addHeader(Util.translate('CON_BILLING_ADDRESS'));

		var row = [];
		row.push(data.deliveryAddress.name);
		row.push(data.billingAddress.name);
		reviewAddresses.addRow(row);
		
		row = [];
		row.push(data.deliveryAddress.address1);
		row.push(data.billingAddress.address1);
		reviewAddresses.addRow(row);
		
		row = [];
		row.push(data.deliveryAddress.address2);
		row.push(data.billingAddress.address2);
		reviewAddresses.addRow(row);
		
		row = [];
		row.push(data.deliveryAddress.address3);
		row.push(data.billingAddress.address3);
		reviewAddresses.addRow(row);
		
		row = [];
		row.push(data.deliveryAddress.address4);
		row.push(data.billingAddress.address4);
		reviewAddresses.addRow(row);
		
		
		row = [];
		row.push(data.deliveryAddress.postalCode);
		row.push(data.billingAddress.postalCode);
		reviewAddresses.addRow(row);

		row = [];
		row.push(data.deliveryAddress.countryDesc);
		row.push(data.billingAddress.countryDesc);
		reviewAddresses.addRow(row);
		
		
		return reviewAddresses;
	};
	
	var setReviewOrderDataforExport = function(target){
		var reviewOrderTable = new Table();
		if(isEqualStrings(target,"XML")){
			reviewOrderTable.addHeader('PRODUCT');
			reviewOrderTable.addHeader('DESCRIPTION');
			reviewOrderTable.addHeader('DELIVERY_DATE');
			reviewOrderTable.addHeader('UNIT');
			if($rootScope.webSettings.isShowPrice){
				reviewOrderTable.addHeader('DISCOUNT');
				reviewOrderTable.addHeader('PRICE');
			}
			reviewOrderTable.addHeader('QUANTITY');
			reviewOrderTable.addHeader('TOTAL_AMOUNT');
			reviewOrderTable.addHeader('SHIPMENT_MARKING');
		}else{
			reviewOrderTable.addHeader(Util.translate('CON_ITEM'));
			reviewOrderTable.addHeader(Util.translate('COH_DESCRIPTION'));
			reviewOrderTable.addHeader(Util.translate('CON_DELIVERY_DATE'));
			reviewOrderTable.addHeader(Util.translate('CON_UNIT'));
			if($rootScope.webSettings.isShowPrice){
				reviewOrderTable.addHeader(Util.translate('CON_DISCOUNT'));
				reviewOrderTable.addHeader(Util.translate('CON_PRICE'));
			}
			reviewOrderTable.addHeader(Util.translate('COH_QUANTITY'));
			reviewOrderTable.addHeader(Util.translate('COH_TOTAL_AMOUNT'));
			reviewOrderTable.addHeader(Util.translate('COH_SHIPMENT_MARKING'));
		}
		//set row data
		data = objectToArray($scope.CartDeliveryInfo.orderLines);
		if(!isNull(data)){
			for(var i=0;i<data.length;i++){
				var item = data[i];
				var row = [];
				row.push(item.ivItem.code);
				row.push(item.ivItem.description); 
				row.push(item.ivEnteredValues.Date);
				row.push(item.ivEnteredValues.Unit); 
				if($rootScope.webSettings.isShowPrice){
					if(Number($rootScope.convertAsNumber(item.ivOrderLine.lineDisCount))>0){
						row.push(item.ivOrderLine.lineDisCount);
					}else{
						row.push("");
					}
					row.push(item.ivOrderLine.price);
				}
				row.push(item.ivOrderLine.orderedQuantity);
				row.push(item.ivOrderLine.lineVal); 
				row.push(item.ivEnteredValues.ShipmentMark);
				reviewOrderTable.addRow(row);
				
			}
		}else{
			var row = [];			
			for(var i=0;i<reviewOrderTable.headers.length;i++){
				row.push("");
			}
			reviewOrderTable.addRow(row);
		}
		
		return reviewOrderTable;
	};
	
	
	

	var getBasicInfoForExportReview = function(target){
		var basicInfo = new Table();
		data = $scope.CartDeliveryInfo.nsOrderValueInfo;
		//		basicInfo.heading = Util.translate('') ; 
		var row = [];
		row.push("");
		row.push("");
		row.push("");
		row.push((Util.translate('CON_TOTAL_LINES'))+':');
		row.push(data.emTotalLines);
		basicInfo.addRow(row);
		
		row = [];
		row.push("");
		row.push("");
		row.push("");
		row.push((Util.translate('CON_ORDER_DISCOUNT'))+':');
		row.push(data.emDisCount);
		basicInfo.addRow(row);
		
		row = [];
		row.push("");
		row.push("");
		row.push("");
		row.push((Util.translate('CON_ADMINISTRATION_FEE'))+':');
		row.push(data.adminFee);
		basicInfo.addRow(row);
		
		row = [];
		row.push("");
		row.push("");
		row.push("");
		row.push((Util.translate('CON_INVOICE_FEE'))+':');
		row.push(data.invoiceFee);
		basicInfo.addRow(row);
		
		row = [];
		row.push((Util.translate('CON_TOTAL_WEIGHT'))+'   '+data.emTotalWeight);
		//row.push(data.emTotalWeight);
		row.push("");
		row.push("");
		row.push((Util.translate('CON_TOTAL_EXCLUDING_VAT'))+':');
		row.push(data.emLineNet);
		basicInfo.addRow(row);
		
		row = [];
		row.push((Util.translate('CON_TOTAL_VOLUME'))+'   '+data.emTotalVolume);
		//row.push(data.emTotalVolume);
		row.push("");
		row.push("");
		row.push((Util.translate('CON_VAT'))+':');
		row.push(data.emVAT);
		basicInfo.addRow(row);
		
		row = [];
		row.push("");
		row.push("");
		row.push("");
		row.push((Util.translate('CON_COIN_ADJUSTMENT'))+':');
		row.push(data.coinAdjustment);
		basicInfo.addRow(row);
	
		row = [];
		row.push("");
		row.push("");
		row.push("");
		row.push((Util.translate('CON_GRAND_TOTAL'))+':');
		row.push((data.emOrderTotal) +' ' +webSettings.currencyCode);
		basicInfo.addRow(row);
	

		return basicInfo;
	};
	
	
	function objectToArray( object ) {
	    var length = 0;
	    var data=[];
	    for( var key in object ) {
	        if( object.hasOwnProperty(key) ) {
	            data[length]=object[key];
	            ++length;
	        }
	    }
	    return data;
	};
	
	cartDeliveryPaymentCtrlInit();
	
}]);