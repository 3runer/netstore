
catalougeControllers.controller('invoiceHistoryCtrl', ['$rootScope','$scope', '$http','$log', 'DataSharingService','InvoiceService', 'Util','$state',
                                                       function ($rootScope,$scope, $http,$log,DataSharingService,InvoiceService,Util,$state) {
	var showLoading = true;
	var invoiceHistoryCtrlInit = function(){
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	//	$log.log(" invoiceHistoryCtrl history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	


	var initController = function()
	{ 
//		$rootScope.initCommitsSecondNextPromise.then(function(){
		$scope.sortColumn = "invoiceNumber";
		$scope.orderType = "DESC";	
		$scope.isUserSessionActive = true;
		$scope.cachedPaginationObj = null;
		$scope.sort = new Sort();
		$scope.sort.DESC('invoiceNumber');
		$scope.filterOn = false;
		$scope.filterList =  null;
		$scope.paginationObj = null;
		$scope.isCollapsed = false;
		makeInvoiceHistoryFilters();
		var params = getSearchParams(); 		
		$scope.callState = $state.current.name;
		if($scope.callState == REQ_SUBMIT_INVOICE_VIEW){
			showLoading = false;
			var requestObject = DataSharingService.getObject("requestObjectInfo");
			if(!isNull(requestObject)){
			if(!isNull(requestObject.reference) && !isEmptyString(requestObject.reference)){
				$scope.sort.ASC('invoiceNumber');
				params = ["InvoiceNumber",requestObject.reference];
				if(!isEmptyString(requestObject.docType)){
					params.push("DocumentType");
					params.push(requestObject.docType);
				}
				$scope.sort.addSortParams(params);
			}}
		}
		getInvoiceHistoryPageData(params,FirstPage,showLoading);
//		});
		
	};
	var getSearchParams = function(){
		var params  = [];
		if($scope.filterOn){
			params = $scope.sort.addSortParams($scope.filterList.getParamsList());
		}else{
			params = $scope.sort.getParam();
		}
		return params;
	};	
	
	var getSearchParamsSorting = function(){
		var params  = [];
		if($scope.filterOn){
			params = $scope.sort.addSortParamsSortingInvoiceSort($scope.filterList.getParamsList());
		}else{
			params = $scope.sort.getParam();
		}
		return params;
	};	
	
	
	var makeInvoiceHistoryFilters = function(){
		//$rootScope.initCommitsSecondNextPromise.then(function(){
			var finalFilterList = new FilterList();
			var staticFilterConfig = InvoiceService.getFilterConfiguration();
			var filterDataList = DataSharingService.getFilterDetailsSettings();
			var dynamicFilterConfig = null;
			
			var filterExist = false;
			if(isNull(filterDataList) || isNull(filterDataList.customerList)){
				filterExist = false;
			}else{
				filterExist = true;
			}
			
			
			if(filterExist ==  false){
				Util.getFilterListDetails([]).then(function(data){
					DataSharingService.setFilterDetailsSettings(data);
					$scope.filterDetailSettingsNew = data;
					setWebSettingObject(DataSharingService.getWebSettings());
					$rootScope.deferredSecondNext.resolve();
					filterDataList = DataSharingService.getFilterDetailsSettings();
					InvoiceService.getInvoiceFilterList().then(function(data){
							dynamicFilterConfig = data.filterBeans;
							finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
							$scope.filterList = finalFilterList;
							//console.log("makeInvoiceHistoryFilters : " + JSON.stringify(finalFilterList));	
					});
				});
			}else{
				InvoiceService.getInvoiceFilterList().then(function(data){
					dynamicFilterConfig = data.filterBeans;
					finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
					$scope.filterList = finalFilterList;
					//console.log("makeInvoiceHistoryFilters : " + JSON.stringify(finalFilterList));	
			});
			}
	};
	
	var getInvoiceHistoryPageDataSorting =  function(requestParams,page,showLoading){
		$scope.paginationObj = $rootScope.getPaginationObject('invoiceHistoryUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('invoiceHistoryUrl',requestParams,page,showLoading ).then(function(data){
				$scope.selectedRequest = {};
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true);
				}else{
					setUIPageData(data,false);
				}
			});
		}
		if($scope.filterOn){
			if(requestParams[0]=='InvoiceNumber'){
				$scope.sort.ASC('invoiceNumber');
			}else if(requestParams[0]=='OrderNumber'){
				$scope.sort.ASC('invoiceNumber');
			}else if(requestParams[0]=='InvoiceCustomer'||requestParams[0]=='OrderReference'||requestParams[0]=='YourReference'||requestParams[0]=='InvoiceCustomer'
				||requestParams[0]=='Customer'){
				$scope.sort.DESC('invoiceNumber');
			}else{
				$scope.sort.DESC(requestParams[0]);
			} 
			
		}
	};
	
	$scope.submitFilter = function(){
		if($scope.filterList.hasFilterTextValues()){
			$scope.sort.ASC('invoiceNumber');
		}else{
			$scope.sort.DESC('invoiceNumber');
		}
		$scope.filterOn = true;
		var params = getSearchParamsSorting();	
		getInvoiceHistoryPageDataSorting(params,FirstPage,false);
	};
	var getInvoiceHistoryPageData =  function(requestParams,page,showLoading){
		$scope.paginationObj = $rootScope.getPaginationObject('invoiceHistoryUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('invoiceHistoryUrl',requestParams,page,showLoading ).then(function(data){
				$scope.selectedRequest = {};
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true);
				}else{
					setUIPageData(data,false);
				}
			});
		}
	};
	
	
	var setUIPageData = function(data,isFirstPageData){
		var ItemList = [];
			if(isNull(data)){
				setItemList(null,isFirstPageData); 
				if(isFirstPageData){
					$rootScope.applyPreviousPaginationObject('invoiceHistoryUrl' , $scope);
				}
				
			}else{
				toggleHeadingName(data);
				ItemList = data.invoiceHistoryBean;
				setItemList(ItemList,isFirstPageData); 
				$scope.sortColumn = data.sortColumn;
				$scope.orderType = data.orderBy;
				
				$scope.propertyList = Util.getObjPropList(ItemList[0]);
				$rootScope.cachePaginationObject('invoiceHistoryUrl' , ItemList, $scope);
				if(isFirstPageData && (! Array.isArray(ItemList) ||  !ItemList.length > 0 ) ){
					$rootScope.applyPreviousPaginationObject('invoiceHistoryUrl' , $scope);
				} else {
					if(!isNull(data.moreRecords)) $scope.moreDataArrow = data.moreRecords;
				}
				
				
			}
			$rootScope.setPageLoadMsg(ItemList,isFirstPageData,$scope);
			$rootScope.scrollToId("result_set");
			$scope.$root.$eval();
		};
		
	var setItemList = function(itemList,isFirstPageData){
		if(Array.isArray(itemList) && itemList.length > 0 ){
			$scope.invoiceData = itemList;				
			
		}else{
			if(!isFirstPageData){
				$scope.invoiceData = [];
			}
		}
		updateFilterStatus(itemList,isFirstPageData);
	};
	
	var updateFilterStatus = function(list,isFirstPageData){
		if(Array.isArray(list) && list.length > 0 && isFirstPageData){
			if(!isNull($scope.filterList)){
			$scope.filterList.cacheFilters();
			}
		}else if($scope.filterOn && isFirstPageData){			
			$scope.filterList.applyPreviousFilters();			
			showMsgNoObjFound();
		}
	};
	
	// sorting 
    $scope.sortASC = function(column){
		$scope.sort.ASC(column);
		sorting();
	};
	
	$scope.sortDESC = function(column){
		$scope.sort.DESC(column);
		sorting();
	};
	
	var sorting = function(){
		var params = getSearchParams();		
		getInvoiceHistoryPageData(params,FirstPage,false);
	};
	
	$scope.previousPage = function(){		
		 var params = getSearchParams();		
		 getInvoiceHistoryPageData(params,PrevPage,true);
	};
	$scope.nextPage = function(){	
		 var params = getSearchParams();		
		 getInvoiceHistoryPageData(params,NextPage,true);
	};
			
	$scope.getFirstPage = function()
	{			
		 var params = getSearchParams();		
		 getInvoiceHistoryPageData(params,FirstPage,true);
				
	};
	var toggleHeadingName = function(data){
		if(data.vatFlag == false)
		{		$scope.headingAmount = Util.translate('COH_AMOUNT_EXCLUDING_VAT', null);  $scope.SortByAmount = "AmountExcludingVAT";	}
		else
		{		$scope.headingAmount = Util.translate('COH_AMOUNT_INCLUDING_VAT', null);  $scope.SortByAmount = " AmountIncludingVAT";	} 


	};
	//  Pagination code ends
	invoiceHistoryCtrlInit();
// old code
//	$rootScope.headingAmount = Util.translate('COH_AMOUNT_INCLUDING_VAT', null);
//	$scope.invoiceHistoryPageIndex =1;
//	$scope.sortColumn = "invoiceNumber";
//	$scope.orderType = "DESC";	
//	$scope.filterParam = [];
//	$scope.filterOn = false;	
//	$scope.dataFound = false;
//	$scope.recordFound = true;
//	$scope.propertyList = [];
//	var requestParams = [];
//	$scope.callState = $state.current.name;
//	var showLoading = true;
//	if($scope.callState == REQ_SUBMIT_INVOICE_VIEW){
//		showLoading = false;
//		var requestObject = DataSharingService.getObject("requestObjectInfo");
//		if(!isNull(requestObject.reference))
//		{requestParams = ["InvoiceNumber",requestObject.reference,"DocumentType",requestObject.docType];}
//	}
//	else
//		{
//			showLoading = true;
//		};
//
//	//DataSharingService.setObject("pageState",$state.current.name);
//	InvoiceService.getInvoiceHistory(requestParams,showLoading).then(function(data){
//		$scope.invoiceData = data.invoiceHistoryBean;	
//		$scope.toggleHeadingName(data);
//		$scope.dataFound = true;
//		$scope.propertyList = Util.getObjPropList(data.invoiceHistoryBean[0]);		
//		$log.log("invoiceHistoryCtrl --  " +data.invoiceHistoryBean.length);
//
//	});
//
//	requestParams = ["OrderBy",$scope.orderType,"SortBy",$scope.sortColumn];
//	var pageKey = Util.initPagination('invoiceHistoryUrl',requestParams);
//
//	// Pagination code
//	paginationObj = DataSharingService.getObject(pageKey);	
//	$rootScope.getInvoicePageData = function(page)
//	{
//		$scope.selectedRequest.selected = false;
//		$log.log("getInvoicePageData() - called, isPaginationBlocked = "+ Util.isPaginationBlocked(pageKey).toString());		
//		if(!Util.isPaginationBlocked(pageKey))
//		{	
//			$log.log("getInvoicePageData() - called" );
//			//requestParams = $scope.getParamKeyList(); // 	
//			requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn); 
//			Util.getPageinationData('invoiceHistoryUrl',requestParams,page ).then(function(data)
//			{
//				if(data.invoiceHistoryBean==null || data.invoiceHistoryBean.length == 0)
//				{	
//					paginationObj.setLastPageNo();					
//					$scope.dataFound = false;
//					$scope.invoiceLoadMsg = Util.translate('CON_NO_MORE_PAGE'); 
//					$log.log("invoiceHistoryCtrl -- " + $scope.invoiceLoadMsg);
//					
//				}
//				else
//				{
//					$log.log("invoiceHistoryCtrl messageCode -- "+data.messageCode);
//					//$rootScope.reLogin(data.messageCode);
//					$scope.dataFound = true;
//					$scope.invoiceLoadMsg = "";						
//					$scope.invoiceData = data.invoiceHistoryBean;
//					requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn); 
//					$scope.$root.$eval();					
//				}			
//				$log.log("paginationObj.isLastPageNo(): " +paginationObj.isLastPageNo());
//			});
//		}
//	};
//
//	$scope.previousPage = function(){		
//		
//		$scope.getInvoicePageData(PrevPage);		
//	};
//	$scope.nextPage = function(){			
//		$scope.getInvoicePageData(NextPage);		
//	};
//			
//	$scope.getFirstPage = function()
//	{			
//		$scope.getInvoicePageData(FirstPage);
//				
//	};
//	//  Pagination code ends
//	
//
//	$scope.toggleHeadingName = function(data){
//		if(data.vatFlag == false)
//		{		$rootScope.headingAmount = Util.translate('COH_AMOUNT_EXCLUDING_VAT', null); 	}
//		else
//		{		$rootScope.headingAmount = Util.translate('COH_AMOUNT_INCLUDING_VAT', null);	} 
//
//
//	};
//
//	 $rootScope.sortSelection = '';
//	 $rootScope.sortSelection =  $scope.sortColumn+$scope.orderType;
//	
//
//	$scope.Sorting = function(column,orderBy) {
//			
//		$scope.selectedRequest.selected = false;
//		var	requestParams = $rootScope.getSortingParam(column,orderBy);
//		
//		pageKey = Util.initPagination('invoiceHistoryUrl',requestParams);
//		InvoiceService.getInvoiceHistory(requestParams).then(function(data){
//			$scope.invoiceData = data.invoiceHistoryBean;
//			$scope.toggleHeadingName(data);			
//			$log.log("invoiceHistoryCtrl sorting--  " +data.invoiceHistoryBean.length);
//
//		});
//	};
//	
//	// Search Filters 
//	$scope.filterFormInfo = {};
//	$scope.filterMsg = "";
//	$scope.submitInvoiceFilter = function()
//	{	$scope.selectedRequest.selected = false;
//		$scope.filterOn = true;
//		var keyList = $scope.filterFormInfo;
//		$scope.filterParam =  $rootScope.setParamKeyList(keyList); 
//		var filterParam =  $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn); 
//		$log.log("invoiceHistoryCtrl filterParam--  " + filterParam);
//		pageKey = Util.initPagination('invoiceHistoryUrl',filterParam);
//		InvoiceService.getInvoiceHistory(filterParam,showLoading).then(function(data){
//
//			if(data.invoiceHistoryBean==null || data.invoiceHistoryBean.length == 0){
//				$scope.filterMsg =  Util.translate('MSG_NO_OBJECTS_FOR_SELECTION',null);
//				showMsgNoObjFound();
//				var icon = '<a class="OrderInformation-icon" shref="" title="'+$scope.filterMsg+'"></a>';				
//				$scope.filterMsg = icon + $scope.filterMsg;
//				$scope.recordFound = false;
//			}
//			else{
//			//	$scope.invoiceData=[];
//				$scope.invoiceData = data.invoiceHistoryBean;
//				$scope.toggleHeadingName(data);				
//				$scope.filterMsg = "";
//				$log.log("invoiceHistoryCtrl filter--  " +data.invoiceHistoryBean.length);
//				$log.log("invoiceHistoryCtrl invoiceData--  " +$scope.invoiceData.length);
//				$scope.recordFound = true;
//				$scope.$root.$eval();
//				$rootScope.scrollToId("result_set");		
//			}
//		});
//	};
//
//	var requestFilterParams = [];	
//	// to get Invoice search filter key list 
//	InvoiceService.getInvoiceFilterList(requestFilterParams).then(function(data){
//		//$http.get('./json/catalog/invoice_search_filter.json').success(function(data){
//		$rootScope.invoiceSearchFilterList = data.filterBeans;		
//		$scope.getFilterSettings(data.filterBeans);		
//		$log.log("invoiceHistoryCtrl: invoiceSearchFilterList --  " +data.filterBeans.length);
//
//	});
//		
//	$rootScope.filterDetailSettings = DataSharingService.getFilterDetailsSettings();
//		
//	// to get filter configuration setting from "filterConfig.json"  
//	$scope.getFilterSettings = function(filterList)
//	{
//		 var filterConfigData = InvoiceService.getFilterConfiguration();
//		 $log.log("invoiceHistoryCtrl getFilterSettings--  " +JSON.stringify(filterConfigData));			 
//		 $rootScope.invoiceSearchFilterList =  $rootScope.getSearchFilterSettings($rootScope.invoiceSearchFilterList,filterConfigData);
//	};
//	
	// Search Filters 
	
	$scope.showInvoiceDetail = function(invoice,fromRequest){
	//	DataSharingService.setObject("invoice_clickedDetail",invoice);
		if($rootScope.isShowDisableMenuItem('CON_INVOICE_DETAIL')){
		DataSharingService.setToSessionStorage("invoice_clickedDetail",invoice);
		$rootScope.fromInvoice = true;
		DataSharingService.setToSessionStorage("fromInvoice",$rootScope.fromInvoice);
		if(fromRequest)
		{
			$state.go(REQ_SUBMIT_INVOICE_DETAIL_VIEW);
			}
		else
		{			
			var needToLogin = $rootScope.isSignOnRequired("CON_INVOICE_DETAIL",INVOICE_DETAIL_VIEW,true);
			if(!needToLogin){
				$state.go(INVOICE_DETAIL_VIEW); 	
			}
		}
		}
	};
	
	$scope.showOrderDetail = function(order){
	//	DataSharingService.setObject("order_clickedDetail",order);
		if($rootScope.isShowDisableMenuItem('CON_ORDER_INFORMATION')){
			DataSharingService.setToSessionStorage("order_clickedDetail",order);
			$rootScope.fromCart = false;
			$rootScope.fromRequestPage = false;
			DataSharingService.setToSessionStorage("fromCart",$rootScope.fromCart);
			DataSharingService.setToSessionStorage("fromRequestPage",$rootScope.fromRequestPage);
			$state.go(ORDER_DETAIL_VIEW,{"ORDER_NUM": order.orderNumber});
		}
	};
	
	$scope.exportInvoiceTableAsPDF=function(){
		var invoiceSearchTable = setInvoiceSearchDataforExport();
		var data = invoiceSearchTable.getHTML();
		var title = Util.translate('CON_INVOICE_SEARCH');
		var fileName = "InvoiceSearch_"+getCurrentDateTimeStr()+".pdf";
		savePdfFromHtml(data,title,fileName);
	};
	
	$scope.exportInvoiceTableAsExcel=function(){
		var invoiceSearchTable = setInvoiceSearchDataforExport();
		var data = invoiceSearchTable.getDataWithHeaders();
		var sheetName = "InvoiceSearch";
		var fileName = "InvoiceSearch_"+getCurrentDateTimeStr()+".xlsx";
		saveExcel(data,sheetName,fileName);
	};
	
	$scope.exportInvoiceTableAsXML=function(){
		var invoiceSearchTable = setInvoiceSearchDataforExport('XML');
		invoiceSearchTable.setRootElementName("INVOICE_SEARCH");
		invoiceSearchTable.setEntityName("INVOICE_DETAIL");
		var data = invoiceSearchTable.getXML();
		var fileName = "InvoiceSearch_"+getCurrentDateTimeStr()+".xml";
		saveXML(data,fileName);
	};
	
	var setInvoiceSearchDataforExport = function(target){
		var invoiceSearchTable = new Table();
		if(isEqualStrings(target,"XML")){
			invoiceSearchTable.addHeader('INVOICE');
			invoiceSearchTable.addHeader('DOC_DATE');
			invoiceSearchTable.addHeader('TYPE');
			if($rootScope.isColExist('invoiceCustomer',$scope.propertyList)){
				invoiceSearchTable.addHeader('INVOICE_CUSTOMER');
			}
			if($scope.headingAmount.indexOf('excl') !== -1){
				invoiceSearchTable.addHeader("AMOUNT_EXCL_VAT");
			}else{
				invoiceSearchTable.addHeader("AMOUNT_INCL_VAT");
			}
			invoiceSearchTable.addHeader('ORDER');
			invoiceSearchTable.addHeader('ORDER_DATE');		
			invoiceSearchTable.addHeader('SALESMAN');		
		}else{
			invoiceSearchTable.addHeader(Util.translate('CON_INVOICE'));
			invoiceSearchTable.addHeader(Util.translate('COH_DOCUMENT_DATE'));
			invoiceSearchTable.addHeader(Util.translate('COH_TYPE'));
			if($rootScope.isColExist('invoiceCustomer',$scope.propertyList)){
				invoiceSearchTable.addHeader(Util.translate('COH_INVOICE_CUSTOMER'));
			}
			invoiceSearchTable.addHeader($scope.headingAmount);
			invoiceSearchTable.addHeader(Util.translate('CON_ORDER'));
			invoiceSearchTable.addHeader(Util.translate('CON_ORDER_DATE'));
			invoiceSearchTable.addHeader(Util.translate('COH_SALESMAN'));
		}
		//set row data
		data = $scope.invoiceData;
		if(!isNull(data)){
			for(var i=0;i<data.length;i++){
				var row=[];
				var amount=data[i].amount;
				var order= "";
				if(!data[i].mergeInvoiceFlag){
					order= data[i].orderNumber;	
				}
				row.push(data[i].invoiceNumber);
				row.push(data[i].invoiceDate);
				row.push(data[i].typeDescription);
				if($rootScope.isColExist('invoiceCustomer',$scope.propertyList)){
					row.push(data[i].invoiceCustomer);
				}
				row.push(amount);
				row.push(order);
				row.push(data[i].orderDate);
				row.push(data[i].salesman);
				invoiceSearchTable.addRow(row);
			}	
		}else{
			var row = [];			
			for(var i=0;i<invoiceSearchTable.headers.length;i++){
				row.push("");
			}
			invoiceSearchTable.addRow(row);
		}

		return invoiceSearchTable;
	};
	
}]);
