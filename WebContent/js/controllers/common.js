/**
 * contains properties and methods that are common to all controllers in js.controllers package
 * @param 
 * @param 
 * @constructor
 * @export
 * @ngInject
 */
var CONST_mainCatPrevCompareView = "mainCatPrevCompareView";
var catalougeControllers = angular.module('catalougeControllers', []);
var CONST_State_Home = 'home';
var PROD_SEARCH_RESULT_VIEW= 'home.prodSearchResult';
var PROD_SEARCH_RESULT_LISTVIEW = 'home.prodSearchResult.listView';
var PROD_SEARCH_RESULT_GRIDVIEW = 'home.prodSearchResult.gridView';
var HOME_LIST_VIEW = 'home.homeCatalogTable';
var HOME_GRID_VIEW = 'home.homeCatalogGrid';
var INVOICE_DETAIL_VIEW ='home.invoice.detail';
var ORDER_DETAIL_VIEW = 'home.order.detail';
var REQ_SUBMIT_VIEW= 'home.requestsubmit';
var REQ_SUBMIT_ORDER_VIEW = 'home.requestsubmit.order';
var REQ_SUBMIT_ORDER_DETAIL_VIEW = 'home.requestsubmit.orderdetail';
var REQ_SUBMIT_INVOICE_VIEW = 'home.requestsubmit.invoice';
var REQ_SUBMIT_INVOICE_DETAIL_VIEW = 'home.requestsubmit.invoicedetail';
//ADM-XXX
var REQ_SUBMIT_TRANSPORTNOTE_VIEW = 'home.requestsubmit.transportnote';
var REQ_SUBMIT_TRANSPORTNOTE_DETAIL_VIEW = 'home.requestsubmit.transportnotedetail';
//ADM-XXX End
var REQ_SUBMIT_SHOPPING_LIST_VIEW = 'home.requestsubmit.shoppinglist';
var REQ_ADD_PRODUCT_VIEW = 'home.requestsubmit.addproduct';
var REQ_NEW_LINE_VIEW = 'home.requestsubmit.requestLine';
var REQ_SUBMIT_CONFIRMATION = 'home.requestsubmit.confirmation';
var REQ_SUBMIT_RECEIVED ='home.requestsubmit.received';
var REQ_SUBMIT_RECEIVED_COMPARE ='home.requestsubmit.received.compare';
var PROMOTION_LIST_REQ_SUBMIT_RECEIVED ='home.requestsubmit.received.promotionList';
var PROMOTION_GRID_REQ_SUBMIT_RECEIVED ='home.requestsubmit.received.promotionGrid';
var PRODUCT_DETAIL ='home.productDetail';
var CATALOG_DETAIL ='home.catalog';
var PROMOTION_LIST_PRODUCT_DETAIL ='home.productDetail.promotionList';
var PROMOTION_GRID_PRODUCT_DETAIL ='home.productDetail.promotionGrid';
var PRODUCT_DETAIL_COMPARE = 'home.productDetail.compare';
var THANK_YOU = 'home.goThankYou';
var THANK_YOU_COMPARE = 'home.goThankYou.compare';
var PROMOTION_LIST_THANKYOU ='home.goThankYou.promotionList';
var PROMOTION_GRID_THANKYOU ='home.goThankYou.promotionGrid';
var SHOPPING_CART = 'home.shoppingcart';
var SHOPPING_CART_COMPARE = 'home.shoppingcart.compare';
var PROMOTION_LIST_SHOPPING_CART ='home.shoppingcart.promotionList';
var PROMOTION_GRID_SHOPPING_CART ='home.shoppingcart.promotionGrid';
var BEST_OFFERS_STATE = 'home.bestoffer';
var MOST_POPULAR_STATE = 'home.popular';
var PROMOTION_GRID_BEST_OFFERS = 'home.bestoffer.promotionGrid';
var PROMOTION_LIST_BEST_OFFERS = 'home.bestoffer.promotionList';
var PROMOTION_GRID_MOST_POPULAR = 'home.popular.promotionGrid';
var PROMOTION_LIST_MOST_POPULAR = 'home.popular.promotionList';
//ADM-008
var ALTERNATIVES_STATE ='home.alternatives';
var ALTERNATIVES_LIST_STATE ='home.alternatives.list';
var ALTERNATIVES_GRID_STATE ='home.alternatives.grid';
//ADM-008 end
var REQUEST_VALIDATION_SUCCESSFULL = '4310';
var ORDER_NUMBER_REQUIRED = '4302'; 
var PROBLEM_TYPE_MISSING =  '4303';
var REQUEST_DESCRIPTION_MISSING = '4304';
var REQUEST_SUCCESSFULL = '4300';
var REQUEST_ERROR = '4301';
var INVALID_REQUEST_REFERENCE = '4305';
var REQUEST_LINE_SUCCESSFULL = '4400';
var REQUEST_LINE_ERROR = '4401';
var INPUT_PARAMETER_NOT_VALID = '2110';
var CATALOG_GROUP_SIZE = 5;
var FirstPage = "first";
var NextPage = "next";
var PrevPage = "prev";
var CONST_PAY_METHOD_INVOICE = "INVOICE";
var CONST_PAY_METHOD_ONLINE = "ONLINE_PAYMENT";

function getDefaultImg(source) {
	source.onerror=null;
	source.src='./images/ImageIcon.png';
	return true;
};
function getDefaultBannerImg(source) {
	source.onerror=null;
	source.src='./images/BannerDefaultImage.jpg';
	return true;
};
function getDefaultShipmentImage(source) {
	source.onerror=null;
	source.src='./images/ShipmentTracking.png';
	return true;
};
var historyManager = new UIHistoryManager();
var translate = new Translate();
var showMsgNoObjFound = null;
var searchInstanceIssueMsgFound = null;

var webSettings;
 
 var setWebSettingObject= function(webSettingsObject)
 {
	 webSettings = webSettingsObject;
 };

 var DEBUG = false;
////ENABLE/DISABLE Console Logs
if(!DEBUG){

//console.log = function() {};

}
	
	
	
	
	function Email(from, subject, message,mailTo) {
	    this.mailFrom = from ;
	    this.mailTo = returnBlankIfNull(mailTo) ;
	    this.subject = subject;
	    this.body =  message;
	    this.UIErrorKey = null;
	    this.UIErrorParams =[];
	    this.startShowError = false;
	    this.showMailFrom=true;
	    this.showMailTo=false;
	}
	
	Email.prototype.isValidEmailAddress = function() {
		return true;
//		var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
//		if (re.test(this.mailFrom)){  
//		    return true;
//		}else{
//			return false;
//		}
	};
	
	Email.prototype.isValidValues = function() {
		var isValid = true;
		if(isEmptyString(this.subject.trim()) || isEmptyString(this.body.trim()) ) {
			if(this.startShowError){
				this.UIErrorKey ='MSG_MAIL_SUBJECT_AND_MESSAGE_MUST_BE_ENTERED';
			}
			return isValid = false;
		}
		if(!this.isValidEmailAddress()) {
			if(this.startShowError){
				this.UIErrorKey ='MSG_INVALID_EMAIL_ADDRESS';
			}
			return isValid = false;
		}
		if(isValid){
			this.UIErrorKey  = null;
		}
		return isValid;
	};
	
	Email.prototype.getUIErrorKey = function() {
		return this.UIErrorKey;
	};
	
		/*		 
		  utility code
		  
		 */
	var showAvailabilityAsLink = function(){
		
		var itemAvailabilityType = webSettings.itemAvailabilityType;
		var showAsLink = true; 
		switch(itemAvailabilityType){
		case '*LINK' :
		case '*MAX_100' :	
		case '*MIN' :
			showAsLink = true;
			break;
		}

		return showAsLink;
	};
	
	var showAvailabilityAsImage = function(){
		//webSettings = $rootScope.webSettings;
		var itemAvailabilityType = webSettings.itemAvailabilityType;
		var showAsImage = false; 
		switch(itemAvailabilityType){
		case '*YES_NO_ICON' :
		case '*YES_NO_ICONS' :
		case '*NO_ICON'	:
		case '*YES_ICON' :
		case '*LINK' :	
			showAsImage = true;
			break;
		}
	
		return showAsImage;
	};
	
	var getAvailabilityImage = function(availabilityValue){
	//	webSettings = $rootScope.webSettings;
	var	itemAvailabilityType = webSettings.itemAvailabilityType;
		var imageName = ''; 
		var val = '';
		if(!isNull(availabilityValue) ){
			val = availabilityValue.toUpperCase();
		}
		switch(itemAvailabilityType){
		case '*YES_NO_ICON' :
		case '*YES_NO_ICONS' :
		case '*NO_ICON'	:
		case '*YES_ICON' :
			if(isEqualStrings(val,'YESIMAGE',true)){
				imageName = 'YesImage.png';
			}if(isEqualStrings(val,'NOIMAGE',true)){
				imageName = 'NoImage.png';
			}
			break;
		case '*LINK' :
			//show ware house image in this case
			imageName = 'InfoIcon.png';
			break;
		};

	//	$log.log("getAvailabilityImage "+ imageName);
		return imageName;
	};
	
	var getAvailabilityText = function(availabilityValue){
	//	webSettings = $rootScope.webSettings;
	var	itemAvailabilityType = webSettings.itemAvailabilityType;
		var val = null;
		var returnVal = null;
		if(!isNull(availabilityValue)){
			switch(itemAvailabilityType){
			case '*YES_NO_TEXT' :
				val = availabilityValue.toUpperCase();
				if(val=='YES'){
					returnVal = translate.getTranslation('CON_YES');
				}if(val=='NO'){
					returnVal = translate.getTranslation('CON_NO');
				}
				break;
			case '*MAX_100' :	
				if(val > 100){
					returnVal = ">100";
				}
				break;
			}
		}
		if (returnVal==null){
			returnVal = availabilityValue;
		}
		//$log.log("getAvailabilityText "+ returnVal);
		return returnVal;
	};
	var getEnquiryImageName = function(enquiryImage){
		var imageName = null;
		if(!isNull(enquiryImage)){
			imageName = 'Email.png';
		}
		return imageName;
	};
	
	var showEnquiryImage = function(enquiryImage){
		var bool = false;
		if(!isNull(enquiryImage) && isEqualStrings(enquiryImage,'InquiryImage',true)){
			bool = true;
		}
		return bool;
	};

	var handleEmptyAvailabilityImage = function(item){
		if(!isNull(item)){
			if(item.showAvailabilityAsImage && isEqualStrings(item.availabilityImageName,"")){
				item.showAvailabilityAsImage = false;
			}
		}
	};
	
	var displayAvailabilityLinkImage = function (item){
		var flag = true;
		if(!isNull(item)){
			if(item.showAvailabilityAsLink && item.showAvailabilityAsImage && !isEqualStrings(item.availabilityImageName,"")){
				flag = true;
			}
			if(isEqualStrings(item.availabilityImageName,"")){
				flag = false;
			}
		}
		return flag;
	};
	
	var displayAvailabilityLinkText = function (item){
		var flag = false;
		if(!isNull(item)){
			if(item.showAvailabilityAsLink && !item.showAvailabilityAsImage){
				flag = true;
			}
		}
		return flag;
	};
	
	var displayAvailabilityImage = function (item){
		var flag = false;
		if(!isNull(item)){
			if(item.showAvailabilityAsLink && item.showAvailabilityAsImage && !isEqualStrings(item.availabilityImageName,"")){
				flag = true;
			}
		}
		return flag;
	};
	
	var displayAvailabilityText = function (item){
		var flag = false;
		if(!isNull(item)){
			if(!item.showAvailabilityAsLink && !item.showAvailabilityAsImage){
				flag = true;
			}
		}
		return flag;
	};
	
	var displayWhsAvailabilityImage = function (itemWhsAvailability){
		var showAsImage = false;
		//webSettings = $rootScope.webSettings;
		var warehouseAvailabilityType = webSettings.warehouseAvailQty;
		var showAsImage = false; 
		switch(warehouseAvailabilityType){
		case '*YES_NO_ICONS' :
		case '*NO_ICON'	:
		case '*YES_ICON' :
		case '*LINK' :	
			showAsImage = true;
			break;
		}
		if(isNull(itemWhsAvailability) || isEmptyString(itemWhsAvailability) ){
			showAsImage = false;
		}
		
		return showAsImage;
	};

	var getWhsAvailabilityImage = function(itemWHSAvailability){
		var	whsAvailabilityType = webSettings.warehouseAvailQty;
			var imageName = ''; 
			var val = '';
			if(!isNull(itemWHSAvailability) ){
				val = itemWHSAvailability.toUpperCase();
			}
			switch(whsAvailabilityType){
			case '*YES_NO_ICON' :
			case '*YES_NO_ICONS' :
			case '*NO_ICON'	:
			case '*YES_ICON' :
				if(isEqualStrings(val,'YESIMAGE',true)){
					imageName = 'YesImage.png';
				}if(isEqualStrings(val,'NOIMAGE',true)){
					imageName = 'NoImage.png';
				}
				break;
			case '*LINK' :
				//show ware house image in this case
				imageName = 'InfoIcon.png';
				break;
			};

			return imageName;
		};
		
		var getWhsAvailabilityText = function(itemWhsAvailability){
			var	whsAvailabilityType = webSettings.warehouseAvailQty;
			var val = null;
			var returnVal = null;
			if(!isNull(whsAvailabilityType)){
				switch(whsAvailabilityType){
				case '*YES_NO_TEXT' :
					val = itemWhsAvailability.toUpperCase();
					if(val=='YES'){
						returnVal = translate.getTranslation('CON_YES');
					}if(val=='NO'){
						returnVal = translate.getTranslation('CON_NO');
					}
					break;
				case '*MAX_100' :	
					if(val > 100){
						returnVal = ">100";
					}
					break;
				}
			}
			if (returnVal==null){
				returnVal = itemWhsAvailability;
			}
			return returnVal;
		};
		
	var displayWhsOnHand = function (itemWhsOnHand){
		var showAsImage = false;
		//webSettings = $rootScope.webSettings;
		var whsOnHandType = webSettings.warehouseOnHand;
		var showAsImage = false; 
		switch(whsOnHandType){
		case '*YES_NO_ICONS' :
		case '*NO_ICON'	:
		case '*YES_ICON' :
			showAsImage = true;
			break;
		}
		if(isNull(itemWhsOnHand) || isEmptyString(itemWhsOnHand) ){
			showAsImage = false;
		}
		
		return showAsImage;
	};

	var getWhsOnHandImage = function (itemWhsOnHand){
		var	whsOnHandType = webSettings.warehouseOnHand;
			var imageName = ''; 
			var val = '';
			if(!isNull(itemWhsOnHand) ){
				val = itemWhsOnHand.toUpperCase();
			}
			switch(whsOnHandType){
			case '*YES_NO_ICON' :
			case '*YES_NO_ICONS' :
			case '*NO_ICON'	:
			case '*YES_ICON' :
				if(isEqualStrings(val,'YESIMAGE',true)){
					imageName = 'YesImage.png';
				}if(isEqualStrings(val,'NOIMAGE',true)){
					imageName = 'NoImage.png';
				}
				break;
			};
			//console.log("getWhsOnHandImage() - " + imageName);
			return imageName;
		};
		
		var getWhsOnHandText = function (itemWhsOnHand){
			var	whsOnHandType = webSettings.warehouseOnHand;
			var val = null;
			var returnVal = null;
			if(!isNull(whsOnHandType)){
				switch(whsOnHandType){
				case '*YES_NO_TEXT' :
					val = itemWhsOnHand.toUpperCase();
					if(val=='YES'){
						returnVal = translate.getTranslation('CON_YES');
					}if(val=='NO'){
						returnVal = translate.getTranslation('CON_NO');
					}
					break;
				case '*MAX_100' :	
					if(val > 100){
						returnVal = ">100";
					}
					break;
				}
			}
			if (returnVal==null){
				returnVal = itemWhsOnHand;
			}
			return returnVal;
		};
		
	var setCatalogExtendedData = function(list){
		//$log.log("setHomeCatalogExtendedData : product list : " +JSON.stringify($scope.productList));
		if(!isNull(list)){
			for (var i=0;i<list.length ; i++){
				var  item  = list[i];
				item.showAvailabilityAsLink = showAvailabilityAsLink();
				item.showAvailabilityAsImage = showAvailabilityAsImage();
				item.availabilityImageName = getAvailabilityImage(item.availabilityOfItem);
				item.availabilityText = getAvailabilityText(item.availabilityOfItem);
				item.showEnquiryImage = showEnquiryImage(item.enquiryImage);
				item.enquiryImage = getEnquiryImageName(item.enquiryImage);
				handleEmptyAvailabilityImage(item);
				//append % if discountPercentage has value
				if(!isEmptyString(item.discountPercentage)){
					item.discountPercentageText = item.discountPercentage + " %";
				}
				if(!item.hasOwnProperty("salesUnit")){
					item.salesUnit=item.defaultSalesUnit;
				}
				if(!item.hasOwnProperty("salesUnitObject")){
					item.selectedUnitObject= {};
					item.selectedUnitObject = getSelectedSalesUnit(item.salesUnit,item.salesUnitsDesc);
				}
			}
			addpropertyToList(list,'validQuantity',true);
		}
	};
	
	var setQuotationExtendedData = function(list){
		if(!isNull(list)){
			for (var i=0;i<list.length ; i++){
				var  item  = list[i];
				item.showAvailabilityAsLink = showAvailabilityAsLink();
				item.showAvailabilityAsImage = showAvailabilityAsImage();
				item.availabilityImageName = getAvailabilityImage(item.availability);
				item.availabilityText = getAvailabilityText(item.availability);
				handleEmptyAvailabilityImage(item);
			}
		}
	};
	
	var setTableHeader = function(rowObj,columnsToCheck,tableHeaderObject){
		var name = null;
		for (var i=columnsToCheck.length-1;i>=0; i--){
			name = columnsToCheck[i];
			if (rowObj.hasOwnProperty(name)){
				tableHeaderObject[name+''] = true;
				columnsToCheck.splice(i, 1); //remove, to avoid further check
				////console.log("columnsToCheck : " + JSON.stringify(columnsToCheck));
			}
		}
	};
	
	
	
	
	
	function clearListLines(lineList, buyObjList){
		for(var i=0;i<lineList.length;i++){
			var lineObj=lineList[i];
			
			for(var j=0;j<buyObjList.length;j++){
				var obj=buyObjList[j];
				if(obj.itemCode==lineObj.itemCode && obj.messageCode==3014){
					lineObj.itemCode=null;
					lineObj.ordered=null;
					lineObj.unit=null;
					lineObj.delDate=null;
					lineObj.shipmentMarking=null;
					lineObj.showUnitList = false;
					lineObj.activeUnitList = [];
					lineObj.isValidProduct = true;
					lineObj.validQuantity=true;
					lineObj.UIErrorKey=null;
					lineObj.errorMsg= "";
					break;					
				}				
			}
		}

	};
	
	
	
	var setFilterRequiredMessage = function(filter,secondDateInuput){
		var filterName = translate.getTranslation(filter.filterName,null);
				
		var msg =	translate.getTranslation('MSP_MUST_BE_ENTERED',[filterName]);
		if(secondDateInuput){
			filter.isValidInput2 = false;
			filter.validationMsg2 =   msg;
		}else{
			filter.isValidInput = false;
			filter.validationMsg  =  msg;	
		}
		
	};
	var resetFilterMessage = function(filter,secondDateInuput){
		if(secondDateInuput){
			filter.isValidInput2 = true;
			filter.validationMsg2 =  "";
			
		}else{
			filter.isValidInput = true;
			filter.validationMsg  =  "";	
		
		}
		
	};
	var clearFilterMessage = function(filter){
	
			filter.isValidInput2 = true;
			filter.validationMsg2 =  "";
			filter.isValidInput = true;
			filter.validationMsg  =  "";	
	};
	var setDateMessage = function(filter,secondDateInuput){
		var filterName = translate.getTranslation(filter.filterName,null);
		
		var msg =	translate.getTranslation('MSG_INVALID_DATE',[filterName]);
		if(secondDateInuput){
			filter.isValidInput2 = false;
			filter.validationMsg2 =   msg;
		}else{
			filter.isValidInput = false;
			filter.validationMsg  =  msg;	
		}
		
	};
	var setAmountMessage = function(filter,secondDateInuput){
		var filterName = translate.getTranslation(filter.filterName,null);
		
		if(secondDateInuput){
			filter.isValidInput2 = false;
			filter.validationMsg2 =   translate.getTranslation('MSP_INVALID_NUMERIC_VALUE',[filterName,filter.selectedItems2]);;
		}else{
			filter.isValidInput = false;
			filter.validationMsg  =  translate.getTranslation('MSP_INVALID_NUMERIC_VALUE',[filterName,filter.selectedItems1]);;	
		}
		
	};
	var isFilterRangeEmpty = function(filterObj) {
		var isEmpty = false ; 
		var isFirstInputEmpty = false;
		var isSecondInputEmpty = false;
            // it considers only text boxes
		if(filterObj.rangeType){
			var isBothInputRequired =(isEqualStrings(filterObj.selectedItems,"*BETWEEN",true) || isEqualStrings(filterObj.selectedItems,"*NOT_BETWEEN",true));
			switch(filterObj.selectedItems){
			case "*BETWEEN" :
			case "*NOT_BETWEEN" :
				if(isNull(filterObj.selectedItems1 ) ||isEmptyString(filterObj.selectedItems1)){	
					isFirstInputEmpty = true ;
					setFilterRequiredMessage(filterObj,false);
				}else{
					isFirstInputEmpty = false;
				}
				if(isBothInputRequired){
					if(isNull(filterObj.selectedItems2)||isEmptyString(filterObj.selectedItems2)){
						isSecondInputEmpty = true ;
						setFilterRequiredMessage(filterObj,true);
					}
					else{
						isSecondInputEmpty = true ;
					}
				}
				if(isFirstInputEmpty && isSecondInputEmpty){
					isEmpty = true ;
				}
				break;
			case "*ALL" :
				//resetFilterMessage(filterObj,false);
				clearFilterMessage(filterObj);
				filterObj.selectedItems1 =  "";	
				filterObj.selectedItems2 =  "";		
				isEmpty = true ;
				break;
			default :
				resetFilterMessage(filterObj,true);
				filterObj.selectedItems2 =  "";	
				if(isNull(filterObj.selectedItems1 ) ||isEmptyString(filterObj.selectedItems1)){	
					isEmpty = true ;
					setFilterRequiredMessage(filterObj,false);
				}
			

			}
			
		}
		return isEmpty;
	};
			
	///
	
	
	
	function parseIntegerQuantity(quantityStr){
		
		var quantity = parseInt(quantityStr);
		if(isNaN(quantity)){
			quantity="0";
		}
		return quantity+"";
		
	} ;
	function getSelectedSalesUnit(defaultSalesUnit,unitList){
		var selectedUnit ={};
	     selectedUnit = getObjectFromList('salesUnit',defaultSalesUnit,unitList); 
		 return selectedUnit;
		
	};