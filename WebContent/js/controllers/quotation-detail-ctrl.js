
//Quotation detail ctrl
catalougeControllers.controller('quotationDetailCtrl', ['$rootScope','$scope', '$http','$log', 'DataSharingService','QuotationService', 'Util','$state',
                                                        function ($rootScope,$scope, $http,$log,DataSharingService,QuotationService,Util,$state) {
	var showLoading = true;
	var quotationDetailCtrlInit = function(){
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
	};
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
	//	$log.log(" quotationDetailCtrlInit history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	var initController = function()
	{ 
//		$rootScope.initCommitsSecondNextPromise.then(function(){
		$scope.isUserSessionActive = true;
		$scope.cachedPaginationObj = null;
		$scope.paginationObj = null;
		$scope.isCollapsed = false;
		$scope.propertyList = [];	
		$scope.sort = new Sort();
		$scope.sort.ASC('lineNumber');
		$scope.sortType = 'lineNumber';
		$scope.quotationDetailProperty = [];
		$scope.callState = $state.current.name;
		var params = getRequestParams();
		if(webSettings == undefined){
			setWebSettingObject(DataSharingService.getWebSettings());
		}
		getQuotationDetailPageData(params,FirstPage,showLoading);
//		});
		
	};
	var getRequestParams = function(){
		var param = [];
		//var quotation = DataSharingService.getObject("quotation_clickedDetail");
		var quotation = DataSharingService.getFromSessionStorage("quotation_clickedDetail");
	
		if( !isNull(quotation)){
			$scope.quotation = quotation;
			param = ["QuotationNumber",quotation.QuotationNumber,"VersionNumber",quotation.VersionNumber,"Type",quotation.Type];
			//console.log("quotation detail "+ JSON.stringify(quotation));
		};
		
		//console.log("Quotation line detail requestParams"+ JSON.stringify(param));
		return param;
	};
	var getQuotationDetailPageData =  function(requestParams,page,showLoading){
		$scope.paginationObj = $rootScope.getPaginationObject('quotationDetailsUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		var pageNo = "";
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('quotationDetailsUrl',requestParams,pageNo,showLoading ).then(function(data){
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true);
				}else{
					setUIPageData(data,false);
				}
			});
		}
	};
	var setUIPageData = function(data,isFirstPageData){
		var ItemList = data.quotationLineBean;
		if(isNull(data)){
			setQuotationLineList(null,isFirstPageData); 
			if(isFirstPageData){
				$rootScope.applyPreviousPaginationObject('quotationDetailsUrl' , $scope);
			}

		}else{
			$scope.quotationLineData = [];
			if(angular.isDefined(data.quotationLineBean) && !isNull(data.quotationLineBean)){
				//setOrderDetailExtendedData(data.quotationLineBean); 
				ItemList =  data.quotationLineBean;
				//$scope.initBuyObject(ItemList);
				$scope.quotationDetailProperty = data.quotationLineBean[0];
				$scope.propertyList = Util.getObjPropList(data.quotationLineBean[0]);	
				addpropertyToList(ItemList,'validQuantity',true);
				$scope.initBuyObject(ItemList);
				//console.log("quotationDetailCtrl : quotationLineData length--  " +ItemList.length);

			}
			$scope.quotationDetailsPropList = Util.getObjPropList(data);	
			$scope.quotationDetails = data;
			$rootScope.changeHomePageTitle(Util.translate('TXT_PAGE_TITLE_QUOTATION_WITH_NUMBER') + $scope.quotationDetails.quotationNumber, true);
			$scope.currency = data.currency;
			//DataSharingService.setObject("orderDetail",data);
			setQuotationLineList(ItemList,isFirstPageData); 
			$rootScope.cachePaginationObject('quotationDetailsUrl' , ItemList, $scope);
			if(isFirstPageData && (! Array.isArray(ItemList) ||  !ItemList.length > 0 )){
				$rootScope.applyPreviousPaginationObject('quotationDetailsUrl' , $scope);
			}

		}
		$rootScope.setPageLoadMsg(ItemList,isFirstPageData,$scope);
		//$log.log("object info : "+JSON.stringify($scope.invoiceDetails));
		$scope.$root.$eval();
	};

	var setQuotationLineList = function(itemList,isFirstPageData){
		if(Array.isArray(itemList) && itemList.length > 0 ){
			setQuotationExtendedData(itemList);
			$scope.quotationLineData = itemList;				

		}else{
			if(!isFirstPageData){
				$scope.quotationLineData = [];
			}
		}
	};
	$scope.previousPage = function(){		
		 var params = getRequestParams();		
		 getQuotationDetailPageData(params,PrevPage,true);
	};
	$scope.nextPage = function(){	
		 var params = getRequestParams();		
		 getQuotationDetailPageData(params,NextPage,true);
	};
			
	$scope.getFirstPage = function()
	{			
		 var params = getRequestParams();		
		 getQuotationDetailPageData(params,FirstPage,true);
				
	};
	$scope.sortASC = function(column){
		$scope.sort.ASC(column);
		
	};
	
	$scope.sortDESC = function(column){
		$scope.sort.DESC(column);
		
	};
	// new code
	// old code
	
//	var requestParams = [];
//	$scope.dataFound = false;
//	$scope.resultMsg ="";
//	$scope.errorMessage = "";
//	$scope.propertyList = [];
//	$scope.callState = $state.current.name;
////	$scope.prevState = 'home.quotation';
////	DataSharingService.setObject("pageState",$state.current.name);
//	$scope.quotationDetailProperty = [];
//
//	var quotation = DataSharingService.getObject("quotation_clickedDetail");
//	if( quotation !=null){
//		$scope.quotation = quotation;
//		requestParams = ["QuotationNumber",quotation.QuotationNumber,"VersionNumber",quotation.VersionNumber,"Type",quotation.Type];
//		$log.log("quotation detail "+ JSON.stringify(quotation));
//		$log.log("quotation detail requestParams"+ JSON.stringify(requestParams));
//	};
//
//	QuotationService.getQuotationDetailList(requestParams).then(function(data){		
//		//if(data.quotationLineBean==null || data.quotationLineBean.length == 0){
//		if(isNull(data))	{
//			$scope.errorMessage = data.errorMessage;			
//		}
//		else
//		{
//			$rootScope.quotationLineData = [];
//			$log.log("quotationDetailCtrl messageCode -- "+data.messageCode);
//			//$rootScope.reLogin(data.messageCode);			
//			if(angular.isDefined(data.quotationLineBean) && !isNull(data.quotationLineBean))
//				{
//				$rootScope.quotationLineData = data.quotationLineBean;	
//				$rootScope.quotationDetailProperty = data.quotationLineBean[0];
//				$scope.propertyList = Util.getObjPropList(data.quotationLineBean[0]);
//				$log.log("quotationDetailCtrl : quotationLine length--  " +data.quotationLineBean.length);
//				}
//			
//			$scope.quotationDetails = data;			
//			$scope.currency = $scope.quotationDetails.currency;			
//			$scope.errorMessage = " ";	
//			$scope.dataFound = true;
//					
//			$scope.quotationDetailsPropList = Util.getObjPropList(data);			
//			
//
//		}
//
//	});
//
//	// Pagination code
//	var pageKey = Util.initPagination('quotationDetailsUrl',requestParams);	
//	paginationObj = DataSharingService.getObject(pageKey);	
//	$rootScope.getQuotationDetailsPageData = function(page)
//	{
//		$log.log("getQuotationDetailsPageData() - called, isPaginationBlocked = "+ Util.isPaginationBlocked(pageKey).toString());		
//		if(!Util.isPaginationBlocked(pageKey))
//		{	
//			$log.log("getQuotationDetailsPageData() - called" );
//			requestParams = ["QuotationNumber",quotation.QuotationNumber,"VersionNumber",quotation.VersionNumber,"Type",quotation.Type];
//			Util.getPageinationData('quotationDetailsUrl',requestParams,page).then(function(data)
//			{
//				if(isNull(data.quotationLineBean))
//				{	
//					paginationObj.setLastPageNo();
//					$scope.dataFound = false;
//					$scope.quotationDetailLoadMsg = Util.translate('CON_NO_MORE_PAGE'); 
//					$log.log("quotationDetailCtrl --  "+ $scope.quotationDetailLoadMsg);
//					
//					
//					
//				}
//				else
//				{
//					$log.log("quotationDetailCtrl messageCode -- "+data.messageCode);
//					//$rootScope.reLogin(data.messageCode);					
//					$scope.dataFound = true;
//					$scope.quotationDetailLoadMsg = "";						
//					$scope.quotationLineData = data.quotationLineBean;
//					requestParams = ["QuotationNumber",quotation.QuotationNumber,"VersionNumber",quotation.VersionNumber,"Type",quotation.Type];
//					$scope.$root.$eval();					
//				}			
//				$log.log("paginationObj.isLastPageNo(): " +paginationObj.isLastPageNo());
//			});
//		}
//	};
//
//	$scope.previousPage = function(){		
//		
//		$scope.getQuotationDetailsPageData(PrevPage);		
//	};
//	$scope.nextPage = function(){			
//		$scope.getQuotationDetailsPageData(NextPage);		
//	};
//			
//	$scope.getFirstPage = function()
//	{			
//		$scope.getQuotationDetailsPageData(FirstPage);
//				
//	};
//	//  Pagination code ends

	$scope.getProperty = function(property)
	{  
		var data = $scope.quotationDetailProperty;
		return data[property];
	};

	$scope.isListEmpty = function(list)
	{
		var isEmpty = false;	
		if( list == null)
		{ 	isEmpty = true;
			$scope.resultMsg = "No information found...";
		}
		return isEmpty;
	};

	$scope.isCollapsed = false;
	$scope.toggleCollapse = function()
	{
		var isCollapsed = !$scope.isCollapsed;
		return isCollapsed;
	};	

	$scope.showQuotationLineDetail = function(quotationLine){
		//DataSharingService.setObject("quotationLine_clickedDetail",quotationLine);
		if($rootScope.isShowDisableMenuItem('CON_QUOTATION_LINE_INFORMATION')){
			DataSharingService.setToSessionStorage("quotationLine_clickedDetail",quotationLine);
			var needToLogin = $rootScope.isSignOnRequired('CON_QUOTATION_LINE_INFORMATION','home.quotationline',true);
			if(!needToLogin){
				$state.go('home.quotationline');
			}
		}
	};

	$scope.isColExist = function(colPropName,propertyList)
	{
		var isCol = false;
		var val = Util.isPropertyExist(colPropName,propertyList);
		if(val){
			isCol = true;
		}
		return isCol;
	};
	
	/* Buy From Quotation */
	$scope.initBuyObject = function(list){
		$scope.buyObj = new Buy(list);
		$scope.buyObj.setPropNameItemCode('itemCode');
		$scope.buyObj.setPropNameOrdered('quantity1');
		$scope.buyObj.setPropNameUnit('unit');
		$scope.buyObj.setPropNameisBuyAllowed('buyAllowed');
		$scope.buyObj.setpropNameQuotationPrice('price');
	};
	
	$scope.buyItems = function(item,buyObj){
		if($rootScope.webSettings.isUseQuotationPrice){
			$rootScope.buyMultipleItemsQuo(item,$scope.buyObj);
		}else{
			$rootScope.buyMultipleItems(item,$scope.buyObj);
		}
		
	};
	
	$scope.addItem = function(item)
    {
    	$rootScope.addItemToCurrentList(item);    	
    };
	
	$scope.addMultipleItemsToCurrentList = function(item){
		$rootScope.addMultipleItemsToList(item,$scope.buyObj);
	};
	
	$scope.goThankYou= function(quotationNo, versionNo){
		var needToLogin = $rootScope.isSignOnRequired("CON_ORDER_RECEIVED_CONFIRMATION",null,false);
		if(!needToLogin){
			var requestParams = ["quotationNo",quotationNo,"versionNo",versionNo];
			var msg = Util.translate('CON_CONFIRM_QUOTATION_TO_ORDER',null);
			var mode = $rootScope.openConfirmDlg(msg , "");
			mode.result.then(function () {
				QuotationService.convertQuotationToOrder(requestParams).then(function(data) {
					 if(!isNull(data)) {
						 $scope.thankYouObj = new ThankYouPage(data);
						 DataSharingService.setToSessionStorage("thankYouObj",$scope.thankYouObj);	
						 if($scope.thankYouObj.isOrderPlaced){
							 $rootScope.setBubbleCartToZero();
							 DataSharingService.setToSessionStorage("shoppingCart_orderNumber",$scope.thankYouObj.orderNumber);
							 //{PAY_MODE}/{MESSAGE_CODE}/{ORDER_NUM}
							 $state.go('home.goThankYou',{"PAY_MODE":"INVOICE","MESSAGE_CODE":"NA","ORDER_NUM":$scope.thankYouObj.orderNumber});
						 }else{
							 $scope.quotationMsg= $scope.thankYouObj.orderErrorMsg;
						 }
					 }
				 });
			}, function () {
		
			});
		}				
	};
	
//	$scope.goThankYou= function(quotationNo, versionNo){
//		var needToLogin = $rootScope.isSignOnRequired("CON_ORDER_RECEIVED_CONFIRMATION",null,false);
//		var isOrderPlaced = false;
//		if(!needToLogin){
//			var requestParams = ["quotationNo",quotationNo,"versionNo",versionNo];
//			var msg = Util.translate('CON_CONFIRM_QUOTATION_TO_ORDER',null);
//			var mode = $rootScope.openConfirmDlg(msg , "");
//			mode.result.then(function () {
//				QuotationService.convertQuotationToOrder(requestParams).then(function(data) {
//					 if(Util.isNotNull(data)) {
//						$scope.orderNumber = data.salesOrderNum;				
//						 if(!isNull(data.salesOrderNum)) {
//							 	$rootScope.cartStatus = "success";	
//							 	isOrderPlaced = true;
//							 	$rootScope.cartMsg =  Util.translate('MSG_ORDER_RECEIVED_ASSIGNED_ORDER_NUMBER',[ $scope.orderNumber]);
//								DataSharingService.setToSessionStorage("shoppingCart_orderNumber",$scope.orderNumber);
//								$state.go('home.goThankYou');
//						 } else {
//							 	isOrderPlaced = false;
//							    $scope.quotationMsg=Util.translate('MSG_QUOTATION_ALREADY_COPIED_ORDER');
//						 }
//					 }
//					 DataSharingService.setToSessionStorage("isOrderPlaced",isOrderPlaced);
//					 DataSharingService.setToSessionStorage("cartMsg",$rootScope.cartMsg);
//					 DataSharingService.setToSessionStorage("cartStatus",$rootScope.cartStatus);
//					
//					 
//				 });
//			}, function () {
//		
//			});
//		}				
//	};
	
	$scope.exportQuotationLineTableAsPDF = function(){
		
		var quotationLines = getQuotationLinesForExport($scope.quotationDetails);
		var basicInfo = getBasicInfoForExport($scope.quotationDetails);
		var quotationReferences = getQuotationReferencesForExport($scope.quotationDetails);
		var orderText,orderFees,addresses;
		
		var fileName = "QuotationDetail_"+$scope.quotationDetails.quotationNumber+"_"+getCurrentDateTimeStr()+".pdf";
		var docHeader = Util.translate('CON_QUOTATION_NUMBER') + " " + $scope.quotationDetails.quotationNumber;
		saveDetailsPDF(quotationLines,basicInfo,orderText,quotationReferences,orderFees,addresses,docHeader,fileName);
	};
	
	var getBasicInfoForExport = function(quotationDetails){
		var basicInfo = new Table();
		basicInfo.heading = Util.translate('CON_QUOTATION_HEADER_INFORMATION') ; 
		var row = [];
		row.push(Util.translate('COH_CUSTOMER'));
		row.push(quotationDetails.customerCode);
		row.push(quotationDetails.customerDesc);
		row.push("");
		row.push("");
		basicInfo.addRow(row);
		
		row = [];
		row.push(Util.translate('COH_CURRENCY'));
		row.push(quotationDetails.currency);
		basicInfo.addRow(row);
		
		row = [];
		if(!isNull(quotationDetails.orderValueExclVAT)){
			row.push(Util.translate('CON_ORDER_VALUE_EXCLUDING_VAT'));
			row.push(quotationDetails.orderValueExclVAT);
		}
		row.push("");
		
		if(!isNull(quotationDetails.orderValueInclVAT)){
			row.push(Util.translate('CON_ORDER_VALUE_INCLUDING_VAT'));
			row.push(quotationDetails.orderValueInclVAT);
		}
		basicInfo.addRow(row);		

		if(!isNull(quotationDetails.motCode)){
			row = [];
			row.push(Util.translate('CON_MANNER_OF_TRANSPORT'));
			row.push(quotationDetails.motCode);
			row.push(quotationDetails.motDesc);
			row.push("");
			row.push("");
			basicInfo.addRow(row);
		}
		if(!isNull(quotationDetails.todCode)){
			row = [];
			row.push(Util.translate('CON_TERMS_OF_DELIVERY'));
			row.push(quotationDetails.todCode);
			row.push(quotationDetails.todDesc);
			row.push("");
			row.push("");
			basicInfo.addRow(row);
		}
		if(!isNull(quotationDetails.topCode)){
			row = [];
			row.push(Util.translate('CON_TERMS_OF_PAYMENT'));
			row.push(quotationDetails.topCode);
			row.push(quotationDetails.topDesc);
			row.push("");
			row.push("");
			basicInfo.addRow(row);
		}
		
		row = [];
		row.push(Util.translate('COH_EXPIRE_DATE'));
		row.push(quotationDetails.dueDate);
		
		basicInfo.addRow(row);

		return basicInfo;
	};


	var getQuotationLinesForExport = function(quotationDetails){
		var quotationLines = new Table();
		quotationLines.heading = Util.translate('CON_QUOTATION');
		
		quotationLines.addHeader(Util.translate('COH_LINE'));
		quotationLines.addHeader(Util.translate('COH_PRODUCT'));
		if(!isNull($scope.quotationLineData[0]) && $scope.quotationLineData[0].descFlag){
			quotationLines.addHeader(Util.translate('COH_DESCRIPTION'));
		}
		quotationLines.addHeader(Util.translate('COH_UNIT'));
		quotationLines.addHeader(Util.translate('COH_QUANTITY'));
		quotationLines.addHeader(Util.translate('CON_PRICE'));
		quotationLines.addHeader(Util.translate('COH_AMOUNT'));
		quotationLines.addHeader(Util.translate('COH_BACKLOG'));
		quotationLines.addHeader(Util.translate('COH_DISPATCH_DATE'));
		

		if(!isNull($scope.quotationLineData)){
			for(var i=0;i< $scope.quotationLineData.length;i++){
				var row = $scope.quotationLineData[i];
				var colData = [];
				colData.push(row.lineNumber);
				colData.push(row.itemCode);
				if($scope.quotationLineData[0].descFlag){
					colData.push(row.itemDescription);
				}
				colData.push(row.unit);
				colData.push(row.quantity);
				colData.push(row.price + row.pricingUnit.trim());
				colData.push(row.amount);
				colData.push(row.backlog);
				colData.push(row.deliveryDate);
			
				quotationLines.addRow(colData);
			}
		}
		else{
			var colData = [];
			
				for(var i=0;i<quotationLines.headers.length;i++){
					colData.push("");
				}
				quotationLines.addRow(colData);
		}
		
		return quotationLines;
	};	
	
	
	
	var getQuotationReferencesForExport = function(quotationDetails){
		var quotationReferences = new Table();

		quotationReferences.heading = Util.translate('CON_QUOTATION_REFERENCES') ; 

		var row = [];
		
		row.push(Util.translate('CON_HANDLER'));
		row.push(quotationDetails.handlerCode);
		row.push(quotationDetails.handlerDesc);
		quotationReferences.addRow(row);
		
		row = [];
		row.push(Util.translate('CON_SALES_PERSON'));
		row.push(quotationDetails.salespersonCode);
		row.push(quotationDetails.salespersonDesc);
		quotationReferences.addRow(row);

		return quotationReferences;
	};

	
	
	quotationDetailCtrlInit();
}]);