catalougeControllers.controller('shoppingListDetailsCtrl', ['$rootScope','$scope','$stateParams', '$http','$log', '$q','DataSharingService','ShoppingListService', 'Util','$filter','$state',
                                                            function ($rootScope,$scope,$stateParams, $http,$log, $q,DataSharingService,ShoppingListService,Util,$filter,$state) {
     	var shoppingListDetailsCtrlInit = function(){
     		if(historyManager.isBackAction){
     			historyManager.loadScopeWithCache($scope);
     			$scope.listUpdated=false;
     			$scope.$root.$eval();
     		}else{
     			initController();
     		}
     	};
     	
     	$scope.$on('$destroy', function(event){ 
     		historyManager.pushScopeObject($scope);
     		//$log.log(" shoppingListDetailsCtrl history manager " + JSON.stringify(historyManager.stateCacheList));
     	});
     	
     	var initController = function()
     	{ 
     		$rootScope.initCommitsSecondNextPromise.then(function(){     			
     			$scope.filterOn = false;
     	   		$scope.itemSelected=false;
     			$scope.sortColumn = "ListId";
     			$scope.orderType = "ASC";
     			$rootScope.sortSelection = '';
     			$scope.validDateShoppinglist=true;
//     			$scope.requestParams = 	$rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn); 

//     			$rootScope.sortSelection =  $scope.sortColumn+$scope.orderType;		
//     			$scope.pageKey = Util.initPagination('getShoppingListDetailsUrl',$scope.requestParams);
//     			$scope.paginationObj = DataSharingService.getObject($scope.pageKey );	

     			$scope.shoppingListDetails =  new MyShoppingListDetails(Util.translate); 
     			var listObject = DataSharingService.getFromSessionStorage("listInfo");
     			if(listObject==null){$rootScope.isShoppingListSignOnRequiredOpen();}
     			$scope.ListId = listObject.listId;
     			$rootScope.currentList=listObject.listId;
     			$scope.params = ["ListId",listObject.listId ,"ListOwnerCode",listObject.listOwner];	
     			$scope.requestParams = ["ListId",listObject.listId ,"ListOwnerCode",listObject.listOwner];
     			$scope.pageKey = Util.initPagination('getShoppingListDetailsUrl',$scope.requestParams);
     			$scope.paginationObj = DataSharingService.getObject($scope.pageKey );	
     			getShoppingListDetailsData($scope.params,$scope.filterOn);
     			$scope.listUpdated=false;
     		});
     		};
     		
     		$scope.$watch('addToListWatch',function() {
     			if($scope.listUpdated){
         			getShoppingListDetailsData($scope.params,$scope.filterOn);
     			}
     			$scope.listUpdated=true;
     		});
     		
     	var getShoppingListDetailsData = function(requestParam,isFilterOn,fromAddItems){
     		if(!isNull(fromAddItems)){
     			if(fromAddItems == true){
     			$scope.pageKey = Util.initPagination('getShoppingListDetailsUrl',$scope.requestParams);
     			$scope.paginationObj = DataSharingService.getObject($scope.pageKey );	
     			}
     		}
     		ShoppingListService.getShoppingListDetails(requestParam).then(function(data){ 
     			$scope.shoppingListDetails.setListData(data,isFilterOn);
     			if(isNull(data) || (!isNull(data) && data.messageCode==111)) 
     			{						
     				$scope.paginationObj.setLastPageNo();				
     				$scope.dataFound = false;
     				$scope.itemSelected=false;
     			}
     			else
     				{
     				$scope.dataFound = true;
     				addpropertyToList(data.listOfLine,'validQuantity',true);
	 				// ADM-010 Begin
					if ($rootScope.webSettings.allowSelectUnitOfMeasure) {
						addpropertyToList(
								data.listOfLine, 'selectedUnitObject', 
								{
									"salesUnit" : $scope.freeSearchObj.packPiece,
									"salesUnitDesc" : $scope.freeSearchObj.packPiece
								});
					}
					//ADM-010 End
        			 // to be used after final web service deployed			
//        			$scope.initBuyObject(data.listOfLine);
        			$scope.checkSelectedItems(data.listOfLine);
//     				$scope.sortColumn = data.sortBy;  // to be used after final web service deployed
//     				$scope.orderType = data.orderBy;  // to be used after final web service deployed
     				}
     			
     			DataSharingService.setObject("SHOPPING_LIST_LINES_DATA", data.listOfLine );
     			$scope.$root.$eval();
     			$rootScope.scrollToId("result_set");

     		});
     	};
     	
     	
     	$scope.validateDateRangeFormatShoppingList = function(dateStr,secondMsg)	{
    		////console.log(filter);
    		
    		var dateFormat = $rootScope.webSettings.defaultDateFormat;
    		//var isFilterTextEmpty = isFilterRangeEmpty(filter);
    		this.validDateShoppinglist = true;
    		if(isNull(secondMsg)){
    			secondMsg = true;}
    		
    		if(Util.isNotNull(dateStr)){
//    			var isValidDate = isValidDateFormat(dateStr,dateFormat);
    			var dateValidator =new DateValidator(dateFormat);			
    			var isValidDate =dateValidator.isValidDate(dateStr);
    			if(!isValidDate){
    				this.validDateShoppinglist = false;	
    					
    			}else{
    				this.validDateShoppinglist = true;
    			}
    			if(dateStr==""||dateStr==null){
    				this.validDateShoppinglist = true;
    			}
    			
    		}
    		
    	};
     	
    	$scope.exportShoppingListLinesData = function(fileType)
    	{
    		$scope.fileTypeForExport = fileType;
    		// going to bring data
    		$scope.pageName = FirstPage;
    		$scope.dataReceived = [];
    		$scope.getDataForExport($scope.pageName);
    	};
    	
    	/*
    	 * will start export once whole data is received to export
    	 */
    	$scope.startExport = function() {
    		var shoppingListLinesTable = new Table();
    		var data;
    		var filePrefix = "ShoppingListEntry";
    		var title = Util.translate('CON_SHOPPING_LIST_ENTRY');
    		var sheetName = title ;
    		switch($scope.fileTypeForExport){

    		case 'pdf' :
    			shoppingListLinesTable = getShoppingListLinesDataForExport();
    			data = shoppingListLinesTable.getHTML();												
    			var fileName = $rootScope.getExportFileName (filePrefix,".pdf");
    			savePdfFromHtml(data,title,fileName);
    			break;

    		case 'xls' :
    			shoppingListLinesTable = getShoppingListLinesDataForExport();
    			var data = shoppingListLinesTable.getDataWithHeaders();
    			var fileName = $rootScope.getExportFileName (filePrefix,".xlsx");						
    			saveExcel(data,sheetName,fileName);
    			break;
    		case 'xml' :
    			shoppingListLinesTable = getShoppingListLinesDataForExport('XML');
    			shoppingListLinesTable.setRootElementName("CON_SHOPPING_LIST_ENTRY");
    			shoppingListLinesTable.setEntityName("SHOPPING_LIST_DETAIL");
    			var data = shoppingListLinesTable.getXML();
    			var fileName = $rootScope.getExportFileName (filePrefix,".xml");						
    			saveXML(data,fileName);
    			break;

    		default :
    			//console.log("Exporting Request Search - failed");
    		}
    	}
    	
    	var getShoppingListLinesDataForExport = function(target){
    		var shoppingListLinesTable = new Table();
    		if(isEqualStrings(target,"XML")){
    			shoppingListLinesTable.addHeader('PRODUCT');
    			shoppingListLinesTable.addHeader('DESCRIPTION');
    		    if($rootScope.webSettings.isShowPrice){
    		    	shoppingListLinesTable.addHeader('ACTUAL_PRICE');
    			}
    			if($rootScope.webSettings.isShowPrice){
    				shoppingListLinesTable.addHeader('DISCOUNT_PRICE');
    			}
    			if($rootScope.webSettings.isShowPrice){
    				shoppingListLinesTable.addHeader('DISCOUNT_PERCENTAGE');
    			}
    			shoppingListLinesTable.addHeader('QUANTITY');
    			shoppingListLinesTable.addHeader('UNIT');
//    			shoppingListLinesTable.addHeader('REQUESTED_DELIVERY_DATE');
    			
    		}else{
    			shoppingListLinesTable.addHeader(Util.translate('COH_ITEM'));
    			shoppingListLinesTable.addHeader(Util.translate('COH_DESCRIPTION'));
    			if($rootScope.webSettings.isShowPrice){
    				shoppingListLinesTable.addHeader(Util.translate('COH_ACTUAL_PRICE'));
    				shoppingListLinesTable.addHeader(Util.translate('COH_DISCOUNT_PRICE'));
    				shoppingListLinesTable.addHeader(Util.translate('CON_DISCOUNT_%'));
    			}
    			shoppingListLinesTable.addHeader(Util.translate('COH_QUANTITY'));
    			shoppingListLinesTable.addHeader(Util.translate('COH_UNIT'));
//    			shoppingListLinesTable.addHeader(Util.translate('COH_REQUESTED_DELIVERY_DATE'));
    		}
    		data = $scope.dataReceived;	
    		for(var i=0;i<data.length;i++){
    			var item = data[i];
    			var row = [];
    			row.push(item.code);
    			row.push(item.description);
    			if($rootScope.webSettings.isShowPrice){
    				row.push(item.actualPrice);
    				row.push(item.discountPrice);
    				row.push(item.discountPercentage);
    			}
    			row.push(item.quantity);
    			row.push(item.defaultSalesUnit);
//    			row.push("");
    			shoppingListLinesTable.addRow(row);
    		}
    		return shoppingListLinesTable;
    	};
    	
    	$scope.getDataForExport = function() {
    		var listObject = DataSharingService.getFromSessionStorage("listInfo");   			
 			requestParams = ["ListId",listObject.listId ,"ListOwnerCode",listObject.listOwner];
    		Util.getPageinationData('getShoppingListDetailsUrl',requestParams,$scope.pageName).then(function(data) {
    			if(!isNull(data) && !isNull(data.listOfLine)) {
    				$scope.dataReceived = $scope.dataReceived.concat(data.listOfLine);
         			if(data.moreRecords) {
         				$scope.pageName = NextPage;
         				$scope.getDataForExport();
         			} else {
         				$scope.startExport();
         			}
    			}
     		});
    	};
    	
    	$scope.changeFormatToLocale = function(value){
    		var newVal = parseFloat(value);
    		if(!isNull($rootScope.webSettings.localeCode) && $rootScope.webSettings.localeCode !=""){
    			if(!isNull($rootScope.webSettings.allowDecimalsForQuantities) && $rootScope.webSettings.allowDecimalsForQuantities == true){
    			var langCode = $rootScope.webSettings.localeCode;
    			langCode = langCode.replace("_","-");
    			newVal = newVal.toLocaleString(langCode);
    		}}
    		return newVal;
    	};
    	$scope.toggleSelectAll = function(flag){
    		var list = $scope.shoppingListDetails.listData.listOfLine;
    		for(var i=0;i<list.length ;i++){
    			var item = list[i];
    			item.checked = flag;
    		}
    		$scope.checkSelectedItems(list);    		
    	};
    	
    	$scope.initBuyObject = function(list){
    		$scope.buyObj = new Buy(list);
    		
    		$scope.buyObj.setPropNameItemCode('code');
    		$scope.buyObj.setPropNameOrdered('quantity');
    		$scope.buyObj.setPropNameUnit('salesUnitCode');
    		$scope.buyObj.setPropNameisBuyAllowed('isBuyingAllowed');
    		$scope.buyObj.setPropNameShipmentMarking('shipmentMark');
    		$scope.buyObj.setPropNameDelDate('deliveryDate');
    	};
    	
    	$scope.buyListItems = function(list){
    		var listlines=DataSharingService.getObject("SHOPPING_LIST_LINES_DATA");
    		var selectedLines =$filter('filter')(list, {checked: true});
    		
    		if(selectedLines.length>0){
    			for(var i=0;i<selectedLines.length ;i++){
    				var item=selectedLines[i];
    				selectedLines[i].salesUnitCode = item.selectedUnitObject.salesUnit;
    				/*FIX*/item.quantity = item.quantity + "";
    				if(isEmptyString(item.quantity)){
    					item.validQuantity=false;
    					item['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
    				}
    			}
    			$scope.initBuyObject(selectedLines);
    			$rootScope.buyMultipleItems(null,$scope.buyObj);
    		}
    	};
    	
    	$scope.deleteShoppingListLines = function(list){
    		$scope.selectedShoppingListLines =getSelectedShoppingListLines(list);
			var msg = Util.translate('CON_DELETE_SHOPPING_LIST_LINE',null);
			var mode = $rootScope.openConfirmDlg(msg);
			mode.result.then(function () {
				//console.log('Modal Ok clicked');	
				var params = "DeleteShoppingListLines="+JSON.stringify($scope.selectedShoppingListLines);
				ShoppingListService.deleteShoppingListLines(params).then(function(data){
					if(!isNull(data)){						
						$scope.selectAll=false;
//						$scope.shoppingListDetails.updateMsg(data);
						handleDeleteMsgCode(data);
					}
				});		
			}, function () {
				//console.log('Modal dismissed');
			});
	   };

    	$scope.saveShoppingListLines = function(data){
    		var listObject =getParams(data);
		   var params = "SaveShoppingListLine="+JSON.stringify(listObject);
		   ShoppingListService.saveShoppingListLines(params).then(function(data){
			   if(!isNull(data)){						
				   handlemessageCodeforUpdate(data);
			   }
		   });	
	   };
	   
	   var handlemessageCodeforUpdate = function(data){
		   $scope.updateMessage = "";
		   $scope.showUpdatedMessage = true;
		   for(var i=0 ; i<data.length ; i++){
				var item = data[i];
				if(item.messageCode == 2269){
					var msg = Util.translate('MSG_SHOPPING_LIST_IS_SAVED');
					$scope.updateMessage = msg;
					$scope.listUpdateSuccess = true;
				}else{
					$scope.updateMessage = "";
					$scope.showUpdatedMessage = false;
				}
				/*else{
					var msg = Util.translate('MSG_ERROR_OCCURED_WHEN_SAVING_SHOPPING_LIST');
					$scope.updateMessage = msg;
					$scope.listUpdateSuccess = false;
				}*/
			}
		   setTimeout(function(){
			   $scope.updateMessage = "";
			   $scope.showUpdatedMessage = false;
		   },4000);
		};
        
		var getParams = function(data){
			var selectedList=[];
			var listid=data.listId;
			for(var i=0;i<data.listOfLine.length ;i++){
				var item = data.listOfLine[i];
				if(data.listOfLine[i].validQuantity && !isEmptyString(item.quantity)){
				var paramObject = new Object();
				paramObject.listId= listid;
				paramObject.code = encodeURIComponent(item.code);
				paramObject.salesUnitCode=item.selectedUnitObject.salesUnit;
				paramObject.quantity = item.quantity;
				if($rootScope.webSettings.allowDecimalsForQuantities == true){
					paramObject.quantity = paramObject.quantity.replace(",",".");
				}
				paramObject.lineNumber=item.lineNumber;
				if(item.deliveryDate !== undefined){
					paramObject.deliveryDate = item.deliveryDate;
				}else{
					paramObject.deliveryDate = "";
				}
				selectedList.push(paramObject);
				}else if(isEmptyString(item.quantity)){
					item.validQuantity=false;
					item['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');		
				}
			}
			return selectedList;
		};
	   
		var handleDeleteMsgCode = function(msgList){
			var errorMsgList = [];
			var anyListLineGotDeleted = false ;
			for(var i=0 ; i<msgList.delShopLineBeanList.length ; i++){
				var item = msgList.delShopLineBeanList[i];
				if(item.messageCode == 2262){
					var msg = Util.translate('MSG_ERROR_IN_DELETING_LIST_LINE');
					errorMsgList.push(msg);
				}else if(item.messageCode == 2261){
					anyListLineGotDeleted = true;
				}else if(item.messageCode == 2265){
					anyListLineGotDeleted = false;
					var msg = Util.translate('MSG_NO_LINES_HAVE_DELETED_FROM_LIST');
					errorMsgList.push(msg);
				}
			}
			showErrorMsg(errorMsgList);
			if(anyListLineGotDeleted){
				getShoppingListDetailsData($scope.params,$scope.filterOn);
			}
		};
		
		var showErrorMsg = function(errorMsgList){
			if(errorMsgList.length > 0){
				var msg = "";
				var dlg = $rootScope.openMsgDlg(msg,errorMsgList);
			}
		};
		var getSelectedShoppingListLines = function(list){
			var selectedList=[];
			
			var selectedShoppingListLines =$filter('filter')(list, {checked: true});
			for(var i=0;i<selectedShoppingListLines.length ;i++){
				item = selectedShoppingListLines[i];
				var paramObject = new Object();
				var myStr = item.code;
				myStr = myStr.replace(/"/g, '\\"');
				paramObject.code = encodeURIComponent(myStr);
				paramObject.quantity = item.quantity;
				selectedList.push(paramObject);
			}
			return selectedList;
		};
		
     	$scope.getShoppingListEntryPageData = function(page)
    	{
    		var pageKey = $scope.pageKey;
    		if(!Util.isPaginationBlocked(pageKey)){	
//    		var	requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn);
    			var listObject = DataSharingService.getFromSessionStorage("listInfo");   			
     			requestParams = ["ListId",listObject.listId ,"ListOwnerCode",listObject.listOwner];		
    			Util.getPageinationData('getShoppingListDetailsUrl',requestParams,page ).then(function(data)
    			{
    				if(isNull(data) || data.messageCode ==111){						
    					$scope.paginationObj.setLastPageNo();
    					$scope.dataFound = false;					
    					$scope.pageLoadMsg = Util.translate('CON_NO_MORE_PAGE'); 
    				}else{					
    					$scope.dataFound = true;	
    					$scope.pageLoadMsg = "";
//    					$scope.sortColumn = data.sortBy;  
//    					$scope.orderType = data.orderBy;  
    					//requestParams = $rootScope.getParamKeyList($scope.filterOn,$scope.filterParam,$scope.orderType,$scope.sortColumn);
    					addpropertyToList(data.listOfLine,'validQuantity',true);
    					var listObject = DataSharingService.getFromSessionStorage("listInfo");   			
    	     		    requestParams = ["ListId",listObject.listId ,"ListOwnerCode",listObject.listOwner];	
    					$scope.$root.$eval();					
    				}	
    				
    				$scope.shoppingListDetails.setListData(data,$scope.filterOn,true);
    	    		$scope.selectAll=false;
    			});
    		}
    	};
    	
    	var checkIfPageHasDefaultListRecord = function(pageList)
    	{
    		var defaultListRecordFound = false;
    		if(!isNull(pageList))
    		{
    			for(var i = 0; i<pageList.length ; i++){
    				
    				if(pageList[i].listId == $rootScope.webSettings.defaultShoppingListId )
    					{
    						defaultListRecordFound = true;
    						break;
    					}

    			}
    		}

    		return defaultListRecordFound;
    	};
       $scope.previousPage = function(){		
    		$scope.getShoppingListEntryPageData(PrevPage);		
    	};
    	
    	$scope.nextPage = function(){			
    		$scope.getShoppingListEntryPageData(NextPage);		
    	};
    			 
    	$scope.getFirstPage = function(){			
    		$scope.getShoppingListEntryPageData(FirstPage);
    				
    	};
    	
    	$scope.goToSUbmit = function(list){
    		if($rootScope.isShowDisableMenuItem('CON_SUBMIT_REQUEST')){
	     		var selectedLines =$filter('filter')(list, {checked: true});
	    		
	    		if(selectedLines.length>0){
	    			for(var i=0;i<selectedLines.length ;i++){
	    				var item=selectedLines[i];
	    				selectedLines[i].salesUnitCode = item.selectedUnitObject.salesUnit;
	    				if(isEmptyString(item.quantity)){
	    					item.validQuantity=false;
	    					item['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
	    				}
	    			}
	    		}
	    		$scope.listOfRequest = [];
	    		if(selectedLines.length>0){
	    			for(var j=0;j<selectedLines.length;j++){
	    				var item = selectedLines[j];
	    				if(item.quantityBoxMsg == undefined || item.quantityBoxMsg == ""){
	    					$scope.listOfRequest.push(item);
	    				}
	    			}
	    		}
	    		if(($scope.listOfRequest.length>0)){
	    		//var selectedLines =$filter('filter')(list, {checked: true});
	    		$rootScope.listOfRequest = $scope.listOfRequest;
	    		var callState = REQ_SUBMIT_SHOPPING_LIST_VIEW;
	    		$rootScope.reqTemplate.checked = "product";
	    		$rootScope.reqTemplate.fromShoppingList = true;
	    		var stateParameter = {};
	    		$state.go(callState,stateParameter);
	    		}
    		}
    	};
    	
    	$scope.checkSelectedItems= function(list){			
    		var count=0;
    		var buyableItemsCount = 0;
    		for(var i=0; i < list.length; i++) {
    			if(list[i].isBuyingAllowed) {
    				buyableItemsCount++;
    			}
    		}
			var selectedShoppingListLines =$filter('filter')(list, {checked: true});
			for(var i=0;i<selectedShoppingListLines.length ;i++){
				count++ ;
			}
			if(selectedShoppingListLines.length<buyableItemsCount ){
				$scope.selectAll=false;
			}else if(selectedShoppingListLines.length == buyableItemsCount){
				$scope.selectAll = true;
			}	
			if(selectedShoppingListLines.length==0){
				$scope.selectAll=false;
			}
    	    if(count>0){
    	   		$scope.itemSelected=true;
    	    }else{
    	    	$scope.itemSelected=false;
    	    }
    	};
    	
    	$scope.showAddProductTable = function()
    	{
    		if(!$rootScope.isShowDisableMenuItem('CON_ADD_PRODUCTS_TO_YOUR_CART')){
    			return;
    		}
    		$scope.isCollapsed = !$scope.isCollapsed;
    		$scope.copyOrder =  new MultipleAdd(null,$rootScope,Util.translate,$rootScope.CopyOrderDelimeter);	
    		$scope.copyOrder.clearAll();
    	};
    	
    	$scope.addItemsToList = function(obj)
    	{
    		var itemsList = obj.getLineList();
    		var items=getItems(itemsList);
    		var isValidProduct=true;
    		var noProductEntered= true;
    		for(var i=0;i<itemsList.length;i++){
    			if((itemsList[i].ordered == null)&&(itemsList[i].itemCode != null)){
    				itemsList[i].validQuantity = false;
    			}
    			if(!itemsList[i].validQuantity){
    				 isValidProduct=false;
    				 addUIError(itemsList[i],Util.translate,'MSG_INVALID_QUANTITY');
//    				 itemsList[i]['errorMsg']=Util.translate('MSG_INVALID_QUANTITY');
    			}
    			if(!isNull(itemsList[i].itemCode)){
    				noProductEntered=false;
    			}
    		}
    		if(noProductEntered){
    			obj.UIErrorKey = 'MSG_ITEM_NOT_SPECIFIED'; 
    		}
    		if(isValidProduct && !noProductEntered){
    			var params = "AddItemsToListDetails="+JSON.stringify(items);
    			ShoppingListService.addItemToShoppingList(params).then(function(data){
    				if(!isNull(data)){		
    					var anyItemAddedToList = $rootScope.handleAddItemMsgCode(data,$scope.copyOrder);
    					if(anyItemAddedToList){
    						var fromAddItems = true;
    						getShoppingListDetailsData($scope.params,$scope.filterOn,fromAddItems);
    					}
    				}
    			});			
    		}
    	};
    	
//		var handleAddItemMsgCode = function(msgList){
//			var errorMsgList = [];
//			var anyItemAddedToList = false ;
//			var countForClear=0;
//			for(var i=0 ; i<msgList.addListBean.length ; i++){
//				var item = msgList.addListBean[i];				
//				if(item.messageCode == "2256"){
//					var msg = Util.translate('MSG_LINE_IN_ERROR_ADDING_ITEM_TO_LIST',[item.itemCode]);
//					errorMsgList.push(msg);
//				}else if(item.messageCode == "2258"){
//					anyItemAddedToList = true;
//					countForClear=countForClear + 1;
//				}else if(item.messageCode == "2267"){
//					anyItemAddedToList = false;
//					var msg = Util.translate('MSG_NO_LINES_HAVE_ADDED_TO_LIST');
//					errorMsgList.push(msg);
//				}
//			}
//			if(msgList.addListBean.length==countForClear){
//				$scope.copyOrder.clearAll();
//			}
//			showErrorMsg(errorMsgList);
//			if(anyItemAddedToList){
//				getShoppingListDetailsData($scope.params,$scope.filterOn);
//			}
//		};
		var getItems = function(itemsList){
			var items=[];
			for(var i=0;i<itemsList.length ;i++){
				var item = itemsList[i];
				if(item.itemCode !=null && item.unit!=null && item.ordered!=null){
					var paramObject = new Object();
					paramObject.itemCode = encodeURIComponent(item.itemCode);
					paramObject.unitCode = item.unit;
					paramObject.quantity = item.ordered;
					if($rootScope.webSettings.allowDecimalsForQuantities == true){
						paramObject.quantity = paramObject.quantity.replace(",",".");
					}
					if(!isNull(item.shipmentMarking)){
						paramObject.shipmentMarking =encodeURIComponent(item.shipmentMarking);
					}					
					items.push(paramObject);
				}				
			}
			return items;
		};

		// ADM-010
		$scope.$watch('freeSearchObj.packPiece',
			function(){
				refreshUIforPackPiece();
			}
		);
		
		// ADM-010
		function refreshUIforPackPiece(){
			if($rootScope.webSettings.allowSelectUnitOfMeasure){
				var listLines = DataSharingService.getObject("SHOPPING_LIST_LINES_DATA");
				if (Util.isNotNull(listLines)) {
//					var orderLinesNumbers = Object.keys($scope.orderLines);
//					for ( var i = 0; i < orderLinesNumbers.length; i++) {
//						orderLine = $scope.orderLines[orderLinesNumbers[i]];
//						$rootScope.updatePackPieceFromShoppingCart(
//								$scope.freeSearchObj.packPiece, orderLine);
//					}
//					updateQuantityTextBox($scope.orderLines);
				}
			}
		}
		shoppingListDetailsCtrlInit();
     }]);