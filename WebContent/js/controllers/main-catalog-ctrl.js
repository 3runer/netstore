catalougeControllers.controller('mainCatalogCtrl', ['NewConfigService','MainCatalogService','ShoppingCartService','$rootScope','$scope', '$stateParams','$log', '$state','$filter','DataSharingService','Util',
                                                    function (NewConfigService, MainCatalogService,ShoppingCartService,$rootScope,$scope, $stateParams,$log,$state,$filter,DataSharingService,Util) {

	
	var tblViewObj  = {
			"name" :"tableView",
			"lable":""
	};

	var gridViewObj = {
			"name" : "gridView",
			"lable" : ""
	};
	var requestParams = [];
	var pageKey = null;
	
	var mainCatalogCtrlInit = function(){ 
		if(historyManager.isBackAction && !isNull($scope.productList)){
			historyManager.loadScopeWithCache($scope);
			$scope.initBuyObject($scope.productList);
			$scope.$root.$eval();
		}else{
			init();
		}
	};
	$rootScope.showCatalog = function(paramObj){
		initFromCatalogPage(paramObj);
		
	};
	
	var initFromCatalogPage = function(paramsObj){
		
			$scope.sortColumn = "";
			$scope.orderType = "";
			$scope.filterOn = false;
			$scope.filterParam = [];
			$scope.showExtendInfo='false';
			$scope.filterFormInfo = {};
			$scope.filterMsg = "";
			//$scope.currentView = $scope.getView();
			setView();
			$scope.ViewLabelName = $scope.getViewLable();
			$scope.previousView = '';
			$scope.mainCatalogId =paramsObj.CAT_ID;
			$scope.mainCatalogElementId =paramsObj.ELEMENT_ID;
			//$scope.mainCatalogElementDesc =decodeURIComponent(paramsObj.ELEMENT_DESC);
			$rootScope.productfilterDetailSettings = DataSharingService.getProductFilterDetailsSettings();
			
			$scope.mainCataloguePageIndex =1;
			var langCodeMain = NewConfigService.getLanguageCode();
			requestParams = ["PageNo",$scope.mainCataloguePageIndex,"CAT_ID",$scope.mainCatalogId,"ELEMENT_ID",$scope.mainCatalogElementId,"OrderBy",$scope.orderType,"SortBy",$scope.sortColumn,"LanguageCode",langCodeMain];
			$scope.productPropList = null; 
			//collapsed true - opens the fieldset
			$scope.collapsed = true;
			//$scope.filterPanelCollapsed=false;
			pageKey = Util.initPagination('mainCatalogProductListUrl',requestParams);
			$rootScope.sortSelection = '';
			$rootScope.sortSelection =  $scope.sortColumn+$scope.orderType;
			getMainCatalogFilters();
			getDataForFirstPage();
		
		
	};
	
	var init = function(){
		$scope.sortColumn = "";
		$scope.orderType = "";
		$scope.filterOn = false;
		$scope.filterParam = [];
		$scope.showExtendInfo='false';
		$scope.filterFormInfo = {};
		$scope.filterMsg = "";
		//$scope.currentView = $scope.getView();
		setView();
		$scope.ViewLabelName = $scope.getViewLable();
		$scope.previousView = '';
		$scope.mainCatalogId = decodeURIComponent($stateParams.CAT_ID);
		$scope.mainCatalogElementId = decodeURIComponent($stateParams.ELEMENT_ID);
		//$scope.mainCatalogElementDesc = decodeURIComponent($stateParams.ELEMENT_DESC);
		$rootScope.productfilterDetailSettings = DataSharingService.getProductFilterDetailsSettings();
		
		$scope.mainCataloguePageIndex =1;
		var langCodeMainInit = NewConfigService.getLanguageCode();
		requestParams = ["PageNo",$scope.mainCataloguePageIndex,"CAT_ID",$scope.mainCatalogId,"ELEMENT_ID",$scope.mainCatalogElementId,"OrderBy",$scope.orderType,"SortBy",$scope.sortColumn,"LanguageCode",langCodeMainInit];
		$scope.productPropList = null; 
		//collapsed true - opens the fieldset
		$scope.collapsed = true;
		//$scope.filterPanelCollapsed=false;
		pageKey = Util.initPagination('mainCatalogProductListUrl',requestParams);
		$rootScope.sortSelection = '';
		$rootScope.sortSelection =  $scope.sortColumn+$scope.orderType;
		getMainCatalogFilters();
		getDataForFirstPage();
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
		//$log.log("history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	
	$scope.$on('userLoggedIn', function(event){ 
		if($state.current.name=='home.mainCatalog.Compare') {
			$scope.SelectedProductList = DataSharingService.getFromLocalStorage("compareObjectList");
			$scope.updateCount = 0; 
			for(var i = 0; i < $scope.SelectedProductList.length; i++) {
				$rootScope.getProductsDetail($scope.SelectedProductList[i].code, $scope.updateBuy);
			}
		}
	});

	// ADM-010
	$scope.$watch('freeSearchObj.packPiece',
		function(){
			if(!isNull($scope.productList)){
				$rootScope.refreshUIforPackPiece($scope.freeSearchObj.packPiece, 
				$scope.productList);
			}
		}
	);
	
	$scope.updateBuy = function(data) {
		for(var i = 0; i < $scope.SelectedProductList.length; i++) {
			if($scope.SelectedProductList[i].code == data.itemCode) {
				$scope.SelectedProductList[i].isBuyingAllowed = data.isBuyAllowed;
				break;
			}
		}
		$scope.updateCount++;
		if($scope.updateCount >= $scope.SelectedProductList.length) {
			$scope.compare();
		}
	}
	
	$scope.getView = function(){
		var view = DataSharingService.getObject(CONST_mainCatPrevCompareView);
		if(!angular.isDefined(view) || view==null){
			//Default view is grid view
			view = gridViewObj;
		}
		return view;
	};

	$scope.getViewLable = function(){
		if ($scope.currentView.name == tblViewObj.name ){
			gridViewObj.lable = Util.translate('CON_GRID_VIEW');
			return gridViewObj.lable;
		}else{
			tblViewObj.lable = Util.translate('CON_TABLE_VIEW');
			return tblViewObj.lable;
		}
	};

	$scope.addToCart = function (itemCode,quantity,salesUnit){
		if(Util.isNotNull(itemCode) && Util.isNotNull(quantity) && quantity >0 && Util.isNotNull(salesUnit)) {
			var params =  ["itemCode",itemCode,"itemQty",quantity,"unitCode",salesUnit];
			ShoppingCartService.addToCart(params);
		}
	};

	$scope.gridViewBuyClicked = function(cat){
		if(!cat.validQuantity || isNull(cat.quantity) || ""==cat.quantity){
			cat.validQuantity = false;
			cat['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
		}else{
		cat.isBuyingAllowed = false;
		//$rootScope.addItemToCart(cat.code,cat.quantity,$scope.buyItem[$rootScope.getId('salesUnit',cat.code)],cat,"quantity");
		$rootScope.addItemToCart(cat.code,cat.quantity,cat.salesUnit,cat,"quantity");
		}
	};

	$scope.tableViewBuyClicked = function(cat){
		//console.log("buyClicked: Itemcode = " +cat.code + " quantity = " + $scope.buyItem[$rootScope.getId('quantity',cat.code)] + " salesUnit = " + $scope.buyItem[$rootScope.getId('salesUnit',cat.code)]);
		$rootScope.addItemToCart(cat.code,$scope.buyItem[$rootScope.getId('quantity',cat.code)],cat.defaultSalesUnit);
	};

	$scope.addItem = function(item)
    {
//    	item['quantity'] = $scope.buyItem[$rootScope.getId('quantity',item.code)];
    	$rootScope.addItemToCurrentList(item);    	
    };
    
	$scope.initBuyObject = function(list){
		$scope.buyObj = new Buy(list);
		$scope.buyObj.setPropNameItemCode('code');
		$scope.buyObj.setPropNameOrdered('quantity');
		$scope.buyObj.setPropNameUnit('salesUnit');
		$scope.buyObj.setPropNameisBuyAllowed('isBuyingAllowed');
	};
	
	$scope.buyItems = function(item,buyObj){
		$rootScope.buyMultipleItems(item,$scope.buyObj);
	};
	
	$scope.addMultipleItemsToCurrentList = function(item){
		$rootScope.addMultipleItemsToList(item,$scope.buyObj);
	};
	
	if($state.current.name == 'home.mainCatalog.Compare'){
		$scope.SelectedProductList = DataSharingService.getFromLocalStorage("compareObjectList");
		$scope.previousState = DataSharingService.getObject("prevCompareState");	

	}else{
		$scope.SelectedProductList = [];
	}

	var getDataForFirstPage = function(){
		$rootScope.initCommitsPromise.then(function(){
			MainCatalogService.getMainCatalogProducts(requestParams).then(function(data){
				if(data.messageCode == "2001"){
					$scope.filterMsg =  Util.translate('MSG_SELECT_ANOTHER_CATALOG',null);
					showMsgNoObjFound(data.messageCode);
					}else setUIData(data,true);
			});
		});
	};
	
	var setUIData = function(data,isFirstPageData){
		if(data.moreRecords != undefined){
		$scope.isMoreRecords = data.moreRecords;
		if(!hasVerticalScroll()) {
			$rootScope.mainCatalogLoadMore();
		}
		}
		if(isFirstPageData){
			if(!isNull(data.description)){
				$scope.mainCatalogElementDesc = data.description;
			}
			if(data.productList==null || data.productList.length == 0){
				$scope.dataFound = false;
				if(data.messageCode == "2001"){
				$scope.filterMsg =  Util.translate('MSG_SELECT_ANOTHER_CATALOG',null);
				showMsgNoObjFound(data.messageCode);
				}else if(data.messageCode == "3232"){
					$scope.filterMsg =  Util.translate('SP_MISSING_FOR_PRICE_IN_BULK',null);
					showMsgNoObjFound(data.messageCode);
				}else if(data.messageCode == "3233"){
					$scope.filterMsg =  Util.translate('SP_MISSING_FOR_AVAIL_IN_BULK',null);
					showMsgNoObjFound(data.messageCode);
				}
				else{
				$scope.filterMsg =  Util.translate('MSG_NO_OBJECTS_FOR_SELECTION',null);
				showMsgNoObjFound();
				}
				var icon = '<a class="OrderInformation-icon" shref="" title="'+$scope.filterMsg+'"></a>';				
				$scope.filterMsg = icon + $scope.filterMsg;
				$scope.filterMsg = "";
				$scope.productList = DataSharingService.getObject("CATALOG_PRODUCT_LIST" );
			}
			$rootScope.openLoader();
			$scope.mainCatalogue = data;			
			$rootScope.initHomeCatalogTableHeaders();
			$rootScope.setHomeCatalogExtendedData(data.productList);
			$scope.productList = data.productList;
			$rootScope.setHomeCatalogTableHeaders($scope.productList);
			$scope.initBuyObject($scope.productList);
			$scope.recordFound = true;
			DataSharingService.setObject("CATALOG_PRODUCT_LIST",$scope.productList );
			if(!isNull($scope.$root)){
				$scope.$root.$eval();
			}
			$rootScope.closeLoader();
		}else{
			Util.appendItemsToList(data.productList, $scope.productList);
			$rootScope.setHomeCatalogExtendedData(data.productList);
			$rootScope.setHomeCatalogTableHeaders($scope.productList);
			$scope.initBuyObject($scope.productList);
			DataSharingService.setObject("CATALOG_PRODUCT_LIST",$scope.productList );
			requestParams=  getParamKeyList($scope.filterOn,$scope.filterParam,$scope.mainCatalogId,$scope.mainCatalogElementId,$scope.orderType,$scope.sortColumn);
			if(!isNull($scope.$root)){
				$scope.$root.$eval();
			}
		}
		/* ADM-010 */$rootScope.refreshUIforPackPiece($scope.freeSearchObj.packPiece,
				$scope.productList);
	};
	
	$rootScope.mainSearchFilterChange = function(lang){
		getMainCatalogFilters(lang);
		
	};

	
	// Main Catalog Filter Starts
	
	
	
	var getMainCatalogFilters = function(lang){	
//		Util.getDataFromServer('mainCatelogFilterUrl',[]).then(function(data) {
//			//$scope.FilterData = data;
//			$scope.FilterList = data.filterBeans;
//			$scope.getFilterSettings(data.filterBeans);
//			$log.log("FilterListCtrl Data- items fetched : " +data.filterBeans.length);
//		});
		var params = [];
		if(!isNull(lang)){
			params = ["LanguageCode",lang.code];
		}else if(!isNull($rootScope.languageChangeFilter)||$rootScope.languageChangeFilter != undefined){
			params = ["LanguageCode",$rootScope.languageChangeFilter.code];
		}
		var finalFilterList = new FilterList();
		var staticFilterConfig = MainCatalogService.getFilterConfiguration();
		var filterDataList = DataSharingService.getProductFilterDetailsSettings(); 
		var dynamicFilterConfig = null;
		Util.getDataFromServer('mainCatelogFilterUrl',params).then(function(data){
				dynamicFilterConfig = data.filterBeans;
				finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig,'searchFilterList');
				$scope.FilterList = finalFilterList;
		});
	};
	
	$scope.removeMainCatalogFilter = function(filterItem)
	{
		$scope.FilterList.removeSelection(filterItem);
		$scope.filterParam  = $scope.FilterList.getParamsList();		
		var filterParam =  getParamKeyList($scope.filterOn,$scope.filterParam,$scope.mainCatalogId,$scope.mainCatalogElementId,$scope.orderType,$scope.sortColumn); 
		$scope.pageKey = Util.initPagination('mainCatalogProductListUrl',filterParam);
		$scope.paginationObj = DataSharingService.getObject($scope.pageKey );	
		MainCatalogService.getMainCatalogProducts(filterParam).then(function(data){
			//$rootScope.openLoader();
			if(data.productList==null || data.productList.length == 0){
				$scope.dataFound = false;				
				$scope.filterMsg =  Util.translate('MSG_NO_OBJECTS_FOR_SELECTION',null);
				showMsgNoObjFound();
				var icon = '<a class="OrderInformation-icon" shref="" title="'+$scope.filterMsg+'"></a>';				
				$scope.filterMsg = icon + $scope.filterMsg;
				$scope.recordFound = false;				
			}else{
				$scope.filterMsg = "";
				$scope.SelectedProductList = [];
				setUIData(data,true);
			}
		});
	};
	
	
		// to get filter configuration setting from "filterConfig.json"  
	$scope.getFilterSettings = function(filterList)
	{
		var filterConfigData = MainCatalogService.getFilterConfiguration();
		//console.log("mainCatalogCtrl getFilterSettings--  " +JSON.stringify(filterConfigData));			 
		$scope.FilterList =  $rootScope.getSearchFilterSettings(filterList,filterConfigData);
		if( $scope.FilterList != null )
		{
			for(var i= 0; i<$scope.FilterList.length; i++)
			{
				for(var j=0; j <$rootScope.productfilterDetailSettings.length ;j++)
				{
					if($scope.FilterList[i].filterName == $rootScope.productfilterDetailSettings[j].filterName)
					{
						$scope.FilterList[i].filterValueList = $rootScope.productfilterDetailSettings[j].searchFilterList;
					}
				}

			}
		}
	};

	var getParamKeyList = function(filterOn,filterParam,catalogId,catalogElementId,orderType,sortColumn)
	{
		var i=0;
		var requestParams =[];
		if(filterOn == true)
		{
			requestParams = ["CAT_ID",catalogId,"ELEMENT_ID",catalogElementId,"OrderBy",orderType,"SortBy",sortColumn];
			if(filterParam.length > 0)
			{
				for(i=0;i<filterParam.length;i++)
				{requestParams.push(filterParam[i]);}
			}
		}
		else			
		{	requestParams = ["CAT_ID",catalogId,"ELEMENT_ID",catalogElementId,"OrderBy",orderType,"SortBy",sortColumn];}

		return requestParams;		
	};
		
	$scope.submitMainCatlogFilter = function()
	{
		$scope.filterOn = true;
		$scope.SelectedProductList=[];
//		var keyList = $scope.filterFormInfo;		
//		$scope.filterParam =  $rootScope.setParamKeyList(keyList); 
		$scope.filterParam  = $scope.FilterList.getParamsList();
		var filterParam =  getParamKeyList($scope.filterOn,$scope.filterParam,$scope.mainCatalogId,$scope.mainCatalogElementId,$scope.orderType,$scope.sortColumn); 
		
		//console.log("Main Catalog Filter Info :  " + filterParam);
		pageKey = Util.initPagination('mainCatalogProductListUrl',filterParam);
		MainCatalogService.getMainCatalogProducts(filterParam).then(function(data){
			//$rootScope.openLoader();
			if(data.productList==null || data.productList.length == 0){
				$scope.dataFound = false;				
				$scope.filterMsg =  Util.translate('MSG_NO_OBJECTS_FOR_SELECTION',null);
				showMsgNoObjFound();
				var icon = '<a class="OrderInformation-icon" shref="" title="'+$scope.filterMsg+'"></a>';				
				$scope.filterMsg = icon + $scope.filterMsg;
				$scope.filterMsg = "";
				$scope.productList = DataSharingService.getObject("CATALOG_PRODUCT_LIST" );
				$scope.recordFound = true;
				
			}
			else
				{
				$scope.filterMsg = "";
				setUIData(data,true);
			}
		});
	};
	
	// Main Catalog Filter End

	$scope.isColExist = function(colPropName){
		var isCol = false;
		var val = Util.isPropertyExist(colPropName,$scope.productPropList);
		if(val){
			isCol = true;
		}
		return isCol;
	};

	$rootScope.mainCatalogLoadMore = function(){
		if($scope.isMoreRecords == true){
			//console.log("mainCatalogLoadMore() - called, isPaginationBlocked = "+ Util.isPaginationBlocked(pageKey).toString());
		if(!Util.isPaginationBlocked(pageKey)){
			//requestParams = ["PageNo",$scope.mainCataloguePageIndex++,"CAT_ID",$scope.mainCatalogId,"ELEMENT_ID",$scope.mainCatalogElementId];
			requestParams=  getParamKeyList($scope.filterOn,$scope.filterParam,$scope.mainCatalogId,$scope.mainCatalogElementId,$scope.orderType,$scope.sortColumn);
			//requestParams = ["CAT_ID",$scope.mainCatalogId,"ELEMENT_ID",$scope.mainCatalogElementId,"OrderBy",$scope.orderType,"SortBy",$scope.sortColumn];
			$rootScope.openBottomLoader();
			Util.getPageData('mainCatalogProductListUrl',requestParams ).then(function(data){
				$rootScope.closeBottomLoader();
				if(data.productList==null || data.productList.length == 0){
					Util.stopLoadingPageData(pageKey);
				}else{
					setUIData(data,false);
				}
			});
		}
		}
	};

	$scope.isTableView = function(){
		if ($scope.currentView.name == tblViewObj.name ){
			return true;
		}else{
			return false;
		}
	};

	$scope.isGridView = function(){
		if ($scope.currentView.name == gridViewObj.name ){
			return true;
		}else{
			return false;
		}
	};
	
	var setView = function(){
		if(isNull($stateParams.VIEW)  || isEmptyString($stateParams.VIEW)){
			$scope.currentView = gridViewObj;
		}else if(gridViewObj.name == $stateParams.VIEW){
			$scope.currentView = gridViewObj;
			$scope.ViewLabelName =  $scope.getViewLable();
			$scope.filterPanelCollapsed=false;
			$scope.isList  = false;
		}else {
			$scope.currentView = tblViewObj;
			$scope.ViewLabelName =  $scope.getViewLable();
			$scope.filterPanelCollapsed=true;
			$scope.isList  = true;
		}
	};
	
//	$scope.changeView = function(){
////		if($scope.currentView.name == gridViewObj.name){
////			$scope.currentView = tblViewObj;
////			$scope.ViewLabelName =  $scope.getViewLable();
////			$scope.filterPanelCollapsed=true;
////			$scope.isList  = true;
////		}else{
////			$scope.currentView = gridViewObj;
////			$scope.ViewLabelName =  $scope.getViewLable();
////			$scope.filterPanelCollapsed=false;
////			$scope.isList  = false;
////		}
//		if($scope.currentView.name == gridViewObj.name){
//			$state.go("home.mainCatalog",{"CAT_ID":$stateParams.CAT_ID,"ELEMENT_ID":$stateParams.ELEMENT_ID,"VIEW":tblViewObj.name});
//		}else{
//			$state.go("home.mainCatalog",{"CAT_ID":$stateParams.CAT_ID,"ELEMENT_ID":$stateParams.ELEMENT_ID,"VIEW":gridViewObj.name});
//		}
//	};
	
	$scope.changeView = function(){
		if($scope.currentView.name == gridViewObj.name){
			$scope.filterNewSearch = true;
			$scope.filterCatalog = true;
			$scope.currentView = tblViewObj;
			$scope.ViewLabelName =  $scope.getViewLable();
			$scope.filterPanelCollapsed=true;
			$scope.isList  = true;
		}else{
			$scope.currentView = gridViewObj;
			$scope.ViewLabelName =  $scope.getViewLable();
			$scope.filterPanelCollapsed=false;
			$scope.isList  = false;
		}
	};

	$scope.SelectedProduct = function(productList)
	{
		$scope.selectionMsg = "";
		$scope.SelectedProductList = $filter('filter')(productList, {checked: true});
		if($scope.SelectedProductList.length <=10)
		{

			$scope.selectionMsg = " Total selected Items :  "+ $scope.SelectedProductList.length;
			$scope.Comparison = true;
		}
		else
		{
			$scope.selectionMsg = Util.translate('CON_MAX_PRODUCT_COMPARISION');
			$scope.Comparison = false;
		}
		DataSharingService.setToLocalStorage("compareObjectList",$scope.SelectedProductList);
		//console.log("addCompareListItem length -  "+	$scope.SelectedProductList.length);

	};

	$scope.RemoveProduct = function()
	{
		$scope.SelectedProductList = $filter('filter')($scope.SelectedProductList, {checked: true});
		DataSharingService.setToLocalStorage("compareObjectList",$scope.SelectedProductList);
		if($scope.SelectedProductList.length > 1)
		{
		$scope.selectionMsg = " Total selected Items :  "+ $scope.SelectedProductList.length;
		}
		else
		{
			$state.go($scope.previousState);					
			$scope.selectionMsg = "";
		}

	};
	$scope.compare = function()
	{
		var isOpenLoginDlg=$rootScope.isSignOnRequired("CON_PRODUCT_COMPARE",'home.mainCatalog');
		if(isOpenLoginDlg==true){
			return;
		}
		if(($scope.SelectedProductList.length > 1) && ($scope.Comparison == true))
		{
			$rootScope.mainSelectedProductUnit = {};
			for(var i= 0; i< $scope.SelectedProductList.length ; i++)
			{
				for(var j= 0;j<$scope.SelectedProductList[i].productUnitList.length;j++)					
					{
						if($scope.SelectedProductList[i].productUnitList[j].unit == $scope.SelectedProductList[i].defaultSalesUnit)
						{		
							$rootScope.mainSelectedProductUnit[$scope.SelectedProductList[i].code] = $scope.SelectedProductList[i].productUnitList[j];						
						}
					}			
				
			}
			addpropertyToList($scope.SelectedProductList,'validQuantity',true);		
			DataSharingService.setToLocalStorage("compareObjectList",$scope.SelectedProductList);
			DataSharingService.setObject("prevCompareState",$state.current.name);
			DataSharingService.setObject(CONST_mainCatPrevCompareView,$scope.currentView );	
			DataSharingService.setObject("prevViewLabelMain",$scope.ViewLabelName );	

			$state.go('home.mainCatalog.Compare');
			$scope.$root.$eval();
		}
	};

	$scope.showHide = function(item){	  
		if(item === true){	return true;   }
		return false;
	};
	


	//console.log("main catelog sorting1-- Current Sorting Selection : " + $scope.Selection);
	 // home catelog sorting
		$scope.Sorting = function(column,orderBy) {
			if($scope.SelectedProductList.length>1){
		 		$scope.SelectedProductList = [];
		 		angular.forEach($scope.productList, function(a) {        
				    a.checked = false;
				});
		 	}
			$scope.sortColumn = column;
			$scope.orderType = orderBy;
			$rootScope.sortSelection =  $scope.sortColumn+$scope.orderType;
			requestParams=  getParamKeyList($scope.filterOn,$scope.filterParam,$scope.mainCatalogId,$scope.mainCatalogElementId,$scope.orderType,$scope.sortColumn);
			pageKey = Util.initPagination('mainCatalogProductListUrl',requestParams);
			Util.getDataFromServer('mainCatalogProductListUrl',requestParams ).then(function(data){			
				setUIData(data,true);
			});
		};
		
		$scope.onUnitChangeMainCat= function(itemCode,unitCode,oldCatalogItem){
			//var oldCatalogItem = getObjectFromList("code", itemCode, $scope.productList);
			$rootScope.getAndUpdateCatalogItem(itemCode,unitCode,oldCatalogItem);
		};


		mainCatalogCtrlInit();

}]);