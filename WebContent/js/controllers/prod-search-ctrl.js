//product search controller
catalougeControllers.controller('prodSearchCtrl', ['$rootScope','$scope','$q', '$http','$log', 'ProdSearchService','NewConfigService','DataSharingService','Util','$state','$stateParams','$filter',
                                                            function ($rootScope,$scope,$q, $http,$log,ProdSearchService,NewConfigService,DataSharingService,Util,$state,$stateParams,$filter) {
	
	
	var prodSearchCtrlInit = function(){
		$scope.identifierUnique = "prodSearchCtrl";
		$scope.advanceSearchParamsExist = false;
		$scope.PageNumberAdvanceSearch = 0;
		$scope.isMoreRecords = true;
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$rootScope.setFreeSearchObj($scope.freeSearchObj.text);
			if(!isNull($scope.freeSearchObj.andOr)){
				$rootScope.setAndOr($scope.freeSearchObj.andOr);
			}
			if(!isNull($scope.freeSearchObj.enableAndOrRadioBool)){
				$rootScope.setFreeSearchObjAndOrBool($scope.freeSearchObj.enableAndOrRadioBool);
			}
			$scope.initBuyObject($scope.searchProductList);
			/*var lc = DataSharingService.getFromLocalStorage("languageChanged");
			if(lc == 1) {*/
				//makePrdSearchFilters();
				/*DataSharingService.setToLocalStorage("languageChanged", 0);
			}*/
			$scope.$root.$eval();
		}else if($state.current.name=='home.prodSearchResult.Compare') {
			$scope.SelectedProductList = DataSharingService.getFromLocalStorage("compareObjectList");
			$scope.previousState = DataSharingService.getObject("prevCompareState");	
			$scope.updateCount = 0; 
			for(var i = 0; i < $scope.SelectedProductList.length; i++) {
				$rootScope.getProductsDetail($scope.SelectedProductList[i].code, $scope.updateBuy);
			}
		}
		else{
			var storedCache = historyManager.getCache($scope);
			if(!isNull(storedCache) && (storedCache.freeSearchObj.text == $rootScope.getFreeSearchObj()) && !isNull($scope.searchProductList)) {
				historyManager.loadScopeWithCacheProvided($scope, storedCache);
				$rootScope.setFreeSearchObj($scope.freeSearchObj.text);
				if(!isNull($scope.freeSearchObj.andOr)){
					$rootScope.setAndOr($scope.freeSearchObj.andOr);
				}
				if(!isNull($scope.freeSearchObj.enableAndOrRadioBool)){
					$rootScope.setFreeSearchObjAndOrBool($scope.freeSearchObj.enableAndOrRadioBool);
				}
				$scope.initBuyObject($scope.searchProductList);
				$scope.$root.$eval();
			} else {
//				$scope.productSearchFilterList = getProductSearchFilterList();
//				if(!isNull($scope.productSearchFilterList)){
//					//--$scope.productSearchFilterList.removeAllSelectedFiltersAdvanceSearch();
//					removeAllSelectedFiltersAdvanceFromFilterList($scope.productSearchFilterList);
//				} 
				$rootScope.setFreeSearchObj($stateParams.FREE_TEXT);
				if(!isNull($stateParams.AND_OR)){
					$rootScope.setAndOr($stateParams.AND_OR);
				}
				if(!isNull($stateParams.BOOL_VALUE)){
					$rootScope.setFreeSearchObjAndOrBool($stateParams.BOOL_VALUE);
				}
				$scope.freeTextSearch();
				prdSearchInit();
				//makePrdSearchFilters();
				//initController();
			}
		}
	};
	
	
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
		$rootScope.setFreeSearchObj("");
		$rootScope.setFreeSearchObjAndOrBool(false);
		$rootScope.setAndOr("OR");
	//	$log.log(" prodSearchCtrlInit history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	
	$scope.$on('userLoggedIn', function(event){ 
		if($state.current.name=='home.prodSearchResult.Compare') {
			$scope.SelectedProductList = DataSharingService.getFromLocalStorage("compareObjectList");
			$scope.updateCount = 0; 
			for(var i = 0; i < $scope.SelectedProductList.length; i++) {
				$rootScope.getProductsDetail($scope.SelectedProductList[i].code, $scope.updateBuy);
			}
		}
	});
	
	$rootScope.$on('searchTextChange', function(event, data) {
		$scope.freeSearchObj.text = data.text;
		if($rootScope.webSettings.showOperatorAndOr){
		$scope.freeSearchObj.andOr=data.andOr;		
		}
		if(!isNull(data.enableAndOrRadioBool)){
			$scope.freeSearchObj.enableAndOrRadioBool = data.enableAndOrRadioBool;
		}
	});

	// ADM-010
	$scope.$watch('freeSearchObj.packPiece',
		function(){
			if(!isNull($scope.searchProductList)){
				$rootScope.refreshUIforPackPiece($scope.freeSearchObj.packPiece, 
				$scope.searchProductList);
			}
		}
	);
	
	$scope.updateBuy = function(data) {
		for(var i = 0; i < $scope.SelectedProductList.length; i++) {
			if($scope.SelectedProductList[i].code == data.itemCode) {
				$scope.SelectedProductList[i].isBuyingAllowed = data.isBuyAllowed;
				break;
			}
		}
		$scope.updateCount++;
		if($scope.updateCount >= $scope.SelectedProductList.length) {
			$scope.compare();
		}
	}

	var setViewLabel = function(){
		if($state.current.name == PROD_SEARCH_RESULT_VIEW || $state.current.name==PROD_SEARCH_RESULT_GRIDVIEW){
			$scope.ViewLabelName = Util.translate('CON_TABLE_VIEW');
		}else{
			$scope.ViewLabelName = Util.translate('CON_GRID_VIEW');
		}
	};
	
	var setState = function(){
		if($state.current.name == 'home.prodSearchResult.Compare'){
			$scope.SelectedProductList = DataSharingService.getFromLocalStorage("compareObjectList");
			$scope.previousState = DataSharingService.getObject("prevCompareState");	

		}else{
			$scope.SelectedProductList = [];
		}
	};

	var getFreeTextParams = function(){
		var param = [];
		var langCode = NewConfigService.getLanguageCode();
		var operandAndOr = false;
		if(!isNull($rootScope.webSettings)){
		 operandAndOr = $rootScope.webSettings.showOperatorAndOr;
		}
		if(operandAndOr || !isNull($scope.freeSearchObj.andOr) ){
			if(!isEmptyString($scope.freeSearchObj.text)){
				param = ['SEARCH_TYPE','FREE','FREE_TEXT',$scope.freeSearchObj.text,'LanguageCode',langCode,'OPR_AND_OR',$scope.freeSearchObj.andOr];
		}else{
			param = ['SEARCH_TYPE','FREE','LanguageCode',langCode,'OPR_AND_OR',$scope.freeSearchObj.andOr];
		}
			}
		else if(!isEmptyString($scope.freeSearchObj.text)){
			param = ['SEARCH_TYPE','FREE','FREE_TEXT',$scope.freeSearchObj.text,'LanguageCode',langCode];
		}else{
			param = ['SEARCH_TYPE','FREE','LanguageCode',langCode];
		}
		return param;
	};
	
	var setMessages = function (data){
		if(!isNull(data)){
			if((!isNull(data.solrItemSearchListBean) && data.solrItemSearchListBean.length == 0 ) || data.messageCode ==111){
				$scope.infoMsg = Util.translate('MSG_NO_OBJECTS_FOR_SELECTION');
				showMsgNoObjFound();
			}else if(data.messageCode ==5003){
				$scope.infoMsg = Util.translate('ENTERPRISE_INSTANCE_IS_NOT_ASSIGNED_NETSTORE_INSTANCE');
				searchInstanceIssueMsgFound("ENTERPRISE_INSTANCE_IS_NOT_ASSIGNED_NETSTORE_INSTANCE");
			}else if(data.messageCode ==5004){
				$scope.infoMsg = Util.translate('RESPECTIVE_SOLR_INSTANCE_IS_NOT_UP');
				searchInstanceIssueMsgFound("RESPECTIVE_SOLR_INSTANCE_IS_NOT_UP"); 
			}else if(data.messageCode ==3232){
				$scope.infoMsg = Util.translate('SP_MISSING_FOR_PRICE_IN_BULK');
				showMsgNoObjFound(data.messageCode);
			}else if(data.messageCode ==3233){
				$scope.infoMsg = Util.translate('SP_MISSING_FOR_AVAIL_IN_BULK');
				showMsgNoObjFound(data.messageCode);
			}
			else if(!isNull(data.messageCode) && data.messageCode>0 && data.solrItemSearchListBean.length == 0){
				$scope.errorMsg = Util.translate('MSG_UNEXPECTED_ERROR_OCCURED_WHILE_SEARCH');
				//console.log("prodSearchCtrl.setMessages"+ JSON.stringify(data.messageCode));
			}else{
				setMessagesToNull();
			}
		}else{
			$scope.infoMsg = Util.translate('MSG_NO_OBJECTS_FOR_SELECTION');
			showMsgNoObjFound();
		}
	};
	
	var setMessagesToNull = function(){
		$scope.infoMsg = null;
		$scope.errorMsg = null;
		//$scope.infoMsg = "test";
		//$scope.errorMsg = "test error";
	};
	
	$scope.initBuyObject = function(list){
		$scope.buyObj = new Buy(list);
		$scope.buyObj.setPropNameItemCode('solrItemCode');
		$scope.buyObj.setPropNameOrdered('quantity');
		$scope.buyObj.setPropNameUnit('salesUnit');
		$scope.buyObj.setPropNameisBuyAllowed('isBuyingAllowed');
	};
	
	$scope.buyItems = function(item,buyObj){
		$rootScope.buyMultipleItems(item,$scope.buyObj);
	};
	
	$scope.addMultipleItemsToCurrentList = function(item){
		$rootScope.addMultipleItemsToList(item,$scope.buyObj);
	};
	
	var setupDataForUI = function(data,isFirstPageData){
		if(data.moreRecords != undefined){
		$scope.isMoreRecords = data.moreRecords;
		}
		if(!hasVerticalScroll() && $scope.isMoreRecords) {
			$rootScope.prodSearchLoadMore();
		}
		if(isFirstPageData){
			
			setMessages(data);
			
			if(isNull($scope.infoMsg)){
				if(data.solrItemSearchListBean.length>0){
					initPrdSearchTableHeaders();
					setPrdSearchExtendedData(data.solrItemSearchListBean);
					addpropertyToList(data.solrItemSearchListBean,'validQuantity',true);
					$scope.searchProductList=data.solrItemSearchListBean;
					setPrdSearchTableHeaders($scope.searchProductList);
					$scope.initBuyObject($scope.searchProductList);	
				}
			}
			//$rootScope.closeLoader();();
		}else if(data.solrItemSearchListBean.length>0){
			$rootScope.closeBottomLoader();
			setPrdSearchExtendedData(data.solrItemSearchListBean);
			addpropertyToList(data.solrItemSearchListBean,'validQuantity',true);
			Util.appendItemsToList(data.solrItemSearchListBean, $scope.searchProductList);
			setPrdSearchTableHeaders($scope.searchProductList);
			$scope.initBuyObject($scope.searchProductList);
		}
		/* ADM-010 */$rootScope.refreshUIforPackPiece($scope.freeSearchObj.packPiece,
				$scope.searchProductList);
		$scope.$root.$eval();
	};
	
	$scope.exportPrdSearchData = function(fileType)
	{
		var prdSearchTable = new Table();
		var data;
		var filePrefix = "ProductSearch";
		var title = Util.translate('CON_PRODUCT_SEARCH');
		var sheetName = title ;
		switch(fileType){

		case 'pdf' :
			prdSearchTable = getPrdSearchDataForExport();
			data = prdSearchTable.getHTML();												
			var fileName = $rootScope.getExportFileName (filePrefix,".pdf");
			savePdfFromHtml(data,title,fileName);
			//$log.log("exportPrdSearchData -  table Object "+ JSON.stringify(data));
			break;

		case 'xls' :
			prdSearchTable = getPrdSearchDataForExport();
			var data = prdSearchTable.getDataWithHeaders();
			var fileName = $rootScope.getExportFileName (filePrefix,".xlsx");						
			//$rootScope.exportTableAsExcel(data,title,fileName);
			saveExcel(data,sheetName,fileName);
			//$log.log("exportPrdSearchData -  table Object "+ JSON.stringify(data));

			break;
		case 'xml' :
			prdSearchTable = getPrdSearchDataForExport('XML');
			prdSearchTable.setRootElementName("PRODUCT_SEARCH");
			prdSearchTable.setEntityName("PRODUCT_DETAIL");
			var data = prdSearchTable.getXML();
			var fileName = $rootScope.getExportFileName (filePrefix,".xml");						
			saveXML(data,fileName);
			//$log.log("exportPrdSearchData -  table Object "+ JSON.stringify(data));
			break;

		default :
			//console.log("Exporting Request Search - failed");
		}
	};
	
	var getPrdSearchDataForExport = function(target){
		var prdSearchTable = new Table();
		if(isEqualStrings(target,"XML")){
			prdSearchTable.addHeader('SCORE');
			prdSearchTable.addHeader('PRODUCT');
			prdSearchTable.addHeader('DESCRIPTION');
			if($scope.prdSearchTblHeaders.showCrossReferenceColumn){
				prdSearchTable.addHeader('CROSS_REFERENCE');
			}
			if($scope.prdSearchTblHeaders.actualPrice && $rootScope.webSettings.isShowPrice){
				prdSearchTable.addHeader('ACTUAL_PRICE');
				prdSearchTable.addHeader('UNIT');
			}
			if($rootScope.webSettings.isShowPrice && !isNull($rootScope.webSettings.currencyCode)){
				prdSearchTable.addHeader('CURRENCY_CODE');
			}
			if($scope.prdSearchTblHeaders.discountPrice && $rootScope.webSettings.isShowPrice){
				prdSearchTable.addHeader('DISCOUNT_PRICE');
			}
			if($scope.prdSearchTblHeaders.discountPercentage && $rootScope.webSettings.isShowPrice){
				prdSearchTable.addHeader('DISCOUNT_PERCENTAGE');
			}
			
			prdSearchTable.addHeader('UNIT');
			
			if($scope.prdSearchTblHeaders.showItemGroupColumn){
				prdSearchTable.addHeader('ITEM_GROUP');
			}
			prdSearchTable.addHeader('ITEM_CATEGORY1');
			prdSearchTable.addHeader('ITEM_CATEGORY2');
			prdSearchTable.addHeader('ITEM_CATEGORY3');
			prdSearchTable.addHeader('ITEM_CATEGORY4');
		}else{
			prdSearchTable.addHeader(Util.translate('COH_MATCHES_SEARCH_CRITERIA'));
			prdSearchTable.addHeader(Util.translate('CON_PRODUCT'));
			prdSearchTable.addHeader(Util.translate('COH_DESCRIPTION'));
			if($scope.prdSearchTblHeaders.showCrossReferenceColumn){
				prdSearchTable.addHeader(Util.translate('CON_CROSS_REFERENCE'));
			}
			if($scope.prdSearchTblHeaders.actualPrice && $rootScope.webSettings.isShowPrice){
				var val = Util.translate('COH_ACTUAL_PRICE');
				val = val+"(" + $rootScope.webSettings.currencyCode +")";
				prdSearchTable.addHeader(val);
				
			}
			if($scope.prdSearchTblHeaders.discountPrice && $rootScope.webSettings.isShowPrice){
				var val = Util.translate('COH_DISCOUNT_PRICE') +"("+ $rootScope.webSettings.currencyCode +")";
				prdSearchTable.addHeader(val);
			}
			if($scope.prdSearchTblHeaders.discountPercentage && $rootScope.webSettings.isShowPrice){
				prdSearchTable.addHeader(Util.translate('CON_DISCOUNT_%'));
			}
			
			prdSearchTable.addHeader(Util.translate('COH_UNIT'));
			
			if($scope.prdSearchTblHeaders.showItemGroupColumn){
				prdSearchTable.addHeader(Util.translate('COH_ITEM_GROUP'));
			}
			//prdSearchTable.addHeader(Util.translate('COH_ITEM_CATEGORY1'));
			//prdSearchTable.addHeader(Util.translate('COH_ITEM_CATEGORY2'));
			//prdSearchTable.addHeader(Util.translate('COH_ITEM_CATEGORY3'));
			//prdSearchTable.addHeader(Util.translate('COH_ITEM_CATEGORY4'));
		}
		//set row data
		data = $scope.searchProductList;
		for(var i=0;i<data.length;i++){
			var item = data[i];
			var row = [];
			
			row.push(item.propScoring);
			row.push(item.solrItemCode);
			row.push(item.itemDescription);
			if($scope.prdSearchTblHeaders.showCrossReferenceColumn){
				row.push(item.crossReference);
			}
			if($scope.prdSearchTblHeaders.actualPrice && $rootScope.webSettings.isShowPrice){
				row.push(item.actualPrice);
			}
			if(isEqualStrings(target,"XML") && $rootScope.webSettings.isShowPrice && !isNull($rootScope.webSettings.currencyCode)){
				row.push($rootScope.webSettings.currencyCode);
			}
			if($scope.prdSearchTblHeaders.discountPrice && $rootScope.webSettings.isShowPrice){
				row.push(item.discountPrice);
				
			}
			if($scope.prdSearchTblHeaders.discountPercentage && $rootScope.webSettings.isShowPrice){
				row.push(item.discountPercentageText);
			}

			row.push(item.itemUnitCode);
			
			if($scope.prdSearchTblHeaders.showItemGroupColumn){
				row.push(item.itemGroup);
			}
			//row.push(item.itemCat1);
			//row.push(item.itemCat2);
			//row.push(item.itemCat3);
			//row.push(item.itemCat4);
			
			prdSearchTable.addRow(row);
		}
		//$log.log("request export table  "+ JSON.stringify(prdSearchTable));
		return prdSearchTable;
	};
	
	var getSearchResultforFirstTime = function(params,isAdvanceSearch){
        setMessagesToNull();
        if(Array.isArray(params) && params.length>0){
        	var paramStringForSearch = "solrSearchInputBean=="+JSON.stringify(params);
               $scope.pageKey = Util.initPagination('solrSearchResultUrl',params);
               Util.appendItemsToList(["PageNo",1], params);
               ProdSearchService.getPrdSearchResult(params).then(function(data){
            	   if(!isNull(data.filtersList)){
						$scope.filterDetails = data.filtersList;
	                     makeProductSearchFilters();
					}
                     setupDataForUI(data,true);
                     if(isAdvanceSearch && !isNull(data.solrItemSearchListBean) && data.solrItemSearchListBean.length>0){
                            $scope.freeSearchObj.text="";
                     }
               });
        }
 };	
 
 
 var getSearchResultforFirstTimeAdvanceSearch = function(params,isAdvanceSearch,searchFlag){
     setMessagesToNull();
     if(Array.isArray(params) && params.length>0){
     	$scope.advanceSearchParamsExist = true;
     	var paramStringForSearch = "solrSearchInputBean="+JSON.stringify(params);
           // $scope.pageKey = Util.initPagination('solrSearchResultUrl',params);
           //  Util.appendItemsToList(["PageNo",1], params);
            ProdSearchService.getPrdSearchResultNew(paramStringForSearch).then(function(data){
            	if(!isNull(searchFlag) && isNull(data.messageCode)){
            	 $scope.filterDetails = data.filtersList;
                 makeProductSearchFilters();
            	}
            	  setupDataForUI(data,true);
                  if(isAdvanceSearch && !isNull(data.solrItemSearchListBean) && data.solrItemSearchListBean.length>0){
                       //  $scope.freeSearchObj.text="";
                  }
            });
     }
};		

	var setPageKey = function(params){
		//var paginationObj = DataSharingService.getObject($scope.pageKey);	
		//if(isNull(paginationObj)){
			var prevPageKey =	DataSharingService.getObject("SolarSearchFilterPageKey");
		  	if(isNull(prevPageKey) || $scope.pageKey != prevPageKey ){
				$scope.pageKey = Util.initPagination('solrSearchResultUrl',params);
				DataSharingService.setObject("SolarSearchFilterPageKey",$scope.pageKey);
		  	}
		  		
		//}
	};
	
// pageKey initailized on first call of WS 
	$rootScope.prodSearchLoadMore = function(){
		//console.log("prodSearchLoadMore() - called, isPaginationBlocked = "+ Util.isPaginationBlocked($scope.pageKey).toString());
		
		if($scope.advanceSearchParamsExist == false){
		if(!Util.isPaginationBlocked($scope.pageKey)){
			if(!isNull($scope.productSearchFilterList)){
				var requestParams = getParamList(); 
							
				setPageKey(requestParams);
				if(requestParams.length >1){
					$rootScope.openBottomLoader();
					Util.getPageData('solrSearchResultUrl',requestParams ).then(function(data){
						$rootScope.closeBottomLoader();
						if(data.solrItemSearchListBean==null || data.solrItemSearchListBean.length == 0){
							Util.stopLoadingPageData($scope.pageKey);
						}else{
							  setupDataForUI(data,false);

						}
					});
				}
			}
		}}
		if($scope.advanceSearchParamsExist == true){
			if(!Util.isPaginationBlocked($scope.pageKey)){
				
				if(!isNull($scope.productSearchFilterList)){
				var	advanceParamList = getAdvanceParamListProductSearch();
					//setPageKey(advanceParamList);
					if(advanceParamList.length >=1){
						$rootScope.openBottomLoader();
						var paramStringForSearch = "solrSearchInputBean="+JSON.stringify(advanceParamList);
						ProdSearchService.getPrdSearchResultNew(paramStringForSearch).then(function(data){
							$rootScope.closeBottomLoader();
							if(data.solrItemSearchListBean==null || data.solrItemSearchListBean.length == 0){
								Util.stopLoadingPageData($scope.pageKey);
							}else{
								
			                     setupDataForUI(data,false);
							}
						});
					}
				}
			}
			
		}
		
	};
	
	$scope.changeView = function(){
		if($state.current.name == PROD_SEARCH_RESULT_VIEW || $state.current.name==PROD_SEARCH_RESULT_GRIDVIEW){
			$scope.ViewLabelName = Util.translate('CON_GRID_VIEW');
			$scope.isList  = true;
			$scope.filterPanelCollapsed=true;
			$state.go(PROD_SEARCH_RESULT_LISTVIEW);	
		}else if($state.current.name == "home.prodSearchResultMenu"){
			$scope.ViewLabelName = Util.translate('CON_GRID_VIEW');
			$scope.isList  = true;
			$scope.filterPanelCollapsed=true;
			$state.go("home.prodSearchResultMenu.listView");
		}else if($state.current.name == "home.prodSearchResultMenu.listView"){
			$scope.ViewLabelName = Util.translate('CON_TABLE_VIEW');
			$scope.isList  = false;
			$scope.filterPanelCollapsed=false;
			$state.go("home.prodSearchResultMenu" );
		}else{
			$scope.ViewLabelName = Util.translate('CON_TABLE_VIEW');
			$scope.isList  = false;
			$scope.filterPanelCollapsed=false;
			$state.go(PROD_SEARCH_RESULT_GRIDVIEW );
		}
	};
	
	$scope.isTrue = function(value){
		return isValueTrue(value);
	};
	
	$scope.buyClicked = function(itemObj){
		if(!itemObj.validQuantity || isNull(itemObj.quantity) || ""==itemObj.quantity){
			itemObj.validQuantity = false;
			itemObj['quantityBoxMsg'] = Util.translate('MSG_QUANTITY_NOT_SPECIFIED');	
		}else{
		itemObj.isBuyingAllowed = false;
		//$rootScope.addToCart(itemObj.solrItemCode,itemObj.quantity,itemObj.itemUnitCode);
		$rootScope.addItemToCart(itemObj.solrItemCode,itemObj.quantity,itemObj.salesUnit,itemObj,"quantity");
		}
	};
	
	$scope.addItem = function(item)
    {
		item['code'] = item.solrItemCode;
    	item['salesUnit'] = item.itemUnitCode;
//    	item['quantity'] = $scope.buyItem[$rootScope.getId('quantity',item.code)];
    	$rootScope.addItemToCurrentList(item);    	
    };
    
	var setPrdSearchTableHeaders = function(list){
		var columnsToCheck = $scope.getPrdSearchColumnsToCheck();
		var headers =  $scope.getPrdSearchTableHeaders();

		// initialize with false
		headers.showCrossReferenceColumn=false;
		headers.showItemGroupColumn=false;
		headers.showAvailableColumn=false;
		headers.isBuyingAllowed=false;

		if(Util.isNotNull(list)){
			for (var i=0;i<list.length ; i++){
				var  item  = list[i];
				setTableHeader(item,columnsToCheck,headers);

				if (isValueTrue(item.showCrossReferenceColumn)){
					headers.showCrossReferenceColumn=true;
				}
				if (isValueTrue(item.showItemGroupColumn)){
					headers.showItemGroupColumn=true;
				}
				if (isValueTrue(item.showAvailableColumn)){
					headers.showAvailableColumn=true;
				}
				if(isValueTrue(item.isBuyingAllowed)){
					headers.isBuyingAllowed = true;
				}
				//setPrdSearchExtendedData(item);
			}
		}
		//console.log("Product Search Table headers : " +JSON.stringify(headers));
	};	
	
	var setPrdSearchExtendedData = function (list){
		
		if(Util.isNotNull(list)){
			for (var i=0;i<list.length ; i++){
				var item  = list[i];
				item.showAvailabilityAsLink = $rootScope.showAvailabilityAsLink();
				item.showAvailabilityAsImage = $rootScope.showAvailabilityAsImage();
				item.availabilityImageName = $rootScope.getAvailabilityImage(item.availabilityOfItem);
				item.availabilityText = $rootScope.getAvailabilityText(item.availabilityOfItem);

				item.showEnquiryImage = $rootScope.showEnquiryImage(item.enquiryImage);
				item.enquiryImage = $rootScope.getEnquiryImageName(item.enquiryImage);
				handleEmptyAvailabilityImage(item);
				
				item.validQuantity=true;
				item.code=item.solrItemCode;
				item.description =  item.itemDescription;
				if(isNull(item.salesUnit)){
					item.salesUnit=item.itemUnitCode;
					item.selectedUnit = item.itemUnitCode;
				}else{
					item.selectedUnit = item.salesUnit;
				}
				//append % if discountPercentage has value
				if(!isEmptyString(item.discountPercentage)){
					item.discountPercentageText = item.discountPercentage + " %";
				}
				if(!item.hasOwnProperty("salesUnitObject")){
					item.selectedUnitObject= {};
					item.selectedUnitObject = getSelectedSalesUnit(item.salesUnit,item.salesUnitsDesc);
				}
			}
		}
	};
	

	$scope.SelectedProduct = function(productList)
	{
		$scope.selectionMsg = "";
		$scope.SelectedProductList = $filter('filter')(productList, {checked: true});
		if($scope.SelectedProductList.length <=10)
		{
			$scope.selectionMsg = " Total selected Items :  "+ $scope.SelectedProductList.length;
			$scope.Comparison = true;
		}
		else
		{
			$scope.selectionMsg = Util.translate('CON_MAX_PRODUCT_COMPARISION');
			$scope.Comparison = false;
		}
		DataSharingService.setToLocalStorage("compareObjectList",$scope.SelectedProductList);
		//console.log("addCompareListItem length -  "+	$scope.SelectedProductList.length);

	};
	$scope.RemoveProduct = function()
	{
		$scope.SelectedProductList = $filter('filter')($scope.SelectedProductList, {checked: true});
		DataSharingService.setToLocalStorage("compareObjectList",$scope.SelectedProductList);
		if($scope.SelectedProductList.length > 1)
		{	
		$scope.selectionMsg = " Total selected Items :  "+ $scope.SelectedProductList.length;
		}
		else
		{
			$state.go($scope.previousState);					
			$scope.selectionMsg = "";
			if($state.current.name == 'home.homeCatalogTable'){
				$scope.ViewLabelName = gridViewLabelName;
				$scope.isList  = false;
			}
			else{			
				$scope.ViewLabelName = tableViewLabelName;
				$scope.isList  = true;
			}
		}
	};
	
	$scope.getProductUnitBean = function (item,unitCode){
		var productUnitBean = null;
		if(!isNull(item) && !isNull(item.productUnitBeanList) && item.productUnitBeanList.length > 0){
			productUnitBean = getObjectFromList('unit',unitCode,item.productUnitBeanList);
		}
		return productUnitBean;
	};
	
	$scope.compare = function()
	{
		var isOpenLoginDlg=$rootScope.isSignOnRequired("CON_PRODUCT_COMPARE",'home.prodSearchResult');
		if(isOpenLoginDlg==true){
			return;
		}
		if(($scope.SelectedProductList.length > 1) && ($scope.Comparison == true))
		{
			$rootScope.solrSelectedProductUnit = {};
			for(var i= 0; i< $scope.SelectedProductList.length ; i++)
			{
				for(var j= 0;j<$scope.SelectedProductList[i].productUnitBeanList.length;j++)					
					{
						if($scope.SelectedProductList[i].productUnitBeanList[j].unit == $scope.SelectedProductList[i].itemUnitCode)
						{		
							$rootScope.solrSelectedProductUnit[$scope.SelectedProductList[i].solrItemCode] = $scope.SelectedProductList[i].productUnitBeanList[j];						
						}
					}			
				
			}
			addpropertyToList($scope.SelectedProductList,'validQuantity',true);		
			DataSharingService.setToLocalStorage("compareObjectList",$scope.SelectedProductList);
			DataSharingService.setObject("prevCompareState",$state.current.name);		
			DataSharingService.setObject("prevCompareView",$scope.currentView );	
			DataSharingService.setObject("prevViewLabel",$scope.ViewLabelName );		
			$state.go('home.prodSearchResult.Compare');
			$scope.$root.$eval();
		}
	};
	
	var prdSearchInit = function(){
		$scope.filterPanelCollapsed=$state.current.searchSectionCollapsed;
		setViewLabel();
		setState();
		$scope.prdSearchShowExtendInfo = $scope.ExtendedInfoOption2;
		$scope.errorMsg = null;
		//$scope.freeTextSearch();
		//setTableDataforExport();
	};
	
	$scope.getPrdSearchColumnsToCheck = function(){
		var  columnsToCheck = ['actualPrice','discountPrice','discountPercentage','availabilityOfItem'];
		return columnsToCheck;
	};
	
	var initPrdSearchTableHeaders = function(){
		$scope.prdSearchTblHeaders = {};
	};
	
	$scope.getPrdSearchTableHeaders = function(){
		return $scope.prdSearchTblHeaders;
	};
	
	var getProductSearchFilterList = function(){
		return DataSharingService.getObject("productSearchFilterList");
	};
	
	var setProductSearchFilterList = function(productSearchFilters){
		return DataSharingService.setObject("productSearchFilterList",productSearchFilters);
	};
	
	var makePrdSearchFilters = function(){
		if($rootScope.productfilterDetailSettings) {
			makeProductSearchFilters();
		} else {
			$rootScope.$on('filterDataUpdated', function(event){ 
				makeProductSearchFilters();
			});
		}
	};
	
	function makeProductSearchFilters() {
		//console.log("--------------------- hey filters already applied-----------------");
			var finalFilterList = new FilterList();
			var staticFilterConfig = ProdSearchService.getStaticSolrFilterConfig();
			var filterDataList = $scope.filterDetails ;

			var dynamicFilterConfig = null;
			ProdSearchService.getDynamicSolrFilterConfig().then(function(data){
				dynamicFilterConfig = data.filterBeans;
				finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig,'searchFilterList');
				$scope.productSearchFilterList = finalFilterList;
				setProductSearchFilterList(finalFilterList);
				//console.log("makePrdSearchFilters : " + JSON.stringify(finalFilterList));	
			});
		

		//removeAllAdvanceFilters();
	}
	
	$scope.advanceSearch = function(searchflag){
		if(!isNull(searchflag)){
			$scope.fromSearchButton = searchflag;
		}
		$scope.SelectedProductList=[];
		$scope.PageNumberAdvanceSearch = 0;
		var paramForKeySet = [];
		paramForKeySet = ['SEARCH_TYPE','FREE'];
		paramList = getAdvanceParamListProductSearch();
		$scope.pageKey = Util.initPagination('solrSearchResultUrl',paramForKeySet);
		if(paramList.length>0){
			$scope.errorMsg = null;
			//$scope.freeSearchObj.text="";
			getSearchResultforFirstTimeAdvanceSearch(paramList,true,searchflag);
		}else{
			//$scope.errorMsg = Util.translate('MSG_SELECT_PRODUCT_SEARCH_CRITERIA') ;
			getSearchResultforFirstTime(getFreeTextParams());
		}
		$scope.$root.$eval();
	};
	
	$scope.removeFilter = function(filterItem)
	{
		$scope.productSearchFilterList.removeSelectionProductSearch(filterItem);
		$scope.advanceSearch();
		
		
	};
	
	$scope.removeAllSelectedFiltersAdvanceSearchScope = function(){
		$scope.advanceSearchParamsExist = false;
		if(!isNull($scope.productSearchFilterList)){
			//--$scope.productSearchFilterList.removeAllSelectedFiltersAdvanceSearch();
			removeAllSelectedFiltersAdvanceFromFilterList($scope.productSearchFilterList);
		}
		$scope.advanceSearch();
	};
	$scope.clearFilterSelection = function(){
		$scope.productCodeSearchText="";
		$scope.productDescSearchText="";
		//ADM-011
		$scope.minsanCodeSearchText = "";
		$scope.eanCodeSearchText = "";
		$scope.substanceNameSearchText = "";
		$scope.manufacturerNameSearchText = "";
		//ADM-011 end
		$scope.advanceSearchParamsExist = false;
		if(!isNull($scope.productSearchFilterList)){
			$scope.productSearchFilterList.removeAllSelectedFiltersAdvanceSearch();
		}
		$scope.advanceSearch();
	};
	
	var removeAllAdvanceFilters = function(){
		$scope.productCodeSearchText="";
		$scope.productDescSearchText="";
		//ADM-011
		$scope.minsanCodeSearchText = "";
		$scope.eanCodeSearchText = "";
		$scope.substanceNameSearchText = "";
		$scope.manufacturerNameSearchText = "";
		//ADM-011 end
		if(!isNull($scope.productSearchFilterList)){
			//--$scope.productSearchFilterList.removeAllSelectedFilters();
			removeAllSelectedFiltersFromFilterList($scope.productSearchFilterList);
		}
	};
	
	var getParamList = function(){
		var params = getAdvanceParamList();
		if(params.length>0){
			return params;
		}else{
			return getFreeTextParams();
		}
	};
	var getAdvanceParamList = function(){
		var param = [];
		var itemId = new Object();
		var itemDesc = new Object();
		var searchType = new Object();
		
		if(!isEmptyString($scope.productCodeSearchText)){
			param.push('ITEM_ID');
			param.push($scope.productCodeSearchText);
		}
		if(!isEmptyString($scope.productDescSearchText)){
			param.push('ITEM_DESC');
			param.push($scope.productDescSearchText);
		}
		//ADM-011
		if(!isEmptyString($scope.minsanCodeSearchText)){
			param.push('MINSAN_CODE');
			param.push($scope.minsanCodeSearchText);
		}
		if(!isEmptyString($scope.eanCodeSearchText)){
			param.push('EAN_CODE');
			param.push($scope.eanCodeSearchText);
		}
		if(!isEmptyString($scope.substanceNameSearchText)){
			param.push('SUBSTANCE_NAME');
			param.push($scope.substanceNameSearchText);
		}
		if(!isEmptyString($scope.manufacturerNameSearchText)){
			param.push('MANUFACTURER_NAME');
			param.push($scope.manufacturerNameSearchText);
		}
		//ADM-011 end
		filterParamList = $scope.productSearchFilterList.getParamsList();
		appendItemsToList(filterParamList,param);
		if(param.length>0){
			param.push('SEARCH_TYPE');
			param.push('ADV');
		}

		return param;
	};
	
	var getAdvanceParamListProductSearch = function(){
		var languageCode = NewConfigService.getLanguageCode();
		var param = [];
		var itemIDSelected = null;
		var itemDesc = null;
		var oprAndOr = null;
		//ADM-011
		var minsanCode = null;
		var eanCode = null;
		var substanceName = null;
		var manufacturerName = null;
		var text = null;
		var isFullTextSearch = !isNull($rootScope.webSettings.itemSearchFreeTextMode) && 
		$rootScope.webSettings.itemSearchFreeTextMode == "*FULL_TEXT";
		//ADM-011 end
		
		$scope.PageNumberAdvanceSearch = $scope.PageNumberAdvanceSearch + 1;
		if(!isEmptyString($scope.productCodeSearchText)){
			itemIDSelected = encodeURIComponent($scope.productCodeSearchText);
		}else if(!isEmptyString($scope.freeSearchObj.text) && !isFullTextSearch){
			itemIDSelected = encodeURIComponent($scope.freeSearchObj.text);
		}
		if(!isEmptyString($scope.productDescSearchText)){
			itemDesc = $scope.productDescSearchText; 
		}else if(!isEmptyString($scope.freeSearchObj.text) && !isFullTextSearch){
			itemDesc = encodeURIComponent($scope.freeSearchObj.text);
		}
		//ADM-011
		if(!isEmptyString($scope.minsanCodeSearchText)){
			minsanCode = $scope.minsanCodeSearchText; 
		}else if(!isEmptyString($scope.freeSearchObj.text) && !isFullTextSearch){
			minsanCode = encodeURIComponent($scope.freeSearchObj.text);
		}
		if(!isEmptyString($scope.eanCodeSearchText)){
			eanCode = $scope.eanCodeSearchText; 
		}else if(!isEmptyString($scope.freeSearchObj.text) && !isFullTextSearch){
			eanCode = encodeURIComponent($scope.freeSearchObj.text);
		}
		if(!isEmptyString($scope.substanceNameSearchText)){
			substanceName = $scope.substanceNameSearchText; 
		}else if(!isEmptyString($scope.freeSearchObj.text) && !isFullTextSearch){
			substanceName = encodeURIComponent($scope.freeSearchObj.text);
		}
		if(!isEmptyString($scope.manufacturerNameSearchText)){
			manufacturerName = $scope.manufacturerNameSearchText; 
		}else if(!isEmptyString($scope.freeSearchObj.text) && !isFullTextSearch){
			manufacturerName = encodeURIComponent($scope.freeSearchObj.text);
		}
		if(!isNull($scope.freeSearchObj.text) || $scope.freeSearchObj.text != ""){
			text =$scope.freeSearchObj.text;
		}
		//ADM-011 end
		if(!isNull($scope.freeSearchObj.andOr)||$scope.freeSearchObj.andOr!=""){
			oprAndOr =$scope.freeSearchObj.andOr;
		}
		filterParamList = $scope.productSearchFilterList.getParamsListProductSearch(itemIDSelected,itemDesc,$scope.PageNumberAdvanceSearch,oprAndOr,languageCode,
				/* ADM-011 */minsanCode, eanCode, manufacturerName, substanceName, text);
		param = filterParamList;
		return param;
	};
	
	
	$rootScope.freeTextSearch = function (){
		if(!isNull($scope.productSearchFilterList)){
			//--$scope.productSearchFilterList.removeAllSelectedFiltersAdvanceSearch();
			removeAllSelectedFiltersAdvanceFromFilterList($scope.productSearchFilterList);
		} 
		removeAllAdvanceFilters();
		getSearchResultforFirstTime(getFreeTextParams());
	};
	
	$rootScope.freeTextSearchSearchPage = function (text,andOrLogic,logicAndOrBool){
		if(!isNull($scope.productSearchFilterList)){
			//--$scope.productSearchFilterList.removeAllSelectedFiltersAdvanceSearch();
			removeAllSelectedFiltersAdvanceFromFilterList($scope.productSearchFilterList);
		} 
		removeAllAdvanceFilters();
		getSearchResultforFirstTimeFromSearch(getFreeTextParamsFromSearchPage(text,andOrLogic),text,andOrLogic,logicAndOrBool);
	};
	
	
	var getFreeTextParamsFromSearchPage = function(text,andOrLogic){
		var param = [];
		var langCode = NewConfigService.getLanguageCode();
		var operandAndOr = false;
		if(!isNull($rootScope.webSettings)){
		 operandAndOr = $rootScope.webSettings.showOperatorAndOr;
		}
		if(operandAndOr || !isNull(andOrLogic) ){
			if(!isEmptyString(text)){
				param = ['SEARCH_TYPE','FREE','FREE_TEXT',text,'LanguageCode',langCode,'OPR_AND_OR',andOrLogic];
		}else{
			param = ['SEARCH_TYPE','FREE','LanguageCode',langCode,'OPR_AND_OR',andOrLogic];
		}
			}
		else if(!isEmptyString(text)){
			param = ['SEARCH_TYPE','FREE','FREE_TEXT',text,'LanguageCode',langCode];
		}else{
			param = ['SEARCH_TYPE','FREE','LanguageCode',langCode];
		}
		return param;
	};

	var getSearchResultforFirstTimeFromSearch = function(params,text,andOrLogic,boolValue){
        setMessagesToNull();
        if(Array.isArray(params) && params.length>0){
               $scope.pageKey = Util.initPagination('solrSearchResultUrl',params);
               Util.appendItemsToList(["PageNo",1], params);
               ProdSearchService.getPrdSearchResult(params).then(function(data){
            	   if(!isNull(data.messageCode)){
            		   setMessages(data);
            	   }else{
            		   if(!isNull(boolValue)){
            			   //$state.go(PROD_SEARCH_RESULT_VIEW,{FREE_TEXT:text,AND_OR:andOrLogic,BOOL_VALUE:boolValue});
            			   setupDataForUI(data,true);
	           			}else{
	           				$state.go(PROD_SEARCH_RESULT_VIEW,{FREE_TEXT:text,AND_OR:andOrLogic});
	           			}
            	   }
               });
        }
 };	

	
	function removeAllSelectedFiltersAdvanceFromFilterList(list) {
		for(var i = 0; i < list.length; i++) {
			list[i].removeAllSelectedFiltersAdvanceSearch();
		}
		removeAllAdvanceFilters();
	}
	
	function removeAllSelectedFiltersFromFilterList(list) {
		for(var i = 0; i < list.length; i++) {
			list[i].removeAllSelectedFilters();
		}
	}
	
	$rootScope.priceCalculationOnUnitChange = function(ic, su, it) {
		//$scope.onUnitChange(ic, su, it);
	};
	
	$scope.onUnitChange= function(itemCode,unitCode,oldCatalogItem){
		$scope.getAndUpdateCatalogItem(itemCode,unitCode,oldCatalogItem);
	};

	$scope.getAndUpdateCatalogItem = function(itemCode,unitCode,oldCatalogItem){
		if(!isEmptyString(itemCode) && !isEmptyString(unitCode) ){
			var prams = ["ITEM_CODE",itemCode,"UNIT_CODE",unitCode,"Page","ItemSearchPage"];
			if($state.current.name!='home.prodSearchResult') {
				prams.pop(); prams.pop();
			}
			var deferred = $q.defer();
			Util.getDataFromServer('getSingleItemDetailUrl',prams,false).then(function(data){
				if(!isNull(data) && Array.isArray(data.solrItemSearchListBean) && data.solrItemSearchListBean.length > 0){
					newCatalogItem = data.solrItemSearchListBean[0];
					if(angular.isDefined(oldCatalogItem.checked) && oldCatalogItem.checked!=null){
						newCatalogItem['checked']=oldCatalogItem.checked;
					}
					copyObjectWithNull(newCatalogItem,oldCatalogItem);
					//copyConsideringSearch(oldCatalogItem, newCatalogItem);
					
					setPrdSearchExtendedData([oldCatalogItem]);					
					oldCatalogItem.selectedUnitObject = getSelectedSalesUnit(newCatalogItem.salesUnit,newCatalogItem.salesUnitsDesc);
					oldCatalogItem.selectedUnit = oldCatalogItem.selectedUnitObject.salesUnit;		
				}
				deferred.resolve(data);	
			});
		}
	};
//	$scope.freeTextSearch();
//	prdSearchInit();
//	makePrdSearchFilters();
	prodSearchCtrlInit();
	$scope.$root.$eval();
}]);
//

