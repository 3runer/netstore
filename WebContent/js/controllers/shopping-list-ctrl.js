catalougeControllers.controller('shoppingListCtrl', ['$rootScope','$scope','$stateParams', '$http','$log', '$q','DataSharingService','ShoppingListService', 'Util','$state',
                                                function ($rootScope,$scope,$stateParams, $http,$log, $q,DataSharingService,ShoppingListService,Util,$state) {

	var shoppingListCtrlInit = function(){
		historyManager.isBackAction = false;
		if(historyManager.isBackAction){
			historyManager.loadScopeWithCache($scope);
			$scope.$root.$eval();
		}else{
			initController();
		}
	};
	
	$scope.$on('$destroy', function(event){ 
		historyManager.pushScopeObject($scope);
		//$log.log(" shoppingListCtrl history manager " + JSON.stringify(historyManager.stateCacheList));
	});
	
	var initController = function()
	{ 
		$scope.filterOn = false;
		$rootScope.initCommitsSecondNextPromise.then(function(){
			
			$scope.selectedShoppingList = {};
			$scope.selectedShoppingList.selected = false;
			$scope.ShoppingList =  new MyShoppingList();
			makeShoppingListFilters();			
			$scope.cachedPaginationObj = null;
	 		$scope.ShoppingList.sort.ASC('ListId');
	 		var params = $scope.ShoppingList.getSearchParams(); 
	 		getShoppingListPageData(params,FirstPage);
	 		$scope.listMessage = Util.translate('TXT_SL_001_01',null)+'<img src="images/ShoppingListLock.png"/>'+Util.translate('TXT_SL_001_02',null)+Util.translate('TXT_OE_002_03',null);	 		
			
			$scope.ShoppingList.setNewList();
			
		});
		};
	
	// fetching the shopping list service data
	var getShoppingListData = function(requestParam,isFilterOn){
//	
	};
	$scope.showShoppingListDetails = function(list){	
		//DataSharingService.setObject("listInfo",list);
		DataSharingService.setToSessionStorage("listInfo",list);
		
		$state.go('home.shoppinglist.details');
	};
	
	$scope.deleteShoppingList = function(list){
			var params = ["ListId",list.listId ,"ListOwnerCode",list.listOwner,"CustomerCode",list.customerId];	
			
			var msg = Util.translate('CON_DELETE_SHOPPING_LIST',null);
			var mode = $rootScope.openConfirmDlg(msg);
			mode.result.then(function () {
				//console.log('Modal Ok clicked');	
				ShoppingListService.deleteShoppingList(params).then(function(data){
					if(!isNull(data)){						
						$scope.ShoppingList.updateMsg(data);
					}
					var refreshList = true;
					var param = $scope.ShoppingList.getSearchParams(); 
			 		getShoppingListPageData(param,FirstPage,refreshList);
					//getShoppingListData([],$scope.filterOn);
				});		
			}, function () {
				//console.log('Modal dismissed');
			});
	};
	
	$scope.makeShoppingListDefault = function(list){
		var params = ["ListId",list.listId ,"ListOwnerCode",list.listOwner,"CustomerCode",list.customerId];	
		var defaultList = {};
			ShoppingListService.makeShoppingListDefault(params).then(function(data){
				if(!isNull(data)){						
//					$scope.ShoppingList.updateMsg(data);
					if(!isNull(data.messageCode) && data.messageCode==2263){						
						$scope.selectedShoppingList['list']= list;
						$scope.selectedShoppingList.selected = true;
						var translatedMsgList = [];
						translatedMsgList.push(Util.translate('CON_DEFAULTLIST_SUCCESS'));
						$rootScope.openMsgDlg("",translatedMsgList);
					}else{
						defaultList = $scope.ShoppingList.getDefaultList();
						$scope.selectedShoppingList['list']= defaultList;
						$scope.selectedShoppingList.selected = checkIfPageHasDefaultListRecord(data.listSearchBean,defaultList);
						//$scope.selectedShoppingList.selected = true;
					}
					$scope.ShoppingList.updateMsg(data);
				}
			});		
     };
     
     $scope.createNewShoppingList = function(list){
    	 var isSharedList = 'NO';
    	 if(!isNull($rootScope.webSettings.sessionDefaultCustomerCode) && $rootScope.webSettings.sessionDefaultCustomerCode !=""){
    	 var params = ["ListId",list.listId,"ListDesc",list.listDesc,"CustomerId",$rootScope.webSettings.sessionDefaultCustomerCode,"ListOwnerCode",$rootScope.webSettings.selectedListOwner];
    	 }else{
    		 var params = ["ListId",list.listId,"ListDesc",list.listDesc,"CustomerId",$rootScope.webSettings.defaultCustomerCodes,"ListOwnerCode",$rootScope.webSettings.selectedListOwner];
    	 }
    	 if($rootScope.webSettings.allowedSharedList =='Y')
    	 {
    		 if(list.checked){
    			 isSharedList='YES';
    		 }
    		 params.push('Shared');
    		 params.push(isSharedList);
    	 }
    	 ShoppingListService.createNewShoppingList(params).then(function(data){
    		if(!isNull(data)){		
    			 
    			 $rootScope.currentList=list.listId;
    			 /*if(!(data.messageCode==2250)){
    				 $rootScope.populateShoppingListCount(0);
    			 }*/
    			 $scope.ShoppingList.updateMsg(data,list);
    			 
    		 }
    			var param = $scope.ShoppingList.getSearchParams(); 
		 		getShoppingListPageData(param,FirstPage);
    		// getShoppingListData([],$scope.filterOn);

    	 });		
     };

	// sorting 
     $scope.sortASC = function(column){
 		$scope.ShoppingList.sort.ASC(column);
 		sorting();
 	};
 	
 	$scope.sortDESC = function(column){
 		$scope.ShoppingList.sort.DESC(column);
 		sorting();
 	};
 	
 	var sorting = function(){
 		var params = $scope.ShoppingList.getSearchParams();		
 		getShoppingListPageData(params,FirstPage);
 	};
     

	var makeShoppingListFilters = function(){
		var finalFilterList = new FilterList();
		var staticFilterConfig = ShoppingListService.getFilterConfiguration();
		var filterDataList = DataSharingService.getFilterDetailsSettings();
		
		var filterExist = false;
		if(isNull(filterDataList.customerList)){
			filterExist = false;
		}else{
			filterExist = true;
		}
		
		
		if(filterExist ==  true){
		var dynamicFilterConfig = null;
		ShoppingListService.getShoppingListFilters().then(function(data){
				dynamicFilterConfig = data.filterBeans;
				finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
				$scope.shoppingListFilters = finalFilterList;
				$scope.ShoppingList.setFilterList(finalFilterList);
				//console.log("shoppingListFilters : " + JSON.stringify(finalFilterList));
		});
		}else{
			Util.getFilterListDetails([]).then(function(data){
				DataSharingService.setFilterDetailsSettings(data);
				filterDataList = DataSharingService.getFilterDetailsSettings();
				var dynamicFilterConfig = null;
				ShoppingListService.getShoppingListFilters().then(function(data){
					dynamicFilterConfig = data.filterBeans;
					finalFilterList.createFilterList(staticFilterConfig,filterDataList,dynamicFilterConfig);
					$scope.shoppingListFilters = finalFilterList;
					$scope.ShoppingList.setFilterList(finalFilterList);
					//console.log("shoppingListFilters : " + JSON.stringify(finalFilterList));
			});
			});
		}
	};
	
	
	
	//applying filter 

	$scope.submitFilter = function(){
		$scope.ShoppingList.isfilterOn = true;
		$scope.filterOn = true;
		var params = $scope.ShoppingList.getSearchParams();		
		getShoppingListPageData(params,FirstPage);
	};
	
	var getShoppingListPageData = function(requestParams,page,refreshList)
	{
//		
		$scope.paginationObj = $rootScope.getPaginationObject('getShoppingListUrl',requestParams,$scope.pageKey);
		$scope.pageKey = $scope.paginationObj.objKey;
		if(!Util.isPaginationBlocked($scope.pageKey))
		{
			Util.getPageinationData('getShoppingListUrl',requestParams,page ).then(function(data){
				if($scope.paginationObj.getPageNo()==1){
					setUIPageData(data,true,refreshList);
				}else{
					setUIPageData(data,false,refreshList);
				}
			});
		}
//	
	};
	
	var setUIPageData = function(data,isFirstPageData,refreshList){
		
		var 	ItemList = data.listSearchBean;
		if(isNull(data) ||  data.messageCode=='111'){
			if(refreshList){
				$scope.ShoppingList.setItemList([],data,isFirstPageData,refreshList); 
			}
			else{
				$scope.ShoppingList.setItemList(null,data,isFirstPageData);

				if(isFirstPageData){
					$rootScope.applyPreviousPaginationObject('getShoppingListUrl' , $scope);
				}
				
			}
			
			
		}else{
			addpropertyToList(ItemList,'validQuantity',true);		
			$scope.ShoppingList.setItemList(ItemList,data,isFirstPageData); 
			$scope.moreDataArrow = data.moreRecords;
			$rootScope.cachePaginationObject('getShoppingListUrl' , ItemList, $scope);
			if(isFirstPageData && (! Array.isArray(ItemList) ||  !ItemList.length > 0 ) ){
				$rootScope.applyPreviousPaginationObject('getShoppingListUrl' , $scope);
			}
			
		}
		
		var defaultList = $scope.ShoppingList.getDefaultList();
		$scope.selectedShoppingList['list']=defaultList;
		$scope.selectedShoppingList.selected = checkIfPageHasDefaultListRecord(ItemList,defaultList);
		$rootScope.setPageLoadMsg(ItemList,isFirstPageData,$scope);
		$rootScope.scrollToId("result_set");
		$scope.$root.$eval();
	};
	
	var checkIfPageHasDefaultListRecord = function(pageList,defaultList)
	{
		var defaultListRecordFound = false;
		if(!isNull(pageList) && !isNull(defaultList))
		{
			for(var i = 0; i<pageList.length ; i++){
				
				if(pageList[i].listId == defaultList.listId)
					{
						defaultListRecordFound = true;
						break;
					}

			}
		}

		return defaultListRecordFound;
	};
   $scope.previousPage = function(){		
	   var params = $scope.ShoppingList.getSearchParams();		
	
		getShoppingListPageData(params,PrevPage);		
	};
	
	$scope.nextPage = function(){		
		  var params = $scope.ShoppingList.getSearchParams();	
		getShoppingListPageData(params,NextPage);		
	};
			
	$scope.getFirstPage = function(){	
		  var params = $scope.ShoppingList.getSearchParams();	
		getShoppingListPageData(params,FirstPage);
				
	};
	
	
	shoppingListCtrlInit();
}]);