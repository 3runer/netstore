var DlgCtrl = function ($rootScope, $scope ,$modal, $log,$sce) {

	var dlgObj= {};
	$rootScope.openMsgDlg = function (dlgTitle, messageList) {
		dlgObj['dlgTitle'] = dlgTitle;
		dlgObj['messageList'] = strArrToObjArr(messageList);
		var modalInstance = $modal.open({
			templateUrl: 'messageDlgView.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});
		return modalInstance;
	};

	$rootScope.openDeletedOrderLineMsgDlg = function (dlgTitle, messageList) {
		//var dlgObj = {};
		dlgObj['dlgTitle'] = dlgTitle;
		dlgObj['orderMessageList'] = messageList;
		var modalInstance = $modal.open({
			templateUrl: 'cartDeletedOrderMessage.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});
		return modalInstance;
	};
	//console.log("------------------------ registering Dlg --------------------");
	$rootScope.openWelComeMsgDlg = function (dlgTitle, messageList) {
		////console.log("------------------------ welcome to display openWelComeMsgDlg --------------------");
		if(!isNull(webSettings) && webSettings.showWelcomeText){
		dlgObj['dlgTitle'] = dlgTitle;
		//--dlgObj['messageList'] = strArrToObjArr(messageList);
		var modalInstance = $modal.open({
			templateUrl: 'welcomeMessage.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});
		return modalInstance;}
	};

	$rootScope.printDoc = function(div){
		printDocument(div);
	};
	
	$rootScope.renderHtml = function(htmlCode) {
        return $sce.trustAsHtml(htmlCode);
    };
	$rootScope.openOrderViewDlg = function (dlgTitle,order) {
		dlgObj['dlgTitle'] = dlgTitle;
		dlgObj['orderInfo'] = order.orderInfo;
		var modalInstance = $modal.open({
			templateUrl: 'viewPrintOrderWrapper.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});

//		modalInstance.print = function(div){
//			 printDocument(div);
//		};

		return modalInstance;
	};
	
	$rootScope.openPriceCalcDlg = function (dlgTitle,priceCalObj) {

		dlgObj['dlgTitle'] = dlgTitle;
		dlgObj['priceCalObj'] = priceCalObj;
		var modalInstance = $modal.open({
			templateUrl: 'priceCalc.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});

		modalInstance.getPriceCalObject = function(){
			return dlgObj.priceCalObj;
		};

		return modalInstance;
	};
	
	//quickView Dialog start 
	
	$rootScope.openQuickViewDlg = function (dlgTitle,quickViewObj1) {

		dlgObj['dlgTitle'] = dlgTitle;
		dlgObj['quickViewObj1'] = quickViewObj1;
		var modalInstance = $modal.open({
			templateUrl: 'quickViewProduct.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});

		modalInstance.getPriceCalObject1 = function(){
			return dlgObj.quickViewObj1;
		};

		return modalInstance;
	};
	
	//quickView dialog ends
	
	//ADM-020 - START
	$rootScope.openProcureQuickViewDlg = function (dlgTitle,quickViewObj1) {
		dlgObj['dlgTitle'] = dlgTitle;
		dlgObj['quickViewObj1'] = quickViewObj1;
		var modalInstance = $modal.open({
			templateUrl: 'quickViewProcure.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});
		modalInstance.getPriceCalObject1 = function(){
			return dlgObj.quickViewObj1;
		};
		return modalInstance;
	};
	//ADM-020 - END
	
	
	//quickView Matrix Dialog Start 
	
	$rootScope.openQuickViewMatrixDlg = function (dlgTitle,quickViewMatrixObj1) {

		dlgObj['dlgTitle'] = dlgTitle;
		dlgObj['quickViewMatrixObj1'] = quickViewMatrixObj1;
		var modalInstance = $modal.open({
			templateUrl: 'quickViewMatrixProduct.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});

		modalInstance.getMatrixPriceCalObject1 = function(){
			return dlgObj.quickViewMatrixObj1;
		};

		return modalInstance;
	};
	
	//quickView Matrix Dialog Ends

	$rootScope.openCreateDefaultListDlg = function (dlgTitle,ShoppingListObject,items, params,buyObj,quantityPropertyName,fromQuotation) {
		dlgObj['dlgTitle'] = dlgTitle;
		dlgObj['ShoppingList'] = ShoppingListObject;
		dlgObj['item'] = items;
		dlgObj['params'] = params;
		dlgObj['buyObj'] = buyObj;
		dlgObj['quantityPropName'] = quantityPropertyName;
		dlgObj['fromQuotation'] = returnBlankIfNull(fromQuotation);
		var modalInstance = $modal.open({
			templateUrl: 'createNewList.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});
		modalInstance.getShoppingListObject = function(){
			return dlgObj.ShoppingListObject;
		};

		return modalInstance;
	};
	
	$rootScope.openConfirmDlg = function (dlgTitle, messageList) {
		dlgObj['dlgTitle'] = dlgTitle;
		dlgObj['messageList'] = messageList;
		var modalInstance = $modal.open({
			templateUrl: 'confrimDlgView.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});
		return modalInstance;
	};
	
	
	$rootScope.openQuantityZeroDlg = function (dlgTitle, messageList) {
		dlgObj['dlgTitle'] = dlgTitle;
		dlgObj['messageList'] = messageList;
		var modalInstance = $modal.open({
			templateUrl: 'quantityZeroView.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});
		return modalInstance;
	};
	
	

	$rootScope.openEnterMOT = function (dlgTitle, messageList) {
		dlgObj['dlgTitle'] = dlgTitle;
		dlgObj['messageList'] = messageList;
		var modalInstance = $modal.open({
			templateUrl: 'mannerOfTrans.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});
		return modalInstance;
	};
	
	$rootScope.openEmailDlg = function (dlgTitle,email) {
		
		dlgObj['dlgTitle'] = dlgTitle;
		dlgObj['email'] = email;
		var modalInstance = $modal.open({
			templateUrl: 'emailForm.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});
		
		modalInstance.getEmailObject = function(){
			return dlgObj.email;
		};
		
		return modalInstance;
	};
	
	$rootScope.openForgotPwdDlg = function (dlgTitle,forgotPwdObj) {

		dlgObj['dlgTitle'] = dlgTitle;
		dlgObj['forgotPwd'] = forgotPwdObj;
		var modalInstance = $modal.open({
			templateUrl: 'forgotPassword.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});

		modalInstance.getForgotPwdObject = function(){
			return dlgObj.forgotPwd;
		};

		return modalInstance;
	};
	
	$rootScope.openImageDlg = function (image,index) {
		dlgObj['dlgTitle'] = "";
		dlgObj['messageList'] = {};
		dlgObj['imageList'] =  image;
		//console.log("image " + JSON.stringify(image));
		var modalInstance = $modal.open({
			templateUrl: 'imageSlider.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});
		
		dlgObj.direction = 'left';
		
		if(!isNull(index) && angular.isNumber(index)){
			dlgObj.currentIndex = index;
		}else{
			dlgObj.currentIndex = 0;
		}

		dlgObj.setCurrentSlideIndex = function (index) {
			dlgObj.direction = (index > dlgObj.currentIndex) ? 'left' : 'right';
			dlgObj.currentIndex = index;
        };

        dlgObj.isCurrentSlideIndex = function (index) {
            return dlgObj.currentIndex === index;
        };

        dlgObj.prevSlide = function () {
        	dlgObj.direction = 'left';
        	dlgObj.currentIndex = (dlgObj.currentIndex < dlgObj.imageList.length - 1) ? ++dlgObj.currentIndex : 0;
        };

        dlgObj.nextSlide = function () {
        	dlgObj.direction = 'right';
        	dlgObj.currentIndex = (dlgObj.currentIndex > 0) ? --dlgObj.currentIndex : dlgObj.imageList.length - 1;
        };

        return modalInstance;
	};

	$rootScope.openNewAccountDlg = function (dlgTitle,newAccObj) {
		
		$rootScope.closeNewAccDlgOnsuccess = function(){
			if(!isNull($rootScope.newAccountDlg)){
				$scope.newAccountDlg.close('result');
			}
			//console.log("new accunt dlg closeOnsuccess() called");
		};
		
		newAccObj.setSuccessCallBack($rootScope.closeNewAccDlgOnsuccess);
		
		dlgObj['dlgTitle'] = dlgTitle;
		dlgObj['newAccObj'] = newAccObj;
		
		$rootScope.newAccountDlg = $modal.open({
			backdrop: 'static',
			templateUrl: 'newAccountView.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});

		$rootScope.newAccountDlg.getNewAccObject = function(){
			return dlgObj.newAccObj;
		};

		return $rootScope.newAccountDlg;
	};
	
	$rootScope.openChangePwdDlg = function (dlgTitle,changePwdObj) {
		
		$rootScope.closeChangePwdDlgOnsuccess = function(){
			if(!isNull($rootScope.changePwdDlg)){
				try{
				$scope.changePwdDlg.close('result');
				}catch(err){}
			}
			//console.log("change pwd dlg closeOnsuccess() called");
		};
		
		changePwdObj.setSuccessCallBack($rootScope.closeChangePwdDlgOnsuccess);
		
		dlgObj['dlgTitle'] = "";
		dlgObj['changePwdObj'] = changePwdObj;
		
		$rootScope.changePwdDlg = $modal.open({
			backdrop: 'static',
			templateUrl: 'changePassword.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});

		return $rootScope.changePwdDlg;
	};
	
	$rootScope.openSelectCustomerDlg = function (dlgTitle,selectCustObj) {
		
		$rootScope.closeSelectCustomerDlgOnsuccess = function(){
			if(!isNull($rootScope.selectCustomerDlg)){
				try{
				$scope.selectCustomerDlg.close('result');
				}catch(err){

				}
			}
			//console.log("select customer dlg closeOnsuccess() called");
		};
		
		//selectCustObj.setSuccessCallBack($rootScope.closeSelectCustomerDlgOnsuccess);
		
		dlgObj['dlgTitle'] = "";
		dlgObj['selectCustObj'] = selectCustObj;
		
		$rootScope.selectCustomerDlg = $modal.open({
			backdrop: 'static',
			templateUrl: 'selectCustomer.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});

		return $rootScope.selectCustomerDlg;
	};
	
	
	// ADM-015
	$rootScope.openNotAvailableDlg = function (dlgTitle, notAvailableObj) {

		if($rootScope.webSettings.allowSelectUnitOfMeasure){
			if(!isNull(notAvailableObj.salesPackageQty) && notAvailableObj.salesPackageQty > 1){
				if(notAvailableObj.selectedUnit == "CF"){
					notAvailableObj.availableQuantity = Math.floor(notAvailableObj.availableQuantity / notAvailableObj.salesPackageQty);
				}else{
					notAvailableObj.availableQuantity = notAvailableObj.availableQuantity * notAvailableObj.salesPackageQty;
				}
			}
		}
		dlgObj['dlgTitle'] = dlgTitle;
		dlgObj['notAvailableObj'] = notAvailableObj;
			
		var modalInstance = $modal.open({
			templateUrl: 'notAvailableDlgView.html',
			controller: DlgInstanceCtrl,
			resolve: {
				dlgObj: function () {
					return dlgObj;
				}
			}
		});
		
		modalInstance.getNotAvailablePriceCalcObject1 = function() {
			return dlgObj.notAvailableObj;
		};
		
		return modalInstance;
	};
};


var DlgInstanceCtrl = function ($rootScope, $scope, $modalInstance,dlgObj,$location) {

	$scope.dlgObj = dlgObj;
//	$scope.selected = {
//	item: $scope.items[0]
//	};
	$rootScope.$on('$locationChangeStart', function() {
		$modalInstance.dismiss('cancel');
	});
	
	$scope.ok = function () {
		$modalInstance.close();
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
		$rootScope.feedbackPerDialog="";
	};
	
	$scope.removeEmailWelcome = function () {
		$rootScope.removeSendEmailLogin();
		$rootScope.removeWelComeMsg();
	};
	$scope.cancelMotEnter = function () {
		
		$modalInstance.dismiss('cancel');
		$rootScope.showDeliveryPaymentInfoPopUp();
		
	};
};