netStoreApp.animation('.slide-animation', function () {
        return {
            beforeAddClass: function (element, className, done) {
                var scope = element.scope();

                if (className == 'ng-hide') {
                    var finishPoint = element.parent().width();
                    if(scope.direction !== 'right') {
                        finishPoint = -finishPoint;
                  }

                }
                else {
                    done();
                }
            },
            removeClass: function (element, className, done) {
                var scope = element.scope();

                if (className == 'ng-hide') {
                    element.removeClass('ng-hide');

                    var startPoint = element.parent().width();
                    if(scope.direction === 'right') {
                        startPoint = -startPoint;
                    }
                }
                else {
                    done();
                }
            }
        };
    });

/*
 * Translate util to perform certain translations on objects
 */

function Translate (){
	this.translateFn = null;
	this.sanitizer = null;
};

Translate.prototype.setTranslationFunc= function (translateFunction){
	this.translateFn = translateFunction;
};

Translate.prototype.setSanitizerObj= function (sanitizerObj){
	this.sanitizer = sanitizerObj;
};

Translate.prototype.getTranslation= function (key,param){
	if(!isNull(this.translateFn)){
		return this.translateFn(key,param);
	}else{
		return "";
	}
};

Translate.prototype.getTranslationWithTags= function (key,param){
	var val="";
	if(!isNull(this.translateFn)){
		val = this.translateFn(key,param);
		if(!isNull(this.sanitizer)){
			val = this.sanitizer.trustAsHtml(val);
		}
	}
	return val;
};

/*
 * multiple utilites methods
 */

var isNull = function(xvar){
	if((typeof xvar == 'undefined' ) || xvar ==null){
		return true;
	}else{
		return false;
	}
};	

var isEmptyObject=function(obj){
	if((typeof obj=='undefined')|| obj==null || (typeof obj!='object')){
		return true;
	}else {
		var i = 0;
		for(var key in obj){
			++i;
		}
		if(i==0){
			return true;
		}else {
			return false;
		}
	}
};

var changeQuantityValueToInt=function(objList){
	for(var i=0;i<objList.length;i++){
		var item=objList[i];
		var val = parseFloat(item['quantity']);
		if(!isNaN(val)){
			item['quantity']=val.toString();
		}
	}
};

var ChangeToInteger=function(obj){
	var intVal = null;
	if(!isNull(obj)){
		var val = parseFloat(obj);
		if(!isNaN(val)){
			intVal=val.toString();
		}else{
			//if NaN , make default zero 
			intVal = "0";
		}
	}else{
		intVal = "0";
	}
	return intVal;
};

var isValidQuantity = function(val, maxQuantity, skipInteger){
	val=val+"";
	//var pattern = /^(0|[1-9][0-9]{0,2}(?:(,[0-9]{3})*|[0-9]*))(\.[0-9]+){0,1}$/;
	var pattern = /^[0-9.,]*$/;
	if(!isEmptyString(val) && pattern.test(val)){

		if(val<=0 || val> maxQuantity ){
			return false;
		}else {
			return true;
		}
	}else{
		if(isNaN(val)){
			return false;
		}else if(val.indexOf('-') >=0){
			return false;
		}else if(val == ""){
			return false;
		}else{
			return true;
		}
	}
};

var isValidQuantityQuickInfo = function(val, maxQuantity){
	val=val+"";
	if(!isEmptyString(val)){
		if(isNaN(val) ||  !isInteger(val) || val<=0 || val> maxQuantity ){
			return false;
		}else {
			return true;
		}
	}else{
		
		return false;
	}
};

var isValidQuantityForBuy = function(val){
	val=val+"";
	if(!isEmptyString(val)){
		if(isNaN(val) ||  !isInteger(val) || val<=0){
			return false;
		}else {
			return true;
		}
	}else{
		return true;
	}
};


var isValidDecimalsBuy = function(itemObj, itemQty,allowDecimalsForQuantities ) {
	var flag = true;
	if(allowDecimalsForQuantities) {
		var reqNumDecimalPlaces = null;
		
		var suosu;
		if(!isNull(itemObj.selectedUnitObject) && !isNull(itemObj.selectedUnitObject.salesUnit)) {
			suosu = itemObj.selectedUnitObject.salesUnit;
		} else if(!isNull(itemObj.unit)) {
			suosu = itemObj.unit;
		} else if(!isNull(itemObj.unit_code)) {
			suosu = itemObj.unit_code;
		} else if(!isNull(itemObj.ivOrderLine) && !isNull(itemObj.ivOrderLine.unitCode)) {
			suosu = itemObj.ivOrderLine.unitCode;
		}
		
		
		var su;
		if(!isNull(itemObj.salesUnits)) {
			su = itemObj.salesUnits;
		} else if(!isNull(itemObj.itemUnits)) {
			su = itemObj.itemUnits;
		} else if(!isNull(itemObj.unitCodes)) {
			su = itemObj.unitCodes;
		} else if(!isNull(itemObj.unitList)) {
			su = itemObj.unitList;
		} else if(!isNull(itemObj.activeUnitList)) {
			su = itemObj.activeUnitList;
		} else if(!isNull(itemObj.prdBaseDetail) && !isNull(itemObj.prdBaseDetail.unitCodes)) {
			su = itemObj.prdBaseDetail.unitCodes;
		} else if(!isNull(itemObj.ivOrderLine) && !isNull(itemObj.ivOrderLine.item) && !isNull(itemObj.ivOrderLine.item.salesUnits)) {
			su = itemObj.ivOrderLine.item.salesUnits;
		}
		
		var inoda;
		if(!isNull(itemObj.itemNumberOfDecimalsAllowed)) {
			inoda = itemObj.itemNumberOfDecimalsAllowed;
		} else if(!isNull(itemObj.prdBaseDetail) && !isNull(itemObj.prdBaseDetail.itemNumberOfDecimalsAllowed)) {
			inoda = itemObj.prdBaseDetail.itemNumberOfDecimalsAllowed;
		} else if(!isNull(itemObj.ivOrderLine) && !isNull(itemObj.ivOrderLine.item) && !isNull(itemObj.ivOrderLine.item.itemNumberOfDecimalsAllowed)) {
			inoda = itemObj.ivOrderLine.item.itemNumberOfDecimalsAllowed;
		}
		if(!isNull(itemObj) && !isNull(suosu) && !isNull(su) && !isNull(inoda) && (su.length == inoda.length)) {
			// test code below
			//itemObj.itemNumberOfDecimalsAllowed = [2];
			// test code above
			for(var i = 0; i < su.length; i++) {
				if(suosu == su[i]) {
					reqNumDecimalPlaces = inoda[i];
					break;
				}
			}
		}
		var temp = (typeof itemQty == "string") ? itemQty : (itemQty + ""),temp2;
		if(!isNull(reqNumDecimalPlaces) && !isNaN(reqNumDecimalPlaces)) {
			if(temp.indexOf(',') > 0){
				temp2 = temp.split(',');
			}else{
				temp2 = temp.split(".");
			}
			if(temp2.length == 2) {
				var temp3 = temp2[1];
				if(temp3.length > reqNumDecimalPlaces || temp3.length <= 0) {
					flag = false;
				}
			}else if(temp2.length >2){flag = false;}
		}else{
			if((temp.indexOf(',') > 0) || (temp.indexOf('.') > 0)){
				if(allowDecimalsForQuantities){
					flag = true;
				}else{
					flag = false;
				}
			}
		}
	}else{
		var temp = (typeof itemQty == "string") ? itemQty : (itemQty + ""),temp2;
		if((temp.indexOf(',') > 0) || (temp.indexOf('.') > 0)){
			flag = false;
		}else{
			flag = true;
		}
	}
	
	return flag;
	
	
};

function isInteger(value){ 
	var re = /^[0-9]+$/;
	 return re.test(value); 
};


function isValidDelimeter(value){
	var re = /^[A-Za-z0-9]+$/;
	var isValid = true; 
	if((value.length >1) ||(re.test(value)) ){
		isValid =  false;}
	return isValid;
};

var addpropertyToList = function(list,property,value){
	if(!isNull(list)){
		for(var i = 0; i<list.length ; i++){
			list[i][property]=value;
		};
	}
};
var isEqualStrings = function (str1, str2, isIgnoreCase){
	var isEqual = false;
	if(!isNull(str1) && !isNull(str2) && (typeof str1 == 'string') && (typeof str2 == 'string')){
		if(isIgnoreCase){
			if(str1.toUpperCase() == str2.toUpperCase()){
				isEqual = true;
			} 
		}else{
			if(str1 == str2){
				isEqual = true;
			} 
		}
	}
	return isEqual;
};
function replaceQuotes(str){
	if(typeof str == "string") {
		var rStr = str.replace(/"/g, '\\"');
		return rStr;
	} else {
		return str;
	}
};

function returnBlankIfNull(str){
	if(isNull(str)){
		return "";
	}else{
		str = replaceQuotes(str);
		return str;
	}
}
function returnZeroIfNull(str){
	if(isNull(str)){
		return "0";
	}else{
		return str;
	}
}

function returnBlankIfObjectNull(obj,key){
	if(isNull(obj)){
		return "";
	}else{
		return obj[key+''];
	}
}
var isEmptyString = function (str1){
	var isEmpty = true;
	if(!isNull(str1) && (typeof str1 == 'string') && str1.length>0 ){
		isEmpty = false;
	}
	return isEmpty;
};

function isValueTrue (value){
	var bool = false;
	if(!isNull(value)){
		if(value==true || isEqualStrings(value,'true',true)){
			bool = true;
		};
	}
	return bool;
};

function appendItemsToList(fromList, toList){
	if(isNull(toList)){
		toList = [];
	}
	if(!isNull(fromList)){
		for(var i = 0; i<fromList.length ; i++){
			toList.push(fromList[i]);
		};
	}
};

var prependZero= function(num){
	var newStr = '';
	num = num+"";
	if(!isEmptyString(num)){
		if(num.length>0 && num.length<2){
			newStr="0"+num;
		}else{
			newStr = num; 
		}
	}
	return newStr ;
};

var getCurrentDateTimeStr = function (){
	var date = new Date();
	var dateStr = '';
	//dateStr = date.getFullYear()+""+(date.getMonth()+1) +""+ date.getDate()+"_"+date.getHours()+""+date.getMinutes()+""+date.getSeconds();
	dateStr = date.getFullYear()+prependZero((date.getMonth()+1)) +prependZero(date.getDate())+"_"+
		prependZero(date.getHours())+prependZero(date.getMinutes())+prependZero(date.getSeconds());
	return dateStr;
};

var getDateStrYYYYMMDD = function (){
	var date = new Date();
	var dateStr = '';
	//dateStr = date.getFullYear()+""+(date.getMonth()+1) +""+ date.getDate()+"_"+date.getHours()+""+date.getMinutes()+""+date.getSeconds();
	dateStr = date.getFullYear()+"-"+prependZero((date.getMonth()+1)) +"-"+prependZero(date.getDate());
	return dateStr;
};

function getCommaSeparatedStrings(list,propertyName){
	var str = "";
	if(!isNull(list)){
		for(var i=0;i<list.length ;i++){
			item = list[i];
			if(i==0){
				if(!isNull(item[propertyName])){
					str = item[propertyName];
				}
			}else{
				if(!isNull(item[propertyName])){
					str = str+", "+item[propertyName];
				}
			}
		}
	}
	return str;
}

/* To remove the keys from an object list */
var replacer =function(objList,keys) {
	
	var newObjList = [];
	for(var i=0;i<objList.length;i++){
		var dup = {};
		var  obj =  objList[i];
		for (key in obj) {
			if (keys.indexOf(key) == -1) {
				dup[key] = obj[key];
			}
		}

		newObjList.push(dup);
	};

	return newObjList;
};

/* To remove the keys from an object */
var replaceObjectKey = function(obj,keys)
{
	 var dup = new Object();		  
	    for (key in obj) {
	        if (keys.indexOf(key) == -1) {
	            dup[key] = obj[key];
	        }
	    }
	return dup;
};

function copyObject(fromObject, toObject){
	if(isNull(toObject)){
		toObject = {};
	}
    for (var key in fromObject) {
        //copy all the fields
    	toObject[key] = fromObject[key];
    }
    return toObject;
};

function copyObjectWithNull(fromObject, toObject){
	//this method copies all properties to toObject, but set null to the existing properies which are not present in fromObject 
	if(isNull(toObject)){
		toObject = {};
	}
	//assign null to all to toObject properties
    for (var key in toObject) {
    	if(key!='$$hashKey'){
    		toObject[key] = null;
    	}
    }
	
    for (var key in fromObject) {
        //copy all the fields
    	toObject[key] = fromObject[key];
    }
    return toObject;
};

function copyConsideringSearch(oldset, newset) {
	
	if(!isNull(oldset.itemDescription)) {
		for(var prop in oldset) {
			if(!isNull(newset[prop])) {
				oldset[prop] = newset[prop];
			}
		}
		oldset.solrItemCode = newset.code;
		oldset.itemDescription = newset.description;
		oldset.itemUnitCode = newset.defaultSalesUnit;
		oldset.itemUnitCodeDesc = newset.defaultSalesUnitDesc;
		oldset.actualPrice = newset.actualPrice;
		oldset.isBuyingAllowed = newset.isBuyingAllowed;
		oldset.isMatrixBaseItem = newset.isMatrixBaseItem;
		oldset.enquiryImage = newset.enquiryImage;
		oldset.salesUnit = newset.salesUnit;
		oldset.salesUnitsDesc = newset.salesUnitsDesc;
		oldset.salesUnits = newset.salesUnits;
		oldset.salesUnitsDesc = newset.salesUnitsDesc;
		oldset.currencyCode = newset.currencyCode;
		oldset.imageUrl = newset.imageUrl;
		oldset.isShowAttentionImage = newset.isShowAttentionImage;
		oldset.isRelative = newset.isRelative;
		oldset.productUnitBeanList = newset.productUnitList;
	} else {
		copyObjectWithNull(newset,oldset);
	}
}

function getObjectFromList(propertyName, value, list){
	var obj = null;
	if(!isNull(list)){
		var tempObj = null;
		for (var i=0; i<list.length;i++){
			tempObj = list[i];
			if(tempObj[propertyName+''] == value){
				obj = tempObj;
				break;
			}
		}
	}
    return obj;
};

var isIE9orLower  = function (){
	if(/MSIE [1-9]\./.test(navigator.userAgent)){
		return true;
	}else{
		return false;
	}
};

function removeDefaultCallBack(responseStr){
	responseObj =  null;
	if(!isEmptyString(responseStr)){
		responseStr = responseStr.trim();
		if(responseStr.indexOf('Default(')==0){
			responseStr = responseStr.replace('Default(',"");
		}
		if(responseStr[responseStr.length-1]==')'){
			responseStr =responseStr.substring(0,responseStr.length-1);
		}
		responseObj = JSON.parse(responseStr);
	}else{
		// same response object 
		responseObj  = responseStr;
	}
	return responseObj;
};

/////
function isInt(n){
	 return /^-?[\d.]+(?:e-?\d+)?$/.test(n); 
};

function setfocus(id)
{
	document.getElementById(id).focus();
};

function deepCopy(oldObject) { 
	  var newObj = null;
	  try {
	  var strValue = JSON.stringify(oldObject);
	  newObj = JSON.parse(strValue);
	  }
	  catch (err) { 
		  console.log(err);
	  } finally {}
	  return newObj;
};

function deepCopyWithFunctions(obj) {
	var copy;

	// Handle the 3 simple types, and null or undefined
	if (isNull(obj) ) return obj;

	// Handle Date
	if (obj instanceof Date) {
		copy = new Date();
		copy.setTime(obj.getTime());
		return copy;
	}
	//Handle Array
	if (obj instanceof Array) {
		var strValue = JSON.stringify(obj);
		return JSON.parse(strValue);
	}
	
	// Handle function
	if (typeof obj  == "function" ) {
		return obj;
	}
	// Handle Object
	if (obj instanceof Object) {
		copy = {};
		for (var attr in obj) {
			//console.log("type :" + attr);
			copy[attr] = deepCopyWithFunctions(obj[attr]);
		}
		return copy;
	}else{
		// unknown types
		try{
			var strValue = JSON.stringify(obj);
			return JSON.parse(strValue);
		}catch (err) { 
			console.log(err);
		} finally {}

	};

};

function isValidEmailAddress(emailAddress) {
	var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if (re.test(emailAddress)){  
	    return true;
	}else{
		return false;
	}
};

function printDocument(divName){
	  var printContents = document.getElementById(divName).innerHTML;
	//  var originalContents = document.body.innerHTML;        
	  var popupWin = window.open('', '_blank', 'width=650,height=400');
	  popupWin.document.open();
	  popupWin.document.write('<html><head><link href="./css/home.css" rel="stylesheet" /></head><body onload="window.print()">' + printContents + '</html>');
	  popupWin.document.close();
};

/**
* converts string array to object array
* @param stringArray - ['str1','str2']
* @returns {Array} - [{"id":1,"str":"str1"},{"id":2,"str":"str2"}]
*/
function strArrToObjArr(stringArray){
	var objArr = [];
	if(Array.isArray(stringArray) && stringArray.length > 0){
		for(var i=0;i<stringArray.length;i++){
			var strObj = {};
			strObj.id = i+1;
			strObj.str = stringArray[i];
			objArr.push(strObj);
		}
	}
	return objArr;
};

function encodeObjectData( object ) {
    
    for( var key in object ) {
        if(object.hasOwnProperty(key) ) {
        	object[key]=encodeURIComponent(object[key]);
        }
    }
    return object;
};

//function getDateFormatRegExpression(dateFormat){
//	//dd/mm/yyyy
//	var dateRegExp = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i;
//
//	switch(dateFormat)
//	{	
//	case 'YYYY-MM-DD' :
//	case 'yyyy-mm-dd' :
//			dateRegExp = /^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/;
//		break;
//	default : 
//		dateRegExp = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i;
//	}
//	return dateRegExp;
//};

//function isValidDateFormat(dateValue, dateFormat){
//	var flag = false;
//	dateFormat = webSettings.defaultDateFormat;
//	
//	var dateExpression = getDateFormatRegExpression(dateFormat);
//	if(!isNull(dateExpression)){
//		flag = dateExpression.test(dateValue);
//	}
//	return flag;
//};

function isValidDateFormat(dateStr,defaultDateFormat){
	var defaultDateObject = getDefaultDateFormatObject(defaultDateFormat);
    var dateToken = dateStr.match(defaultDateObject.dateRegExp);  
    var isValidDate = false;
    if(dateToken!==null){
      var day = +dateToken[defaultDateObject.dayLocation];
      var month =+dateToken[defaultDateObject.monthLocation];
      var year =+dateToken[defaultDateObject.yearLocation];
      var date = new Date(year,month-1,day);
      if(date.getFullYear()===year && date.getMonth()===month-1){
    	  isValidDate = true; 
      }
    }
   return isValidDate;
}



// This function will return the object for default date format to validate
function getDefaultDateFormatObject(defaultDateFormat){
	//var dateFormat = webSettings.defaultDateFormat;		
	var dateSeparator = "-";
	var defaultDateObject = {};
	defaultDateObject.dateRegExp = /^(\d{4})\-(\d{2})\-(\d{2})$/; // yyyy-mm-dd
	defaultDateObject.dayLocation = 3;
	defaultDateObject.monthLocation = 2;
	defaultDateObject.yearLocation = 1;
	switch(defaultDateFormat){	
		case 'YYYY-MM-DD' :
		case 'yyyy-mm-dd' :
				dateSeparator = "-";
				defaultDateObject = getDateObject_YYYYMMDD(defaultDateFormat,dateSeparator);			
				break;
		case 'YYYY/MM/DD' :
		case 'yyyy/mm/dd' :
				dateSeparator = "/";
				defaultDateObject = getDateObject_YYYYMMDD(defaultDateFormat,dateSeparator);			
				break;
		case 'YYYY.MM.DD' :
		case 'yyyy.mm.dd' :
				dateSeparator = ".";
				defaultDateObject = getDateObject_YYYYMMDD(defaultDateFormat,dateSeparator);			
				break;
		case 'DD-MM-YYYY' :
		case 'dd-mm-yyyy' :	
				dateSeparator = "-";
				defaultDateObject = getDateObject_DDMMYYYY(defaultDateFormat,dateSeparator);			
				break;
		case 'DD/MM/YYYY' :
		case 'dd/mm/yyyy' :
				dateSeparator = "/";
				defaultDateObject = getDateObject_DDMMYYYY(defaultDateFormat,dateSeparator);			
				break;
		case 'DD.MM.YYYY' :
		case 'dd.mm.yyyy' :
				dateSeparator = ".";
				defaultDateObject = getDateObject_DDMMYYYY(defaultDateFormat,dateSeparator);			
				break;
		case 'MM-DD-YYYY' :
		case 'mm-dd-yyyy' :
				dateSeparator = "-";
				defaultDateObject = getDateObject_MMDDYYYY(defaultDateFormat,dateSeparator);			
				break;
		case 'MM/DD/YYYY' :
		case 'mm/dd/yyyy' :
					dateSeparator = "/";
					defaultDateObject = getDateObject_MMDDYYYY(defaultDateFormat,dateSeparator);			
					break;
		case 'MM.DD.YYYY' :
		case 'mm.dd.yyyy' :
					dateSeparator = ".";
					defaultDateObject = getDateObject_MMDDYYYY(defaultDateFormat,dateSeparator);			
					break;
		default : 
			defaultDateObject.dateRegExp = /^(\d{4})\-(\d{2})\-(\d{2})$/; // yyyy-mm-dd	
	}
	return defaultDateObject;
};

//Big-endian format  (year, month, day), Ex- yyyy-mm-dd
function getDateObject_YYYYMMDD(dateFormat,dateSeparator){
	var dateObject = {};
	dateObject.dateRegExp = /^(\d{4})\-(\d{2})\-(\d{2})$/; // yyyy-mm-dd
	dateObject.dayLocation = 3;
	dateObject.monthLocation = 2;
	dateObject.yearLocation = 1;
	switch(dateSeparator)
	{	
	case '-' :
			dateObject.dateRegExp = /^(\d{4})\-(\d{2})\-(\d{2})$/; // yyyy-mm-dd						
			break;
	case '/' :
			dateObject.dateRegExp = /^(\d{4})\/(\d{2})\/(\d{2})$/;
			break;
	case '.' :
			dateObject.dateRegExp = /^(\d{4})\.(\d{2})\.(\d{2})$/;
			break;
	default : 
			dateObject.dateRegExp = /^(\d{4})\-(\d{2})\-(\d{2})$/; // yyyy-mm-dd
		
	}
	return dateObject;
}

//Little-endian date format  (day, month,year ), Ex- dd-mm-yyyy
function getDateObject_DDMMYYYY(dateFormat,dateSeparator){
	var dateObject = {};
	dateObject.dateRegExp = /^(\d{2})\-(\d{2})\-(\d{4})$/;	// DD-MM-YYYY
	dateObject.dayLocation = 1;
	dateObject.monthLocation = 2;
	dateObject.yearLocation = 3;
	
	switch(dateSeparator){	
		case '-' :
			dateObject.dateRegExp = /^(\d{2})\-(\d{2})\-(\d{4})$/ ; // DD-MM-YYYY
			break;
		case '/' :
			dateObject.dateRegExp = /^(\d{2})\/(\d{2})\/(\d{4})$/; // DD/MM/YYYY
			break;
		case '.' :
			dateObject.dateRegExp =  /^(\d{2})\.(\d{2})\.(\d{4})$/; // DD.MM.YYYY
			break;
		default : 
			dateObject.dateRegExp = /^(\d{2})\-(\d{2})\-(\d{4})$/; // DD-MM-YYYY				
	}
	return dateObject;
}
//  Middle-endian date format  ( month, day,year), Ex - mm-dd-yyyy
function getDateObject_MMDDYYYY(dateFormat,dateSeparator){
	var dateObject = {};
	dateObject.dateRegExp = /^(\d{2})\-(\d{2})\-(\d{4})$/ ;  // MM/DD/YYYY
	dateObject.dayLocation = 2;
	dateObject.monthLocation = 1;
	dateObject.yearLocation = 3;
	switch(dateSeparator){	
		case '-' :
			dateObject.dateRegExp = /^(\d{2})\-(\d{2})\-(\d{4})$/ ; // MM-DD-YYYY
			break;
		case '/' :
			dateObject.dateRegExp = /^(\d{2})\/(\d{2})\/(\d{4})$/; // MM/DD/YYYY
			break;
		case '.' :
			dateObject.dateRegExp =  /^(\d{2})\.(\d{2})\.(\d{4})$/; // MM.DD.YYYY
			break;
		default : 
			dateObject.dateRegExp = /^(\d{2})\-(\d{2})\-(\d{4})$/ ; // MM-DD-YYYY
			
	}
	return dateObject;
}

function DateValidator(dateFormat){
    this.dateFormat = dateFormat ;
    this.day="";
    this.month ="";
    this.year="";
    this.seprator=null;
    this.dayIndex = 0;
    this.monthIndex = 0 ;
    this.yearIndex = 0 ;
    this.yearLimit = 9999; 
    this.init();
}

DateValidator.prototype.init = function(){
    if(!isNull(this.dateFormat)){
           this.seprator = this.findSeprator();
           this.setIndexs();
    }
};

DateValidator.prototype.findSeprator = function(){
    var sep = null;
    if(!isNull(this.dateFormat)){
           var lookUpSep = ['/', '.' , '-' , ' '];
           for(var i=0;i<lookUpSep.length;i++){
                 var tokens = this.dateFormat.split(lookUpSep[i]);
                 if(tokens.length == 3){
                        sep = lookUpSep[i];
                        break;
                 }
           }
    }
    return sep;
};

DateValidator.prototype.setIndexs = function(){
    if(!isNull(this.seprator)){
           var tokens = this.dateFormat.split(this.seprator);
           if(tokens.length == 3){
                 for (var i=0; i<tokens.length;i++){
                        var token = (tokens[i]).toUpperCase();
                        switch (token){
                        case "DD" :
                        case "D" :
                               this.dayIndex = i;
                               break;
                        case "MM" :
                        case "M" :
                               this.monthIndex = i;
                               break;
                        case "YYYY" :
                        		this.yearLimit = 9999;
                        		this.yearIndex = i;
                                break;
                        case "YY" :
                        case "Y" :
                        		this.yearLimit = 99;
                                this.yearIndex = i;
                               break;
                        } 
                 }
           }
    }
};

DateValidator.prototype.isValidDate = function(dateStr){
    var flag = false;    
    if(!isNull(dateStr) && !isNull(this.seprator)){
             var tokens = dateStr.split(this.seprator);
             var year = tokens[this.yearIndex];
             var month = tokens[this.monthIndex];
             var day = tokens[this.dayIndex];
          var date = new Date(year, month-1,day);
          if(date.getDate()==day && date.getMonth()==month-1 && tokens.length==3 && year<=this.yearLimit){
             flag = true; 
          }
    }
    return flag;
};
function Sort (){
	this.sortColumn = "";
	this.orderType = "";
	this.sortSelection = "";
	this.CONST_ASC = "ASC";
	this.CONST_DESC = "DESC";
};

Sort.prototype.ASC = function(colName){
	this.sortColumn = colName;
	this.orderType = this.CONST_ASC;
	this.sortSelection = this.sortColumn + this.orderType;
};

Sort.prototype.DESC = function(colName){
	this.sortColumn = colName;
	this.orderType = this.CONST_DESC ;
	this.sortSelection = this.sortColumn + this.orderType;
};

Sort.prototype.sorting = function(colName,orderType){
	if(!isEmptyString(orderType) && !isEmptyString(colName)){
		if(isEqualStrings(orderType,this.CONST_ASC,true)){
			this.ASC(colName);
		}
		if(isEqualStrings(orderType,this.CONST_DESC,true)){
			this.DESC(colName);
		}
	}
};

Sort.prototype.getSelection = function(){
	return this.sortSelection;
};

Sort.prototype.getParam = function(){
	var sortParams = [];
	if(!isEmptyString(this.orderType) && !isEmptyString(this.sortColumn)){
		sortParams = ["OrderBy",this.orderType,"SortBy",this.sortColumn];
	}
	return sortParams;
};

Sort.prototype.getParamSortingOrderSort = function(params){
	var sortParams = [];
	var orderTypeSorting = params[0];
	if(orderTypeSorting == 'ReferenceNumber'){
		orderTypeSorting = "Reference";
	}else if(orderTypeSorting == 'YourOrderNumber'||orderTypeSorting == 'YourReference'||orderTypeSorting == 'Customer'){
		orderTypeSorting = "OrderNumber";
		this.orderType = 'DESC';
	}else{
		
	}
	if(!isEmptyString(this.orderType) && !isEmptyString(this.sortColumn)){
		sortParams = ["OrderBy",this.orderType,"SortBy",orderTypeSorting];
	}
	return sortParams;
};

Sort.prototype.getParamSortingInvoiceSort = function(params){
	var sortParams = [];
	var orderTypeSorting = params[0];
	if(orderTypeSorting == 'OrderNumber'){
		orderTypeSorting = "invoiceNumber";
		this.orderType = 'ASC';
	}else if(orderTypeSorting == 'OrderReference'||orderTypeSorting == 'YourReference'||orderTypeSorting == 'YourReference'||orderTypeSorting == 'InvoiceCustomer'
		||orderTypeSorting == 'Customer'){
		orderTypeSorting = "invoiceNumber";
		this.orderType = 'DESC';
	}else{
		//this.orderType = 'DESC';
	}
	if(!isEmptyString(this.orderType) && !isEmptyString(this.sortColumn)){
		sortParams = ["OrderBy",this.orderType,"SortBy",orderTypeSorting];
	}
	return sortParams;
};

Sort.prototype.getParamSorting = function(params){
	var sortParams = [];
	var orderTypeSorting = params[0];
	if(orderTypeSorting == 'RequestNumber'){
		this.orderType = 'ASC';
	}else if(orderTypeSorting == 'Customer'){
		this.orderType = 'DESC';
		orderTypeSorting = 'RequestNumber';
	}else if(orderTypeSorting == 'ReferenceNumber'){
		this.orderType = 'DESC';
		orderTypeSorting = 'Reference';
	}else if(orderTypeSorting == 'YourReference'){
		this.orderType = 'DESC';
		orderTypeSorting = 'YourReference';
	}
	if(!isEmptyString(this.orderType) && !isEmptyString(this.sortColumn)){
		sortParams = ["OrderBy",this.orderType,"SortBy",orderTypeSorting];
	}
	return sortParams;
};

Sort.prototype.addSortParams = function(params){
	if(isNull(params)){
		params =[];
	}
	if(Array.isArray(params)){
		appendItemsToList(this.getParam(),params);
	}
	return params;
};

Sort.prototype.addSortParamsSortingOrderSort = function(params){
	if(isNull(params)){
		params =[];
	}
	if(Array.isArray(params)){
		appendItemsToList(this.getParamSortingOrderSort(params),params);
	}
	return params;
};

Sort.prototype.addSortParamsSortingInvoiceSort = function(params){
	if(isNull(params)){
		params =[];
	}
	if(Array.isArray(params)){
		appendItemsToList(this.getParamSortingInvoiceSort(params),params);
	}
	return params;
};

Sort.prototype.addSortParamsSorting = function(params){
	if(isNull(params)){
		params =[];
	}
	if(Array.isArray(params)){
		appendItemsToList(this.getParamSorting(params),params);
	}
	return params;
};

Sort.prototype.showASC = function(colName){
	var flag = false;
	if(!isNull(colName)){
		var checkStr = colName + this.CONST_ASC;
		if(checkStr==this.sortSelection){
			flag = true;
		}
	}
	return flag;
};

Sort.prototype.showDESC = function(colName){
	var flag = false;
	if(!isNull(colName)){
		var checkStr = colName + this.CONST_DESC;
		if(checkStr==this.sortSelection){
			flag = true;
		}
	}
	return flag;
};

Sort.prototype.getDescCSS = function(colName){
	if(this.showDESC(colName)){
		return "decending_selected";
	}else{
		return "decending";
	}
};

Sort.prototype.getAscCSS = function(colName){
	if(this.showASC(colName)){
		return "ascending_selected";
	}else{
		return "ascending";
	}
};

/*
 * sets selected item in dropdown
 */
function setSelected(dropDown, value, $timeout) {
	$timeout(function() {
		setSelectedIndex(dropDown, value);
	}, 2);
}

/*
 * sets option selected
 * s: dropdown reference
 * v: value that needs to be selected
 */
function setSelectedIndex(s, v) {
	for ( var i = 0; i < s.options.length; i++ ) {
		if ( s.options[i].text == v ) {
			s.options[i].selected = true;
			return;
		}
	}
}

/*
 * checks and returns whether verical scrollbar is visible
 */
function hasVerticalScroll(node){
    if(node == undefined){
        if(window.innerHeight){
            return document.body.offsetHeight> innerHeight;
        }
        else {
            return  document.documentElement.scrollHeight > 
                document.documentElement.offsetHeight ||
                document.body.scrollHeight>document.body.offsetHeight;
        }
    }
    else {
        return node.scrollHeight> node.offsetHeight;
    }
}