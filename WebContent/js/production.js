/**
* Created by andrea on 23/03/15.
*/

$( document ).ready(function() {
    
    
    $('.logged-hp-slider').slick({
      dots: true,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      adaptiveHeight: true
    });
    
    
    $('.carousel-products').slick({
      dots: false,
      infinite: false,
      circular: false,
      speed: 300,
      slidesToShow: 4
    });
    
    
});
//# sourceMappingURL=production.js.map
