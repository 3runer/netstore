
netStoreApp.directive("scroll", function ($window,$state) {
	return function(scope, element, attrs) {
		restrict: 'A',
		angular.element($window).bind("scroll", function() {
			{
				var raw = element[0];
				var rectObject = raw.getBoundingClientRect();
				//var scrolltop =  raw.scrollTop;
				//console.log(" hhgh " + $window.IsScrollbarAtBottom());
				//console.log("rectObject.bottom= "+ rectObject.bottom + " $window.innerHeight = "+$window.innerHeight 
				//		+" $window.scrollY= " + $window.scrollY +" scrolltop="+scrolltop + "scope.Height " + scope.height);
				if (rectObject.bottom <= ($window.innerHeight + 40) ) {					
					state = $state.current.name;
					switch(state)
					{
					case 'home':
					case HOME_LIST_VIEW:
					case HOME_GRID_VIEW:
						scope.$apply('homeCatalogLoadMore()');break;
					case 'home.catalog':
						scope.$apply('catalogLoadMoreFunction()');break;
					case 'home.catalogSub':
						scope.$apply('catalogLoadMoreFunction()');break;
					case 'home.mainCatalog':					
						scope.$apply('mainCatalogLoadMore()');break;
					case PROD_SEARCH_RESULT_VIEW:
					case PROD_SEARCH_RESULT_LISTVIEW:
					case PROD_SEARCH_RESULT_GRIDVIEW:
						scope.$apply('prodSearchLoadMore()');break;
				//	case THANK_YOU:
				//	case PROMOTION_LIST_THANKYOU:
				//	case PROMOTION_GRID_THANKYOU:
					case PRODUCT_DETAIL:
					case PROMOTION_LIST_PRODUCT_DETAIL:										
					case PROMOTION_GRID_PRODUCT_DETAIL:
					case SHOPPING_CART:
					case PROMOTION_LIST_SHOPPING_CART:
					case PROMOTION_GRID_SHOPPING_CART:
					case REQ_SUBMIT_RECEIVED:
					case PROMOTION_LIST_REQ_SUBMIT_RECEIVED:
					case PROMOTION_GRID_REQ_SUBMIT_RECEIVED:
					case PROMOTION_LIST_MOST_POPULAR:
					case PROMOTION_GRID_MOST_POPULAR:
					case PROMOTION_LIST_BEST_OFFERS:
					case PROMOTION_GRID_BEST_OFFERS:
					/* ADM-008 */case ALTERNATIVES_LIST_STATE:
					/* ADM-008 */case ALTERNATIVES_GRID_STATE:
						scope.$apply('catalogLoadMore()');break;
					case MOST_POPULAR_STATE:	
					case BEST_OFFERS_STATE:						
					/*ADM-008*/case ALTERNATIVES_STATE:						
						scope.$apply('catalogLoadMorePromotion()');break;
						
					default: break;

					};

//					if (state =="home" || state == HOME_LIST_VIEW || state == HOME_GRID_VIEW ){
//						scope.$apply('homeCatalogLoadMore()');
//					}
//
//					if (state =="home.mainCatalog" ){
//						scope.$apply('mainCatalogLoadMore()');
//					}
//					if(state == PROD_SEARCH_RESULT_VIEW || state == PROD_SEARCH_RESULT_LISTVIEW || state == PROD_SEARCH_RESULT_GRIDVIEW){
//						scope.$apply('prodSearchLoadMore()');
//					}
//					if(state == THANK_YOU || state == PROMOTION_LIST_THANKYOU || state == PROMOTION_GRID_THANKYOU){
//						scope.$apply('catalogLoadMore()');
//					}
				} else {
					//scope.boolChangeClass = false;
					//console.log('With in view area.');
				}
			}
			//scope.$apply('loadMore()');
		});
	};
});

netStoreApp.directive('onEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.onEnter);
                });
                event.preventDefault();
            }
        });
    };
});

netStoreApp.directive('myDatepicker', function ($parse) {
    return{
    	restrict: 'EA',
    	
    	link:function (scope, element, attrs, controller) {
    	var dateFormatNew;	
    	var dateFormatPlaceHolder = attrs.placeholder;
    	var dateFormatFormated = dateFormatPlaceHolder.toUpperCase();
    	if(dateFormatFormated == "YY-MM-DD"){
    		dateFormatNew = "y-mm-dd";
    	}else if(dateFormatFormated == "YY.MM.DD"){
    		dateFormatNew = "y.mm.dd";
    	}else if(dateFormatFormated == "YY/MM/DD"){
    		dateFormatNew = "y/mm/dd";
    	}else if(dateFormatFormated == "MM-YY-DD"){
    		dateFormatNew = "mm-y-dd";
    	}else if(dateFormatFormated == "MM.YY.DD"){
    		dateFormatNew = "mm.y.dd";
    	}else if(dateFormatFormated == "MM/YY/DD"){
    		dateFormatNew = "mm/y/dd";
    	}else if(dateFormatFormated == "DD-YY-MM"){
    		dateFormatNew = "dd-y-mm";
    	}else if(dateFormatFormated == "DD.YY.MM"){
    		dateFormatNew = "dd.y.mm";
    	}else if(dateFormatFormated == "DD/YY/MM"){
    		dateFormatNew = "dd/y/mm";
    	}else if(dateFormatFormated == "DD-MM-YY"){
    		dateFormatNew = "dd-mm-y";
    	}else if(dateFormatFormated == "DD.MM.YY"){
    		dateFormatNew = "dd.mm.y";
    	}else if(dateFormatFormated == "DD/MM/YY"){
    		dateFormatNew = "dd/mm/y";
    	}else if(dateFormatFormated == "MM-DD-YY"){
    		dateFormatNew = "mm-dd-y";
    	}else if(dateFormatFormated == "MM.DD.YY"){
    		dateFormatNew = "mm.dd.y";
    	}else if(dateFormatFormated == "MM/DD/YY"){
    		dateFormatNew = "mm/dd/y";
    	}
		
		
		else if(dateFormatFormated == "Y-M-D"){
    		dateFormatNew = "y-m-d";
    	}else if(dateFormatFormated == "Y.M.D"){
    		dateFormatNew = "y.m.d";
    	}else if(dateFormatFormated == "Y/M/D"){
    		dateFormatNew = "y/m/d";
    	}else if(dateFormatFormated == "M-Y-D"){
    		dateFormatNew = "m-y-d";
    	}else if(dateFormatFormated == "M.Y.D"){
    		dateFormatNew = "m.y.d";
    	}else if(dateFormatFormated == "M/Y/D"){
    		dateFormatNew = "m/y/d";
    	}else if(dateFormatFormated == "D-Y-M"){
    		dateFormatNew = "d-y-m";
    	}else if(dateFormatFormated == "D.Y.M"){
    		dateFormatNew = "d.y.m";
    	}else if(dateFormatFormated == "D/Y/M"){
    		dateFormatNew = "d/y/m";
    	}else if(dateFormatFormated == "D-M-Y"){
    		dateFormatNew = "d-m-y";
    	}else if(dateFormatFormated == "D.M.Y"){
    		dateFormatNew = "d.m.y";
    	}else if(dateFormatFormated == "D/M/Y"){
    		dateFormatNew = "d/m/y";
    	}else if(dateFormatFormated == "M-D-Y"){
    		dateFormatNew = "m-d-y";
    	}else if(dateFormatFormated == "M.D.Y"){
    		dateFormatNew = "m.d.y";
    	}else if(dateFormatFormated == "M/D/Y"){
    		dateFormatNew = "m/d/y";
    	}
		
		else if(dateFormatFormated == "YY-M-D"){
    		dateFormatNew = "y-m-d";
    	}else if(dateFormatFormated == "YY.M.D"){
    		dateFormatNew = "y.m.d";
    	}else if(dateFormatFormated == "YY/M/D"){
    		dateFormatNew = "y/m/d";
    	}else if(dateFormatFormated == "M-YY-D"){
    		dateFormatNew = "m-y-d";
    	}else if(dateFormatFormated == "M.YY.D"){
    		dateFormatNew = "m.y.d";
    	}else if(dateFormatFormated == "M/YY/D"){
    		dateFormatNew = "m/y/d";
    	}else if(dateFormatFormated == "D-YY-M"){
    		dateFormatNew = "d-y-m";
    	}else if(dateFormatFormated == "D.YY.M"){
    		dateFormatNew = "d.y.m";
    	}else if(dateFormatFormated == "D/YY/M"){
    		dateFormatNew = "d/y/m";
    	}else if(dateFormatFormated == "D-M-YY"){
    		dateFormatNew = "d-m-y";
    	}else if(dateFormatFormated == "D.M.YY"){
    		dateFormatNew = "d.m.y";
    	}else if(dateFormatFormated == "D/M/YY"){
    		dateFormatNew = "d/m/y";
    	}else if(dateFormatFormated == "M-D-YY"){
    		dateFormatNew = "m-d-y";
    	}else if(dateFormatFormated == "M.D.YY"){
    		dateFormatNew = "m.d.y";
    	}else if(dateFormatFormated == "M/D/YY"){
    		dateFormatNew = "m/d/y";
    	}
		
		else if(dateFormatFormated == "YYYY-M-D"){
    		dateFormatNew = "yy-m-d";
    	}else if(dateFormatFormated == "YYYY.M.D"){
    		dateFormatNew = "yy.m.d";
    	}else if(dateFormatFormated == "YYYY/M/D"){
    		dateFormatNew = "yy/m/d";
    	}else if(dateFormatFormated == "M-YYYY-D"){
    		dateFormatNew = "m-yy-d";
    	}else if(dateFormatFormated == "M.YYYY.D"){
    		dateFormatNew = "m.yy.d";
    	}else if(dateFormatFormated == "M/YYYY/D"){
    		dateFormatNew = "m/yy/d";
    	}else if(dateFormatFormated == "D-YYYY-M"){
    		dateFormatNew = "d-yy-m";
    	}else if(dateFormatFormated == "D.YYYY.M"){
    		dateFormatNew = "d.yy.m";
    	}else if(dateFormatFormated == "D/YYYY/M"){
    		dateFormatNew = "d/yy/m";
    	}else if(dateFormatFormated == "D-M-YYYY"){
    		dateFormatNew = "d-m-yy";
    	}else if(dateFormatFormated == "D.M.YYYY"){
    		dateFormatNew = "d.m.yy";
    	}else if(dateFormatFormated == "D/M/YYYY"){
    		dateFormatNew = "d/m/yy";
    	}else if(dateFormatFormated == "M-D-YYYY"){
    		dateFormatNew = "m-d-yy";
    	}else if(dateFormatFormated == "M.D.YYYY"){
    		dateFormatNew = "m.d.yy";
    	}else if(dateFormatFormated == "M/D/YYYY"){
    		dateFormatNew = "m/d/yy";
    	}
		
		else if(dateFormatFormated == "YYYY-MM-DD"){
    		dateFormatNew = "yy-mm-dd";
    	}else if(dateFormatFormated == "YYYY.MM.DD"){
    		dateFormatNew = "yy.mm.dd";
    	}else if(dateFormatFormated == "YYYY/MM/DD"){
    		dateFormatNew = "yy/mm/dd";
    	}else if(dateFormatFormated == "MM-YYYY-DD"){
    		dateFormatNew = "mm-yy-dd";
    	}else if(dateFormatFormated == "MM.YYYY.DD"){
    		dateFormatNew = "mm.yy.dd";
    	}else if(dateFormatFormated == "MM/YYYY/DD"){
    		dateFormatNew = "mm/yy/dd";
    	}else if(dateFormatFormated == "DD-YYYY-MM"){
    		dateFormatNew = "dd-yy-mm";
    	}else if(dateFormatFormated == "DD.YYYY.MM"){
    		dateFormatNew = "dd.yy.mm";
    	}else if(dateFormatFormated == "DD/YYYY/MM"){
    		dateFormatNew = "dd/yy/mm";
    	}else if(dateFormatFormated == "DD-MM-YYYY"){
    		dateFormatNew = "dd-mm-yy";
    	}else if(dateFormatFormated == "DD.MM.YYYY"){
    		dateFormatNew = "dd.mm.yy";
    	}else if(dateFormatFormated == "DD/MM/YYYY"){
    		dateFormatNew = "dd/mm/yy";
    	}else if(dateFormatFormated == "MM-DD-YYYY"){
    		dateFormatNew = "mm-dd-yy";
    	}else if(dateFormatFormated == "MM.DD.YYYY"){
    		dateFormatNew = "mm.dd.yy";
    	}else if(dateFormatFormated == "MM/DD/YYYY"){
    		dateFormatNew = "mm/dd/yy";
    	}
		
		
		else if(dateFormatFormated == "YYYY-M-DD"){
    		dateFormatNew = "yy-m-dd";
    	}else if(dateFormatFormated == "YYYY.M.DD"){
    		dateFormatNew = "yy.m.dd";
    	}else if(dateFormatFormated == "YYYY/M/DD"){
    		dateFormatNew = "yy/m/dd";
    	}else if(dateFormatFormated == "M-YYYY-DD"){
    		dateFormatNew = "m-yy-dd";
    	}else if(dateFormatFormated == "M.YYYY.DD"){
    		dateFormatNew = "m.yy.dd";
    	}else if(dateFormatFormated == "M/YYYY/DD"){
    		dateFormatNew = "m/yy/dd";
    	}else if(dateFormatFormated == "DD-YYYY-M"){
    		dateFormatNew = "dd-yy-m";
    	}else if(dateFormatFormated == "DD.YYYY.M"){
    		dateFormatNew = "dd.yy.m";
    	}else if(dateFormatFormated == "DD/YYYY/M"){
    		dateFormatNew = "dd/yy/m";
    	}else if(dateFormatFormated == "DD-M-YYYY"){
    		dateFormatNew = "dd-m-yy";
    	}else if(dateFormatFormated == "DD.M.YYYY"){
    		dateFormatNew = "dd.m.yy";
    	}else if(dateFormatFormated == "DD/M/YYYY"){
    		dateFormatNew = "dd/m/yy";
    	}else if(dateFormatFormated == "M-DD-YYYY"){
    		dateFormatNew = "m-dd-yy";
    	}else if(dateFormatFormated == "M.DD.YYYY"){
    		dateFormatNew = "m.dd.yy";
    	}else if(dateFormatFormated == "M/DD/YYYY"){
    		dateFormatNew = "m/dd/yy";
    	}
		
		else if(dateFormatFormated == "YYYY-MM-D"){
    		dateFormatNew = "yy-mm-d";
    	}else if(dateFormatFormated == "YYYY.MM.D"){
    		dateFormatNew = "yy.mm.d";
    	}else if(dateFormatFormated == "YYYY/MM/D"){
    		dateFormatNew = "yy/mm/d";
    	}else if(dateFormatFormated == "MM-YYYY-D"){
    		dateFormatNew = "mm-yy-d";
    	}else if(dateFormatFormated == "MM.YYYY.D"){
    		dateFormatNew = "mm.yy.d";
    	}else if(dateFormatFormated == "MM/YYYY/D"){
    		dateFormatNew = "mm/yy/d";
    	}else if(dateFormatFormated == "D-YYYY-MM"){
    		dateFormatNew = "d-yy-mm";
    	}else if(dateFormatFormated == "D.YYYY.MM"){
    		dateFormatNew = "d.yy.mm";
    	}else if(dateFormatFormated == "D/YYYY/MM"){
    		dateFormatNew = "d/yy/mm";
    	}else if(dateFormatFormated == "D-MM-YYYY"){
    		dateFormatNew = "d-mm-yy";
    	}else if(dateFormatFormated == "D.MM.YYYY"){
    		dateFormatNew = "d.mm.yy";
    	}else if(dateFormatFormated == "D/MM/YYYY"){
    		dateFormatNew = "d/mm/yy";
    	}else if(dateFormatFormated == "MM-D-YYYY"){
    		dateFormatNew = "mm-d-yy";
    	}else if(dateFormatFormated == "MM.D.YYYY"){
    		dateFormatNew = "mm.d.yy";
    	}else if(dateFormatFormated == "MM/D/YYYY"){
    		dateFormatNew = "mm/d/yy";
    	}
		
		
		else if(dateFormatFormated == "YYYY-M-DD"){
    		dateFormatNew = "yy-m-dd";
    	}else if(dateFormatFormated == "YYYY.M.DD"){
    		dateFormatNew = "yy.m.dd";
    	}else if(dateFormatFormated == "YYYY/M/DD"){
    		dateFormatNew = "yy/m/dd";
    	}else if(dateFormatFormated == "M-YYYY-DD"){
    		dateFormatNew = "m-yy-dd";
    	}else if(dateFormatFormated == "M.YYYY.DD"){
    		dateFormatNew = "m.yy.dd";
    	}else if(dateFormatFormated == "M/YYYY/DD"){
    		dateFormatNew = "m/yy/dd";
    	}else if(dateFormatFormated == "DD-YYYY-M"){
    		dateFormatNew = "dd-yy-m";
    	}else if(dateFormatFormated == "DD.YYYY.M"){
    		dateFormatNew = "dd.yy.m";
    	}else if(dateFormatFormated == "DD/YYYY/M"){
    		dateFormatNew = "dd/yy/m";
    	}else if(dateFormatFormated == "DD-M-YYYY"){
    		dateFormatNew = "dd-m-yy";
    	}else if(dateFormatFormated == "DD.M.YYYY"){
    		dateFormatNew = "dd.m.yy";
    	}else if(dateFormatFormated == "DD/M/YYYY"){
    		dateFormatNew = "dd/m/yy";
    	}else if(dateFormatFormated == "M-DD-YYYY"){
    		dateFormatNew = "m-dd-yy";
    	}else if(dateFormatFormated == "M.DD.YYYY"){
    		dateFormatNew = "m.dd.yy";
    	}else if(dateFormatFormated == "M/DD/YYYY"){
    		dateFormatNew = "m/dd/yy";
    	}
		
		else if(dateFormatFormated == "YYYY-MM-D"){
    		dateFormatNew = "yy-mm-d";
    	}else if(dateFormatFormated == "YYYY.MM.D"){
    		dateFormatNew = "yy.mm.d";
    	}else if(dateFormatFormated == "YYYY/MM/D"){
    		dateFormatNew = "yy/mm/d";
    	}else if(dateFormatFormated == "MM-YYYY-D"){
    		dateFormatNew = "mm-yy-d";
    	}else if(dateFormatFormated == "MM.YYYY.D"){
    		dateFormatNew = "mm.yy.d";
    	}else if(dateFormatFormated == "MM/YYYY/D"){
    		dateFormatNew = "mm/yy/d";
    	}else if(dateFormatFormated == "D-YYYY-MM"){
    		dateFormatNew = "d-yy-mm";
    	}else if(dateFormatFormated == "D.YYYY.MM"){
    		dateFormatNew = "d.yy.mm";
    	}else if(dateFormatFormated == "D/YYYY/MM"){
    		dateFormatNew = "d/yy/mm";
    	}else if(dateFormatFormated == "MM-D-YYYY"){
    		dateFormatNew = "mm-d-yy";
    	}else if(dateFormatFormated == "MM.D.YYYY"){
    		dateFormatNew = "mm.d.yy";
    	}else if(dateFormatFormated == "MM/D/YYYY"){
    		dateFormatNew = "mm/d/yy";
    	}
		
		
		else if(dateFormatFormated == "YY-M-DD"){
    		dateFormatNew = "y-m-dd";
    	}else if(dateFormatFormated == "YY.M.DD"){
    		dateFormatNew = "y.m.dd";
    	}else if(dateFormatFormated == "YY/M/DD"){
    		dateFormatNew = "y/m/dd";
    	}else if(dateFormatFormated == "M-YY-DD"){
    		dateFormatNew = "m-y-dd";
    	}else if(dateFormatFormated == "M.YY.DD"){
    		dateFormatNew = "m.y.dd";
    	}else if(dateFormatFormated == "M/YY/DD"){
    		dateFormatNew = "m/y/dd";
    	}else if(dateFormatFormated == "DD-YY-M"){
    		dateFormatNew = "dd-y-m";
    	}else if(dateFormatFormated == "DD.YY.M"){
    		dateFormatNew = "dd.y.m";
    	}else if(dateFormatFormated == "DD/YY/M"){
    		dateFormatNew = "dd/y/m";
    	}else if(dateFormatFormated == "DD-M-YY"){
    		dateFormatNew = "dd-m-y";
    	}else if(dateFormatFormated == "DD.M.YY"){
    		dateFormatNew = "dd.m.y";
    	}else if(dateFormatFormated == "DD/M/YY"){
    		dateFormatNew = "dd/m/y";
    	}else if(dateFormatFormated == "M-DD-YY"){
    		dateFormatNew = "m-dd-y";
    	}else if(dateFormatFormated == "M.DD.YY"){
    		dateFormatNew = "m.dd.y";
    	}else if(dateFormatFormated == "MM/DD/YY"){
    		dateFormatNew = "mm/dd/y";
    	}
		
		else if(dateFormatFormated == "YY-MM-D"){
    		dateFormatNew = "y-mm-d";
    	}else if(dateFormatFormated == "YY.MM.D"){
    		dateFormatNew = "y.mm.d";
    	}else if(dateFormatFormated == "YY/MM/D"){
    		dateFormatNew = "y/mm/d";
    	}else if(dateFormatFormated == "MM-YY-D"){
    		dateFormatNew = "mm-y-d";
    	}else if(dateFormatFormated == "MM.YY.D"){
    		dateFormatNew = "mm.y.d";
    	}else if(dateFormatFormated == "MM/YY/D"){
    		dateFormatNew = "mm/y/d";
    	}else if(dateFormatFormated == "D-YY-MM"){
    		dateFormatNew = "d-y-mm";
    	}else if(dateFormatFormated == "D.YY.MM"){
    		dateFormatNew = "d.y.mm";
    	}else if(dateFormatFormated == "D/YY/MM"){
    		dateFormatNew = "d/y/mm";
    	}else if(dateFormatFormated == "D-MM-YY"){
    		dateFormatNew = "d-mm-y";
    	}else if(dateFormatFormated == "D.MM.YY"){
    		dateFormatNew = "d.mm.y";
    	}else if(dateFormatFormated == "D/MM/YY"){
    		dateFormatNew = "d/mm/y";
    	}else if(dateFormatFormated == "MM-D-YY"){
    		dateFormatNew = "mm-d-y";
    	}else if(dateFormatFormated == "MM.D.YY"){
    		dateFormatNew = "mm.d.y";
    	}else if(dateFormatFormated == "MM/D/YY"){
    		dateFormatNew = "mm/d/y";
    	}
		
		else if(dateFormatFormated == "Y-M-DD"){
    		dateFormatNew = "y-m-dd";
    	}else if(dateFormatFormated == "Y.M.DD"){
    		dateFormatNew = "y.m.dd";
    	}else if(dateFormatFormated == "Y/M/DD"){
    		dateFormatNew = "y/m/dd";
    	}else if(dateFormatFormated == "M-Y-DD"){
    		dateFormatNew = "m-y-dd";
    	}else if(dateFormatFormated == "M.Y.DD"){
    		dateFormatNew = "m.y.dd";
    	}else if(dateFormatFormated == "M/Y/DD"){
    		dateFormatNew = "m/y/dd";
    	}else if(dateFormatFormated == "DD-Y-M"){
    		dateFormatNew = "dd-y-m";
    	}else if(dateFormatFormated == "DD.Y.M"){
    		dateFormatNew = "dd.y.m";
    	}else if(dateFormatFormated == "DD/Y/M"){
    		dateFormatNew = "dd/y/m";
    	}else if(dateFormatFormated == "DD-M-Y"){
    		dateFormatNew = "dd-m-y";
    	}else if(dateFormatFormated == "DD.M.Y"){
    		dateFormatNew = "dd.m.y";
    	}else if(dateFormatFormated == "DD/M/Y"){
    		dateFormatNew = "dd/m/y";
    	}else if(dateFormatFormated == "M-DD-Y"){
    		dateFormatNew = "m-dd-y";
    	}else if(dateFormatFormated == "M.DD.Y"){
    		dateFormatNew = "m.dd.y";
    	}else if(dateFormatFormated == "MM/DD/Y"){
    		dateFormatNew = "mm/dd/y";
    	}
		
		else if(dateFormatFormated == "Y-MM-D"){
    		dateFormatNew = "y-mm-d";
    	}else if(dateFormatFormated == "Y.MM.D"){
    		dateFormatNew = "y.mm.d";
    	}else if(dateFormatFormated == "Y/MM/D"){
    		dateFormatNew = "y/mm/d";
    	}else if(dateFormatFormated == "MM-Y-D"){
    		dateFormatNew = "mm-y-d";
    	}else if(dateFormatFormated == "MM.Y.D"){
    		dateFormatNew = "mm.y.d";
    	}else if(dateFormatFormated == "MM/Y/D"){
    		dateFormatNew = "mm/y/d";
    	}else if(dateFormatFormated == "D-Y-MM"){
    		dateFormatNew = "d-y-mm";
    	}else if(dateFormatFormated == "D.Y.MM"){
    		dateFormatNew = "d.y.mm";
    	}else if(dateFormatFormated == "D/Y/MM"){
    		dateFormatNew = "d/y/mm";
    	}else if(dateFormatFormated == "D-MM-Y"){
    		dateFormatNew = "d-mm-y";
    	}else if(dateFormatFormated == "D.MM.Y"){
    		dateFormatNew = "d.mm.y";
    	}else if(dateFormatFormated == "D/MM/Y"){
    		dateFormatNew = "d/mm/y";
    	}else if(dateFormatFormated == "MM-D-Y"){
    		dateFormatNew = "mm-d-y";
    	}else if(dateFormatFormated == "MM.D.Y"){
    		dateFormatNew = "mm.d.y";
    	}else if(dateFormatFormated == "MM/D/Y"){
    		dateFormatNew = "mm/d/y";
    	}
    	
    	
        var ngModel = $parse(attrs.ngModel);
        $(function(){
            element.datepicker({
                
                changeYear:true,
                changeMonth:true,
                //attrs.placeholder in place of yy-dd-mm
                dateFormat:dateFormatNew,
                //minDate: new Date(),
                
                onSelect:function (dateText, inst) {
                    scope.$apply(function(scope){
                        // Change binded variable
                        ngModel.assign(scope, dateText);
                    });
                }
            }); 
        });
        
    }}
});

netStoreApp.directive('productTableview', function () {
    return {
        templateUrl: 'directives/productListview.html'
    };
});

netStoreApp.directive('productGridview', function () {
    return {
        templateUrl: 'directives/gridViewItem.html'
    };
});



netStoreApp.directive('heightStuff', ['$timeout',
                          function($timeout) {
                            return {
                              scope: {
                                myData: '='
                              },
                              link: function($scope, element, attrs) {
                                $scope.$watch('myData', function() {
                                  $timeout(function() {
                                	  $(function() {
                  	        		cbpHorizontalMenu.init();
                  	        	});
                                  }, 0, false);
                                })
                              }
                            };
                          }
                        ]);
netStoreApp.directive(
        "adminMessage",
        function( $document, $parse ){
            var linkFunction = function( $scope, $element, $attributes ){
                var scopeExpression = $attributes.adminMessage;
                var invoker = $parse( scopeExpression );
                $document.on(
                    "click",
                    function( event ){
                        $scope.$apply(
                            function(){
                                invoker(
                                    $scope,
                                    {
                                        $event: event
                                    }
                                );
                            }
                        );
                    }
                );
            };
            return( linkFunction );
        }
    );