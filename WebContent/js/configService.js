
var propFile = "./config/config.properties";
var BASE_URL ='baseUrl';
var PORT = 'port';
var CONTEXT = 'context';


var configPropertyService = angular.module('configPropertyService', []).
factory('configProperty', function($http,$q,$rootScope){
	var pData = '';
	var service = {};
	
	// function to read property file and return data
	 service.getData = function()
	{
		var deferred = $q.defer();				
		if(pData == '')
		{
			$http.get(propFile).success(function(data){
					deferred.resolve(data);	});
		}
		return  deferred.promise;		
	};

		// Function to make final url
	service.getURL = function(url,data,jsonCall){
		var finalurl = data[BASE_URL]+data[PORT]+data[CONTEXT]+data[url];
		if(jsonCall)
    	 finalurl = finalurl+'?CallBack=JSON_CALLBACK';
    	return finalurl;
    		
   	} ;
   	service.getKeyValue = function(key,data){
   		var keyValue = "";
   		if(BASE_URL == key)
    	 {keyValue = data[BASE_URL]+data[PORT];}
   		else
   			{keyValue = data[key] ;}
    	return keyValue;
    		
   	} ;
	   	
	return service;
	
});
