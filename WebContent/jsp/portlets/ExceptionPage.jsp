<%-- Show exception page.
NOTE:	The bean may not be available and session attributes might not exist.
		This jsp tries to translate all texts but defaults to English if unable to. --%>
<html>

<%-- Imports --%>
<%@ page import="se.ibs.ccf.*, se.ibs.ns.cf.*, se.ibs.ns.adm.* " %>

<%-- Declarations --%>
<%
javax.portlet.RenderRequest renderRequest = (javax.portlet.RenderRequest) request.getAttribute("javax.portlet.request");
javax.portlet.RenderResponse renderResponse = (javax.portlet.RenderResponse) request.getAttribute("javax.portlet.response");
se.ibs.ccf.Request wrappedRequest = new se.ibs.ccf.Request(renderRequest, se.ibs.ns.cf.NSConstantsSession.SESSION_PREFIX);
se.ibs.ccf.Session wrappedSession = new se.ibs.ccf.Session(renderRequest.getPortletSession(), se.ibs.ns.cf.NSConstantsSession.SESSION_PREFIX);
SessionHelper sh = SessionHelper.getSessionHelper(wrappedSession);
ExceptionWrapper ew = null;
NSAdminBean admBean = null;
boolean isNSAdminInvokerClass = false;
boolean isNetStoreInit = NSObject.isInitialized();
if (sh != null) {
	ew = (ExceptionWrapper)sh.getSessionObject(CCFConstantsSession.EXCEPTION_WRAPPER);
	sh.removeSessionObject(CCFConstantsSession.EXCEPTION_WRAPPER);
	admBean = (NSAdminBean) sh.getSessionObject(NSAdminConstantsSession.NSADMIN_BEAN);
	if (ew != null) {
		isNSAdminInvokerClass = (ew.getInvokerClass().indexOf("NSAdmin") > 0);
	}
}
String lc = null;
String errorText = ""; 
String stackLabel = "";

if (ew == null) {
	NSBaseBean nsBaseBean = (NSBaseBean) sh.getSessionObject(NSConstantsSession.NS_BASE_BEAN);
	if (nsBaseBean != null) {
		lc = nsBaseBean.getLanguageCode(); 
	} else {
		lc = NSRequestHelper.getUserLanguageCode(wrappedRequest);
	}
	
	String dft = "An unexpected error has occurred, exception details are missing!";
	errorText = NSObject.translate("TXT_CF_018",lc,dft);
	
} else if (ew.getThrowable() != null) { 
	String dft = "An unexpected error has occurred. See the information below for details about the error..."; 
	errorText = ew.translate("TXT_CF_016",dft);
	stackLabel = ew.translate("CON_EXCEPTION_STACK_TRACE","Exception stack trace");
}

String classLabel = "";
String methodLabel = "";
if (ew != null) {
	classLabel = ew.translate("CON_INVOKER_CLASS","Invoker class");
	methodLabel = ew.translate("CON_INVOKER_METHOD","Invoker method");
}

// Unregister the current NetStore session  
SessionTracker.unregisterSession(wrappedSession);
%>

<div style="height:500px;overflow:auto;">
<table width="100%" height="10%">
	<tr>
		<% if (ew == null) {%>     				
			<td><h3><%=errorText%></h3></td>
		<%} else {%>
			<td><h3><%=ew.getDescription()%></h3></td>
		<%}%>
	</tr>
	<tr><td>&nbsp;</td></tr>
</table>
<%
if (ew != null ) {
	String invokerClass = ew.getInvokerClass().trim();
	String invokerMethod = ew.getInvokerMethod().trim(); 
	if (invokerClass.length() > 0 || invokerMethod.length() > 0) {%>
		<table width="100%" height="10%">
			<tr>	
				<td nowrap="nowrap"><h4><%=classLabel%></h4></td>
				<td nowrap="nowrap"><h4><%=methodLabel%></h4></td>
			</tr>
			<tr>
				<td>&nbsp;<%=ew.getInvokerClass()%></td>
				<td>&nbsp;<%=ew.getInvokerMethod()%></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<% if (ew.getThrowable() != null) {%>
				<tr><td colspan="2" nowrap="nowrap"><h4><%=stackLabel%></h4></td></tr>
				<tr><td colspan="2">&nbsp;<%=ew.getStackTrace()%></td></tr>
			<%}%>
		</table>
	<%}%>
<%}%>
</div>