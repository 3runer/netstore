<%-- Imports --%>
<%@ page import= "se.ibs.ccf.*, se.ibs.ns.adm.*" %>

<%-- Declarations --%>
<%@ include file="FuncWrappers.jsp" %>
<%
NSAdminSignonBean signonBean = (NSAdminSignonBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_SIGNON_BEAN, wrappedSession);
%>

<%@ include file="NSAdminFuncPageBegin.jspbb" %>
<table width="100%" height="10%">
	<tr align="center">
		<td>
			<img src="/NS/IBSStaticResources/Portlets/Error.png"/>
			<%= baseBean.translate("TXT_CF_041") %>
		</td>
	</tr>
	<%
	if (signonBean.hasValidationError()) {
		VectorIterator vi2 = signonBean.createIterator (signonBean.getValidationErrors());
		while (vi2.next()) {%>
			<tr align="center">
				<td>
				<img src="/NS/IBSStaticResources/Portlets/Error.png"/>
				<%=vi2.getString()%>
				</td>
			</tr>
		<%}%>
	<%}%>
</table>
<%@ include file="NSAdminFuncEnd.jspbb" %>