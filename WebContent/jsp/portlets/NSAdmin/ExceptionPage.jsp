<%-- Show exception page.
NOTE:	The bean may not be available and session attributes might not exist.
		This jsp tries to translate all texts but defaults to English if unable to. --%>
<%-- Imports --%>
<%@ page import="se.ibs.ccf.*, se.ibs.ns.cf.*, se.ibs.ns.adm.* " %>

<%-- Declarations --%>
<%@ include file="FuncWrappers.jsp" %>
<%@ include file="FuncHead.jspbb" %>
<%
SessionHelper sh = SessionHelper.getSessionHelper(wrappedSession);
ExceptionWrapper ew = null;
String resourcePath  = null;
String themeResourcePath  = null;
String imageExt = ".gif";
String css = null;
String lc = null;
String title = null;

boolean isNetStoreInit = NSObject.isInitialized();

if (sh != null) {
	ew = (ExceptionWrapper) sh.getSessionObject(CCFConstantsSession.EXCEPTION_WRAPPER);
	sh.removeSessionObject(CCFConstantsSession.EXCEPTION_WRAPPER);
}

if (ew != null && ew.getBean() != null) {
	css = ew.getBean().getStyleSheet();
	title = ew.getBean().translate("CON_ERROR","Error");
	resourcePath = ew.getBean().getResourcePath(null);
	themeResourcePath = ew.getBean().getThemeResource(null);
	imageExt = ew.getBean().getImageExtension();
} else {
	NSBaseBean nsBaseBean = (NSBaseBean) sh.getSessionObject(NSConstantsSession.NS_BASE_BEAN);
	if (nsBaseBean != null) {
		lc = nsBaseBean.getLanguageCode();
		css = nsBaseBean.getStyleSheet();
		resourcePath = nsBaseBean.getResourcePath(null);
		themeResourcePath = nsBaseBean.getThemeResource(null);
		imageExt = nsBaseBean.getImageExtension();
	} else {
		lc = NSRequestHelper.getUserLanguageCode(wrappedRequest);
		css = NSObject.getStyleSheet(request.getHeader("User-Agent").indexOf("MSIE") < 0);
		resourcePath = NSObject.getResourcePath();
		themeResourcePath = NSObject.getThemeResourcePath();
		imageExt = CCFObject.getImageExtension(request.getHeader("User-Agent"));
	}
	title = NSObject.translate("CON_ERROR",lc,"ERROR");
}

String errorText = ""; 
String stackLabel = ""; 
if (ew == null) { 
	String dft = "An unexpected error has occurred, exception details are missing!";
	errorText = NSObject.translate("TXT_CF_018",lc,dft); 
} else if (ew.getThrowable() != null) { 
	String dft = "An unexpected error has occurred. See the information below for details about the error..."; 
	errorText = ew.translate("TXT_CF_016",dft);
	stackLabel = ew.translate("CON_EXCEPTION_STACK_TRACE","Exception stack trace");
}

String classLabel = "";
String methodLabel = "";
String restartText = "";
if (ew != null) {
	classLabel = ew.translate("CON_INVOKER_CLASS","Invoker class");
	methodLabel = ew.translate("CON_INVOKER_METHOD","Invoker method");
	restartText = ew.translate("TXT_CF_020","Obtain a new session");
} else {
	restartText = NSObject.translate("TXT_CF_020",lc,"Obtain a new session");
}

// No caching
wrappedResponse.setHeader("Cache-Control","no-cache"); // For HTTP version 1.1
wrappedResponse.setHeader("Pragma","no-cache"); //For HTTP version 1.0
wrappedResponse.setDateHeader ("Expires", 0); // To prevent caching at the proxy server 
   
SessionTracker.unregisterSession(wrappedSession);
%>

<div style="height: 500px; overflow: auto;">
	<table border="0" width="100%">
		<tr>
			<td class="IBSPageTitleText" valign="bottom" nowrap>
				<font class="IBSPageTitleHeader"><%= title %></font>
			</td>
		</tr>
		<tr>
			<td class="IBSPageTitleDivider" height="1"></td>
		</tr>
		<tr>
			<td class="IBSEmpty">&nbsp;</td>
		</tr>
		<tr>
		<%	if (ew == null) {%>     				
			<td class="IBSTextNormal"><%= errorText %></td>
		<%} else {%>
			<td class="IBSTextWarning"><%= ew.getDescription() %></td>
		<%}%>
		</tr>
		<tr><td class="IBSEmpty">&nbsp;</td></tr>
	</table>
<%
if (ew != null ) { 
	String invokerClass = ew.getInvokerClass().trim();
	String invokerMethod = ew.getInvokerMethod().trim(); 
	if (invokerClass.length() > 0 || invokerMethod.length() > 0) {%>
	<table class="IBSListTable2" width="100%">
		<tr>
			<td class="IBSHeaderCell" nowrap>&nbsp;<%= classLabel %>&nbsp;</td>
			<td class="IBSHeaderCell" nowrap>&nbsp;<%= methodLabel %>&nbsp;</td>
		</tr>
		<tr>
			<td class="IBSListCell2"><%= ew.getInvokerClass() %></td>
			<td class="IBSListCell2"><%= ew.getInvokerMethod() %></td>
		</tr>
		<% if (ew.getThrowable() != null) {%>
			<tr>
				<td class="IBSHeaderCell" colspan="2" nowrap>&nbsp;<%= stackLabel %>&nbsp;</td>
			</tr>
			<tr>
				<td class="IBSListCell2" colspan="2"><%= ew.getStackTrace() %></td>
			</tr>
		<%}%>
	</table>
	<%}}%>
</div>