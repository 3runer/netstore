<%-- Imports --%>
<%@ page import="se.ibs.ns.adm.*" %>

<%-- Declarations --%>
<%@ include file="FuncWrappers.jsp" %>
<%@ include file="FuncHead.jspbb" %>
<div style="width:160px; height:120px; margin:0px auto;">
<table class="IBSLayoutTable" height="100%" width="100%" cellspacing="0" cellpadding="0">
<tr>
	<td width="100%" align="center" valign="top" nowrap>
		<%= baseBean.getResourceHtml("Logo",null) %>
	</td>
</tr>
<% if (baseBean.isSignedOn()) {%>
<tr>
	<td width="100%" align="center" valign="top" nowrap>
		<a class="IBSTopLink" href="<%=baseBean.getEncodedServlet(NSAdminConstantsServlets.NSADMINMENU_SERVLET, wrappedResponse) %>">
 			<%=baseBean.getResourceHtml("HomeImg", baseBean.translate("CON_HOME")) %>
 		</a>
	</td>
</tr>
<%}%>
</table>
</div>