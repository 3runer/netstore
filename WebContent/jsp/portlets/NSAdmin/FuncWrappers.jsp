<%
javax.portlet.RenderRequest renderRequest = (javax.portlet.RenderRequest) request.getAttribute("javax.portlet.request");
javax.portlet.RenderResponse renderResponse = (javax.portlet.RenderResponse) request.getAttribute("javax.portlet.response");
se.ibs.ccf.Request wrappedRequest = new se.ibs.ccf.Request(renderRequest, se.ibs.ns.adm.NSAdminConstantsSession.SESSION_PREFIX);
se.ibs.ccf.Response wrappedResponse = new se.ibs.ccf.Response(renderResponse, null);
se.ibs.ccf.Session wrappedSession = new se.ibs.ccf.Session(renderRequest.getPortletSession(), se.ibs.ns.adm.NSAdminConstantsSession.SESSION_PREFIX);
se.ibs.ns.adm.NSAdminBean baseBean = se.ibs.ns.adm.NSAdminBean.getInstance(wrappedSession);
wrappedResponse.setPortletURL(renderResponse.createActionURL());
%>