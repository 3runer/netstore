<%-- Imports --%>
<%@ page import= "java.util.*, se.ibs.ns.cf.*" %>

<%-- Declarations --%>
<%
javax.portlet.RenderRequest renderRequest = (javax.portlet.RenderRequest) request.getAttribute("javax.portlet.request");
se.ibs.ccf.Request wrappedRequest = new se.ibs.ccf.Request(renderRequest, se.ibs.ns.cf.NSConstantsSession.SESSION_PREFIX);
se.ibs.ccf.Session wrappedSession = new se.ibs.ccf.Session(renderRequest.getPortletSession(), se.ibs.ns.cf.NSConstantsSession.SESSION_PREFIX);
se.ibs.ns.cf.NSBaseBean baseBean = se.ibs.ns.cf.NSBaseBean.getInstance(wrappedSession);
String portalRole = (String) wrappedRequest.getAttribute("PORTAL_ROLE");
Vector<String> substituteData = new Vector<String>();
substituteData.add(portalRole);
String message = baseBean.translate("TXT_CF_042", baseBean.getLanguageCode(), substituteData);
%>

<div style="height:500px;overflow:auto;">
<table width="100%" height="10%">
	<tr align="center">
		<td>
			<img src="/NS/IBSStaticResources/Portlets/Error.png"/>
			<%=message%>
		</td>
	</tr>
</table>
</div>