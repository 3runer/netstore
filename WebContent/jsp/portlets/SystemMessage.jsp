<%-- Show system message --%>
<%-- Imports --%>
<%@ page import="se.ibs.ns.cf.*, se.ibs.ccf.*" %>

<%-- Declarations --%>
<%
javax.portlet.RenderRequest renderRequest = (javax.portlet.RenderRequest) request.getAttribute("javax.portlet.request");
se.ibs.ccf.Request wrappedRequest = new se.ibs.ccf.Request(renderRequest, se.ibs.ns.cf.NSConstantsSession.SESSION_PREFIX);
se.ibs.ccf.Session wrappedSession = new se.ibs.ccf.Session(renderRequest.getPortletSession(), se.ibs.ns.cf.NSConstantsSession.SESSION_PREFIX);
SessionHelper sh = SessionHelper.getSessionHelper(wrappedSession);
String sysMsg = "UNKNOWN SYSTEM MESSAGE";   
if (sh != null) {
	sysMsg = (String) sh.getSessionObject(CCFConstantsSession.SYSTEM_MESSAGE);
	sh.removeSessionObject(CCFConstantsSession.SYSTEM_MESSAGE);
}
%>

<div style="height:500px;overflow:auto;">
<table width="100%" height="10%">
	<tr><td><h3>System Message</h3></td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td><%= sysMsg %></td></tr>
</table>
</div>