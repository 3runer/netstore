<%-- Page for update or refresh the site cache in NetStore --%>
<%-- Imports --%>
<%@ page import="se.ibs.ns.adm.*,se.ibs.ccf.*, se.ibs.ns.cf.*" %>

<%-- Declarations --%>     
<%
NSAdminSiteCacheBean bean = (NSAdminSiteCacheBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_SITECACHE_BEAN,wrappedSession);
String servletName = bean.getEncodedServlet(NSAdminConstantsServlets.NSADMINNETSTORESITECACHEUPDATE_SERVLET, wrappedResponse);      
int applicationStatus = bean.getNetStoreStatus();
String title = bean.translate("CON_WORK_NETSTORE_SITE_CACHE");
%>

<script language="JavaScript" type="text/javascript">
loaded = false;
function onLoad() {
	loaded = true;
	top.window.blockedLanguageSwitch = true;
}
addOnLoadEvent(onLoad);
</script>

<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminNetStoreSiteCacheUpdate" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>

<%-- Title  --%>
<table width="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
    	<td width="50%" align="left" valign="bottom" nowrap>
    	<% 	if (applicationStatus == ApplicationStatus.OPEN) { 
				if (NSObject.isSiteCacheInitialized() && NSObject.isInitialized()) { %>
				<font class="IBSPageTitleRightHeader"><%= bean.translate("CON_NETSTORE_SITECACHE_INITILIZED") %>:</font>
				&nbsp;
				<font class="IBSOutput"><%= bean.formatDate(bean.getNetStoreSiteCacheDateTime("DATE")) %>
				&nbsp;<%= bean.getNetStoreSiteCacheDateTime("TIME") %>
				</font>
			<% } 
			} %>	
		</td>
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" width="97%" colspan="2" height="1"></td>
  	</tr>
  	<tr valign="top">
  		<td>
  			<%= bean.getHtmlForActionLink("ACTION_MENU", null, bean.translate("CON_BACK_TO_MENU"), bean.getResource("LeftArrow"), null) %>
			<% 	if (applicationStatus == ApplicationStatus.OPEN) { 
					if (NSObject.isSiteCacheInitialized() && NSObject.isInitialized()) { %>
						&nbsp;<%= bean.getHtmlForActionLink("ACTION_SITECACHE", null, bean.translate("CON_SHOW"), bean.getResource("View"), null) %>
						&nbsp;<%= bean.getHtmlForActionLink("ACTION_CLEAR", null, bean.translate("CON_CLEAR"), bean.getResource("Clear"), null) %>
				<% } 
			} %>	
		</td>
	</tr>
	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
  	<tr valign="top">
  		<td colspan="2">
		<table class="IBSLayoutTable" cellspacing="0" width="100%">
		
		<% if (applicationStatus == ApplicationStatus.OPEN) { %>
		<!-- Message -->
		<tr>
			<td class="IBSOutput" colspan="2" nowrap>                				                          
 			<% if(bean.hasCCFMessage()){ %>
 			<small>
    			<%= bean.getMessageHtml(bean.getCCFMessage()) %>
    		</small>
    		<br>
    		<% bean.setCCFMessage(null); %>
 		<%  } %>
   			</td>
		</tr>

	<% 	}  %>
	<% 	if (applicationStatus == ApplicationStatus.OPEN) { 
			if (!NSObject.isSiteCacheInitialized() || !NSObject.isInitialized()) { %>
		<tr>
			<td class="IBSEmpty">&nbsp;</td>
		</tr>
		<tr>
			<td class="IBSLabel" width="10%" nowrap><%= bean.translate("CON_INITILIZE_NETSTORE_SITECACHE") %>:&nbsp;</td>
			<td><input type="submit" name="ACTION_LOAD" value="<%= bean.translate("CON_LOAD") %>" onClick="submitButtonOnce(this); return false"></td>
		</tr>
		<tr>
			<td class="IBSEmpty">&nbsp;</td>
		</tr>

		<%--  Display area when NetStore is initilized  --%>

		<%	} else { %>
	
		<tr>
			<td class="IBSEmpty">&nbsp;</td>
		</tr>
		<tr>
			<td valign="top" class="IBSLabel">
				<input type="Checkbox" name="INCLUDE_PRELOAD">&nbsp;<%= bean.translate("CON_INCLUDE_PRELOAD_DATA")%>&nbsp;(<%= bean.translate("CON_NOT_RECOMMENDED") %>)	
				&nbsp;<input type="submit" name="ACTION_REFRESH" value="<%= bean.translate("CON_REFRESH") %>" onClick="javascript:submitButtonConfirmation('<%= bean.translate("CON_CONFIRM_REFRESH_SITE_CACHE") %>', '<%= servletName %>', this); return false">
			</td>
		</tr>
		
		<tr>
			<td class="IBSEmpty">&nbsp;</td>
		</tr>
		
	<% 	} 
	} else { %>

		<tr>
			<td class="IBSTextAttention" align="center" colspan="2">
				<br><b><%= bean.translate("CON_NETSTORE_NOT_OPEN") %></b><br>
			</td>
		</tr>
	<% } %>
		</table>
		</td>
	</tr>
</table>
</form>