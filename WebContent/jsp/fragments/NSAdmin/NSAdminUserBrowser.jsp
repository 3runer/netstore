<%-- Page to enable or active users --%>
<%-- Imports --%>
<%@ page import="java.util.*, se.ibs.ns.adm.*,se.ibs.ccf.*, se.ibs.bap.web.*" %>

<%-- Declarations --%>
<%
NSAdminUserBrowserBean bean = (NSAdminUserBrowserBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_USER_BROWSER_BEAN,wrappedSession);
String servletName = bean.getEncodedServlet(NSAdminConstantsServlets.NSADMINUSERBROWSER_SERVLET, wrappedResponse); 	
String title = bean.translate("CON_WORK_WITH_USERS");
Vector pageIterator = bean.getUsers();
boolean isOneLanguage = bean.isOneLanguage();
Vector statuses = bean.getSelectFormUserStatus().getValues();
boolean isNewUserColumn = statuses.contains("NEW_USER");
boolean isNewCustomerColumn = statuses.contains("NEW_CUSTOMER");
int columns = 8;
if (!isOneLanguage) {
	columns++;
} 
if (isNewUserColumn) {
	columns++;
}
if (isNewCustomerColumn) {
	columns++;
}    
%>

<script language="JavaScript" type="text/javascript">
loaded = false;
function onLoad() {
	loaded = true;
	top.window.blockedLanguageSwitch = true;
}
addOnLoadEvent(onLoad);
</script>

<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminUserBrowser" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>

<%-- Title  --%>
<table width="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" width="97%" height="1"></td>
  	</tr>
  	<tr valign="top">
  		<td>
  			<%= bean.getHtmlForActionLink("ACTION_BACK", null, bean.translate("CON_BACK_TO_MENU"), bean.getResource("LeftArrow"), null) %>
		</td>
	</tr>
	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
  	<% int size = pageIterator.size();
		if (size > 0 ) { %>
  	<tr valign="top">
		<td>
		<!-- User table -->
		<table>
		<tr>
  			<td colspan="2">
				<table class="IBSLayoutTable" cellspacing="0" width="97%">
				<tr>
					<td class="IBSHeaderCellSmall" nowrap>&nbsp;</td>
					<td class="IBSHeaderCellSmall" nowrap><%= bean.translate("CON_USER_ID") %>&nbsp;</td>
					<td class="IBSHeaderCellSmall" nowrap><%= bean.translate("CON_NAME") %>&nbsp;</td>
					<td class="IBSHeaderCellSmall" nowrap><%= bean.translate("COH_DISABLED") %>&nbsp;</td>
					<% if (isNewUserColumn) { %>
						<td class="IBSHeaderCellSmall" nowrap><%= bean.translate("COH_IS_NEW_USER") %>&nbsp;</td>
					<% } %>
					<% if (isNewCustomerColumn) { %>
						<td class="IBSHeaderCellSmall" nowrap><%= bean.translate("COH_IS_NEW_CUSTOMER") %>&nbsp;</td>
					<% } %>
					<td class="IBSHeaderCellSmall" nowrap><%= bean.translate("COH_LAST_USED_DATE") %>&nbsp;</td>
					<% if(!isOneLanguage) { %>
						<td class="IBSHeaderCellSmall" nowrap>						
							<%= bean.translate("COH_LANGUAGE") %>
							&nbsp;						
						</td>
					<% } %>
					<td class="IBSHeaderCell" nowrap><%= bean.translate("CON_EMAIL_ADDRESS") %>&nbsp;</td>
					<td class="IBSHeaderCell" nowrap><%= bean.translate("COH_FORCE_PASSWORD_CHANGE") %>&nbsp;</td>
					<td class="IBSHeaderCell" nowrap><%= bean.translate("COH_SEVERAL_CUSTOMERS_ALLOWED") %>&nbsp;</td>
				</tr>

  				<%	BAPWebUser user = null;
				    String checkedImage = bean.getResource("Checked");
				    String altDisabled = bean.translate("CON_DISABLED");
    				String altNewUser = bean.translate("CON_NEW_USER");
    				String altFPC = bean.translate("CON_FORCE_PASSWORD_CHANGE");
    				String altSeveralCust = bean.translate("CON_SEVERAL_CUSTOMERS_ALLOWED");
    				String userCode = null;   					
					for(int i = 0; i<size; i++) {
  						user = (BAPWebUser)pageIterator.elementAt(i);
  						userCode = user.getCode();
  						if (i == 0) {
  							bean.setTopUser(user);
  						}  			
						if (i%2 == 0) { %>
        					<tr class="IBSListRow1">
    					<% } else { %>
      		 				<tr class="IBSListRow2">
  	  					<% } %>
   							<td class="IBSListCell" nowrap>
   								<input type="checkbox" name="CB_USER_SELECTED" value="<%= userCode %>" <%= bean.getCheckedString(userCode) %>>
   								&nbsp;
   								<%= bean.getErrorMessageHtml(bean.getLineMessage(userCode)) %>
   								&nbsp;
   							</td>
   							<td class="IBSListCell" nowrap><%= userCode %>&nbsp;</td>
							<td class="IBSListCell" nowrap><%= user.getName() %>&nbsp;</td>
							<td class="IBSListCell" align="center" nowrap>
								<% if (!user.isActive()) { %>
										<img border="0" align="middle" src="<%= checkedImage %>" alt="<%= altDisabled %>">
								<% } %>
								&nbsp;
							</td>
							<% if (isNewUserColumn) { %>
								<td class="IBSListCell" align="center" nowrap>
									<% if (bean.isNewUser(user)) { %>
										<img border="0" align="middle" src="<%= checkedImage %>" alt="<%= altNewUser %>">
									<% } %>
									&nbsp;
								</td>
							<% } %>
							<% if (isNewCustomerColumn) { %>
								<td class="IBSListCell" align="center" nowrap>
									<% if (bean.isNewCustomer(user)) { %>
										<input class="IBSInput" type="text" name="EF_CUSTOMER_<%= userCode %>" size="11" value="<%= bean.getCustomerCode(user) %>" maxlength="11">
									<% } %>	
									&nbsp;									
								</td>
							<% } %>
							<td class="IBSListCell" nowrap><%= bean.formatDate(user.getLastUsedDate()) %>&nbsp;</td>
							<% if(!isOneLanguage) { %>
								<td class="IBSListCell" nowrap>								
									<%= bean.getUserLanguageCode(user) %>
								</td>	
							<% } %>							
							<td class="IBSListCell" nowrap><%= user.getEmail() %>&nbsp;</td>
							<td class="IBSListCell" align="center" nowrap>
								<% if (user.isForcePasswordChange()) { %>
									<img border="0" align="middle" src="<%= checkedImage %>" alt="<%= altFPC %>">
								<% } %>
								&nbsp;
							</td>
							<td class="IBSListCell" align="center" nowrap>
								<% if (user.isSeveralCustomersAllowed()) { %>
									<img border="0" align="middle" src="<%= checkedImage %>" alt="<%= altSeveralCust %>">
								<% } %>
								&nbsp;
							</td>
   						</tr>
   					<% } %>
   				
					
					<tr><td class="IBSTableFooter" colspan="<%= columns %>" nowrap></td></tr>
					<!-- Actions -->
					<tr>
						<td class="IBSOutput" colspan="<%= columns %>" nowrap>
							<input type="checkbox" name="ALL_SELECTED" onclick="javascript:toggleCheckboxes('NetStoreAdminDetailForm', this.checked,'CB_USER_SELECTED');">
							&nbsp;<%= bean.translate("CON_SELECT_ALL") %>
							<% if (bean.isActiveAction("DISABLE")) { %>
								&nbsp;<%= bean.getHtmlForActionLink("ACTION_DISABLE", null , bean.translate("CON_DISABLE"), bean.getResource("DisableUser"), null) %>  
							<% } %>
							<% if (bean.isActiveAction("ENABLE")) { %>
								&nbsp;<%= bean.getHtmlForActionLink("ACTION_ENABLE", null , bean.translate("CON_ENABLE"), bean.getResource("EnableUser"), null) %>
							<% } %>
							<% if (bean.isActiveAction("APPROVE_USER")) { %>
								&nbsp;<%= bean.getHtmlForActionLink("ACTION_APPROVE_USER", null , bean.translate("CON_APPROVE_USER"), bean.getResource("ApproveUser"), null) %>
							<% } %>
							<% if (bean.isActiveAction("REJECT_USER")) { %>
								&nbsp;<%= bean.getHtmlForActionLink("ACTION_REJECT_USER", null , bean.translate("CON_REJECT_USER"), bean.getResource("RejectUser"), null) %>
							<% } %>
							<% if (bean.isActiveAction("APPROVE_CUSTOMER")) { %>
								&nbsp;<%= bean.getHtmlForActionLink("ACTION_APPROVE_CUSTOMER", null , bean.translate("CON_APPROVE_CUSTOMER"), bean.getResource("ApproveCustomer"), null) %>
							<% } %>
							<% if (bean.isActiveAction("REJECT_CUSTOMER")) { %>
								&nbsp;<%= bean.getHtmlForActionLink("ACTION_REJECT_CUSTOMER", null , bean.translate("CON_REJECT_CUSTOMER"), bean.getResource("RejectCustomer"), null) %>
							<% } %>
							<% if (!wrappedRequest.isHttpServletRequest()) { %>
								&nbsp;<%= bean.getHtmlForActionLink("ACTION_SYNC", null, bean.translate("CON_SYNC_USERS"),bean.getResource("Synchronize"),null) %>
							<% } %>
							&nbsp;<%= bean.getHtmlForActionLink("ACTION_EMAIL", null , bean.translate("CON_SEND_EMAIL"), bean.getResource("SendMail"), null) %>
						</td>
					</tr>		
					<tr>
						<td class="IBSOutput" colspan="7" nowrap>
						<% if (bean.showFirstPage()) { %>
							&nbsp<%= bean.getHtmlForActionLink("ACTION_SEARCH",null, bean.translate("CON_FIRST_PAGE"), bean.getResource("FirstPage"), null) %>     	
       					<% } %>
						 <% if(bean.hasMore()) { %>
							&nbsp;<%= bean.getHtmlForActionLink("ACTION_NEXT", null, bean.translate("CON_NEXT_PAGE"), bean.getResource("NextPage"), null) %>
						<% } %>
            			</td>
            		</tr>		
			 	</table>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	<% } %>
	<!-- Message -->
	<tr>
		<td class="IBSOutput">                				                          
    	<% if(bean.hasCCFMessage()){ %>
    		<small>
        		<%= bean.getMessageHtml(bean.getCCFMessage()) %>
        	</small>
        	<% bean.setCCFMessage(null); %>
   		<%  } %>
   		</td>
	</tr>
    <tr>
		<td class="IBSEmpty">&nbsp;</td>
   	</tr>	
	<!-- Search actions and fields -->
	<tr>
		<td class="IBSHeaderCellLight" nowrap><%= bean.translate("CON_NEW_SEARCH") %></td>
	</tr>
	<tr width="100%">
		<td class="IBSPageTitleDivider" colspan="2" width="100%" height="1"></td>
	</tr>
	<tr>
		<td class="IBSEmpty">&nbsp;</td>
   	</tr>	
	<tr>
      <td nowrap>
      <table class="IBSLayoutTable" cellspacing="0" width="97%">
        	<%-- Include function Search bar and validationErrors --%>
         	<%@ include file="NSAdminFuncSearchBarAndValidationErrors.jspbb" %>          
    	<tr>
    	  <td class="IBSLabelSmall" width="10%" nowrap>
    	  	<%= bean.translate("CON_USER_ID") %>&nbsp;
    	  </td>
    	  <td class="IBSOutput">
          	<input class="IBSInput" type="text" name="EF_CODE" size="30" value="<%= bean.getSelection("CODE") %>">
       	  </td>
        </tr>
        <tr>
    	  <td class="IBSLabelSmall" width="10%" nowrap>
    	  	<%= bean.translate("CON_NAME") %>&nbsp;
    	  </td>
    	  <td class="IBSOutput">
          	<input class="IBSInput" type="text" name="EF_NAME" size="30" value="<%= bean.getSelection("NAME") %>" >
       	  </td>
        </tr>
         <tr>
    	  <td class="IBSLabelSmall" width="10%" nowrap>
    	  	<%= bean.translate("CON_EMAIL_ADDRESS") %>&nbsp;
    	  </td>
    	  <td class="IBSOutput">
          	<input class="IBSInput" type="text" name="EF_EMAIL" size="30" value="<%= bean.getSelection("EMAIL") %>" >
       	  </td>
        </tr>
        <tr>
    	  <td class="IBSLabelSmall" width="10%" nowrap>
    	  	<%= bean.translate("CON_USER_STATUS") %>&nbsp;
    	  </td>
    	  <td class="IBSOutput">
          	<%= bean.getSelectFormUserStatus().getForm("NetStoreAdminDetailForm") %>
       	  </td>
        </tr>     
        	<!-- Include function SearchBar -->
      		<%@ include file="FuncSearchBar.jspbb" %> 
        </table>
      </td>
    </tr>
</table>
</form>