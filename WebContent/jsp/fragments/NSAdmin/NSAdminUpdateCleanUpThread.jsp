<%-- Page for start or stop the wrappedSession clean up thread  --%>
<%-- Imports --%>
<%@ page import="se.ibs.ccf.*, se.ibs.ns.adm.*" %>

<%-- Declarations --%>
<% 
NSAdminBean bean = (NSAdminBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_BEAN, wrappedSession);
String title = bean.translate("CON_UPDATE_SESSION_TRACKER_CLEANUP");
boolean threadStarted = SessionTracker.isCleanUpThreadStarted();
int interval = SessionTracker.getCleanupInterval();
String servletName = bean.getEncodedServlet(NSAdminConstantsServlets.NSADMINUPDATECLEANUPTHREAD_SERVLET, wrappedResponse);
%>

<script language="JavaScript" type="text/javascript">
loaded = false;
function onLoad() {
	loaded = true;
	top.window.blockedLanguageSwitch = true;
}
addOnLoadEvent(onLoad);
</script>

<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminUpdateCleanUpThread" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>

<%-- Title  --%>
<table width="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" width="97%" height="1"></td>
  	</tr>
  	<tr>
  		<td>
  			<%= bean.getHtmlForActionLink("ACTION_BACK", null, bean.translate("CON_BACK_TO_MENU"), bean.getResource("LeftArrow"), null) %>
  		</td>
  	</tr>
	<tr>
		<td class="IBSEmpty">&nbsp;</td>
	</tr>
	<tr>
		<td class="IBSOutput">                				                          
 		<% if(bean.hasCCFMessage()){ %>
 		<small>
    		<%= bean.getMessageHtml(bean.getCCFMessage()) %>
    	</small>
    	<% bean.setCCFMessage(null); %>
 		<%  } %>
   		</td>
	</tr>
  	<tr>
    	<td class="IBSEmpty">&nbsp;</td>
  	</tr>
	<tr>
  		<td>
		<table class="IBSLayoutTable" cellspacing="0">
		
		<tr>
			<td class="IBSLabel"><%= bean.translate("CON_MAX_INACTIVE_SESSION_TIME") %>:</td>
			<td class="IBSOutput" nowrap><%= SessionTracker.getMaxInactiveTime() %>&nbsp;<%= bean.translate("CON_SEC") %></td>
		</tr>
		<tr>
			<td class="IBSLabel"><%= bean.translate("CON_SESSION_TRACKER_CLEANUP_INTERVAL") %>:</td>
			<%	if (interval == 0) { %>
			<td class="IBSOutput" nowrap><%= bean.translate("CON_NO_AUTOMATIC_CLEANUP") %></td>
			<%	} else { %>
			<td class="IBSOutput" nowrap><%= interval %>&nbsp;<%= bean.translate("CON_SEC") %></td>
		<%	} %>
		</tr>

		<% if (threadStarted && interval > 0) { %>
		<tr>
			<td class="IBSLabel"><%= bean.translate("CON_START_CLEAN_UP_THREAD") %>:</td>
			<td><input type="submit" name="ACTION_START" value="<%= bean.translate("CON_START") %>" disabled></td>
		</tr>
		<tr>
			<td class="IBSLabel"><%= bean.translate("CON_STOP_CLEAN_UP_THREAD") %>:</td>
			<td><input type="submit" name="ACTION_STOP" value="<%= bean.translate("CON_STOP") %>"></td>
		</tr>
		<% } else if (!threadStarted && interval > 0) { %>
		<tr>
			<td class="IBSLabel"><%= bean.translate("CON_START_CLEAN_UP_THREAD") %>:</td>
			<td><input type="submit" name="ACTION_START" value="<%= bean.translate("CON_START") %>"></td>
		</tr>
		<tr>
			<td class="IBSLabel"><%= bean.translate("CON_STOP_CLEAN_UP_THREAD") %>:</td>
			<td><input type="submit" name="ACTION_STOP" value="<%= bean.translate("CON_STOP") %>" disabled></td>
		</tr>
		<% } %>

		<tr>
			<td class="IBSLabel" nowrap><%= bean.translate("CON_START_SESSION_TRACKER_CLEANUP_SINGLE") %>:</td>
			<td><input type="submit" name="ACTION_START_DIRECT" value="<%= bean.translate("CON_START") %>"></td>
		</tr>

		<tr><td>&nbsp;</td></tr>
		</table>
		</td>
	</tr>
</table>
</form>