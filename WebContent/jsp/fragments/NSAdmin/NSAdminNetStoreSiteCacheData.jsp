<%-- Page for show NetStores site cache   --%>
<%-- Imports --%>
<%@ page import="java.util.*, se.ibs.ns.adm.*,se.ibs.ccf.*, se.ibs.bap.util.*, se.ibs.ns.cf.*" %>

<%-- Declarations --%>    
<%
NSAdminSiteCacheBean bean = (NSAdminSiteCacheBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_SITECACHE_BEAN, wrappedSession);
String servletName = bean.getEncodedServlet(NSAdminConstantsServlets.NSADMINSITECACHE_SERVLET, wrappedResponse); 
Hashtable siteCacheTable = bean.getTotalNetStoreSiteCache();	
String title = bean.translate("CON_NETSTORE_SITECACHE");
%>

<script language="JavaScript" type="text/javascript">
loaded = false;
function onLoad() {
	loaded = true;
	top.window.blockedLanguageSwitch = true;
}
addOnLoadEvent(onLoad);
</script>

<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminSiteCacheDisplayData" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>

<%-- Title  --%>
<table width="97%" height="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" width="97%" height="1"></td>
  	</tr>
  	<tr valign="top">
  		<td>
  			<%= bean.getHtmlForActionLink("ACTION_BACK", null, bean.translate("CON_BACK"), bean.getResource("LeftArrow"), null) %>
			<% if (!bean.seeAllSitecache()) { %>	
				&nbsp;<%= bean.getHtmlForActionLink("ACTION_SEE_ALL", null, bean.translate("CON_SEE_ALL_INFO"), bean.getResource("SeeAllInfo"), null) %>
			<% } else { %>
				&nbsp;<%= bean.getHtmlForActionLink("ACTION_SEE_SUMMARY", null, bean.translate("CON_SEE_SUMMARY"), bean.getResource("SeeSummary"), null) %>
		<% } %>
		</td>
	</tr>
  	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
  	<tr valign="top">
		<td>
		<table class="IBSLayoutTable" cellspacing="0" width="100%">

		<%-- Displayarea for preloaded data from NetStore site cache --%>

	<% if (siteCacheTable != null) {
		Enumeration en = BAPSorter.sortedKeys(siteCacheTable); 
		while (en.hasMoreElements()) {
			String siteKey = (String)en.nextElement();  %>
			<tr>
				<td class="IBSEmpty">&nbsp;</td>
			</tr> 
		<%	if (siteKey.equalsIgnoreCase(NSAdminSiteCacheBean.NS_SITECACHE_PRELOAD)) { %>
				<tr>
					<td class="IBSHeaderCell" colspan="2" align="center">
						<%= bean.translate("CON_NETSTORE_PRELOAD_DATA") %>
					</td>
				</tr>
		<%	}
			if (siteKey.equalsIgnoreCase(NSAdminSiteCacheBean.NS_SITECACHE_ADDITIONAL)) { %>
				<tr>
					<td class="IBSHeaderCell" colspan="2" align="center">
						<%= bean.translate("CON_NETSTORE_ADDITIONAL_DATA") %>
					</td>
				</tr>
		<%	} %>
		
  		<tr>
			<td class="IBSEmpty">&nbsp;</td>
		</tr>
<%		int i = 0;
		TreeMap sc = (TreeMap)siteCacheTable.get(siteKey);
		Set keySet = sc.keySet();
		Iterator it = keySet.iterator();

		if (bean.seeAllSitecache() == false) {
			while (it.hasNext()) { 
				String key = (String)it.next(); 
				Object obj = (Object)sc.get(key);
				if (i%2 == 0) { %>
       					<tr class="IBSListRow1">
     		<% 		} else { %>
       					<tr class="IBSListRow2">
     		<%		}  i++;	%> 	

					<td class=IBSLabel width="10%" valign="top"><%= key %></td>	
		<%		if ((key.equalsIgnoreCase(NSConstantsSiteCache.TRANSLATION_CACHE)) || (key.equalsIgnoreCase(NSConstantsSiteCache.DESCRIPTION_CACHE))) { 
					Enumeration enSpec = null;
					if (key.equalsIgnoreCase(NSConstantsSiteCache.TRANSLATION_CACHE)) {
						enSpec = BAPSorter.sortedKeys(((TranslationCache)obj).getTranslations()); 
					} else {
						enSpec = BAPSorter.sortedKeys(((DescriptionCache)obj).getDescriptions()); 
					} %>
					<td class="IBSOutput">
			<%		while (enSpec.hasMoreElements()) {
						String tranKey = (String)enSpec.nextElement(); %>
						<a class="IBSLink" href="#" onClick="javascript:invokeLink('<%= bean. getDetailLink(siteKey, key, tranKey ,wrappedResponse) %>'); return false;"> <%= tranKey %></a>&nbsp;
			<%		} %>
					</td>
			<%	} else if (key.equalsIgnoreCase(NSConstantsSiteCache.LANGUAGE_DEPENDENT)) { 
						Enumeration enu = BAPSorter.sortedKeys((Hashtable)obj); %>
						<td class="IBSOutput" valign="top">	
					<%	while(enu.hasMoreElements()) { 
							String langKey = (String)enu.nextElement(); %>
							<a class="IBSLink" href="#" onClick="javascript:invokeLink('<%= bean.getDetailLink(siteKey, key, langKey ,wrappedResponse) %>'); return false;"> <%= langKey %></a>&nbsp;
					<%	} %>
						</td>
				
			<%	} else if (!bean.getDetailLine(key, obj).equalsIgnoreCase("link")) { %>
					<td valign="top" class="IBSOutput"><%= obj.toString() %></td>		
		 	<%	} else { %>	
			 		<td class="IBSOutput" valign="top"><a class="IBSLink" href="#" onClick="javascript:invokeLink('<%= bean.getDetailLink(siteKey, key, null, wrappedResponse) %>'); return false;"> <%= bean.translate("CON_SEE") %>&nbsp;<%= key %> </a></td>		 
			<% 	} %>
				</tr>
	<%		} 
		}  else {
				while (it.hasNext()) { 
					String key = (String)it.next(); 
					Object obj = (Object)sc.get(key);  
       			if (i%2 == 0) { %>
       				<tr class="IBSListRow1">
     		<% 	} else { %>
       				<tr class="IBSListRow2">
     		<%	}  i++;	%> 	

					<td class=IBSLabel width="10%" valign="top"><%= key %></td>	
			<%	if (key.equalsIgnoreCase(NSConstantsSiteCache.TRANSLATION_CACHE)) { %>
					<td valign="top" class="IBSOutput"><%= ((TranslationCache)obj).getTranslations().toString() %></td>
			<%	} else if (key.equalsIgnoreCase(NSConstantsSiteCache.DESCRIPTION_CACHE)) { %>
					<td valign="top" class="IBSOutput"><%= ((DescriptionCache)obj).getDescriptions().toString() %></td>
			<%	} else { %>
					<td valign="top" class="IBSOutput"><%= obj.toString() %></td>		
			<%	} %>
			 </tr>	
  		<%	} 
	 	} 
 	}
 } %>
		<tr><td class="IBSTableFooter" colspan="2" nowrap></td></tr>
		<tr>
			<td class="IBSEmpty">&nbsp;</td>
		</tr>
		</table>
		</td>
	</tr>
</table>
</form>