<%-- Pool menu page --%>
<%-- Imports --%>
<%@ page import="se.ibs.ns.adm.*,se.ibs.ccf.*" %>

<%-- Declarations --%>
<%
NSAdminPoolBean bean = (NSAdminPoolBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_POOL_BEAN,wrappedSession);
String bullet = bean.getResource("BulletBHp");
String servletName = bean.getEncodedServlet(NSAdminConstantsServlets.NSADMINPOOLMENU_SERVLET, wrappedResponse); 
String title = bean.translate("CON_POOL_MENU");
%>

<script language="JavaScript" type="text/javascript">
loaded = false;
function onLoad() {
	loaded = true;
	top.window.blockedLanguageSwitch = true;
}
addOnLoadEvent(onLoad);
</script>

<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminPoolMenu" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>

<%-- Title  --%>
<table width="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" width="97%" height="1"></td>
  	</tr>
  	<tr valign="top">
  		<td>
  			<%= bean.getHtmlForActionLink("ACTION_BACK", null, bean.translate("CON_TO_MAIN_MENU"), bean.getResource("LeftArrow"), null) %>
		</td>
	</tr>
  	<tr>
  		<td class="IBSEmpty">
  			&nbsp;
  		</td>
  	</tr>
	<tr valign="top">
  		<td>
		<%-- Menu options --%> 
		<table border="0" width=100%>
		<tr>
			<td>
				<a class="IBSMenuLink" href="#" onClick="javascript:submitLinkOnce('NetStoreAdminDetailForm', 'ACTION_MONITOR', null); return false">
				<IMG border="0" src="<%= bullet %>">&nbsp;<%= bean.translate("CON_MONITOR_POOL") %></a>
			</td>
		</tr>
		<tr>
			<td>
				<a class="IBSMenuLink" href="#" onClick="javascript:submitLinkOnce('NetStoreAdminDetailForm', 'ACTION_UPDATE', null); return false">
				<IMG border="0" src="<%= bullet %>">&nbsp;<%= bean.translate("CON_CLOSE_POOL") %></a>
			</td>
		</tr>
		<tr>
			<td class="IBSEmpty">&nbsp;</td>
		</tr>
		</table>
		</td>
	</tr>
</table>
</form>