<%-- Page to show NetStore configuration, administration and database connection property files --%>
<%-- Imports --%>
<%@ page import="java.util.*, se.ibs.ns.adm.*, se.ibs.ccf.*, se.ibs.bap.util.*" %>

<%-- Declarations --%>    
<%
NSAdminBean bean = (NSAdminBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_BEAN,wrappedSession);
String servletName = bean.getEncodedServlet(NSAdminConstantsServlets.NSADMINCONFIGURATION_SERVLET, wrappedResponse); 
Hashtable cf = bean.getPropertyFile();
String title = bean.translate("CON_NETSTORE_CONFIGURATION");
%>

<script language="JavaScript" type="text/javascript">
loaded = false;
function onLoad() {
	loaded = true;
	top.window.blockedLanguageSwitch = true;
}
addOnLoadEvent(onLoad);
</script>

<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminConfiguration" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>

<%-- Title  --%>
<table width="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" width="97%" height="1"></td>
  	</tr>
  	<tr valign="top">
  		<td>
  			<%= bean.getHtmlForActionLink("ACTION_MENU", null, bean.translate("CON_BACK_TO_MENU"), bean.getResource("LeftArrow"), null) %>
		</td>
	</tr>
  	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
  	
  	<tr valign="top">
		<td>
		<%-- Display area for NetStore property file name  --%>
		<table class="IBSLayoutTable" cellspacing="0">
		<tr>
		<td class="IBSHeaderCellLight" align="center" nowrap><%= bean.translate("CON_STANDARD_PROPERTY_FILES") %></td>
		</tr>
  		<tr>
			<td class="IBSEmpty">&nbsp;</td>
		</tr>
		<tr>
			<td nowrap class="IBSListCell"><input type="radio" name="CONFIGFILE" value="bap.asw.properties"><%= bean.translate("CON_APPLICATION_PROP") %></td>
		</tr>
		<tr>
			<td nowrap class="IBSListCell"><input type="radio" name="CONFIGFILE" value="asw.naming.properties"><%= bean.translate("CON_APPLICATION_NAMING_CONTEXT_PROP") %></td>
		</tr>
		<tr>
			<td nowrap class="IBSListCell"><input type="radio" name="CONFIGFILE" value="ccf.dbconnection.properties"><%= bean.translate("CON_DATABASE_CONNECTION_PROP") %></td>
		</tr>

		<!-- ccf.StaticConnection.properties removed... -->

		<tr>
			<td nowrap class="IBSListCell"><input type="radio" name="CONFIGFILE" value="adm.config.properties"><%= bean.translate("CON_ADMINISTRATION_CONFIG") %></td>
		</tr>
		<tr>
			<td nowrap class="IBSListCell"><input type="radio" name="CONFIGFILE" value="ns.config.properties"><%= bean.translate("CON_NETSTORE_CONFIGURATION") %></td>
		</tr>
		<%	Vector ev = bean.getExtensionPropertyNames();
			int size = ev.size();
			if (!ev.isEmpty()) { %>
		<tr>
			<td class="IBSEmpty">&nbsp;</td>
		</tr>
		<tr>
			<td class="IBSHeaderCellLight" align="center" nowrap><%= bean.translate("CON_NETSTORE_EXT_PROP_FILES") %></td>
		</tr>
  		<tr>
			<td class="IBSEmpty">&nbsp;</td>
		</tr>
		<%	for (int i = 0; i < size; i++) { %>
		<tr>
			<td class="IBSListCell"><input type="radio" name="CONFIGFILE" value="<%= ev.elementAt(i) %>"><%=ev.elementAt(i)%></td>
		</tr>
		<%	}
				} %>					
		<tr>
			<td>
				<br>
				<input type="submit" name="ACTION_SHOW" value="<%= bean.translate("CON_SHOW") %>" class="IBSPushButtonSmall" onclick="submitButtonOnce(this); return false">
			</td>
		</tr>	
		
		</table>
		</td>
	</tr>
	<tr>
		<td class="IBSEmpty">&nbsp;</td>
	</tr>
<%	if (bean.getConfigFile() != null) { %>	
	<tr valign="top">
		<td>
		<%-- Display area for configuration file --%>
			<table class="IBSLayoutTable" cellspacing="0">
<%		if (cf != null && !cf.isEmpty()) { %>
				<tr><td class=IBSTableHeader colspan="2" align="center"><%= bean.getConfigFile() %></td></tr>
<%			int j = 0;
			Enumeration e = BAPSorter.sortedKeys(cf);
			while (e.hasMoreElements()) {
				String key = (String) e.nextElement(); %>
				<tr class="<%= (j++ % 2 == 0) ? "IBSListRow1" : "IBSListRow2" %>">
					<td class="IBSLabel" width="10%"><%= key %>&nbsp;</td>
<%				String value = (String) cf.get(key);
				if (key.equalsIgnoreCase("password"))
					value = "********"; %>
					<td class="IBSOutput"><%= value %></td>
				</tr>
<%			} %>
				<tr><td class="IBSTableFooter" colspan="2" nowrap></td></tr>
<%		} else { %>
				<tr><td class="IBSTextAttention" nowrap><%= bean.translate("MSG_FILE_DOES_NOT_EXIST") %></td></tr>
<%		} %>
			</table>
		</td>
	</tr>
	<tr>
		<td class="IBSEmpty">&nbsp;</td>
	</tr>
<%	} %>
</table>
</form>