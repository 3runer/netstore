<%-- Page for viewing the wrappedSession detail --%>
<%-- Imports --%>
<%@ page import="java.util.*, se.ibs.ccf.*, se.ibs.ns.adm.*" %>

<%-- Declarations --%>
<% 	
NSAdminSessionTrackingBean bean = (NSAdminSessionTrackingBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_SESSION_TRACKING_BEAN, wrappedSession);	
String title = bean.translate("CON_SESSION_DETAILS");
Session selectedSession = bean.getSelectedSession();
String servletName =  bean.getEncodedServlet(NSAdminConstantsServlets.NSADMINSESSIONTRACKINGDETAIL_SERVLET, wrappedResponse);
%>

<script language="JavaScript" type="text/javascript">
loaded = false;
function onLoad() {
	loaded = true;
	top.window.blockedLanguageSwitch = true;
}
addOnLoadEvent(onLoad);
</script>

<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminSessionTrackingDetail" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>

<%-- Title  --%>
<table width="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" width="97%" height="1"></td>
  	</tr>
  	<tr valign="top">
  		<td>
  			<%= bean.getHtmlForActionLink("ACTION_BACK", null, bean.translate("CON_BACK"), bean.getResource("LeftArrow"), null) %>
		</td>
	</tr>
  	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
  	<tr valign="top">
		<td>
    		<table class="IBSLayoutTable" cellspacing="0" width="97%">
    		<% if (selectedSession != null) { %> 	
    		<tr>
    			<td class="IBSHeaderCell" nowrap align="center" colspan="2"><%= bean.translate("CON_SESSION_ID") %>: &nbsp;<%= selectedSession.getId() %></td>
    		</tr>
    		<% 	Enumeration e = selectedSession.getAttributeNames();
    		int i = 0;
				while(e.hasMoreElements()){
				i++;
					String name = (String)e.nextElement();
					if(name != null && !name.equals("")) { 
						if (i%2 == 0) { %>
        				<tr class="IBSListRow1">
    					<% 	} else { %>
      		 			<tr class="IBSListRow2">
  	  					<%	} %>
    						<td class="IBSLabel" valign="top" width="10%" nowrap><%= name %></td>
    						<td class="IBSOutput" valign="top"><%= selectedSession.getAttribute(name) %></td>
    				<%  } %>	
    					</tr>
    				<% } %>
    	<%  } else { %>
    			<tr>
					<td nowrap align="center"><%= bean.getErrorMessageHtml(bean.translate("MSG_SELECTED_SESSION_NOT_ACTIVE")) %></td>
				</tr>		
    	<%  } %>
			<tr><td class="IBSTableFooter" colspan="2" nowrap></td></tr>
    		</table>
       </td>
  	</tr> 
  	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
</table>
</form>