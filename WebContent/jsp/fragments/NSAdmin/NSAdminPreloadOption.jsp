<%-- Page for preload data in NetStore administration --%>
<%-- Imports --%>
<%@ page import="se.ibs.ns.adm.*, se.ibs.ccf.*, se.ibs.bap.*, se.ibs.bap.cf.*" %>

<%-- Declarations --%>
<%	
NSAdminPreloadBean bean = (NSAdminPreloadBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_PRELOAD_BEAN,wrappedSession);
String servletName = bean.getEncodedServlet(NSAdminConstantsServlets.NSADMINPRELOADOPTION_SERVLET, wrappedResponse); 
String title = bean.translate("CON_PRELOAD_SELECTION");
BAPLanguage pl = bean.getPrimaryLanguage();
String primaryLanguageCodeAndDesc = "?";
if (pl != null) {
	primaryLanguageCodeAndDesc = pl.getCode()+" - "+pl.getDescription();
}
%>

<script language="JavaScript" type="text/javascript">
loaded = false;
function onLoad() {
	loaded = true;
	top.window.blockedLanguageSwitch = true;
}
addOnLoadEvent(onLoad);
</script>

<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminPreloadOption" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>

<%-- Title  --%>
<table width="97%" height="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" width="97%" height="1"></td>
  	</tr>
  	
  	<% if (!bean.isSystemSetup()) { %>
  	<tr valign="top">
  		<td>
  			<%= bean.getHtmlForActionLink("ACTION_MENU", null, bean.translate("CON_BACK_TO_MENU"), bean.getResource("LeftArrow"), null) %>
  		</td>
  	</tr>
  <% } %>
	<tr>
  		<td class="IBSEmpty">
  			&nbsp;
  		</td>
  	</tr>
	<tr>
		<td class="IBSOutput" align="center">                				                          
    	<% if(bean.hasCCFMessage()){ %>
		<br>
    	<small>
        	<%= bean.getMessageHtml(bean.getCCFMessage()) %>
        </small>
        <% bean.setCCFMessage(null); %>
		<br>
		<br>
   		<%  } %>
		</td>
	</tr>
  		
  	<tr valign="top">
  		<td>
		<table class="IBSLayoutTable" cellspacing="0">
		
		<% if (bean.isSystemSetup()) { %>
		<tr>
			<td class="IBSHeaderCellLight" colspan="4"><%= bean.translate("CON_INITILIZE_PRELOAD_DATA") %></td>
		</tr>
		<tr>
  			<td class="IBSPageTitleDivider" width="97%" colspan="2 height="1"></td>
  		</tr>
  		<tr>
    		<td class="IBSEmpty">&nbsp;</td>
  		</tr>
		<input type="hidden" name="QRY_UPDATE_CODE" value="DFT">
		<input type="hidden" name="QRY_FILE" value="*ALL">	
	<% } else { %>
		<tr>
			<td class="IBSHeaderCellLight" colspan="4"><%= bean.translate("CON_PRELOAD_UPDATE") %></td>
		</tr>
		<tr>
  			<td class="IBSPageTitleDivider" width="100%" colspan="2 height="1"></td>
  		</tr>
  		<tr>
    		<td class="IBSEmpty">&nbsp;</td>
  		</tr>
  		<tr>
			<td class="IBSLabel" width="10%" nowrap><%= bean.translate("CON_BACKEND_APPLICATION") %>:</td>
			<td class="IBSOutput" nowrap><%= bean.getVersionManager().getFormatedBackendApplicationVersion() %></td>
		</tr>
		<tr>
			<td class="IBSLabel" width="10%" nowrap><%= bean.translate("CON_PRELOAD_MODE") %>:</td>
    		<td class="IBSOutput" nowrap><%= bean.getHtmlSelectForm("QRY_UPDATE_CODE") %></td>
		</tr>
		<tr>
			<td class="IBSLabel" width="10%" nowrap><%= bean.translate("CON_FILE") %>:</td>
    		<td class="IBSOutput" nowrap><%= bean.getHtmlSelectForm("QRY_FILE") %></td>
		</tr>
		<tr>
			<td class="IBSEmpty">&nbsp;</td>
		</tr>
	<% } %>
		<tr>
			<td class="IBSLabel" width="10%" nowrap><%= bean.translate("CON_PRIMARY_LANGUAGE") %>:</td>
			<td class="IBSOutput" nowrap><%= primaryLanguageCodeAndDesc %></td>
		</tr>
		<tr>
			<td class="IBSLabel" width="10%" nowrap><%= bean.translate("CON_SECONDARY_LANGUAGE") %>:</td>
    		<td class="IBSOutput" nowrap><%= bean.getHtmlSelectForm("QRY_LANGUAGE2") %></td>
		</tr>
		<tr>
			<td class="IBSLabel" width="10%" nowrap><%= bean.translate("CON_HANDLER") %>:</td>
    		<td class="IBSOutput" nowrap><%= bean.getHtmlSelectForm("QRY_HANDLER") %></td>
		</tr>
		<tr>
			<td class="IBSLabel" width="10%"nowrap><%= bean.translate("CON_ORDER_TYPE") %>:</td>
   			<td class="IBSOutput" nowrap><%= bean.getHtmlSelectForm("QRY_ORDER_TYPE") %></td>
		</tr>
		<% if (bean.getVersionManager().handleDeleteOrder()) { %>
			<tr>
				<td class="IBSLabel" width="10%" nowrap><%= bean.translate("CON_LOST_SALES_REASON") %>:</td>
    			<td class="IBSOutput" nowrap><%= bean.getHtmlSelectForm("QRY_LOST_SALES") %></td>
			</tr>
		<% } %>
		<% if (bean.getVersionManager().handleSalesRestrictions()) { %>
			<tr>
				<td class="IBSLabel" width="10%" nowrap><%= bean.translate("CON_ADDRESS_CATEGORY") %>:</td>
    			<td class="IBSOutput" nowrap><%= bean.getHtmlSelectForm("QRY_ADDRESS_CATEGORY") %></td>
			</tr>
		<% } %>
		<% if (bean.getVersionManager().handleBPRMandatoryResolutionCode()) { %>
			<tr>
				<td class="IBSLabel" width="10%" nowrap><%= bean.translate("CON_REQUEST_INITIAL_STATUS") %>:</td>
   				<td class="IBSOutput" nowrap><%= bean.getHtmlSelectForm("QRY_BPR_INITIAL") %></td>
			</tr>
		<% } %>
		<% if (bean.getVersionManager().handleEmailNotification()) { %>
			<tr>
				<td class="IBSLabel" width="10%" nowrap><%= bean.translate("CON_SENDER_EMAIL_ADDRESS") %>:</td>
    			<td class="IBSOutput" nowrap><input class="IBSInput" type="text" name="QRY_SENDER_EMAIL" value="<%= bean.formatText(bean.getSenderEmailAddress()) %>" size="35" maxlength="35"></td>
			</tr>
		<% } %>
		<tr>
			<td class="IBSLabel" width="10%" nowrap><%= bean.translate("CON_FROM_DATE") %>:</td>
    		<td class="IBSOutput" nowrap><input class="IBSInput" type="text" name="QRY_FROM_DATE" value="<%= bean.formatText(bean.getFromDate()) %>">&nbsp;<%= bean.translateDateInputFormatYYYYMMDD() %></td>
		</tr>
		<tr>
			<td class="IBSLabel" width="10%" nowrap><%= bean.translate("CON_TO_DATE") %>:</td>
   			<td class="IBSOutput" nowrap><input class="IBSInput" type="text" name="QRY_TO_DATE" value="<%= bean.formatText(bean.getToDate()) %>"></td>
		</tr>
		<tr>
			<td class="IBSEmpty">&nbsp;</td>
    		<td><input name="ACTION_LOAD" type="submit" VALUE="<%= bean.translate("CON_LOAD") %>" onClick="javascript:submitButtonConfirmation('<%= bean.translate("CON_CONFIRM_PRELOAD") %>', '<%= servletName %>', this); return false"></td>
		</tr>
		</table>
		</td>
	</tr>
</table>
</form>