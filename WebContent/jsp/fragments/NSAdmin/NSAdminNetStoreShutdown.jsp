<%-- Page for shutdown NetStore --%>
<%-- Imports --%>
<%@ page import="se.ibs.ns.adm.*,se.ibs.ccf.*" %>

<%-- Declarations --%>     
<%
NSAdminNetStoreShutdownBean bean = (NSAdminNetStoreShutdownBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_NS_SHUTDOWN_BEAN, wrappedSession);
String servletName = bean.getEncodedServlet(NSAdminConstantsServlets.NSADMINNETSTORESHUTDOWN_SERVLET, wrappedResponse); 
int applicationStatus = bean.getNetStoreStatus();
String title = bean.translate("CON_SHUTDOWN_OF_NETSTORE");
boolean isOneLanguage = bean.isOneLanguage();
%>

<script language="JavaScript" type="text/javascript">
loaded = false;
function onLoad() {
	loaded = true;
	top.window.blockedLanguageSwitch = true;
}
addOnLoadEvent(onLoad);
</script>

<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminNetStoreShutdown" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>

<%-- Title  --%>
<table width="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" width="97%" height="1"></td>
  	</tr>
  	<tr valign="top">
  		<td>
  			<%= bean.getHtmlForActionLink("ACTION_MENU", null, bean.translate("CON_BACK_TO_MENU"), bean.getResource("LeftArrow"), null) %>
			<% if (applicationStatus == ApplicationStatus.CLOSING || applicationStatus == ApplicationStatus.CLOSED_FOR_NEW) { %>
				&nbsp;<%= bean.getHtmlForActionLink("ACTION_CANCEL", null, bean.translate("CON_CANCEL_CLOSE"), bean.getResource("LeftArrow"), null) %>
			<% } %>	
  		</td>
  	</tr>
  	<tr>
  		<td class="IBSEmpty">
  			&nbsp;
  		</td>
  	</tr>
  	<%	if (applicationStatus == ApplicationStatus.CLOSED_FOR_NEW) { %>	
	<tr>
		<td class="IBSOutput" nowrap>
		<small>
			<%= bean.getMessageHtml(bean.getCloseForNewMessage()) %>
		</small>
		</td>
	</tr>
	<%  } %>
  	<tr>
		<td class="IBSOutput">    
		<!-- Message -->            				                          
    	<% if(bean.hasCCFMessage()){ %>
    		<small>
        		<%= bean.getMessageHtml(bean.getCCFMessage()) %>
        	</small>
        	<% bean.setCCFMessage(null); %>
			<br>
			<br>
   	<%  } %>
   		</td>
	</tr>
  	<%-- Display area for total shutdown of NetStore  --%>
  	<tr>
  		<td>
  		<table class="IBSLayoutTable" cellspacing="0" width="100%">
		<% 	if (applicationStatus == ApplicationStatus.OPEN) { %>
		<tr>
			<td class="IBSHeaderCellLight" colspan="2" nowrap><%= bean.translate("CON_CLOSE_NETSTORE_WITH_NOTIFICATION") %></td>
		</tr>
		<tr>
  			<td class="IBSPageTitleDivider" width="100%" colspan="2 height="1"></td>
  		</tr>
		<tr>
			<td nowrap class="IBSAdmOutput"><br><input type="Checkbox" name="CB_NO_SESSION" value="TRUE">&nbsp; <%= bean.translate("CON_DO_NOT_ALLOW_NEW_SESSIONS") %></td>
		</tr>
		<tr>
			<td nowrap class="IBSAdmOutput"><input type="Checkbox" name="CB_MESSAGE" value="TRUE">&nbsp; <%= bean.translate("MSG_NOTIFY_ACTIVE_USER_SHUTDOWN_NETSTORE_IN") %> &nbsp;<input class="IBSInput" type="text" name="QRY_MINUTES" size="1" maxlength="2"> <%= bean.translate("CON_MINUTES") %>.<br></td>
		</tr>
		<tr>
  			<td class="IBSEmpty">&nbsp;</td>
  		</tr>
  	
		<!-- Message to send -->
		<tr>
			<td>
			<table>
			<tr>
				<td class="IBSLabelSmall" nowrap><%= bean.translate("CON_CLOSE_MESSAGE") %>&nbsp;</td>
			</tr>
			<tr>
				<td class="IBSLabelSmall" nowrap><textarea class="IBSTextArea" name="TA_DEFAULT_MESSAGE" rows="5" cols="100" disabled> <%= bean.translate("MSG_STANDARD_CLOSE_MESSAGE") %></textarea>&nbsp;</td>
			</tr>
			<tr>
				<td class="IBSLabelSmall" align="middle" nowrap>
				
				
				<!-- Enter a close message below if the default close message above shall be replaced.
				 <b>Note:</b> Only the default close message above are translated to user language. -->
				 <br>
				 <br>
				<%= bean.translate("TXT_ADM_004") %><br>
				<% if (!isOneLanguage) { %>
					<%= bean.translate("TXT_ADM_005") %><br>
				<% } %>
				</td>
			</tr>
			<tr>
				<td class="IBSLabelSmall" nowrap>&nbsp;</td>
			</tr>
			<tr>
				<td class="IBSLabelSmall" nowrap><%= bean.translate("CON_CLOSE_MESSAGE") %>&nbsp;</td>
			</tr>
			<tr>
				<td class="IBSLabelSmall" nowrap><textarea class="IBSTextArea" rows="5" name="TA_NEW_CLOSE_MESSAGE" cols="100"><%= bean.getNewCloseMessage() %> </textarea>&nbsp;</td>
			</tr>		
			<tr>
				<td align="center">
				<input type="submit" name="ACTION_CLEAR_TEXT" value="<%= bean.translate("CON_CLEAR_TEXTS") %>" align="right" onclick="submitButtonOnce(this); return false">
				<input type="submit" name="ACTION_CLOSE" value="<%= bean.translate("CON_CLOSE") %>" onclick="javascript:submitButtonOnce(this); return false;"><br>
				</td>
			</tr>
			</table>
			</td>
		</tr>
		</table>
		</td>
	</tr>
			
<% 	} 
 	if (applicationStatus == ApplicationStatus.CLOSING || applicationStatus == ApplicationStatus.CLOSED_FOR_NEW) { %>
	<tr>
		<td class="IBSHeaderCellLight" nowrap><%= bean.translate("CON_SHUTDOWN_NETSTORE") %></td>
	</tr>
	<tr>
  		<td class="IBSPageTitleDivider" width="100%" height="1"></td>
  	</tr>

	<tr>
		<td nowrap class="IBSAdmOutput"><input type="Checkbox" name="CLOSE_POOL" value="TRUE">&nbsp;<%= bean.translate("CON_CLOSE_POOL") %></td>
	</tr>
	<tr>
		<td nowrap class="IBSAdmOutput"><input type="Checkbox" name="RESET_SITECACHE" value="TRUE">&nbsp;<%= bean.translate("CON_CLEAR_SITE_CACHE") %><br></td>
	</tr>
	<tr>
		<td align="center">
			<input type="submit" name="ACTION_SHUTDOWN" value="<%= bean.translate("CON_SHUTDOWN") %>" onclick="javascript:submitButtonOnce(this); return false;">
		</td>
	</tr>
<% 	}
 	if (applicationStatus == ApplicationStatus.CLOSED) { %>
	<tr>
		<td class="IBSHeaderCellLight" nowrap><%= bean.translate("CON_OPEN_NETSTORE") %></td>
	</tr>
	<tr>
  		<td class="IBSPageTitleDivider" width="100%" height="1"></td>
  	</tr>
	<tr>
		<td nowrap align="center">
		<br>
		<input type="submit" name="ACTION_OPEN" value="<%= bean.translate("CON_OPEN") %>" onclick="javascript:submitButtonOnce(this); return false;">
		</td>
	</tr>
<% 	} %>
</table>
</form>