<%-- This page display the sitecache details in a table --%>
<%-- Imports --%>
<%@ page import="java.util.*, se.ibs.ns.adm.*,se.ibs.ccf.*, se.ibs.ns.cf.*, se.ibs.bap.util.*" %>

<%-- Declarations --%>    
<%
NSAdminSiteCacheBean bean =
	(NSAdminSiteCacheBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_SITECACHE_BEAN,wrappedSession);
String servletName = bean.getEncodedServlet(NSAdminConstantsServlets.NSADMINSITECACHEDETAILS_SERVLET, wrappedResponse); 
String cacheKey = bean.getCacheKey();
String title = bean.translate("CON_SITE_CACHE_DETAILS");
%>

<script language="JavaScript" type="text/javascript">
loaded = false;
function onLoad() {
	loaded = true;
	top.window.blockedLanguageSwitch = true;
}
addOnLoadEvent(onLoad);
</script>

<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminSiteCacheDisplayDetails" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>

<%-- Title  --%>
<table width="97%" height="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" width="97%" height="1"></td>
  	</tr>
  	<tr valign="top">
  		<td>
  			<%= bean.getHtmlForActionLink("ACTION_BACK", null, bean.translate("CON_BACK"), bean.getResource("LeftArrow"), null) %>
		</td>
	</tr>
  	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
  	<tr valign="top">
		<td>
		<table class="IBSLayoutTable" cellspacing="0" width="100%">

		<%-- Show DefaultWebUser  --%>
		<% 	Object obj = bean.getDetailSitecache();	 
			String tableTitle = (String)wrappedRequest.getAttribute("TITLE"); %>
		<tr>
			<td class="IBSHeaderCellLight" colspan="2"><%= tableTitle %></td> 
		</tr>
		<tr>
  			<td class="IBSPageTitleDivider" width="100%" colspan="2 height="1"></td>
  		</tr>
  		<tr>
			<td class="IBSEmpty">&nbsp;</td>
		</tr>

	<%	if ((obj instanceof WebUserCacheBean) || (obj instanceof TranslationCache) || (obj instanceof DescriptionCache)) {
			TreeMap map = null;
			if (obj instanceof WebUserCacheBean) {
				map = new TreeMap(((WebUserCacheBean)obj).getData());
			} else if (obj instanceof TranslationCache) {
				map = new TreeMap((Hashtable)((TranslationCache)obj).getTranslations().get(cacheKey));
			} else {
				map = new TreeMap((Hashtable)((DescriptionCache)obj).getDescriptions().get(cacheKey));
			}
 			int i = 0;
            Set keySet = map.keySet();
            for (Object key : keySet) { %> 
				<tr class="<%=(i++ % 2 == 0) ? "IBSListRow1" : "IBSListRow2"%>">
					<td class="IBSLabel" valign="top" width="10%" nowrap><%=key%></td>
					<td valign="top" class="IBSOutput"><%=(map.get(key) == null) ? "" : map.get(key)%></td>
				</tr>
	<%		} 
  		} else if (obj instanceof Hashtable) {
  			int i = 0;
			Hashtable hashtable = (Hashtable) obj;
            if (tableTitle.startsWith(NSConstantsSiteCache.LANGUAGE_DEPENDENT))
            	hashtable = (Hashtable) hashtable.get(cacheKey);
            Enumeration e = BAPSorter.sortedKeys(hashtable);
			while (e.hasMoreElements()) {
				Object key = e.nextElement(); %>
				<tr class="<%=(i++ % 2 == 0) ? "IBSListRow1" : "IBSListRow2"%>"> 
					<td class="IBSLabel" valign="top" width="10%" nowrap><%=key%></td>
					<td valign="top" class="IBSOutput"><%=(hashtable.get(key) == null) ? "" : hashtable.get(key)%></td>
				</tr>
	<%		} 
  		} else if (obj instanceof Vector) {
  			int i = 0;
			for (Object element : (Vector) obj) { %>
				<tr class="<%=(i++ % 2 == 0) ? "IBSListRow1" : "IBSListRow2"%>">
					<td valign="top" class="IBSOutput"><%=(element == null) ? "" : element%></td>
				</tr>
	<%		} 
		} %> 
		</table>
		</td>
	</tr>
</table>
</form>