<%-- Page for monitor a pool --%>
<%-- Imports --%>
<%@ page import="java.util.*, se.ibs.ns.adm.*, se.ibs.ccf.*, se.ibs.bap.util.*" %>

<%-- Declarations --%>
<%
NSAdminPoolBean bean = (NSAdminPoolBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_POOL_BEAN,wrappedSession);
String servletName = bean.getEncodedServlet(NSAdminConstantsServlets.NSADMINPOOLMONITOR_SERVLET, wrappedResponse); 
Vector vect = bean.getMonitorData();
String monitorForm = bean.getHtmlSelectForm("QRY_POOLNAME");
String title = bean.translate("CON_MONITOR_POOL");
%>

<script language="JavaScript" type="text/javascript">
loaded = false;
function onLoad() {
	loaded = true;
	top.window.blockedLanguageSwitch = true;
}
addOnLoadEvent(onLoad);
</script>

<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminPoolMonitor" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>

<table width="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" colspan="2" width="97%" height="1"></td>
  	</tr>
  	<tr valign="top" colspan="2">
  		<td>
  			<%= bean.getHtmlForActionLink("ACTION_MENU", null, bean.translate("CON_BACK_TO_MENU"), bean.getResource("LeftArrow"), null) %>
		</td>
	</tr>
  	<tr>
  		<td class="IBSEmpty">
  			&nbsp;
  		</td>
  	</tr>	
  	<tr>
  		<td class="IBSEmpty">
  			&nbsp;
  		</td>
  	</tr>	
	<tr valign="top">
		<td>
		<table class="IBSLayoutTable" cellspacing="0" width="100%">
		<% if (monitorForm != null) { %>
		<tr>
			<td class="IBSLabel" nowrap width="10%"><%= bean.translate("CON_SELECT_POOL_TO_MONITOR") %>:</td>
			<td class="IBSOutput" nowrap><%= monitorForm %></td>
		</tr>
		<tr>
			<td class="IBSLabel" nowrap><%= bean.translate("CON_SHOW_CONFIG") %>:</td>
			<td class="IBSAdmOutput"> <input type="Checkbox" name="SHOW_CFG" value="ON"><br></td>
		</tr>
		<tr>
			<td class="IBSLabel" nowrap>&nbsp;</td>
			<td>
				<input type="submit" name="ACTION_SHOW" value="<%= bean.translate("CON_SHOW") %>" onClick="submitButtonOnce(this); return false">
				<input type="submit" name="ACTION_REFRESH" value="<%= bean.translate("CON_REFRESH") %>" onClick="submitButtonOnce(this); return false">
			</td>
		</tr>
		<% } else { %>
		<tr>
			<td class="IBSOutput" colspan="2"  nowrap>&nbsp;<%= bean.translate("CON_NO_POOL_TO_MONITOR") %></td>
		</tr>
		<% } %>	
		<tr>
			<td class="IBSEmpty">&nbsp;</td>
		</tr>
		</table>
		</td>
	</tr>

<%-- Display area for pool monitor result --%>
	<tr valign="top">
		<td>
		<table class="IBSLayoutTable">

		<% if (vect != null) { 
			int size = vect.size();
			for (int i=0; i < size; i++) {
				BAPCodeAndDescription cad = (BAPCodeAndDescription)vect.elementAt(i);	
				String key = cad.getCode();
			
				if (key.equalsIgnoreCase("TITLE")) { %>
				<tr><td class="IBSTableHeader" colspan=2 align="center"><%=cad.getDescription() %></td></tr>
			<%	} else {	
					if (i%2 == 0) { %>
        			<tr class="IBSListRow1">
    			<% 	} else { %>
       				<tr class="IBSListRow2">
    			<% 	} %>
					<td class=IBSLabel width="20%" nowrap><%= cad.getCode() %></td>
					<td class="IBSOutput"><%= cad.getDescription() %></td>
				</tr>
		<%	} 
		} %>
		<tr><td class="IBSTableFooter" colspan="2" nowrap></td></tr>
<%	} %>
		</table>
		</td>
	</tr>
	<tr>
		<td class="IBSEmpty">&nbsp;</td>
	</tr>
</table>
</form>