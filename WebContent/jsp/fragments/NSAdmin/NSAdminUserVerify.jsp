<%-- Page to verify certain user browser actions --%>
<%-- Imports --%>
<%@ page import="java.util.*, se.ibs.ns.adm.*,se.ibs.ccf.*, se.ibs.bap.cf.*, se.ibs.bap.web.*" %>

<%-- Declarations --%>
<%
NSAdminUserBrowserBean bean = (NSAdminUserBrowserBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_USER_BROWSER_BEAN, wrappedSession);
String servletName = bean.getEncodedServlet(NSAdminConstantsServlets.NSADMINUSERVERIFY_SERVLET, wrappedResponse); 	
String title = bean.translate("CON_WORK_WITH_USERS");
String action = bean.getAction();
if (action != null) {
	if (action.equals("APPROVE_USER")) {
		title = title + " - " + bean.translate("CON_APPROVE_USER");
	} else if (action.equals("APPROVE_CUSTOMER")) {
		title = title + " - " + bean.translate("CON_APPROVE_CUSTOMER");
	} else if (action.equals("REJECT_USER")) {
		title = title + " - " + bean.translate("CON_REJECT_USER");
	} else if (action.equals("REJECT_CUSTOMER")) {
		title = title + " - " + bean.translate("CON_REJECT_CUSTOMER");
	} else if (action.equals("EMAIL")) {
		title = title + " - " + bean.translate("CON_SEND_EMAIL");
	}
}
Vector users = bean.getSelectedUsers();
boolean isOneLanguage = bean.isOneLanguage();
int columns = 6;
if (!isOneLanguage) {
	columns = 7;
}
String userSP = "&U";
String userNameSP = "&N";
String passwordSP = "&P";
String customerSP = "&C";
String warningImage = null;
%>

<script language="JavaScript" type="text/javascript">
loaded = false;
function onLoad() {
	loaded = true;
	top.window.blockedLanguageSwitch = true;
}
addOnLoadEvent(onLoad);

function clearMailTexts() {
	document.getElementById("NSAdminUserVerify").EF_SUBJECT.value = '';
	document.getElementById("NSAdminUserVerify").TA_MESSAGE.value = '';	
}
</script>

<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminUserVerify" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>

<%-- Title  --%>
<table width="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" width="97%" height="1"></td>
  	</tr>
  	<tr valign="top">
  		<td>
  			<%= bean.getHtmlForActionLink("ACTION_BACK", null, bean.translate("CON_BACK_TO_MENU"), bean.getResource("LeftArrow"), null) %>
			&nbsp;<%= bean.getHtmlForActionLink("ACTION_VERIFY_OK", null, bean.translate("CON_OK"), bean.getResource("OKSmall"), null) %>
			&nbsp;<%= bean.getHtmlForActionLink("ACTION_VERIFY_CANCEL", null, bean.translate("CON_CANCEL"), bean.getResource("CancelSmall"), null) %>
		</td>
	</tr>
	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
	<!-- User table -->
	<tr>
  		<td colspan="2">			    
			<table class="IBSLayoutTable" cellspacing="0" width="97%">
				<tr>
					<td class="IBSHeaderCell" nowrap>&nbsp;</td>
					<td class="IBSHeaderCell" nowrap><%= bean.translate("CON_USER_ID") %>&nbsp;</td>
					<td class="IBSHeaderCell" nowrap><%= bean.translate("CON_NAME") %>&nbsp;</td>
					<% if(!isOneLanguage) { %>
						<td class="IBSHeaderCell" nowrap>
							<%= bean.translate("COH_LANGUAGE") %>
							&nbsp;						
						</td>
					<% } %>
					<td class="IBSHeaderCell" nowrap><%= bean.translate("CON_CUSTOMER_NUMBER") %>&nbsp;</td>
					<td class="IBSHeaderCell" nowrap><%= bean.translate("CON_NAME") %>&nbsp;</td>
					<td class="IBSHeaderCell"><%= bean.translate("CON_EMAIL_ADDRESS") %>&nbsp;</td>
				</tr>
				
				<% BAPWebUser user = null;
				   BAPCustomer customer = null;
				   int size = users.size(); 					
				   for(int i = 0; i<size; i++) {
  					  user = (BAPWebUser)users.elementAt(i);
  					  customer = bean.getCustomer(user);
  					 	if (i%2 == 0) { %>
        					<tr class="IBSListRow1">
    					<% } else { %>
      		 				<tr class="IBSListRow2">
  	  					<% } %>
  	  					    <td class="IBSListCell" nowrap>
  	  					    	<% if (action.startsWith("APPROVE_") && bean.hasWarning(user)) {
  	  					    		warningImage = bean.getResource("Warning"); %>
  	  					    		<img border="0" align="middle" src="<%= warningImage %>">
  	  					    	<% } else { %>
  	  					    		&nbsp;
  	  					    	<% } %>
  	  					    </td>
   							<td class="IBSListCell" nowrap><%= user.getCode() %>&nbsp;</td>
							<td class="IBSListCell" nowrap><%= user.getName() %>&nbsp;</td>
							<% if(!isOneLanguage) { %>
								<td class="IBSListCell" nowrap>								
									<%= bean.getUserLanguageCode(user) %>
								</td>	
							<% } %>
							<% if (customer == null) { %>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							<% } else { %>
								<td class="IBSListCell" nowrap><%= customer.getCode() %>&nbsp;</td>
								<td class="IBSListCell" nowrap><%= customer.getName() %>&nbsp;</td>
							<% } %>	
							<td class="IBSListCell" nowrap><%= user.getEmail() %>&nbsp;</td>					
						   </tr>
				  <% } %>	
				<tr><td class="IBSTableFooter" colspan="<%= columns %>" nowrap></td></tr>		
			 </table>
		</td>
	</tr>
	<% if (warningImage != null) { %>
	<tr>
		<td>
			<%= bean.getWarningMessageHtml(bean.translate("MSG_WARNING_USER_WITH_SAME_EMAIL_EXIST")) %>
		</td>
	</tr>
	<% } %>
	<!-- Message -->
	<tr>
		<td class="IBSOutput">                				                          
    	<% if(bean.hasCCFMessage()){ %>
    		<small>
        		<%= bean.getMessageHtml(bean.getCCFMessage()) %>
        	</small>
        	<% bean.setCCFMessage(null); %>
   		<%  } %>
   		</td>
	</tr>
	<!-- Error messages -->
    <% if (bean.hasValidationError()) { 
         for (int i = 0; i < bean.getValidationErrors().size(); i++){ %> 
         <tr>
            <td class="IBSOutput" colspan="2">
              <%= bean.getErrorMessageHtml((String)bean.getValidationErrors().elementAt(i)) %>
            </td>
         </tr>
      <% } %> 
    <% } %>
	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
	<!-- Mail to send -->
	<tr>
  		<td colspan="2">
		<table class="IBSLayoutTable" cellspacing="0" width="97%">
		<% if (!action.equals("EMAIL")) { %>
			<tr>
				<td class="IBSLabelSmall" nowrap>
					<%= bean.translate("CON_SEND_EMAIL") %>
				</td>
				<td class="IBSOutput" nowrap>
					<input type="checkbox" name="CB_SEND_MAIL" value="" checked>
				</td>
			</tr>
			<tr>
				<td class="IBSLabelSmall" nowrap><%= bean.translate("CON_SUBJECT") %>&nbsp;</td>
				<td class="IBSOutput" nowrap><input class="IBSInput" type="text" name="EF_DEFAULT_SUBJECT" size="60" maxlength="44" value="<%= bean.getDefaultEmailText(action,"SUBJECT",null,null,false) %>" disabled>&nbsp;</td>
				
			</tr>
			<tr>
				<td class="IBSLabelSmall" nowrap><%= bean.translate("CON_MESSAGE_TEXT") %>&nbsp;</td>
				<td class="IBSLabelSmall" colspan="2" nowrap><textarea class="IBSTextArea" name="TA_DEFAULT_MESSAGE" rows="5" cols="100" disabled><%= bean.getDefaultEmailText(action,"MESSAGE",null,user,true) %></textarea>&nbsp;</td>
			</tr>
			<tr>
  				<td class="IBSEmpty">&nbsp;</td>
  			</tr>
			<tr>
				<td class="IBSLabelSmall" nowrap>&nbsp;</td>
				<td class="IBSLabelSmall" colspan="2">
				<!-- Enter subject and/or message text below if the default mail above shall be replaced.
					 Note: Only the default mail texts above are translated to user language. -->
					<%= bean.translate("TXT_ADM_001") %><br>
					<% if (!isOneLanguage) { %>
						<%= bean.translate("TXT_ADM_002") %><br>
					<% } %>
				</td>
			</tr>
		 <% } %>
			<tr>
				<td class="IBSLabelSmall" nowrap><%= bean.translate("CON_SUBJECT") %></td>
				<td class="IBSLabelSmall" nowrap>
					<input class="IBSInput" type="text" name="EF_SUBJECT" size="60" value="<%= bean.getEmailText(action,"SUBJECT") %>" maxlength="44">
				</td>
			</tr>
			<tr>
				<td class="IBSLabelSmall" nowrap><%= bean.translate("CON_MESSAGE_TEXT") %></td>
				<td class="IBSLabelSmall" colspan="2" nowrap><textarea class="IBSTextArea" rows="5" name="TA_MESSAGE" cols="100"><%= bean.getEmailText(action,"MESSAGE") %></textarea>&nbsp;</td>
			</tr>
			<tr>
				<td class="IBSLabelSmall" nowrap>&nbsp;</td>
				<td class="IBSLabelSmall" nowrap>
					<input type="submit" name="ACTION_CLEAR_TEXT" value="<%= bean.translate("CON_CLEAR_TEXTS") %>" class="IBSPushButtonSmall" align="right" onClick="clearMailTexts(); return false">
				</td>
			</tr>
			<tr>	
				<td class="IBSLabelSmall" nowrap>&nbsp;</td>
				<td class="IBSLabelSmall" colspan="2">
					<!-- The following message text substitute variables can be used to retrieve user specific values. -->
					<%= bean.translate("TXT_ADM_003") %><br>
				</td>
			</tr>
			<tr>
				<td class="IBSLabelSmall" nowrap>&nbsp;</td>
				<td class="IBSLabelSmall" colspan="2" nowrap>
					<b><%= customerSP %></b>&nbsp;-&nbsp;<%= bean.translate("CON_CUSTOMER_NUMBER") %> 
				</td>
			</tr>
			<tr>
				<td class="IBSLabelSmall" nowrap>&nbsp;</td>
				<td class="IBSLabelSmall" colspan="2" nowrap>
					<b><%= userNameSP %></b>&nbsp;-&nbsp;<%= bean.translate("CON_USER_NAME") %> 
				</td>
			</tr>
			<tr>
				<td class="IBSLabelSmall" nowrap>&nbsp;</td>
				<td class="IBSLabelSmall" colspan="2" nowrap>
					<b><%= passwordSP %></b>&nbsp;-&nbsp;<%= bean.translate("CON_PASSWORD") %> 
				</td>
			</tr>
			<tr>
				<td class="IBSLabelSmall" nowrap>&nbsp;</td>
				<td class="IBSLabelSmall" colspan="2" nowrap>
					<b><%= userSP %></b>&nbsp;-&nbsp;<%= bean.translate("CON_USER_ID") %> 
				</td>
			</tr>					
		</table>
		</td>
	</tr>
	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
</table>
</form>