<%-- Start page without frames but with a layout looking like frames --%>

<%-- Imports --%>
<%@ page import="se.ibs.ns.adm.*,se.ibs.ccf.*" %>

<%-- Declarations --%>
<%
String bullet = baseBean.getResource("BulletBHp");
int applicationStatus = baseBean.getNetStoreStatus();
String servletName = baseBean.getEncodedServlet(NSAdminConstantsServlets.NSADMINMENU_SERVLET, wrappedResponse); 
String title = baseBean.translate("CON_ADMINISTRATION_MENU");
%>

<script language="JavaScript" type="text/javascript">
loaded = false;
function onLoad() {
	loaded = true;
	top.window.blockedLanguageSwitch = true;
}
addOnLoadEvent(onLoad);
</script>

<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminMenu" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>

<%-- Title  --%>
<table width="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" width="97%" height="1"></td>
  	</tr>
  	<tr>
  		<td class="IBSEmpty">
  			&nbsp;
  		</td>
  	</tr>
  	<tr valign="top">
  		<td>
		<%-- Menu options --%> 
		<table border="0" width=100%>
		<% if (baseBean.isValidFunction("PRELOAD")) { %>
		<tr>	
			<td nowrap>
				<a class="IBSMenuLink" href="#" onclick="javascript:invokeLink('<%= baseBean.getMenuLink("ACTION_PRELOAD", wrappedResponse) %>'); return false">
				<img border="0" src="<%= bullet %>">&nbsp;<%= baseBean.translate("CON_PRELOAD") %></a>
			</td>	
		</tr>
		<% } %>
		<% if (baseBean.isValidFunction("CLEANUP_THREAD")) { %>
		<tr>	
			<td nowrap>
				<a class="IBSMenuLink" href="#" onclick="javascript:invokeLink('<%= baseBean.getMenuLink("ACTION_CLEANUP_THREAD", wrappedResponse) %>'); return false"> 
				<img border="0" src="<%= bullet %>">&nbsp;<%= baseBean.translate("CON_UPDATE_SESSION_TRACKER_CLEANUP") %></a>	
			</td>
		</tr>
		<% } %>
		<% if (baseBean.isValidFunction("SHUTDOWN")) { %>
		<tr>	
			<% if (applicationStatus != ApplicationStatus.CLOSED) { %>
			<td nowrap>
				<a class="IBSMenuLink" href="#" onclick="javascript:invokeLink('<%= baseBean.getMenuLink("ACTION_SHUTDOWN", wrappedResponse) %>'); return false">
				<img border="0" src="<%= bullet %>">&nbsp;<%= baseBean.translate("CON_CLOSE_SHUTDOWN_NETSTORE") %></a>
			</td>
			<% } else { %>
			<td nowrap>
				<a class="IBSMenuLink" href="#" onclick="javascript:invokeLink('<%= baseBean.getMenuLink("ACTION_SHUTDOWN", wrappedResponse) %>'); return false">
				<img border="0" src="<%= bullet %>">&nbsp;<%= baseBean.translate("CON_OPEN_NETSTORE") %></a>
			</td>
		<% } %>
		</tr>
		<% } %>
		<% if (baseBean.isValidFunction("SITECACHE")) { %>
		<tr>
			<td nowrap>
				<a class="IBSMenuLink" href="#" onclick="javascript:invokeLink('<%= baseBean.getMenuLink("ACTION_SITECACHE", wrappedResponse) %>'); return false">
				<img border="0" src="<%= bullet %>">&nbsp;<%= baseBean.translate("CON_WORK_NETSTORE_SITE_CACHE") %></a>
			</td>
		</tr>
		<% } %>
		<% if (baseBean.isValidFunction("POOL")) { %>
		<tr>
			<td nowrap>
				<a class="IBSMenuLink" href="#" onclick="javascript:invokeLink('<%= baseBean.getMenuLink("ACTION_POOL", wrappedResponse) %>'); return false"> 
				<img border="0" src="<%= bullet %> ">&nbsp;<%= baseBean.translate("CON_WORK_WITH_POOLS") %></a>
			</td>
		</tr>
		<% } %>
		<% if (baseBean.isValidFunction("CONFIGURATION")) { %>
		<tr>
			<td nowrap>
				<a class="IBSMenuLink" href="#" onclick="javascript:invokeLink('<%= baseBean.getMenuLink("ACTION_CONFIGURATION", wrappedResponse) %>'); return false"> 
				<img border="0" src="<%= bullet %>">&nbsp;<%= baseBean.translate("CON_SHOW_CONFIGURATION") %></a>
			</td>
		</tr>
		<% } %>
		<% if (baseBean.isValidFunction("SESSION")) { %>
		<tr>
			<td nowrap>
				<a class="IBSMenuLink" href="#" onclick="javascript:invokeLink('<%= baseBean.getMenuLink("ACTION_SESSION", wrappedResponse) %>'); return false"> 
				<img border="0" src="<%= bullet %> ">&nbsp;<%= baseBean.translate("CON_SESSION_TRACKING") %></a>
			</td>
		</tr>
		<% } %>
		<% if (baseBean.isValidFunction("USER")) { %>
		<tr>
			<td nowrap>
				<a class="IBSMenuLink" href="#" onclick="javascript:invokeLink('<%= baseBean.getMenuLink("ACTION_USER", wrappedResponse) %>'); return false"> 
				<img border="0" src="<%= bullet %> ">&nbsp;<%= baseBean.translate("CON_WORK_WITH_USERS") %></a>
			</td>
		</tr>
		<% } %>
		</table>
		</td>
	</tr>
</table>	
</form>