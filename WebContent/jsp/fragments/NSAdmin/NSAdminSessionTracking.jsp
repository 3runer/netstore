<%-- Page for wrappedSession administration tasks --%>
<%-- Imports --%>
<%@ page import="java.util.*, se.ibs.ccf.*, se.ibs.ns.adm.*" %>

<%-- Declarations --%>
<% 	
NSAdminSessionTrackingBean bean = (NSAdminSessionTrackingBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_SESSION_TRACKING_BEAN,wrappedSession);	
String title = bean.translate("CON_SESSION_ADMINISTRATION");
Vector pageIterator = bean.getFoundSessions();
int interval = SessionTracker.getCleanupInterval();
int status = bean.getNetStoreStatus();
String id = wrappedSession.getId();
String servletName =  bean.getEncodedServlet(NSAdminConstantsServlets.NSADMINSESSIONTRACKING_SERVLET, wrappedResponse);
boolean isHttpServletRequest = wrappedRequest.isHttpServletRequest();
%>

<script language="JavaScript" type="text/javascript">
loaded = false;
function onLoad() {
	loaded = true;
	top.window.blockedLanguageSwitch = true;
}
addOnLoadEvent(onLoad);
</script>

<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminSessionTracking" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>

<%-- Title  --%>
<table width="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" width="97%" height="1"></td>
  	</tr>
  	<tr valign="top">
  		<td>
  			<%= bean.getHtmlForActionLink("ACTION_MENU", null, bean.translate("CON_BACK_TO_MENU"), bean.getResource("LeftArrow"), null) %>
		</td>
	</tr>
	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
  	
  	<tr valign="top">
		<td>
		<!-- Session data -->
		<table class="IBSLayoutTable" cellspacing="0" width="97%">
		<tr>
			<td width="50%" valign="top">
			<table width="100%">
			<tr>
				<td class="IBSHeaderCell" colspan="2" align="center"><%= bean.translate("CON_SESSION_CONFIGURATION") %></td>
			</tr>
			<%-- NetStore status  --%>
			<tr>
    			<td class="IBSListCell" nowrap width="10%"><%= bean.translate("CON_NETSTORE_STATUS") %>:</td>			
				<td class="IBSOutput" align="left">
				<% if (status == -1) { %>
		 			<%= bean.translate("CON_NETSTORE_IS_NOT_INITIALIZED") %>
				<%	} else if (status == ApplicationStatus.OPEN) { %>
					<%= bean.translate("CON_OPEN") %>
			<%	} else if (status == ApplicationStatus.CLOSING){ %>
					<%= bean.translate("CON_CLOSING") %>
			<%	} else if (status == ApplicationStatus.CLOSED_FOR_NEW) {	%>
					<%= bean.translate("CON_CLOSING") %>
			<%	} else if (status == ApplicationStatus.CLOSED) { %>
					<%= bean.translate("CON_CLOSED") %>
			<%  } %>
				</td>
			</tr>
			<tr>
   				<td class="IBSListCell" nowrap width="10%" nowrap><%= bean.translate("CON_MAX_INACTIVE_SESSION_TIME") %>:</td>
				<td class="IBSOutput" align="left" nowrap><%= SessionTracker.getMaxInactiveTime() %>&nbsp;<%= bean.translate("CON_SEC") %></td>
			</tr>
			<tr>
      			<td class="IBSListCell" nowrap width="10%"><%= bean.translate("CON_SESSION_TRACKER_CLEANUP_INTERVAL") %>:</td>
			<%	if (interval == 0) { %>
				<td class="IBSOutput" nowrap><%= bean.translate("CON_NO_AUTOMATIC_CLEANUP") %></td>
			<%	} else { %>
				<td class="IBSOutput" nowrap><%= interval %>&nbsp;<%= bean.translate("CON_SEC") %></td>
			<%	} %>
			</tr>
			<tr>
      			<td class="IBSListCell" nowrap width="10%"><%= bean.translate("CON_TOTAL_NUMBER_ACTIVE_SESSION") %>:</td>
				<td class="IBSOutput" align="left" nowrap><%= SessionTracker.getNumberOfActiveSessions() %></td>
			</tr>
			</table>
			</td>
			<% Hashtable adminSessionTable = bean.getSessionConfiguration(id);
			if (adminSessionTable != null) { %>
			<td width="50%" valign="top">
			<table>
			<tr>
				<td class="IBSHeaderCell" colspan="2" align="center"><%= bean.translate("CON_SESSION_DATA") %></td>
			</tr>
			<tr>
      			<td class="IBSListCell" nowrap width="10%" nowrap><%= bean.translate("CON_WEBUSER_NAME") %>:</td>
				<td class="IBSOutput" align="left" nowrap><%= adminSessionTable.get(NSAdminConstantsSession.WEB_USER_NAME) %></td>      
			<tr>
      			<td class="IBSListCell" nowrap width="10%" nowrap><%= bean.translate("CON_WEBUSER_CODE") %>:</td> 
				<td class="IBSOutput" align="left" nowrap><%= adminSessionTable.get(NSAdminConstantsSession.WEB_USER_CODE) %></td>
			</tr>
			<tr>
      			<td class="IBSListCell" nowrap width="10%" nowrap><%= bean.translate("CON_SESSION_ID") %>:</td>
				<td class="IBSOutput" align="left" nowrap><%= adminSessionTable.get(NSAdminConstantsSession.SESSION_ID) %></td>
			</tr>
			</table>
			</td>
		<% } %>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
  	<% if (pageIterator != null && !pageIterator.isEmpty()) {%>
	<tr>
		<td width="97%">
		<table class="IBSLayoutTable" cellspacing="0" width="97%">
		<!-- Search result -->
		<tr>
			<td colspan="2">
			<table class="IBSLayoutTable" cellspacing="0" width="97%">
			<tr>
				<td class="IBSHeaderCell" nowrap>&nbsp;</td>
				<td class="IBSHeaderCell" nowrap><%= bean.translate("CON_SESSION_ID") %>&nbsp;</td>
				<td class="IBSHeaderCell" nowrap><%= bean.translate("CON_WEBUSER_NAME") %>&nbsp;</td>
    			<td class="IBSHeaderCell" nowrap><%= bean.translate("CON_WEBUSER_CODE") %>&nbsp;</td>
				<td class="IBSHeaderCell" nowrap><%= bean.translate("CON_WEBAPPLICATION") %>&nbsp;</td>
				<td class="IBSHeaderCell" nowrap><%= bean.translate("CON_LANGUAGE") %>&nbsp;</td>
    			<td class="IBSHeaderCell" nowrap><%= bean.translate("CON_SIGNEDON") %>&nbsp;</td>
			</tr>
		<% 
			int index = bean.getIndex();
			int offset = bean.getOffset();
			for(int i = index; i < index + offset; i++) {
  				String wrappedSessionId = (String)pageIterator.elementAt(i);
				Hashtable wrappedSessionTable = bean.getSessionConfiguration(wrappedSessionId);
				if (wrappedSessionTable != null) {	
					String snID = (String)wrappedSessionTable.get(NSAdminConstantsSession.SESSION_ID);
					if (i%2 == 0) { %>
       				<tr class="IBSListRow1">
    			<% 	} else { %>
    				<tr class="IBSListRow2">
  	  			<%	} %>

   				<td class="IBSListCell" nowrap><input type="checkbox" name="OPT_SESSION_SELECTED" value="<%= wrappedSessionId %>">&nbsp;</td>
   				<td class="IBSListCell" nowrap>&nbsp;<a class="IBSCellLink" href="#" onClick="javascript:invokeLink('<%= bean.getSessionLink(snID, wrappedResponse) %>'); return false"><%= snID %></a></td>
				<td class="IBSListCell" nowrap><%= wrappedSessionTable.get(NSAdminConstantsSession.WEB_USER_NAME) %>&nbsp;</td>
   				<td class="IBSListCell" nowrap><%= wrappedSessionTable.get(NSAdminConstantsSession.WEB_USER_CODE) %>&nbsp;</td>
				<td class="IBSListCell" nowrap><%= wrappedSessionTable.get(NSAdminConstantsSession.CLIENT_APPLICATION) %>&nbsp;</td>
   				<td class="IBSListCell" nowrap><%= wrappedSessionTable.get(NSAdminConstantsSession.LANGUAGE_CODE) %>&nbsp;</td>
   			<% String signon =  (String)wrappedSessionTable.get(NSAdminConstantsSession.SIGNON_STATUS);
			if (signon.equalsIgnoreCase(SignonStatus.SIGNED_ON)) { %>
    			<td class="IBSListCell" align="center">
				<img border="0" align="middle" src="<%=bean.getResource("CheckedYellow")%>" alt="<%= bean.translate("CON_SIGNEDON") %>">
				&nbsp;
				</td>		
 		<%	} else { %>
 	 			<td class="IBSListCell">&nbsp;</td>
		<%	}  %>
  			</tr>
		<%	} 		
     	 } %>
			<tr><td class="IBSTableFooter" colspan="7" nowrap></td></tr>
			<tr>
				<td class="IBSOutput" colspan="7" nowrap>
					<input type="checkbox" name="ALL_SELECTED" onclick="javascript:selectAllCheckboxes('NetStoreAdminDetailForm',this);">
  					&nbsp;<%= bean.translate("CON_ALL_VISIBLE_LINES") %>
  					<% if (isHttpServletRequest) {%>
  						&nbsp;<%= bean.getHtmlForActionLink("ACTION_INACTIVATE",null, bean.translate("CON_INACTIVATE"), bean.getResource("Inactivate"), null) %>
  					<%}%>
  					&nbsp;<%= bean.getHtmlForActionLink("ACTION_SEND_MESSAGE",null, bean.translate("CON_SEND_MESSAGE"), bean.getResource("SendMessage"), null) %>
  					<% if (isHttpServletRequest) {%>
      					&nbsp;<%= bean.getHtmlForActionLink("ACTION_INACTIVATE_ALL",null, bean.translate("CON_INACTIVATE_ALL"), bean.getResource("InactivateAll"), null) %>
      				<%}%>
      				&nbsp;<%= bean.getHtmlForActionLink("ACTION_SEND_MESSAGE_ALL",null, bean.translate("CON_SEND_MESSAGE_ALL"), bean.getResource("SendMessageToAll"), null) %>
      			</td>
            </tr>
            <tr>
				<td class="IBSOutput" colspan="7" nowrap>
					<% if (bean.showFirstPage()) { %>
						&nbsp<%= bean.getHtmlForActionLink("ACTION_SEARCH",null, bean.translate("CON_FIRST_PAGE"), bean.getResource("FirstPage"), null) %>     	
       				<% } %>
					<% if(pageIterator != null && bean.hasMore()) { %>
						&nbsp;<%= bean.getHtmlForActionLink("ACTION_NEXT", null, bean.translate("CON_NEXT_PAGE"), bean.getResource("NextPage"), null) %>
					<% } %>
            		</td>
            </tr>
			</table>
   			</td>
		</tr>
		</table>
		</td>
	</tr>
	<!-- Message -->
	<% if(bean.hasCCFMessage()){ %>
	<tr>
		<td class="IBSOutput" colspan="2" nowrap>     
    	<small>
       		<%= bean.getMessageHtml(bean.getCCFMessage()) %>
       	</small>
       	<% bean.setCCFMessage(null); %>
		<br>
   		</td>
	</tr>
  <%  } %>
		
	<tr>
		<td class="IBSEmpty">&nbsp;</td>
   	</tr>	
	<!-- Send message text box -->
	<tr>
		<td class="IBSListCell" nowrap colspan="2"><%= bean.translate("CON_MESSAGE_TEXT") %><br><textarea name="OPT_MESSAGE_TEXT" rows="5" cols="55"><%= bean.getEnteredMessageText() %></textarea></td>
	</tr>
<% }   %>
	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
	<!-- Search actions and fields -->
	<tr>
		<td class="IBSHeaderCellLight" nowrap><%= bean.translate("CON_NEW_SEARCH") %></td>
	</tr>
	<tr>
		<td class="IBSPageTitleDivider" colspan="2" width="100%" height="1"></td>
	</tr>
	<tr>
		<td class="IBSEmpty">&nbsp;</td>
   	</tr>	
	<tr>
		<td nowrap colspan="2">
       	<table class="IBSLayoutTable" cellspacing="0" width="97%">
	  		  <%-- Include function Search bar and validationErrors --%>
         	 <%@ include file="NSAdminFuncSearchBarAndValidationErrors.jspbb" %>          
			<tr>
      			<td class="IBSLabel" width="10%" nowrap><%= bean.translate("CON_WEBUSER_CODE") %>:</td>
        		<td class="IBSOutput"><p><input class="IBSInput" type="text" name="QRY_SEARCH_WEBUSERCODE" value="<%= bean.getSelectedWebUserCode() %>" maxlength="30"></p></td>
      		</tr>
	  		<tr>
				<td class="IBSLabel" width="10%" nowrap><%= bean.translate("CON_SIGN_ON_USERS") %>:</td>
				<td class="IBSOutput"><%= bean.getSelectFormSignOnUsers() %></td>
	  		</tr>
	  		
	  		<tr>
				<td class="IBSLabel" width="10%"><%= bean.translate("CON_APPLICATION") %>:</td>
				<td class="IBSOutput"><%= bean.getSelectFormApplication() %></td>
	  		</tr>
        	<tr>			
           		<td class="IBSLabel" nowrap width="10%"><%= bean.translate("CON_LANGUAGE") %>:</td>
           		<td class="IBSOutput"><%= bean.getSelectFormLanguage() %></td>
        	</tr>
        	<!-- Include function SearchBar -->
      		<%@ include file="FuncSearchBar.jspbb" %> 
        </table>
		</td>
	</tr>
	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
</table>
</form>