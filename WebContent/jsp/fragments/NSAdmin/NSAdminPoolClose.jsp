<%-- Page for closing a pool --%>
<%-- Imports --%>
<%@ page import="se.ibs.ns.adm.*, se.ibs.ccf.*" %>

<%-- Declarations --%>
<%
NSAdminPoolBean bean = (NSAdminPoolBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_POOL_BEAN,wrappedSession);
String servletName = bean.getEncodedServlet(NSAdminConstantsServlets.NSADMINPOOLCLOSE_SERVLET, wrappedResponse); 
String closeForm = bean.getHtmlSelectForm("QRY_POOLNAME_CLOSE");
String title = bean.translate("CON_CLOSE_POOL");
%>

<script language="JavaScript" type="text/javascript">
loaded = false;
function onLoad() {
	loaded = true;
	top.window.blockedLanguageSwitch = true;
}
addOnLoadEvent(onLoad);
</script>

<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminPoolClose" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>

<%-- Title  --%>
<table width="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" width="97%" height="1"></td>
  	</tr>
  	<tr valign="top">
  		<td>
  			<%= bean.getHtmlForActionLink("ACTION_MENU", null, bean.translate("CON_BACK_TO_MENU"), bean.getResource("LeftArrow"), null) %>
		</td>
	</tr>
  	<tr>
  		<td class="IBSEmpty">
  			&nbsp;
  		</td>
  	</tr>
  	<!-- Message -->
  	<tr>
		<td class="IBSOutput">                				                          
   		<% if(bean.hasCCFMessage()){ %>
    		<small>
        		<%= bean.getMessageHtml(bean.getCCFMessage()) %>
        	</small>
        	<% bean.setCCFMessage(null); %>
			<br>
   		<%  } %>
   		</td>
	</tr>
	<tr valign="top">
		<td>
		<%-- Display area for optional pools to close --%>
		<table border="0" width=100%>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="IBSLabel" width="10%"><%= bean.translate("CON_CLOSE_POOL") %>:</td>
	
			<% if (closeForm != null) { %>
	  			<td class="IBSOutput">
	  			<%=	closeForm %>
				&nbsp;<input name="ACTION_CLOSE_POOL" type="submit" value="<%= bean.translate("CON_CLOSE") %>" onClick="javascript:confirmClosePool('NetStoreAdminDetailForm', '<%= bean.translate("CON_CONFIRM_CLOSE") %>', '<%= servletName %>'); return false">
				</td>
  		
			<% } else { %>
	 			<td class="IBSOutput">&nbsp;<%= bean.translate("CON_NO_POOL_TO_CLOSE") %></td>
			<% } %>		
		</tr>
		</table>
		</td>
	</tr>
</table>
</form>