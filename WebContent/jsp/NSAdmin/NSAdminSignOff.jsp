<%-- Signoff (NSAdmin) --%>
<html>

<%-- Imports --%>
<%@ page import="se.ibs.ccf.*, se.ibs.ns.adm.*" %>

<%-- Declarations --%>
<%@ include file="FuncWrappers.jsp" %>
<%
String servletName = baseBean.getEncodedServlet(NSAdminConstantsServlets.NSADMINSTART_SERVLET, wrappedResponse);
String title = baseBean.translate("CON_SIGNOFF");
String css = baseBean.getStyleSheet();

SessionTracker.unregisterSession(wrappedSession);
%>

<head>
	<title><%= title %></title>
	<link rel="STYLESHEET" href="<%= css %>" type="text/css">
	<script language="JavaScript" type="text/javascript">
		function redirect(url) {
			top.location.replace(url);
		}
	</script>
</head>

<body onLoad="javascript:redirect('<%= servletName %>'); return false;">
</body>

</html>
