<%-- Signon prompt for administration --%>
<html>

<%-- Imports --%>
<%@ page import="se.ibs.ccf.*, se.ibs.ns.adm.*" %>

<%-- Declarations --%>
<%@ include file="FuncWrappers.jsp" %>
<%
NSAdminSignonBean signonBean = (NSAdminSignonBean)SessionHelper.getSessionObject(NSAdminConstantsSession.NSADMIN_SIGNON_BEAN, wrappedSession);		
String msg = signonBean.getValidationError();
String warnMsg = signonBean.getSignonWarningMsg();
CCFMessage warn = null; 
if (warnMsg != null && !warnMsg.equals("")) {
	warn = baseBean.getCCFFactory().createCCFWarning(warnMsg);
}
String title = baseBean.translate("CON_ADMINISTRATION_SIGN_ON");
String servletName = baseBean.getEncodedServlet("se.ibs.ns.adm.NSAdminSignonServlet", wrappedResponse);
%>

<%@ include file="FuncHead.jspbb" %>

<body class="IBSAdmBody" onload="loaded = true; document.forms[0].User.focus();" >
<form method="POST" name="NetStoreAdminDetailForm" id="NSAdminSignOn" action="<%=servletName%>">
<%@ include file="FuncHidden.jspbb" %>
<%@ include file="NSAdminFuncPageBegin.jspbb" %>

<%-- Title  --%>
<table width="97%">
	<tr>
    	<td class="IBSPageTitleText" valign="bottom" width="50%" nowrap>
      		<%@ include file="FuncPageTitle.jspbb" %>          
    	</td>      
  	</tr>
  	<tr>
  		<td class="IBSPageTitleDivider" width="97%" height="1"></td>
  	</tr>
  	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
  	<tr>
  		<td class="IBSEmpty">&nbsp;</td>
  	</tr>
	<tr valign="top">
		<td>
		<%-- Enter user id and password  --%>
		<table class="IBSLayoutTable" cellspacing="0" width="100%">
		<tr>
			<td class="IBSLabel" width="10%" nowrap><%= baseBean.translate("CON_USER_ID") %>:</td>
			<td><input class="IBSInput" type="text" tabindex="1" name="User" value="<%= signonBean.getSignonUserCode() %>"></td>
		</tr>

		<tr>
			<td class="IBSLabel" width="10%" nowrap><%= baseBean.translate("CON_PASSWORD") %>:</td>
    		<td><input class="IBSInput" tabindex="2" type="password" name="Password"></td>
		</tr>
		<% if(!baseBean.isOneLanguage()) {
          HtmlSelectForm lf = (HtmlSelectForm)signonBean.getAdditionalSignonData("LANGUAGE_FORM");
          if (lf != null) { %>
   		<tr>
        	<td class="IBSLabel" width="10%" nowrap><%= baseBean.translate("CON_CONTINUE_IN") %>:</td>
            <td><%= lf.getForm("NetStoreAdminDetailForm") %> </td>
            <td>&nbsp;</td>
        </tr>
        <% } %> 
  	<% } %> 
	<tr>
		<td>&nbsp;</td>
		<td>
			<br>
				<input type="submit" value="<%= baseBean.translate("CON_SIGN_ON") %>" name="ACTION_SIGNON" tabindex="3" onclick="submitButtonOnce(this); return false">
			<br>
		</td>
	</tr>
	<!-- Message -->
	<tr>
		<td colspan="2" align="left" nowrap><%= baseBean.getErrorMessageHtml(msg) %></td>
	</tr>

	<% if (warn != null) { %>
	 <tr>
 		<td colspan="2" align="left" nowrap><%= baseBean.getMessageHtml(warn) %></td>
  	</tr>
	<% } %>
	</table>
	</td>
</tr>
</table>

<%@ include file="NSAdminFuncEnd.jspbb" %>
</form>
</body>
</html>