<%
boolean isInitialized = false;
try {
	isInitialized = (se.ibs.ns.adm.NSAdminObject.isInitialized()
			&& session.getAttribute(se.ibs.ccf.CCFConstantsSession.CONNECTION) != null);
} catch (Exception ignore) {
}
if (!isInitialized) {
	String arg0 = // se.ibs.ns.adm.NSAdminObject.getJspName(... );
		se.ibs.ns.adm.NSAdminConstantsPages.NSADMINSTART;
	if (!arg0.startsWith("/"))
		arg0 = "/" + arg0;
	RequestDispatcher rd = application.getRequestDispatcher(arg0);
	rd.forward(request, response);
	return;
}

se.ibs.ccf.Request wrappedRequest = new se.ibs.ccf.Request(request);
se.ibs.ccf.Response wrappedResponse = new se.ibs.ccf.Response(response, null);
se.ibs.ccf.Session wrappedSession = new se.ibs.ccf.Session(session);
se.ibs.ns.adm.NSAdminBean baseBean = se.ibs.ns.adm.NSAdminBean.getInstance(wrappedSession);
%>