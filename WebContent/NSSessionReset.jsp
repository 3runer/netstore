<%-- SessionTimeout
NOTE: 	This jsp is assumed to exist in the web application root.
		A session is normally not available when this method is used. --%>
<html>

<%-- Imports --%>
<%@ page import="se.ibs.ccf.*, se.ibs.ns.cf.*" %>

<%-- Declarations --%>
<%
se.ibs.ccf.Request wrappedRequest = new se.ibs.ccf.Request(request);
se.ibs.ccf.Session wrappedSession = new se.ibs.ccf.Session(session);
String ulc = NSRequestHelper.getUserLanguageCode(wrappedRequest);
String title = NSObject.translate("CON_SESSION_ENDED", ulc, "Session ended");
String css = NSObject.getStyleSheet(request.getHeader("User-Agent").indexOf("MSIE") < 0);
String url = response.encodeURL(NSObject.getServlet(NSConstantsServlets.START_SERVLET));
SessionTracker.unregisterSession(wrappedSession);
%>

<head>
	<title><%= title %></title>
	<link rel="STYLESHEET" href="<%= css %>" type="text/css">
	<script language="JavaScript" type="text/javascript">
		function redirect(url) {
			if (top.window.name == 'MAIN') {		    
				top.location.href = url;
			} else {
				parent.opener.top.location.href = url;
				self.close();			
			}
		}
	</script>
</head>

<body onLoad="javascript:redirect('<%= url %>'); return false;">
</body>

</html>