<%-- Start NetStore Administration 
NOTE:  This jsp is assumed to exist in the web application root. --%>
<html>

<%-- Imports --%>
<%@ page session="false" import="net.ibs.framework.*,se.ibs.bap.*,se.ibs.bap.util.*,se.ibs.ccf.*,se.ibs.ns.adm.*" %>

<%-- Declarations --%>
<% 	
	// No caching
	response.setHeader("Cache-Control", "no-cache");	// For HTTP version 1.1
	response.setHeader("Pragma", "no-cache");	 		// For HTTP version 1.0
	response.setDateHeader("Expires", 0); 				// To prevent caching at the proxy server
	
	String nextPage = null;
	
	// override config
    if (!IBSNetStoreEnvironment.isRegistered("Admin")) {
    	if (IBSNetStoreEnvironment.getBasePath() == null) {
    		String property = System.getProperty("xt.bin");
    		if (property != null && (property = property.trim()).length() > 0) {
				IBSNetStoreEnvironment.setBasePath(property);
			}
    	}
		String property = System.getProperty("xt.esite");
		if (property == null || (property = property.trim()).length() == 0) {
			property = "NS";
		}
		try {
			IBSNetStoreEnvironment.registerApplication(property, "Admin");
		} catch (Exception e) {
			BAPTrace.error("NSAdminStart.jsp", "Unexpected exception: ", e);
   			request.setAttribute("SystemSetupError", e.getMessage());
   			nextPage = "SystemSetupError.jsp";
   		}	
  	}

	// initialize NetStore Admin (if not already so)
	BAPConnection connection = null;
	if (nextPage == null) {
		try {
			connection = NSAdminObject.init();
			// is session in use?
		    Session s = new Session(request.getSession(false));
		    if (!s.isEmpty()) {
	    		String clientAppl =
	    			NSAdminObject.getConfigAttribute(CCFConstantsConfig.CLIENT_APPLICATION);
	    		String comparand = (String) s.getAttribute(CCFConstantsSession.CLIENT_APPLICATION);
	    		if (comparand == null || !clientAppl.equalsIgnoreCase(comparand)) {
	    			SessionTracker.unregisterSession(s);
	    		}
		    }	 
		} catch (Exception e) {
			BAPTrace.error("NSAdminStart.jsp", "Unexpected exception: ", e);
   			request.setAttribute ("SystemSetupError", e.getMessage());
   			nextPage = "SystemSetupError.jsp";
	    }		
	}
	
    if (nextPage != null) {
//    	nextPage = NSAdminObject.getJspName(nextPage);
    	if (!nextPage.startsWith("/")) {
    		nextPage = "/" + nextPage;
    	}
    	RequestDispatcher rd = application.getRequestDispatcher(nextPage);
 	    rd.forward(request, response);
 	    return;
    }        
        
    // get the start servlet
    String webApplicationPath = NSAdminObject.getConfigAttribute("WebApplicationPath");
	if (webApplicationPath == null) {
		webApplicationPath = "";
	}
    String servletPath = NSAdminObject.getConfigAttribute("ServletBasePath");
    String url = response.encodeURL(webApplicationPath + servletPath + "se.ibs.ns.adm.NSAdminStartServlet");  
    url = response.encodeURL(BAPStringHelper.scanAndReplace(url, "//", "/"));
    // start a new session and save the connection on it
	Session s = new Session(request.getSession(true));
	s.setAttribute(CCFConstantsSession.REGISTERED,  Boolean.toString(false));
	if (connection != null)
		s.setAttribute(CCFConstantsSession.CONNECTION, connection);
%>

<head>
	<title>Start</title>
	<script language="JavaScript">
		function redirect(url) {
  			top.location.replace(url);
		}
	</script>
</head>

<body onLoad="javascript:redirect('<%= url %>'); return false;"></body>

</html>