<%-- Show 'NetStore is closed' page. 
NOTE:	This jsp is assumed to exist in the web application root. 
		Only the application status object is available. --%>
<html>

<%-- Imports --%>
<%@ page session="false" import="se.ibs.ccf.*, se.ibs.ns.cf.*, java.util.*" %>

<%-- Declarations --%>
<%
se.ibs.ccf.Request wrappedRequest = new se.ibs.ccf.Request(request);
String lc = wrappedRequest.getParameter(NSConstantsSession.LANGUAGE_CODE);
String css = NSObject.getStyleSheet(wrappedRequest.getHeader("User-Agent").indexOf("MSIE") < 0);
String title = NSObject.translate("CON_CLOSED",lc,"Closed");
Hashtable statusMsg = NSObject.getApplicationStatus().getStatusMessages();
String themePath  = NSObject.getThemeResourcePath();
String imageExt = CCFObject.getImageExtension(wrappedRequest.getHeader("User-Agent"));
%>

<head>
<title><%= title %></title>
<link rel="STYLESHEET" href="<%= css %>"  type="text/css">
</head>

<body class="IBSBody">
<%-- FuncPageBegin --%>
<div align="left">
<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
	<tr>
		<td height="20" valign="top" colspan="2"></td>
	</tr>
	<tr>
		<td width="20" align="left"><img border="0" src="<%= themePath %>BackgroundBody<%= imageExt %>" width="20" height="1"></td>
		<td align="left" >
			<table border="0" cellspacing="0" cellpadding="0" height="97%" width="97%">
			<tr>	
				<td width="9" height="9"><img border="0" src="<%= themePath %>MainTopLeft<%= imageExt %>" width="9" height="9"></td>
		<%	if (((String) wrappedRequest.getHeader ("User-Agent")).indexOf ("MSIE") < 0) { %>
				<td class="IBSBackgroundDummyCell">
						<img border="0" src="<%= themePath %>1pxDummy<%= imageExt %>" width="1" height="1"></td>
		<%	} else { %>
				<td class="IBSBackgroundDummyCell" width="100%"><img border="0" src="<%= themePath %>1pxDummy<%= imageExt %>" width="1" height="1"></td>
		<%	} %>
				<td width="9" height="9"><img border="0" src="<%= themePath %>MainTopRight<%= imageExt %>" width="9" height="9"></td>
			</tr>		
			<tr>
				<td class="IBSBackgroundCell" colspan="3">	
				<table cellpadding="0" cellspacing="0" width="97%" height="97%">
				<tr>
					<td valign="top"><img border="0" src="<%= themePath %>1pxDummy<%= imageExt %>" width="20" height="1"></td>
					<td valign="top" width="100%">
<%-- end --%>					
          		<table border="0" width="100%">
					<tr>
						<td class="IBSPageTitleText" valign="bottom" nowrap>
            				<font class="IBSPageTitleHeader">NetStore</font>
            			</td>
          			</tr>
          			<tr>
          				<td class="IBSPageTitleDivider" height="1"></td>
  					</tr>
					<tr>
						<td class="IBSEmpty" height="100px">&nbsp;</td>
   					</tr>
   						
			<%	if (statusMsg.isEmpty()) { %>	
    				<tr>
    					<td class="IBSTextWarning" align="center">
    						<%= NSObject.translate("TXT_CF_017",lc,"The application is closed!") %></td>
        			</tr>
			<%	} else { 
						Enumeration e = statusMsg.elements();
						while (e.hasMoreElements()) { %>
		     				<tr>
    							<td class="IBSTextWarning" align="center"><%= (String) e.nextElement() %></td>
        	     			</tr>	
  			<%			}
				} %>
      			</table>
<%-- FuncPageEnd --%>	      			
					</td>
    			</tr>
				</table>
  				</td>
   			</tr>
    		</table>
    	</td>
	</tr>
</table>
</div>
<%-- end --%>
</body>
</html>