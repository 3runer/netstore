<%-- Show system message in target frame 
NOTE:  This jsp is assumed to exist in the web application root. --%>
<html>

<%-- Imports --%>
<%@ page import="se.ibs.ns.cf.*, se.ibs.ccf.*" %>

<%-- Declarations --%>
<%
se.ibs.ccf.Session wrappedSession = new se.ibs.ccf.Session(session);
SessionHelper sh = SessionHelper.getSessionHelper(wrappedSession);
String sysMsg = "UNKNOWN SYSTEM MESSAGE";
String css = null;   
String resourcePath  = NSObject.getResourcePath();
if (sh != null) {
	sysMsg = (String)sh.getSessionObject(CCFConstantsSession.SYSTEM_MESSAGE);
	sh.removeSessionObject(CCFConstantsSession.SYSTEM_MESSAGE);
	NSBaseBean bean = (NSBaseBean)sh.getSessionObject(NSConstantsSession.NS_BASE_BEAN);
	if (bean != null) {
		css = bean.getStyleSheet();
	}
}
if (css == null) {
	css = NSObject.getStyleSheet(request.getHeader("User-Agent").indexOf("MSIE") < 0);
}
String themePath  = NSObject.getThemeResourcePath();
String imageExt = CCFObject.getImageExtension(request.getHeader("User-Agent"));
%>

<head>
<title>System Message</title>
<link rel="STYLESHEET" href="<%= css %>"  type="text/css">
</head>
	
<body class="IBSBody" onload="loaded = true;">
<div align="left">
<table border="0" width="95%" cellspacing="0" cellpadding="0" height="95%">
	<tr>
		<td height="20" valign="top" colspan="2"></td>
	</tr>
	<tr>
		<td width="20" align="left"><img border="0" src="<%= themePath %>BackgroundBody<%= imageExt %>" width="20" height="1"></td>
		<td align="left" >
			<table border="0" cellspacing="0" cellpadding="0" height="100%" width="95%">
			<tr>	
				<td width="9" height="9"><img border="0" src="<%= themePath %>MainTopLeft<%= imageExt %>" width="9" height="9"></td>
				<td class="IBSBackgroundDummyCell" width="100%" height="9"><img border="0" src="<%= themePath %>1pxDummy<%= imageExt %>" width="1" height="1"></td>
				<td width="9" height="9"><img border="0" src="<%= themePath %>MainTopRight<%= imageExt %>" width="9" height="9"></td>
			</tr>		
			<tr>
				<td class="IBSBackgroundCell" colspan="3" height="100%">	
				<table cellpadding="0" cellspacing="0" height="100%">
				<tr>
					<td valign="top" height="50%"><img border="0" src="<%= themePath %>1pxDummy<%= imageExt %>" width="20" height="1"></td>
					<td valign="top" width="100%" height="50%">
					<table border="0" width="97%">
    				<tr>
      					<td  valign="top" width="100%">
          				<font class="IBSPageTitleHeader">System Message</font>
      					</td>
   					</tr>
   					<tr>
          				<td class="IBSPageTitleDivider" width="97%" height="1"></td>
  					</tr>
    				<tr>
						<td class="IBSEmpty">&nbsp;</td>
   					</tr>	
   					<tr>
						<td class="IBSEmpty">&nbsp;</td>
   					</tr>	
   					<tr>
						<td class="IBSEmpty">&nbsp;</td>
   					</tr>	
   					<tr>
						<td class="IBSEmpty">&nbsp;</td>
   					</tr>	
   					<tr>
						<td class="IBSEmpty">&nbsp;</td>
					</tr>
    				<tr>
      					<td class="IBSTextWarning" align="center"><%= sysMsg %></td>
    				</tr>
					</table>
		  			</td>
    			</tr>
    	</table>
    	</td>
		</tr>
	</table>
	</td>
  </tr>
</table>
</div>
</body>
</html>