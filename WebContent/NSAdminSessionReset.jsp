<%-- SessionTimeout (NSAdmin)
NOTE: 	This jsp is assumed to exist in the web application root.
		A session is normally not available when this method is used. --%>
<html>

<%-- Imports --%>
<%@ page import="se.ibs.ccf.*, se.ibs.ns.adm.*" %>

<%-- Declarations --%>
<%
se.ibs.ccf.Session wrappedSession = new se.ibs.ccf.Session(session);
String title = NSAdminObject.translate("CON_SESSION_ENDED", "Session ended");
String css = NSAdminObject.getStyleSheet(request.getHeader("User-Agent").indexOf("MSIE") < 0);
String url = response.encodeURL(NSAdminObject.getServletPath(NSAdminConstantsServlets.NSADMINSTART_SERVLET));
SessionTracker.unregisterSession(wrappedSession);
%>

<head>
	<title><%= title %></title>
	<link rel="STYLESHEET" href="<%= css %>" type="text/css">
	<script language="JavaScript" type="text/javascript">
		function redirect(url) {
			top.location.replace(url);
		}
	</script>
</head>

<body onLoad="javascript:redirect('<%= url %>'); return false;">
</body>
</html>