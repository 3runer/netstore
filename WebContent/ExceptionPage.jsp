<%-- Show exception page.
NOTE:	This jsp is assumed to exist in the web application root.
		The bean may not be available and session attributes might not exist.
		This jsp tries to translate all texts but defaults to English if unable to. --%>
<html>

<%-- Imports --%>
<%@ page import="se.ibs.ccf.*, se.ibs.ns.cf.*, se.ibs.ns.adm.* " %>

<%-- Declarations --%>
<%
se.ibs.ccf.Request wrappedRequest = new se.ibs.ccf.Request(request);
se.ibs.ccf.Response wrappedResponse = new se.ibs.ccf.Response(response, null);
se.ibs.ccf.Session wrappedSession = new se.ibs.ccf.Session(session);
String clientAppl =
	NSAdminObject.getConfigAttribute(CCFConstantsConfig.CLIENT_APPLICATION);		
String comparand = (String) wrappedSession.getAttribute(CCFConstantsSession.CLIENT_APPLICATION);
boolean isNSAdmin = (comparand != null && clientAppl.equalsIgnoreCase(comparand));
ExceptionWrapper ew =
	(ExceptionWrapper) wrappedSession.getAttribute(CCFConstantsSession.EXCEPTION_WRAPPER);
wrappedSession.removeAttribute(CCFConstantsSession.EXCEPTION_WRAPPER);
CCFBaseBean bean = (ew == null) ? null : ew.getBean();
String languageCode = (String) wrappedSession.getAttribute(CCFConstantsSession.LANGUAGE_CODE);
if (languageCode == null)
	languageCode = NSRequestHelper.getUserLanguageCode(wrappedRequest);
String css, resourcePath, userAgent = (String) request.getHeader("User-Agent");
if (isNSAdmin) {
	css = NSAdminObject.getStyleSheet(userAgent.indexOf("MSIE") == -1);
	resourcePath = NSAdminObject.getConfigAttribute(CCFConstantsConfig.RESOURCE_PATH);
} else {
	css = NSObject.getStyleSheet(userAgent.indexOf("MSIE") == -1);
	resourcePath = NSObject.getResourcePath();
}
String imageExt = CCFObject.getImageExtension(userAgent),
	themePath = css.substring(0, css.lastIndexOf("/") + 1),
	title = NSObject.translate("CON_ERROR", languageCode, "Error"),
	url = (ew == null) ? null : wrappedResponse.encodeURL(isNSAdmin ? NSAdminObject.getServletPath(
				NSAdminConstantsServlets.NSADMINSTART_SERVLET) : NSObject.getServlet(NSConstantsServlets.START_SERVLET));
   
SessionTracker.unregisterSession(wrappedSession);
%>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="pragma" content="no-cache">
		
		<link rel="stylesheet" type="text/css" href="<%= css %>" />
		
		<script language="JavaScript">
			window.history.forward(1);
		</script>
		
		<title><%= title %></title>
		
		<script language="JavaScript">
			function restart(url) {
				if (top.window.name == 'MAIN' || <%= isNSAdmin %>) {    
	    			top.location.href = url;
	    		} else {
	        		parent.opener.top.location.href = url;
					self.close();			
				}
			}
		</script>
	</head>
	
	<body class="IBSBody">
<%-- page begin --%>	
		<div align="left">
		<table border="0" cellspacing="0" cellpadding="0" width="97%" height="97%">
		<tr><td height="20" colspan="3" valign="top"></td></tr>
		<tr>
			<td width="20"><img border="0" src="<%= themePath %>BackgroundBody<%= imageExt %>" width="20" height="1"></td>
			<td>
				<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
					<tr>
						<td width="9" height="9"><img border="0" src="<%= themePath %>MainTopLeft<%= imageExt %>" width="9" height="9"></td>
						<td class="IBSBackgroundDummyCell" width="100%" height="9"><img border="0" src="<%= themePath %>1pxDummy<%= imageExt %>" width="1" height="1"></td>
						<td width="9" height="9"><img border="0" src="<%= themePath %>MainTopRight<%= imageExt %>" width="9" height="9"></td>
					</tr>
					<tr>
						<td class="IBSBackgroundCell" colspan="3" height="100%">
							<table>
								<tr valign="top">
									<td><img border="0" src="<%= themePath %>1pxDummy<%= imageExt %>" width="20" height="1"></td>
									<td width="100%">
<%-- end --%>
										<table border="0" width="100%" height="100%">
											<tr><td style="text-align: bottom;" nowrap><div class="IBSPageTitleHeader" style="margin-top: 10px;"><%= title %></div></td></tr>
											<tr><td class="IBSPageTitleDivider" width="100%" height="1"></td></tr>
											<tr><td class="IBSEmpty">&nbsp;</td></tr>
										<%	String s = null;
											if (ew == null) {
												s = NSObject.translate("TXT_CF_018", languageCode, "An unexpected error has occurred, exception details missing.");
											} else {
												s = ew.getDescription();
												if (s == null || (s.length() == 0))
													s = NSObject.translate("TXT_CF_016", languageCode, "An unexpected error has occurred; see below for details.");
											} %>
											<tr><td nowrap><span class="IBSTextWarning"><%= s %></span></td></tr>
										<%	if (ew != null) { %>
											<tr><td style="padding-top: 10px;" nowrap><a class="IBSActionLink" href="#" onclick="javascript:restart('<%= url %>'); return false"><img
														border="0" src="<%= resourcePath %>StartNewSession<%= imageExt %>">&nbsp;<%= NSObject.translate("TXT_CF_020",
																languageCode, "Click here to obtain a new session.") %></a></td></tr>
										<%	} %>
											<tr><td class="IBSEmpty">&nbsp;</td></tr>
										</table>
										<%	if (ew != null ) {
												String invokerClass = ew.getInvokerClass();
												String invokerMethod = ew.getInvokerMethod();
												if (invokerClass.length() > 0 || invokerMethod.length() > 0) { %>
										<table class="IBSListTable2">
											<tr>
												<td class="IBSHeaderCell" nowrap>&nbsp;<%= NSObject.translate("CON_INVOKER_CLASS", languageCode,
														"Invoker class") %>&nbsp;</td>
												<td class="IBSHeaderCell" nowrap>&nbsp;<%= NSObject.translate("CON_INVOKER_METHOD", languageCode,
														"Invoker method") %> &nbsp;</td>
											</tr>
											<tr>
												<td class="IBSListCell2">&nbsp;<%= ew.getInvokerClass() %></td>
												<td class="IBSListCell2">&nbsp;<%= ew.getInvokerMethod() %></td>
											</tr>
										<%			Throwable t = ew.getThrowable();
													if (t != null) { %>
											<tr><td class="IBSHeaderCell" colspan="2" nowrap>&nbsp;<%= NSObject.translate(
																"CON_EXCEPTION_STACK_TRACE", languageCode, "Exception stack trace") %>&nbsp;</td></tr>
										<%				StringBuffer trace = new StringBuffer("&nbsp;" + t.toString());
														trace.append("<div style=\"margin-left: 45px; padding-right: 20px;\">");
														StackTraceElement[] arr = t.getStackTrace();
														for (StackTraceElement ste: arr)
															trace.append(ste + "<br />");
														trace.append("</div>");
														trace.append("<br />"); %>																
											<tr><td class="IBSListCell2" colspan="2"><%= trace %></td></tr>
										<%			} %>
										</table>
										<%		}
											} %>
<%-- page end --%>
									</td>
									<td><img border="0" src="<%= themePath %>1pxDummy.gif" width="20" height="1"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td width="20"><img border="0" src="<%= themePath %>BackgroundBody.gif" width="20" height="1"></td>
		</tr>
		</table>
		</div>
<%-- end --%>
	</body>
</html>