<%-- Redirect 
NOTE:  This jsp is assumed to exist in the web application root. --%>
<html>

<%-- Imports --%>
<%@ page import="se.ibs.bap.util.*, se.ibs.ccf.*, se.ibs.ns.cf.*" %>

<%-- Declarations --%>  
<%
se.ibs.ccf.Request wrappedRequest = new se.ibs.ccf.Request(request);
String webApplicationPath = NSObject.getConfigAttribute(NSConstantsConfig.WEB_APPLICATION_PATH);
if (webApplicationPath == null)
	webApplicationPath = "";
String pageUri = webApplicationPath + "/SessionReset.html";
String redirectPage = (String) request.getAttribute(NSConstantsSession.PAGE_REDIRECT);
Response wrappedResponse = new Response(response, null);
if (redirectPage != null)
	pageUri = wrappedResponse.encodeURL(webApplicationPath + "/" + redirectPage);
pageUri = BAPStringHelper.scanAndReplace(pageUri, "//", "/");

// Get language code (to be used on pages where session and bean not available)
String lc = "";
if(redirectPage != null) {
	lc = NSRequestHelper.getUserLanguageCode(wrappedRequest);
	if (lc == null) {
		lc = "";
	} else {
		if (pageUri.indexOf("?") < 0) {
			pageUri += "?";
		} else {
			pageUri += "&";
		}
		pageUri = pageUri + NSConstantsSession.REQUEST_LANGUAGE_CODE + "=" + lc;
	}
}

// No caching
wrappedResponse.setHeader("Cache-Control","no-cache");	// For HTTP version 1.1
wrappedResponse.setHeader("Pragma","no-cache"); 		// For HTTP version 1.0
wrappedResponse.setDateHeader ("Expires", 0); 			// To prevent caching at the proxy server 
%>

<head>
	<title>Redirect</title>
	<script language="JavaScript" type="text/javascript">
		function redirect(url) {
			if (top.window.name != 'MAIN') {
				self.close();
				document.forms[0].submit();
			} else {
		    	top.location.replace(url);		    
  			}	
		}
	</script>
</head>

<body onload="redirect('<%= pageUri %>'); return false;">
	<form method="POST" name="NSSimpleRedirect" target="MAIN" action="<%= pageUri %>">
		<input name="<%= NSConstantsSession.LANGUAGE_CODE %>"  type="hidden"  value="<%= lc %>"> 
	</form>
</body>

</html> 