<%-- 
	Show system setup error page.
	NOTE: This jsp is assumed to exist in the web application root.
--%>

<html>
	<%@ page session="false" %>
	<body topmargin="20">
		<div style="color: #CC0000; font-family: Verdana, Arial; font-size: 24px;">System setup error</div><br />
		<%	String s = (String) request.getAttribute("SystemSetupError");
			s = s.replace("\r", "").replace("\n", "<br />"); 
			int index = s.indexOf("<br />");
			String line1 = (index > 0) ? s.substring(0, index) : s;
			s = (index > 0) ? s.substring(index + 6) : null; %>
		<div style="font-family: Verdana, Arial; font-size: 18px;"><%=line1%></div><br />
<%	if (s != null) { %>
		<div style="margin-left: 45px; font-family: Verdana, Arial; font-size: 18px;"><%=s%></div><br />
<%	} %>		
		<div style="font-family: Verdana, Arial; font-size: 14px; font-style: italic;">(see log for more information)</div>
	</body>
</html>