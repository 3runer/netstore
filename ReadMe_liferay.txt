**********************************************************************************************************************
All the following 3 files should be placed in the root of the Eclipse workspace folder which contains your projects.
**********************************************************************************************************************
- build_liferay.properties => set the deploy-dir property to the folder in which you would like the war file to be created. Please note that the content of this folder will be cleared when running the build script. So do not use a folder which already contains files.

 - build_liferay.xml => the actual build script. The following Eclipse project folders are used: aswbap for the BAP classes, ccf for the CCF classes and netstore for the NS classes and webcontent.
 
 - build_liferay.bat => bat file which is used to start the build
 
 